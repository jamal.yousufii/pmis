<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyWeather extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_weathers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('daily_report_id');
            $table->string('high_temp',32)->nullable();
            $table->string('low_temp',32)->nullable();
            $table->string('weather_precipitation',32)->nullable();
            $table->string('wind_speed',32)->nullable();
            $table->integer('is_worked_stoped'); 
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_weathers');
    }
}
