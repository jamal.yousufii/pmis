<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableImplementationsAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('implements', function (Blueprint $table) {
            $table->tinyInteger('operation');
            $table->date('operation_date')->default(null);
            $table->integer('operation_number');
            $table->text('operation_description');
            $table->integer('operation_by');
            $table->dateTime('operation_at')->default(null);
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('implements', function (Blueprint $table) {
            $table->dropColumn('operation');
            $table->dropColumn('operation_date');
            $table->dropColumn('operation_number');
            $table->dropColumn('operation_description');
            $table->dropColumn('operation_by');
            $table->dropColumn('operation_at');
            $table->dropColumn('completed');
        });
    }
}
