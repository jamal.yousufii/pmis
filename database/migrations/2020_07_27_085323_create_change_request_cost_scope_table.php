<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeRequestCostScopeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('change_request_cost_scope', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_quantity_id')->nullable()->default(NULL);
            $table->integer('project_id');
            $table->string('operation_type')->nullable()->default(NULL);
            $table->string('description')->nullable()->default(NULL);
            $table->integer('location_id');
            $table->integer('unit_id');
            $table->integer('amount');
            $table->integer('price');
            $table->integer('total_price');
            $table->integer('new_old_bill')->comment('1 for new 0 for old bill of quantity')->default(0);
            $table->tinyInteger('operation')->comment('0: not approved, 1: approved, 2: rejected')->default(0);
            $table->string('operation_description')->nullable()->default(null);
            $table->integer('operation_by')->nullable()->default('0');
            $table->dateTime('operation_at')->nullable()->default(null);
            $table->string('remarks');
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_request_cost_scope');
    }
}
