<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreePhaseInspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('three_phase_inspections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('inspection_id');
            $table->unsignedInteger('inspection_sub_id');
            $table->unsignedInteger('status')->comment('0 => stop, 1 => progrees, 2 => complete');
            $table->unsignedInteger('bill_quant_id')->comment('foreign key table bill_of_quantities');
            $table->text('description');
            $table->integer('project_id')->after('id')->default(0)->nullable(false);
            $table->integer('project_location_id')->after('id')->default(0)->nullable(false);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('three_phase_inspection');
    }
}
