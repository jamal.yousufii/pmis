<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'sections';

    /**
     * Run the migrations.
     * @table sections
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('module_id')->default('0');
            $table->string('name_en')->nullable()->default(null);
            $table->string('name_dr')->nullable()->default(null);
            $table->string('name_pa')->nullable()->default(null);
            $table->string('code');
            $table->string('url_route', 32)->nullable()->default(null);
            $table->integer('tab')->default('0')->comment('0 for main, 1 for tabs');
            $table->string('tab_code')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('orders', 20)->default('0');
            $table->string('tables', 32)->nullable()->default(null);
            $table->string('language', 32)->nullable()->default(null);
            $table->string('icon', 16)->default('');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
