<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'dashboard';

    /**
     * Run the migrations.
     * @table dashboard
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->text('introduction')->nullable()->default(null);
            $table->text('goals')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
