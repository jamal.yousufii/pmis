<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWatersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('waters', function (Blueprint $table) {
            $table->integer('is_adjustment')->default(0)->nullable(false)->comment('0 => Not adjustment, 1 => adjustment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('waters', function (Blueprint $table) {
            $table->dropColumn('is_adjustment');
        });
    }
}
