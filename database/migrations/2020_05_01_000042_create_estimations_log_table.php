<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimationsLogTable extends Migration
{
    /**
     * Run the migrations.
     * @table estimations
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('estimations_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->Integer('urn');
            $table->integer('project_id')->default('0');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->integer('cost')->default('0')->comment('Estimated Cost');
            $table->text('description')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->Integer('department')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
            $table->Integer('process')->comment('0 under process, 1 completed');
            $table->timestamp('logged_at',0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists('estimations_log');
     }
}
