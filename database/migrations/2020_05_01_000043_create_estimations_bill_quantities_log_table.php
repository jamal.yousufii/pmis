<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimationsBillQuantitiesLogTable extends Migration
{
    /**
     * Run the migrations.
     * @table estimations_bill_quantities
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('estimations_bill_quantities_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->Integer('rec_id');
            $table->integer('project_id')->default('0');
            $table->integer('project_location_id')->default('0');
            $table->text('operation_type');
            $table->integer('unit_id')->default('0');
            $table->integer('amount')->nullable()->default(null)->comment('changed from float');
            $table->integer('price')->default('0');
            $table->integer('total_price')->default('0');
            $table->string('percentage');
            $table->text('remarks')->nullable()->default(null);
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->date('actual_end_date')->nullable()->default(null);
            $table->date('actual_start_date')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
            $table->timestamp('logged_at',0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists('estimations_bill_quantities_log');
     }
}
