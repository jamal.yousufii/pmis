<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('survey_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('record_id');
            $table->integer('document_type')->nullable()->default(null);
            $table->string('filename')->nullable()->default(null);
            $table->string('path')->nullable()->default(null);
            $table->string('extension')->nullable()->default(null);
            $table->integer('size')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('survey_documents');
    }
}
