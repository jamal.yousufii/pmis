<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('contractors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('urn');
            $table->string('company_name');
            $table->string('primary_contact_name');
            $table->string('primary_contact_phone');
            $table->string('primary_contact_email');
            $table->string('alternative_contact_name')->nullable();
            $table->string('alternative_contact_phone')->nullable();
            $table->string('alternative_contact_email')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('contractors');
    }
}
