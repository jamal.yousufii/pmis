<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchitectureLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('architecture_logs', function (Blueprint $table) {
            $table->Integer('id')->nullable(false)->default(0);
            $table->Integer('urn')->nullable(false)->default(0);
            $table->integer('project_id')->nullable(false)->default(0);
            $table->integer('employee_id')->nullable(false)->default(0);
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->integer('created_by')->default(0);
            $table->integer('department_id')->default(0);
            $table->tinyInteger('status')->nullable()->default('0')->comment('0: not approved, 1: approved, 2: rejected');
            $table->string('version_id')->default(0)->commment('0 => has not version');
            $table->string('version')->default('first');
            $table->text('changed_comment')->nullable()->commment('Approved or Reject Comment');
            $table->integer('changed_status_by')->nullable()->default(null)->comment('id of employee whome approve or reject');
            $table->dateTime('changed_date')->nullable()->default(null)->comment('date of approve or reject');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('process')->default(0)->comment('0 under process, 1 completed');
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
			$table->timestamp('logged_at')->nullable()->default(NULL);
			$table->integer('logged_by')->nullable(false)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('architecture_logs');
    }
}
