<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStructureProjectLocaton extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::connection('pmis')->create('structure_project_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('record_id'); 
            $table->integer('project_location_id');
            $table->string('section')->nullable()->comment('define which record from which table');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('structure_project_locations');
    }
}
