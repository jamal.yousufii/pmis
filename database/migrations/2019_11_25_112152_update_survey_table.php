<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('surveys', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->Integer('location_id')->after('project_id');
            $table->Integer('department')->after('created_by');
            $table->Integer('process')->comment('0 under process, 1 completed')->after('wastewater');
            $table->dropColumn('approved_date');
            $table->dropColumn('approved_by');
            $table->dropColumn('school_no');
            $table->dropColumn('health_center_no');
            $table->dropColumn('council_no');
            $table->dropColumn('village_no');
            $table->dropColumn('beneficiaries_no');
            $table->dropColumn('length');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('floor');
            $table->dropColumn('diameter');
            $table->dropColumn('depth');
            $table->dropColumn('scheme_file');
            $table->dropColumn('soil_file');
            $table->dropColumn('topography_file');
            $table->dropColumn('line_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('surveys', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->dropColumn('location_id');
            $table->dropColumn('department');
            $table->dropColumn('process');
            $table->string('approved_date')->nullable()->default(null);
            $table->string('approved_by')->nullable()->default(null);
            $table->integer('school_no')->nullable()->default(null);
            $table->integer('health_center_no')->nullable()->default(null);
            $table->integer('council_no')->nullable()->default(null);
            $table->integer('village_no')->nullable()->default(null);
            $table->integer('beneficiaries_no')->nullable()->default(null);
            $table->string('length')->nullable()->default(null)->comment('changed from float');
            $table->string('width')->nullable()->default(null)->comment('changed from float');
            $table->string('height')->nullable()->default(null);
            $table->string('floor')->nullable()->default(null);
            $table->string('diameter')->nullable()->default(null);
            $table->string('depth')->nullable()->default(null);
            $table->string('scheme_file', 64)->nullable()->default(null);
            $table->string('soil_file')->nullable()->default(null);
            $table->string('topography_file')->nullable()->default(null);
            $table->string('line_file')->nullable()->default(null);
        });
    }
}
