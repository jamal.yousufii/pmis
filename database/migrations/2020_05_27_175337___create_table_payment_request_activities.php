<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentRequestActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('payment_request_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumInteger('payment_request_id');
            $table->mediumInteger('project_id');
            $table->mediumInteger('project_location_id');
            $table->bigInteger('bq_id');
            $table->bigInteger('request_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('payment_request_activities');
    }
}
