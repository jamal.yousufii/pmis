<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('urn');
            $table->integer('project_id');
            $table->integer('project_location_id');
            $table->date('report_date');
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_reports');
    }
}
