<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'estimate';

    /**
     * Run the migrations.
     * @table estimate
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_id')->default('0');
            $table->integer('number')->nullable()->default(null);
            $table->string('subject')->nullable()->default(null);
            $table->date('takeover_date')->nullable()->default(null);
            $table->date('return_date')->nullable()->default(null);
            $table->tinyInteger('invoice')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('board')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('supervisor_approval')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('estimate_accor_contract')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('overtime')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('company_seal_sign')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('discounting')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('license')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('bank_account')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('vendor_form')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('tadil_kamkari')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('estate_reg')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->text('other_topics')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
