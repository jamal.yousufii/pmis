<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('survey_measurements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('record_id');
            $table->integer('type')->nullable()->default(null);
            $table->integer('unit')->nullable()->default(null);
            $table->integer('quantity')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('survey_measurements');
    }
}
