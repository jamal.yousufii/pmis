<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImplementDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('implement_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parent_id');
            $table->integer('record_id');
            $table->string('section')->nullable()->default(null);
            $table->integer('type')->nullable()->default(null);
            $table->string('filename')->nullable()->default(null);
            $table->string('path')->nullable()->default(null);
            $table->string('extension')->nullable()->default(null);
            $table->integer('size')->nullable()->default(null);
            $table->timestamps();
            $table->integer('created_by')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('implement_attachments');
    }
}
