<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricityTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::connection('pmis')->create('electricity_teams', function (Blueprint $table) {
			$table->bigIncrements('id');
            $table->integer('electricity_id')->nullable(false)->default(0);
            $table->integer('employee_id')->nullable(false)->default(0);
        });
			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('electricity_teams');
    }
}
