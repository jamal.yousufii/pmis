<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValueSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('surveys', function (Blueprint $table) {
            $table->date('start_date')->nullable()->default(NULL)->change(); 
            $table->date('end_date')->nullable()->default(NULL)->change(); 
            $table->string('process',16)->default('0')->change();
            $table->string('has_survey')->default('yes'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('surveys', function (Blueprint $table) {
            $table->drongColumn('has_survey');
        });
    }
}
