<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'estimations';

    /**
     * Run the migrations.
     * @table estimations
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_id')->default('0');
            $table->integer('estimated_cost')->default('0');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->string('file_name')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->integer('architecture_cost')->nullable()->default('0');
            $table->integer('structure_cost')->nullable()->default('0');
            $table->integer('electricity_cost')->nullable()->default('0');
            $table->integer('water_cost')->nullable()->default('0');
            $table->integer('mechanic_cost')->nullable()->default('0');
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->tinyInteger('is_submited')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
