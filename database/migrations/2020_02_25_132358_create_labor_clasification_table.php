<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaborClasificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('labor_clasification', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en', 64)->nullable()->default(null);
            $table->string('name_dr', 128)->nullable()->default(null);
            $table->string('name_pa', 128)->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('labor_clasification');
    }
}
