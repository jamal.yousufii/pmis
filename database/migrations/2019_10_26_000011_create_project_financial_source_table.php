<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFinancialSourceTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'project_financial_source';

    /**
     * Run the migrations.
     * @table project_financial_source
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_en', 64)->nullable()->default(null);
            $table->string('name_dr', 128)->nullable()->default(null);
            $table->string('name_pa', 128)->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
