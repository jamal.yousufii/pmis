<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBillQuantityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->date('actual_end_date')->default(null)->after('end_date');
            $table->date('actual_start_date')->default(null)->after('end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->dropColumn('actual_start_date');
            $table->dropColumn('actual_end_date');
        });
    }
}
