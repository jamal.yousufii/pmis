<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeRequestTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('change_request_time', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_quantity_id')->nullable()->default(NULL);
            $table->integer('project_id');
            $table->integer('location_id');
            $table->tinyInteger('operation')->comment('0: not approved, 1: approved, 2: rejected')->default(0);
            $table->string('operation_description')->nullable()->default(null);
            $table->integer('operation_by')->nullable()->default('0');
            $table->dateTime('operation_at')->nullable()->default(null);
            $table->string('remarks');
            $table->date('start_date')->nullable()->default(NULL);
            $table->date('end_date')->nullable()->default(NULL);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_request_time');
    }
}
