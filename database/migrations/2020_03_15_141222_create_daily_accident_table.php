<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyAccidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_accidents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('daily_report_id');
            $table->date('accident_date')->nullable()->default(null);
            $table->smallInteger('personal_injury')->default(0)->comment('0 => Not Injured, 1 ==> Injured'); 
            $table->text('description')->nullable();
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_accidents');
    }
}
