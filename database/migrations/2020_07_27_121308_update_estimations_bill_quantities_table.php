<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEstimationsBillQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->integer('resources')->nullable(true)->default(0)->after('end_date');
            $table->integer('shift')->after('end_date')->default(0)->nullable(true);
            $table->integer('duration')->after('end_date')->default(0)->nullable(true);
            $table->integer('started')->nullable(false)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->dropColumn('shift');
            $table->dropColumn('duration');
        });
    }
}
