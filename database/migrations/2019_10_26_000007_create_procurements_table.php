<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'procurements';

    /**
     * Run the migrations.
     * @table procurements
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_id')->default('0');
            $table->tinyInteger('executive')->nullable()->default(null)->comment('1 for yes, 0 for no');
            $table->text('executive_desc')->nullable()->default(null);
            $table->tinyInteger('planning')->nullable()->default(null)->comment('1 for yes, 0 for no');
            $table->text('planning_desc')->nullable()->default(null);
            $table->string('tin')->nullable()->default(null);
            $table->tinyInteger('financial_letter')->nullable()->default(null);
            $table->text('financial_letter_desc')->nullable()->default(null);
            $table->tinyInteger('bidding')->nullable()->default(null);
            $table->date('bidding_time')->nullable()->default(null);
            $table->tinyInteger('bidding_approval')->nullable()->default(null);
            $table->text('bidding_approval_desc')->nullable()->default(null);
            $table->string('bidding_approval_time', 100)->nullable()->default(null);
            $table->tinyInteger('announcement')->nullable()->default(null);
            $table->text('announcement_desc')->nullable()->default(null);
            $table->string('announcement_time', 100)->nullable()->default(null);
            $table->tinyInteger('bids')->nullable()->default(null);
            $table->text('bids_desc')->nullable()->default(null);
            $table->string('bids_time', 100)->nullable()->default(null);
            $table->tinyInteger('estimate')->nullable()->default(null);
            $table->text('estimate_desc')->nullable()->default(null);
            $table->string('estimate_time', 100)->nullable()->default(null);
            $table->tinyInteger('public_announce')->nullable()->default(null);
            $table->text('public_announce_desc')->nullable()->default(null);
            $table->string('public_announce_time', 100)->nullable()->default(null);
            $table->tinyInteger('send')->nullable()->default(null);
            $table->text('send_desc')->nullable()->default(null);
            $table->string('send_time', 100)->nullable()->default(null);
            $table->tinyInteger('contract_arrangement')->nullable()->default(null);
            $table->text('contract_arrangement_desc')->nullable()->default(null);
            $table->string('contract_arrangement_time', 100)->nullable()->default(null);
            $table->tinyInteger('contract_approval')->nullable()->default(null);
            $table->text('contract_approval_desc')->nullable()->default(null);
            $table->string('contract_approval_time', 100)->nullable()->default(null);
            $table->string('company')->nullable()->default(null);
            $table->string('contract_code')->nullable()->default(null);
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->integer('estimated_price')->nullable()->default(null);
            $table->string('file_name', 64)->nullable()->default(null);
            $table->tinyInteger('start_construction')->nullable()->default('0')->comment('0 for not start, 1 for start');
            $table->string('send_letter_time', 100)->nullable()->default(null);
            $table->tinyInteger('controle')->nullable()->default(null);
            $table->text('controle_desc')->nullable()->default(null);
            $table->string('controle_time', 100)->nullable()->default(null);
            $table->tinyInteger('is_submited')->nullable()->default('0');
            $table->dateTime('submited_at')->nullable()->default(null);
            $table->integer('submited_by')->nullable()->default('0');
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
