<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'districts';

    /**
     * Run the migrations.
     * @table districts
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('province_id')->nullable()->default(null)->comment('province code');
            $table->string('name_en')->nullable()->default(null)->comment('district in english');
            $table->string('name_dr')->nullable()->default(null)->comment('district dari');
            $table->string('name_pa')->nullable()->default(null)->comment('district pashto');

            $table->index(["province_id"], 'province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
