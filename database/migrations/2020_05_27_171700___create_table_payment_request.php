<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('payment_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('urn');
            $table->string('request_number',255); 
            $table->date('request_date');
            $table->text('description');
            $table->float('total_request_amount');
            $table->smallInteger('total_request_activity');
            $table->integer('contractor_id');
            $table->integer('project_id');
            $table->enum('completed',[0,1])->comment('0 ==> !compelete, 1 ==> compelet');
            $table->string('status',32)->default(0);
            $table->smallInteger('created_by');
            $table->softDeletes();     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('payment_requests');
    }
}
