<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEstimationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('estimations', function (Blueprint $table) {
            $table->renameColumn('estimated_cost', 'cost')->comment('Estimated Cost')->after('end_date');
            $table->Integer('urn')->after('id');
            $table->Integer('process')->comment('0 under process, 1 completed')->after('deleted_at');
            $table->Integer('department')->after('created_by');
            $table->dropColumn('is_submited');
            $table->dropColumn('file_name');
            $table->dropColumn('architecture_cost');
            $table->dropColumn('structure_cost');
            $table->dropColumn('electricity_cost');
            $table->dropColumn('water_cost');
            $table->dropColumn('mechanic_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('estimations', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->dropColumn('department');
            $table->dropColumn('process');
            $table->renameColumn('cost', 'estimated_cost');
            $table->renameColumn('process', 'is_submited');
            $table->integer('file_name');
            $table->integer('architecture_cost');
            $table->integer('structure_cost');
            $table->integer('electricity_cost');
            $table->integer('water_cost');
            $table->integer('mechanic_cost');
            $table->integer('is_submited');
        });
    }
}
