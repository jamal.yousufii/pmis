<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrnToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('projects', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->Integer('department')->after('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('projects', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->dropColumn('department');
        });
    }
}
