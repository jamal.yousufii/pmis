<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurvyAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('survey_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable(); 
            $table->integer('record_id')->nullable(); 
            $table->string('section',64)->comment('where is attachment from'); 
            $table->string('filename')->nullable()->default(null);
            $table->string('path')->nullable()->default(null);
            $table->string('extension', 50)->nullable()->default(null);
            $table->string('size', 50)->nullable()->default(null);
            $table->integer('created_by')->nullable()->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('survey_attachments');
    }
}
