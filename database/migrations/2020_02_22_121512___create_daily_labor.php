<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyLabor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_labours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('daily_report_id');
            $table->mediumInteger('labor_clasification');
            $table->string('labor_number');
            $table->string('work_hours');
            $table->enum('is_worked_stoped',[0,1])->comment('0 => not stopped, 1 ==> stopped'); 
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_labours');
    }
}
