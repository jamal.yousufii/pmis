<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSectionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_section';

    /**
     * Run the migrations.
     * @table user_section
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('Id');
            $table->integer('user_id')->default('0');
            $table->integer('section_id')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
