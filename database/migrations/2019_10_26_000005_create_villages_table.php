<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVillagesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'villages';

    /**
     * Run the migrations.
     * @table villages
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name_en', 90)->nullable()->default(null);
            $table->string('name_dr', 90)->nullable()->default(null);
            $table->string('name_pa')->nullable()->default(null);
            $table->integer('province_id')->nullable()->default(null);
            $table->integer('district_id')->nullable()->default(null);
            $table->string('villagecode', 50)->nullable()->default(null);

            $table->index(["district_id"], 'districtcode');

            $table->index(["province_id"], 'provincecode');

            $table->index(["villagecode"], 'villagecode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
