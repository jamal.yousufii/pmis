<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Modifying column name in users table of pmis_auth database
        Schema::connection('pmis_auth')->table('users',function (Blueprint $table){
            $table->integer('contractor_id')->after('is_admin');
            $table->string('phone_number')->after('active');
            $table->renameColumn('is_engineer','contractor_staff');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis_auth')->table('users',function (Blueprint $table){
            $table->dropColumn('contractor_id');
            $table->dropColumn('phone_number');
            $table->renameColumn('contractor_staff','is_engineer');
        });
    }
}
