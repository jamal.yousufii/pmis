<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMechanicColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('mechanics', function (Blueprint $table) {
            $table->integer('process')->default(0)->after('updated_at')->comment('0 under process, 1 completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('mechanics', function (Blueprint $table) {
            $table->dropColumn('process');
        });
    }
}
