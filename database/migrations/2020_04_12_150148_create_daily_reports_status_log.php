<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsStatusLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_reports_status_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('report_id')->default(0)->nullable(false);
            $table->integer('status')->nullable()->comment('0 => Not Approved, 1 => Approved,2 => Rejected');
            $table->integer('status_by')->nullable();
            $table->date('status_date')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_reports_status_log');
    }
}
