<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterProjectLocationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('water_project_location_logs', function (Blueprint $table) {
            $table->integer('id')->nullable(false)->default(0);
            $table->integer('record_id')->nullable(false)->default(0);
            $table->integer('project_id')->nullable(false)->default(0); 
            $table->integer('project_location_id')->nullable(false)->default(0);
            $table->string('section')->nullable()->comment('define which record from which table');
            $table->timestamps();
			$table->timestamp('logged_at')->nullable()->default(NULL);
			$table->integer('logged_by')->nullable(false)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('water_project_location_logs');
    }
}
