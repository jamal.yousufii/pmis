<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('calendar_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id')->default('0');
            $table->date('from_date')->nullable()->default(null)->comment('Holiday start from');
            $table->date('to_date')->nullable()->default(null)->comment('Holiday end');
            $table->string('description')->nullable()->default(null);
            $table->integer('created_by')->nullable()->default(null);
			$table->Integer('department_id')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('calendar_holidays');
    }
}
