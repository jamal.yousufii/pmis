<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplyChangesToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('requests', function (Blueprint $table) {
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
            $table->renameColumn('approval_date','operation_date');
            $table->renameColumn('approval_number','operation_number');
            $table->renameColumn('approval_description','operation_description');
            $table->renameColumn('is_approved','operation');
            $table->renameColumn('approved_by','operation_by');
            $table->renameColumn('approved_at','operation_at');
            $table->dropColumn('rejected_by');
            $table->dropColumn('rejected_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('requests', function (Blueprint $table) {
            $table->dropColumn('completed');
            $table->renameColumn('operation_date','approval_date');
            $table->renameColumn('operation_number','approval_number');
            $table->renameColumn('operation_description','approval_description');
            $table->renameColumn('operation','is_approved');
            $table->renameColumn('operation_by','approved_by');
            $table->renameColumn('operation_at','approved_at');
            $table->Integer('rejected_by');
            $table->dateTime('rejected_at');
        });
    }
}
