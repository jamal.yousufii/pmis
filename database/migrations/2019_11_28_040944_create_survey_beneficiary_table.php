<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyBeneficiaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('survey_beneficiaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('record_id');
            $table->integer('users_type')->nullable()->default(null);
            $table->integer('total')->nullable()->default(null);
            $table->string('comments')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('survey_beneficiaries');
    }
}
