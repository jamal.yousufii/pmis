<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('structures', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('structures', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->dropColumn('completed');
        });
    }
}
