<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStructureTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::connection('pmis')->create('structure_teams', function (Blueprint $table) {
			$table->bigIncrements('id');
            $table->integer('structure_id')->nullable(false)->default(0);
            $table->integer('employee_id')->nullable(false)->default(0);
        });
			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('structure_teams');
    }
}
