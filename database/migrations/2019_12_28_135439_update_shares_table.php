<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('shares', function (Blueprint $table) {
            $table->Integer('share_from')->after('record_id')->nullable()->default(null);
            $table->renameColumn('sections','share_from_code')->after('share_from');
            $table->string('share_to_code')->after('share_to');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('shares', function (Blueprint $table) {
            $table->dropColumn('share_from');
            $table->dropColumn('share_to_code');
            $table->renameColumn('share_from_code','sections');
        });
    }
}
