<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('project_locations', function (Blueprint $table) {
            $table->dropColumn('location');
            $table->float('latitude')->after('village_id');
            $table->float('longitude')->after('latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('project_locations', function (Blueprint $table) {
            $table->integer('location');
            $table->dropColumn('latitude'); 
            $table->dropColumn('longitude'); 
        });
    }
}
