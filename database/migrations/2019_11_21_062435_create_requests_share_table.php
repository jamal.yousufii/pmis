<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsShareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('request_shares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('share_to');
            $table->unsignedInteger('record_id');
            $table->string('sections')->comment('identify record is from which table');  
            $table->unsignedInteger('user_id'); 
            $table->unsignedInteger('department_id'); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('request_shares');
    }
}
