<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('father');
            $table->string('email');
            $table->string('position');
            $table->integer('department_id')->default('0');
            $table->string('username')->nullable()->default(null);
            $table->string('password');
            $table->integer('is_admin')->default('0');
            $table->integer('is_engineer')->default('0')->comment('0 = NO, 1 = YES');
            $table->string('profile_pic')->nullable()->default(null);
            $table->integer('active')->default('1')->comment('0 = NO, 1 = YES');
            $table->rememberToken();
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
