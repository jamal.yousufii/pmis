<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBillOfQuantitiesLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('resources')->nullable(true)->default(0)->after('end_date');
            $table->integer('shift')->nullable(true)->default(0)->after('end_date');
            $table->integer('duration')->nullable(true)->default(0)->after('end_date');
            $table->date('logged_by')->nullable()->default(null)->after('logged_at');
            $table->integer('started')->nullable(false)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
