<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModificationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'modifications';

    /**
     * Run the migrations.
     * @table modifications
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_id')->default('0');
            $table->integer('number')->nullable()->default(null);
            $table->string('subject')->nullable()->default(null);
            $table->date('takeover_date')->nullable()->default(null);
            $table->date('return_date')->nullable()->default(null);
            $table->tinyInteger('company_request')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('mod_proposal')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('technical_committee')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->tinyInteger('committee_comment')->nullable()->default(null)->comment('1: exist, 2: not exist');
            $table->text('other_topics')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
