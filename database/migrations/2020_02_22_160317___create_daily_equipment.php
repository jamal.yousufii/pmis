<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyEquipment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_equipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('daily_report_id');
            $table->mediumInteger('equipment_type');
            $table->mediumInteger('equipment_no');
            $table->string('work_hourse');
            $table->enum('is_worked_stoped',[0,1])->comment('0 => not stopped, 1 ==> stopped'); 
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_equipments');
    }
}
