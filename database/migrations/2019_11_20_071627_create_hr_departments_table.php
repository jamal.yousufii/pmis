<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create('hr_departments', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->string('name_dr');
            $table->string('name_en');
            $table->string('name_pa');
            $table->string('abbreviation');
            $table->string('location');
            $table->integer('year');
            $table->integer('parent');
            $table->integer('user_id');
            $table->integer('unactive');
            $table->integer('advisor');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis_auth')->dropIfExists('hr_departments'); 
    }
}
