<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyEntityValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('survey_entity_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_id');
            $table->integer('survey_id');
            $table->string('type')->nullable()->default(null);
            $table->string('number')->nullable()->default(null);
            $table->string('thickness')->nullable()->default(null);
            $table->string('strenght')->nullable()->default(null);
            $table->string('destroy_component')->nullable()->default(null);
            $table->string('height')->nullable()->default(null);
            $table->string('width')->nullable()->default(null);
            $table->string('length')->nullable()->default(null);
            $table->string('stair')->nullable()->default(null);
            $table->string('engine_specification')->nullable()->default(null);
            $table->string('shape')->nullable()->default(null);
            $table->string('perseverance')->nullable()->default(null);
            $table->string('material')->nullable()->default(null);
            $table->string('moisture_status')->nullable()->default(null);
            $table->string('water_well')->nullable()->default(null);
            $table->string('plumbery')->nullable()->default(null);
            $table->string('pool')->nullable()->default(null);
            $table->string('quality')->nullable()->default(null);
            $table->string('sewage')->nullable()->default(null);
            $table->integer('created_by');
            $table->integer('department_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('survey_entity_values');
    }
}
