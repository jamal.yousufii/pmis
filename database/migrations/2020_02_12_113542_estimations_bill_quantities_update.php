<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EstimationsBillQuantitiesUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->string('percentage')->after('total_price');
            $table->date('start_date')->default(null)->after('remarks');
            $table->date('end_date')->default(null)->after('remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('estimations_bill_quantities', function (Blueprint $table) {
            $table->dropColumn('percentage');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });
    }
}
