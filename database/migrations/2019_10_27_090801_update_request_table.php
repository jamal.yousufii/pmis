<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('pmis')->table('requests', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->dropColumn(['department','is_process']);
            $table->renameColumn('is_approved', 'is_approved')->after('deleted_at');
            $table->renameColumn('hokm_date', 'approval_date')->after('is_approved');
            $table->renameColumn('hokm_number', 'approval_number')->after('hokm_date');
            $table->longText('approval_description')->nullable()->after('hokm_number');  
            $table->renameColumn('approved_by', 'approved_by')->after('approval_description');
            $table->renameColumn('approved_at', 'approved_at')->after('approved_by');
            $table->renameColumn('rejected_by', 'rejected_by')->after('approved_at');
            $table->renameColumn('rejected_at', 'rejected_at')->after('rejected_by');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('requests', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->string('department');
            $table->smallInteger('is_process');
            $table->renameColumn('is_approved', 'is_approved');
            $table->renameColumn('approval_date','hokm_date');
            $table->renameColumn('approval_number','hokm_number');
            $table->renameColumn('approved_by','approved_by');
            $table->renameColumn('approved_at','approved_at');
            $table->renameColumn('rejected_by','rejected_by');
            $table->renameColumn('rejected_at','rejected_at');
            $table->dropColumn('approval_description');
        });
    }
}
