<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DailyActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('daily_report_id');
            $table->integer('bq_id');
            $table->integer('unit_id');
            $table->integer('amount'); 
            $table->string('location')->nullable(); 
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_activities');
    }
}
