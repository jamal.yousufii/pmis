<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHazardInspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('hazard_analysis_sub', function (Blueprint $table) {
            $table->integer('project_location_id')->after('project_id')->default(0)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('hazard_analysis_sub', function (Blueprint $table) {
            $table->dropColumn('project_location_id');
        });
    }
}
