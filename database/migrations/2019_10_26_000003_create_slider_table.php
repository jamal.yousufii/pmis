<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'slider';

    /**
     * Run the migrations.
     * @table slider
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis_auth')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('file_name')->nullable()->default(null);
            $table->integer('order')->nullable()->default('0')->comment('0:active, 1: not active');
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis_auth')->dropIfExists($this->tableName);
     }
}
