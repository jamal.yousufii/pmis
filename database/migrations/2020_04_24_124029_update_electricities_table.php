<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateElectricitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('electricities', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
            $table->integer('is_adjustment')->electricitiesdefault(0)->nullable(false)->comment('0 => Not adjustment, 1 => adjustment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('electricities', function (Blueprint $table) {
            $table->dropColumn(['urn','completed','is_adjustment']);
        });
    }
}
