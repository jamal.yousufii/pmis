<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'surveys';

    /**
     * Run the migrations.
     * @table surveys
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_id')->default('0');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('length')->nullable()->default(null)->comment('changed from float');
            $table->string('width')->nullable()->default(null)->comment('changed from float');
            $table->string('height')->nullable()->default(null);
            $table->string('floor')->nullable()->default(null);
            $table->string('diameter')->nullable()->default(null);
            $table->string('depth')->nullable()->default(null);
            $table->integer('team_head')->default('0');
            $table->date('approved_date')->nullable()->default(null);
            $table->string('approved_by')->nullable()->default(null);
            $table->integer('school_no')->nullable()->default('0');
            $table->integer('health_center_no')->nullable()->default('0');
            $table->integer('council_no')->nullable()->default('0');
            $table->integer('village_no')->nullable()->default('0');
            $table->integer('beneficiaries_no')->nullable()->default('0');
            $table->text('description')->nullable()->default(null);
            $table->string('scheme_file', 64)->nullable()->default(null);
            $table->string('soil_file')->nullable()->default(null);
            $table->string('topography_file')->nullable()->default(null);
            $table->string('line_file')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->string('roof_type', 50)->nullable()->default('0');
            $table->integer('roof_number')->nullable()->default(null);
            $table->string('roof_width')->nullable()->default(null);
            $table->string('roof_resistance')->nullable()->default(null);
            $table->string('roof_ruined')->nullable()->default(null);
            $table->tinyInteger('fear_type')->nullable()->default(null);
            $table->integer('fear_number')->nullable()->default(null);
            $table->string('fear_resistance')->nullable()->default(null);
            $table->string('fear_length')->nullable()->default(null);
            $table->string('fear_width')->nullable()->default(null);
            $table->tinyInteger('foundation_type')->nullable()->default(null);
            $table->integer('foundation_number')->nullable()->default(null);
            $table->string('foundation_resistance')->nullable()->default(null);
            $table->string('foundation_length')->nullable()->default(null);
            $table->string('foundation_width')->nullable()->default(null);
            $table->integer('floor_number')->nullable()->default(null);
            $table->string('floor_hight')->nullable()->default(null);
            $table->string('floor_length')->nullable()->default(null);
            $table->string('floor_width')->nullable()->default(null);
            $table->integer('room_number')->nullable()->default(null);
            $table->string('room_hight')->nullable()->default(null);
            $table->string('room_length')->nullable()->default(null);
            $table->string('room_width')->nullable()->default(null);
            $table->integer('wc_number')->nullable()->default(null);
            $table->string('wc_hight')->nullable()->default(null);
            $table->string('wc_length')->nullable()->default(null);
            $table->string('wc_width')->nullable()->default(null);
            $table->integer('corridor_number')->nullable()->default(null);
            $table->string('corridor_hight')->nullable()->default(null);
            $table->string('corridor_length')->nullable()->default(null);
            $table->string('corridor_width')->nullable()->default(null);
            $table->integer('balcony_number')->nullable()->default(null);
            $table->string('balcony_length')->nullable()->default(null);
            $table->string('balcony_width')->nullable()->default(null);
            $table->integer('kitchen_number')->nullable()->default(null);
            $table->string('kitchen_length')->nullable()->default(null);
            $table->string('kitchen_width')->nullable()->default(null);
            $table->tinyInteger('stair_type')->nullable()->default(null);
            $table->integer('stair_number')->nullable()->default(null);
            $table->string('stair_length')->nullable()->default(null);
            $table->string('stair_width')->nullable()->default(null);
            $table->string('stair_floor')->nullable()->default(null);
            $table->integer('lift_number')->nullable()->default(null);
            $table->string('lift_length')->nullable()->default(null);
            $table->string('lift_width')->nullable()->default(null);
            $table->string('lift_engine')->nullable()->default(null);
            $table->tinyInteger('wall_type')->nullable()->default(null);
            $table->tinyInteger('wall_shap')->nullable()->default(null);
            $table->string('wall_width')->nullable()->default(null);
            $table->tinyInteger('column_type')->nullable()->default(null);
            $table->tinyInteger('column_shap')->nullable()->default(null);
            $table->string('column_lenght')->nullable()->default(null);
            $table->string('column_width')->nullable()->default(null);
            $table->string('column_number')->nullable()->default(null);
            $table->string('ston_width')->nullable()->default(null);
            $table->string('ston_quality')->nullable()->default(null);
            $table->string('ston_resistance')->nullable()->default(null);
            $table->string('ston_durability')->nullable()->default(null);
            $table->string('stone_component')->nullable()->default(null);
            $table->string('stone_component_resistance')->nullable()->default(null);
            $table->tinyInteger('brick_type')->nullable()->default(null);
            $table->string('brick_wet')->nullable()->default(null);
            $table->string('brick_quality')->nullable()->default(null);
            $table->string('brick_resistance')->nullable()->default(null);
            $table->text('well')->nullable()->default(null);
            $table->text('piping')->nullable()->default(null);
            $table->text('pool')->nullable()->default(null);
            $table->text('wastewater')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
