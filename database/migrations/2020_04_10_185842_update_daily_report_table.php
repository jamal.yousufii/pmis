<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDailyReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('daily_reports', function (Blueprint $table) {
            $table->text('description')->after('report_date')->nullable();
            $table->date('status_date')->after('report_date')->default(null)->nullable();
            $table->integer('status_by')->after('report_date')->default(null)->nullable();
            $table->integer('status')->after('report_date')->default(null)->nullable()->comment('0 => Not Approved, 1 => Approved,2 => Rejected');
            $table->integer('completed')->after('report_date')->default(0)->nullable(false)->comment('0 => Not Complete, 1 => Completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('daily_reports', function (Blueprint $table) {
            $table->dropColumn('status_date');
            $table->dropColumn('description');
            $table->dropColumn('status_by');
            $table->dropColumn('status');
            $table->dropColumn('completed');
        });
    }
}
