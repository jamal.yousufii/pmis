<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImplementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('implements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('urn');
            $table->integer('project_id')->default('0');
            $table->text('description')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->integer('created_by')->default('0');
            $table->Integer('department')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->Integer('process')->comment('0 under process, 1 completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('implements');
    }
}
