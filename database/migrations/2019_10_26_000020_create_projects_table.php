<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'projects';

    /**
     * Run the migrations.
     * @table projects
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id')->default('0');
            $table->integer('urn')->default('0');
            $table->integer('status')->nullable()->default('0');
            $table->string('name')->nullable()->default(null);
            $table->string('code', 128)->nullable()->default(null);
            $table->integer('year')->nullable()->default(null);
            $table->date('project_start_date')->nullable()->default(null);
            $table->date('project_end_date')->nullable()->default(null);
            $table->integer('category_id')->default('0');
            $table->integer('project_type_id')->default('0');
            $table->text('goal')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->integer('progress')->nullable()->default('0')->comment('0 construction, 1 completed, 2 stoped, 3 terminated');
            $table->dateTime('progress_at')->nullable()->default(null);
            $table->integer('progress_by')->nullable()->default(null);
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
