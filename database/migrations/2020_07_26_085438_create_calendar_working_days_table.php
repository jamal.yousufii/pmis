<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarWorkingDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('calendar_working_days', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id')->default('0');
            $table->tinyInteger('saturday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('sunday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('monday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('tuesday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('wednesday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('thursday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->tinyInteger('friday')->nullable()->default(0)->comment('8 for full day, 4 for half day, 0 for weekend');
            $table->integer('created_by')->nullable()->default(null);
			$table->Integer('department_id')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('calendar_working_days');
    }
}
