<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyTest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create('daily_test', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_quant_id');
            $table->integer('daily_report_id');
            $table->string('test_number'); 
            $table->string('test_type'); 
            $table->string('test_location')->nullable(); 
            $table->enum('is_completed',['completed','waiting']);
            $table->enum('test_status',['passed','failed','waiting']);
            $table->text('description')->nullable();
            $table->enum('is_worked_stoped',[0,1])->comment('0 => not stopped, 1 ==> stopped'); 
            $table->mediumInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->dropIfExists('daily_test');
    }
}
