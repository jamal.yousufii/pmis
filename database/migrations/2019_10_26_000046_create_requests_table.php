<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'requests';

    /**
     * Run the migrations.
     * @table requests
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('request_date')->nullable()->default(null);
            $table->integer('department_id')->default('0');
            $table->integer('doc_type')->default('0');
            $table->integer('doc_number')->nullable()->default(null);
            $table->date('hokm_date')->nullable()->default(null);
            $table->integer('hokm_number')->nullable()->default(null);
            $table->text('goals')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('file_name', 64)->nullable()->default(null);
            $table->tinyInteger('is_approved')->default('0')->comment('0: not approved, 1: approved, 2: rejected');
            $table->integer('approved_by')->nullable()->default(null);
            $table->dateTime('approved_at')->nullable()->default(null);
            $table->integer('rejected_by')->nullable()->default(null);
            $table->dateTime('rejected_at')->nullable()->default(null);
            $table->integer('created_by');
            $table->integer('department')->nullable()->default(null);
            $table->integer('updated_by')->default('0');
            $table->tinyInteger('is_process')->default('0')->comment('0: No, 1: Yes');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::connection('pmis')->dropIfExists($this->tableName);
     }
}
