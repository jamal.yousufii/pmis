<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrnToProcurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pmis')->table('procurements', function (Blueprint $table) {
            $table->Integer('urn')->after('id');
            $table->date('executive_date')->nullable()->default(null)->after('executive');
            $table->Integer('department')->after('created_by');
            $table->Integer('completed')->default('0')->comment('0 for incompleted, 1 for completed');
            $table->dropColumn('is_submited');
            $table->dropColumn('submited_at');
            $table->dropColumn('submited_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pmis')->table('procurements', function (Blueprint $table) {
            $table->dropColumn('urn');
            $table->dropColumn('executive_date');
            $table->dropColumn('department');
            $table->dropColumn('completed');
            $table->Integer('is_submited');
            $table->Integer('submited_at');
            $table->Integer('submited_by');
        });
    }
}
