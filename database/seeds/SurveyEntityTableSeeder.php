<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SurveyEntityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Entities = [
            [
                'name_dr' => 'سقف / سلب',
                'name_pa' => 'سقف / سلب',
                'name_en' => 'Ceiling',
                'code' => 'sa',
            ],
            [
                'name_dr' => 'ترس',
                'name_pa' => 'ترس',
                'name_en' => 'Trace',
                'code' => 'sb',
            ],
            [
                'name_dr' => 'تهداب',
                'name_pa' => 'تهداب',
                'name_en' => 'Foundation',
                'code' => 'sc',
            ],
            [
                'name_dr' => 'تعداد منازل',
                'name_pa' => 'تعداد منازل',
                'name_en' => 'Number of Floors',
                'code' => 'sd',
            ],
            [
                'name_dr' => 'اطاق ها',
                'name_pa' => 'اطاق ها',
                'name_en' => 'Rooms',
                'code' => 'se',
            ],
            [
                'name_dr' => 'تشناب ها',
                'name_pa' => 'تشناب ها',
                'name_en' => 'WashRooms',
                'code' => 'sf',
            ],
            [
                'name_dr' => 'دهلیزها',
                'name_pa' => 'دهلیزها',
                'name_en' => 'Hals',
                'code' => 'sg',
            ],
            [
                'name_dr' => 'بالکن ها',
                'name_pa' => 'بالکن ها',
                'name_en' => 'Balconies',
                'code' => 'sh',
            ],
            [
                'name_dr' => 'آشپزخانه',
                'name_pa' => 'آشپزخانه',
                'name_en' => 'Kitchen',
                'code' => 'si',
            ],
            [
                'name_dr' => 'زینه ها',
                'name_pa' => 'زینه ها',
                'name_en' => 'Stairs',
                'code' => 'sj',
            ],
            [
                'name_dr' => 'لفت',
                'name_pa' => 'لفت',
                'name_en' => 'Elavator',
                'code' => 'sk',
            ],
            [
                'name_dr' => 'دیوارها',
                'name_pa' => 'دیوارها',
                'name_en' => 'Walls',
                'code' => 'sl',
            ],
            [
                'name_dr' => 'پایه ها',
                'name_pa' => 'پایه ها',
                'name_en' => 'Pillars',
                'code' => 'sm',
            ],
            [
                'name_dr' => 'عناصر سنگی',
                'name_pa' => 'عناصر سنگی',
                'name_en' => 'Stony Elements',
                'code' => 'sn',
            ],
            [
                'name_dr' => 'مصالح سنگی',
                'name_pa' => 'مصالح سنگی',
                'name_en' => 'Stony Cement',
                'code' => 'so',
            ],
            [
                'name_dr' => 'خشت',
                'name_pa' => 'خشت',
                'name_en' => 'Bricks',
                'code' => 'sp',
            ]
        ];
        foreach ($Entities as $entity) {
            DB::connection('pmis')->table('survey_entities')->insert($entity);
        }
        
    }
}
