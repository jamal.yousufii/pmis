<?php

use Illuminate\Database\Seeder;
use App\models\Authentication\Modules; 

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
              [
                    'id' => '1',
                    'code' => 'auth',
                    'name_en' => 'Authentication',
                    'name_dr' => 'مدیریت سیستم',
                    'name_pa' => 'مدیریت سیستم',
                    'url' => 'home',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'deleted_at' => NULL
              ],

              [
                  'id' => '2',
                  'code' => 'pmis',
                  'name_en' => 'PMIS',
                  'name_dr' => 'مدیریت پروژها',
                  'name_pa' => 'مدیریت پروژها',
                  'url' => 'home',
                  'description' => NULL,
                  'created_at' => NULL,
                  'updated_at' => NULL,
                  'deleted_at' => NULL
              ], 

              [
                'id' => '3',
                'code' => 'setting',
                'name_en' => 'Settings',
                'name_dr' => 'تنظیمات',
                'name_pa' => 'تنظیمات',
                'url' => 'home',
                'description' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULl
              ],
          ];
        // Truncate table first 
        Modules::truncate();   
        foreach ($modules as $module) {
            Modules::create($module);
        }
    }
}
