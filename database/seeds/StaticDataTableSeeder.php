<?php

use Illuminate\Database\Seeder;
use App\models\Static_data; 

class StaticDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $static_data = array(
        array('id' => '1','code' => '1','name_dr' => 'عادی','name_pa' => 'عادی','name_en' => 'Normal','type' => '1','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '2','code' => '2','name_dr' => 'انتقالی','name_pa' => 'انتقالی','name_en' => 'Delivery','type' => '1','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '3','code' => '3','name_dr' => 'عاجل','name_pa' => 'عاجل','name_en' => 'Urgent','type' => '1','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '4','code' => '1','name_dr' => 'ملی متر','name_pa' => 'ملی متر','name_en' => 'millimeter','type' => '2','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '5','code' => '2','name_dr' => 'سانتی متر','name_pa' => 'سانتی متر','name_en' => 'centimeter','type' => '2','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '8','code' => '3','name_dr' => 'متر','name_pa' => 'meter','name_en' => 'meter','type' => '2','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '10','code' => '4','name_dr' => 'کیلومتر','name_pa' => 'kilometer','name_en' => 'kilometer','type' => '2','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '12','code' => '1','name_dr' => 'طول','name_pa' => 'lenght','name_en' => 'lenght','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '13','code' => '2','name_dr' => 'عرض','name_pa' => 'Width','name_en' => 'Width','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '14','code' => '3','name_dr' => 'مساحت','name_pa' => 'Area','name_en' => 'Area','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '15','code' => '4','name_dr' => 'عمق','name_pa' => 'Depth','name_en' => 'Depth','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '16','code' => '5','name_dr' => 'سقف','name_pa' => 'Ceiling','name_en' => 'Ceiling','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '17','code' => '6','name_dr' => 'قطر','name_pa' => 'Diameter','name_en' => 'Diameter','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '18','code' => '7','name_dr' => 'ارتفاع','name_pa' => 'Height','name_en' => 'Height','type' => '3','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '19','code' => '1','name_dr' => 'تست خاک','name_pa' => 'sand test','name_en' => 'sand test','type' => '4','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '20','code' => '2','name_dr' => 'نقشه برداری','name_pa' => 'Mapping','name_en' => 'Mapping','type' => '4','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '21','code' => '3','name_dr' => 'خط اندازی','name_pa' => 'Lining','name_en' => 'Lining','type' => '4','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '22','code' => '1','name_dr' => 'تعداد مکاتب','name_pa' => 'Schools Number','name_en' => 'Schools Number','type' => '5','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '23','code' => '2','name_dr' => 'تعداد مراکز صحی','name_pa' => 'Medicle Centers Number','name_en' => 'Medicle Centers Number','type' => '5','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '24','code' => '3','name_dr' => 'تعداد شورا ها','name_pa' => 'Council Number','name_en' => 'Council Number','type' => '5','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '25','code' => '4','name_dr' => 'تعداد قریه جات','name_pa' => 'Villages Number','name_en' => 'Villages Number','type' => '5','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '26','code' => '5','name_dr' => 'تعداد استفاده کننده گان','name_pa' => 'Number of Beneficiary','name_en' => 'Number of Beneficiary','type' => '5','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '29','code' => '1','name_dr' => 'چک لیست','name_pa' => 'چک لیست','name_en' => 'چک لیست','type' => '6','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '30','code' => '2','name_dr' => 'لیست پرسونل','name_pa' => 'لیست پرسونل','name_en' => 'لیست پرسونل','type' => '6','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '31','code' => '3','name_dr' => 'لیست تجهیزات','name_pa' => 'لیست تجهیزات','name_en' => 'لیست تجهیزات','type' => '6','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '32','code' => '1','name_dr' => 'در حالت اجرا','name_pa' => 'در حالت اجرا','name_en' => 'Ongoing','type' => '7','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '33','code' => '2','name_dr' => 'متوقف','name_pa' => 'متوقف','name_en' => 'Stop','type' => '7','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '34','code' => '3','name_dr' => 'تکمیل','name_pa' => 'تکمیل','name_en' => 'Complete','type' => '7','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '36','code' => '4','name_dr' => 'آب و هوا باعث توقف کاری نشد','name_pa' => 'آب و هوا باعث توقف کاری نشد','name_en' => 'آب و هوا باعث توقف کاری نشد','type' => '8','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '39','code' => '1','name_dr' => 'آب و هوا باعث توقف جدی کاری شد','name_pa' => 'آب و هوا باعث توقف جدی کاری شد','name_en' => 'آب و هوا باعث توقف جدی کاری شد','type' => '8','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '40','code' => '2','name_dr' => 'آب و هوا باعث توقف جدی کاری نشد','name_pa' => 'آب و هوا باعث توقف جدی کاری نشد','name_en' => 'آب و هوا باعث توقف جدی کاری نشد','type' => '8','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '41','code' => '3','name_dr' => 'آب و هوا باعت توقف کاری شد','name_pa' => 'آب و هوا باعت توقف کاری شد','name_en' => 'آب و هوا باعت توقف کاری شد','type' => '8','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
        array('id' => '42','code' => '5','name_dr' => 'روز کاری نبود','name_pa' => 'روز کاری نبود','name_en' => 'روز کاری نبود','type' => '8','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
      );
      

      // Truncate table first 
      Static_data::truncate();  
      foreach($static_data as $data)
      {
        Static_data::create($data); 
      }
    }
}
