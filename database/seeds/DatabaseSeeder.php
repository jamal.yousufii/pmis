<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ModulesTableSeeder::class, 
            DepartmentTableSedeer::class, 
            sectionTableSeeder::class, 
            RoleTableSeeder::class, 
            StaticDataTableSeeder::class,
            SurveyEntityTableSeeder::class,
        ]);
    }
}
