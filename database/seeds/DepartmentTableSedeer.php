<?php

use Illuminate\Database\Seeder;
use App\models\Authentication\Departments; 

class DepartmentTableSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
                [
                    'id' => '1'
                    ,'organization_id' => '1'
                    ,'name_en' => 'معاونیت تخنیکی اداره عملیاتی'
                    ,'name_dr' => 'معاونیت تخنیکی اداره عملیاتی'
                    ,'name_pa' => 'معاونیت تخنیکی اداره عملیاتی'
                    ,'description' => 'معاونیت تخنیکی اداره عملیاتی'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => NULL,'created_by' => '0'
                    ,'updated_at' => '2019-11-04 06:35:18'
                ],
                [
                    'id' => '2'
                    ,'organization_id' => '1'
                    ,'name_en' => 'ریاست انسجام'
                    ,'name_dr' => 'ریاست انسجام'
                    ,'name_pa' => 'ریاست انسجام'
                    ,'description' => 'ریاست انسجام'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => NULL,'created_by' => '0'
                    ,'updated_at' => '2019-11-04 06:35:25'
                ],
                [
                    'id' => '3'
                    ,'organization_id' => '1'
                    ,'name_en' => 'ریاست حفظ و مراقبت'
                    ,'name_dr' => 'ریاست حفظ و مراقبت'
                    ,'name_pa' => 'ریاست حفظ و مراقبت'
                    ,'description' => 'ریاست حفظ و مراقبت'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => '2019-06-25 05:54:06'
                    ,'created_by' => '2'
                    ,'updated_at' => '2019-11-04 06:35:32'
                ],
                [
                    'id' => '4'
                    ,'organization_id' => '1'
                    ,'name_en' => 'ریاست طرح و دیزاین'
                    ,'name_dr' => 'ریاست طرح و دیزاین'
                    ,'name_pa' => 'ریاست طرح و دیزاین'
                    ,'description' => 'ریاست طرح و دیزاین'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => '2019-11-03 06:25:59'
                    ,'created_by' => '0'
                    ,'updated_at' => '2019-11-04 06:35:39'],
                [
                    'id' => '5'
                    ,'organization_id' => '1'
                    ,'name_en' => 'ریاست تدارکات'
                    ,'name_dr' => 'ریاست تدارکات'
                    ,'name_pa' => 'ریاست تدارکات'
                    ,'description' => 'ریاست تدارکات'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => '2019-11-03 06:26:34'
                    ,'created_by' => '0'
                    ,'updated_at' => '2019-11-04 06:35:51'],
                [
                    'id' => '6'
                    ,'organization_id' => '1'
                    ,'name_en' => 'ریاست بنایی'
                    ,'name_dr' => 'ریاست بنایی'
                    ,'name_pa' => 'ریاست بنایی'
                    ,'description' => 'ریاست بنایی'
                    ,'active' => '0'
                    ,'deleted_at' => NULL,'created_at' => '2019-11-03 06:27:31'
                    ,'created_by' => '0'
                    ,'updated_at' => '2019-11-04 06:36:01'
                ]
        ];


        // Truncate table first 
        Departments::truncate();   
        foreach ($departments as $department) {
            Departments::create($department);
        } 
    }
}
