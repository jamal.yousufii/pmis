<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
     $permissions = [
        [
            'name'        => 'Req-list',
            'name_dr'     => 'لیست درخواست ها',
            'guard_name'  => 'web'
        ],
        [
            'name'        => 'Req-add',
            'name_dr'     => 'ثبت درخواست ها',
            'guard_name'  => 'web'
        ],
        [
            'name'        => 'Req-view',
            'name_dr'     => 'نمایش درخواست ها',
            'guard_name'  => 'web'
        ],
        [
            'name'        => 'Req-edit',
            'name_dr'     => 'تجدید درخواست ها',
            'guard_name'  => 'web'
        ],
        [
            'name'        => 'Req-delete',
            'name_dr'     => 'حذف درخواست ها',
            'guard_name'  => 'web'
        ],
        [
            'name'        => 'Req-approve',
            'name_dr'     => 'تائید درخواست ها',
            'guard_name'  => 'web'
        ],

      ];

      foreach ($permissions as $permission) {
          Permission::create($permission);
      }

  }
}
