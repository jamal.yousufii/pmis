<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        @yield('head')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <!--end::Web font -->
        <?php
        $lang = get_language();
        if($lang=="en"){
        $dir = "";
        }else {
        $dir = ".rtl";
        }
        ?>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/vendors/base/vendors.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/assets/vendors/custom/jstree/dist/themes/default/style.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{!!asset('public/assets/font/font.css')!!}" rel="stylesheet" type="text/css" />
        {{-- Include Custome CSS based on Language  --}}
        <link rel="stylesheet" href="{{asset('public//css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public//css/custome.css')}}">
        <!--end:: Styles -->
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        @yield('content')
        <script src="{!!asset('public/assets/vendors/custom/jquery.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('public/assets/vendors/custom/jstree/dist/jstree.min.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('public/assets/vendors/base/vendors.bundle.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('public/assets/demo/demo12/base/scripts.bundle.js')!!}" type="text/javascript"></script>
        <!-- Page Scripts -->
        @include('assets.custom_js')
        @yield('js-code')
    </body>
    <!-- end::Body -->
</html>