<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              {{ trans('work_category.add_new_category') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">  
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('work_category.category_name') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="category_name" id="category_name">
              <div class="category_name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('work_category.category_code') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="category_code" id="category_code">
              <div class="category_code error-div" style="display:none;"></div>
            </div>
          </div> 
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('work_category.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
