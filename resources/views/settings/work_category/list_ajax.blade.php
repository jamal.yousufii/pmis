<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
        <tr>
        <th width="8%">{{ trans('global.id') }}</th>
        <th width="20%">{{ trans('work_category.category_name') }}</th>
        <th width="15%">{{ trans('work_category.category_code') }}</th>
        <th width="6%">{{ trans('global.action') }}</th>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
            @foreach($records AS $rec)
                <?php $enc_id = encrypt($rec->id); ?>
                <tr>
                    <td>{!!$loop->iteration!!}</td>
                    <td>{!!$rec->name!!}</td>
                    <td>{!!$rec->code!!}</td>
                    <td>
                    <span class="dtr-data">
                        <span class="dropdown">
                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                            @if(doIHaveRole("setting_work_category","work_category_edit"))
                            <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('work_category.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('work_category.edit') }}</a>
                            @endif
                            @if(doIHaveRole("setting_work_category","work_category_delete") and $rec->is_admin=='1')
                            <a class="dropdown-item" href="javascript:void()" onclick="doDestroy('{{route('work_category.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('work_category.delete') }}</a>
                            @endif
                        </div>
                        </span>
                    </span>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
      <!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
  			document.cookie = "no="+$(this).text();
        var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1;
  			$.ajax({
                url: '{{ route("filterWorkCategory") }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                    $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                    $('#searchresult').html(response);
                }
            }
        );
  		}
  	});
  });
</script>