@if($users->count()>0) 
    <div class="m-form__group form-group table-responsive">
        @foreach($users as $item)
            <table width="100%" class="table" style="border-top:0px">
                <tr>
                    <td width="80%" class="border-bottom">
                        <img src="{!!asset('public/attachments/users/'.Auth::user()->profile_pic)!!}" class="m--img-rounded m--marginless m--img-centered img-thumbnail" alt="" width="50px;"/>
                        <span class="kt-widget4__username pt-3">{{$item->name}}</span>
                    </td>
                    <td width="20%" class="border-bottom">
                        <div class="m-checkbox-list">
                            <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                <input type="checkbox">  <span style="transform: rotate(90deg);"></span>
                            </label>
                        </div>
                    </td>
                </tr>
            </table>
        @endforeach
        <a class="btn btn-sm btn-secondary border-dark mt-5 mb-2 float-right" onclick="getAndShwoData('users','selected_users')">
            <span>{{ trans('projects.select') }}</span>
        </a>
    </div>
@else
    <span class="title-custom text-danger">{{ trans('projects.not_found') }} </span>
@endif
