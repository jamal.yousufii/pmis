<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head m-portlet__head-bg">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ $plan->name }}</h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('projects.add_contractor') }}</h3>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="dataForm" method="post">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('projects.contractor') }} : <span style="color:red;">*</span></label>
                    <div id="contractor_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="contractor_id">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($contractors)
                                @foreach($contractors as $item)
                                    <option value="{!!$item->id!!}">{!!$item->company_name!!}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="contractor_id error-div" style="display:none;"></div>
                </div> 
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('projects.project') }} : <span style="color:red;">*</span></label>
                    <div id="project_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="project_id" required>
                            <option value="{{$plan->id}}" selected="selected">{{$plan->name}}</option>
                        </select>
                    </div>
                    <div class="project_id error-div" style="display:none;"></div>
                </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <button type="button" onclick="storeRecord('{{route('projects.store')}}','dataForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @csrf
    </form>
    <!--end::Form--> 
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>