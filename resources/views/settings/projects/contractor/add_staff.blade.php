<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head m-portlet__head-bg">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ $plan->name }}</h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('projects.add_staff') }}</h3>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="dataForm" method="post">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('projects.project') }} : <span style="color:red;">*</span></label>
                    <div id="project_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="project_id" required>
                            <option value="{{$plan->id}}" selected="selected">{{$plan->name}}</option>
                        </select>
                    </div>
                    <div class="project_id error-div" style="display:none;"></div>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('surveys.project_location') }}: <span style="color:red;">*</span></label>
                    <div id="location_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="location_id" required>
                            <option value="">{{ trans('global.select') }}</option>
                            @if($location)
                                @foreach($location as $loc)
                                    <option value="{{$loc->id}}">{{ $loc->province->{'name_'.$lang} }} @if($loc->district_id) / {{ $loc->district->{'name_'.$lang} }} @endif @if($loc->village_id) / {{ $loc->village->{'name_'.$lang} }} @endif  @if($loc->latitude) / {{ $loc->latitude }} @endif @if($loc->longitude) / {{ $loc->longitude }} @endif </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="location_id error-div" style="display:none;"></div>
                    <span class="m-form__help">{{ trans('surveys.location_note') }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('projects.contractor') }} : <span style="color:red;">*</span></label>
                    <div id="contractor_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="contractor_id" required>
                            <option value="{{$contractor->id}}" selected="selected">{{$contractor->company_name}}</option>
                        </select>
                    </div>
                    <div class="contractor_id error-div" style="display:none;"></div>
                </div> 
            </div>
            <div class="m-form__group form-grouprow m-form__group_custom table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                        <tr class="bg-dark white-color">
                            <th width="20%">{{ trans('authentication.photo') }}</th>
                            <th width="20%">{{ trans('authentication.name') }}</th>
                            <th width="20%">{{ trans('authentication.father') }}</th>
                            <th width="30%">{{ trans('authentication.position') }}</th>
                            <th width="10%">{{ trans('global.choose') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($users) 
                            @foreach($users as $item)
                                <tr>
                                    <td class="border-bottom border-top-0">
                                        <span class="m-topbar__userpic">
                                            <img src="{{ asset('attachments/users/'.$item->profile_pic) }}" class="img m--img-centered img-thumbnail" width="50px;" height="60px;">
                                        </span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <span class="kt-widget4__username pt-6">{{$item->name}}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <span class="kt-widget4__username pt-6">{{$item->father}}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <span class="kt-widget4__username pt-6">{{$item->position}}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <div class="m-checkbox-list">
                                                <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                                    <input type="checkbox" name="user_id[]" id="{{$item->id}}" value="{{$item->id}}" onchange='handleChange(this)'>  <span style="transform: rotate(90deg);"></span>
                                                </label>
                                            </div>
                                        </td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif   
                    </tbody> 
                </table>          
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <button type="button" id="submit" disabled onclick="storeRecord('{{route('projects.store_staff')}}','dataForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @csrf
    </form>
    <!--end::Form--> 
</div>
<script type="text/javascript">
    var flag = false;
    function handleChange(checkbox) {
        if(checkbox.checked == true){
            document.getElementById("submit").removeAttribute("disabled"); 
        }else{
            document.getElementById("submit").setAttribute("disabled", "disabled");   
        }
    }
    $(".select-2").select2();
</script>