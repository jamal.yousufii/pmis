@extends('master')
@section('head')
  <title>{{ trans('projects.projects') }}</title>
  <style>
    .m-portlet-custom{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19) !important;
        background: #ffffff !important;
    }
    .btn-accent {
        background-color: #FFFFFF !important;
    }
    .disabled {
        background-color: #B0C798;
        color: #808080; !important;
        background: transparent;
        opacity: 0.4;
    }
    .main-link-div:hover{
        cursor: pointer;
        box-shadow: 0 4px 8px 0 #aed7f7, 0 6px 20px 0 #aed7f7 !important;
    }
    .main-link-div-disabled :hover{
        cursor: not-allowed;
    }
  </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">{{ trans('projects.projects') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <div class="m-input-icon m-input-icon--left">
                            <a href="{{ route('home','setting') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row"> 
        @if($departments)
            @foreach($departments as $item)
                <div class="col-xl-4 col-lg-4 col-xs-4 col-md-4 main pb-2">
                    @if(check_my_main_department($item->id))
                        <div class="col-lg-10 offset-lg-1">
                            <a class="main-link-div" href="{{ route('bringProjects',encrypt($item->id)) }}">
                                <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 m-portlet-custom-hover">
                                    <div class="m-widget21 info">
                                        <div class="row">
                                            <div class="col">
                                                <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                                    <span class="m-widget21__icon px-3">
                                                        <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                                    </span>
                                                    <div class="m-widget21__info">
                                                        <span class="m-widget21__title">{{ $item->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @elseif(!check_my_main_department($item->id))
                        <div class="col-lg-10 offset-lg-1 disabled ain-link-div-disabled">
                            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
                                <div class="m-widget21">
                                    <div class="row">
                                        <div class="col">
                                            <div class="m-widget21__item proj_sum_wdg">
                                                <span class="m-widget21__icon">
                                                    <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                                </span>
                                                <div class="m-widget21__info">
                                                    <span class="m-widget21__title">{{$item->name}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        @endif
    </div>
@endsection
