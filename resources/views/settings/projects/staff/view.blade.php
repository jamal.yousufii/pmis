@extends('master')
@section('head')
  <title>{{ trans('contracts.contracts') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head m-portlet__head-bg table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text text-white">{{ $plan->name }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('bringProjectsStaff',session('current_department')) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">{{ trans('projects.list_staff') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                @if(doIHaveRole("setting_projects","pro_set_add"))
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="javascript:void()" onclick="addRecord('{{route('staff_add',$enc_id)}}','','GET','response_div')" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                <span><i class="la la-cart-plus"></i><span>{{ trans('projects.add_department_staff') }}</span></span>
                            </a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
        @alert()
        @endalert
        <div class="m-portlet__body table-responsive">
            <table class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                    <tr>
                        <th width="8%">{{ trans('global.number') }}</th>
                        <th width="20%">{{ trans('authentication.name') }}</th>
                        <th width="20%">{{ trans('authentication.father') }}</th>
                        <th width="20%">{{ trans('authentication.position') }}</th>
                        <th width="20%">{{ trans('surveys.project_location') }}</th>
                        <th width="8%">{{ trans('global.action') }}</th>
                    </tr>
                </thead>
                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                    @if($records)
                        @foreach($records AS $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->users['name']}}</td>
                                <td>{{$item->users['father']}}</td>
                                <td>{{$item->users['position']}}</td>
                                <td>@if($item->location){{$item->location->province->{'name_'.$lang} }} @if($item->location->district_id)/ {{$item->location->district->{'name_'.$lang} }} @endif @if($item->location->village_id)/ {{$item->location->village->{'name_'.$lang} }} @endif @if($item->location->latitude)/ {{ $item->location->latitude }} @endif @if($item->location->longitude)/ {{ $item->location->longitude }} @endif @endif</td>
                                <td>
                                    <span class="dtr-data">
                                        <span class="dropdown">
                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('staff_destroy',encrypt($item->id))}}','','GET','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                                            </div>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection