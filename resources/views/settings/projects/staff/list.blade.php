@extends('master')
@section('head')
  <title>{{ trans('projects.projects') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4 mb-0" role="tablist">
      <li class="nav-item m-tabs__item">
        <a href="{{ route('bringProjects',encrypt($dep_id)) }}" class="nav-link m-tabs__link">
          <span class="m-portlet__head-text tab_custome"><strong>{{ trans('projects.contractor') }}</strong></span>
        </a>
      </li>
      <li class="nav-item m-tabs__item">
        <a href="{{ route('bringProjectsStaff',encrypt($dep_id)) }}" class="nav-link m-tabs__link active">
          <span class="m-portlet__head-text tab_custome"><strong>{{ $department_name }}</strong></span>
        </a>
      </li>
    </ul>
    <div class="tab-content" id="content">
      <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet__head table-responsive">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('projects.list') }}</h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <div class="m-input-icon m-input-icon--left">
                  <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                    <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
                  </a>
                </div>
              </li>
              <li class="m-portlet__nav-item">
                <a href="{{ route('projects.index') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!-- Filter Start -->
        <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-5 col-xs-5 col-md-5 col-sm-5">
                  <div class="m-input-icon m-input-icon--left" style="width:280px;">
                    <input class="form-control m-input" type="text" name="keyWord" id="keyWord" placeholder="{{ trans('projects.search') }}" onkeyup="searchRecords('{{route('filter_projects')}}','search','GET','searchresult')">
                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
                    <input type="hidden" name="department_id" value="{{$dep_id}}" />
                    <input type="hidden" name="section" value="staff" />
                  </div>
                </div>
              </div>
            </div>
          </form>  
        </div>
        <!-- Filter End -->
        <div class="m-portlet__body table-responsive" id="searchresult">
          <table class="table table-striped- table-bordered table-hover table-checkable">
            <thead>
              <tr class="bg-light">
                <th width="12%">{{ trans('global.urn') }}</th>
                <th width="24%">{{ trans('contracts.project_name') }}</th>
                <th width="15%">{{ trans('contracts.project_code') }}</th>
                <th width="20%">{{ trans('global.shared_from') }}</th>
                <th width="20%">{{ trans('global.shared_by') }}</th>
                <th width="8%">{{ trans('global.action') }}</th>
              </tr>
            </thead>
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
              @if($records)
                @foreach($records AS $rec)
                  <?php $enc_id = encrypt($rec->id); ?>
                  <tr>
                    <td>{{$rec->urn}}</td>
                    <td>{{$rec->name}}</td>
                    <td>{{$rec->code}}</td>
                    <td>@if($rec->share()->where('share_to_code','pmis_progress')->first()){!!$rec->share()->where('share_to_code','pmis_progress')->first()->from_department->{'name_'.$lang}!!}@endif</td>
                    <td>@if($rec->share()->where('share_to_code','pmis_progress')->first()->user){!!$rec->share()->where('share_to_code','pmis_progress')->first()->user->name!!}@endif</td>
                    <td>
                      <span class="dtr-data">
                        <span class="dropdown">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                          <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                            @if(doIHaveRole("setting_projects","pro_set_view"))
                              <a class="dropdown-item" href="{{ route('list_staff',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                            @endif
                          </div>
                        </span>
                      </span>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          @if(!empty($records))
            {!!$records->links('pagination')!!}
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection