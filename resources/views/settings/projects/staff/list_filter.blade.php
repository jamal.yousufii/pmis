<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead>
    <tr class="bg-light">
      <th width="12%">{{ trans('global.urn') }}</th>
      <th width="24%">{{ trans('contracts.project_name') }}</th>
      <th width="15%">{{ trans('contracts.project_code') }}</th>
      <th width="20%">{{ trans('global.shared_from') }}</th>
      <th width="20%">{{ trans('global.shared_by') }}</th>
      <th width="8%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
      @foreach($records AS $rec)
        <?php $enc_id = encrypt($rec->id); ?>
        <tr>
          <td>{{$rec->urn}}</td>
          <td>{{$rec->name}}</td>
          <td>{{$rec->code}}</td>
          <td>@if($rec->share()->where('share_to_code','pmis_progress')->first()){!!$rec->share()->where('share_to_code','pmis_progress')->first()->from_department->{'name_'.$lang}!!}@endif</td>
          <td>@if($rec->share()->where('share_to_code','pmis_progress')->first()->user){!!$rec->share()->where('share_to_code','pmis_progress')->first()->user->name!!}@endif</td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  <!-- @if(doIHaveRole("setting_projects","pro_set_view"))
                    <a class="dropdown-item" href="{{ route('projects.show',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                  @endif -->
                  @if(doIHaveRole("setting_projects","pro_set_view"))
                    <a class="dropdown-item" href="{{ route('list_staff',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                  @endif
                </div>
              </span>
            </span>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function() {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
        document.cookie = "no="+$(this).text();
  			var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&keyWord="+'{!!$keyWord!!}'+"&department_id="+'{!!$department_id!!}';
  			$.ajax({
                url: '{{ route("filter_projects") }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                    $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
                },
                success: function(response)
                {
                    $('#searchresult').html(response);
                }
  	    });
  		}
  	});
  });
</script>
