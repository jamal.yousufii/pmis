@extends('master')
@section('head')
  <title>{{ trans('contracts.contracts') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head m-portlet__head-bg">
      <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text text-white">{{ $record->project->name }}</h3>
          </div>
      </div>
      <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                  <a href="{{ route('projects.list',encrypt($record->project->id)) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                      <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                  </a>
              </li>
          </ul>
      </div>
    </div>
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('projects.view_contractor') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        @if(doIHaveRole("setting_projects","pro_set_add"))    
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="javascript:void()" onclick="addRecord('{{ route('projects.create_staff',encrypt($record->project->id)) }}','contractor_id={{encrypt($record->contractor->id)}}','GET','response_div')" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                        <span><i class="la la-cart-plus"></i><span>{{ trans('projects.add_staff') }}</span></span>
                    </a>
                </li>
            </ul>
        @endif
      </div>
    </div>
    @alert()
    @endalert
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
      <div class="m-portlet__body">
        @if($record)
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('projects.contractor') }} :</label><br>
              <span>{{ $record->contractor->company_name }}</span>
            </div> 
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('projects.project') }} :</label><br>
              <span>{{ $record->project->name }}</span>
            </div>
          </div>
          <table class="table table-striped- table-bordered table-hover table-checkable">
              <thead>
                  <tr class="bg-dark white-color">
                      <th width="10%">{{ trans('global.number') }}</th>
                      <th width="20%">{{ trans('authentication.name') }}</th>
                      <th width="20%">{{ trans('authentication.father') }}</th>
                      <th width="20%">{{ trans('authentication.position') }}</th>
                      <th width="20%">{{ trans('surveys.project_location') }}</th>
                      <th width="10%">{{ trans('global.action') }}</th>
                  </tr>
              </thead>
              <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                  @if($users) 
                      @foreach($users as $item)
                          <tr>
                              <td class="border-bottom border-top-0">
                                    <span class="kt-widget4__username pt-6">{{$loop->iteration}}</span>
                              </td>
                              <td class="border-bottom border-top-0">
                                    <span class="kt-widget4__username pt-6">{{$item->name}}</span>
                              </td>
                              <td class="border-bottom border-top-0">
                                    <span class="kt-widget4__username pt-6">{{$item->father}}</span>
                              </td>
                              <td class="border-bottom border-top-0">
                                    <span class="kt-widget4__username pt-6">{{$item->position}}</span>
                              </td>
                              <td class="border-bottom border-top-0">
                                    <span class="kt-widget4__username pt-6">{{$item->pro_name}} / {{$item->dis_name}} / {{$item->vil_name}} / {{$item->location}}</span>
                              </td>
                              <td class="border-bottom border-top-0">
                                <span class="dtr-data">
                                    <span class="dropdown">
                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                            <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('projects.destroy',encrypt($item->staff_id))}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                                        </div>
                                    </span>
                                </span>
                              </td>
                          </tr>
                      @endforeach
                  @endif   
              </tbody> 
          </table>
        @endif
      </div>
    </div> 
  </div>
@endsection