@extends('master')
@section('head')
  <title>{{ trans('feature_of_work.list') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            {{ trans('feature_of_work.list') }}
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("setting_work_feature","feature_add"))
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" onclick="addRecord('{{route('feature_of_work.create')}}','','GET','response_div');" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="la la-cart-plus"></i><span>{{ trans('feature_of_work.add_new_feature') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('home','setting') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-5 col-xs-5 col-md-5 col-sm-5">
            <div class="m-input-icon m-input-icon--left" style="width:280px;">
            <input class="form-control m-input" type="text" name="keyWord" id="keyWord" placeholder="{{ trans('feature_of_work.search') }}" onkeyup="filterRecords('{{route('filterFeatureOfWork')}}','GET','searchresult')">
              <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-search"></i></span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Filter End -->
    <div class="m-portlet__body table-responsive" id="searchresult">
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr>
            <th width="8%">{{ trans('global.id') }}</th>
            <th width="20%">{{ trans('feature_of_work.feature_name') }}</th>
            <th>{{ trans('feature_of_work.feature_description') }}</th>
            <th width="5%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <?php $enc_id = encrypt($rec->id); ?>
              <tr id='removetr_{{$rec->id}}'>
                <td>{!!$loop->iteration!!}</td>
                <td>{!!$rec->name!!}</td>
                <td>{!!$rec->description!!}</td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRole("setting_work_feature","feature_edit"))
                          <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('feature_of_work.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('feature_of_work.edit') }}</a>
                        @endif
                        @if(doIHaveRole("setting_work_feature","feature_delete"))
                          <a class="dropdown-item" href="javascript:void()" onclick="doDestroy('{{route('feature_of_work.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('feature_of_work.delete') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection
