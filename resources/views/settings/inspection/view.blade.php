
<div class="m-portlet m-portlet--full-height ">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('inspection.view_inspection') }}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('inspection',session('current_department')) }}">
                        <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__head text-title" style="height: 3.1rem">
        <div class="m-portlet__head-caption">{{ trans('inspection.main') }}</div>
    </div>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('inspection.inspection_name_dr') }} :</label><br>
                    <span>{!!$inspection->name_dr!!}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('inspection.inspection_name_pa') }} :</label><br>
                    <span>{!!$inspection->name_pa!!}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('inspection.inspection_name_en') }} :</label><br>
                    <span>{!!$inspection->name_en!!}</span>
                </div>
            </div>
            
            <div class="m-portlet__head text-title" style="height: 3.1rem">
                <div class="m-portlet__head-caption">{{ trans('inspection.sub') }}</div>
            </div>

            <div class="form-group m-form__group row m-form__group_custom">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="33.33%" style="border-top:0px;">{{trans('inspection.inspection_name_dr')}}</th>
                            <th width="33.33%" style="border-top:0px;">{{trans('inspection.inspection_name_pa')}}</th>
                            <th width="33.33%" style="border-top:0px;">{{trans('inspection.inspection_name_en')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($inspection->inspectionSub)
                            @foreach($inspection->inspectionSub as $item)
                                <tr>
                                    <td>{{$item->name_dr}}</td>
                                    <td>{{$item->name_pa}}</td>
                                    <td>{{$item->name_en}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>