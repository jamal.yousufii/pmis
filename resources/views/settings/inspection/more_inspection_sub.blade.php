<div id="more_inspection_sub_div_{!!$number!!}">
    <div class="m-portlet__head text-title" style="height: 3.1rem">
        <div class="m-portlet__head-caption">{{ trans('inspection.sub') }}</div>
        <div class="m-portlet__head-tools">
            <button type="button" id="more_inspection_sub_div_btn_{!!$number!!}" class="btn btn-warning btn-sm btn-sm--air btn-xs" onclick="remove_more('more_inspection_sub_div_{!!$number!!}',{!!$number!!},'more_inspection_sub_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
        </div>
    </div>
    <div class="row form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('inspection.inspection_name_dr') }} :</label>
            <input class="form-control m-input errorDiv" type="text" name="name_sub_dr[]" id="name_sub_dr">
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('inspection.inspection_name_pa') }} :</label>
            <input class="form-control m-input errorDiv" type="text" name="name_sub_pa[]" id="name_sub_pa">
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('inspection.inspection_name_en') }} :</label>
            <input class="form-control m-input errorDiv" type="text" name="name_sub_en[]" id="name_sub_en">    
        </div>
    </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>