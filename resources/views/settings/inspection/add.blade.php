<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              {{ trans('inspection.add_new_inspection') }}
            </h3>
          </div>
        </div>
      </div>

      <div class="m-portlet__head text-title" style="height: 3.1rem">
        <div class="m-portlet__head-caption">
            {{ trans('inspection.main') }}
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">  
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_dr') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_dr" id="name_dr">
              <div class="name_dr error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_pa') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_pa" id="name_pa">
              <div class="name_pa error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_en') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_en" id="name_en">
              <div class="name_en error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="m-portlet__head text-title" style="height: 3.1rem">
            <div class="m-portlet__head-caption">{{ trans('inspection.sub') }}</div>
            <div class="m-portlet__head-tools">
              <button type="button" class="btn btn-primary btn-sm btn-sm--air btn-xs mt-33" onclick="add_more('inspection_sub_div','{{route('more_inspection_sub')}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">  
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_dr') }} : </label>
              <input class="form-control m-input errorDiv" type="text" name="name_sub_dr[]" id="name_sub_dr">
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_pa') }} : </label>
              <input class="form-control m-input errorDiv" type="text" name="name_sub_pa[]" id="name_sub_pa">
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('inspection.inspection_name_en') }} : </label>
              <input class="form-control m-input errorDiv" type="text" name="name_sub_en[]" id="name_sub_en">
            </div>
          </div>
          <div id="inspection_sub_div"></div><!-- Display mor inspection sub  -->
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('inspection.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
