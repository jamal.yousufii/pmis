@extends('master')
@section('head')
  <title>{{ trans('contractor.cont_staff_list') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            {{ trans('contractor.cont_staff_list') }}
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("setting_staff","contractor_staff_add"))
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" onclick="addRecord('{{route('contractor_staff.create')}}','','GET','response_div');" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="la la-cart-plus"></i><span>{{ trans('contractor.add_new_cont_staff') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a href="{{ route('home','setting') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-5 col-xs-5 col-md-5 col-sm-5">
            <div class="m-input-icon m-input-icon--left" style="width:280px;">
              <input class="form-control m-input" type="text" name="keyWord" id="keyWord" placeholder="{{ trans('contractor.search_users') }}" onkeyup="filterRecords('{{route('filter_contractor_staff')}}','GET','searchresult')">
              <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-search"></i></span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Filter End -->
    <div class="m-portlet__body table-responsive" id="searchresult">
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr class="bg-light">
            <th width="8%">{{ trans('contractor.rec_id') }}</th>
            <th width="20%">{{ trans('contractor.contact_name') }}</th>
            <th width="15%">{{ trans('contractor.father') }}</th>
            <th width="15%">{{ trans('contractor.contact_email') }}</th>
            <th width="16%">{{ trans('contractor.position') }}</th>
            <th width="20%">{{ trans('contractor.company') }}</th>
            <th width="6%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
          @foreach($records AS $rec)
            <?php $enc_id = encrypt($rec->id); ?>
            <tr>
              <td>{!!$loop->iteration!!}</td>
              <td>{!!$rec->name!!}</td>
              <td>{!!$rec->father!!}</td>
              <td>{!!$rec->email!!}</td>
              <td>{!!$rec->position!!}</td>
              <td>{!!$rec->contractor['company_name']!!}</td>
              <td>
                <span class="dtr-data">
                  <span class="dropdown">
                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                      @if(doIHaveRole("setting_staff","contractor_staff_view"))
                        <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{route('contractor_staff.show',$enc_id)}}','','GET','response_div')"><i class="la la-file-text"></i>{{ trans('contractor.view') }}</a>
                      @endif
                      @if(doIHaveRole("setting_staff","contractor_staff_edit"))
                        <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('contractor_staff.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('contractor.edit') }}</a>
                      @endif
                      @if(doIHaveRole("setting_staff","contractor_staff_delete") and $rec->is_admin=='1')
                        <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('contractor_staff.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('contractor.delete') }}</a>
                      @endif
                    </div>
                  </span>
                </span>
              </td>
            </tr>
          @endforeach
        @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection
