<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('contractor.add_new_cont_staff') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="staffForm" method="post">
        <div class="m-portlet__body" style="background: #f7f8fa;">
          <div class="row">
            <div class="col-xl-10">
              <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                      <div id="department_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="department_id" onchange="bringModules(this.value,'{{ route("contractor_modulesByDepartment") }}');">
                          <option value="">{{ trans('global.select') }}</option>
                          @if($departments)
                            @foreach($departments as $item)
                                <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="department_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('contractor.title') }} : <span style="color:red;">*</span></label>
                      <div id="contractor_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="contractor_id" >
                          <option value="">{{ trans('global.select') }}</option>
                          @if($contractor)
                            @foreach($contractor as $item)
                              <option value="{{$item->id}}">{{ $item->company_name }}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="contractor_id error-div" style="display:none;"></div>
                    </div>            
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.name') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="text" value="" name="name" id="name" onkeyup="putValue(this.id,'username_div')">
                      <div class="name error-div" style="display:none;"></div>
                    </div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">       
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.father') }} :</label>
                      <input class="form-control m-input" type="text" value="" name="father" id="father">
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.position') }} :</label>
                      <input class="form-control m-input" type="text" value="" name="position" id="position">
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.phone_no') }} : </label>
                      <input class="form-control m-input" type="number" min="0" value="" name="phone_no" id="phone_no">
                    </div>
                  </div>  
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.email') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="email" value="" name="email" id="email">
                      <div class="email error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.password') }} : <span style="color:red;">*</span></label>
                      <div class="m-input-icon m-input-icon--left">
                        <input class="form-control m-input errorDiv" type="password" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
                        <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
                      </div>
                      <div class="password error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.confirm_password') }} : <span style="color:red;">*</span></label>
                      <div class="m-input-icon m-input-icon--left">
                        <input class="form-control m-input errorDiv" type="password" name="confirm_password" id="conf_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
                        <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf(0)"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
                      </div>
                      <div class="confirm_password error-div" style="display:none;"></div>
                      <span id="msg"><span>
                    </div>
                  </div>   
                </div>
              </div>
            </div>
            <div class="col-xl-2">
              <div class="m-portlet m-portlet--full-height m-portlet--fit">
                <div class="m-portlet__body pt-4">
                  <div style="text-align:center" class="col-lg-12">
                    <img src="{!!asset('public/attachments/users/default.png')!!}" class="img img-thumbnail" width="150" />
                  </div>
                  <div style="text-align:center" class="col-lg-12 title-custom pt-3" id="username_div">
                  </div>
                </div>
              </div>
            </div>
          </div> 
          <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
            <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom" id="user_roles"></div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('contractor_staff.store')}}','staffForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
