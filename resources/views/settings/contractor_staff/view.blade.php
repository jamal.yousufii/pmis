<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.view_user') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="#" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
          </ul>
        </div>
      </div>
      <!--begin::record-->
      @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body" style="background: #f7f8fa;">
            <div class="row">
              <div class="col-xl-10">
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                  <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4 col-md-4">
                        <label class="title-custom">{{ trans('authentication.user_dep') }} :</label><br>
                        <span>{!!$record->department['name_'.$lang]!!}</span>
                      </div>
                      <div class="col-lg-4 col-md-4">
                        <label class="title-custom">{{ trans('contractor.title') }} :</label><br>
                        <span>{!!$record->contractor->company_name!!}</span>
                      </div>  

                      <div class="col-lg-4 col-md-4">
                        <label class="title-custom">{{ trans('authentication.name') }} :</label><br>
                        <span>{!!$record->name!!}</span>
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4 col-md-4">
                        <label class="title-custom">{{ trans('authentication.father') }} :</label><br>
                        <span>{!!$record->father!!}</span>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.position') }} :</label><br>
                        <span>{!!$record->position!!}</span>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.phone_no') }} : </label><br>
                        <span>{!!$record->phone_number!!}</span>
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-12 col-md-12">
                        <label class="title-custom">{{ trans('authentication.email') }} :</label><br>
                        <span>{!!$record->email!!}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-2">
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                  <div class="m-portlet__body pt-4">
                    <div style="text-align:center" class="col-lg-12">
                      <img src="{!!asset('public/attachments/users/'.$record->profile_pic)!!}" class="img img-thumbnail" width="150" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
              <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview p-3 px-4" style="height: 180px">
                    @if($modules)
                      @foreach($modules as $mod)
                        <div class="m-checkbox-list <?=in_array($mod->id,$selectedMods) ? 'bg-warning' : ''?>">
                          <label class="m-checkbox m-checkbox--state-brand">
                            <input type="checkbox" disabled <?=in_array($mod->id,$selectedMods) ? 'checked' : ''?>> {{ $mod->name }}
                            <span class="rotate"></span>
                          </label>
                        </div>
                      @endforeach
                    @endif
                  </div>
                </div> 
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.section') }} :</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px">
                    @if($sections)
                      <div class="p-3 px-4">
                        @foreach($sections as $key => $value)
                          <strong>{!!$key!!}</strong>
                          @foreach($value as $item)
                            <?php $section = explode('-',$item) ?>
                            <div class="m-checkbox-list <?=in_array($section[0],$selectedSec) ? 'bg-warning' : ''?>">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" disabled <?=in_array($section[0],$selectedSec) ? 'checked' : ''?> > {{ $section[1] }}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endforeach  
                        @endforeach
                      </div>
                    @endif
                  </div>
                </div> 
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.role') }} :</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px">
                    @if($roles)
                      <div class="p-3 px-4">
                        @foreach($roles as $key => $value)
                          <strong>{!!$key!!}</strong>
                          @foreach($value as $item)
                            <?php $role = explode('-',$item) ?>
                            <div class="m-checkbox-list <?=in_array($role[0],$selectedRol) ? 'bg-warning' : ''?>">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" disabled <?=in_array($role[0],$selectedRol) ? 'checked' : ''?>> {{ $role[1] }}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endforeach  
                        @endforeach
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif
      <!--end::record-->
    </div>
  </div>
</div>
