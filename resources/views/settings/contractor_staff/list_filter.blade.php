<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead>
    <tr class="bg-light">
      <th width="8%">{{ trans('contractor.rec_id') }}</th>
      <th width="20%">{{ trans('contractor.name') }}</th>
      <th width="15%">{{ trans('contractor.father') }}</th>
      <th width="15%">{{ trans('contractor.email') }}</th>
      <th width="16%">{{ trans('contractor.position') }}</th>
      <th width="20%">{{ trans('contractor.company') }}</th>
      <th width="6%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
  @if($records)
    @foreach($records AS $rec)
      <?php $enc_id = encrypt($rec->id); ?>
      <tr>
        <td>{!!$loop->iteration!!}</td>
        <td>{!!$rec->name!!}</td>
        <td>{!!$rec->father!!}</td>
        <td>{!!$rec->email!!}</td>
        <td>{!!$rec->position!!}</td>
        <td>{!!$rec->contractor['company_name']!!}</td>
        <td>
            <span class="dtr-data">
                <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRole("contractor_staff","contractor_staff_view"))
                        <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{route('contractor_staff.show',$enc_id)}}','','GET','response_div')"><i class="la la-file-text"></i>{{ trans('contractor.view') }}</a>
                        @endif
                        @if(doIHaveRole("contractor_staff","contractor_staff_edit"))
                        <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('contractor_staff.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('contractor.edit') }}</a>
                        @endif
                        @if(doIHaveRole("contractor_staff","contractor_staff_delete") and $rec->is_admin=='1')
                        <a class="dropdown-item" href="javascript:void()" onclick="doDestroy('{{route('contractor_staff.destroy',$enc_id)}}','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('contractor.delete') }}</a>
                        @endif
                    </div>
                </span>
            </span>
        </td>
      </tr>
    @endforeach
  @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
    $(document).ready(function()
    {
      $('.pagination a').on('click', function(event) {
          event.preventDefault();
          if ($(this).attr('href') != '#') {
              document.cookie = "no="+$(this).text();
              var dataString = '';
              item = $('#keyWord').val();
              dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&item="+item+"&type=search";
                      $.ajax({
                      url: '{{ route("filter_contractor_staff") }}',
                      data: dataString,
                      type: 'get',
                      beforeSend: function(){
                          $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                      },
                      success: function(response)
                      {
                          $('#searchresult').html(response);
                      }
                  }
              );
          }
      });
    });
</script>