@if($modules)
  <div class="col-lg-4">
    <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
    <div class="m-portlet">
      <div class="m-demo__preview p-3 px-4" style="height: 180px">
        @if($modules)
          @foreach($modules as $item)
            <div class="m-checkbox-list" id="module_div">
              <label class="m-checkbox m-checkbox--state-brand">
                <input type="checkbox" name="modules" id="module_{{$item->id}}" value="{{$item->id}}" onclick="setBackground('module_div','module_{{$item->id}}','bg-warning');bringSections('{{ route("contractor_sectionsByModule") }}','module_{{$item->id}}')"> {!!$item->name!!}
                <span class="rotate"></span>
              </label>
            </div>
          @endforeach
        @endif
      </div>
    </div> 
  </div>
  <div class="col-lg-4">
    <label class="title-custom">{{ trans('authentication.section') }} :</label><br>
    <div class="m-portlet">
      <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="section_div">

      </div>
    </div> 
  </div>
  <div class="col-lg-4">
    <label class="title-custom">{{ trans('authentication.role') }} :</label><br>
    <div class="m-portlet">
      <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="role_div">

      </div>
    </div>
  </div>
@endif
