@if($sections)
  <div class="p-3 px-4">
    @foreach($sections as $key => $value)
      <label class="title-custom">{!!$key!!}</label>
      @foreach($value as $item)
        <?php $section = explode('-',$item) ?>
        <div class="m-checkbox-list" id="div_section_{{$section[0]}}">
          <label class="m-checkbox m-checkbox--state-brand">
            <input type="checkbox" name="sections[]" id="sec_{{$section[0]}}" value="{{ $section[0] }}" onclick="setBackground('div_section_{{$section[0]}}','sec_{{$section[0]}}','bg-warning');bringRoles('{{ route("contractor_rolesBySections") }}','sections[]')"> {!!$section[1]!!}
            <span class="rotate"></span>
          </label>
        </div>
      @endforeach
    @endforeach
  </div>
@endif