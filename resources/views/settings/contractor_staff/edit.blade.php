<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('contractor.edit_cont_staff') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="staffForm" method="post">
          <div class="m-portlet__body" style="background: #f7f8fa;">
            <div class="row">
              <div class="col-xl-10">
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                  <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                        <div id="department_id" class="errorDiv">
                          <select class="form-control m-input m-input--air select-2" name="department_id" onchange="bringModules(this.value,'{{ route("contractor_modulesByDepartment") }}');">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($departments)
                              @foreach($departments as $item)
                                <option value="{!!$item->id!!}" <?=$item->id==$record->department_id ? 'selected' : ''?>>{!!$item->name!!}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                        <div class="department_id error-div" style="display:none;"></div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('contractor.title') }} : <span style="color:red;">*</span></label>
                        <div id="contractor_id" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2" name="contractor_id">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($contractor)
                                @foreach($contractor as $item)
                                    @if($item->id==$record->contractor_id)
                                        <option value="{!!$item->id!!}" selected>{!!$item->company_name!!}</option>
                                    @else
                                        <option value="{!!$item->id!!}">{!!$item->company_name!!}</option>
                                    @endif
                                @endforeach
                            @endif
                            </select>
                        </div>
                        <div class="contractor_id error-div" style="display:none;"></div>
                      </div>              
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.name') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="text" value="{!!$record->name!!}" name="name" id="name">
                        <div class="name error-div" style="display:none;"></div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.father') }} :</label>
                        <input class="form-control m-input" type="text" value="{!!$record->father!!}" name="father" id="father">
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.position') }} :</label>
                        <input class="form-control m-input" type="text" value="{!!$record->position!!}" name="position" id="position">
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.phone_no') }} :</label>
                        <input class="form-control m-input" type="number" min="0" value="{!!$record->phone_number!!}" name="phone_no" id="phone_no">
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.email') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="email" value="{!!$record->email!!}" name="email" id="email">
                        <div class="email error-div" style="display:none;"></div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.new_password') }} :</label>
                        <div class="m-input-icon m-input-icon--left">
                          <input class="form-control m-input" type="password" value="" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
                          <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.conf_new_password') }} :</label>
                        <div class="m-input-icon m-input-icon--left">
                          <input class="form-control m-input" type="password" value="" name="confirm_password" id="conf_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
                          <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf()"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
                        </div>
                        <span id="msg"><span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-2">
                <div class="m-portlet m-portlet--full-height m-portlet--fit">
                  <div class="m-portlet__body pt-4">
                    <div style="text-align:center" class="col-lg-12">
                      <img src="{!!asset('public/attachments/users/'.$record->profile_pic)!!}" class="img img-thumbnail" width="150" />
                    </div>
                    <div style="text-align:center" class="col-lg-12 title-custom pt-3" id="username_div">
                      {!!$record->name!!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
              <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
            </div>
            

            <div class="form-group m-form__group row m-form__group_custom" id="user_roles">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview p-3 px-4" style="height: 180px">
                    @if($modules)
                      @foreach($modules as $item)
                        <div class="m-checkbox-list <?=in_array($item->id,$selectedMods) ? 'bg-warning' : ''?>" id="module_div">
                          <label class="m-checkbox m-checkbox--state-brand">
                            <input type="checkbox" name="modules" id="module_{{$item->id}}" value="{{$item->id}}" <?=in_array($item->id,$selectedMods) ? 'checked' : ''?> onclick="setBackground('module_div','module_{{$item->id}}','bg-warning');bringSections('{{ route("contractor_sectionsByModule") }}','module_{{$item->id}}')"> {!!$item->name!!}
                            <span class="rotate"></span>
                          </label>
                        </div>
                      @endforeach
                    @endif
                  </div>
                </div> 
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.section') }}:</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="section_div">
                    @if($sections)
                      <div class="p-3 px-4">
                        @foreach($sections as $key => $value)
                          <label class="title-custom">{!!$key!!}</label>
                          @foreach($value as $item)
                            <?php $section = explode('-',$item) ?>
                            <div class="m-checkbox-list <?=in_array($section[0],$selectedSec) ? 'bg-warning' : ''?>" id="div_section_{{$section[0]}}">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" name="sections[]" id="sec_{{$section[0]}}" value="{{ $section[0] }}" <?=in_array($section[0],$selectedSec) ? 'checked' : ''?> onclick="setBackground('div_section_{{$section[0]}}','sec_{{$section[0]}}','bg-warning');bringRoles('{{ route("contractor_rolesBySections") }}','sections[]','{{$enc_id}}')"> {!!$section[1]!!}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endforeach
                        @endforeach
                      </div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.role') }}:</label><br>
                <div class="m-portlet">
                  <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="role_div">
                    @if($roles)
                      <div class="p-3 px-4">
                        @foreach($roles as $key => $value)
                          <label class="title-custom">{!!$key!!}</label>
                          @foreach($value as $item)
                            <?php $role = explode('-',$item) ?>
                            <div class="m-checkbox-list <?=in_array($role[0],$selectedRol) ? 'bg-warning' : ''?>" id="div_role_{{$role[0]}}">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" name="roles[]" id="role_{{$role[0]}}" value="{{$role[0]}}" <?=in_array($role[0],$selectedRol) ? 'checked' : ''?> onclick="setBackground('div_role_{{$role[0]}}','role_{{$role[0]}}','bg-warning');"> {!!$role[1]!!}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endforeach
                        @endforeach
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <input type="hidden" name="enc_id" id="enc_id" value="{!!$enc_id!!}"/>
                    <button type="button" onclick="doEditRecord('{{route('contractor_staff.update',$enc_id)}}','staffForm','PUT','response_div')" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>

     