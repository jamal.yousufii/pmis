@if($roles)
  <div class="p-3 px-4">
    @foreach($roles as $key => $value)
      <label class="title-custom">{!!$key!!}</label>
      @foreach($value as $item)
        <?php $role = explode('-',$item) ?>
        <div class="m-checkbox-list" id="div_role_{{$role[0]}}">
          <label class="m-checkbox m-checkbox--state-brand">
            <input type="checkbox" name="roles[]" id="role_{{$role[0]}}" value="{{$role[0]}}" onclick="setBackground('div_role_{{$role[0]}}','role_{{$role[0]}}','bg-warning');"> {!!$role[1]!!}
            <span class="rotate"></span>
          </label>
        </div>
      @endforeach
    @endforeach
  </div>
@endif
