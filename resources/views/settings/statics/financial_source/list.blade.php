<div class="row">
  <div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{ trans('statics.financial_source') }}
            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          @if(doIHaveRole("setting_statics","sta_add"))
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <a href="javascript:void()" onclick="addFinancial_source('{!! $posted_value !!}')" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                  <span><i class="la la-cart-plus"></i><span>{{ trans('statics.add_financial_source') }}</span></span>
                </a>
              </li>
            </ul>
          @endif
        </div>
      </div>
      @if (Session::has('success'))
        <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
          <div class="m-alert__icon"><i class="la la-check-square"></i></div>
          <div class="m-alert__text">{!! Session::get('success') !!}</div>
          <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
        </div>
      @elseif (Session::has('fail'))
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
          <div class="m-alert__icon"><i class="la la-warning"></i></div>
          <div class="m-alert__text">{!! Session::get('fail') !!}</div>
          <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
        </div>
      @endif
      <div class="m-portlet__body" id="searchresult">
        <table class="table table-striped- table-bordered table-hover table-checkable">
          <thead>
            <tr class="bg-light">
              <th width="10%">{{ trans('statics.rec_id') }}</th>
              <th width="27%">{{ trans('statics.name_dr') }}</th>
              <th width="27%">{{ trans('statics.name_pa') }}</th>
              <th width="27%">{{ trans('statics.name_en') }}</th>
              <th width="10%">{{ trans('global.action') }}</th>
            </tr>
          </thead>
          <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
            @if($record)
              @foreach($record AS $rec)
              <?php $enc_id = encrypt($rec->id); ?>
              <tr>
                <td>{!!$rec->id!!}</td>
                <td>{!!$rec->name_dr!!}</td>
                <td>{!!$rec->name_pa!!}</td>
                <td>{!!$rec->name_en!!}</td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRole("setting_statics","sta_view"))
                          <a class="dropdown-item" href="javascript:void()" onclick="viewFinancial_source('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                        @endif
                        @if(doIHaveRole("setting_statics","sta_edit"))
                          <a class="dropdown-item" href="javascript:void()" onclick="editFinancial_source('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                        @endif
                        @if(doIHaveRole("setting_statics","sta_delete"))
                          <a class="dropdown-item" href="javascript:void()" onclick="deleteFinancial_source('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                        @endif
                        <input class="form-control m-input" type="hidden" value="{!! $posted_value !!}" name="posted_value" id="posted_value">
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
              @endforeach
            @endif
          </tbody>
        </table>
        @if(!empty($record))
          {!!$record->links('pagination')!!}
        @endif
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function()
{text()
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			document.cookie = "no="+$(this).text();
      var code = $('#posted_value').val();
      var dataString = '';
			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&code="+code;
			$.ajax({
              url: '{{ route("viewStatics") }}',
              data: dataString,
              type: 'get',
              beforeSend: function(){
                  $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
              },
              success: function(response)
              {
                  $('#searchresult').html(response);
              }
          }
      );
		}
	});
});
function addFinancial_source(posted_value)
{
  var posted_value = posted_value;
  $.ajax({
      url: '{{ route("addFinancial_source") }}',
      data: {
          "_method"       : 'POST',
          "posted_value"  : posted_value
      },
      type: 'post',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

function editFinancial_source(id,posted_value)
{
  $.ajax({
      url: '{{ route("editFinancial_source") }}',
      data: {
        "_method"      : 'POST',
        "id"           : id,
        "posted_value" : posted_value,
      },
      type: 'post',
      beforeSend: function(){
          $('#showContent').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

function deleteFinancial_source(id,posted_value)
{
  if(confirm('{{ trans('global.conf_msg') }}'))
  {
    $.ajax({
        url: '{{ route("deleteFinancial_source") }}',
        data: {
          "_method"       : 'POST',
          "id"            : id,
          "posted_value"  : posted_value,
        },
        type: 'post',
        beforeSend: function(){
            $('#showContent').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
        },
        success: function(response)
        {
            list_records(posted_value);
        }
    });
  }
}

function list_records(posted_value)
{
  var code = posted_value;
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : code
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

function viewFinancial_source(id,posted_value)
{
  $.ajax({
      url: '{{ route("viewFinancial_source") }}',
      data: {
        "_method"       : 'POST',
        "id"            : id,
        "posted_value"  : posted_value,
      },
      type: 'post',
      beforeSend: function(){
          $('#showContent').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}
</script>
