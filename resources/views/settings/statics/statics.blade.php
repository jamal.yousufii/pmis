@extends('master')
@section('head')
  <title>{{ trans('statics.statics') }}</title>
  <style>
    .table thead th {
      padding: 1rem !important;
        font-size: 16px !important;
    }
    .table tbody td {
      color: #575962 !important;
      padding: 0.75rem !important;
      font-size: 15px !important;
      font-weight: 300 !important;
    }
  </style>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile mb-3">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{ trans('statics.statics') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('home','setting') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!--Begin::Section-->
    <div class="m-portlet mb-3" id="m_portlet">
      <div class="m-portlet__body">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('statics.select') }}: </label>
          <select class="form-control m-input m-input--air select-2" id="static_id" onchange="javascript:bringPageStatic()">
            @if($statics)
              <option value="0">{{ trans('global.select') }}</option>
              @foreach($statics as $item)
                <option value="{!! $item->code !!}-{!! $item->tbl_name !!}">{!! $item->name !!}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>
    </div>
    <!--End::Section-->
  </div>
  <span id="showContent"></span>
@endsection
<script type="text/javascript">
  function bringPageStatic()
  {
    var token = $('meta[name="csrf-token"]').attr('content');
    var code = $('#static_id').val();
    $.ajax({
        url: '{!!URL::route("viewStatics")!!}',
        data: {
            "_method" : 'GET',
            "_token"  : token,
            "code"    : code
        },
        type: 'get',
        beforeSend: function(){
            $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
        },
        success: function(response)
        {
            $('#showContent').html(response);
        }
    });
  }
  $(".select-2").select2({ width: '100%' });
</script>
