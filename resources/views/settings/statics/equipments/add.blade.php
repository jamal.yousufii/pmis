<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('statics.add_equipments') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.name_dr') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_dr" id="name_dr" required>
              <div class="name_dr error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.name_pa') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_pa" id="name_pa" required>
              <div class="name_pa error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.name_en') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_en" id="name_en" required>
              <div class="name_en error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <input class="form-control m-input" type="hidden" value="{!! $posted_value !!}" name="posted_value" id="posted_value">
                  <button type="button" onclick="submitForm();" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="goBack()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
function submitForm()
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','1px solid #ebedf2');
  var formData = new FormData($("#requestForm")[0]);
  var posted_value = $('#posted_value').val();
  $.ajax({
      url         : '{{ route("storeEquipments") }}',
      data        : formData,
      contentType : false,
      type        : 'post',
      success: function(response)
      {
          list_records(posted_value);
      },
      error: function (request, status, error) {
          json = $.parseJSON(request.responseText);
          $.each(json.errors, function(key, value){
              $('.'+key).show();
              $('.'+key).html('<span class="text-danger">'+value+'</span>');
              $('#'+key).css('border-bottom','1px solid #dc3545');
          });
      },
      cache: false,
      processData: false
  });
}

function list_records(posted_value)
{
  var code  = posted_value;
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : code
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

function goBack()
{
  var posted_value = $('#posted_value').val();
  code = "2-pmis.doc_types";
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : posted_value
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}
</script>
