<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{ trans('statics.view_wall') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::record-->
      @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('statics.name_dr') }} : </span></label><br>
                <span>{!!$record->name_dr!!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('statics.name_pa') }} : </span></label><br>
                <span>{!!$record->name_pa!!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('statics.name_en') }} : </span></label><br>
                <span>{!!$record->name_en!!}</span>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <button type="button" onclick="goBack('{!!$posted_value!!}')" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </div>
      @endif
      <!--end::record-->
    </div>
  </div>
</div>
<script type="text/javascript">
function goBack(posted_value)
{
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : posted_value
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}
</script>
