<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              {{ trans('statics.add_employees') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.first_name') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="first_name" id="first_name" required>
              <div class="first_name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.father') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="last_name" id="last_name" required>
              <div class="last_name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('statics.job') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="job" id="job" required>
              <div class="job error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('authentication.section') }} : <span style="color:red;">*</span></label>
              <div id="section" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="section">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($sections)
                    @foreach($sections as $item)
                      <option value="{!!$item->code!!}">{!!$item->{'name_'.$lang}!!}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="section error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <input class="form-control m-input" type="hidden" value="{!! $posted_value !!}" name="posted_value" id="posted_value">
                  <button type="button" onclick="submitForm();" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="goBack()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
function submitForm()
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','1px solid #ebedf2');
  var formData = new FormData($("#requestForm")[0]);
  var posted_value = $('#posted_value').val();
  $.ajax({
      url         : '{{ route("storeEmployee") }}',
      data        : formData,
      contentType : false,
      type        : 'post',
      success: function(response)
      {
          list_records(posted_value);
      },
      error: function (request, status, error) {
          json = $.parseJSON(request.responseText);
          $.each(json.errors, function(key, value){
              $('.'+key).show();
              $('.'+key).html('<span class="text-danger">'+value+'</span>');
              $('#'+key).css('border-bottom','1px solid #dc3545');
          });
      },
      cache: false,
      processData: false
  });
}

function list_records(posted_value)
{
  var code  = posted_value;
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : code
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

function goBack()
{
  var posted_value = $('#posted_value').val();
  code = "2-pmis.doc_types";
  $.ajax({
      url: '{{ route("viewStatics") }}',
      data: {
          "_method" : 'GET',
          "code"    : posted_value
      },
      type: 'get',
      beforeSend: function(){
          $("#showContent").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}
$('.select-2').select2();
</script>
