<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead>
    <tr class="bg-light">
      <th width="10%">{{ trans('statics.rec_id') }}</th>
      <th width="27%">{{ trans('statics.name_dr') }}</th>
      <th width="27%">{{ trans('statics.name_pa') }}</th>
      <th width="27%">{{ trans('statics.name_en') }}</th>
      <th width="10%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($record)
      @foreach($record AS $rec)
        <?php $enc_id = encrypt($rec->id); ?>
        <tr>
          <td>{!!$rec->id!!}</td>
          <td>{!!$rec->name_dr!!}</td>
          <td>{!!$rec->name_pa!!}</td>
          <td>{!!$rec->name_en!!}</td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  @if(doIHaveRole("setting_statics","sta_view"))
                    <a class="dropdown-item" href="javascript:void()" onclick="viewRoof('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                  @endif
                  @if(doIHaveRole("setting_statics","sta_edit"))
                    <a class="dropdown-item" href="javascript:void()" onclick="editRoof('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                  @endif
                  @if(doIHaveRole("setting_statics","sta_delete"))
                    <a class="dropdown-item" href="javascript:void()" onclick="deleteRoof('{!!$enc_id!!}','{!!$posted_value!!}')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                  @endif
                  <input class="form-control m-input" type="hidden" value="{!! $posted_value !!}" name="posted_value" id="posted_value">
                </div>
              </span>
            </span>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
@if(!empty($record))
  {!!$record->links('pagination')!!}
@endif
<script type="text/javascript">
$(document).ready(function()
{
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			document.cookie = "no="+$(this).text();
      var code = $('#posted_value').val();
      var dataString = '';
			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&code="+code;
			$.ajax({
              url: '{{ route("viewStatics") }}',
              data: dataString,
              type: 'get',
              beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
              },
              success: function(response)
              {
                  $('#searchresult').html(response);
              }
          }
      );
		}
	});
});
</script>
