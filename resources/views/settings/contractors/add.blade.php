<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              {{ trans('contractor.add_new_contractor') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('contractor.contractor_name') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="company_name" id="company_name">
              <div class="company_name error-div" style="display:none;"></div>
            </div> 
          </div>
          <div class="row m-form__group_custom pt-3 pl-2 pr-2">
              <div class="col-lg-12 text-title">{{ trans('contractor.primary_contact') }}</div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">  
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_name') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="primary_contact_name" id="primary_contact_name">
              <div class="primary_contact_name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_phone') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="number" name="primary_contact_phone" id="primary_contact_phone">
              <div class="primary_contact_phone error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_email') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="primary_contact_email" id="primary_contact_email">
              <div class="primary_contact_email error-div" style="display:none;"></div>
            </div>
          </div> 
          <div class="row m-form__group_custom pt-3 pl-2 pr-2">
            <div class="col-lg-12 text-title">{{ trans('contractor.alternative_contact') }}</div>
          </div> 
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_name') }} :</label>
              <input class="form-control m-input" type="text" name="alternative_contact_name" id="alternative_contact_name">
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_phone') }} :</label>
              <input class="form-control m-input" type="number" name="alternative_contact_phone" id="alternative_contact_phone">
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('contractor.contact_email') }} :</label>
              <input class="form-control m-input" type="text" name="alternative_contact_email" id="alternative_contact_email">
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('contractors.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
