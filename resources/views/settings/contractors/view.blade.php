
    <div class="m-portlet m-portlet--full-height ">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">{{ trans('contractor.view_contractor') }}</h3>
					</div>
				</div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('contractors',session('current_department')) }}">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
          </ul>
				</div>
      </div>
      <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('contractor.contractor_name') }} :</label><br>
              <span>{!!$record->company_name!!}</span>
            </div>
          </div>
          <div class="row m-form__group_custom pt-3">
            <div class="col-lg-12 text-title">{{ trans('contractor.primary_contact') }}</div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">  
            
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_name') }} :</label><br>
              <span>{!!$record->primary_contact_name!!}</span>
            </div>
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_phone') }} :</label><br>
              <span>{!! $record->primary_contact_phone !!}</span>
            </div>
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_email') }} :</label><br>
              <span>{!!$record->primary_contact_email!!}</span>
            </div>
          </div>
          <div class="row m-form__group_custom pt-4">
            <div class="col-lg-12 text-title">{{ trans('contractor.alternative_contact') }}</div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_name') }} :</label><br>
              <span>{!!$record->alternative_contact_name!!}</span>
            </div>
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_phone') }} :</label><br>
              <span>{!!$record->alternative_contact_phone !!}</span>
            </div>
            <div class="col-lg-4 col-md-4">
              <label class="title-custom">{{ trans('contractor.contact_email') }} :</label><br>
              <span>{!!$record->alternative_contact_email!!}</span>
            </div>
          </div>
        </div>
      </div>
    </div>