<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('portfolios.edit_portfolios') }}</h3>
          </div>
        </div>
      </div>
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">  
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('portfolios.portfolios_name_dr') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!! $record->name_dr !!}" name="name_dr" id="name_dr">
                <div class="name_dr error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('portfolios.portfolios_name_pa') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!! $record->name_pa !!}" name="name_pa" id="name_pa">
                <div class="name_pa error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('portfolios.portfolios_name_en') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!! $record->name_en !!}" name="name_en" id="name_en">
                <div class="name_en error-div" style="display:none;"></div>
              </div>
            </div>  
            <div class="form-group m-form__group row m-form__group_custom">  
              <div class="col-lg-6">
                <label class="title-custom">{{ trans('portfolios.departments') }} : <span style="color:red;">*</span></label>
                <div id="department_id" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2" name="department_id" id="department" onchange="viewRecord('{{route('setting_portfolios.projects')}}','id='+this.value,'POST','project')">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($departments)
                      @foreach($departments as $item)
                        <option value="{!!$item->id!!}" <?=$record->department_id==$item->id? 'selected':'' ?> >{!!$item->name!!}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="department_id error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-6">
                <?php
                $selected_projects = array();
                if($record->PortfoliosSub) {
                  foreach($record->PortfoliosSub as $item)
                  {
                    $selected_projects[] = $item->project_id;  
                  }  
                }
                ?>
                <label class="title-custom">{{ trans('portfolios.projects') }} : <span style="color:red;">*</span></label>
                <div id="project_id" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2" name="project_id[]" id="project" multiple>
                    @if($projects) 
                      @foreach($projects as $item)
                        <option value="{!!$item->id!!}" <?=in_array($item->id,$selected_projects)? 'selected':'' ?> >{!!$item->name!!}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="project_id error-div" style="display:none;"></div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <button type="button" onclick="doEditRecord('{{route('setting_portfolios.update',$enc_id)}}','requestForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
