
<div class="m-portlet m-portlet--full-height ">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('portfolios.view_portfolios') }}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('setting_portfolio') }}">
                        <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('portfolios.portfolios_name_dr') }} :</label><br>
                    <span>{!!$record->name_dr!!}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('portfolios.portfolios_name_pa') }} :</label><br>
                    <span>{!!$record->name_pa!!}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('portfolios.portfolios_name_en') }} :</label><br>
                    <span>{!!$record->name_en!!}</span>
                </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                        <tr class="bg-dark white-color">
                            <th width="10%">{{ trans('global.number') }}</th>
                            <th width="20%">{{ trans('portfolios.departments') }}</th>
                            <th width="20%">{{ trans('portfolios.projects') }}</th>
                            <th width="10%">{{ trans('global.action') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($record->PortfoliosSub) 
                            @foreach($record->PortfoliosSub as $item)
                                <tr>
                                    <td class="border-bottom border-top-0">
                                        <span class="kt-widget4__username pt-6">{{ $loop->iteration }}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                            <span class="kt-widget4__username pt-6">{{ $record->department->{'name_'.$lang} }}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <span class="kt-widget4__username pt-6">{{ $item->project->name }}</span>
                                    </td>
                                    <td class="border-bottom border-top-0">
                                        <span class="dtr-data">
                                            <span class="dropdown">
                                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                    <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('setting_portfolios.destroy',encrypt($item->id))}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                                                </div>
                                            </span>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        @endif   
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
</div>