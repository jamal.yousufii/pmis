<!DOCTYPE html>
<html lang="en">
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>{{ trans('home.title-4') }} | {{ trans('login.login') }}</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--begin:: Theme Styles -->
		<link href="{!!asset('public/assets/vendors/base/vendors.bundle.rtl.css')!!}" rel="stylesheet" type="text/css" />
		<link href="{!!asset('public/assets/demo/demo12/base/style.bundle.rtl.css')!!}" rel="stylesheet" type="text/css" />
		<link href="{!!asset('public/assets/font/font.css')!!}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{!!asset('public/img/index.png')!!}" />
		<style>
		.m-login.m-login--1 .m-login__wrapper .m-login__form .m-login__form-action .btn {
            padding: 1rem 2.5rem !important;
        }
        html {
			margin: 0;
			padding: 0;
			height: 100%;
			background: #60a3bc !important;
		}
		.user_card {
			height: 400px;
			width: 350px;
			margin-top: auto;
			margin-bottom: auto;
			background: #1f164294;
			position: relative;
			display: flex;
			justify-content: center;
			flex-direction: column;
			padding: 10px;
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;

		}
		.brand_logo_container {
			position: absolute;
			height: 170px;
			width: 170px;
			top: -75px;
			border-radius: 50%;
            background: #1f164287;
			padding: 10px;
			text-align: center;
		}
		.brand_logo {
			height: 150px;
			width: 150px;
			border-radius: 50%;
			border: 2px solid white;
		}
		.form_container {
			margin-top: 100px;
		}
		.login_btn {
			width: 100%;
			background: #c0392b !important;
			color: white !important;
		}
		.login_btn:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.login_container {
			padding: 0 2rem;
		}
		.input-group-text {
			background: #1f164270 !important;
			color: white !important;
			border: 0 !important;
			border-radius: 0.25rem 0 0 0.25rem !important;
		}
		.input_user,
		.input_pass:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
			background-color: #c0392b !important;
        }
        .m-header-custome{
            background-color: #1f1642 !important;
            height: 90px !important;
        }
        img {
            pointer-events: none !important;
        }
        </style>
	</head>
	<!-- end::Head -->
	<!-- begin::Body -->
        <header id="m_header" class="m-grid__item m-header m-header-custome " m-minimize-offset="200" m-minimize-mobile-offset="200" >
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" >
                            <div class="m-grid__item pt-3">
                                <table width="100%" class="text-center">
                                    <tr>
                                        <td>
                                            <h6 class="m-login__welcome custtome_heading_title" style="color:#e8c311;font-size:1.6rem;font-weight:650;">{{ trans('home.title-1') }}</h6>
                                            <h5 class="m-login__welcome" style="color:#e8c311;font-size:1.3rem;font-weight:650;">{{ trans('home.title-2') }}</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
		<!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page m-grid__item m-grid__item--fluid m-grid m-login m-login--1 m-login--signin" id="m_login"
        style="background-image: url({{asset('public/img/login.jpg') }}); height: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;"" >
            <div class="d-flex h-100 justify-content-center" style="padding-top: 10%">
                <div class="user_card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="public/img/logo.png" class="brand_logo" alt="Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <form class="m-login__form m-form" method="post" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group m-form__groupm-login__head">
                                <h5 class="m-login__welcome" style="color:#f3c106;;font-size:1.6rem;font-weight:270;">{{ trans('home.login-title') }}</h5>
                                <h5 class="m-login__welcome text-center" style="color:#f3c106;;font-size:1.6rem;font-weight:270;">{{ trans('login.login') }}</h5>
                            </div>
                            <div class="form-group m-form__group">
                                <div class="input-group m-input-group m-input-group--square">
                                    <input id="email" type="email" style="" placeholder="{{ trans('login.email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group m-form__group " style="padding-top: 0px !important">
                                <div class="input-group m-input-group m-input-group--square">
                                    <input id="password" type="password" style="" placeholder="{{ trans('login.password') }}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                </div>
                                <div class="m-0 text-warning text-center">
                                    @if($errors->has('email'))
                                        <strong>{{ $errors->first('email') }}</strong>
                                    @endif
                                </div>
                            </div>
                            {{-- <div class="row m-login__form-sub">
                                <div class="col text-center">
                                    <a href="javascript:;" id="m_login_forget_password" style="color:#fff" class="m-link">{{ trans('login.forgot_password') }}</a>
                                </div>
                            </div> --}}
                            <div class="m-login__form-action" style="text-align: center">
                                <button type="submit"  class="btn btn-dark m-btn m-btn--square btn-sm btn-block m-btn--air text-warning py-2" style="background: #1f1642; border-color:#1f1642; font-size:1.1rem">{{ trans('login.login') }}</button>
                            </div>
                        </form>
                    </div>
                    <div class="m-login__forget-password">
                        <div class="m-login__head">
                            <h3 class="m-login__title" style="color:#fff">{{ trans('login.forgot_password') }}</h3>
                            <div class="m-login__desc" style="color:#fff">{{ trans('login.enter_email') }}</div>
                        </div>
                        <form class="m-login__form m-form" action="">
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="text" style="border-radius: 10px;padding:10px" placeholder="{{ trans('login.email') }}" name="email" id="m_email" autocomplete="off">
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_forget_password_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air">{{ trans('login.new_password') }}</button>
                                <button id="m_login_forget_password_cancel" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air">{{ trans('login.cancel') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>

		<!-- end:: Page -->
		<!--begin::Page Scripts -->
		<script src="{!!asset('public/assets/vendors/base/vendors.bundle.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('public/assets/demo/demo12/base/scripts.bundle.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('public/assets/snippets/custom/pages/user/login.js')!!}" type="text/javascript"></script>
		<!--end::Page Scripts -->
	</body>
	<!-- end::Body -->
</html>
<script>
$('#signin_submit').click(function(){
  $(this).addClass("m-loader m-loader--right m-loader--light").attr("disabled",!0);
});
</script>
