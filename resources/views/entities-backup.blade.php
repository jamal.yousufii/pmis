@extends('master')
@section('head')
  <title>{{ trans('login.pmis') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">@include('breadcrumb_nav')</div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row"> 
    @if($sections)
      @foreach($sections->sections as $item)
        <div class="col-xl-3 col-lg-3 col-xs-3 col-md-6 main">
          @if(check_my_section(array(decrypt($enc_depId)),$item->code))
            <a class="main-link" href="{{ route($item->url_route, $enc_depId) }}">
              <div class="card col-xl-10 col-lg-10 col-xs-10 col-md-12 offset-xl-1 offset-lg-1 offset-xs-1 pt-2 rounded">
                <div class="card-body d-card-body">
                  <div class="card-block">
                    <div class="media">
                      <div class="media-right media-middle">
                        <div class="d-icon pt-2">
                            <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2rem;"></i></i>
                        </div>
                        <!-- @if($item->code!='pmis_reports' and $item->code!='pmis_graph' and $item->code!='pmis_porfolio' and $item->code!='pmis_tasks' and $item->code!='pmis_change_request' and $item->code!='pmis_payments') -->
                            <div class="text-xs-center text-xs-left">
                                <h3 class="pink lead pt-3">
                                    <!-- @if($item->code=='pmis_request')
                                        {{ trans('global.total_requests') }}: 
                                    @else
                                        {{ trans('global.total_projects') }}: 
                                    @endif
                                    <span class="m-badge m-badge--info m-badge--wide">{{ GetAllRecords($item->tables,$item->code,decrypt($enc_depId),$item->section_role) }}</span> -->
                                    <span class="dashboard_title_entity">{{ $item->{'name_'.$lang} }}</span>
                                </h3>
                            </div>
                        <!-- @endif -->
                        <!-- <div class="media-body d-media-body_entity text-xs-left">
                          <span class="dashboard_title_entity">{{ $item->{'name_'.$lang} }}</span>
                        </div> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          @else
            <div class="card-disabled col-xl-10 col-lg-10 col-xs-10 col-md-12 offset-xl-1 offset-lg-1 offset-xs-1 rounded">
              <div class="card-body d-card-body">
                <div class="card-block">
                  <div class="media">
                    <div class="media-right media-middle">
                      <div class="d-icon pt-2">
                        <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2rem;"></i></i>
                      </div>
                      <!-- @if($item->code!='pmis_reports' and $item->code!='pmis_graph' and $item->code!='pmis_portfolio' and $item->code!='pmis_tasks') -->
                            <div class="text-xs-center text-xs-left">
                                <h3 class="pink lead pt-3">
                                    <!-- @if($item->code=='pmis_request')
                                        {{ trans('global.total_requests') }}: 
                                    @else
                                        {{ trans('global.total_projects') }}: 
                                    @endif
                                    <span class="m-badge m-badge--info m-badge--wide">0</span> -->
                                    <span class="dashboard_title_entity">{{ $item->{'name_'.$lang} }}</span>
                                </h3>
                            </div>
                        <!-- @endif -->
                      <!-- <div class="media-body d-media-body_entity text-xs-left">
                        <span class="dashboard_title">{{ $item->{'name_'.$lang} }}</span>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        </div>
      @endforeach
    @endif
  </div>
@endsection
