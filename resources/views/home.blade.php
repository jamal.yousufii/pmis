@extends('master')
@section('head')
  <title>{{ trans('login.pmis') }}</title>
  <style>
    .m-portlet {
        margin-bottom: 1.8rem !important;
        background: #f2f3f8 !important;
    }
    .m-portlet-custom{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19) !important;
        background: #ffffff !important;
    }
    .btn-accent {
        background-color: #FFFFFF !important;
    }
    .bg-accent{
        background: #36a3f799 !important;
        opacity: 0.7;
    }
    .btn-title{
        font-size: 1.1rem !important;
        font-weight: 600;
        color: #000;
        opacity: 1;
    }
    .clearfix:before {
        content: " ";
        display: table;
    }
    .m-portlet-custom-hover:hover {
        box-shadow: 0px 2px 10px #4474B7 !important;
    }
    .icon-color{
        color: #36a3f7;
    }
    .icon-btn {
        padding: 5px 0 0 !important;
        border: 0px !important;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%) !important;
        background: #ffffff !important;
    }
  </style>
@endsection
@section('content')
    <!-- Dashboard => Title: Start -->
    <div class="m-subheader pt-0 pb-2">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator" style="font-family: B Nazanin;">{{ trans('home.title-4') }}</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ route('home',session('current_mod')) }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon fas fa-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="{{ route('home',session('current_mod')) }}" class="m-nav__link">
                            <span class="m-nav__link-text">{{ trans('home.dashboard') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="m-topbar__nav m-nav m-nav--inline">
                @if(session('current_mod')=="pmis")
                    @if(is_admin())
                        <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" data-toggle="modal" data-target="#projectProgressDashboard">
                            <a href="{{ route('home.dashboard') }}" class="icon-btn" style="text-decoration: none">
                                <i class="fab fa-accusoft" style="font-size:1.4rem"></i>
                                <div class="px-3 m-widget21__title">{{trans('home.project_dashboard')}}</div>
                            </a>
                        </li>
                    @endif
                    <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                        <a href="{!! route('calendarTasks') !!}" class="icon-btn" style="text-decoration: none">
                            <i class="fa fa-tasks" style="font-size:1.4rem"></i>
                            <div> {{ trans('tasks.my_tasks') }} </div>
                            <span class="badge badge-info"> {!! $tasks !!} </span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <!-- Main Page Dashboard => Title: End -->
    <!-- Main Page Dashboard => Directorates & Sections: Start -->
    <div class="m-portlet--unair py-1">
        <div class="m-portlet__body m-portlet__body--no-padding" style="background: #f2f3f8">
            <div class="row mt-4" style="background: #f2f3f8">
                @if(session('current_mod')=='pmis')
                    @include('pmis')
                @elseif(session('current_mod')=='npmis')
                    @include('npmis')
                @else
                    @include('settings')
                @endif
            </div>
        </div>
    </div>
    <!-- Main Page Dashboard => Directorates & Sections: End -->
    <!-- Main Page Dashboard => Directorates & Sections: Start -->
    @if(session('current_mod')=='pmis')
        @include('dashboard')
    @endif
    <!-- Main Page Dashboard => Directorates & Sections: End -->
@endsection
@section('js-code')
    <script type="text/javascript">
        $(document).ready(function()
        {
            var projectInEachSections = echarts.init(document.getElementById('projectInEachSections'));
            var projectInEachSectionsOption = {
                tooltip : {
                    show: true
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : {
                    type : 'category',
                    data : [
                        '{{ trans('home.under_tech_documentation') }}',
                        '{{ trans('home.under_procurement') }}',
                        '{{ trans('global.progress') }}',
                        '{{ trans('global.completed') }}',
                        '{{ trans('global.stoppedـpro') }}',
                    ],
                    axisTick : {
                        show : true,
                        alignWithLabel : true,
                        autoSkip : false
                    },
                    axisLabel : {
                        @if($lang=='en')
                            rotate : -25,
                        @else
                            rotate : 25,
                        @endif
                    }
                },
                yAxis : {
                    type:'value',
                },
                series : [
                    {
                        data : [
                                {

                                    value: "{{ $under_tech_doc }}",
                                    itemStyle: {color:'#00c5dc', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $total_procurements }}",
                                    itemStyle: {color:'#5867dd', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $construction_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $completed_projects }}",
                                    itemStyle: {color:'#34bfa3', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $stopped_projects }}",
                                    itemStyle: {color:'#f4516c', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                            ],
                        type : 'bar',
                    }
                ]
            };
            projectInEachSections.setOption(projectInEachSectionsOption);

            var projectByZone = echarts.init(document.getElementById('projectByZone'));
            var projectByZoneOption = {
                tooltip : {
                    show: true
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : {
                    type : 'category',
                    data: [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                '{{ $item->zone }}',
                            @endforeach
                        @endif
                    ],
                    axisTick : {
                        show : true,
                        alignWithLabel : true,
                        autoSkip : false
                    },
                    axisLabel : {
                        rotate : 35,
                    }
                },
                yAxis : {
                    type:'value',
                },
                series : [
                    {
                        data : [
                            @if($projectByZone)
                                @foreach($projectByZone as $item)
                                    @php
                                    $color= Config::get('static.zone.'.$item->zcode.'.color');
                                    @endphp
                                    {
                                        value: '{{ $item->total_projects }}',
                                        itemStyle: {color:'{{$color}}', opacity: 0.7},
                                    },
                                @endforeach
                            @endif
                        ],
                        type : 'bar',
                    }
                ]
            };
            projectByZone.setOption(projectByZoneOption);

            var moneyByZone = echarts.init(document.getElementById('moneyByZone'));
            var moneyByZoneOption = {
                tooltip : {
                    show: true
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : {
                    type : 'category',
                    data: [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                '{{ $item->zone }}',
                            @endforeach
                        @endif
                    ],
                    axisTick : {
                        show : true,
                        alignWithLabel : true,
                        autoSkip : false
                    },
                    axisLabel : {
                        rotate : 35,
                    }
                },
                yAxis : {
                    type:'value',
                },
                series : [
                    {
                        data : [
                            @if($projectByZone)
                                @foreach($projectByZone as $item)
                                    @php
                                    $color= Config::get('static.zone.'.$item->zcode.'.color');
                                    @endphp
                                    {
                                        @if(isset($project_budget[$item->zcode]))
                                            value: '{{ $project_budget[$item->zcode] }}',
                                        @else
                                            value: '0',
                                        @endif
                                        itemStyle: {color:'{{$color}}', opacity: 0.7},
                                    },
                                @endforeach
                            @endif
                        ],
                        type : 'bar',
                    }
                ]
            };
            moneyByZone.setOption(moneyByZoneOption);

            var projectBySection = echarts.init(document.getElementById('projectBySection'));
            var projectBySectionOption = {
                tooltip : {
                    show: true
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : {
                    type : 'category',
                    data : [
                        '{{ trans('plans.plan') }}',
                        '{{ trans('surveys.surveys') }}',
                        '{{ trans('designs.designs') }}',
                        '{{ trans('estimation.estimation') }}',
                        '{{ trans('implements.implements') }}',
                        '{{ trans('procurement.procurement') }}',
                        '{{ trans('global.progress') }}',
                        '{{ trans('global.completed') }}',
                        '{{ trans('global.stoppedـpro') }}',
                    ],
                    axisTick : {
                        show : true,
                        alignWithLabel : true,
                        autoSkip : false
                    },
                    axisLabel : {
                        rotate : 35,
                    }
                },
                yAxis : {
                    type:'value',
                },
                series : [
                    {
                        data : [
                                {

                                    value: "{{ $plan_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $survey_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $design_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $estimation_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $checklist_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $total_procurements }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $construction_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {

                                    value: "{{ $completed_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                                {
                                    value: "{{ $stopped_projects }}",
                                    itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                },
                            ],
                        type : 'bar',
                    }
                ]
            };
            projectBySection.setOption(projectBySectionOption);
        });
    </script>
@endsection
