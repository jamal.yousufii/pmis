@if($departments)
    @foreach($departments as $item)
        <!-- Check for contractor department -->
        @php
        if($item->code=='dep_06')
            { $url = 'bringContractors'; }
        else
            { $url = 'bringSections'; }
        @endphp
        @if(check_my_departments($item->id) and session('current_mod')=='pmis')
            <div class="col-lg-4">
                <a class="main-link-div" href="{{ route($url,encrypt($item->id)) }}">
                    <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 m-portlet-custom-hover rounded" style="height:70px;">
                        <div class="m-widget21 info">
                            <div class="row">
                                <div class="col">
                                    <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                        <span class="m-widget21__icon px-3">
                                            <i class="icon-bag2 icon-color font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                        </span>
                                        <div class="m-widget21__info">
                                            <span class="m-widget21__title">{{ $item->name }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @elseif(!check_my_departments($item->id) and session('current_mod')=='pmis')
            <div class="col-lg-4 disabled ain-link-div-disabled">
                <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 rounded" style="height:70px;">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                    <span class="m-widget21__icon">
                                        <i class="icon-bag2 icon-color font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">{{$item->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif