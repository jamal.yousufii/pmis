<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead>
    <tr class="bg-light">
      <th width="10%">{{ trans('authentication.rec_id') }}</th>
      <th width="10%">{{ trans('authentication.code') }}</th>
      <th width="20%">{{ trans('authentication.name_dr') }}</th>
      <th width="20%">{{ trans('authentication.name_pa') }}</th>
      <th width="20%">{{ trans('authentication.name_en') }}</th>
      <th width="10%">{{ trans('authentication.url') }}</th>
      <th width="10%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
      @foreach($records AS $rec)
        <?php $enc_id = encrypt($rec->id); ?>
        <tr>
          <td>{!!$rec->id!!}</td>
          <td>{!!$rec->code!!}</td>
          <td>{!!$rec->name_dr!!}</td>
          <td>{!!$rec->name_pa!!}</td>
          <td>{!!$rec->name_en!!}</td>
          <td>{!!$rec->url!!}</td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  @if(doIHaveRole("auth_app","app_view"))
                    <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{route('modules.show',$enc_id)}}','','GET','response_div')"><i class="la la-file-text"></i>{{ trans('global.view') }}</a>
                  @endif
                  @if(doIHaveRole("auth_app","app_edit"))
                    <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('modules.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                  @endif
                  @if(doIHaveRole("auth_app","app_delete"))
                    <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('modules.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                  @endif
                </div>
              </span>
            </span>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
      $('.pagination a').on('click', function(event) {
          event.preventDefault();
          if ($(this).attr('href') != '#') {
              document.cookie = "no="+$(this).text();
              var dataString = '';
              item = $('#keyWord').val();
              dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&item="+item+"&type=search";
                      $.ajax({
                      url: '{{ route("filterModule") }}',
                      data: dataString,
                      type: 'get',
                      beforeSend: function(){
                          $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                      },
                      success: function(response)
                      {
                          $('#searchresult').html(response);
                      }
                  }
              );
          }
      });
  });
</script>