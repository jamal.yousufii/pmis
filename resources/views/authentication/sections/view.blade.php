<!--begin::record-->
@if($record)
  @include('authentication.sections.share')
  <div class="row">
    <div class="col-lg-12">
      <div class="m-portlet">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('authentication.view_sections') }}</h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              @if(doIHaveRole("auth_section","sec_add"))
                <!-- Include Approval Modal -->
                <li class="m-portlet__nav-item">
                  <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="" data-toggle="modal" data-target="#ShareModal">
                    <span><i class="la la-cart-plus"></i> <span>{{ trans('authentication.share_sections') }}</span></span>
                  </a>
                </li>
              @endif 
              <li class="m-portlet__nav-item">
                  <a href="#" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                      <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                  </a>
              </li>
            </ul>
          </div>
        </div>      
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.name_dr') }} :</span></label><br>
                <span>{!! $record->name_dr !!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.name_pa') }} :</span></label><br>
                <span>{!! $record->name_pa !!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.name_en') }} :</span></label><br>
                <span>{!! $record->name_en !!}</span>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.department') }} :</span></label><br>
                @if($record->section_deps)
                  @foreach($record->section_deps as $item)
                    <span>{{ $item->departments->{'name_'.$lang} }}</span><br>
                  @endforeach
                @endif
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.module') }} :</span></label><br>
                <span>{!! $record->modules['name_'.$lang] !!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.code') }} :</span></label><br>
                <span>{!! $record->code !!}</span>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4 col-md-4">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.route') }} :</span></label><br>
                <span>{!! $record->url_route !!}</span>
              </div>
              <div class="col-lg-8 col-md-8">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.description') }} :</span></label><br>
                <span>{!! nl2br($record->description) !!}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
          <div class="m-portlet__head-tools">
              <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                          <h3 class="m-portlet__head-text">{{ trans('authentication.share_sections') }}</h3>
                      </div>
                  </div>
                  <div class="m-portlet__head-tools">
                      <ul class="m-portlet__nav">
                          <li class="m-portlet__nav-item">
                              <a class="btn btn-secondary btn-sm border-dark pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
          <div class="m-portlet__body table-responsive">
            <div class="form-group m-form__group row m-form__group_custom">
              <table class="table table-bordered">
                <thead>
                    <tr class="bg-dark white-color">
                        <th width="10%">{{ trans('global.number') }}</th>
                        <th width="40%">{{ trans('authentication.section') }}</th>
                        <th width="40%">{{ trans('authentication.module') }}</th>
                        <th width="10%">{{ trans('global.delete') }}</th>
                    </tr>
                </thead>
                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                  @if($allowed_sections) 
                    @foreach($allowed_sections as $item)
                      <tr>
                        <td class="border-bottom border-top-0">
                            <span class="kt-widget4__username pt-6">{{ $loop->iteration }}</span>
                        </td>
                        <td class="border-bottom border-top-0">
                            <span class="kt-widget4__username pt-6">{{ $item->allowedSections->{'name_'.$lang} }}</span>
                        </td>
                        <td class="border-bottom border-top-0">
                            <span class="kt-widget4__username pt-6">{{ $item->allowedSections->modules->{'name_'.$lang} }}</span>
                        </td>
                        <td class="border-bottom border-top-0">
                          <a href="javascript:void()" class="btn btn-info btn-sm" onclick="destroy('{{route('sections.distroySharing',encrypt($item->id))}}','','GET','response_div')">
                              <i class="flaticon-delete"></i>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>  
@endif
<!--end::record-->