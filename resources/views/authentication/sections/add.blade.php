<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{ trans('authentication.add_sections') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.name_dr') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_dr" id="name_dr">
              <div class="name_dr error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.name_pa') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_pa" id="name_pa">
              <div class="name_pa error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.name_en') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="name_en" id="name_en">
              <div class="name_en error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.department') }} : <span style="color:red;">*</span></label>
              <div id="department_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="department_id[]" multiple>
                  @if($departments)
                    @foreach($departments as $key => $value)
                      <optgroup label="{!!$key!!}">
                        @foreach($value as $item)
                          @php $exp_item = explode('-',$item) @endphp
                          <option value="{!!$exp_item[0]!!}">{!!$exp_item[1]!!}</option>
                        @endforeach
                      </optgroup>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="department_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.module') }} : <span style="color:red;">*</span></label>
              <div id="module_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="module_id">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($modules)
                    @foreach($modules as $item)
                      <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="module_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.code') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="code" id="code">
              <div class="code error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('authentication.route') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" value="" name="url_route" id="url_route">
              <div class="url_route error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-8">
              <label class="title-custom">{{ trans('authentication.description') }} : </label>
              <input class="form-control m-input" type="text" value="" name="description">
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('sections.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
