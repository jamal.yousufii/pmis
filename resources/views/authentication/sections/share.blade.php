<!-- Approval Start-->
<div id="ShareModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="ShareModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header profile-head bg-color-dark">
            <div class="m-portlet__head-caption px-2">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text text-white">{{ trans('authentication.share_sections') }}</h3>
                </div>
            </div>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
        </div>
        <!--begin::Form-->
        <form class="" enctype="multipart/form-data" id="sharingForm" method="post">
            <div class="m-3 m-scrollable border border-secondary" style="max-height: 350px; overflow:auto" data-mobile-height="200">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-dark white-color">
                            <th width="15%">{{ trans('global.number') }}</th>
                            <th width="40%">{{ trans('authentication.section') }}</th>
                            <th width="30%">{{ trans('authentication.module') }}</th>
                            <th width="15%">{{ trans('global.choose') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($sections) 
                            @foreach($sections as $item)
                                @if($item->id!=decrypt($enc_id))
                                    <tr>
                                        <td class="border-bottom border-top-0">
                                            <span class="kt-widget4__username pt-6">{{ $loop->iteration }}</span>
                                        </td>
                                        <td class="border-bottom border-top-0">
                                            <span class="kt-widget4__username pt-6">{{ $item->{'name_'.$lang} }}</span>
                                        </td>
                                        <td class="border-bottom border-top-0">
                                            <span class="kt-widget4__username pt-6">{{ $item->modules->name_dr }}</span>
                                        </td>
                                        <td class="border-bottom border-top-0">
                                            <div class="m-checkbox-list">
                                                <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                                    <input type="checkbox" name="section_id[]" id="{{$item->id}}" value="{{$item->id}}" onchange="CheckForSelected(this.id,'submitBtn')" @if(in_array($item->id,$selected)) checked @endif /> <span style="transform: rotate(90deg);"></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif   
                    </tbody> 
                </table>
            </div>
            <div class="row col-lg-12 m-3">
                <div class="m-form__actions">
                    <input type="hidden" name="id" id="id" value="{!!$enc_id!!}"/>
                    <input type="hidden" name="code" id="code" value="{!!$record->code!!}"/>
                    <button type="button" id="submitBtn" disabled="disabled" onclick="storeRecord('{{route('sections.sharing')}}','sharingForm','POST','response_div',redirectFunction,false,this.id);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
  </div>
</div>