@if($departments)
  @foreach($departments as $item)
    <div class="col-lg-2 px-1">
      <div class="clearfix m-portlet py-3 px-3" id="div_{{ $item->id }}">
        <div class="m-section__content d-inline">
          <div class="m-checkbox-list">
            <label class="m-checkbox m-checkbox--state-brand">
              <input type="checkbox" name="departments[]" onclick="setBackground('div_{{ $item->id }}','dep_{{ $item->id }}','bg-warning');getDepartmentModules('{{route('getModulesByDepartments')}}','dep_{{ $item->id }}','POST','dep_content','0')" value="{{ $item->id }}" id="dep_{{ $item->id }}"> {{ $item->name }}
              <span class="rotate"></span>
            </label>
          </div>
        </div>
      </div> 
    </div>
  @endforeach
@endif