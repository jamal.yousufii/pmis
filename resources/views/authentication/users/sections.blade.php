@if($sections)
  <div class="p-3 px-4">
    @foreach($sections as $key => $value)
      <?php $module = explode('-',$key) ?>
      <label class="title-custom">{!!$module[1]!!}</label>
      @foreach($value as $item)
        <?php $section = explode('-',$item) ?>
        <div class="m-checkbox-list <?=in_array($section[0],$selectedSec) ? 'bg-warning' : ''?>" id="div_sec_{{$department_id}}_{{$section[0]}}">
          <label class="m-checkbox m-checkbox--state-brand">
            <input type="checkbox" name="sections_{{ $department_id }}[]" id="sec_{{$department_id}}_{{$section[0]}}" <?=in_array($section[0],$selectedSec) ? 'checked' : ''?> value="{{ $section[0] }}" onclick="setBackground('div_sec_{{$department_id}}_{{$section[0]}}','sec_{{$department_id}}_{{$section[0]}}','bg-warning');getSectionRoles('{{route('getSectionRoles',encrypt($department_id))}}','sections_{{ $department_id }}[]','GET','role_div_{{$department_id}}','{{$user_id}}')"> {!!$section[1]!!}
            <span class="rotate"></span>
          </label>
        </div>
      @endforeach
    @endforeach
  </div>
@endif