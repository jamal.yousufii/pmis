                          
<option value="">{{ trans('global.select') }}</option>
@if($departments)
  @foreach($departments as $item)
    <option value="{{ $item->id }}">{{ $item->name }}</option>
  @endforeach
@endif