@if($modules)
  <div class="row" id="dep_content_{{$department_id}}">
    <div class="col-lg-12 bg-dark p-1 px-2 mb-1">
      <span class="text-white">{{ getDepartmentNameByID($department_id,$lang) }}</span>
    </div>
    <div class="col-lg-4">
      <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
      <div class="m-portlet">
        <div class="m-demo__preview p-3 px-4" style="height: 180px">
          @if($modules)
            @foreach($modules as $item)
              <div class="m-checkbox-list" id="div_mod_{{ $department_id }}_{{ $item->id }}">
                <label class="m-checkbox m-checkbox--state-brand">
                  <input type="checkbox" name="modules_{{ $department_id }}[]" id="mod_{{ $department_id }}_{{ $item->id }}" value="{{ $item->id }}" onclick="setBackground('div_mod_{{ $department_id }}_{{ $item->id }}','mod_{{ $department_id }}_{{ $item->id }}','bg-warning');getModuleSections('{{route('getModuleSections',encrypt($department_id))}}','modules_{{$department_id}}[]','GET','section_div_{{$department_id}}','{{$user_id}}')"> {!!$item->name!!}
                  <span class="rotate"></span>
                </label>
              </div>
            @endforeach
          @endif
        </div>
      </div> 
    </div>
    <div class="col-lg-4">
      <label class="title-custom">{{ trans('authentication.section') }} :</label><br>
      <div class="m-portlet">
        <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="section_div_{{$department_id}}">

        </div>
      </div> 
    </div>
    <div class="col-lg-4">
      <label class="title-custom">{{ trans('authentication.role') }} :</label><br>
      <div class="m-portlet">
        <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="role_div_{{$department_id}}">

        </div>
      </div>
    </div>
  </div>
@endif
