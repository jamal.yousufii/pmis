@if($roles)
  <div class="p-3 px-4">
    @foreach($roles as $key => $value)
      <label class="title-custom">{!!$key!!}</label>
      @foreach($value as $item)
        <?php $role = explode('-',$item) ?>
        <div class="m-checkbox-list <?=in_array($role[0],$selectedRol) ? 'bg-warning' : ''?>" id="div_sec_{{ $role[0] }}">
          <label class="m-checkbox m-checkbox--state-brand">
            <input type="checkbox" name="roles_{{$department_id}}[]" id="sec_{{ $role[0] }}" value="{{ $role[0] }}"  <?=in_array($role[0],$selectedRol) ? 'checked' : ''?> onclick="setBackground('div_sec_{{ $role[0] }}','sec_{{ $role[0] }}','bg-warning');"> {!!$role[1]!!}
            <span class="rotate"></span>
          </label>
        </div>
      @endforeach
    @endforeach
  </div>
@endif