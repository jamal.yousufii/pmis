<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.view_user') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="#" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
          </ul>
        </div>
      </div>
      <!--begin::record-->
      @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body" style="background: #f7f8fa;">
          <div class="row">
            <div class="col-xl-10">
              <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.departments.organization') }} :</span></label><br>
                      <span>{!!$record->organization['name_'.$lang]!!}</span>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.user_dep') }} :</span></label><br>
                      <span>{!!$record->department['name_'.$lang]!!}</span>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.name') }} :</span></label><br>
                      <span>{!!$record->name!!}</span>
                    </div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.father') }} :</span></label><br>
                      <span>{!!$record->father!!}</span>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.phone_no') }} :</span></label><br>
                      <span>{!!$record->phone_number!!}</span>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.position') }} :</span></label><br>
                      <span>{!!$record->position!!}</span>
                    </div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.email') }} :</span></label><br>
                      <span>{!!$record->email!!}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-2">
              <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__body pt-4">
                  <div style="text-align:center" class="col-lg-12">
                    <img src="{!!asset('public/attachments/users/'.$record->profile_pic)!!}" class="img img-thumbnail" width="150" />
                  </div>
                  <div class="col-lg-12 title-custom py-3" style="text-align:center">
                    @if($record->is_admin==1)
                      <span>{{ trans('authentication.is_admin') }}</span>
                    @else
                      <span>{{ trans('authentication.normal') }}</span>
                    @endif
                  </div>
                  <div class="col-lg-12 pt-3 text-center">
                        <label class="title-custom">{{ trans('authentication.is_dashboard') }}</label><br>
                        @if($record->is_dashboard==0)
                            <span>{{ trans('authentication.system') }}</span>
                        @elseif($record->is_dashboard==1)
                            <span>{{ trans('authentication.dashboard') }}</span>
                        @endif
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
            <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
          </div>
          @if($departments)
            @foreach($departments AS $item)
              @php
              // Get all modules by department
              $modules = getModulesByDepartment($item->id,$lang);
              // Get selected modules
              $selectedMods = getUserModulesByDepartment(decrypt($enc_id),$item->id);
              // Get all sections
              $sections = getSectionsByModules($item->id,$selectedMods,$lang);
              // Get selected sections
              $selectedSec = getUserSectionsByModules(decrypt($enc_id),$item->id);
              // Get all roles
              $roles = getRolesBySections($selectedSec,$item->id);
              // Get selected roles
              $selectedRol = getUserRolesBySections(decrypt($enc_id),$item->id);
              @endphp
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12 bg-dark p-1 px-2 mb-1">
                  <span class="text-white">{{ $item->name }}</span>
                </div>
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
                  <div class="m-portlet">
                    <div class="m-demo__preview p-3 px-4" style="height: 180px">
                      @if($modules)
                        @foreach($modules as $mod)
                          @if($mod->dep_id==$item->id)
                            <div class="m-checkbox-list <?=in_array($mod->id,$selectedMods) ? 'bg-warning' : ''?>">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" disabled <?=in_array($mod->id,$selectedMods) ? 'checked' : ''?> > {{ $mod->name }}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endif
                        @endforeach
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('authentication.section') }} :</label><br>
                  <div class="m-portlet">
                    <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px">
                    @if($sections)
                      <div class="p-3 px-4">
                        @foreach($sections as $key => $value)
                          <?php $module = explode('-',$key) ?>
                          <label class="title-custom">{!!$module[1]!!}</label>
                          @foreach($value as $sec)
                            <?php $section = explode('-',$sec) ?>
                            <div class="m-checkbox-list <?=in_array($section[0],$selectedSec) ? 'bg-warning' : ''?>">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" disabled <?=in_array($section[0],$selectedSec) ? 'checked' : ''?>> {{ $section[1] }}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          @endforeach
                        @endforeach
                      </div>
                    @endif
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('authentication.role') }} :</label><br>
                  <div class="m-portlet">
                    <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px">
                      @if($roles)
                        <div class="p-3 px-4">
                          @foreach($roles as $key => $value)
                            <label class="title-custom">{!!$key!!}</label>
                            @foreach($value as $rol)
                              <?php $role = explode('-',$rol) ?>
                              <div class="m-checkbox-list <?=in_array($role[0],$selectedRol) ? 'bg-warning' : ''?>">
                                <label class="m-checkbox m-checkbox--state-brand">
                                  <input type="checkbox" disabled <?=in_array($role[0],$selectedRol) ? 'checked' : ''?> > {{ $role[1] }}
                                  <span class="rotate"></span>
                                </label>
                              </div>
                            @endforeach
                          @endforeach
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          @endif
          </div>
        </div>
      @endif
      <!--end::record-->
    </div>
  </div>
</div>
