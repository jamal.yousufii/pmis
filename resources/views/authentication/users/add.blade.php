<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.add_user') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body" style="background: #f7f8fa;">
          <div class="row">
            <div class="col-xl-10">
              <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.departments.organization') }} : <span style="color:red;">*</span></label>
                      <div id="organization_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="organization_id" id="org_id" onchange="bringDataById('{{route('getDepartmentByOrganization')}}',this.value,'POST','dep_id','user_dep');bringDataById('{{route('getDepartmentByOrganization')}}',this.value,'POST','all_departments','all_dep');">
                          <option value="">{{ trans('global.select') }}</option>
                          @if($organizations)
                            @foreach($organizations as $item)
                              <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="organization_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                      <div id="department_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="department_id" id="dep_id" onchange="bringModules(this.value);">
                          <option value="">{{ trans('global.select') }}</option>
                        </select>
                      </div>
                      <div class="department_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.name') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="text" value="" name="name" id="name" onkeyup="putValue(this.id,'username_div')">
                      <div class="name error-div" style="display:none;"></div>
                    </div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.father') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="text" value="" name="father" id="father">
                      <div class="father error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.phone_no') }} : </label>
                      <input class="form-control m-input" type="number" min="0" value="" name="phone_number" id="phone_number">
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.position') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="text" value="" name="position" id="position">
                      <div class="position error-div" style="display:none;"></div>
                    </div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.email') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="email" value="" name="email" id="email">
                      <div class="email error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.password') }} : <span style="color:red;">*</span></label>
                      <div class="m-input-icon m-input-icon--left">
                        <input class="form-control m-input errorDiv" type="password" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
                        <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
                      </div>
                      <div class="password error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.confirm_password') }} : <span style="color:red;">*</span></label>
                      <div class="m-input-icon m-input-icon--left">
                        <input class="form-control m-input errorDiv" type="password" name="confirm_password" id="confirm_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
                        <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf(0)"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
                      </div>
                      <div class="confirm_password error-div" style="display:none;"></div>
                      <span id="msg"><span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-2">
              <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__body pt-4">
                  <div style="text-align:center" class="col-lg-12">
                    <img src="{!!asset('public/attachments/users/default.png')!!}" class="img img-thumbnail" width="150" />
                  </div>
                  <div style="text-align:center" class="col-lg-12 title-custom pt-3" id="username_div">
                  </div>
                  <div class="col-lg-12">
                    <select class="form-control m-input m-input--air select-2" name="is_admin" id="is_admin">
                      <option value="0">{{ trans('authentication.normal') }}</option>
                      <option value="1">{{ trans('authentication.is_admin') }}</option>
                    </select>
                  </div>
                  <div class="col-lg-12 pt-3 text-center">
                    <label class="title-custom">{{ trans('authentication.is_dashboard') }}</label>
                    <select class="form-control m-input m-input--air select-2" name="is_dashboard" id="is_dashboard">
                        <option value="0">{{ trans('authentication.system') }}</option>
                        <option value="1">{{ trans('authentication.dashboard') }}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
            <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom p-0 pt-3">
            <div class="col-lg-12">
              <div class="col-lg-1 mx-2 p-3">
                <div class="m-section__content d-inline">
                  <label class="title-custom">{{ trans('authentication.department') }} :</label>
                </div>
              </div>
              <div class="row" id="all_departments">
              </div>
            </div>
          </div>
          <div class="form-group m-form__group m-form__group_custom" id="dep_content">
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <button type="button" onclick="storeRecord('{{route('users.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
              <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.select-2').select2();
  function ShowHide()
  {
    if($('#password').attr("type") == "text"){
      $('#password').attr('type', 'password');
      $('#icon-pass').addClass( "la-eye" );
      $('#icon-pass').removeClass( "la-eye-slash" );
    }else if($('#password').attr("type") == "password"){
      $('#password').attr('type', 'text');
      $('#icon-pass').removeClass( "la-eye" );
      $('#icon-pass').addClass( "la-eye-slash" );
    }
  }

  function ShowHideConf()
  {
    if($('#confirm_password').attr("type") == "text"){
      $('#confirm_password').attr('type', 'password');
      $('#icon-passConf').addClass( "la-eye" );
      $('#icon-passConf').removeClass( "la-eye-slash" );
    }else if($('#confirm_password').attr("type") == "password"){
      $('#confirm_password').attr('type', 'text');
      $('#icon-passConf').removeClass( "la-eye" );
      $('#icon-passConf').addClass( "la-eye-slash" );
    }
  }

  function ConfirmPassword()
  {
    var pass = $('#password').val();
    var conf = $('#confirm_password').val();
    if(conf == pass){
      $("#msg").css("color", "green");
      $('#msg').html("{{ trans('authentication.pass_match') }}");
    }else{
      $("#msg").css("color", "red");
      $('#msg').html("{{ trans('authentication.pass_not_match') }}");
    }
  }
</script>
