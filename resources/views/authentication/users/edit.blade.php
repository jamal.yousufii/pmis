<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.edit_user') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
          <div class="m-portlet__body" style="background: #f7f8fa;">
            <div class="row">
              <div class="col-xl-10">
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                  <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                        <select class="form-control m-input m-input--air select-2" name="organization_id" id="org_id">
                          <option value="">{{ trans('global.select') }}</option>
                          @if($organizations)
                            @foreach($organizations as $item)
                              <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                        <div id="department_id" class="errorDiv">
                          <select class="form-control m-input m-input--air select-2" name="department_id" id="dep_id" onchange="bringModules(this.value);">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($departments)
                              @foreach($departments as $item)
                                <option value="{!!$item->id!!}" <?=$item->id==$record->department_id ? 'selected' : ''?>>{!!$item->name!!}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                        <div class="department_id error-div" style="display:none;"></div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.name') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="text" value="{!!$record->name!!}" name="name" id="name" onkeyup="putValue(this.id,'username_div')">
                        <div class="name error-div" style="display:none;"></div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.father') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="text" value="{!!$record->father!!}" name="father" id="father">
                        <div class="father error-div" style="display:none;"></div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.phone_no') }} : </label>
                        <input class="form-control m-input" type="number" min="0" value="{!!$record->phone_number!!}" name="phone_number" id="phone_number">
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.position') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="text" value="{!!$record->position!!}" name="position" id="position">
                        <div class="position error-div" style="display:none;"></div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.email') }} : <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="email" value="{!!$record->email!!}" name="email" id="email">
                        <div class="email error-div" style="display:none;"></div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.new_password') }} :</label>
                        <div class="m-input-icon m-input-icon--left">
                          <input class="form-control m-input" type="password" value="" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
                          <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <label class="title-custom">{{ trans('authentication.conf_new_password') }} :</label>
                        <div class="m-input-icon m-input-icon--left">
                          <input class="form-control m-input" type="password" value="" name="confirm_password" id="confirm_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
                          <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf()"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
                        </div>
                        <span id="msg"><span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-2">
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                  <div class="m-portlet__body pt-4">
                    <div style="text-align:center" class="col-lg-12">
                      <img src="{!!asset('public/attachments/users/'.$record->profile_pic)!!}" class="img img-thumbnail" width="150" />
                    </div>
                    <div style="text-align:center" class="col-lg-12 title-custom pt-3" id="username_div">
                    </div>
                    <div class="col-lg-12">
                      <select class="form-control m-input m-input--air select-2" name="is_admin" id="is_admin">
                        <option value="0" <?=$record->is_admin==0 ? 'selected' : ''?> >{{ trans('authentication.normal') }}</option>
                        <option value="1" <?=$record->is_admin==1 ? 'selected' : ''?> >{{ trans('authentication.is_admin') }}</option>
                      </select>
                    </div>
                    <div class="col-lg-12 pt-3 text-center">
                        <label class="title-custom">{{ trans('authentication.is_dashboard') }}</label>
                        <select class="form-control m-input m-input--air select-2" name="is_dashboard" id="is_dashboard">
                            <option value="0" <?=$record->is_dashboard=='0'? 'selected': ''?> >{{ trans('authentication.system') }}</option>
                            <option value="1" <?=$record->is_dashboard=='1'? 'selected': ''?> >{{ trans('authentication.dashboard') }}</option>
                        </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-portlet__head text-title" style="background:#fff; margin-top:-10px;">
              <div class="m-portlet__head-caption pb-2">{{ trans('authentication.user_roles') }}</div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom p-0 pt-3">
              <div class="col-lg-12">
                <div class="col-lg-1 mx-2 p-3">
                  <div class="m-section__content d-inline">
                    <label class="title-custom">{{ trans('authentication.department') }} :</label>
                  </div>
                </div>
                <div class="row">
                  @if($departments)
                    @foreach($departments as $item)
                      <div class="col-lg-2 px-1">
                        <div class="clearfix m-portlet py-3 px-3 <?=in_array($item->id,$selectedDeps) ? 'bg-warning' : ''?>" id="div_{{ $item->id }}">
                          <div class="m-section__content d-inline">
                            <div class="m-checkbox-list">
                              <label class="m-checkbox m-checkbox--state-brand">
                                <input type="checkbox" name="departments[]" <?=in_array($item->id,$selectedDeps) ? 'checked' : ''?> onclick="setBackground('div_{{ $item->id }}','dep_{{ $item->id }}','bg-warning');getDepartmentModules('{{route('getModulesByDepartments')}}','dep_{{ $item->id }}','POST','dep_content','{{decrypt($enc_id)}}')" value="{{ $item->id }}" id="dep_{{ $item->id }}"> {!!$item->name!!}
                                <span class="rotate"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
            <div class="form-group m-form__group m-form__group_custom" id="dep_content">
              @foreach($departments as $item)
                @if(in_array($item->id,$selectedDeps))
                  @php
                  // Get all modules by department
                  $modules = getModulesByDepartment($item->id,$lang);
                  // Get selected modules
                  $selectedMods = getUserModulesByDepartment(decrypt($enc_id),$item->id);
                  // Get all sections
                  $sections = getSectionsByModules($item->id,$selectedMods,$lang);
                  // Get selected sections
                  $selectedSec = getUserSectionsByModules(decrypt($enc_id),$item->id);
                  // Get all roles
                  $roles = getRolesBySections($selectedSec,$item->id);
                  // Get selected roles
                  $selectedRol = getUserRolesBySections(decrypt($enc_id),$item->id);
                  @endphp
                  <div class="row" id="dep_content_{{$item->id}}">
                    <div class="col-lg-12 bg-dark p-1 px-2 mb-1">
                      <span class="text-white">{{ getDepartmentNameByID($item->id,$lang) }}</span>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.applications') }} :</label><br>
                      <div class="m-portlet">
                        <div class="m-demo__preview p-3 px-4" style="height: 180px">
                          @if($modules)
                            @foreach($modules as $mod)
                              @if($mod->dep_id==$item->id)
                                <div class="m-checkbox-list <?=in_array($mod->id,$selectedMods) ? 'bg-warning' : ''?>" id="div_mod_{{ $item->id }}_{{ $mod->id }}">
                                  <label class="m-checkbox m-checkbox--state-brand">
                                    <input type="checkbox" name="modules_{{ $item->id }}[]" id="mod_{{ $item->id }}_{{ $mod->id }}" <?=in_array($mod->id,$selectedMods) ? 'checked' : ''?> value="{{ $mod->id }}" onclick="setBackground('div_mod_{{ $item->id }}_{{ $mod->id }}','mod_{{ $item->id }}_{{ $mod->id }}','bg-warning');getModuleSections('{{route('getModuleSections',encrypt($item->id))}}','modules_{{$item->id}}[]','GET','section_div_{{$item->id}}','{{decrypt($enc_id)}}')"> {!!$mod->name!!}
                                    <span class="rotate"></span>
                                  </label>
                                </div>
                              @endif
                            @endforeach
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.section') }} :</label><br>
                      <div class="m-portlet">
                        <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="section_div_{{$item->id}}">
                        @if($sections)
                          <div class="p-3 px-4">
                            @foreach($sections as $key => $value)
                              <?php $module = explode('-',$key) ?>
                              <label class="title-custom">{!!$module[1]!!}</label>
                              @foreach($value as $sec)
                                <?php $section = explode('-',$sec) ?>
                                <div class="m-checkbox-list <?=in_array($section[0],$selectedSec) ? 'bg-warning' : ''?>" id="div_sec_{{$item->id}}_{{$section[0]}}">
                                  <label class="m-checkbox m-checkbox--state-brand">
                                    <input type="checkbox" name="sections_{{ $item->id }}[]" id="sec_{{$item->id}}_{{$section[0]}}" <?=in_array($section[0],$selectedSec) ? 'checked' : ''?> value="{{ $section[0] }}" onclick="setBackground('div_sec_{{$item->id}}_{{$section[0]}}','sec_{{$item->id}}_{{$section[0]}}','bg-warning');getSectionRoles('{{route('getSectionRoles',encrypt($item->id))}}','sections_{{ $item->id }}[]','GET','role_div_{{$item->id}}','{{decrypt($enc_id)}}')"> {!!$section[1]!!}
                                    <span class="rotate"></span>
                                  </label>
                                </div>
                              @endforeach
                            @endforeach
                          </div>
                        @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('authentication.role') }} :</label><br>
                      <div class="m-portlet">
                        <div class="m-demo__preview custome_m-demo__preview p-2" style="height: 180px" id="role_div_{{$item->id}}">
                          @if($roles)
                            <div class="p-3 px-4">
                              @foreach($roles as $key => $value)
                                <label class="title-custom">{!!$key!!}</label>
                                @foreach($value as $rol)
                                  <?php $role = explode('-',$rol) ?>
                                  <div class="m-checkbox-list <?=in_array($role[0],$selectedRol) ? 'bg-warning' : ''?>" id="div_sec_{{$item->id}}_{{ $role[0] }}">
                                    <label class="m-checkbox m-checkbox--state-brand">
                                      <input type="checkbox" name="roles_{{$item->id}}[]" id="sec_{{$item->id}}_{{ $role[0] }}" <?=in_array($role[0],$selectedRol) ? 'checked' : ''?> value="{{ $role[0] }}" onclick="setBackground('div_sec_{{$item->id}}_{{ $role[0] }}','sec_{{$item->id}}_{{ $role[0] }}','bg-warning');"> {!!$role[1]!!}
                                      <span class="rotate"></span>
                                    </label>
                                  </div>
                                @endforeach
                              @endforeach
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <input type="hidden" name="enc_id" id="enc_id" value="{!!$enc_id!!}"/>
                    <button type="button" onclick="doEditRecord('{{route('users.update',$enc_id)}}','requestForm','PUT','response_div')" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.select-2').select2();
  function ShowHide()
  {
    if($('#password').attr("type") == "text"){
      $('#password').attr('type', 'password');
      $('#icon-pass').addClass( "la-eye" );
      $('#icon-pass').removeClass( "la-eye-slash" );
    }else if($('#password').attr("type") == "password"){
      $('#password').attr('type', 'text');
      $('#icon-pass').removeClass( "la-eye" );
      $('#icon-pass').addClass( "la-eye-slash" );
    }
  }

  function ShowHideConf()
  {
    if($('#confirm_password').attr("type") == "text"){
      $('#confirm_password').attr('type', 'password');
      $('#icon-passConf').addClass( "la-eye" );
      $('#icon-passConf').removeClass( "la-eye-slash" );
    }else if($('#confirm_password').attr("type") == "password"){
      $('#confirm_password').attr('type', 'text');
      $('#icon-passConf').removeClass( "la-eye" );
      $('#icon-passConf').addClass( "la-eye-slash" );
    }
  }

  function ConfirmPassword()
  {
    var pass = $('#password').val();
    var conf = $('#confirm_password').val();
    if(conf == pass){
      $("#msg").css("color", "green");
      $('#msg').html("{{ trans('authentication.pass_match') }}");
    }else{
      $("#msg").css("color", "red");
      $('#msg').html("{{ trans('authentication.pass_not_match') }}");
    }
  }
</script>
