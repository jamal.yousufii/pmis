<div class="row">
  <div class="col-lg-12">
    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.organization.view_org') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="#" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
          </ul>
        </div>
      </div>
      <!--begin::record-->
      @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4 col-md-4">
                <label class=""><span class="m-widget12__text2">{{ trans('authentication.name_dr') }} :</span></label><br>
                <span>{!!$record->name_dr!!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class=""><span class="m-widget12__text2">{{ trans('authentication.name_pa') }} :</span></label><br>
                <span>{!!$record->name_pa!!}</span>
              </div>
              <div class="col-lg-4 col-md-4">
                <label class=""><span class="m-widget12__text2">{{ trans('authentication.name_en') }} :</span></label><br>
                <span>{!!$record->name_en!!}</span>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12 col-md-12">
                <label class="">{{ trans('authentication.code') }} : </span></label><br>
                <span>{!!$record->org_code!!}</span>
              </div>
            </div>
          </div>
        </div>
      @endif
      <!--end::record-->
    </div>
  </div>
</div>
