<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{ trans('authentication.organization.edit_org') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="organization_form">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_dr') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_dr!!}" name="name_dr" id="name_dr">
                <div class="name_dr error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_pa') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_pa!!}" name="name_pa" id="name_pa">
                <div class="name_pa error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_en') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_en!!}" name="name_en" id="name_en">
                <div class="name_en error-div" style="display:none;"></div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.code') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->org_code!!}" name="org_code" id="org_code">
                <div class="org_code error-div" style="display:none;"></div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <button type="button" onclick="doEditRecord('{{route('organizations.update',$enc_id)}}','organization_form','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
