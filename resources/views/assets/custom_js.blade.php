@php
use Illuminate\Support\Facades\session;
@endphp
<!-- Custom Scripts -->
<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function chooseFile(id) {
    var id = $("#" + id);
    var fileName = id[0].files[0].name;
    id.parent().find('label').html(fileName);
}

counter = 1;
function add_moreAttachments(route, total)
{
    if (counter <= 4)
    {
        if (counter == 1 && total > 0) {
            counter = parseInt(total) + 1;
        } else {
            $('#btn_'+counter).hide();
            counter = parseInt(counter) + 1;
        }
        $.ajax({
            url: route,
            data: {
                "_method": 'POST',
                "number" : counter,
                "_token" : "{{ csrf_token() }}",
            },
            type: 'POST',
            success: function (response) {
                $('#targetDiv').append(response);
            }
        });
    }
}

function remove_moreAttachments(div, number)
{
    if (confirm("آیا شما موافق هستید؟"))
    {
        counter = number - 1;
        $('#btn_' + counter).show();
        $('#'+ div).remove();
    }
}

function readImg(input)
{
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#user-img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
      $("#apply_changes").css("display", "inline");
  }
}

function callToPressBtn(input)
{
  $("#"+input).click();
}

function applyChanges()
{
  var formData = new FormData($("#docForm")[0]);
  $.ajax({
      url   : '{{ route("uploadPic") }}',
      data  : formData,
      processData : false,
      contentType : false,
      type  : 'post',
      success: function(response)
      {
        $('#msg-profile').html(response);
        $("#apply_changes").css("display", "none");
      }
  });
}

function ShowHide()
{
  if($('#password').attr("type") == "text"){
    $('#password').attr('type', 'password');
    $('#icon-pass').addClass( "la-eye" );
    $('#icon-pass').removeClass( "la-eye-slash" );
  }else if($('#password').attr("type") == "password"){
    $('#password').attr('type', 'text');
    $('#icon-pass').removeClass( "la-eye" );
    $('#icon-pass').addClass( "la-eye-slash" );
  }
}

function ShowHideConf()
{
  if($('#conf_password').attr("type") == "text"){
    $('#conf_password').attr('type', 'password');
    $('#icon-passConf').addClass( "la-eye" );
    $('#icon-passConf').removeClass( "la-eye-slash" );
  }else if($('#conf_password').attr("type") == "password"){
    $('#conf_password').attr('type', 'text');
    $('#icon-passConf').removeClass( "la-eye" );
    $('#icon-passConf').addClass( "la-eye-slash" );
  }
}

function ConfirmPassword()
{
  var pass = $('#password').val();
  var conf = $('#conf_password').val();
  if(conf == pass){
    $("#msg").css("color", "green");
    $('#msg').html("{{ trans('global.pass_match') }}");
  }else{
    $("#msg").css("color", "red");
    $('#msg').html("{{ trans('global.pass_not_match') }}");
  }
}

function editPassword()
{
  var pass = $('#password').val();
  var conf = $('#conf_password').val();
  if(conf != pass){
    $('#msg').html("{{ trans('global.pass_not_match') }}");
    $("#conf_password").css("border", "1px solid red");
    $("#msg").css("color", "red");
    return;
  }
  var formData = new FormData($("#changePassword")[0]);
  $.ajax({
      url   : '{{ route("editPassword") }}',
      data  : formData,
      processData: false,
      contentType : false,
      type  : 'post',
      success: function(response)
      {
        $('#btn-password').click();
        swal({
            text: response,
            icon: "success",
            type: "",
            confirmButtonText:  "<span>{{trans('global.back')}}</span>",
            confirmButtonClass: "btn btn-success m-btn btn-sm m-btn--icon",
            showCancelButton:   0,
            cancelButtonText:   "<span><span>{{trans('global.no')}}</span></span>",
            cancelButtonClass:  "btn btn-secondary btn-sm m-btn--icon"
        }).then(function (e) {
            e.value && redirectFunction();
        });
      }
  });
}

function readNotifications(id)
{
  $.ajax({
      url: '{{ route("markAsRead") }}',
      data: {
          "_method" : 'GET',
          "id"      : id
      },
      type: 'get',
      success: function(response)
      {
      }
  });
}

// Add Records
function addRecord(url,params,method,response_div)
{
  serverRequest(url,params,method,response_div);
}

// Edit Records
function editRecord(url,params,method,response_div,is_modal)
{
  serverRequest(url,params,method,response_div,is_modal);
}

// Call data from server side
function serverRequest(url,params,method,response_div,is_modal=false)
{
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      if(is_modal){
         $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      $('#'+response_div).html(response);
      $(".datePicker").css('width','100%');
      @if($lang=="en")
        $(".datePicker").attr('type', 'date');
      @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
      @endif
      $('.select-2').select2();
      tinymce.remove();
      tinymce.init({
        selector:'textarea.tinymce',
        @if($lang!="en")
          directionality : 'rtl',
        @endif
        setup: function (editor) {
          editor.on('change', function () {
              tinymce.triggerSave();
          });
        }
      });
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){

        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false
  })
}

// Store Records
function storeRecord(url,form_id,method,response_div,redirectFunction,is_modal=false,btnID)
{
  if(validation(form_id))
  {
    var params = new FormData($('#'+form_id)[0]);
    serverRequestResponse(url,params,method,response_div,redirectFunction,is_modal,btnID);
  }
}

// Pass result to the function form server side
function serverRequestResponse(url,params,method,response_div,redirectFunction,is_modal=false,btnID='submitBtn')
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','');
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      // disable submit button
      $('#'+btnID).attr('disabled','disabled');
      if(is_modal){
         $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      // alert(response); console.log(response); return;
      // enable submit button
      $('#'+btnID).removeAttr('disabled');
      $(".m-page-loader.m-page-loader--base").css("display","none");
      redirectFunction(response,response_div);
    },
    error: function (request, status, error) {
      // enable submit button
      $('#'+btnID).removeAttr('disabled');
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){
        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false,
    contentType: false,
  })
}

// Update Records
function doEditRecord(url,form_id,method,response_div)
{
  if(validation(form_id))
  {
    var params = $('#'+form_id).serialize();
    serverRequestResponseEdit(url,params,method,response_div,redirectFunction);
  }
}

// Pass result to the update function form server side
function serverRequestResponseEdit(url,params,method,response_div='content',redirectFunction)
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','');
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      $(".m-page-loader.m-page-loader--base").css("display","block");
    },
    success: function(response)
    {
      // alert(response); console.log(response); return;
      $(".m-page-loader.m-page-loader--base").css("display","none");
      redirectFunction(response,response_div);
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){
        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false,
  })
}

// Update Records with its attachments
function doEditRecordWithAttachments(url,form_id,method,redirectFunction)
{
  if(validation(form_id))
  {
    $('.error-div').html('');
    $('.errorDiv').css('border-bottom','1px solid #ebedf2');
    var form_data = new FormData($('#'+form_id)[0]);
    $.ajax({
        url:  url,
        data: form_data,
        type: method,
        beforeSend: function()
        {
          $(".m-page-loader.m-page-loader--base").css("display","block");
        },
        success: function(response)
        {
          $(".m-page-loader.m-page-loader--base").css("display","none");
          redirectFunction(response);
        },
        error: function (request, status, error) {
          $(".m-page-loader.m-page-loader--base").css("display","none");
          json = $.parseJSON(request.responseText);
          $.each(json.errors, function(key, value){
              $('.'+key).show();
              $('.'+key).html('<span class="text-danger">'+value+'</span>');
              $('#'+key).css('border-bottom','1px solid #dc3545');
          });
        },
        cache: false,
        processData: false,
        contentType: false,
    });
  }
}

// View Records
function viewRecord(url,params,method,response_div)
{
  serverRequest(url,params,method,response_div);
}

function destroy(url,params,method,response_div,removed_id)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
    if(params!="")
    {
      e.value && deleteRecordAttachment(url,params,method,response_div,removed_id);
    }else
    {
      e.value && deleteRecord(url,params,method,response_div,removed_id);
    }
  });
}

// Delete Records
function deleteRecord(url,params,method,response_div,removed_id)
{
  $.ajax({
      url : url,
      data: params,
      type: method,
      beforeSend: function(){
        $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        redirectFunction();
      }
  });
}

// Delete Records
function deleteRecordAttachment(url,params,method,response_div,removed_id)
{
  $.ajax({
      url : url,
      data: params,
      type: method,
      beforeSend: function(){
        $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        if(removed_id!='')
        {
          $('#'+removed_id).slideUp();
        }
        $('#'+response_div).html(response);
      }
  });
}

// Redirect to index function
function redirectFunction(response)
{
  location.reload();
}

// Filter Records
function filterRecords(url,method,response_div)
{
  item = $('#keyWord').val();
  $.ajax({
      url : url,
      data: {
          "item" : item
      },
      type: method,
      beforeSend: function(){
        $("#"+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $("#"+response_div).html(response);
      }
  });
}

// Paginate Records
$(document).ready(function()
{
    $('.pagination a').on('click', function(event) {
        event.preventDefault();
        if($(this).attr('href') != '#') {
            // Get current URL route
            document.cookie = "no="+$(this).text();
            var dataString = '';
            counter = parseInt($(this).attr('id'));
            dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter;
            $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                    $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
                },
                success: function(response)
                {
                    $('#searchresult').html(response);
                }
            });
        }
    });
});

// Display Project Types Dropdown
function bringTypes()
{
  var id = $('#category_id').val();
  $.ajax({
      url: '{{ route("bringTypes") }}',
      data: {
          "_method" : 'POST',
          "id"      : id
      },
      type: 'post',
      beforeSend: function(){
          $("#ptype_div").html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#ptype_div').html(response);
      }
  });
}

// Display District Dropdown
function bringDistricts(id,targetDiv,number)
{
  var id = $('#'+id).val();
  $.ajax({
      url: '{{ route("bringDistricts") }}',
      data: {
          "_method" : 'POST',
          "id"      : id,
          "counter" : number,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $('#'+targetDiv).html(response);

      }
  });
}

// Display Village Dropdown
function bringVillages(id,targetDiv,number)
{
  var id = $('#'+id).val();
  $.ajax({
      url: '{{ route("bringVillages") }}',
      data: {
          "_method" : 'POST',
          "id"      : id,
          "counter" : number,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

// Display users Dropdown
function bringSectionUsers(id,dep,targetDiv)
{
  var section = $('#'+id).val();
  var id = $('#'+id).val();
  $.ajax({
      url: '{{ route("bringSectionUsers") }}',
      data: {
          "_method"     : 'POST',
          "id"          : id,
          "dep"          : dep,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

// Display users Dropdown
function bringDepartmentUsers(dep_id,targetDiv)
{
  var department = $('#'+dep_id).val();
  var dep_id = $('#'+dep_id).val();
  $.ajax({
      url: '{{ route("bringDepartmentUsers") }}',
      data: {
          "_method"     : 'POST',
          "dep_id"      : dep_id,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

// Display Sections Dropdown
function bringDepSections(id,targetDiv)
{
  var id = $('#'+id).val();
  $.ajax({
      url: '{{ route("bringDepSections") }}',
      data: {
          "_method" : 'POST',
          "id"      : id,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
          $('#div_contractors_section').empty();
      }
  });
}

// Display Sections Dropdown
function bringContractorSections(id,dep,targetDiv,url)
{

  var id = $('#'+id).val();
  $.ajax({
      url: url,
      data: {
          "_method" : 'POST',
          "id": id,
          "dep_id" : dep,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $("#div_contractors_section").css( "display","block" );
          $('#'+targetDiv).html(response);
      }
  });
}

// Display contractor Users
function bringContractorSectionUsers(id,dep,contractor,targetDiv)
{
    var contractor_section = $('#'+id).val();
    $.ajax({
      url: '{{ route("bringContractorSectionUsers") }}',
      data: {
          "_method" : 'POST',
          "id"      : contractor_section,
          "dep_id"  : dep,
          "con_id"  : contractor,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $("#div_contractors_section").css( "display","block" );
          $('#'+targetDiv).html(response);
      }
  });
}

// Filter Records
function searchRecords(url,form_id,method,response_div,loading=true)
{
  if(validation(form_id))
  {
   var params = $('#'+form_id).serialize();
   serverRequest(url,params,method,response_div,loading);
 }
}

more = 1;
function add_more(div,url,total)
{
  if(more==1 && total>0){
    more = parseInt(total) + 1;
  }else{
    $('#'+div+'_btn_'+more).hide();
    more = parseInt(more) + 1;
  }
  $.ajax({
    url: url,
    data: {
      "_method" : 'POST',
      "number"  : more,
    },
    type:'POST',
    success:function(response){
      $('#'+div).prepend(response);
      $('.select-2').select2();
    }
  });
}

function confirmRemoveMore(div,number,btn)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && remove_more(div,number,btn);
  });
}

function remove_more(div,number,btn)
{
  more = number-1;
  $('#'+btn+'_'+more).show();
  $('#'+div).remove();
}

function destroyFile(url,method,params,response_div)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && deleteFile(url,method,params,response_div);
  });
}

function deleteFile(url,method,params,response_div)
{
  $.ajax({
      url: url,
      data: {
          "_method" : method,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+hide).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        if(response=='true')
        {
          $('#'+hide).hide();
          $('#'+show).show();
        }
      }
  });
}

function destroyUpdatedFile(url,old_div,new_div)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && deleteUpdatedFile(url,old_div,new_div);
  });
}

function deleteUpdatedFile(url,old_div,new_div)
{
  $.ajax({
    url: url,
    data: {
      "_method" : 'GET',
    },
    type: 'get',
    beforeSend: function(){
      $('#'+old_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
    },
    success: function(response)
    {
      if(response=='true')
      {
        $('#'+old_div).hide();
        $('#'+new_div).show();
      }
    }
  });
}

entityCounter = 0;
function bringSurveyEntities(total)
{
  if(entityCounter==1 && total>0){
    entityCounter = parseInt(total) + 1;
  }else{
    entityCounter = parseInt(entityCounter) + 1;
  }
  var token = $('meta[name="csrf-token"]').attr('content');
  var code = $('#code').val();

  $.ajax({
      url: '{!!URL::route("SurveyEntities")!!}',
      data: {
          "_method" : 'GET',
          "_token"  : token,
          "counter" : entityCounter,
          "code"    : code
      },
      type: 'get',
      success: function(response)
      {
          $('#showContent').html(response);
      }
  });
}

/**
 * @Author: Share record
 * @Date: 2019-11-21 11:23:30
 * @Desc:
 */
function shareRecord(route,form_id,btnID)
{
  if(validation(form_id))
  {
    var params = new FormData($("#"+form_id)[0]);
    serverRequestResponse(route,params,'POST','',redirectFunction,false,btnID);
  }
}

function getTotal(amount,price,total_price,total_cost,perc_div,rem_perc_div,remaining_percent,current_percent,isEdit,msg_div)
{
  var amount = $('#'+amount).val();
  var price = $('#'+price).val();
  var total = amount * price;
  var percentage = (total*100)/total_cost;
  if(isEdit){
    remaining_percent = remaining_percent+current_percent;
    remaining_percent = remaining_percent-percentage;
  }else{
    remaining_percent = remaining_percent-percentage;
  }
  if(total > 0)
  {
    $('#'+total_price).val(total);
  }
  else
  {
    $('#'+total_price).val();
  }
  if(percentage > 100)
  {
    $('#'+perc_div).val(percentage);
    $('#'+perc_div).css('border','1px solid red');
    $('#'+perc_div).attr('disabled','disabled');
  }
  else
  {
    $('#'+perc_div).css('border','');
    $('#'+perc_div).val(percentage);
    $('#'+perc_div).removeAttr('disabled');

  }
  if(remaining_percent < 0 ){
    $('#'+rem_perc_div).css('border','1px solid red');
    $('#'+rem_perc_div).attr('disabled','disabled');
    $('#'+rem_perc_div).val(remaining_percent);
    $('#'+msg_div).css({"display": "block"});
  }
  else
  {
    $('#'+rem_perc_div).css('border','');
    $('#'+rem_perc_div).val(remaining_percent);
    $('#'+rem_perc_div).removeAttr('disabled');
    $('#'+msg_div).css({"display": "none"});
  }
}

function getTotalBQ(amount,price,total_price)
{
    if(amount!=null && price!=null){
        var amount = $('#'+amount).val();
        var price = $('#'+price).val();
        var total = amount * price;
        if(total > 0)
        {
            $('#'+total_price).val(total);
        }
        else
        {
            $('#'+total_price).val();
        }
    }
}

function completeRecord(url,id,form_id,method,response_div)
{
  if($('#'+id).prop('checked'))
  {
    swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText:  "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton:   !0,
      cancelButtonText:   "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass:  "btn btn-secondary btn-sm m-btn--icon"
    })
    .then(function (e) {
      if(e.value==true){
        var params = new FormData($('#'+form_id)[0]);
        serverRequestResponse(url,params,method,response_div,redirectFunction);
      }else{
        $('#'+id). prop("checked", false);
      }
    });
  }
}

function unshare(url)
{
  swal({
      text: "{{trans('global.confirm_unshare')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.unshare')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.cancel')}}</span></span>",
      cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
  }).then(function (e) {
      e.value && serverRequestResponse(url,'','DELETE','',redirectFunction);
  });
}

function unshareAdjustments(url,params)
{
  swal({
      text: "{{trans('global.confirm_unshare')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.unshare')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.cancel')}}</span></span>",
      cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
  }).then(function (e) {
      e.value && serverRequestResponse(url,params,'GET','',redirectFunction);
  });
}

function AddSurveyRecord()
{
  var selectElement = document.getElementById("code").value;
  $('#'+selectElement+'_'+entityCounter).appendTo($('#aabedat_div'));
  $('#code option').prop('selected', function() {
        return this.defaultSelected;
    });
}

function validation(form_id)
{
  var form = document.getElementById(form_id);
  for (i=0;i<form.length;i++)
  {
    var tempobj = form.elements[i];
    if(tempobj.id)
    {
      if(($('#'+tempobj.id).hasClass('required') && $.trim(tempobj.value) == ''))
      {
        tempobj.focus();
        if($('#'+tempobj.id).attr("type") == "number" || $('#'+tempobj.id).attr("type") == "text" || $('#'+tempobj.id).attr("type") == "date"){
          $('#'+tempobj.id).css('border-bottom','1px solid #FF0000');
        }else{
          $('#div_'+tempobj.id).css('border-bottom','1px solid #FF0000');
        }
        $('.'+tempobj.id).css('display','inline');
        $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        return false;
      }
      else
      {
        $('#'+tempobj.id).css('border','');
        $('.'+tempobj.id).html('');
      }
    }
  }
  return true;
}

function validate(url,form_id,method)
{
  var form = document.getElementById(form_id);
  for (i=0;i<form.length;i++)
  {
    var tempobj = form.elements[i];
    if(tempobj.id)
    {
      if (tempobj.id.substring(0,4) == 'req_' && $.trim(tempobj.value) == '' )
      {
        tempobj.focus();
        if($('#'+tempobj.id).attr("type") == "number" || $('#'+tempobj.id).attr("type") == "text"){
          $('.'+tempobj.id).css('border-top','1px solid #FF0000');
          $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        }else {
          $('.'+tempobj.id).css('border-top','1px solid #FF0000');
          $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        }
        return false;
      }
      else
      {
        $('.'+tempobj.id).css('border','');
        $('.'+tempobj.id).html('');
      }
    }
  }
  storeRecord(url,form_id,method);
}

/** contractor
 * Make window
 */
function openWindow(route,width=300,height=100)
{
  if( typeof( window.innerWidth ) == 'number' )
  {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
  myWidth  -= width;
  myHeight -= height;
  //get top and left
  var left = (screen.width/2)-(myWidth/2);
  var top  = (screen.height/2)-(myHeight/2);
  var property = "width = " + myWidth + ", height = " +myHeight+",left="+left+",top="+top+ ",location=no, menubar=no,status=no, scrollbars=yes, resizable=no";
  var naw = window.open(route, 'document', property);
  if (window.focus)
  {
    naw.focus();
  }
}

function doDestroy(url,params,method,response_div,removed_id)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && doDeleteRecord(url,params,method,response_div,removed_id);
  });
}

// Delete Records
function doDeleteRecord(url,params,method,response_div,removed_id)
{
  $.ajax({
      url : url,
      data: params,
      type: method,
      beforeSend: function(){
        $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        redirectFunction(response);
      }
  });
}

function bringModules(id,url)
{
  $.ajax({
      url: url,
      data: {
          "_method" : 'POST',
          "id"      : id,
      },
      type: 'post',
      success: function(response)
      {
        $("#app_div").removeAttr("style");
        $("#user_roles").html(response);
      }
  });
}

function bringSections(url,id)
{
  var modules = $("#"+id).val();
  $.ajax({
      url: url,
      data: {
        "_method" : 'POST',
        "modules" : modules,
      },
      type: 'post',
      success: function(response)
      {
        $("#section_div").html(response);
      }
  });
}

function bringRoles(url,id)
{
  var sections = getGroupCheckedValue(id);
  $.ajax({
      url: url,
      data: {
          "_method"  : 'POST',
          "sections" : sections,
      },
      type: 'post',
      success: function(response)
      {
        $("#role_div").html(response);
      }
  });
}

// Display Inspection Dropdown
function bringSubInspection(id,targetDiv,number)
{
  var id = $('#'+id).val();
  $.ajax({
      url: '{{ route("getSubInepection") }}',
      data: {
          "_method" : 'POST',
          "id"      : id,
          "counter" : number,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $('#'+targetDiv).html(response);
      }
  });
}

function bringData(id,url,method,display,pro_id="",code="",prev_dep="-")
{
  var id = $('#'+id).val();
  $.ajax({
      url: url,
      data: {
        "id"        : id,
        "pro_id"    : pro_id,
        "code"      : code,
        "prev_dep"  : prev_dep,
      },
      type: method,
      beforeSend: function(){
        $('#'+display).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $('#'+display).html(response);
        $(".select-2").select2({ width: '100%' });
      }
  });
}

function getAndShwoData(id,display)
{
  var content = $('#'+id).html();
  $('#'+display).html(content);
}

var product = [];
function BQSelected(id)
{
    var checked = $('#'+id).val();
    if($('#'+id).is(':checked'))
    {
        product.push(checked);
    }
    else
    {
        product = jQuery.grep(product, function(value) {
            return value != checked;
        });
    }
    $('#selectedBQ').val(product);
    if(product.length>0)
    {
        $('#submitBTN').removeClass('disabled');
    }
    else
    {
        $('#submitBTN').addClass('disabled');
    }
}

function bringUnit(id,targetDiv)
{
  $.ajax({
      url: '{{ route("bringUnit") }}',
      data: {
        "_method" : 'POST',
        "id"      : id,
      },
      type: 'post',
      beforeSend: function(){
        $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

function bringAmount(id,targetDiv)
{
  $.ajax({
      url: '{{ route("bringAmount") }}',
      data: {
        "_method" : 'POST',
        "id"      : id,
      },
      type: 'post',
      beforeSend: function(){
        $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

// Store Records
function storeData(url,form_id,method,response_div,responseFunction,is_modal,modal_id=0)
{
  if(validation(form_id))
  {
    $('#'+modal_id).removeClass("show");;
    var params = new FormData($('#'+form_id)[0]);
    serverRequestResponse(url,params,method,response_div,responseFunction,is_modal);
  }
}

/**
 * @Date: 2020-02-23 11:55:43
 * @Desc: Put Content after request finished
 * @params: result, response_div
 */
  function put_content(result,response_div)
  {
    $('#'+response_div).html(result);
  }

  function bringPage()
  {
    var code = $('#static_id').val();
    var params = 'code='+code;
    serverRequest('{{route("report.show")}}',params,'POST','showContent');
  }

  function bringGraphPage(method,dropdownId,showdiv)
  {
    var code = $('#'+dropdownId).val();
    var params = 'code='+code;
    url='{{ route("location_year") }}'
    serverRequest(url,params,method,showdiv);
  }

  function bringLocationYearGraph(form_id)
  {
    var year = $('#year').val();
    var province = $('#province').val();
    var params   = 'year='+year+'&province='+province;
    serverRequest('{{route("location_year_graph")}}',params,'POST','showLocationYearContent');
  }



$(".select-2").select2({ width: '100%' });

// Redirect to another page
function requestPage(url,location_id)
{
  if(location_id!="")
  {
    url = url.replace('loc_id',location_id);
    $(location).attr('href',url);
  }
}

// Bring Search fielf based on value
function bringSearchFiel(url,method,value,target_div)
{
  var date_field = ['request_date'];
  var select_box = ['department_id','process','p.process','projects.process','s.process'];
  var params = 'type='+value;
  if(date_field.includes(value)) // if it's date then return Date Input
  {
    var input = '<div class="m-input-icon m-input-icon--left"><input type="text" name="value" class="form-control m-input datePicker required" placeholder="{{trans('global.date')}}" id="search_vlaue"><span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-calendar"></i></span></span></div> <div class="search_value error-div" style="display:none;"></div>';
    $('#'+target_div).html(input);
    @if($lang=="en")
    $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
  }
  else if(select_box.includes(value)) // Bring select box from serverside
  {
    //Call server request function
    serverRequest(url,params,method,target_div,true);
  }
  else
  {
    var input = '<div class="m-input-icon m-input-icon--left"><input type="text" name="value" class="form-control m-input required" placeholder="{{trans('global.search_dote')}}" id="search_value"><span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span></div><div class="search_value error-div" style="display:none;"></div>';
    $('#'+target_div).html(input);
  }

}

var selected = [];
function CheckForSelected(id,btn)
{
    var checked = $('#'+id).val();
    if($('#'+id).is(':checked'))
    {
        selected.push(checked);
    }
    else
    {
        selected = jQuery.grep(selected, function(value) {
            return value != checked;
        });
    }
    if(selected.length>0)
    {
      $('#'+btn).removeAttr('disabled');
    }
    else
    {
      $('#'+btn).attr('disabled','disabled');
    }
}

/**
 * @Date: 2020-05-11 13:24:30
 * @Desc:
 */
function hasSurvey(value,content_div)
{
  if(value=='yes')
  {
    $('#'+content_div).removeClass('d-none');
  }
  else
  {
    $('#'+content_div).addClass('d-none');
  }
}

/**
 * @Date: 2020-05-11 13:24:30
 * @Desc:
 */
function hasData(value,content_div,desc_div)
{
  if(value=='yes')
  {
    $('#'+content_div).removeClass('d-none');
    $('#'+desc_div).addClass('d-none');
  }
  else
  {
    $('#'+content_div).addClass('d-none');
    $('#'+desc_div).removeClass('d-none');
  }
}

function dynamicColors() {
  var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ", 0.5)";
}

function poolColors(a) {
  var pool = [];
  for(i = 0; i < a; i++) {
      pool.push(dynamicColors());
  }
  return pool;
}

function showHideClass(id)
{
    $('#'+id).removeClass('show');
}

function inVisible(id,attr){
  if(attr=='visible'){
    $("#"+id).attr("style", "display:block");
  }
  else
  {
    $("#"+id).removeAttr("style");
  }

}

// function to print all graphs
function printGraph(targetDiv,title){
    var div = document.getElementById(targetDiv);
    var image = div.querySelector("canvas").toDataURL("image/png").replace("image/png", "image/octet-stream");//Convert canvas to image
    var printDiv = $('<div><image src="'+image+'" /></div>');
    document.title = title;
    printDiv.print();
}

/**
 * @Date: 2020-05-21 14:55:44
 * @Desc: Add Activity Payment for Request
 */

function addActivityPayment(url,params,method,content_div,id)
{
    var checked = $("input[id=check_" + id + "]:checked").length;
    var request_amount = $('#request_amount_'+id).val();
    params+='&&is_check='+checked+'&&request_amount='+request_amount;
    serverRequestResponseEdit(url,params,method,content_div,setActivityResponse)
}

function setActivityResponse(result,content_div)
{
  $('#'+content_div).html(result);
  var activity_number = {{session('payment_request.counter')}}
  $('#payment_activity_counter').html($('#activity_counter').html());
}

/**
 * @Date: 2020-05-27 18:03:12
 * @Desc: Validate and submit the form
 */
function validateSubmit(form_id)
{
  if(validation(form_id))
  {
    $("#"+form_id).submit();
  }
}

function OnCheckBoxChange(id){
    if ($('#'+id).is(':checked')) {
        $("#billOfQuantity").hide();
        $("#operation_type").show();
    }
    else{
        $("#billOfQuantity").show();
        $("#operation_type").hide();
    }
}

function OnCheckBoxChangedit(id){
    if ($('#'+id).is(':checked')) {
        $("#billOfQuantityedit").hide();
        $("#operation_type_edit").show();
    }
    else{
        $("#billOfQuantityedit").show();
        $("#operation_type_edit").hide();
    }
}

function OnStartStopChange(id){
    if ($('#'+id).is(':checked')) {
        $("#startdatediv").show();
        $("#enddatediv").hide();
    }
    else{
        $("#startdatediv").hide();
        $("#enddatediv").show();
    }
}

function OnStartStopChangeEdit(id){
    if ($('#'+id).is(':checked')) {
        $("#startdatedivedit").show();
        $("#enddatedivedit").hide();
    }
    else{
        $("#startdatedivedit").hide();
        $("#enddatedivedit").show();
    }
}

// Display users Dropdown
function getBillOfQuantities(id,url,targetDiv)
{
  var record_id = $('#'+id).val();
  $.ajax({
      url: url,
      data: {
          "_method"     : 'GET',
          "id"          : record_id,
      },
      type: 'post',
      beforeSend: function(){
          $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
          $('#'+targetDiv).html(response);
      }
  });
}

// get Bill of Quantities date
function getBillOfQuantitiesDate(id,url)
{
  var record_id = $('#'+id).val();
  $.ajax({
      url: url,
      data: {
          "_method"     : 'GET',
          "id"          : record_id,
      },
      type: 'post',
      success: function(response)
      {
          $('#start_date').val(response);
      }
  });
}

// get Bill of Quantities edit
function getBillOfQuantitiesDateEdit(id,url)
{
  var record_id = $('#'+id).val();
  $.ajax({
      url: url,
      data: {
          "_method"     : 'GET',
          "id"          : record_id,
      },
      type: 'post',
      success: function(response)
      {
          $('#start_date_time_edit').val(response);
          $('#start_date_time_val_edit').val(response);
      }
  });
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-07-25 13:27:40
 * @Desc:  replace url with js
 */
function replaceUrl(url)
{
  window.location.replace(url);
}

// Store Records
function makeBase(url,params,method,response_div,msg)
{
  swal({
      text: '{{ trans('global.conf_msg_base') }}',
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
     e.value && serverRequestResponse(url,params,method,response_div,redirectFunction);
  });
}

function relocate_home(url)
{
     location.href = url;
}

// Payment request function
function payment_request(url,params,method,response_div)
{
  if(params=='')
  {
     swal({
            title: "{{trans('payment.info')}}",
            text: "{{trans('payment.select_proj_location')}}",
            type: "warning",
        });
  }
  else
  {
    serverRequest(url,params,method,response_div);
  }
}

// Display users Dropdown
function display_content(id,pro_id,loc_id,url,method,targetDiv,newFile,existFile)
{
  if ($('#'+id).is(':checked')){
    viewFile = newFile;
  }else{
    viewFile = existFile;
  }
  $.ajax({
      url: url,
      data: {
        "_method"  : method,
        "viewFile" : viewFile,
        "pro_id"   : pro_id,
        "loc_id"   : loc_id,
      },
      type: method,
      beforeSend: function(){
        $('#'+targetDiv).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $('#'+targetDiv).html(response);
      }
  });
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-08-15 14:39:46
 * @Desc: Sever request with confirmation message
 */
function confirmServerRequest(url,params,method,response_div,msg)
{
  swal({
      text: msg,
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
     e.value && serverRequest(url,params,method,response_div);
  });
}

function holidaysContent(id,fDiv,sDiv){
  if ($('#'+id).is(':checked'))
  {
      $("#"+fDiv).addClass('d-none');
      $("#"+sDiv).removeClass('d-none');
  }
  else
  {
      $("#"+sDiv).addClass('d-none');
      $("#"+fDiv).removeClass('d-none');
  }
}

function aside_display(id)
{
  if($('#'+id).hasClass('d-none')){
    $('#'+id).removeClass('d-none');
  }else{
    $('#'+id).addClass('d-none');
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-08-20 14:18:47
 * @Desc: change request, close modal and call the date
 */
function getChangeRequest(response)
{
    $('#'+response.modal_id).modal('toggle');
    $('.modal-backdrop ').remove();
    var params = 'project_id='+response.project_id+'&&project_location_id='+response.project_location_id+'&&dep_id='+response.dep_id;
    var route = 'list_cost_scope';
    if(response.route!='')
    route = response.route;
    serverRequest(response.route,params,response.method,'list_content',false);
}

function add_owner(id,urn)
{
  $('#rec_id').val(id);
  $('#rec_urn').val(urn);
}

function list_owner(url,div)
{
  $.ajax({
    url: url,
    data: {
      "_method" : 'POST',
    },
    type:'POST',
    success:function(response){
      $('#'+div).html(response);
    }
  });
}

/**
  * @Author: Jamal Yousufi
  * @Date: 2020-11-08 10:15:54
  * @Desc: Get checked valued of check box
  */
function getGroupCheckedValue(elem_name)
{
  var checked_value = [];
  $.each($("input[name='"+elem_name+"']:checked"), function()
  {
    checked_value.push($(this).val());
  });
  if(checked_value.length > 0)
    return checked_value;
  else
    return false;
}

/**
  * @Author: Jamal Yousufi
  * @Date: 2020-11-08 10:15:41
  * @Desc:  get serverside data based on check values
  */
function getDepartmentModules(url,elem_id,method,div_id,user_id=0)
{
  if($('#'+elem_id).prop('checked')){
    var params = 'departments_id='+$('#'+elem_id).val()+'&user_id='+user_id;
    serverRequestData(url,params,method,div_id);
  }else{
    var departments_id = $('#'+elem_id).val();
    $('#dep_content_'+departments_id).remove();
  }
}

function serverRequestData(url,params,method,response_div)
{
  $.ajax({
    url: url,
    data:params,
    type:method,
    success: function(response)
    {
      $('#'+response_div).append(response);
    },
    cache: false,
    processData: false
  })
}

function getModuleSections(url,elem_name,method,target_div,user_id=0)
{
  var cheked_value = getGroupCheckedValue(elem_name);
  var params = 'modules_id='+cheked_value+'&user_id='+user_id;
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      $('#'+target_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
    },
    success: function(response)
    {
      $('#'+target_div).html(response);
    },
    cache: false,
    processData: false
  });
}

function getSectionRoles(url,elem_name,method,target_div,user_id=0)
{
  var cheked_value = getGroupCheckedValue(elem_name);
  var params = 'sections='+cheked_value+'&user_id='+user_id;
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      $('#'+target_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
    },
    success: function(response)
    {
      $('#'+target_div).html(response);
    },
    cache: false,
    processData: false
  });
}

function disableBtnAfterSubmit(btn)
{
  var fewSeconds = 5;
  $('#'+btn).attr('disabled','disabled');
  setTimeout(function(){
    $('#'+btn).removeAttr('disabled');
  }, fewSeconds*1000);
}

function projectList(url,div)
{
  $.ajax({
    url: url,
    data: '',
    type:'GET',
    success:function(response){
      $('#'+div).html(response);
    }
  });

}

/**
  * @Author: Jamal Yousufi
  * @Date: 2020-12-14 10:36:16
  * @Desc: Filter payment max value
  */
  function filterPaymentMaxValue(total_value=0,requested_val=0,element)
  {

    var typed_value = element.value;
    var requestable  = total_value - requested_val;
    if(typed_value > requestable)
    {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "<?=$lang=='en'? 'toast-top-right': 'toast-top-left'?>",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      toastr.error("{{trans('payment.greater_request_err')}}","{{trans('global.error')}}");
      element.value = requestable;
    }
  }

function confirmOperation(btn)
{
  swal({
      text: '{{ trans('global.conf_msg') }}',
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && $('#'+btn+ ' span').trigger("click");
  });
}

function setBackground(target,dep_id,color)
{
  if($('#'+dep_id).prop('checked')){
    $('#'+target).addClass(color);
  }else{
    $('#'+target).removeClass(color);
  }
}

function putValue(id,target)
{
  $('#'+target).html($('#'+id).val());
}

function bringDataById(url,val,method,div,target)
{
  $.ajax({
      url: url,
      data: {
        "_method" : method,
        "id"      : val,
        "target"  : target,
      },
      type: method,
      success: function(response)
      {
        $("#"+div).html(response);
      }
  });
}

function setContentToDiv(url,method,div)
{
  $.ajax({
    url: url,
    data: {
      "_method" : method,
    },
    type: method,
    success:function(response)
    {
      $('#'+div).html(response);
      $(".datePicker").css('width','100%');
      @if($lang=="en")
        $(".datePicker").attr('type', 'date');
      @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
      @endif
      $('.select-2').select2();
    }
  });
}

function checkForProgress(url,id,pro_id,div)
{
  var category = $('#'+id).val();
  $.ajax({
    url: url,
    data: {
      "_method"  : 'POST',
      "category" : category,
      "pro_id"   : pro_id,
    },
    type: 'POST',
    success:function(response)
    {
      $('#'+div).html(response);
    }
  });
}

function enforceMinMax(el){
  if(el.value != ""){
    if(parseInt(el.value) < parseInt(el.min)){
      el.value = el.min;
    }
    if(parseInt(el.value) > parseInt(el.max)){
      el.value = el.max;
    }
  }
}

// Display content based on selected option
function bringContent(id,firstDiv,secondDiv){
    if($('#'+id).val()==1)
    {
        $("#"+firstDiv).addClass('d-none');
        $("#"+secondDiv).removeClass('d-none');
    }
    else
    {
        $("#"+secondDiv).addClass('d-none');
        $("#"+firstDiv).removeClass('d-none');
    }
}
</script>
@stack('custom-js')

