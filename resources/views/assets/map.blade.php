
<script type="text/javascript">
    window.onload = init;
    function init(){
        //Point Feature
        var pointFeature = new ol.Feature(new ol.geom.Point([69.160652,34.543896]))

        const iconFeature = new ol.Feature({
            geometry: new ol.geom.Point([61.160652,34.543896]),
            name: 'Somewhere near Nottingham',
        });

        console.log($locations);

        var container = document.getElementById('popup');
        var content = document.getElementById('popup-content');
        var closer = document.getElementById('popup-closer');
        var overlay = new ol.Overlay({
            element: container,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });

        closer.onclick = function() {
            overlay.setPosition(undefined);
            closer.blur();
            return false;
        };

        const map = new ol.Map({
            target: 'map',
            controls: ol.control.defaults().extend([
                new ol.control.MousePosition({
                    coordinateFormat: ol.coordinate.createStringXY(3),
                    projection: 'EPSG:4326',
                    undefinedHTML: '&nbsp;',
                    //className: 'custom-mouse-position',
                    //target: document.getElementById('custom-mouse-position'),
                })
            ]),
            view:new ol.View({
                    extent:[60.48761, 29.37063, 74.90017, 38.50674],
                    center: [69.160652,34.543896],
                    projection: 'EPSG:4326',
                    zoom:4
                }),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM(),
                }),
                new ol.layer.Vector({
                    source: new ol.source.Vector({
                        features: [iconFeature]
                    }),
                    style: new ol.style.Style({
                        image: new ol.style.Icon({
                            anchor: [0.5, 46],
                            anchorXUnits: 'fraction',
                            anchorYUnits: 'pixels',
                            src: '{{ asset("assets/ol/marker.png") }}'
                        })
                    })
                })
            ],
            overlays: [overlay],
            target: 'map'
        });

        // Add an event handler for the map "singleclick" event
        map.on('singleclick', function(evt) {
            var coordinate = evt.coordinate;
            content.innerHTML = '<p>Location:</p><code>' + coordinate +
                '</code>';
            overlay.setPosition(coordinate);
        });
    }
    </script>
