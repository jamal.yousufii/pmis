<!-- Info: Start -->
<div class="row mt-1">
    <div class="col-lg-12 pb-1">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-box m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{ trans('national_projects.project' )}}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">{{ $project->name }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-symbol m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{ trans('national_projects.contractor') }}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">{{ $project->contractor }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-2 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('progress.contraction_duration')}}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">{{ differenceBetweenDate($project->end_date,$project->end_date,'%a',true) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-3 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info" style="width: 100%">
                                <span class="m-widget21__title">{{trans('national_projects.progress_planned')}}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">
                                    <div class="d-flex flex-column w-100">
                                        <div class="row">     
                                            <span class="text-muted font-size-sm col-lg-9 col-md-9 col-sm-9 col-xs-9" style="font-size:1rem;"></span>
                                            <span class="text-muted font-size-sm col-lg-3 col-md-3 col-sm-3 col-xs-3" style="font-size:1rem;">{{$progress_planned}} %</span>
                                        </div>
                                        <div class="progress rounded">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress_planned}}%;" aria-valuenow="{{$progress_planned}}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-3 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info" style="width: 100%">
                                <span class="m-widget21__title">{{trans('national_projects.progress_done')}} ({{trans('national_projects.work_progress')}})</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">
                                    <div class="d-flex flex-column w-100">
                                        <div class="row">     
                                            <span class="text-muted font-size-sm col-lg-9 col-md-9 col-sm-9 col-xs-9" style="font-size:1rem;"></span>
                                            <span class="text-muted font-size-sm col-lg-3 col-md-3 col-sm-3 col-xs-3" style="font-size:1rem;">{{$progress_done}} %</span>
                                        </div>
                                        <div class="progress rounded">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress_done}}%;" aria-valuenow="{{$progress_done}}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Info: End -->