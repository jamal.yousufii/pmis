{{-- Start of the Modal --}}
<div id="stopModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="stopModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
                <!--begin::head-->
                <div class="m-portlet__head table-responsive bg-dark">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem"><span><i class="fl flaticon-danger"></i> <span>{{ trans('national_projects.project_stop') }}</span></span></h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Head-->
            </div>
            <div class="m-portlet__body">
                <!--begin::content-->
                <form class="m-form m-form--label-align-right" enctype="multipart/form-data" id="stopdForm">
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            <label class="">{{ trans('national_projects.activity_desc') }}: <span style="color:red;">*</span></label>
                            <textarea class="form-control m-input m-input--air errorDiv" name="stop_status_desc" id="stop_status_desc" rows="4"></textarea>
                            <div class="stop_status_desc error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            <input name="national_project_id" type="hidden" value="{{ session('national_project_id') }}"/>
                            <input name="status" type="hidden" value="2"/>
                            <button type="button" onclick="storeRecord('{{route('constructions.project_stop')}}','stopdForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
                            <button type="button" onclick="redirectFunction()" class="btn btn-warning">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
</div>