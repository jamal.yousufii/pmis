<div class="m-wizard__form m-portlet--head-sm">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="fl flaticon-file-2"></i></span>
                <h3 class="m-portlet__head-text">{{trans('national_projects.work_progress')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRole("npmis_constructions","ncon_edit") and $record and $project->status==0)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="far fa-edit"></i><span>{{ trans('global.edit') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @alert()
    @endalert
    <div class="code notranslate cssHigh collapse" id="collapseEdit">
        <!--begin:: Edit Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="workProgressEditForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('national_projects.report_date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePicker errorDiv" type="text" name="report_date" id="report_date" value="{{dateCheck($record->report_date,$lang)}}">
                        <div class="report_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('national_projects.progress_planned') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="number" min="0" max="{{ 100-$progress_planned }}" step="0.1" name="work_progress_planned" id="work_progress_planned" value="{{$record->progress_planned}}" onchange="enforceMinMax(this)" onkeyup="enforceMinMax(this)">
                        <div class="work_progress_planned error-div" style="display:none;"></div>
                        <span class="m-form__help">{{ trans('national_projects.progress_planned_desc') }}</span>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('national_projects.progress_done') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input errorDiv" type="number" min="0" max="{{ 100-$progress_done }}" step="0.1" name="work_progress_done" id="work_progress_done" value="{{$record->progress_done}}" onchange="enforceMinMax(this)" onkeyup="enforceMinMax(this)">
                        <div class="work_progress_done error-div" style="display:none;"></div>
                        <span class="m-form__help">{{ trans('national_projects.progress_done_desc') }}</span>
                    </div>
                    <div class="col-lg-12 pt-2">
                        <label class="title-custom">{{ trans('national_projects.description') }}:</label>
                        <textarea class="form-control m-input m-input--air" name="description" rows="2">{{$record->description}}</textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-12">
                        <input type="hidden" name="id" value="{{$record->id}}">
                        <button type="button" onclick="storeData('{{route('constructions.update_progress')}}','workProgressEditForm','POST','show_record',put_content);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>            
                        <button type="button" class="btn btn-warning"  id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </form>
        <!--end:: Edit Form-->
        <hr>
    </div>
    <!--begin:: View Record-->
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('national_projects.report_date') }}: </label><br>
                    <span>{{ dateCheck($record->report_date,$lang) }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('national_projects.progress_planned') }}: </label><br>
                    <span>{{ $record->progress_planned }} %</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('national_projects.progress_done') }}: </label><br>
                    <span>{{ $record->progress_done }} %</span>
                </div>
                <div class="col-lg-12 pt-2">
                    <label class="title-custom">{{ trans('national_projects.description') }}: </label><br>
                    <span>{{ $record->description }}</span>
                </div>
            </div>
        </div>
    </div>
    <!--end:: View Record-->
</div>