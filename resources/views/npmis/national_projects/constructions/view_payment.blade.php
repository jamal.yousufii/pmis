<div class="m-wizard__form m-portlet--head-sm">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="far fa-money-bill-alt"></i></span>
                <h3 class="m-portlet__head-text">{{trans('national_projects.payment')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRole("npmis_constructions","ncon_edit") and $record->count()>0 and $project->status==0)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="far fa-edit"></i><span>{{ trans('global.edit') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @alert()
    @endalert
    <!--begin:: Edit Form-->
    <div class="code notranslate cssHigh collapse" id="collapseEdit">
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="paymentEditForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                @foreach($record as $item)
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-6">
                            @if($loop->first)
                                <label class="title-custom">{{ trans('national_projects.payment_type') }}: <span style="color:red;">*</span></label><br>
                            @endif
                            <div id="div_payment_id" class="errorDiv">
                                <select class="form-control select-2 m-input errorDiv required" name="payment_id[]" id="payment_id" style="width: 100%">
                                    <option value="">{{trans('global.select')}}</option>
                                    @if($payments)
                                        @foreach($payments as $equ)
                                            <option value="{{$equ->id}}" <?=$equ->id==$item->payment_id ? 'selected':''?> >{{ $equ->{'name_'.$lang} }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>    
                            <div class="payment_id error-div" style="display:none;"></div>
                        </div>
                        <div class="col-lg-4">
                            @if($loop->first)
                                <label class="title-custom">{{ trans('national_projects.payment_amount') }}: <span style="color:red;">*</span></label>
                            @endif    
                            <input class="form-control m-input errorDiv" type="number" min="0" name="amount[]" id="amount" value="{{$item->amount}}">
                            <div class="amount error-div" style="display:none;"></div>
                        </div>
                        <div class="col-lg-2">.
                            @if($loop->first)
                                <label class="title-custom">&nbsp;</label><br>
                                <button type="button" onclick="add_more('target_div','{{route('morePayment')}}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm">
                                    <i class="fa fa-plus" style="font-size:10px;"></i>
                                </button>
                            @endif
                        </div>
                    </div>
                @endforeach
                <div id="target_div"></div><!-- Display mor activities  -->
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-12">
                        <input type="hidden" name="national_project_id" value="{{ session('national_project_id') }}">
                        <input type="hidden" name="national_progress_id" value="{{ session('report_id') }}">
                        <button type="button" onclick="storeData('{{route('constructions.update_payment')}}','paymentEditForm','POST','show_record',put_content);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>            
                        <button type="button" class="btn btn-warning"  id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </form> 
    </div>
    <!--end:: Edit Form-->
    <!--begin:: View Record-->
    <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
            <tr style="background:#f4f5f8">
                <th width="10%">{{ trans('global.number') }}</th>
                <th width="30%">{{ trans('national_projects.payment_type') }}</th>
                <th width="20%">{{ trans('national_projects.payment_amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($record as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->payments->{'name_'.$lang} }}</td>
                    <td>{{ $item->amount }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--end:: View Record-->
</div>