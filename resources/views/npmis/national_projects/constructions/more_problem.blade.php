<div class="form-group m-form__group row m-form__group_custom" id="problem_div_{!!$number!!}">
    <div class="col-lg-5">
        <label class="title-custom">{{ trans('national_projects.problem_type') }}: <span style="color:red;">*</span></label><br>
        <div id="div_problem_id_{!!$number!!}" class="errorDiv">
            <select class="form-control select-2 m-input errorDiv required" name="problem_id[]" id="problem_id_{!!$number!!}" style="width: 100%">
                <option value="">{{trans('global.select')}}</option>
                @if($problems)
                    @foreach($problems as $item)
                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                    @endforeach
                @endif
            </select>
        </div>    
        <div class="problem_id error-div" style="display:none;"></div>
        <br>
        <label class="title-custom">{{ trans('national_projects.payment_amount') }}: <span style="color:red;">*</span></label>
        <div id="div_status_{!!$number!!}" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="status[]" id="status_{!!$number!!}">
            <option value="">{{ trans('national_projects.select') }}</option>
            <option value="0">{{ trans('national_projects.low') }}</option>
            <option value="1">{{ trans('national_projects.medium') }}</option>
            <option value="2">{{ trans('national_projects.heigh') }}</option>
            </select>
        </div>
        <div class="status error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-6">
        <label class="title-custom">{{ trans('national_projects.problem_desc') }}:</label>
        <textarea class="form-control m-input m-input--air" name="description[]" id="description_{!!$number!!}" rows="6"></textarea>
    </div>
    <div class="col-lg-1">
        <label class="title-custom">&nbsp;</label><br>
        <button type="button" id="problem_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('problem_div_{{$number}}',{{$number}},'problem_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>