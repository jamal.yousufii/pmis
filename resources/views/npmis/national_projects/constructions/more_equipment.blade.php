<div id="equipment_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-4">
        <div id="div_type_{!!$number!!}" class="errorDiv">
            <select class="form-control select-2 m-input errorDiv required" name="type[]" id="type_{!!$number!!}" style="width: 100%">
                <option value="">{{trans('global.select')}}</option>
                @if($equipment)
                    @foreach($equipment as $item)
                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                    @endforeach
                @endif
            </select>
        </div>    
        <div class="type error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_exist[]" id="equipment_exist_{!!$number!!}">
        <div class="equipment_exist error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_needed[]" id="equipment_needed_{!!$number!!}">
        <div class="equipment_needed error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-1">
        <button type="button" id="equipment_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('equipment_div_{{$number}}',{{$number}},'equipment_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>