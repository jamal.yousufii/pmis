<div class="m-content" id="resutl_div">
    <div class="m-portlet m-portlet--full-height">
        <!--begin: Portlet Body-->
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                <div class="row m-row--no-padding">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m--padding-top-10 m--padding-bottom-15 main-border-div">
                        <div class="m-wizard__head p-3" style="padding: 1rem 2rem !important">
                            <div class="m-wizard__nav">
                                @if($tabs)
                                    @php $x=1; @endphp
                                    @foreach($tabs as $item)
                                        @if(check_my_section(array(departmentid()),$item->code))
                                            <div class="m-wizard__steps mt-1">
                                                <a href="javascript:void()" onclick="viewRecord('{{route($item->url_route)}}','id={{session('national_project_id')}}','POST','modal_content')" class="m-menu__link" style="text-decoration: none;">
                                                    <div class="m-wizard__step m-wizard__step--done m-wizard__step--current mt-3 mb-2">
                                                        <div class="m-wizard__step-info">
                                                            <span class="m-wizard__step-number title-custom"><span class="text-white number <?=session('current_sub_tab')==$item->code ?'bg-success':'bg-info' ?>">{{$x}}</span></span>
                                                            <div class="m-wizard__step-label m-wizard-title"><span class="{{$item->icon}} px-1"></span><span> {{ $item->name }} </span></div>
                                                            @if(session('current_sub_tab')==$item->code)
                                                                <div class="m-wizard__step-icon m-wizard__step-icon-custom p-2 tickClass">
                                                                    <i class="la la-check text-success text-white"></i>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            @if(!$loop->last)<hr class="p-0 m-0">@endif
                                            @php $x++; @endphp
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 main-border-div" id="show_record">
                        <div class="m-wizard__form m-portlet--head-sm">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon"><i class="la flaticon-truck"></i></span>
                                        <h3 class="m-portlet__head-text">{{trans('national_projects.equipment')}}</h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        @if(doIHaveRole("npmis_constructions","ncon_edit") and $record->count()>0 and $project->status==0)
                                            <li class="m-portlet__nav-item">
                                                <a class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">
                                                    <span><i class="far fa-edit"></i><span>{{ trans('global.edit') }}</span></span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <!-- Add Report -->
                            <div class="code notranslate cssHigh collapse show">
                                @if($progress)
                                    @if($record->count()==0)
                                        <!--begin:: Add Form-->
                                        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="equipmentForm" enctype="multipart/form-data">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row m-form__group_custom">
                                                    <div class="col-lg-4">
                                                        <label class="title-custom">{{ trans('national_projects.equipment_type') }}: <span style="color:red;">*</span></label><br>
                                                        <div id="div_type" class="errorDiv">
                                                            <select class="form-control select-2 m-input errorDiv required" name="type[]" id="type" style="width: 100%">
                                                                <option value="">{{trans('global.select')}}</option>
                                                                @if($equipment)
                                                                    @foreach($equipment as $item)
                                                                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>    
                                                        <div class="type error-div" style="display:none;"></div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label class="title-custom">{{ trans('national_projects.equipment_needed') }}: <span style="color:red;">*</span></label>
                                                        <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_needed[]" id="equipment_needed">
                                                        <div class="equipment_needed error-div" style="display:none;"></div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label class="title-custom">{{ trans('national_projects.equipment_exist') }}: <span style="color:red;">*</span></label>
                                                        <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_exist[]" id="equipment_exist">
                                                        <div class="equipment_exist error-div" style="display:none;"></div>
                                                    </div>
                                                    <div class="col-lg-1"></div>
                                                    <div class="col-lg-1">
                                                        <label class="title-custom">&nbsp;</label><br>
                                                        <button type="button" onclick="add_more('target_div','{{route('moreEquipment')}}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm">
                                                            <i class="fa fa-plus" style="font-size:10px;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div id="target_div"></div><!-- Display mor activities  -->
                                                <div class="form-group m-form__group row m-form__group_custom">
                                                    <div class="col-lg-12">
                                                        <input type="hidden" name="national_project_id" value="{{ session('national_project_id') }}">
                                                        <input type="hidden" name="national_progress_id" value="{{ session('report_id') }}">
                                                        <button type="button" onclick="storeRecord('{{route('constructions.store_equipment')}}','equipmentForm','POST','show_record',put_content);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
                                                        <button type="button" onclick="redirectFunction()" class="btn btn-warning">{{ trans('global.cancel') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!--end:: Add Form-->
                                    @else
                                        <!--begin:: Edit Form-->
                                        <div class="code notranslate cssHigh collapse" id="collapseEdit">
                                            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="equipmentEditForm" enctype="multipart/form-data">
                                                <div class="m-portlet__body">
                                                    @foreach($record as $item)
                                                        <div class="form-group m-form__group row m-form__group_custom">
                                                            <div class="col-lg-4">
                                                                @if($loop->first)
                                                                    <label class="title-custom">{{ trans('national_projects.equipment_type') }}: <span style="color:red;">*</span></label><br>
                                                                @endif
                                                                <div id="div_type" class="errorDiv">
                                                                    <select class="form-control select-2 m-input errorDiv required" name="type[]" id="type" style="width: 100%">
                                                                        <option value="">{{trans('global.select')}}</option>
                                                                        @if($equipment)
                                                                            @foreach($equipment as $equ)
                                                                                <option value="{{$equ->id}}" <?=$equ->id==$item->type ? 'selected':''?> >{{ $equ->{'name_'.$lang} }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>    
                                                                <div class="type error-div" style="display:none;"></div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                @if($loop->first)
                                                                    <label class="title-custom">{{ trans('national_projects.equipment_needed') }}: <span style="color:red;">*</span></label>
                                                                @endif    
                                                                <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_needed[]" id="equipment_needed" value="{{$item->equipment_needed}}">
                                                                <div class="equipment_needed error-div" style="display:none;"></div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                @if($loop->first)
                                                                    <label class="title-custom">{{ trans('national_projects.equipment_exist') }}: <span style="color:red;">*</span></label>
                                                                @endif
                                                                <input class="form-control m-input errorDiv" type="number" min="0" name="equipment_exist[]" id="equipment_exist" value="{{$item->equipment_exist}}">
                                                                <div class="equipment_exist error-div" style="display:none;"></div>
                                                            </div>
                                                            <div class="col-lg-1"></div>
                                                            <div class="col-lg-1">
                                                                @if($loop->first)
                                                                    <label class="title-custom">&nbsp;</label><br>
                                                                    <button type="button" onclick="add_more('target_div','{{route('moreEquipment')}}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm">
                                                                        <i class="fa fa-plus" style="font-size:10px;"></i>
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <div id="target_div"></div><!-- Display mor activities  -->
                                                    <div class="form-group m-form__group row m-form__group_custom">
                                                        <div class="col-lg-12">
                                                            <input type="hidden" name="national_project_id" value="{{ session('national_project_id') }}">
                                                            <input type="hidden" name="national_progress_id" value="{{ session('report_id') }}">
                                                            <button type="button" onclick="storeData('{{route('constructions.update_equipment')}}','equipmentEditForm','POST','show_record',put_content);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>            
                                                            <button type="button" class="btn btn-warning"  id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">{{ trans('global.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                        <!--end:: Edit Form-->
                                        <!--begin:: View Record-->
                                        <table class="table table-striped- table-bordered table-hover table-checkable">
                                            <thead>
                                                <tr style="background:#f4f5f8">
                                                    <th width="10%">{{ trans('global.number') }}</th>
                                                    <th width="30%">{{ trans('national_projects.equipment_type') }}</th>
                                                    <th width="20%">{{ trans('national_projects.equipment_needed') }}</th>
                                                    <th width="20%">{{ trans('national_projects.equipment_exist') }}</th>
                                                    <th width="20%">{{ trans('national_projects.equipment_not_exist') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($record as $item)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $item->equipment->{'name_'.$lang} }}</td>
                                                        <td>{{ $item->equipment_needed }}</td>
                                                        <td>{{ $item->equipment_exist }}</td>
                                                        <td>{{ $item->equipment_needed - $item->equipment_exist }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!--end:: View Record-->
                                    @endif
                                @else
                                    <!--begin::Alert div -->
                                    <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                                        <div class="m-alert__icon"><i class="la la-warning"></i></div>
                                        <div class="m-alert__text">
                                            <strong>{{trans('global.notice')}}:</strong> {{trans('national_projects.required_report')}}
                                        </div>
                                    </div>  
                                    <!--End::Alert div -->              
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>