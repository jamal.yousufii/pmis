<div class="m-wizard__form m-portlet--head-sm">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="flaticon-edit-1"></i></span>
                <h3 class="m-portlet__head-text">{{trans('national_projects.mian_problems')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRole("npmis_constructions","ncon_edit") and $record->count()>0 and $project->status==0)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="far fa-edit"></i><span>{{ trans('global.edit') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @alert()
    @endalert
    <!--begin:: Edit Form-->
    <div class="code notranslate cssHigh collapse" id="collapseEdit">
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="problemsEditForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                @foreach($record as $item)
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-5">
                            <label class="title-custom">{{ trans('national_projects.problem_type') }}: <span style="color:red;">*</span></label><br>
                            <div id="div_problem_id" class="errorDiv">
                                <select class="form-control select-2 m-input errorDiv required" name="problem_id[]" id="problem_id-{{$loop->iteration}}" style="width: 100%">
                                    <option value="">{{trans('global.select')}}</option>
                                    @if($problems)
                                        @foreach($problems as $pro)
                                            <option value="{{$pro->id}}" <?=$item->problem_id==$pro->id ? 'selected' : ''?>>{{ $pro->{'name_'.$lang} }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>    
                            <div class="problem_id error-div" style="display:none;"></div>
                            <br>
                            <label class="title-custom">{{ trans('national_projects.problem_status') }}: <span style="color:red;">*</span></label>
                            <div id="div_status" class="errorDiv">
                                <select class="form-control m-input m-input--air select-2 required" name="status[]" id="status-{{$loop->iteration}}" style="width: 100%">
                                <option value="">{{ trans('national_projects.select') }}</option>
                                <option value="0" <?=$item->status=='0' ? 'selected' : ''?>>{{ trans('national_projects.low') }}</option>
                                <option value="1" <?=$item->status=='1' ? 'selected' : ''?>>{{ trans('national_projects.medium') }}</option>
                                <option value="2" <?=$item->status=='2' ? 'selected' : ''?>>{{ trans('national_projects.heigh') }}</option>
                                </select>
                            </div>
                            <div class="status error-div" style="display:none;"></div>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('national_projects.problem_desc') }}:</label>
                            <textarea class="form-control m-input m-input--air" name="description[]" id="description" rows="6">{{$item->description}}</textarea>
                        </div>
                        <div class="col-lg-1">
                            @if($loop->first)
                                <label class="title-custom">&nbsp;</label><br>
                                <button type="button" onclick="add_more('target_div','{{route('moreProblem')}}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm">
                                    <i class="fa fa-plus" style="font-size:10px;"></i>
                                </button>
                            @endif
                        </div>
                    </div>
                @endforeach
                <div id="target_div"></div><!-- Display mor activities  -->
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-12">
                        <input type="hidden" name="national_project_id" value="{{ session('national_project_id') }}">
                        <input type="hidden" name="national_progress_id" value="{{ session('report_id') }}">
                        <button type="button" onclick="storeData('{{route('constructions.update_problems')}}','problemsEditForm','POST','show_record',put_content);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>            
                        <button type="button" class="btn btn-warning"  id="collapseEditBtnEdit" data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseDiv">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </form>
        <hr>
    </div>
    <!--end:: Edit Form-->
    <!--begin:: View Record-->
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                    <tr style="background:#f4f5f8">
                        <th width="5%">{{ trans('global.number') }}</th>
                        <th width="15%">{{ trans('national_projects.problem_type') }}</th>
                        <th width="15%">{{ trans('national_projects.problem_status') }}</th>
                        <th width="65%">{{ trans('national_projects.problem_desc') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($record as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->problems->{'name_'.$lang} }}</td>
                            <td>
                                @if($item->status=='0')
                                    <span class="m-badge  m-badge--success m-badge--wide"><strong>{{ trans('national_projects.low') }}</strong></span>
                                @elseif($item->status=='1')
                                    <span class="m-badge  m-badge--warning m-badge--wide"><strong>{{ trans('national_projects.medium') }}</strong></span>
                                @elseif($item->status=='2')
                                    <span class="m-badge m-badge--danger m-badge--wide"><strong>{{ trans('national_projects.heigh') }}</strong></span>
                                @endif
                            </td>
                            <td>{{ $item->description }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!--end:: View Record-->
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>