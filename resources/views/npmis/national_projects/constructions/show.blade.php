@extends('master')
@section('head')
  <title>{{ trans('national_projects.projects') }}</title>
@endsection
@section('content')
  <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand mb-0" role="tablist">
    @if($tabs)
      @foreach($tabs as $item)
        @if(check_my_section(array(departmentid()),$item->code))
          <li class="nav-item m-tabs__item">
            <a href="{{ route($item->url_route, $id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
              <span style="font-size: 1.4rem;"><span class="{{$item->icon}} px-2"></span><strong>{{ $item->name }}</strong></span>
            </a>
          </li>
        @endif
      @endforeach
    @endif
  </ul>
  <div class="m-portlet m-portlet--full-height dropdown__inner mb-3" id="content_div">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title"><h3 class="m-portlet__head-text">{{ trans('national_projects.reports') }}</h3></div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("npmis_constructions","ncon_add"))
            <li class="m-portlet__nav-item">
              <a class="btn btn-info btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" onclick="setContentToDiv('{{route('constructions.create_progress')}}','POST','modal_content')" data-toggle="modal" data-target="#reportPopup">
                <span><i class="fas fa-plus-circle"></i><span>{{ trans('global.add') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('national_project') }}">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  @include('npmis.national_projects.info')
  <!-- Edit popup: Start -->
  <div id="reportPopup" class="modal fade bd-example-modal-full" role="dialog" aria-labelledby="reportPopupLable" aria-hidden="true">
      <div class="modal-dialog modal-full">
          <div class="modal-content">           
              <div class="modal-header bg-color-dark">
                  <div class="m-portlet__head-title">
                      <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem"><span class="fas fa-layer-group px-2"></span> {{ trans('national_projects.reports') }}</h3>
                  </div>
                  <button type="button" class="close text-white" onclick="redirectFunction()" id="close"><span aria-hidden="true">&times;</button>
              </div>
              <div class="m-content pt-1" id="modal_content"></div>
          </div>
      </div>
  </div>
  <!-- Edit popup: End -->
  <div class="m-portlet m-portlet--full-height dropdown__inner">
    @if($records->count()>0)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-0">
            <div class="m-portlet__body p-4 pt-1">
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                        <tr style="background:#f4f5f8">
                            <th width="8%">{{ trans('global.number') }}</th>
                            <th width="20%">{{ trans('national_projects.report_urn') }}</th>
                            <th width="20%">{{ trans('national_projects.report_date') }}</th>
                            <th width="40%">{{ trans('national_projects.progress') }}</th>
                            <th width="17%" class="text-center">{{ trans('global.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->urn }}</td>
                                <td>{{ dateCheck($item->report_date,$lang) }}</td>
                                <td>
                                    <div class="d-flex flex-column w-100 mr-2">
                                        <div class="row">     
                                            <span class="text-muted font-size-sm col-lg-10 col-md-10 col-sm-10 col-xs-10" style="font-size:1rem;">{{trans('national_projects.progress')}}</span>
                                            <span class="text-muted font-size-sm col-lg-2 col-md-2 col-sm-2 col-xs-2" style="font-size:1rem;">{{$item->progress}}%</span>
                                        </div>
                                        <div class="progress rounded">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$item->progress}}%;" aria-valuenow="{{$item->progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    @if(doIHaveRole("npmis_constructions","ncon_view"))
                                        <button type="button" onclick="setContentToDiv('{{route('constructions.view', encrypt($item->id))}}','GET','modal_content')" data-toggle="modal" data-target="#reportPopup" class="btn m-btn m-btn--air btn-xs btn-sm btn-secondary">
                                            <i class="fa fa-eye text-info pt-2" style="font-size:15px;"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
      <div class="m-portlet__body row">
        <div class="m-wizard__form">
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                    <dt>{{ trans('global.not_done') }}</dt>
                </div>
            </div>  
        </div>
      </div> 
    @endif
  </div>
@endsection