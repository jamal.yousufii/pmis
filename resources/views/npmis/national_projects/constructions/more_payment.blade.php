<div id="payment_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-6">
        <div id="div_payment_id_{!!$number!!}" class="errorDiv">
            <select class="form-control select-2 m-input errorDiv required" name="payment_id[]" id="payment_id_{!!$number!!}" style="width: 100%">
                <option value="">{{trans('global.select')}}</option>
                @if($payments)
                    @foreach($payments as $item)
                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                    @endforeach
                @endif
            </select>
        </div>    
        <div class="payment_id error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-4">
        <input class="form-control m-input errorDiv" type="number" min="0" name="amount[]" id="amount_{!!$number!!}">
        <div class="amount error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-1">
        <button type="button" id="payment_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('payment_div_{{$number}}',{{$number}},'payment_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>