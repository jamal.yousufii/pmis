<table class="table table-striped- table-bordered table-checkable">
  <thead class="bg-light">
    <tr class="font-title">
      <th width="10%">{{ trans('global.urn') }}</th>
      <th width="24%">{{ trans('national_projects.name') }}</th>
      <th width="10%">{{ trans('national_projects.category') }}</th>
      <th width="10%">{{ trans('national_projects.type') }}</th>
      <th width="17%">{{ trans('national_projects.contractor') }}</th>
      <th width="13%">{{ trans('national_projects.contract_price') }}</th>
      <th width="10%">{{ trans('national_projects.status') }}</th>
      <th width="6%" class="text-center">{{ trans('global.view') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
        @foreach($records AS $rec)
          <?php $enc_id = encrypt($rec->id); ?>
          <tr>
            <td>{{ $rec->urn }}</td>
            <td>{{ $rec->name }}</td>
            <td>{{ $rec->category->{'name_'.$lang} }}</td>
            <td>{{ $rec->project_type->{'name_'.$lang} }}</td>
            <td>{{ $rec->contractor }}</td>
            <td>{{ number_format($rec->contract_price) }}</td>
            <td>
              @if($rec->status==0)
                  <span class="m-badge bg-warning m-badge--wide text-white">{{ trans('national_projects.under_process') }}</span>  
              @elseif($rec->status==1)
                <span class="m-badge bg-success m-badge--wide text-white">{{ trans('national_projects.completed') }}</span>
              @elseif($rec->status==2)
                <span class="m-badge bg-danger m-badge--wide text-white">{{ trans('national_projects.stopped') }}</span>
              @endif
            </td>
            <td class="text-center">              
              @if(doIHaveRole("npmis_projects","npro_view"))
                <a href="{{route('national_projects.show',$enc_id)}}" class="btn m-btn m-btn--air btn-xs btn-sm btn-secondary">
                  <i class="fa fa-server pt-2" style="font-size:15px;"></i>
                </a>
              @endif
            </td>
          </tr>
        @endforeach
      @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>
