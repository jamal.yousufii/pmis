@extends('master')
@section('head')
  <title>{{ trans('national_projects.projects') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{trans('national_projects.list')}}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("npmis_projects","npro_add"))
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" onclick="addRecord('{{route('national_projects.create')}}','','GET','response_div')" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="fas fa-plus-circle"></i><span>{{ trans('global.add') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="field" id="field" style="width:100%;" onchange="bringSearchFiel('{{route('search.option')}}','POST',this.value,'value_input')">
                <option value="">{{ trans('global.select') }}</option>
                <option value="urn">{{ trans('global.urn') }}</option>
                <option value="name">{{ trans('national_projects.name') }} {{ trans('national_projects.project') }}</option>
              </select>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="condition" id="condition" style="width:100%;">
                <option value="=">{{ trans('global.equail') }}</option>
                <option value="like">{{ trans('global.like') }} </option>
              </select>
            </div>
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12" id="value_input">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" name="value" class="form-control m-input" placeholder="{{trans('global.search_dote')}}" id="search_value">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span><i class="la la-search"></i></span>
                </span>
             </div>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <a href="javascript:void;" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air" onclick="searchRecords('{{route('filterNationalProjects')}}','search','POST','searchresult')">
                <span><i class="la la-search"></i><span>{{trans('global.search')}}</span></span>
              </a>
            </div>
          </div>
        </div>
      </form>  
    </div>
    <!-- Filter End -->
    @alert()
    @endalert
    <div class="m-portlet__body" id="searchresult"> 
      <table class="table table-striped- table-bordered table-checkable">
        <thead class="bg-light">
          <tr class="font-title">
            <th width="10%">{{ trans('global.urn') }}</th>
            <th width="24%">{{ trans('national_projects.name') }}</th>
            <th width="10%">{{ trans('national_projects.category') }}</th>
            <th width="10%">{{ trans('national_projects.type') }}</th>
            <th width="17%">{{ trans('national_projects.contractor') }}</th>
            <th width="13%">{{ trans('national_projects.contract_price') }}</th>
            <th width="10%">{{ trans('national_projects.status') }}</th>
            <th width="6%" class="text-center">{{ trans('global.view') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
              @foreach($records AS $rec)
                <?php $enc_id = encrypt($rec->id); ?>
                <tr>
                  <td>{{ $rec->urn }}</td>
                  <td>{{ $rec->name }}</td>
                  <td>{{ $rec->category->{'name_'.$lang} }}</td>
                  <td>{{ $rec->project_type->{'name_'.$lang} }}</td>
                  <td>{{ $rec->contractor }}</td>
                  <td>{{ number_format($rec->contract_price) }}</td>
                  <td>
                    @if($rec->status==0)
                        <span class="m-badge bg-warning m-badge--wide text-white">{{ trans('national_projects.under_process') }}</span>  
                    @elseif($rec->status==1)
                      <span class="m-badge bg-success m-badge--wide text-white">{{ trans('national_projects.completed') }}</span>
                    @elseif($rec->status==2)
                      <span class="m-badge bg-danger m-badge--wide text-white">{{ trans('national_projects.stopped') }}</span>
                    @endif
                  </td>
                  <td class="text-center">              
                    @if(doIHaveRole("npmis_projects","npro_view"))
                      <a href="{{route('national_projects.show',$enc_id)}}" class="btn m-btn m-btn--air btn-xs btn-sm btn-secondary">
                        <i class="fa fa-server pt-2" style="font-size:15px;"></i>
                      </a>
                    @endif
                  </td>
                </tr>
              @endforeach
            @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection
@section('js-code')
  <script type="text/javascript">
    $('#search').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });
  </script>
@endsection
