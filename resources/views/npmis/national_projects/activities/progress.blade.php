<label class="title-custom">{{ trans('national_projects.progress') }}:</label>
<div class="input-group">
    <input class="form-control errorDiv" type="number" min="0" max="{{ 100-$progress }}" value="0" name="progress[]" id="progress" onkeyup="enforceMinMax(this)">
    <div class="input-group-append">
        <button class="btn btn-secondary btn-sm" type="button"><strong>%</strong></button>
    </div>
</div>
<div class="progress error-div" style="display:none;"></div>