<div id="activities_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-3">
        <textarea class="form-control tinymce m-input m-input--air errorDiv required" name="activity_desc[]" id="activity_desc_{!!$number!!}" rows="1"></textarea>
        <div class="activity_desc error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-2">
        <input class="form-control m-input datePicker" type="text" name="start_date[]" id="start_date_{!!$number!!}">
    </div>
    <div class="col-lg-2">
        <input class="form-control m-input datePicker" type="text" name="end_date[]" id="end_date_{!!$number!!}">
    </div>
    <div class="col-lg-2">
        <div class="input-group">
            <input class="form-control errorDiv required" type="number" min="0" max="100" value="0" name="progress[]" id="progress_{!!$number!!}">
            <div class="input-group-append">
                <button class="btn btn-secondary btn-sm" type="button"><strong>%</strong></button>
            </div>
        </div>
        <div class="progress error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-2">
        <div id="div_activitie_id" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="status[]" id="status_{!!$number!!}">
                <option value="">{{ trans('national_projects.select') }}</option>
                <option value="0">{{ trans('national_projects.pending') }}</option>
                <option value="1">{{ trans('national_projects.under_process') }}</option>
                <option value="2">{{ trans('national_projects.complete') }}</option>
            </select>
        </div>
        <div class="activitie_id error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-1">
        <button type="button" id="activities_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('activities_div_{{$number}}',{{$number}},'activities_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>