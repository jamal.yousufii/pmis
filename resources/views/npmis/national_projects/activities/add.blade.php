<div class="m-portlet">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title"><h3 class="m-portlet__head-text">{{ trans('global.add') }}</h3></div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="activityForm" method="post">
    <div class="m-portlet__body">
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('national_projects.activities') }}: <span style="color:red;">*</span></label>
          <div id="div_activitie_id" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="activitie_id" id="activitie_id" onchange="checkForProgress('{{route('getProgress')}}',this.id,'{{ session('national_project_id') }}','div-progress')">
              <option value="">{{ trans('global.select') }}</option>
              @if($activities)
                @foreach($activities as $item)
                  <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="activitie_id error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.activity_desc') }}: <span style="color:red;">*</span></label>
          <textarea class="form-control m-input m-input--air errorDiv required" name="activity_desc[]" id="activity_desc" rows="1"></textarea>
          <div class="activity_desc error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-2">
          <label class="title-custom">{{ trans('national_projects.start_date') }}:</label>
          <input class="form-control m-input datePicker" type="text" name="start_date[]" id="start_date">
        </div>
        <div class="col-lg-2">
          <label class="title-custom">{{ trans('national_projects.end_date') }}:</label>
          <input class="form-control m-input datePicker" type="text" name="end_date[]" id="end_date">
        </div>
        <div class="col-lg-2" id="div-progress">
          <label class="title-custom">{{ trans('national_projects.progress') }}:</label>
          <div class="input-group">
            <input class="form-control errorDiv required" type="number" min="0" max="100" value="0" name="progress[]" id="progress" onkeyup="enforceMinMax(this)">
            <div class="input-group-append">
              <button class="btn btn-secondary btn-sm" type="button"><strong>%</strong></button>
            </div>
          </div>
          <div class="progress error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.status') }}: <span style="color:red;">*</span></label>
          <div id="div_status" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="status[]" id="status">
              <option value="">{{ trans('national_projects.select') }}</option>
              <option value="0">{{ trans('national_projects.pending') }}</option>
              <option value="1">{{ trans('national_projects.under_process') }}</option>
              <option value="2">{{ trans('national_projects.complete') }}</option>
            </select>
          </div>
          <div class="status error-div" style="display:none;"></div>
        </div>
        <!-- <div class="col-lg-1">
          <label class="title-custom">&nbsp;</label><br>
          <button type="button" onclick="add_more('target_div','{{route('moreActivities')}}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm">
            <i class="fa fa-plus" style="font-size:10px;"></i>
          </button>
        </div> -->
      </div>
      <div id="target_div"></div><!-- Display mor activities  -->
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
              <input type="hidden" name="national_project_id" value="{{ session('national_project_id') }}">
              <button type="button" onclick="storeRecord('{{route('activities.store')}}','activityForm','POST','content_div',redirectFunction,false,this.id);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
              <button type="button" onclick="redirectFunction()" class="btn btn-warning">{{ trans('global.cancel') }}</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    @csrf
  </form>
  <!--end::Form-->
</div>
<script type="text/javascript">
  $(".select-2").select2();
  @if (!$lang=="en")
    $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
  @endif
</script>