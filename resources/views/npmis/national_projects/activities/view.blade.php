@extends('master')
@section('head')
  <title>{{ trans('national_projects.projects') }}</title>
  <style>
    .alert.m-alert--icon .m-alert__text {
        padding: 0.8rem 1rem !important;
        padding-top: 0.3rem;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc {
        display: table !important;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-time {
        width: 4.57rem !important;
    }
    .btn.green:not(.btn-outline) {
      color: #FFF;
      background-color: #32c5d2;
      border-color: #32c5d2;
    }
    .btn:not(.btn-sm):not(.btn-lg) {
        line-height: 1.44;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc {
        padding-right: 3rem;
        border-right: 5px solid #36a3f7;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc {
        padding-right: 1rem !important;
    }
  </style>
@endsection
@section('content')
  <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand mb-0" role="tablist">
    @if($tabs)
      @foreach($tabs as $item)
        @if(check_my_section(array(departmentid()),$item->code))
          <li class="nav-item m-tabs__item">
            <a href="{{ route($item->url_route, $id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
              <span style="font-size: 1.4rem;"><span class="{{$item->icon}} px-2"></span><strong>{{ $item->name }}</strong></span>
            </a>
          </li>
        @endif
      @endforeach
    @endif
  </ul>
  <div class="m-portlet m-portlet--full-height dropdown__inner mb-3">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title"><h3 class="m-portlet__head-text">{{ trans('national_projects.activities') }}</h3></div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("npmis_activities","nact_add") and $project->status==0 and $project->projectLocations->count()>0)
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" onclick="addRecord('{{route('activities.create')}}','','GET','content_div')" class="btn btn-info m-btn btn-sm m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="fas fa-plus-circle"></i><span>{{ trans('global.add') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('national_project') }}">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  @alert()
  @endalert
  @include('npmis.national_projects.info')
  <!-- Edit popup: Start -->
  <div id="editPopup" class="modal fade bd-example-modal-full" role="dialog" aria-labelledby="editPopupLable" aria-hidden="true">
      <div class="modal-dialog modal-full">
          <div class="modal-content">
              <div class="modal-header bg-color-dark">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem">{{ trans('global.edit') }}</h3>
                  </div>
                  <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close" id="close"><span aria-hidden="true">&times;</button>
              </div>
              <div class="m-content pt-1" id="modal_content"></div>
          </div>
      </div>
  </div>
  <!-- Edit popup: End -->
  <div class="m-portlet m-portlet--full-height dropdown__inner" id="content_div">
    @if($records->count()>0)
      <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-0">
        <div class="m-portlet__body p-4 pt-1">
          @foreach($records as $item)
            @if($item->nationalActivities->count()>0)
              <div class="accordion row m-1">
                <div class="m-alert m-alert--icon m-alert--outline alert alert-secondary alert-dismissible fade show col-lg-12 m-0" role="alert">
                    <div class="m-alert__text py-2 text-info" style="background: #f59b501a">
                        <div style="border-bottom:1px solid #fff;" class="py-2 row">
                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <i class="far fa-comment-dots mx-1"></i> <label class="title-custom">{{ $item->{'name_'.$lang} }}</label>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <span class="m-badge m-badge--accent float-right rounded-0 px-3 py-2" style="font-size:1.2rem"><strong>{{$item->nationalActivities->sum('progress')}}%</strong></span>
                          </div>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#activtie_div_{{$item->id}}" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                              <span><i class="la la-angle-double-down"></i></span>
                            </a>
                          </div>
                        </div>
                        <div class="code notranslate cssHigh collapse show" id="activtie_div_{{$item->id}}" style="background: #fff">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                              <div class="m-portlet__body">
                                <div class="m-portlet__body p-2">
                                  <table class="table table-striped- table-bordered table-checkable">
                                    <thead>
                                        <tr style="background:#f4f5f8">
                                            <th width="5%">{{ trans('global.number') }}</th>
                                            <th width="30%">{{ trans('national_projects.activity_desc') }}</th>
                                            <th width="10%">{{ trans('national_projects.start_date') }}</th>
                                            <th width="10%">{{ trans('national_projects.end_date') }}</th>
                                            <th width="10%">{{ trans('national_projects.status') }}</th>
                                            <th width="25%">{{ trans('national_projects.progress') }}</th>
                                            <th width="10%" class="text-center">{{ trans('global.action') }}</th>
                                        </tr>
                                    </thead>
                                    <body>
                                      @foreach($item->nationalActivities as $value)
                                        <tr>
                                          <td>{{ $loop->iteration }}</td>
                                          <td>{{ $value->activity_desc }}</td>
                                          <td>{{ dateCheck($value->start_date,$lang) }}</td>
                                          <td>{{ dateCheck($value->end_date,$lang) }}</td>
                                          <td>
                                            @if($value->status=='0')
                                              <span class="m-badge  m-badge--warning m-badge--wide"><strong>{{ trans('national_projects.pending') }}</strong></span>
                                            @elseif($value->status=='1')
                                              <span class="m-badge  m-badge--info m-badge--wide"><strong>{{ trans('national_projects.under_process') }}</strong></span>
                                            @elseif($value->status=='2')
                                              <span class="m-badge m-badge--success m-badge--wide"><strong>{{ trans('national_projects.complete') }}</strong></span>
                                            @endif
                                          </td>
                                          <td>
                                            <div class="d-flex flex-column w-100 ">
                                              <div class="row">
                                                <span class="text-muted font-size-sm col-lg-9 col-md-9 col-sm-9 col-xs-9" style="font-size:1rem;">{{ trans('national_projects.progress') }}</span>
                                                <span class="text-muted font-size-sm col-lg-3 col-md-3 col-sm-3 col-xs-3" style="font-size:1rem;">{{$value->progress}}%</span>
                                              </div>
                                              <div class="progress rounded">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: {{$value->progress}}%;" aria-valuenow="{{$value->progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                                              </div>
                                            </div>
                                          </td>
                                          <td class="text-center">
                                            @if(doIHaveRole("npmis_activities","nact_edit") and $project->status==0)
                                              <button type="button" onclick="setContentToDiv('{{route('activities.edit', encrypt($value->id))}}','GET','modal_content')" data-toggle="modal" data-target="#editPopup" class="btn m-btn m-btn--air btn-xs btn-sm btn-secondary">
                                                <i class="fa fa-edit text-info pt-2" style="font-size:15px;"></i>
                                              </button>
                                            @endif
                                          </td>
                                        </tr>
                                      @endforeach
                                    </body>
                                  </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            @endif
          @endforeach
        </div>
      </div>
    @else
      <div class="m-portlet__body row">
        <div class="m-wizard__form">
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                    <dt>{{ trans('global.not_done') }}</dt>
                </div>
            </div>
        </div>
      </div>
    @endif
  </div>
@endsection
