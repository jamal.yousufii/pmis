<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="editForm" method="post">
  <div class="m-portlet__body">
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-6">
        <label class="title-custom">{{ trans('national_projects.activities') }}: <span style="color:red;">*</span></label>
        <div id="div_activitie_id" class="errorDiv">
          <select class="form-control m-input m-input--air select-2 required" name="activitie_id" id="activitie_id" style="width: 100%" onchange="checkForProgress('{{route('getProgress')}}',this.id,'{{ session('national_project_id') }}','div-progress')">>
            @if($activities)
              @foreach($activities as $item)
                <option value="{{$item->id}}" <?=$item->id==$record->activitie_id ? 'selected' : ''?>>{{ $item->{'name_'.$lang} }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="activitie_id error-div" style="display:none;"></div>
      </div>
      <div class="col-lg-6">
        <label class="title-custom">{{ trans('national_projects.activity_desc') }}: <span style="color:red;">*</span></label>
        <textarea class="form-control m-input m-input--air errorDiv required" name="activity_desc" id="activity_desc" rows="1">{{$record->activity_desc}}</textarea>
        <div class="activity_desc error-div" style="display:none;"></div>
      </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-3">
        <label class="title-custom">{{ trans('national_projects.start_date') }}:</label>
        <input class="form-control m-input datePicker" type="text" name="start_date" id="start_date" value="{{dateCheck($record->start_date,$lang)}}">
      </div>
      <div class="col-lg-3">
        <label class="title-custom">{{ trans('national_projects.end_date') }}:</label>
        <input class="form-control m-input datePicker" type="text" name="end_date" id="end_date" value="{{dateCheck($record->end_date,$lang)}}">
      </div>
      <div class="col-lg-3" id="div-progress">
        <label class="title-custom">{{ trans('national_projects.progress') }}:</label>
        <div class="input-group">
          <input class="form-control errorDiv required" type="number" min="0" max="{{ 100-$progress }}" name="progress" id="progress" value="{{$record->progress}}" onkeyup="enforceMinMax(this)">
          <div class="input-group-append">
            <button class="btn btn-secondary btn-sm" type="button"><strong>%</strong></button>
          </div>
        </div>
        <div class="progress error-div" style="display:none;"></div>
      </div>
      <div class="col-lg-3">
        <label class="title-custom">{{ trans('national_projects.status') }}: <span style="color:red;">*</span></label>
        <div id="div_status" class="errorDiv">
          <select class="form-control m-input m-input--air select-2 required" name="status" id="status" style="width: 100%">
            <option value="0" <?=$item->id=='0' ? 'selected' : ''?>>{{ trans('national_projects.pending') }}</option>
            <option value="1" <?=$item->id=='1' ? 'selected' : ''?>>{{ trans('national_projects.under_process') }}</option>
            <option value="2" <?=$item->id=='2' ? 'selected' : ''?>>{{ trans('national_projects.complete') }}</option>
          </select>
        </div>
        <div class="status error-div" style="display:none;"></div>
      </div>
    </div>
    <div id="target_div"></div><!-- Display mor activities  -->
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions--solid">
            <button type="button" onclick="doEditRecord('{{route('activities.update',$id)}}','editForm','PUT','content_div');" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
            <button type="button" onclick="redirectFunction()" class="btn btn-warning">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @csrf
</form>
<!--end::Form-->
<script type="text/javascript">
  $(".select-2").select2();
  $(document).ready(function()
  {
    @if (!$lang=="en")
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
  })
</script>