@extends('master')
@section('head')
  <title>{{ trans('national_projects.projects') }}</title>
@endsection
@section('content')
  <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($tabs)
      @foreach($tabs as $item)
        @if(check_my_section(array(departmentid()),$item->code))
          <li class="nav-item m-tabs__item">
            <a href="{{ route($item->url_route, $id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
              <span style="font-size: 1.4rem;"><span class="{{$item->icon}} px-2"></span><strong>{{ $item->name }}</strong></span>
            </a>
          </li>
        @endif
      @endforeach
    @endif
  </ul>
  <div class="m-portlet m-portlet--full-height dropdown__inner" id="content_div">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title"><h3 class="m-portlet__head-text">{{ trans('national_projects.general_information') }}</h3></div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRole("npmis_projects","npro_edit") and $record->status==0)
            <li class="m-portlet__nav-item">
                <a href="javascript:void()" class="btn btn-info m-btn btn-sm m-btn--pill m-btn--custom m-btn--icon m-btn--air" onclick="editRecord('{{route('national_projects.edit',$id)}}','','GET','content_div')">
                  <span><i class="far fa-edit"></i><span>{{ trans('global.edit') }}</span></span>
                </a>
            </li>
          @endif
          @if(doIHaveRole("npmis_projects","npro_add") and $record->status==0)
            <li class="m-portlet__nav-item">
                <a class="btn btn-info btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" onclick="setContentToDiv('{{route('national_projects.createLocation')}}','POST','modal_content')" data-toggle="modal" data-target="#locationPopup">
                    <span><i class="fas fa-map-marked-alt"></i><span>{{ trans('national_projects.add_location') }}</span></span>
                </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('national_project') }}">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <!-- Edit popup: Start -->
    <div id="locationPopup" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="locationPopupLable" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">           
                <div class="modal-header bg-color-dark">
                    <div class="m-portlet__head-title">
                      <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem"><i class="fas fa-map-marked-alt px-2"></i><span>{{ trans('national_projects.view_location') }}</span></h3>
                    </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close" id="close"><span aria-hidden="true">&times;</button>
                </div>
                <div class="m-content pt-1" id="modal_content"></div>
            </div>
        </div>
    </div>
    <!-- Edit popup: End -->
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.name') }} :</label><br>
            <span>{{ $record->name }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.category') }} :</label><br>
            <span>{{ $record->category->{'name_'.$lang} }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.type') }} :</label><br>
            <span>{{ $record->project_type->{'name_'.$lang} }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.start_date') }} :</label><br>
            <span>{{ dateCheck($record->start_date,$lang) }}</span>
          </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.end_date') }} :</label><br>
            <span>{{ dateCheck($record->end_date,$lang) }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.donor') }} :</label><br>
            <span>{{ $record->donor }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.contractor') }} :</label><br>
            <span>{{ $record->contractor }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.contract_price') }} :</label><br>
            <span>{{ number_format($record->contract_price) }}</span>
          </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.contract_code') }} :</label><br>
            <span>{{ $record->contract_code }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.responsible_name') }} :</label><br>
            <span>{{ $record->responsible_name }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.responsible_number') }} :</label><br>
            <span>{{ $record->responsible_number }}</span>
          </div>
          <div class="col-lg-3 col-md-3">
            <label class="title-custom">{{ trans('national_projects.responsible_email') }} :</label><br>
            <span>{{ $record->responsible_email }}</span>
          </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-6 col-md-6">
            <label class="title-custom">{{ trans('national_projects.goals') }} :</label><br>
            <span>{!! $record->goal !!}</span>
          </div>
          <div class="col-lg-6 col-md-6">
            <label class="title-custom">{{ trans('national_projects.description') }} :</label><br>
            <span>{!! $record->description !!}</span>
          </div>
        </div>
        <div class="m-portlet code notranslate cssHigh collapse show" id="collapseDiv">
          <!-- Display Project locations -->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('national_projects.view_location') }}</h3>
              </div>
            </div>
          </div>
          <div class="m-portlet__body py-2">
            <table class="table table-striped- table-bordered table-hover table-checkable">
              <thead>
                  <tr style="background:#f4f5f8">
                      <th width="10%">{{ trans('global.number') }}</th>
                      <th width="15%">{{ trans('plans.province') }}</th>
                      <th width="25%">{{ trans('plans.district') }}</th>
                      <th width="25%">{{ trans('plans.village') }}</th>
                      <th width="10%">{{ trans('global.action') }}</th>
                  </tr>
              </thead>
              <tbody>
                @if($locations)
                  @foreach($locations as $item)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $item->province->{'name_'.$lang} }}</td>
                      <td>{{ $item->district->{'name_'.$lang} }}</td>
                      <td>{{ $item->village->{'name_'.$lang} }}</td>
                      <td>
                        @if(doIHaveRole("npmis_projects","npro_edit") and $record->status==0)
                          <button type="button" onclick="setContentToDiv('{{route('national_projects.editLocation', encrypt($item->id))}}','GET','modal_content')" data-toggle="modal" data-target="#locationPopup" class="btn m-btn m-btn--air btn-xs btn-sm btn-secondary">
                            <i class="fa fa-edit text-info pt-2" style="font-size:15px;"></i>
                          </button>
                        @endif
                      </td>
                    </tr>
                  @endforeach 
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection