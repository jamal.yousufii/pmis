<div class="m-portlet">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title"><h3 class="m-portlet__head-text">{{ trans('global.add') }}</h3></div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="projectForm" method="post">
    <div class="m-portlet__body">
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.name') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input errorDiv" type="text" name="name" id="name">
          <div class="name error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('plans.proj_category') }}: <span style="color:red;">*</span></label>
          <div id="div_category_id" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="category_id" id="category_id" onchange="bringDataById('{{route('getProjectType')}}',this.value,'POST','project_type_id','');">
              <option value="">{{ trans('global.select') }}</option>
              @if($categories)
                @foreach($categories as $cat)
                  <option value="{{$cat->id}}">{{ $cat->{'name_'.$lang} }}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="category_id error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3" id="ptype_div">
          <label class="title-custom">{{ trans('plans.project_type') }}: <span style="color:red;">*</span></label>
          <div id="div_project_type_id" class="errorDiv">
            <select class="form-control m-input m-input--air select-2 required" name="project_type_id" id="project_type_id">
              <option value="">{{ trans('global.select') }}</option>
            </select>
          </div>
          <div class="project_type_id error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.start_date') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input datePicker errorDiv" type="text" name="start_date" id="start_date">
          <div class="start_date error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.end_date') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input datePicker errorDiv" type="text" name="end_date" id="end_date">
          <div class="end_date error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.donor') }}:</label>
          <input class="form-control m-input errorDiv" type="text" name="donor" id="donor">
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.contractor') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input errorDiv" type="text" name="contractor" id="contractor">
          <div class="contractor error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.contract_price') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input errorDiv" type="number" min="0" name="contract_price" id="contract_price">
          <div class="contract_price error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.contract_code') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input errorDiv" type="text" name="contract_code" id="contract_code">
          <div class="contract_code error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.responsible_name') }}:</label>
          <input class="form-control m-input" type="text" name="responsible_name" id="responsible_name">
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.responsible_number') }}:</label>
          <input class="form-control m-input" type="number" min="0" name="responsible_number" id="responsible_number">
        </div>
        <div class="col-lg-3">
          <label class="title-custom">{{ trans('national_projects.responsible_email') }}:</label>
          <input class="form-control m-input" type="text" name="responsible_email" id="responsible_email">
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('national_projects.goals') }}:</label>
          <textarea class="form-control tinymce m-input m-input--air" name="goals" rows="3"></textarea>
          <span class="m-form__help">{{ trans('national_projects.goals_note') }}</span>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('national_projects.description') }}:</label>
          <textarea class="form-control tinymce m-input m-input--air" name="description" rows="3"></textarea>
          <span class="m-form__help">{{ trans('national_projects.desc_note') }}</span>
        </div>
      </div>
      <div id="targetDiv"></div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
              <button type="button" onclick="storeRecord('{{route('national_projects.store')}}','projectForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
              <button type="button" onclick="redirectFunction()" class="btn btn-warning">{{ trans('global.cancel') }}</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    @csrf
  </form>
  <!--end::Form-->
</div>
<script type="text/javascript">
  $(".select-2").select2();
  tinymce.init({
      selector:'textarea.tinymce',
  });
</script>