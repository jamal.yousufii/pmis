<option value="">{{ trans('global.select') }}</option>
@foreach($types as $type)
  <option value="{{ $type->id }}">{{ $type->{'name_'.$lang} }}</option>
@endforeach