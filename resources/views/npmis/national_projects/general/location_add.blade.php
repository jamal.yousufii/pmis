<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="locationForm" method="post">
  <div class="m-portlet__body">
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-4 col-md-4">
        <div id="province" class="errorDiv">
          <label class="title-custom">{{ trans('plans.province') }}: <span style="color:red;">*</span></label>
          <select class="form-control m-input input-lg m-input--air select-2" name="province" id="province_id_1" style="width:100%;" onchange="bringDistricts('province_id_1','district_div_1',1,'village_id_1');">
            <option value="">{{ trans('global.select') }}</option>
            @if($provinces)
              @foreach($provinces as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="province error-div" style="display:none;"></div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div id="district_div_1">
            <label class="title-custom">{{ trans('plans.district') }}: <span style="color:red;">*</span></label>
            <div id="district" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="district" id="district_id_1" style="width:100%;" onchange="bringVillages('district_id_1','village_div_1',1)">
                    <option value="">{{ trans('global.select') }}</option>
                </select>
            </div>
            <div class="district error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div id="village_div_1">
          <label class="title-custom">{{ trans('plans.village') }}: <span style="color:red;">*</span></label>
            <div id="village" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="village" id="village_id_1" style="width:100%;">
                    <option value="">{{ trans('global.select') }}</option>
                </select>
            </div>
            <div class="village error-div" style="display:none;"></div>
        </div>
      </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <input type="hidden" name="id" value="{{ session('national_project_id') }}">
        <button type="button" onclick="storeRecord('{{route('national_projects.storeLocation')}}','locationForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">{{ trans('global.cancel') }}</button>
      </div>
    </div>
  </div>
</form>
<!--end::Form-->
<script type="text/javascript">
  $(".select-2").select2();
</script>
