<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="locationEditForm" method="post">
  <div class="m-portlet__body">
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-4 col-md-4">
        <div id="province" class="errorDiv">
          <label class="title-custom">{{ trans('plans.province') }}: <span style="color:red;">*</span></label>
          <select class="form-control m-input input-lg m-input--air select-2" name="province" id="province_id" style="width:100%;" onchange="bringDistricts('province_id','district_div_0',0);">
            <option value="">{{ trans('global.select') }}</option>
            @if($provinces)
              @foreach($provinces as $item)
                <option value="{{$item->id}}" <?=$item->id==$record->province_id ? 'selected' : ''?>>{{$item->name}}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="province error-div" style="display:none;"></div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div id="district_div_0">
            <label class="title-custom">{{ trans('plans.district') }}: <span style="color:red;">*</span></label>
            <div id="district" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="district" id="district_id" style="width:100%;" onchange="bringVillages('district_id','village_div_0',0)">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($districts)
                    @foreach($districts as $item)
                      <option value="{{$item->id}}" <?=$item->id==$record->district_id ? 'selected' : ''?>>{{$item->name}}</option>
                    @endforeach
                  @endif
                </select>
            </div>
            <div class="district error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div id="village_div_0">
            <label class="title-custom">{{ trans('plans.village') }}: <span style="color:red;">*</span></label>
            <div id="village" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="village" id="village_id" style="width:100%;">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($villages)
                      @foreach($villages as $item)
                        <option value="{{$item->id}}" <?=$item->id==$record->village_id ? 'selected' : ''?>>{{$item->name}}</option>
                      @endforeach
                    @endif
                </select>
            </div>
            <div class="village error-div" style="display:none;"></div>
        </div>
      </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <button type="button" onclick="doEditRecord('{{route('national_projects.updateLocation',$id)}}','locationEditForm','GET','content_div');" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">{{ trans('global.cancel') }}</button>
      </div>
    </div>
  </div>
</form>
<!--end::Form-->
<script type="text/javascript">
  $(".select-2").select2();
</script>
