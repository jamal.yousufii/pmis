@extends('master')
@section('head')
  <title>{{ trans('global.access_denied') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div align="center" class="pt-4 pb-5">
      <table width="100%" class="mt-3" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td align="center"><img src="{!!asset('public/img/restricted.jpg')!!}" class="unauthorized m--img-rounded m--marginless m--img-centered" alt="" /></td>
        </tr>
        <tr class="pt-3">
            <td align="center"><span class="text-danger title-custom">{{ trans('global.unauthorized') }}</span></td>
        </tr>
        <tr class="pt-5">
          <td align="center"><span class="title-custom">{{ trans('global.unauth_note') }}</span></td>
        </tr>
      </table>
    </div>
  </div>
@endsection
@section('footer')

@endsection
