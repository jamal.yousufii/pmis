<div class="m-subheader pt-0 pb-1">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ route('home') }}" class="m-nav__link">
                        <span class="m-nav__link-text"><h3 class="breadcrumb_nav__head-text"><i class="fas fa-home"></i></h3></span>
                    </a>
                </li>
                @if(!empty($segments))
                    @if($segments[0]=='bringSections' or $segments[0]=='bringContractors')
                        @php
                            $link  = '';
                            $title = '';
                            $link  = $segments[0];
                            $title = getDepartmentNameByID(decrypt($segments[1]),get_language());
                        @endphp
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route($link,[ 'id' => $segments[1] ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text"><h3 class="breadcrumb_nav__head-text">{{ trans($title) }}</h3></span>
                            </a>
                        </li>
                        @elseif($segments[0]=='bringContractorSections')
                        @php
                            $link  = '';
                            $title = '';
                            $link  = $segments[0];
                            $title = getDepartmentNameByID(decrypt($segments[1]),get_language());
                            $contractor_title = getContractorNameById(decrypt($segments[2]));
                        @endphp
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route('bringContractors',[ 'id' => $segments[1] ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ trans($title) }}</h3></span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route($link,[ 'id' => $segments[1],'con_id' => $segments[2] ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ $contractor_title['company_name'] }}</h3></span>
                            </a>
                        </li>
                    @elseif($segments[0]=='bringContractorSections')
                        @php
                            $link  = '';
                            $title = '';
                            $link  = $segments[0];
                            $title = getDepartmentNameByID(decrypt($segments[1]),get_language());
                            $contractor_title = getContractorNameById(decrypt($segments[2]));
                        @endphp
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route('bringContractors',[ 'id' => $segments[1] ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ trans($title) }}</h3></span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route($link,[ 'id' => $segments[1],'con_id' => $segments[2] ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ $contractor_title['company_name'] }}</h3></span>
                            </a>
                        </li>
                    @elseif($segments[0]=='calendarTasks' OR $segments[0]=='assignedTasks')
                        @php
                            $link  = '';
                            $title = '';
                            $link  = $segments[0];
                            $title = 'tasks.my_tasks';
                        @endphp
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route('calendarTasks') }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ trans($title) }}</h3></span>
                            </a>
                        </li>
                    @else
                        @php
                            $section  = getSectionByRoute($segments[0],get_language());
                            $dep_id   = decrypt(session('current_department'));
                            $dep_title= getDepartmentNameByID($dep_id,get_language());
                            $section_title= $section['name'];
                            $link  = $segments[0];
                        @endphp
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route('bringSections',[ 'id' => session('current_department') ]) }}" class="m-nav__link">
                                <span class="m-nav__link-text"><h3 class="breadcrumb_nav__head-text">{{ trans($dep_title) }}</h3></span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ route($link,[ 'id' => session('current_department')]) }}" class="m-nav__link">
                                <span class="m-nav__link-text" style="font-size:18px;"><h3 class="breadcrumb_nav__head-text">{{ trans($section_title) }}</h3></span>
                            </a>
                        </li>
                    @endif
                @endif
            </ul>
        </div>
    </div>
</div>