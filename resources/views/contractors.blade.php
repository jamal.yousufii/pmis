@extends('master')
@section('head')
  <title>{{ trans('login.pmis') }}</title>
  <style>
    .m-portlet {
        margin-bottom: 1.8rem !important;
    }
    .m-portlet-custom{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19) !important;
        background: #ffffff !important;
    }
    .btn-accent {
        background-color: #FFFFFF !important;
    }
    .bg-accent{
        background: #36a3f799 !important;
        opacity: 0.7;
    }
    .btn-title{
        font-size: 1.1rem !important;
        font-weight: 600;
        color: #000;
        opacity: 1;
    }
    .clearfix:before {
        content: " ";
        display: table;
    }
    .m-portlet-custom-hover:hover {
        box-shadow: 0px 2px 10px #4474B7 !important;
    }
    .icon-color{
        color: #36a3f7;
    }
  </style>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text">{{ $department_name }}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-portlet__body  m-portlet__body--no-padding" style="background: #f2f3f8">
      <div class="row mt-4" style="background: #f2f3f8">
        @if($contractors)
          @foreach($contractors as $item)
            @if(check_my_contractor($item->id))
              <div class="col-lg-4">
                <a class="main-link" href="{{ route('bringContractorSections',['dep_id'=>$enc_depId,'con_id'=>encrypt($item->id)]) }}">
                  <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 m-portlet-custom-hover rounded" style="height:70px;">
                      <div class="m-widget21 info">
                          <div class="row">
                              <div class="col">
                                  <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                      <span class="m-widget21__icon px-3">
                                          <i class="icon-bag2 icon-color font-large-2 float-xs-right"><i class="fas fa-school" style="font-size:2.3rem;"></i></i>
                                      </span>
                                      <div class="m-widget21__info">
                                          <span class="m-widget21__title">{{ $item->company_name }}</span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                </a>
              </div>
            @endif
          @endforeach
        @endif
      </div>
    </div>
  </div>
@endsection
