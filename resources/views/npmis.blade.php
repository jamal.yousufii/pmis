
<!-- Dashboard => Project Info Summary: Start -->
<div class="m-portlet m-portlet--mobile m-portlet-custom col-lg-12 mt-3">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-xl-4">
                <!--begin:: Widgets/Support Stats-->
                <div class="m-widget1">
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-accent" style="font-size: 1.5rem"> {{ trans('national_projects.general_information') }} </h3>
                                <span class="m-widget1__desc m--font-accent"> {{ trans('national_projects.general_information_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-accent">{{ $under_tech_doc }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-primary" style="font-size: 1.5rem"> {{ trans('national_projects.tech_documents') }} </h3>
                                <span class="m-widget1__desc m--font-primary"> {{ trans('national_projects.tech_documents_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-primary">{{ $total_procurements }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-info" style="font-size: 1.5rem">{{ trans('national_projects.under_implementaion') }} </h3>
                                <span class="m-widget1__desc m--font-info"> {{ trans('national_projects.under_implementaion_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-info">{{ $construction_projects }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Support Stats-->
            </div>
            <div class="col-xl-4">
                <!--begin:: Widgets/Support Stats-->
                <div class="m-widget1">
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-success" style="font-size: 1.5rem"> {{ trans('global.completed') }} </h3>
                                <span class="m-widget1__desc m--font-success"> {{ trans('home.completed_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-success text-success">{{ $completed_projects }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-danger" style="font-size: 1.5rem">{{ trans('global.stoppedـpro') }} </h3>
                                <span class="m-widget1__desc m--font-danger"> {{ trans('home.stopped_pro_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-danger">{{ $stopped_projects }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h3 class="m-widget1__title m--font-dark" style="font-size: 1.5rem">{{ trans('global.total_projects') }} </h3>
                                <span class="m-widget1__desc m--font-dark"> {{ trans('home.total_projects_note') }} </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 m--align-right">
                                <span class="m-widget1__number m--font-dark">{{ $total_projects }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Support Stats-->
            </div>
            <div class="col-xl-4">
                <!--begin:: Widgets/Profit Share-->
                <div class="m-widget14">
                    <div class="row  align-items-center">
                        <div class="col">
                            <div id="projectInEachSections" class="m-widget14__chart" style="height: 310px; margin-top: -20px; padding-left:50px; padding-right:50px;">
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Profit Share-->
            </div>
        </div>
    </div>
</div>
<!-- Dashboard => Project Info Summary: End -->

<!-- Dashboard => Project By Zone: Start -->
<div class="m-portlet m-portlet--mobile m-portlet-custom col-lg-12">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <!--begin:: Widgets/Support Stats-->
                <div class="m-widget1">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 text-title-custom p2-3 breadcrumb_nav__head-text">
                            <span class="fa flaticon-layers mx-0"></span><span class="mx-1"> {{ trans('home.project_by_zone') }} </span>
                        </div>
                        <form class="m-form m-form--fit m-form--label-align-right" id="zoneForm" method="post" enctype="multipart/form-data"  style="width: 100%">
                            <div class="form-group m-form__group row">    
                                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                                    <div id="div_zone" class="errorDiv inline"> <!-- Error Dive start for select box  -->  
                                        <select class="form-control m-input m-input--air select-2 required" name="zone[]" id="zone" multiple>
                                            <option value="-" selected>{{ trans('global.all') }}</option>
                                            @if($zone)
                                                @foreach($zone as $item)
                                                    <option value="{{ $item->zcode }}">{{ $item->zname }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="zone error-div" style="display:none;"></div>
                                </div>
                                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                    <input type="hidden" name="condition" id="condition" value="zone">
                                    <a class="btn btn-icon btn-sm btn-info btn-circle" href="javascript:void(0);" onclick="storeData('{{route('dashboardFilter')}}','zoneForm','POST','zone_content',put_content,true);">
                                        <span class="fa flaticon-refresh"></span>
                                    </a>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="row mt-2" id="zone_content">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="m-widget14 p-0">
                                <div class="clearfix">
                                    @if($provinces)
                                        @foreach($provinces as $item)
                                            <a class="btn bg-accent m-btn--custom m-btn--icon btn-sm mt-1" href="javascript:void()" onclick="viewRecord('{{route('dashboardFilter')}}','condition=province&province={{$item->id}}','POST','zone_content')">
                                                <span class="btn-title">{{ $item->{'name_'.$lang} }}</span>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mt-5">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="bg-dark text-white">
                                        <th class="p-1 text-center">{{ trans('home.zone') }}</th>
                                        <th class="p-1 text-center">{{ trans('home.total_projects') }}</th>
                                        <th class="p-1 text-center">{{ trans('home.total_money') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($projectByZone)
                                        @php
                                        $all_projects = 0;
                                        $all_price    = 0;
                                        @endphp
                                        @foreach($projectByZone as $item)
                                            <tr>
                                                <td class="p-1 text-center">{{ $item->zone }}</td>
                                                <td class="p-1 text-center">{{ number_format($item->total_projects) }}</td>
                                                <td class="p-1 text-center">
                                                    @if(isset($project_budget[$item->zcode]))
                                                        {{ number_format($project_budget[$item->zcode]) }}
                                                    @else
                                                        0 
                                                    @endif
                                                </td>
                                            </tr> 
                                            @php
                                            $all_projects = $all_projects+$item->total_projects;
                                                if(isset($project_budget[$item->zcode])){
                                                    $all_price = $all_price+$project_budget[$item->zcode];
                                                }
                                            @endphp   
                                        @endforeach
                                        <tr>
                                            <td class="p-1 text-center text-title-custom">{{ trans('home.total') }}</td>
                                            <td class="p-1 text-center text-title-custom">{{$all_projects}}</td>
                                            <td class="p-1 text-center text-title-custom">{{number_format($all_price)}}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                            <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
                                <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.project_by_zone') }} </span>
                            </div>
                            <div id="projectByZone" style="height:370px;"></div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                            <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
                                <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.budget_by_zone') }} </span>
                            </div>
                            <div id="moneyByZone" style="height:370px;"></div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                            <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
                                <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.project_by_section') }} </span>
                            </div>
                            <div id="projectBySection" style="height:387px;"></div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Support Stats-->
            </div>
        </div>
    </div>
</div>
<!-- Dashboard => Project By Zone: End -->