@php
$sections = getSections(session('current_mod'),0,0);
$departments = UserDepartments();
@endphp
@if($sections)
    @foreach($sections as $item)
        @if(check_my_section($departments,$item->code))
            <div class="col-lg-4">
                <a class="main-link-div" href="{{ route($item->url_route) }}">
                    <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 rounded" style="height:70px;">
                        <div class="m-widget21">
                            <div class="row">
                                <div class="col">
                                    <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                        <span class="m-widget21__icon px-3">
                                            <i class="icon-bag2 icon-color font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                        </span>
                                        <div class="m-widget21__info">
                                            <span class="m-widget21__title">{{ $item->{'name_'.$lang} }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @else
            <div class="col-lg-4 disabled ain-link-div-disabled">
                <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2 rounded" style="height:70px;">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg m-widget21__item__custom">
                                    <span class="m-widget21__icon px-3">
                                        <i class="icon-bag2 icon-color font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2.3rem;"></i></i>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">{{ $item->{'name_'.$lang} }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif