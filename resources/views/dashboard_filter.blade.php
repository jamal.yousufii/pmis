<div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 pt-3">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2 px-1">
        <table class="table table-bordered">
            <thead>
                <tr class="bg-dark text-white">
                    <th class="p-1">{{ trans('home.zone') }}</th>
                    <th class="p-1">{{ trans('home.total_projects') }}</th>
                    <th class="p-1">{{ trans('home.total_money') }}</th>
                </tr>
            </thead>
            <tbody>
                @if($projectByZone)
                    @php
                    $all_projects = 0;
                    $all_price    = 0;
                    @endphp
                    @foreach($projectByZone as $item)
                        <tr>
                            <td class="p-1">{{ $item->zone }}</td>
                            <td class="p-1">{{ number_format($item->total_projects) }}</td>
                            <td class="p-1">{{ number_format($item->total_price) }}</td>
                        </tr> 
                        @php
                        $all_projects = $all_projects+$item->total_projects;
                        $all_price    = $all_price+$item->total_price;;
                        @endphp   
                    @endforeach
                    <tr>
                        <td class="p-1 text-title-custom">{{ trans('home.total') }}</td>
                        <td class="p-1 text-title-custom">{{$all_projects}}</td>
                        <td class="p-1 text-title-custom">{{number_format($all_price)}}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="col-xl-5 col-lg-5 col-md-8 col-sm-12">
    <div id="projectByZoneFilter" style="height:400px;"></div>
</div>
<div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 pt-3">
    <table class="table table-bordered">
        <thead>
            <tr class="bg-dark text-white">
                <th class="p-1" width="50%">{{ trans('global.sections') }}</th>
                <th class="p-1" width="50%">{{ trans('global.total_projects') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($project_status)
                @foreach($project_status as $key=>$val)
                    @php
                        $total = 0;
                        if(isset($val[2])) { $total = $total+$val[2]; } 
                        if(isset($val[0])) { $total = $total+$val[0]; } 
                    @endphp
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $total }}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <td>{{ trans('progress.progress') }}</td>
                <td>{{ $progress }}</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-xl-3 col-lg-3 col-md-8 col-sm-12">
    <div id="projectBySectionsFilter" style="height:400px;"></div>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        var projectByZoneFilter = echarts.init(document.getElementById('projectByZoneFilter'));
        projectByZoneFilterOption = {
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            series: [
                {
                    name: '{{ trans("home.total_money") }}',
                    type: 'pie',
                    selectedMode: 'multiple',
                    radius: [0, '30%'],
                    label: {
                        position: 'inner'
                    },
                    labelLine: {
                        show: true
                    },
                    data: [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                @php
                                $color= Config::get('static.zone.'.$item->zcode.'.color');
                                @endphp
                                {
                                    itemStyle: {color:'{{$color}}', opacity: 0.8},
                                    name: '{{ $item->zone }}',
                                    value: {{ $item->total_price }},
                                },
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("home.pro_by_zone") }}',
                    type: 'pie',
                    radius: ['40%', '55%'],
                    label: {
                        <?php
                        if($lang!="en"){
                        ?>
                            formatter: '{a|{a}}{abg|}\n{hr|}\n{per|{d}%}  {c|{c}}{b|{b}：} ',
                        <?php
                        }else{
                        ?>
                            formatter: '{a|{a}}{abg|}\n{hr|}\n {b|{b}：}{c|{c}}  {per|{d}%}',
                        <?php
                        }
                        ?>
                        backgroundColor: '#f4f5f8',
                        borderColor: '#84c9ff',
                        borderWidth: 1,
                        borderRadius: 4,
                        rich: 
                        {
                            a: {
                                color: '#42434b',
                                lineHeight: 22,
                                align: 'center'
                            },
                            hr: {
                                borderColor: '#aaa',
                                width: '100%',
                                borderWidth: 0.5,
                                height: 0,
                            },
                            b: {
                                fontSize: 13,
                                lineHeight: 33,
                                align: 'center',
                                color: '#575962',
                            },
                            c: {
                                fontSize: 13,
                                lineHeight: 33,
                                align: 'center',
                                color: '#575962',
                            },
                            per: {
                                color: '#fff',
                                backgroundColor: '#334455',
                                padding: [2, 4],
                                borderRadius: 2,
                                align: 'center'
                            }
                        }
                    },
                    data: [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                @php
                                $color= Config::get('static.zone.'.$item->zcode.'.color');
                                @endphp
                                {
                                    value: '{{ $item->total_projects }}', 
                                    itemStyle: {color:'{{$color}}', opacity: 0.9},
                                    name: '{{ $item->zone }}'
                                },
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        projectByZoneFilter.setOption(projectByZoneFilterOption);

        var projectBySectionsFilter = echarts.init(document.getElementById('projectBySectionsFilter'));
        var projectBySectionsFilterOption = {
            tooltip : {
                show: true
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : {
                type : 'category',
                data : [
                    '{{ trans('plans.plan') }}',
                    '{{ trans('surveys.surveys') }}',  
                    '{{ trans('designs.designs') }}',
                    '{{ trans('estimation.estimation') }}',
                    '{{ trans('implements.implements') }}',
                    '{{ trans('procurement.procurement') }}',
                    '{{ trans('progress.progress') }}',
                ],
                axisTick : {
                    show : true,
                    alignWithLabel : true,
                    autoSkip : false
                },
                axisLabel : {
                    rotate : 45,
                }
            },
            yAxis : {
                type:'value',
            },
            series : [
                {
                    data : [
                            @if($project_status)
                                @foreach($project_status as $key=>$val)
                                    {
                                        @php
                                            $total = 0;
                                            if(isset($val[2])) { $total = $total+$val[2]; } 
                                            if(isset($val[0])) { $total = $total+$val[0]; } 
                                        @endphp
                                        value: "{{ $total }}",
                                        itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                                    },
                                @endforeach
                            @endif
                            {
                                value: "{{ $progress }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                        ],
                    type : 'bar',
                }
            ]
        };
        projectBySectionsFilter.setOption(projectBySectionsFilterOption);
    });        
</script>
