@extends('master')
@section('head')
  <title>{{ trans('login.pmis') }}</title>
  <link rel="stylesheet" href="{!!asset('public/assets/slider/flexslider.css')!!}">
  <link rel="stylesheet" href="{!!asset('public/assets/slider/flexslider-rtl.css')!!}">
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text">{{ $contractor->company_name }}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringContractors',$enc_depId) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row"> 
    @if($sections)
      @foreach($sections->sections as $item)
        <div class="col-xl-3 col-lg-3 col-xs-3 col-md-6 main">
          @if(check_my_section(array(decrypt($enc_depId)),$item->code))
            <a class="main-link" href="{{ route($item->url_route,[$enc_conId,$enc_depId]) }}">
              <div class="card col-xl-10 col-lg-10 col-xs-10 col-md-12 offset-xl-1 offset-lg-1 offset-xs-1 pt-2">
                <div class="card-body d-card-body">
                  <div class="card-block">
                    <div class="media">
                      <div class="media-right media-middle">
                        <div class="d-icon pt-2">
                          <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2rem;"></i></i>
                        </div>
                        <div class="text-xs-center text-xs-left">
                          <h3 class="pink lead pt-3">
                            <span class="dashboard_title_entity">{{ $item->{'name_'.$lang} }}</span>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          @else
            <div class="card-disabled col-xl-3 col-lg-3 col-xs-3 col-md-6 main">
              <div class="card-body d-card-body">
                <div class="card-block">
                  <div class="media">
                    <div class="media-right media-middle">
                      <div class="d-icon pt-2">
                        <i class="icon-bag2 pink font-large-2 float-xs-right"><i class="{{$item->icon}}" style="font-size:2rem;"></i></i>
                      </div>
                      <div class="text-xs-center">
                        <h3 class="pink lead pt-3">
                          <span class="dashboard_title_entity">{{$item->name_dr}}</span>
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        </div>
      @endforeach
    @endif
  </div>
@endsection

