<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
    <div class="m-widget14 p-0">
        <div class="clearfix">
            @if($locations)
                @foreach($locations as $item)
                    <a class="btn bg-accent m-btn--custom m-btn--icon btn-sm mt-1" href="javascript:void()" onclick="viewRecord('{{route('dashboardFilter')}}','condition=province&province={{$item->id}}','POST','zone_content')">
                        <span class="btn-title">{{ $item->{'name_'.$lang} }}</span>
                    </a>
                @endforeach
            @endif
        </div>
    </div>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mt-5">
    <table class="table table-bordered">
        <thead>
            <tr class="bg-dark text-white">
                <th class="p-1 text-center">{{ trans('global.province') }}</th>
                <th class="p-1 text-center">{{ trans('home.total_projects') }}</th>
                <th class="p-1 text-center">{{ trans('home.total_money') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($projectByZone)
                @php
                $all_projects = 0;
                $all_price    = 0;
                @endphp
                @foreach($projectByZone as $item)
                    <tr>
                        <td class="p-1 text-center">{{ $item->province_name }}</td>
                        <td class="p-1 text-center">{{ number_format($item->total_projects) }}</td>
                        <td class="p-1 text-center">
                            @if(isset($project_budget[$item->pro_id]))
                                {{ number_format($project_budget[$item->pro_id]) }}
                            @else
                                0
                            @endif
                        </td>
                    </tr> 
                    @php
                    $all_projects = $all_projects+$item->total_projects;
                        if(isset($project_budget[$item->pro_id])){
                            $all_price = $all_price+$project_budget[$item->pro_id];
                        }
                    @endphp   
                @endforeach
                <tr>
                    <td class="p-1 text-center text-title-custom">{{ trans('home.total') }}</td>
                    <td class="p-1 text-center text-title-custom">{{$all_projects}}</td>
                    <td class="p-1 text-center text-title-custom">{{number_format($all_price)}}</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
    <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
        <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.project_by_zone') }} </span>
    </div>
    <div id="projectByZone" style="height:370px;"></div>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
    <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
        <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.budget_by_zone') }} </span>
    </div>
    <div id="moneyByZone" style="height:370px;"></div>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
    <div class="text-title-custom p2-3 breadcrumb_nav__head-text pt-4">
        <span class="la la-info-circle mx-0"></span><span class="mx-1"> {{ trans('home.project_by_section') }} </span>
    </div>
    <div id="projectBySection" style="height:387px;"></div>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        var projectByZone = echarts.init(document.getElementById('projectByZone'));
        var projectByZoneOption = {
            tooltip : {
                show: true
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : {
                type : 'category',
                data: [
                    @if($projectByZone)
                        @foreach($projectByZone as $item)
                            '{{ $item->province_name }}',
                        @endforeach
                    @endif
                ],
                axisTick : {
                    show : true,
                    alignWithLabel : true,
                    autoSkip : false
                },
                axisLabel : {
                    rotate : 35,
                }
            },
            yAxis : {
                type:'value',
            },
            series : [
                {
                    data : [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                @php
                                $color= Config::get('static.zone.'.$item->zcode.'.color');
                                @endphp
                                {
                                    value: '{{ $item->total_projects }}', 
                                    itemStyle: {color:'{{$color}}', opacity: 0.7},
                                },
                            @endforeach
                        @endif
                    ],
                    type : 'bar',
                }
            ]
        };
        projectByZone.setOption(projectByZoneOption);

        var moneyByZone = echarts.init(document.getElementById('moneyByZone'));
        var moneyByZoneOption = {
            tooltip : {
                show: true
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : {
                type : 'category',
                data: [
                    @if($projectByZone)
                        @foreach($projectByZone as $item)
                            '{{ $item->province_name }}',
                        @endforeach
                    @endif
                ],
                axisTick : {
                    show : true,
                    alignWithLabel : true,
                    autoSkip : false
                },
                axisLabel : {
                    rotate : 35,
                }
            },
            yAxis : {
                type:'value',
            },
            series : [
                {
                    data : [
                        @if($projectByZone)
                            @foreach($projectByZone as $item)
                                @php
                                $color= Config::get('static.zone.'.$item->zcode.'.color');
                                @endphp
                                {   
                                    @if(isset($project_budget[$item->pro_id]))
                                        value: '{{ $project_budget[$item->pro_id] }}', 
                                    @else
                                        value: '0', 
                                    @endif
                                    itemStyle: {color:'{{$color}}', opacity: 0.7},
                                },
                            @endforeach
                        @endif
                    ],
                    type : 'bar',
                }
            ]
        };
        moneyByZone.setOption(moneyByZoneOption);

        var projectBySection = echarts.init(document.getElementById('projectBySection'));
        var projectBySectionOption = {
            tooltip : {
                show: true
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : {
                type : 'category',
                data : [
                    '{{ trans('plans.plan') }}',
                    '{{ trans('surveys.surveys') }}',  
                    '{{ trans('designs.designs') }}',
                    '{{ trans('estimation.estimation') }}',
                    '{{ trans('implements.implements') }}',
                    '{{ trans('procurement.procurement') }}',
                    '{{ trans('global.progress') }}',
                    '{{ trans('global.completed') }}',
                    '{{ trans('global.stoppedـpro') }}',
                ],
                axisTick : {
                    show : true,
                    alignWithLabel : true,
                    autoSkip : false
                },
                axisLabel : {
                    rotate : 35,
                }
            },
            yAxis : {
                type:'value',
            },
            series : [
                {
                    data : [
                            {
                                        
                                value: "{{ $plan_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                value: "{{ $survey_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                value: "{{ $design_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                value: "{{ $estimation_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                value: "{{ $checklist_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                        
                                value: "{{ $total_procurements }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                        
                                value: "{{ $construction_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                        
                                value: "{{ $completed_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                            {
                                value: "{{ $stopped_projects }}",
                                itemStyle: {color:'#36a3f7', opacity: 0.8, barBorderRadius: [2, 2, 0, 0]},
                            },
                        ],
                    type : 'bar',
                }
            ]
        };
        projectBySection.setOption(projectBySectionOption);
    });        
</script>
