@extends('master')
@section('head')
    <title>{{ trans('stopped.stopped') }}</title>
    <style>
    .m-tabs-line.m-tabs-line--brand.nav.nav-tabs .nav-link:hover, .m-tabs-line.m-tabs-line--brand.nav.nav-tabs .nav-link.active, .m-tabs-line.m-tabs-line--brand a.m-tabs__link:hover, .m-tabs-line.m-tabs-line--brand a.m-tabs__link.active {
        color: #716aca;
        border-bottom: 3px solid #716aca !important;
    }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">{{$project->name}}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('stopped',session('current_department')) }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 pb-1">
            <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('plans.project_name' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{$project->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-signs m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('plans.code' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $project->code }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fl flaticon-list-2 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('plans.category' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $project->category->{'name_'.$lang} }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fl flaticon-clipboard m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('plans.project_type' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $project->project_type->{'name_'.$lang} }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="m-portlet m-portlet--mobile m-0" style="min-height:490px">
                <div class="m-portlet__body p-1" id="content">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
                        @if($sections)
                            @foreach($sections as $item)
                                @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                                    <li class="nav-item m-tabs__item">
                                        <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('stopped.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                                            <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                                        </a>
                                    </li>
                                    @if($item->code==$project_status->section)
                                        @break
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel">
                            <div class="m-portlet--primary m-portlet--head-sm">
                                <div class="m-portlet__head">
                                    <!-- Include Attachments Modal -->
                                    @php 
                                        $attachments = getAttachments('requests_attachments',0,$record->id,'request');  
                                        $attachment_table = 'requests_attachments';
                                    @endphp
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">{{ trans('requests.view') }}</h3>
                                        </div>
                                    </div>                                   
                                    <!-- Include Attachments Modal -->
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            <li class="m-portlet__nav-item">
                                                <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseStatusDiv" role="button" aria-expanded="true" aria-controls="collapseStatusDiv">
                                                <span><i class="la la-arrows-v"></i><span>{{ trans('requests.approval') }}</span></span>
                                                </a>
                                            </li>
                                            <li class="m-portlet__nav-item">
                                                <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                                    <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @include('pmis.stopped.attachment')
                                @include('pmis.stopped.approval_view')
                                <div class="m-content" id="show-content">
                                    @if($record)
                                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row m-form__group_custom">
                                                    <div class="col-lg-3 col-md-3">
                                                    <label class="title-custom">{{ trans('requests.doc_type') }} :</label><br>
                                                    <span>{!!$record->documents['name_'.$lang]!!}</span>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3">
                                                    <label class="title-custom">{{ trans('requests.department') }} :</label><br>
                                                    <span>{!!$record->hr_departments['name_'.$lang]!!}</span>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3">
                                                    <label class="title-custom">{{ trans('requests.req_date') }} :</label><br>
                                                    <span>{!! dateCheck($record->request_date,$lang) !!}</span>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3">
                                                    <label class="title-custom">{{ trans('requests.req_number') }} :</label><br>
                                                    <span>{!!$record->doc_number!!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom">
                                                    <div class="col-lg-6 col-md-6">
                                                    <label class="title-custom">{{ trans('requests.goals') }} :</label><br>
                                                    <span>{!!$record->goals!!}</span>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6">
                                                    <label class="title-custom">{{ trans('requests.description') }} :</label><br>
                                                    <span>{!!$record->description!!}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-xl-9 col-lg-12">
                                            <div class="m-wizard__form">
                                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                                    <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                                        <dt>{{ trans('global.not_done') }}</dt>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="m-portlet m-portlet--mobile m-0" style="min-height:490px">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('stopped.summary_log')}}</div>
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="col">
                                            <div class="m-widget21__item proj_sum_wdg">
                                                <span class="m-widget21__icon"><i class="fl flaticon-users"></i></span>
                                                <div class="m-widget21__info px-1">
                                                    <span class="m-widget21__title py-0">{{ trans('stopped.work_stop_by') }}</span><br>
                                                    <span class="m-widget21__sub m-widget21__sub1">{{ $project_status->users->name }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col">
                                            <div class="m-widget21__item proj_sum_wdg">
                                                <span class="m-widget21__icon"><i class="fl flaticon-calendar-1"></i></span>
                                                <div class="m-widget21__info px-1">
                                                    <span class="m-widget21__title py-0">{{ trans('stopped.work_stop_at') }}</span><br>
                                                    <span class="m-widget21__sub m-widget21__sub1">{{ dateCheck($project_status->created_at,$lang,true) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col">
                                            <div class="m-widget21__item proj_sum_wdg">
                                                <span class="m-widget21__icon"><i class="la	la-align-justify"></i></span>
                                                <div class="m-widget21__info px-1">
                                                    <span class="m-widget21__title py-0">{{ trans('stopped.work_stop_desc') }}</span><br>
                                                    <span class="m-widget21__sub m-widget21__sub1">{{ $project_status->description }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col">
                                            <div class="m-widget21__item proj_sum_wdg">
                                                <span class="m-widget21__icon"><i class="fl flaticon-list-1"></i></span>
                                                <div class="m-widget21__info px-1">
                                                    <span class="m-widget21__title py-0">{{ trans('global.attachment') }}</span><br>
                                                    @if($project_status_attachment)
                                                        <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($project_status_attachment->id),'project_location_status_attachments')) }}">
                                                            <i class="fa fa-download"></i> {{ $project_status_attachment->filename }}.{{ $project_status_attachment->extension }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
@endsection