<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('stopped.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet--primary m-portlet--head-sm">
            <div class="m-portlet__head">
                <!-- Include Attachments Modal -->
                @php 
                    $attachments = getAttachments('requests_attachments',0,$record->id,'request');  
                    $attachment_table = 'requests_attachments';
                @endphp
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('requests.view') }}</h3>
                    </div>
                </div>                                   
                <!-- Include Attachments Modal -->
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseStatusDiv" role="button" aria-expanded="true" aria-controls="collapseStatusDiv">
                            <span><i class="la la-arrows-v"></i><span>{{ trans('requests.approval') }}</span></span>
                            </a>
                        </li>
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            @include('pmis.stopped.attachment')
            @include('pmis.stopped.approval_view')
            <div class="m-content" id="show-content">
                @if($record)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-3 col-md-3">
                                <label class="title-custom">{{ trans('requests.doc_type') }} :</label><br>
                                <span>{!!$record->documents['name_'.$lang]!!}</span>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                <label class="title-custom">{{ trans('requests.department') }} :</label><br>
                                <span>{!!$record->hr_departments['name_'.$lang]!!}</span>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                <label class="title-custom">{{ trans('requests.req_date') }} :</label><br>
                                <span>{!! dateCheck($record->request_date,$lang) !!}</span>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                <label class="title-custom">{{ trans('requests.req_number') }} :</label><br>
                                <span>{!!$record->doc_number!!}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-6 col-md-6">
                                <label class="title-custom">{{ trans('requests.goals') }} :</label><br>
                                <span>{!!$record->goals!!}</span>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                <label class="title-custom">{{ trans('requests.description') }} :</label><br>
                                <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>       
    </div>
</div>