<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('stopped.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->{'name_'.$lang} }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel" id="show_content">
        @if($records)
            <!-- check if has_data return yes -->
            @if((hasData($records)==1)? true: false)
                <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4" role="tablist">
                    @if($tabs)
                        @foreach($tabs as $item)
                            @if($item->code!="tab_visa")
                                <li class="nav-item m-tabs__item">
                                    <a href="javascript:void()" onclick="viewRecord('{{route('stopped.'.$item->code,$enc_id)}}','','GET','show_content')" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                        <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
                <div class="m-portlet--primary m-portlet--head-sm p-0" id="response_show">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('designs.architecture_list') }}</h3>
                            </div>
                        </div>                                   
                    </div>
                    <div class="m-content">
                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                            <div class="m-portlet__body">
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                        <tr class="bg-light">
                                            <th width="15%">{{ trans('global.urn') }}</th>
                                            <th width="25%">{{ trans('designs.responsible') }}</th>
                                            <th width="25%">{{ trans('designs.start_date') }}</th>
                                            <th width="25%">{{ trans('designs.complete_date') }}</th>
                                            <th width="10%">{{ trans('global.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                        @foreach($records AS $rec)
                                            <?php $rec_id = encrypt($rec->id); ?>
                                            <tr>
                                                <td>{!!$rec->urn!!}</td>
                                                <td>{!!$rec->employee['first_name']!!} {!!$rec->employee['last_name']!!}</td>
                                                <td>{!!dateCheck($rec->start_date,$lang)!!}</td>
                                                <td>{!!dateCheck($rec->end_date,$lang)!!}</td>
                                                <td>
                                                    <span class="dtr-data">
                                                        <span class="dropdown">
                                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                                <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('stopped.show_tab_architecture',[$rec_id,$enc_id]) }}','','GET','response_show')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                                            </div>
                                                        </span>
                                                    </span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>           
                    </div>
                </div>  
            <!-- check if has_data return no -->
            @elseif((hasData($records)==2)? true: false) 
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('designs.designs') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body" id="response_show">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('designs.data_exist') }}</label><br>
                                <span class="m-badge m-badge--warning m-badge--wide"> {{trans('global.'.$records->first()->has_data)}} </span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('designs.description') }}:</label>
                                {!! $records->first()->description !!}
                            </div>
                        </div>        
                    </div>
                </div>
            @endif 
        @else
            <div class="col-lg-12">
                <div class="m-wizard__form">
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                            <dt>{{ trans('global.not_done') }}</dt>
                        </div>
                    </div>  
                </div>
            </div>
        @endif    
    </div>
</div>