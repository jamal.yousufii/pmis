<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4" role="tablist">
    @if($tabs)
        @foreach($tabs as $item)
            @if($item->code!="tab_visa")
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" onclick="viewRecord('{{route('stopped.'.$item->code,$enc_id)}}','','GET','show_content')" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                        <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                    </a>
                </li>
            @endif
        @endforeach
    @endif
</ul>
<div class="m-portlet--primary m-portlet--head-sm p-0" id="response_show">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('designs.water_list') }}</h3>
            </div>
        </div>                                   
    </div>
    <div class="m-content">
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
            <div class="m-portlet__body">
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                        <tr class="bg-light">
                            <th width="15%">{{ trans('global.urn') }}</th>
                            <th width="25%">{{ trans('designs.responsible') }}</th>
                            <th width="25%">{{ trans('designs.start_date') }}</th>
                            <th width="25%">{{ trans('designs.complete_date') }}</th>
                            <th width="10%">{{ trans('global.action') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @foreach($records AS $rec)
                            <?php $rec_id = encrypt($rec->id); ?>
                            <tr>
                                <td>{!!$rec->urn!!}</td>
                                <td>{!!$rec->employee['first_name']!!}</td>
                                <td>{!!dateCheck($rec->start_date,$lang)!!}</td>
                                <td>{!!dateCheck($rec->end_date,$lang)!!}</td>
                                <td>
                                    <span class="dtr-data">
                                        <span class="dropdown">
                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('stopped.show_tab_water',[$rec_id,$enc_id]) }}','','GET','response_show')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                            </div>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>           
    </div>
</div>