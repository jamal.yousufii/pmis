<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead class="bg-light">
    <tr>
      <th width="10%">{{ trans('global.pro_urn') }}</th>
      <th width="30%">{{ trans('plans.project_name') }}</th>
      <th width="10%">{{ trans('plans.project_code') }}</th>
      <th width="10%">{{ trans('plans.project_type') }}</th>
      <th width="10%">{{ trans('stopped.work_stop_by') }}</th>
      <th width="10%">{{ trans('stopped.work_stop_at') }}</th>
      <th width="15%">{{ trans('stopped.stop_section') }}</th>
      <th width="5%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
      @foreach($records AS $rec)
        <?php $enc_id = encrypt($rec->id); ?>
        <tr>
          <td>{{ $rec->urn }}</td>
          <td>{{ $rec->name }}</td>
          <td>{{ $rec->code }}</td>
          <td>{{ $rec->project_type->{'name_'.$lang} }}</td>
          <td>{{ $rec->plan_location_status->first()->users->name }}</td>
          <td>{{ dateCheck($rec->plan_location_status->first()->created_at,$lang,true) }}</td>
          <td>{{ $rec->plan_location_status->first()->sections->{'name_'.$lang} }}</td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  @if(doIHaveRoleInDep(session('current_department'),"pmis_stopped","stop_view"))
                    @if($rec->plan_location_status->first()->section=='pmis_progress')
                      <a class="dropdown-item" href="{{ route('stopped.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                    @else
                      <a class="dropdown-item" href="{{ route('stopped.allProjects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                    @endif
                  @endif
                </div>
              </span>
            </span>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function() {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
        document.cookie = "no="+$(this).text();
  			var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&department_id="+'{!!$request->department_id!!}'+"&section="+'{!!$request->section!!}'+"&field="+'{!!$request->field!!}'+"&condition="+'{!! $request->condition!!}'+"&value="+'{!! $request->value !!}';
        serverRequest('{{ route("filter_progress") }}',dataString,'POST','searchresult',true);
  		}
  	});
  });
</script>