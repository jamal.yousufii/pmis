<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('implements.edit') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="implementsForm" method="post" enctype="multipart/form-data">
        <div class="m-portlet__body">
          @if($attachments)
            @php $i=0  @endphp
            @foreach($attachments as $item)
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                  <label class="title-custom">@if($i==0){{ trans('implements.attachment_type') }}: <span style="color:red;">*</span>@endif</label>
                  <select class="form-control m-input m-input--air select-2" name="attachment_type_{{$item->id}}" id="req_attachment_type_{{$item->id}}">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($attachment_type)
                      @foreach($attachment_type as $type)
                        @if($type->id==$item->type)
                          <option value="{{$type->id}}" selected='selected'>{{ $type->{'name_'.$lang} }} </option>
                        @else
                          <option value="{{$type->id}}">{{ $type->{'name_'.$lang} }} </option>
                        @endif
                      @endforeach
                    @endif
                  </select>
                  <div class="req_attachment_type_{{$item->id}}"></div>
                </div>
                <div class="col-lg-3">
                  <label class="title-custom">@if($i==0){{ trans('implements.attachment_name') }}: <span style="color:red;">*</span>@endif</label>
                  <input class="form-control m-input" type="text" name="attachment_name_{{$item->id}}" value="{{ $item->filename }}" id="req_attachment_name_{{$item->id}}">
                  <div class="req_attachment_name_{{$item->id}}"></div>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">@if($i==0) {{ trans('global.attachment') }}: @endif</label>
                    <div id="new_file_{{$item->id}}" @if($item->path!=null) style="display:none;" @endif>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file_{{$item->id}}[]" id="file_{{$item->id}}" onchange="chooseFile(this.id)">
                            <label class="custom-file-label" id="file-label" for="file_{{$item->id}}">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                    </div>
                    <div id="exist_file_{{$item->id}}" @if($item->path==null) style="display:none;" @endif>
                        <div class="custom-file">
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($item->id),'implement_attachments')) }}">
                                <i class="fa fa-download"></i> {{ $item->filename }}.{{ $item->extension }} 
                            </a><br>
                            <a href="javascript:void();" onclick="destroyUpdatedFile('{{ route("deleteUpdatedFile", array(encrypt($item->id),'implement_attachments')) }}','exist_file_{{$item->id}}','new_file_{{$item->id}}');"><i class="flaticon-delete"></i></a>
                            <span class="m-form__help">{{ trans('designs.new_file') }}</span>
                        </div>
                    </div>
                    <input type="hidden" name="file_id[]" value="{{$item->id}}" />  
                </div>
                <div class="col-lg-1">
                  @if($i==0)
                    <label class="title-custom">&nbsp;</label>
                    <button type="button" class="btn btn-primary m-btn m-btn--air btn-sm" onclick="add_more('attachment_div','{{route('more_attachment')}}','{{$attachments->count()}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                  @endif
                </div>
              </div>
              @php $i++ @endphp
            @endforeach
          @endif
          <div id="attachment_div"></div><!-- Display mor attachments -->
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('implements.description') }}:</label>
              <textarea class="form-control m-input m-input--air" name="description" rows="3">{{ $record->description }}</textarea>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <input type="hidden" name="plan_id" value="{{$enc_plan}}"/>
                  <input type="hidden" name="enc_id" value="{!!$enc_id!!}"/>
                  <button type="button" onclick="doEditRecordWithAttachments('{{route('implements/update')}}','implementsForm','POST',redirectFunction);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>