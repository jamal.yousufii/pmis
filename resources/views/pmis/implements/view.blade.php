@extends('master')
@section('head')
    <title>{{ trans('implements.implements') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile" id="showContent">
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <!-- Include Summary Modal -->
                        @include('pmis.summary.summary_modal')
                        <ul class="m-portlet__nav">
                             @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_add") and !$record)
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:void()" onclick="addRecord('{{route('implements.create')}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                        <span><i class="la la-cart-plus"></i><span>{{ trans('implements.add') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_approval") and $record and $record->operation==0 and $record->completed==1)
                                @include('pmis.implements.approval_add')
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-success m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#ApprovalEditModal">
                                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('requests.update_status') }}</span></span>
                                    </a>
                                </li>
                            @elseif($record and $record->operation!=0)
                                @include('pmis.implements.approval_view')
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-success m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#ApprovalViewModal">
                                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('requests.view_status') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","view_pro_summary"))
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('implement',session('current_department')) }}">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if($record and $record->completed==0)
                    <div class="m-portlet__head h-50 pt-2">
                        <form enctype="multipart/form-data" id="completeForm" method="POST">
                            <table>
                                <td>
                                    <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                                    <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                                        <label><input type="checkbox" id="complete" name="complete" value="1" onclick="completeRecord('{{ route('completeRecords') }}','complete','completeForm','POST','response_div')"><span></span></label>
                                    </span>
                                    </span>
                                </td>
                                <td><label class="title-custom">{{ trans('global.complete') }}</label></td>
                            </table>
                            <input type="hidden" name="table" value="implements" />
                            <input type="hidden" name="rec_id" value="{{ encrypt($record->id) }}" />
                            <input type="hidden" name="project_id" value="{{ encrypt($record->project_id) }}" />
                        </form>
                    </div>
                @endif
                @alert()
                @endalert
                @if($record)
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('implements.view') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_edit") and $record->operation!='1')
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" onclick="addRecord('{{route('implements.edit',encrypt($record->id))}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                            <span><i class="la la-edit"></i><span>{{ trans('implements.edit') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($plan->share->count()==0 and $record->completed=='1' and (doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_complete") || doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_stop")))
                                    <!-- Include complete & stop modal -->
                                    @include('pmis.implements.complete_modal')
                                    @include('pmis.implements.stop_modal')
                                    <div class="btn-group open">
                                        <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                            <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                                        </button>
                                        <ul class="dropdown-menu mt-2" role="menu">
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_complete"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                        <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_stop"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                        <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                                @if($plan->share->count()>0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    </li>
                                @elseif(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_share") and $record->operation=='1')
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- Include sharing modal -->
                    @include('pmis.implements.share_modal')
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="searchresult">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                        <tr class="bg-light">
                                            <th width="33.33%" style="border-top:0px;">{{trans('implements.attachment_type')}}</th>
                                            <th width="33.33%" style="border-top:0px;">{{trans('implements.attachment_name')}}</th>
                                            <th width="33.33%" style="border-top:0px;">{{trans('global.attachment')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($attachments)
                                            @foreach($attachments as $item)
                                                <tr>
                                                    <td>@if($item->type) {{ $item->static_data->{'name_'.$lang} }} @endif</td>
                                                    <td>{{ $item->filename }}</td>
                                                    <td>
                                                        @if($item->path!=null)
                                                            <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($item->id),'implement_attachments')) }}">
                                                                <i class="fa fa-download"></i> {{ $item->filename }}.{{ $item->extension }}
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12 col-md-12">
                                    <label class="title-custom">{{ trans('implements.description') }} :</label><br>
                                    <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
