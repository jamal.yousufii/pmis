<!-- Approval Start-->
<div id="ApprovalEditModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head bg-color-dark">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ trans('requests.update_status') }}</h3>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;
        </button>
      </div>
      <div id="Approval-div">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="approveForm" method="post"> 
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.status') }}: <span style="color:red;">*</span></label>
                <div id="status" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2" name="status">
                    <option value="">{{ trans('requests.select') }}</option>
                    <option value="1">{{ trans('requests.req_approve') }}</option>
                    <option value="2">{{ trans('requests.req_reject') }}</option>
                  </select>
                </div>
                <div class="status error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.date') }}: <span style="color:red;">*</span></label>
                <input class="form-control m-input datePicker errorDiv" type="text" name="date" id="date">
                <div class="date error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.number') }}: </label>
                <input class="form-control m-input" type="number" min="0" name="number" id="number">
              </div>
            </div> 
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <label class="title-custom">{{ trans('requests.description') }}:</label>
                <textarea class="form-control m-input m-input--air" name="description" rows="3"></textarea>
                <span class="m-form__help">{{ trans('requests.desc_note') }}</span>
              </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{ encrypt($record->id) }}"/>
            <div class="col-lg-12">
              <div class="m-form__actions">
                <button type="button" onclick="storeRecord('{{route('approve_implement')}}','approveForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-success">{{ trans('global.submit') }}</button>
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
              </div>
            </div>
          </div>
        </form>
        <!--end::Form-->
      </div>
    </div>
  </div>
</div>
@section('js-code')
  <script type="text/javascript">
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
  </script>
@endsection
<!-- Attachments End-->
