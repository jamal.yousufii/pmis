
<div class="m-portlet__body">
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4 col-md-4">
            <label class="title-custom">{{ trans('estimation.start_date') }} :</label><br>
            <span>{!!dateCheck($record->start_date,$lang)!!}</span>
        </div>
        <div class="col-lg-4 col-md-4">
                <label class="title-custom">{{ trans('estimation.end_date') }} :</label><br>
                <span>{!!dateCheck($record->end_date,$lang)!!}</span>
            </div>
            <div class="col-lg-4 col-md-4">
                <label class="title-custom">{{ trans('estimation.estimated_cost') }} :</label><br>
                <span>{!!$record->cost!!}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12 col-md-12">
                <label class="title-custom">{{ trans('estimation.description') }} :</label><br>
                <span>{!!$record->description!!}</span>
            </div>
        </div>
    </div>
    <!-- Display Project Details -->
    @if($bill_quantities->count()>0)
        <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
            <div class="m-portlet__head-tools">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary btn-sm  pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom" id="details-content">
                    <table class="table table-striped-">
                        <tbody>
                            <tr>
                                <th width="40%" class="top-no-border">{{ trans('estimation.operation_type') }}</th>
                                <th width="15%" class="top-no-border">{{ trans('estimation.unit') }}</th>
                                <th width="15%" class="top-no-border">{{ trans('estimation.amount') }}</th>
                                <th width="20%" class="top-no-border">{{ trans('estimation.total_price') }}</th>
                                <th width="10%" class="top-no-border">{{ trans('global.action') }}</th>
                            </tr>
                        </tbody>
                    </table>
                    @foreach($bill_quantities as $item)
                        <table class="table table-striped-" id="detail_content_{{$item->id}}">
                            <tbody>  
                                <tr>
                                    <td width="40%">{{ limit_text($item->operation_type) }}</td>
                                    <td width="15%">{{ $item->unit->name_dr }}</td>
                                    <td width="15%">{{ $item->amount }}</td>
                                    <td width="20%">{{ $item->total_price }}</td>
                                    <td width="10%">
                                        <span class="dtr-data">
                                            <span class="dropdown">
                                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit"))
                                                        <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                                    @endif
                                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_view"))
                                                        <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#ShowDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="ShowDivDetails-{{$item->id}}"><i class="la la-eye"></i>{{ trans('global.view') }}</span>
                                                    @endif
                                                </div>
                                            </span>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Include BQ View and Edit -->
                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
                            @include('pmis.estimations.quantities_edit')   
                        </div>
                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="ShowDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
                            @include('pmis.estimations.quantities_view')   
                        </div>
                    @endforeach 
                    <!-- Pagination -->
                    @if(!empty($bill_quantities))
                        {!!$bill_quantities->links('pagination')!!}
                    @endif  
                </div>
            </div>
        </div>
    @endif
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if ($(this).attr('href') != '#') {
            document.cookie = "no="+$(this).text();
                var dataString = '';
                dataString += "&page="+$(this).attr('id')+"&ajax="+1;
                $.ajax({
                    url: '{{ route("estimations.show",$enc_id) }}',
                    data: dataString,
                    type: 'get',
                    beforeSend: function(){
                        $('#searchresult').html('<span style="position:relative;left:10%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                    },
                    success: function(response)
                    {
                        $('#searchresult').html(response);
                    }
            });
            }
        });
    });
</script>
