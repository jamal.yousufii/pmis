{{-- begin::Collpas Div --}}
<div class="code notranslate cssHigh collapse" id="collapseShareDiv" style="border-bottom:1px solid #ebedf2;">
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2">
        <div class="m-portlet__body">
            <div class="m-portlet__body table-responsive">
                <!--begin::Share-->
                <table class="table m-table m-table--head-separator-default">
                    <thead>
                        <tr class="bg-color-dark white-color">
                            <th width="15%">{{trans('global.shared_to')}}</th>
                            <th width="10%">{{trans('global.section')}}</th>
                            <th width="8%">{{trans('global.doc_number')}}</th>
                            <th width="10%">{{trans('global.doc_date')}}</th>
                            <th width="25%">{{trans('global.description')}}</th>
                            <th width="10%">{{trans('global.shared_by')}}</th>
                            <th width="15%">{{trans('global.shared_date')}}</th>
                            <th width="7%">{{trans('global.unshare')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($plan->share)
                            @foreach($plan->share as $shared)
                                <tr>
                                    <td>{{ $shared->department->{'name_'.$lang} }}</td>
                                    <td>{{ $shared->section->{'name_'.$lang} }}</td>
                                    <td>{{ $shared->doc_number }}</td>
                                    <td>{{ dateCheck($shared->doc_date,$lang) }}</td>
                                    <td>{{ $shared->description }}</td>
                                    <td>{{ $shared->user->name }}</td>
                                    <td>{{ dateCheck($shared->created_at,$lang,true) }}</td>
                                    <td>
                                        @if($plan->procurement()->count()>0)
                                            <i class="flaticon-delete"></i>
                                        @elseif(doIHaveRoleInDep(session('current_department'),"pmis_implement","imp_unshare"))
                                            <a href="javascript:void()" class="btn btn-warning btn-sm" onclick="unshare('{{ route("share.destroy", ['id'=>encrypt($shared->id),'rec_id'=>encrypt($plan->id),'table'=>'implements','column'=>'project_id']) }}')">
                                                <i class="flaticon-delete"></i>
                                            </a>  
                                        @else
                                            <i class="flaticon-delete"></i> 
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <!--end::Share-->
            </div>
        </div>
    </div>
</div>   
{{-- End of Collpas div  --}}
{{-- Start of the Share Modal --}}
<div id="shareModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon"><i class="la la-leanpub"></i></span>
                                <h3 class="m-portlet__head-text">{{trans('global.share')}}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="closeBtn" aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="sharing_form">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-6">
                                    <label class="title-custom">{{ trans('global.section') }} : <span style="color:red;">*</span></label>
                                    <div id="share_to_code" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2" name="share_to_code" id="code" onchange="bringData(this.id,'{{ route('bringDepartments') }}','POST','share_to')">
                                            <option value="">{{trans('global.select')}}</option>
                                            @foreach($sections as $item)
                                                <option value="{{$item->allowedSections->code}}">{{ $item->allowedSections->{'name_'.$lang} }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="share_to_code error-div" style="display:none;"></div>
                                </div>  
                                <div class="col-lg-6">
                                    <label class="title-custom">{{ trans('authentication.department') }} : <span style="color:red;">*</span></label>
                                    <div id="share_to" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2" name="share_to">
                                            <option value="">{{trans('global.select')}}</option>
                                        </select>
                                    </div>
                                    <div class="share_to error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-6">
                                    <label class="title-custom">{{ trans('global.doc_number') }}:</label>
                                    <input class="form-control m-input" type="text" value="" name="doc_number" id="doc_number">
                                </div>
                                <div class="col-lg-6">
                                    <label class="title-custom">{{ trans('global.doc_date') }}:</label>
                                    <input class="form-control m-input datePicker" type="text" value="" name="doc_date" id="doc_date">
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="title-custom" for="request_share_desc">{{trans('global.description')}} :</label>
                                <textarea class="form-control m-input" id="request_share_desc" name="description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions align">
                                {{-- Hiddend Inputs Start --}}
                                <input type="hidden" name='share_from' value="{{decrypt(session('current_department'))}}">
                                <input type="hidden" name='share_from_code' value="{{session('current_section')}}">
                                <input type="hidden" name='table' value="implements">
                                <input type="hidden" name='column' value="project_id">
                                <input type="hidden" name='record_id' value="{{$plan->id}}">
                                {{-- Hiddend Inputs End --}}
                                <button type="button" class="btn btn-primary" onclick="shareRecord('{{route('share.store')}}','sharing_form',this.id);" id="shareBtn">{{trans('requests.share')}}</button>
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    @if ($lang=="en")
        $(".datePicker").attr('type', 'date');
    @else
        $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif
});
</script>
{{-- End of the Share Modal  --}}