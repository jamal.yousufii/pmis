
<div id="attachment_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-4">
        <select class="form-control m-input m-input--air select-2" name="attachment_type[]" id="req_attachment_type_{!!$number!!}">
            <option value="">{{ trans('global.select') }}</option>
            @if($attachments)
                @foreach($attachments as $item)
                    <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                @endforeach
            @endif
        </select>
        <div class="req_attachment_type_{!!$number!!}"></div>
    </div>
    <div class="col-lg-3">
        <input class="form-control m-input" type="text" name="attachment_name[]" id="req_attachment_name_{!!$number!!}">
        <div class="req_attachment_name_{!!$number!!}"></div>
    </div>
    <div class="col-lg-4">
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="file[]" id="file_{!!$number!!}" onchange="chooseFile(this.id)" required>
            <label class="custom-file-label" id="file-label" for="file_{!!$number!!}">{{ trans('requests.att_choose') }}</label>
        </div>
    </div>
    <div class="col-lg-1">
        <button type="button" id="attachment_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('attachment_div_{!!$number!!}',{!!$number!!},'attachment_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>