<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('implements.add') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_implement","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                  <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="implementsForm" method="post" enctype="multipart/form-data">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('implements.attachment_type') }}: <span style="color:red;">*</span></label>
              <div id="div_attachment_type" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" name="attachment_type[]" id="attachment_type">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($attachment_type)
                    @foreach($attachment_type as $item)
                      <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="attachment_type error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
              <label class="title-custom">{{ trans('implements.attachment_name') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv required" type="text" name="attachment_name[]" id="attachment_name">
              <div class="attachment_name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('global.attachment') }}:</label>
              <div class="custom-file">
                  <input type="file" class="custom-file-input errorDiv" name="file[]" id="file" onchange="chooseFile(this.id)">
                  <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
              </div>
              <span class="m-form__help">{{ trans('global.file_extention') }}</span>
            </div>
            <div class="col-lg-1">
              <label class="title-custom">&nbsp;</label>
              <button type="button" class="btn btn-primary m-btn m-btn--air btn-sm" onclick="add_more('attachment_div','{{route('more_attachment')}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
            </div>
          </div>
          <div id="attachment_div"></div><!-- Display mor attachments -->
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('implements.description') }}:</label>
              <textarea class="form-control m-input m-input--air" name="description" rows="2"></textarea>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <input type="hidden" name="enc_plan" value="{{$enc_plan}}"/>
                  <button type="button" onclick="storeRecord('{{route('implements.store')}}','implementsForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
