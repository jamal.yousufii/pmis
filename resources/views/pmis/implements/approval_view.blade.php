<!-- Approval View Start-->
<div id="ApprovalViewModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head bg-color-dark">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ trans('requests.view_status') }}</h3>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
      </div>
      <div id="Approval-div">
        <!--begin::Form-->
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"> 
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.status') }}: </label><br>
                <span>
                    @if($record->operation==1)
                        {{ trans('requests.req_approve') }}
                    @elseif($record->operation==2)
                        {{ trans('requests.req_reject') }}
                    @endif
                </span>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.date') }}: </label><br>
                <span>{{ dateCheck($record->operation_date,$lang) }}</span>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.number') }}: </label><br>
                <span>{!!$record->operation_number!!}</span>
              </div>
            </div> 
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <label class="title-custom">{{ trans('requests.description') }}:</label><br>
                <span>{!!$record->operation_description!!}</span>
              </div>
            </div>
            <input type="hidden" name="id" value="{!!$enc_id!!}"/>
            <div class="col-lg-12">
              <div class="m-form__actions">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary">{{ trans('global.back') }}</button>
              </div>
            </div>
          </div>
        </div>
        <!--end::Form-->
      </div>
    </div>
  </div>
</div>
<!-- Approval View End-->
