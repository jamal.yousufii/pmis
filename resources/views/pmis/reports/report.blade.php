@extends('master')
@section('head')
  <title>{{ trans('global.reports') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile mb-3">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text text-white">{{ trans('global.reports') }}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!--Begin::Section-->
    <div class="m-portlet mb-3" id="m_portlet">
      <div class="m-portlet__body">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('global.select') }}: </label>
          <select class="form-control m-input m-input--air select-2" id="static_id" onchange="javascript:bringPage()">
            @if($report_option)
              <option value="0">{{ trans('global.select') }}</option>
              @foreach($report_option as $item)
                <option value="{!! $item->code !!}">{{ $item->{'name_'.$lang} }}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>
    </div>
    <!--End::Section-->
  </div>
  <span id="showContent"></span>
@endsection
