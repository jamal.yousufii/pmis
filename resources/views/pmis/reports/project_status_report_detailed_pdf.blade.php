<html>
    <head>
        <?php
        $lang = get_language();
        if($lang=="en"){
            $dir = "";
            $direction = "";

        }else {
            $dir = ".rtl"; 
            $direction = "rtl";
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('global.project_status_report') }}</title>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('public/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public/css/custome.css')}}">      
    </head>
    <body dir="{{$direction}}">
        <table width="100%">
            <tr class="text-center">
                <td width="100%" class="text-center"><img src="{!!asset('public/img/logo.png')!!}" style="height: 115px; width: 115px"/></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-1') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-2') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-3') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ $department_name }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.project_status_report') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.'.$code) }}</h3></td>
            </tr>
        </table>
        <br>
        <table border="1" width="100%">
            <tr class="bg-light">
                <th width="4%" class="py-3 text-center">{{ trans('global.number') }}</th>
                <th width="6%" class="py-3">{{ trans('global.urn') }}</th>
                <th width="10%" class="py-3">{{ trans('progress.project_name') }}</th>
                <th width="6%" class="py-3">{{ trans('plans.code') }}</th>
                <th width="6%" class="py-3">{{ trans('plans.category') }}</th>
                <th width="6%" class="py-3">{{ trans('plans.project_type') }}</th>
                @if($code!='pmis_procurement')
                    <th width="6%" class="py-3">{{ trans('global.project_location') }}</th>
                @endif
                @if($code=='pmis_survey')
                    <th width="5%" class="py-3">{{ trans('surveys.data_exist1') }}</th>
                    <th width="5%" class="py-3">{{ trans('surveys.start_date') }}</th>
                    <th width="5%" class="py-3">{{ trans('surveys.end_date') }}</th>
                    <th width="6%" class="py-3">{{ trans('surveys.head_team') }}</th>
                @elseif($code=='pmis_design')
                    <th width="5%" class="py-3">{{ trans('designs.data_exist1') }}</th>
                    <th width="5%" class="py-3">{{ trans('designs.start_date') }} {{ trans('designs.architecture') }}</th>
                    <th width="5%" class="py-3">{{ trans('designs.end_date') }} {{ trans('designs.architecture') }}</th>
                    <th width="6%" class="py-3">{{ trans('designs.responsible') }} {{ trans('designs.architecture') }}</th>
                @elseif($code=='pmis_estimation')
                    <th width="4%" class="py-3">{{ trans('estimation.start_date') }} {{ trans('estimation.estimation') }}</th>
                    <th width="4%" class="py-3">{{ trans('estimation.end_date') }} {{ trans('estimation.estimation') }}</th>
                    <th width="4%" class="py-3">{{ trans('estimation.responsible') }} {{ trans('estimation.estimation') }}</th>
                    <th width="4%" class="py-3">{{ trans('estimation.bq_exists') }}</th>
                    <th width="5%" class="py-3">{{ trans('estimation.estimated_cost') }}</th>
                @elseif($code=='pmis_procurement')
                    <th width="10%" class="py-3">{{ trans('procurement.tin') }}</th>
                    <th width="11%" class="py-3">{{ trans('procurement.current_location') }}</th>
                @endif
                <th width="6%" class="py-3">{{ trans('global.status') }}</th>
                <th width="9%" class="py-3">{{ trans('designs.description') }}</th>
            </tr>
            @if($records)
                @foreach($records as $item)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $item->urn }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->code }}</td>
                        @if($code=='pmis_procurement')
                            <td>{{ $item->category->{'name_'.$lang} }}</td>
                            <td>{{ $item->project_type->{'name_'.$lang} }}</td>
                        @else
                            <td>{{ $item->category }}</td>
                            <td>{{ $item->type }}</td>
                        @endif
                        @if($code!='pmis_procurement')
                            <td>
                                {{$item->province->{'name_'.$lang} }} 
                                @if($item->district_id)/ {{$item->district->{'name_'.$lang} }} @endif 
                                @if($item->village_id) / {{$item->village->{'name_'.$lang} }} @endif
                            </td>
                        @endif
                        @if($code=='pmis_survey')
                            <td>
                                @if($item->has_survey == 'yes')
                                    {{ trans('global.yes') }}
                                @elseif($item->has_survey == 'no')
                                    {{ trans('global.no') }}
                                @endif
                            </td>
                        @elseif($code=='pmis_design')
                            <td>
                                @if($item->has_data == 'yes')
                                    {{ trans('global.yes') }}
                                @elseif($item->has_data == 'no')
                                    {{ trans('global.no') }}
                                @endif
                            </td>
                        @endif
                        @if($code!='pmis_procurement')
                            <td>{{ dateCheck($item->start_date, $lang) }}</td>
                            <td>{{ dateCheck($item->end_date, $lang) }}</td>
                            <td>{{ $item->first_name }}</td>
                        @endif
                        @if($code=='pmis_estimation')
                            <td>
                                @if($item->bill_quantities_est->first())
                                    {{ trans('global.yes') }}
                                @else
                                    {{ trans('global.no') }}   
                                @endif
                            </td>
                            <td>
                            {{ number_format($item->cost) }}
                            </td>
                        @elseif($code=='pmis_procurement')
                            <td>
                                @if($item->procurement)
                                    {{ $item->procurement->tin }}
                                @endif
                            </td>
                            <td>
                                @if($item->procurement)
                                    @if($item->procurement->financial_letter!='' || $item->procurement->company!='')
                                        {{ trans('procurement.construction') }}
                                    @elseif($item->procurement->planning!='')
                                        {{ trans('procurement.planning') }}
                                    @elseif($item->procurement->executive!='')
                                        {{ trans('procurement.executive') }}
                                    @endif
                                @endif 
                            </td>
                        @endif
                        <td class="text-center">
                            @if($code=='pmis_procurement' and $item->procurement)
                                @if($item->procurement->process == '0')
                                    <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
                                @elseif($item->procurement->process == '1')
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
                                @else
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
                                @endif
                            @else
                                @if($item->status == '0')
                                    <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
                                @elseif($item->status == '1')
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
                                @else
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
                                @endif
                            @endif
                        </td>
                        <td>{!! strip_tags($item->description) !!}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </body>
</html>
