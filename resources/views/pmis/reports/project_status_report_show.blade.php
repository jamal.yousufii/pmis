<div class="m-portlet  m-portlet--full-height  m-portlet--unair">
    <div class="m-portlet__body">
        @if($records)
            <!-- Generate Reprot Button -->
            <a class="btn btn-info m-btn--custom m-btn--icon btn-sm mb-2" href="{{route('project_status_report_excel',['department_id'=>encrypt($department_id)])}}">
                <span><i class="la la-print"></i> <span>{{ trans('global.report_excel') }}</span></span>
            </a>
            <!-- Generate Reprot Button -->
            <a class="btn btn-info m-btn--custom m-btn--icon btn-sm mb-2" href="{{route('project_status_report_pdf',['department_id'=>encrypt($department_id)])}}">
                <span><i class="la la-print"></i> <span>{{ trans('global.report_pdf') }}</span></span>
            </a>
        @endif
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <table class="table " width="100%">
                    <thead>
                        <tr class="bg-dark text-white">
                            <th scope="col" width="10%">{{ trans('global.number') }}</th>
                            <th scope="col" width="20%">{{ trans('global.section') }}</th>
                            <th scope="col" width="30%">{{ trans('global.departments') }}</th>
                            <th scope="col" width="10%">{{ trans('report.pending_projects') }}</th>
                            <th scope="col" width="10%">{{ trans('report.progress_projects') }}</th>
                            <th scope="col" width="10%">{{ trans('report.completed_projects') }}</th>
                            <th scope="col" width="10%">{{ trans('report.total_projects') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($records)
                            @foreach($records as $key=>$val)
                                @php
                                    $key = explode('-',$key);
                                    $total = 0;
                                    if(isset($val[2])) { $total = $total+$val[2]; } 
                                    if(isset($val[0])) { $total = $total+$val[0]; } 
                                    if(isset($val[1])) { $total = $total+$val[1]; } 
                                @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $key[1] }}</td>
                                    <td>{{ $key[0] }}</td>
                                    <td>@if(isset($val[2])) <a href="#" onclick="projectList('{{route('project_status_report_list',[$department_id,$key[4],'2',$key[2],$key[3]])}}','project_content')" data-toggle="modal" data-target="#project_list">{{ $val[2] }}</a> @else {{0}} @endif</td>
                                    <td>@if(isset($val[0])) <a href="#" onclick="projectList('{{route('project_status_report_list',[$department_id,$key[4],'0',$key[2],$key[3]])}}','project_content')"  data-toggle="modal" data-target="#project_list">{{ $val[0] }}</a> @else {{0}} @endif</td>
                                    <td>@if(isset($val[1])) <a href="#" onclick="projectList('{{route('project_status_report_list',[$department_id,$key[4],'1',$key[2],$key[3]])}}','project_content')"  data-toggle="modal" data-target="#project_list">{{ $val[1] }}</a> @else {{0}} @endif</td>
                                    <td>{{ $total }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="m-widget16__stats">
                        <div class="m-widget16__visual">
                            <div id="workProgress" style="height: 500px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- List project`s users: Start -->
<div id="project_list" class="modal fade bd-example-modal-lg"  role="dialog" aria-labelledby="project_list" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">           
            <div class="modal-header bg-color-dark pb-0">
                <div class="m-portlet__head-title">
                    <h4 class="m-portlet__head-text text-white"><i class="fa flaticon-box"></i> {{ trans('plans.list') }}</h4>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content pt-0" id="project_content">
            </div>
        </div>
    </div>
</div>
<!-- Assign project to users: End -->
<script type="text/javascript">
    $(document).ready(function()
    {
        var workProgress = echarts.init(document.getElementById('workProgress'));
        var data = [
            @if($records)
                @foreach($records as $key=>$val)
                    @php
                    $total = 0;
                    if(isset($val[2])) { $total = $total+$val[2]; } 
                    if(isset($val[0])) { $total = $total+$val[0]; } 
                    if(isset($val[1])) { $total = $total+$val[1]; } 
                    @endphp
                    '{{ $total }}',
                @endforeach
            @endif
        ];
        var yMax = 250;
        var dataShadow = [];
        for (var i = 0; i < data.length; i++) {
            dataShadow.push(yMax);
        }
        var workProgressOption = {
            tooltip: {
                    trigger: 'item'
            },
            dataLabels: {
                enabled: true
            },
            xAxis: {
                data: [
                    @if($records)
                        @foreach($records as $key=>$val)
                            @php
                                $key = explode('-',$key);
                            @endphp
                            '{{  $key[1] }} | {{ $key[0] }}',
                        @endforeach
                    @endif
                ],
                axisLabel: {
                    inside: true,
                    textStyle: {
                        color: '#575962'
                    },
                    fontWeight: 'bold',
                    rotate: 90,
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                z: 10
            },
            yAxis: {
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#999'
                    }
                }
            },
            dataZoom: [
                {
                    type: 'inside'
                }
            ],
            series: [
                { 
                    // For shadow
                    type: 'bar',
                    itemStyle: {
                        color: 'rgba(0,0,0,0.05)'
                    },
                    barGap: '-100%',
                    barCategoryGap: '40%',
                    data: dataShadow,
                    animation: false,
                },
                {
                    type: 'bar',
                    itemStyle: {
                        color: new echarts.graphic.LinearGradient(
                            0, 0, 0, 1,
                            [
                                {offset: 0, color: '#83bff6'},
                                {offset: 0.5, color: '#188df0'},
                                {offset: 1, color: '#188df0'}
                            ]
                        )
                    },
                    emphasis: {
                        itemStyle: {
                            color: new echarts.graphic.LinearGradient(
                                0, 0, 0, 1,
                                [
                                    {offset: 0, color: '#2378f7'},
                                    {offset: 0.7, color: '#2378f7'},
                                    {offset: 1, color: '#83bff6'}
                                ]
                            )
                        }
                    },
                    data:  [
                        @if($records)
                            @foreach($records as $key=>$val)
                                @php
                                $total = 0;
                                if(isset($val[2])) { $total = $total+$val[2]; } 
                                if(isset($val[0])) { $total = $total+$val[0]; } 
                                if(isset($val[1])) { $total = $total+$val[1]; } 
                                @endphp
                                '{{ $total }}',
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        workProgress.setOption(workProgressOption);
    });
</script>