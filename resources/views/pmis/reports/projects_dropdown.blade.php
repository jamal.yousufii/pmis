<option value="-" selected>{{ trans('global.all') }}</option>
@if($projects)
    @foreach($projects as $item)
        <option value="{!!$item->id!!}">{{ $item->name }}</option>
    @endforeach
@endif