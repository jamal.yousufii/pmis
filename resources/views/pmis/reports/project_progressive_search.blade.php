<div class="m-portlet mb-2">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{trans('report.project_progressive_report')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#dailyReportCollapse" role="button" aria-expanded="true" aria-controls="graphCollapse">
                        <span><i class="la la-arrows-v"></i><span>{{ trans('global.view') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="code notranslate cssHigh collapse show" id="dailyReportCollapse">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="search_form" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                        <div id="div_department_id" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" id="department_id" name="department_id" onchange="viewRecord('{{ route('report.bringCategoriesByDirectorate') }}','id='+this.value,'POST','category_id')">
                                @if($departments)
                                    <option value="" selected>{{ trans('global.select') }}</option>
                                    @foreach($departments as $item)
                                        <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="department_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('plans.proj_category') }}:</label>
                        <select class="form-control m-input m-input--air select-2" id="category_id" name="category_id" onchange="viewRecord('{{ route('report.bringProjectTypesByCategory') }}','id='+this.value,'POST','project_type_id')">
                            <option value="" selected>{{ trans('global.select') }}</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('plans.project_type') }}:</label>
                        <select class="form-control m-input m-input--air select-2" id="project_type_id" name="project_type_id" onchange="viewRecord('{{ route('report.bringProjectsByType') }}','id='+this.value,'POST','projects_id')">
                            <option value="" selected>{{ trans('global.select') }}</option>
                        </select>
                        <div class="project_type_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('global.projects') }}: </label>
                        <select class="form-control m-input m-input--air select-2" id="projects_id" name="projects_id[]" multiple>
                            <option value="-" selected>{{ trans('global.all') }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('global.status') }}: </label>
                        <select class="form-control m-input m-input--air select-2" id="status" name="status">
                            <option value="" selected>{{ trans('global.select') }}</option>
                            <option value="0">{{ trans('progress.progress') }}</option>
                            <option value="1">{{ trans('completed.completed') }}</option>
                            <option value="2">{{ trans('stopped.stopped') }}</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{trans('global.from_date')}} : <span style="color:red;">*</span></label> ({{ trans('report.problems_between') }})
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input datePicker required" value="" name="from_date" id="from_date"  style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}">
                            <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                        </div>
                        <div class="from_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{trans('global.to_date')}}: <span style="color:red;">*</span></label> ({{ trans('report.problems_between') }})
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input datePicker required" value="" name="to_date" id="to_date"  style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}">
                            <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                        </div>
                        <div class="to_date error-div" style="display:none;"></div>
                    </div> 
                    <div class="col-lg-3">
                        <label class="title-custom">&nbsp;</label><br>
                        <a href="#" class="btn btn-primary m-btn m-btn--icon btn-sm" onclick="storeData('{{route('report.project_progressive_report')}}','search_form','POST','search_result',put_content,true,'dailyReportCollapse');">
                            <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div id="search_result" class="pt-0"></div>
@section('js-code')
  <script type="text/javascript">
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
    $('.select-2').select2();
  </script>
@endsection