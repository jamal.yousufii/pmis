 <div class="row">
    <div class="col-lg-12">
        <div class="m-portlet">
            <table width="100%" border="1">
                    <tr>
                        <th colspan="4" class="text-center">
                            ریاست جمهوری اسلامی افغانستان<br>
                            اداره عملیاتی و حمایوی انکشاف ملی ریاست جمهوری ا.ا
                        </th>
                    </tr>
                    <tr>
                        <th class="p-1" width="20%">{{trans('daily_report.reported_date')}}</th>
                        <td class="p-1" width="30"><span style="font-weight:bold;">{{ trans('global.from_date')}} : </span>{{$from_report_date  }} <span style="font-weight:bold;">{{trans('global.to_date')}} : </span>{{$to_report_date}}</td>
                        <th class="p-1" width="20%">{{trans('daily_report.report_no')}}</th>
                        <th class="p-1" width="30%"></th>
                    </tr>
                    <tr>
                        <th class="p-1">{{trans('daily_report.project_name')}}</th>
                        <td class="p-1">{{$project->name}}</td>
                        <th class="p-1">{{trans('daily_report.project_location')}}</th>
                        <td class="p-1">{{$location->province->{'name_'.$lang} }} @if($location->district_id)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village_id) / {{$location->village->{'name_'.$lang} }} @endif </td>
                    </tr>
                    <tr>
                        <th class="p-1">{{trans('daily_report.project_code')}}</th>
                        <td class="p-1">{{$project->code}}</td>
                        <th class="p-1">{{trans('daily_report.contractor')}}</th>
                        <td class="p-1">{{getContractorNameById($project->project_contractor->contractor_id,true)}}</td>
                    </tr>
                    <tr>
                        <th class="p-1">{{trans('daily_report.start_date')}}</th>
                        <td class="p-1">{{ datecheck($project->project_start_date,$lang )}}</td>
                        <th class="p-1">{{trans('daily_report.end_date')}}</th>
                        <td class="p-1">{{ datecheck($project->project_end_date,$lang )}}</td>
                    </tr>              
            </table>
            <table width="100%" border="1">
                <tr class="text-center">
                    <th width="5%" class="text-center">#</th>
                    <th width="45%">{{trans('daily_report.activity')}}</th>
                    <th width="10%">{{trans('daily_report.location')}}</th>
                    <th width="10%">{{trans('daily_report.units')}}</th>
                    <th width="10%">{{trans('daily_report.total_done1')}}</th>
                    <th width="10%">{{trans('daily_report.remain_work')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                @foreach($daily_report as $dreport)
                    @if(count($dreport->activities)>0)
                        @foreach ($dreport->activities as $item)
                            <tr>
                                <td class="p-1">{{$loop->iteration}}</td>
                                <td class="p-1">{{$item->bill_quantity->operation_type }}</td>
                                <td class="p-1">{{$item->location}}</td>
                                <td class="p-1">{{$item->bill_quantity->unit->name_dr}}</td>
                                <td class="p-1">{{$item->amount}}</td>
                                <td class="p-1">{{$item->bill_quantity->amount-$item->amount}}</td>
                                <td class="p-1">{{datecheck($dreport->report_date,$lang)}}</td>
                            </tr>
                        @endforeach 
                    @endif 
                @endforeach    
            </table>
            <table width="100%" border="1">
                <tr>
                    <th colspan="5" class="text-center bg-secondary">{{trans('daily_report.list_equipment')}}</th>
                </tr>
                <tr class="text-center">
                    <th width="5%">#</th>
                    <th width="40%">{{trans('daily_report.equipment')}}</th>
                    <th width="10%">{{trans('daily_report.equipment_no')}}</th>
                    <th width="10%">{{trans('daily_report.work_hours')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                @foreach ($daily_report as $dreport)
                    @if(count($dreport->equipments)>0)                 
                        @foreach ($dreport->equipments as $item)
                            <tr>
                                <td class="p-1">{{$loop->iteration}}</td>
                                <td class="p-1">{{ $item->equipment->{'name_'.$lang} }}</td>
                                <td class="p-1">{{ $item->equipment_no }}</td>
                                <td class="p-1">{{ $item->work_hourse }}</td>
                                <td class="p-1">{{datecheck($dreport->report_date,$lang)}}</td>
                            </tr>
                        @endforeach
                    @endif    
                @endforeach
            </table>
            <table width="100%" border="1">
                <tr>
                    <th colspan="5" class="text-center bg-secondary">{{trans('daily_report.list_labor')}}</th>
                </tr>
                <tr class="text-center">
                    <th width="5%">#</th>
                    <th width="65%">{{trans('daily_report.labor_clasification')}}</th>
                    <th width="10%">{{trans('daily_report.labor_number')}}</th>
                    <th width="10%">{{trans('daily_report.work_hours')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>

                </tr>
                @foreach ($daily_report as $dreport)
                    @if(count($dreport->labour)>0)  
                        @foreach($dreport->labour AS $item)
                            <tr id="tr_{{$loop->iteration}}">
                                <td class="p-1" >{{ $loop->iteration }}</td>
                                <td class="p-1" >{{ $item->labor->{'name_'.$lang} }}</td>
                                <td class="p-1" >{{ $item->labor_number }}</td>
                                <td class="p-1" >{{ $item->work_hours }}</td>
                                <td class="p-1" >{{datecheck($dreport->report_date,$lang)}}</td>
                            </tr>
                        @endforeach
                    @endif      
                @endforeach  
            </table>
            <table width="100%" border="1">
                <tr>
                    <th colspan="7" class="text-center bg-secondary">{{trans('daily_report.qc_test')}}</th>
                </tr>
                <tr class="text-center">
                    <th width="5%">#</th>
                    <th width="30%">{{trans('daily_report.activity')}}</th>
                    <th width="10%">{{trans('daily_report.test_no')}}</th>
                    <th width="20%">{{trans('daily_report.test_type')}}</th>
                    <th width="10%">{{trans('daily_report.location')}}</th>
                    <th width="10%">{{trans('daily_report.test_result')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                @foreach ($daily_report as $dreport)
                    @if(count($dreport->test)>0) 
                        @foreach($dreport->test AS $item)
                            <tr>
                                <td class="p-1">{{$loop->iteration}}</td>
                                <td class="p-1">{{ $item->bill_quantity->operation_type }}</td>
                                <td class="p-1">{{ $item->test_number }}</td>
                                <td class="p-1">{{ $item->test_type }}</td>
                                <td class="p-1">{{ $item->test_location }}</td>
                                <td class="p-1">
                                    {{trans(Config::get('static.'.$lang.'.test_result.'.$item->test_status.'.name'))}}
                                </td>
                                <td class="p-1">{{datecheck($dreport->report_date,$lang)}}</td>
                            </tr>
                        @endforeach
                    @endif  
                @endforeach               
            </table>
            <table width="100%" border="1">
                <tr>
                    <th colspan="8" class="text-center bg-secondary">{{trans('daily_report.weather')}}</th>
                </tr>
                <tr class="text-center">
                    <th width="5%">#</th>
                    <th width="30%">{{trans('daily_report.weather')}}</th>
                    <th width="15%">{{trans('daily_report.high_temp')}}</th>
                    <th width="15%">{{trans('daily_report.low_temp')}}</th>
                    <th width="15%">{{trans('daily_report.weather_precipitation')}}</th>
                    <th width="10%">{{trans('daily_report.wind_speed')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                @foreach ($daily_report as $dreport)              
                    @if($dreport->weather)
                        <tr>
                            <td class="p-1">{{$loop->iteration}}</td>
                            <td class="p-1">{{$dreport->weather->static_data->{'name_'.$lang} }}</td>
                            <td class="p-1">{{$dreport->weather->high_temp }}</td>
                            <td class="p-1">{{$dreport->weather->low_temp }}</td>
                            <td class="p-1">{{$dreport->weather->weather_precipitation }}</td>
                            <td class="p-1">{{$dreport->weather->wind_speed }}</td>
                            <td class="p-1">{{datecheck($dreport->report_date,$lang)}}</td>
                        </tr>
                    @endif  
                @endforeach
            </table>
            <table width="100%" border="1">
                <tr>
                    <th colspan="6" class="text-center bg-secondary">{{trans('daily_report.weather')}}</th>
                </tr>
                <tr class="text-center">
                    <th width="5%">#</th>
                    <th width="40%">{{trans('daily_report.main_prob')}}</th>
                    <th width="40%">{{trans('daily_report.solution')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                <tr>
                @foreach ($daily_report as $dreport)              
                    @if($dreport->narrative)
                        <tr>
                            <td class="p-1">1</td>
                            <td class="p-1">{!! $dreport->narrative->main_problems !!}</td>
                            <td class="p-1">{!! $dreport->narrative->solutions !!}</td>
                            <td class="p-1">{{datecheck($dreport->report_date,$lang)}}</td>
                        </tr>
                    @endif
                @endforeach    
            </table>
        </div>
    </div>
</div>