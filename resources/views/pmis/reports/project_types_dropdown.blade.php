<option value="" selected>{{ trans('global.select') }}</option>
@if($project_types)
    @foreach($project_types as $item)
        <option value="{!!$item->id!!}">{{ $item->{'name_'.$lang} }}</option>
    @endforeach
@endif