<html>
    <head>
        <?php
        $lang = get_language();
        if($lang=="en"){
            $dir = "";
            $direction = "";

        }else {
            $dir = ".rtl"; 
            $direction = "rtl";
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('report.daily_report') }}</title>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('public/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public/css/custome.css')}}">      
    </head>
    <body dir="{{$direction}}">
                <div class="m-portlet  m-portlet--full-height  m-portlet--unair">
                    <div class="m-portlet__body px-4 pt-0">
                        <div class="row px-0">
                            <div class="col-lg-12 col-md-12 col-sm-12 px-0">
                                <table width="100%">
                                    <tr class="text-center">
                                        <td width="100%" class=" text-center"><img src="{!!asset('public/img/logo.png')!!}" style="height: 50%; width: 9%"/></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">جمهوری اسلامی افغانستان</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">اداره عملیاتی و حمایوی انکشاف ملی ریاست جمهوری .ا.ا</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">معاونیت تخنیکی</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">آمریت دفتر</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">آمریت پیگیری پروژه ها، اسناد و سیستم PMIS</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td width="100%" class=""><h3 class="m-portlet__head-text">گزارش پیشرفت کار پروژه ها</h3></td>
                                    </tr>
                                </table>
                                <br>
                                <table class="p-4" border="1" width="100%">
                                    <tr class="bg-light">
                                        <th width="3%" rowspan="2" class="text-center align-middle p-3">{{ trans('global.number') }}</th>
                                        <th width="10%" rowspan="2" class="text-center align-middle p-3">{{ trans('procurement.contract_code') }}</th>
                                        <th width="10%" rowspan="2" class="text-center align-middle p-3">{{ trans('plans.project_name') }}</th>
                                        <th width="10%" colspan="2" class="text-center align-middle p-3">{{ trans('plans.view_location') }}</th>
                                        <th width="10%" colspan="2" class="text-center align-middle p-3">{{ trans('progress.contraction_duration') }}</th>
                                        <th width="12%" rowspan="2" class="text-center align-middle">{{ trans('change_request.time_duration') }}</th>
                                        <th width="8%" colspan="2" class="text-center align-middle p-3">{{ trans('report.project_masool') }}</th>
                                        <th width="5.5%" rowspan="2" class="text-center align-middle p-3">{{ trans('procurement.contract_price') }}</th>
                                        <th width="4%" rowspan="2" class="text-center align-middle p-3">{{ trans('daily_report.work_progress') }}</th>
                                        <th width="5.5%" rowspan="2" class="text-center align-middle p-3">{{ trans('portfolios.paid_budget') }}</th>
                                        <th width="11%" rowspan="2" class="text-center align-middle p-3">{{ trans('daily_report.problems') }}</th>
                                        <th width="11%" rowspan="2" class="text-center align-middle p-3">{{ trans('daily_report.solution') }}</th>
                                    </tr>
                                    <tr class="bg-light">
                                        <th width="4%" class="text-center align-middle p-3">{{ trans('home.zone') }}</th>
                                        <th width="6%" class="text-center align-middle p-3">{{ trans('report.province') }}</th>
                                        <th width="6%" class="text-center align-middle p-3">{{ trans('procurement.start_date') }}</th>
                                        <th width="6%" class="text-center align-middle p-3">{{ trans('procurement.end_date') }}</th>
                                        <th width="5%" class="text-center align-middle p-3">{{ $department_name }}</th>
                                        <th width="5%" class="text-center align-middle p-3">{{ trans('report.contractor') }}</th>
                                    </tr>
                                    @if($records)
                                        @php
                                            $locations = $records->pluck('id')->toArray();
                                            // Get total price of project by all locations
                                            $getTotalPriceByLocations = getTotalPriceByLocations($locations);
                                            // Calcualte actual budget done from project start to end
                                            $getOneLocationBudgetDone = getOneLocationBudgetDone($locations);
                                            //ddd($getOneLocationBudgetDone);
                                        @endphp
                                        @foreach($records as $item)
                                            <tr>
                                                <td class="text-center p-3">{{ $loop->iteration }}</td>
                                                <td class="text-center p-3">{{ $item->projects->procurement->contract_code }}</td>
                                                <td class="p-3">{{ $item->projects->name }}</td>
                                                <td class="text-center p-3">{{ $item->province->{'zname_'.$lang} }}</td>
                                                <td class="p-3">{{ $item->province->{'name_'.$lang} }} @if($item->district_id)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village_id) / {{$item->village->{'name_'.$lang} }} @endif</td>
                                                <td class="text-center p-3">{{ dateCheck($item->projects->procurement->start_date,$lang) }}</td>
                                                <td class="text-center p-3">{{ dateCheck($item->projects->procurement->end_date,$lang) }}</td>
                                                <td class="text-center p-3" class="" style="border-top: 1px solid #1f1f2b6e !important;">
                                                    @if($item->change_request_time)
                                                        @foreach($item->change_request_time as $change_item)
                                                            {{ dateCheck($change_item->end_date,$lang) }}
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td class="text-center p-3">
                                                    @foreach($item->department_staff as $val)
                                                        {{ $val->users->name }}
                                                        @if(!$loop->last), &nbsp;@endif
                                                    @endforeach
                                                </td>
                                                <td class="text-center p-3">
                                                    @foreach($item->contractor_staff as $val)
                                                        {{ $val->contractor_users->name }}
                                                        @if(!$loop->last), &nbsp;@endif
                                                    @endforeach
                                                </td>
                                                <td class="text-center p-3">
                                                    {{ number_format($item->projects->procurement->contract_price) }}
                                                </td>
                                                <td class="text-center p-3">
                                                    <?php
                                                    $total_price    = 0;
                                                    $total_done     = 0;
                                                    $activity_done  = 0;
                                                    // Get total price of location
                                                    if(isset($getOneLocationBudgetDone[$item->id])){
                                                        $total_price = $getTotalPriceByLocations[$item->id];
                                                    }
                                                    // Get actual budget done from location start to end
                                                    if(isset($getOneLocationBudgetDone[$item->id])){
                                                        $total_done = $getOneLocationBudgetDone[$item->id];
                                                    }
                                                    // Set default value
                                                    $activity_done = 0;
                                                    if($total_price > 0){
                                                        $activity_done = getPercentage($total_price,$total_done,true);
                                                    }
                                                    echo $activity_done; 
                                                    ?>
                                                </td>
                                                <td class="text-center p-3">
                                                    <?php
                                                    $total_paid = getTotalPaidBudget($item->id);
                                                    // Set default value
                                                    $paid = 0;
                                                    if($total_paid > 0){
                                                        $paid = round($total_paid,3);
                                                    }
                                                    echo number_format($paid);
                                                    ?>
                                                </td>
                                                <td class="p-3">
                                                    <?php
                                                    // Get daily report main problems
                                                    foreach($item->daily_reports as $val)
                                                    {
                                                        if($val['narrative']['main_problems']!="")
                                                        {
                                                            echo $val['narrative']['main_problems'];
                                                            if(!$loop->last){ echo "<br>"; }
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td class="p-3">
                                                    <?php
                                                    // Get daily report main problems
                                                    foreach($item->daily_reports as $val)
                                                    {
                                                        if($val['narrative']['solutions']!="")
                                                        {
                                                            echo $val['narrative']['solutions'];
                                                            if(!$loop->last){ echo "<br>"; }
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        
    </body>
</html>