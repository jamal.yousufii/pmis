<div class="m-portlet mb-2">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{trans('daily_report.print_daily_report')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#dailyReportCollapse" role="button" aria-expanded="true" aria-controls="graphCollapse">
                        <span><i class="la la-arrows-v"></i><span>{{ trans('global.view') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="code notranslate cssHigh collapse show" id="dailyReportCollapse">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="search_form" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                        <div id="div_department" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" id="department" name="department" onchange="viewRecord('{{ route('bringProjectsByDepartments') }}','id='+this.value,'POST','div_project_id')">
                                @if($departments)
                                    <option value="" selected>{{ trans('global.select') }}</option>
                                    @foreach($departments as $item)
                                        <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="department error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{trans('global.projects')}}: <span style="color:red;">*</span></label>
                        <div id="div_project_id" class="errorDiv">
                            <select name="project_id" onchange="viewRecord('{{route('project_location.option')}}','id='+this.value,'POST','project_location_id')" class="form-control m-input required select-2" id="project_id">
                                <option value="">{{trans('global.select')}}</option>
                            </select>
                        </div>
                        <div class="project_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{trans('daily_report.project_location')}} : <span style="color:red;">*</span></label>
                        <div id="div_project_location_id" class="errorDiv">
                            <select name="project_location_id" class="form-control m-input select-2 required" id="project_location_id">
                                <option value="">{{trans('global.select')}}</option>
                            </select>
                        </div>
                        <div class="project_location_id error-div" style="display:none;"></div>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                        <label class="title-custom">{{trans('global.from_date')}} : <span style="color:red;">*</span></label>
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input datePicker required" value="" name="from_report_date" id="from_report_date"  style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                            <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                        </div>
                        <div class="from_report_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{trans('global.to_date')}} : <span style="color:red;">*</span></label>
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input datePicker required" value="" name="to_report_date" id="to_report_date"  style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                            <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                        </div>
                        <div class="to_report_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">&nbsp;</label><br>
                        <a href="#" class="btn btn-primary m-btn m-btn--icon btn-sm" onclick="storeData('{{route('report.daily_report')}}','search_form','POST','search_result',put_content,true,'dailyReportCollapse');">
                            <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                        </a>
                    </div>
                </div>                
            </div>
        </form>
        <!--end::Form-->
        <hr>
    </div>
</div>
<div class="pt-0" id="search_result"></div>
@section('js-code')
  <script type="text/javascript">
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
    $('.select-2').select2();
  </script>
@endsection