<option value="" selected>{{ trans('global.select') }}</option>
@if($record)
    @foreach($record as $item)
        <option value="{!!$item->code!!}">{{ $item->section }}</option>
    @endforeach
@endif