<select name="project_id" onchange="viewRecord('{{route('project_location.option')}}','id='+this.value,'POST','project_location_id')" class="form-control m-input required select-2" id="project_id">
    <option value="">{{trans('global.select')}}</option>
    @if($projects)
        @foreach($projects as $item)
            <option value="{!! $item->id !!}">{{ $item->name }}</option>
        @endforeach
    @endif
</select>
<script type="text/javascript">
  $(".select-2").select2();
</script>