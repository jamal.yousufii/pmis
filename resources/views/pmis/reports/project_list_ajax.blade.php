@if($share_to!='pmis_plan')
    <table class="table table-bordered mt-2" width="100%">
        <thead>
            <tr>
                <th width="15%">{{ trans('global.urn') }}</th>
                <th width="60%">{{ trans('plans.name') }}</th>
                <th width="25%">{{ trans('plans.project_type') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($records)
                @foreach($records as $item)
                    <tr>
                        <td>{{ $item->urn }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->type }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@else
    <table class="table table-bordered mt-2" width="100%">
        <thead>
            <tr>
                <th width="20%">{{ trans('global.req_urn') }}</th>
                <th width="40%">{{ trans('requests.department') }}</th>
                <th width="20%">{{ trans('requests.req_date') }}</th>
                <th width="20%">{{ trans('requests.req_number') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($records)
                @foreach($records as $item)
                    <tr>
                        <td>{{ $item->urn }}</td>
                        <td>{{ $item->department }}</td>
                        <td>{{ dateCheck($item->request_date,$lang) }}</td>
                        <td>{{ $item->doc_number }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
  </table>
@endif
<!-- Pagination -->
@if(!empty($records))
    {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $(".m-page-loader.m-page-loader--base").css("display","block");
                },
                success: function(response)
                {
                  $(".m-page-loader.m-page-loader--base").css("display","none");
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>