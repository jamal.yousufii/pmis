<option value="" selected>{{ trans('global.select') }}</option>
@if($project_categories)
    @foreach($project_categories as $item)
        <option value="{!!$item->id!!}">{{ $item->{'name_'.$lang} }}</option>
    @endforeach
@endif