<div class="m-portlet  m-portlet--full-height  m-portlet--unair">
    <div class="m-portlet__body">
        @if($records->count()>0)
            <!-- Generate PDF Reprot-->
            <form action="{{route('project_progressive_report_pdf',['dep_id'=>encrypt($department_id)]) }}" method="GET" enctype="multipart/form-data">
                <input type="hidden" name="department_id" value="{{$department_id}}">
                <input type="hidden" name="category_id" value="{{$category_id}}">
                <input type="hidden" name="project_type_id" value="{{$project_type_id}}">
                <input type="hidden" name="projects_id" value="{{$projects_id}}">
                <input type="hidden" name="status" value="{{$status}}">
                <input type="hidden" name="from_date" value="{{$from_date}}">
                <input type="hidden" name="to_date" value="{{$to_date}}">
                <button type="submit" class="btn btn-info m-btn--custom m-btn--icon btn-sm mb-2" >
                    <span><i class="la la-print"></i> <span>{{ trans('global.report_pdf') }}</span></span>
                </button>
            </form>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <table class="table" border="1" width="100%">
                    <thead>
                        <tr class="bg-dark text-white border-white">
                            <th width="3%" rowspan="2" class="text-center align-middle">{{ trans('global.number') }}</th>
                            <th width="10%" rowspan="2" class="text-center align-middle">{{ trans('procurement.contract_code') }}</th>
                            <th width="10%" rowspan="2" class="text-center align-middle">{{ trans('plans.project_name') }}</th>
                            <th width="10%" colspan="2" class="text-center align-middle">{{ trans('plans.view_location') }}</th>
                            <th width="10%" colspan="2" class="text-center align-middle">{{ trans('progress.contraction_duration') }}</th>
                            <th width="12%" rowspan="2" class="text-center align-middle">{{ trans('change_request.time_duration') }}</th>
                            <th width="8%" colspan="2" class="text-center align-middle">{{ trans('report.project_masool') }}</th>
                            <th width="5.5%" rowspan="2" class="text-center align-middle">{{ trans('procurement.contract_price') }}</th>
                            <th width="4%" rowspan="2" class="text-center align-middle">{{ trans('daily_report.work_progress') }}</th>
                            <th width="5.5%" rowspan="2" class="text-center align-middle">{{ trans('portfolios.paid_budget') }}</th>
                            <th width="11%" rowspan="2" class="text-center align-middle">{{ trans('daily_report.problems') }}</th>
                            <th width="11%" rowspan="2" class="text-center align-middle">{{ trans('daily_report.solution') }}</th>
                        </tr>
                        <tr class="bg-dark text-white border-white">
                            <th width="4%" class="text-center align-middle">{{ trans('home.zone') }}</th>
                            <th width="6%" class="text-center align-middle">{{ trans('report.province') }}</th>
                            <th width="6%" class="text-center align-middle">{{ trans('procurement.start_date') }}</th>
                            <th width="6%" class="text-center align-middle">{{ trans('procurement.end_date') }}</th>
                            <th width="5%" class="text-center align-middle">{{ $department_name }}</th>
                            <th width="5%" class="text-center align-middle">{{ trans('report.contractor') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($records)
                            @php
                                $locations = $records->pluck('id')->toArray();
                                // Get total price of project by all locations
                                $getTotalPriceByLocations = getTotalPriceByLocations($locations);
                                // Calcualte actual budget done from project start to end
                                $getOneLocationBudgetDone = getOneLocationBudgetDone($locations);
                                //ddd($getOneLocationBudgetDone);
                            @endphp
                            @foreach($records as $item)
                                <tr>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">{{ $loop->iteration }}</td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">{{ $item->projects->procurement->contract_code }}</td>
                                    <td style="border-top: 1px solid #1f1f2b6e !important;">{{ $item->projects->name }}</td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">{{ $item->province->{'zname_'.$lang} }}</td>
                                    <td style="border-top: 1px solid #1f1f2b6e !important;">{{ $item->province->{'name_'.$lang} }} @if($item->district_id)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village_id) / {{$item->village->{'name_'.$lang} }} @endif</td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">{{ dateCheck($item->projects->procurement->start_date,$lang) }}</td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">{{ dateCheck($item->projects->procurement->end_date,$lang) }}</td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        @if($item->change_request_time)
                                            @foreach($item->change_request_time as $change_item)
                                                {{ dateCheck($change_item->end_date,$lang) }}
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        @foreach($item->department_staff as $val)
                                            {{ $val->users->name }}
                                            @if(!$loop->last), &nbsp;@endif
                                        @endforeach
                                    </td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        @foreach($item->contractor_staff as $val)
                                            {{ $val->contractor_users->name }}
                                            @if(!$loop->last), &nbsp;@endif
                                        @endforeach
                                    </td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        {{ number_format($item->projects->procurement->contract_price) }}
                                    </td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        <?php
                                        $total_price    = 0;
                                        $total_done     = 0;
                                        $activity_done  = 0;
                                        // Get total price of location
                                        if(isset($getOneLocationBudgetDone[$item->id])){
                                            $total_price = $getTotalPriceByLocations[$item->id];
                                        }
                                        // Get actual budget done from location start to end
                                        if(isset($getOneLocationBudgetDone[$item->id])){
                                            $total_done = $getOneLocationBudgetDone[$item->id];
                                        }
                                        // Set default value
                                        $activity_done = 0;
                                        if($total_price > 0){
                                            $activity_done = getPercentage($total_price,$total_done,true);
                                        }
                                        echo $activity_done; 
                                        ?>
                                    </td>
                                    <td class="text-center" style="border-top: 1px solid #1f1f2b6e !important;">
                                        <?php
                                        $total_paid = getTotalPaidBudget($item->id);
                                        // Set default value
                                        $paid = 0;
                                        if($total_paid > 0){
                                            $paid = round($total_paid,3);
                                        }
                                        echo number_format($paid);
                                        ?>
                                    </td>
                                    <td style="border-top: 1px solid #1f1f2b6e !important;">
                                        <?php
                                        // Get daily report main problems
                                        foreach($item->daily_reports as $val)
                                        {
                                            if($val['narrative']['main_problems']!="")
                                            {
                                                echo strip_tags($val['narrative']['main_problems']);
                                                if(!$loop->last){ echo "<br>"; }
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td style="border-top: 1px solid #1f1f2b6e !important;">
                                        <?php
                                        // Get daily report main problems
                                        foreach($item->daily_reports as $val)
                                        {
                                            if($val['narrative']['solutions']!="")
                                            {
                                                echo strip_tags($val['narrative']['solutions']);
                                                if(!$loop->last){ echo "<br>"; }
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>