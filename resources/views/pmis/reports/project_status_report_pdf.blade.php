<html>
    <head>
        <?php
        $lang = get_language();
        if($lang=="en"){
            $dir = "";
            $direction = "";

        }else {
            $dir = ".rtl"; 
            $direction = "rtl";
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('global.project_status_report') }} {{ trans('global.in') }} {{ $department_name }}</title>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('public/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public/css/custome.css')}}">      
    </head>
    <body dir="{{$direction}}">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet  m-portlet--full-height  m-portlet--unair">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <table width="100%">
                                    <tr class="text-center">
                                        <td width="100%" class="text-center"><img src="{!!asset('public/img/logo.png')!!}" style="height: 115px; width: 115px"/></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td><h3 class="m-portlet__head-text">{{ trans('home.title-1') }}</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td><h3 class="m-portlet__head-text">{{ trans('home.title-2') }}</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td><h3 class="m-portlet__head-text">{{ trans('home.title-3') }}</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td><h3 class="m-portlet__head-text">{{ $department_name }}</h3></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td><h3 class="m-portlet__head-text">{{ trans('report.project_status_report') }}</h3></td>
                                    </tr>
                                </table>
                                <br>
                                <table border="1" width="100%">
                                    <tr>
                                        <td width="10%" class="p-3 pdf-heading">{{ trans('global.number') }}</td>
                                        <td width="15%" class="p-3 pdf-heading">{{ trans('global.section') }}</td>
                                        <td width="30%" class="p-3 pdf-heading">{{ trans('global.departments') }}</td>
                                        <td width="10%" class="p-3 pdf-heading">{{ trans('report.pending_projects') }}</td>
                                        <td width="10%" class="p-3 pdf-heading">{{ trans('report.progress_projects') }}</td>
                                        <td width="10%" class="p-3 pdf-heading">{{ trans('report.completed_projects') }}</td>
                                        <td width="15%" class="p-3 pdf-heading">{{ trans('report.total_projects') }}</td>
                                    </tr>
                                    @if($records)
                                        @foreach($records as $key=>$val)
                                            @php
                                                $key = explode('-',$key);
                                                $total = 0;
                                                if(isset($val[2])) { $total = $total+$val[2]; } 
                                                if(isset($val[0])) { $total = $total+$val[0]; } 
                                                if(isset($val[1])) { $total = $total+$val[1]; } 
                                            @endphp
                                            <tr>
                                                <td class="p-3">{{ $loop->iteration }}</td>
                                                <td class="p-3">{{ $key[1] }}</td>
                                                <td class="p-3">{{ $key[0] }}</td>
                                                <td class="p-3">@if(isset($val[2])) {{ $val[2] }} @else {{0}} @endif</td>
                                                <td class="p-3">@if(isset($val[0])) {{ $val[0] }} @else {{0}} @endif</td>
                                                <td class="p-3">@if(isset($val[1])) {{ $val[1] }} @else {{0}} @endif</td>
                                                <td class="p-3">{{ $total }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>
