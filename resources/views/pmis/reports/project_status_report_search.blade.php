<div class="m-portlet mb-2">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{trans('global.project_status_report')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#statusCollapse" role="button" aria-expanded="true" aria-controls="graphCollapse">
                        <span><i class="la la-arrows-v"></i><span>{{ trans('global.view') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="code notranslate cssHigh collapse show" id="statusCollapse">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row" style="border-bottom: 1px dashed #ebedf2;">
                <div class="col-lg-12">
                    <div class="row pt-2 px-3">
                        <label class="title-custom"> {{ trans('report.numeric') }} / {{ trans('report.detailed') }}</label>&nbsp;&nbsp;
                        <span class=" m-switch m-switch--icon">
                            <label><input type="checkbox" class="form-control m-input" name="request" id="request" onclick="holidaysContent(this.id,'byNumeric','byDetailed')"><span></span></label>
                        </span>
                    </div>
                </div>
            </div>
            <!--begin::Form Numeric-->
            <form class="col-lg-12 p-0 pt-3" id="search_numeric" enctype="multipart/form-data">
                <div class="form-group m-form__group row" id="byNumeric">
                    <div class="col-lg-10">
                        <label class="title-custom">{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                        <div id="div_department" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" id="department" name="department" style="width: 100%;">
                                @if($departments)
                                    <option value="" selected>{{ trans('global.select') }}</option>
                                    @foreach($departments as $item)
                                        @if($item->id == departmentid())
                                            <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                        @elseif(departmentid() == 1)
                                            <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="department error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-2">
                        <label class="title-custom">&nbsp;</label><br>
                        <a href="#" class="btn btn-primary m-btn m-btn--icon btn-sm" onclick="storeData('{{route('project_status_report')}}','search_numeric','POST','search_result',put_content,true,'statusCollapse');">
                            <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                        </a>
                    </div>
                </div>
            </form>
            <!--end::Form Detailed-->
            <!--begin::Form Detailed-->
            <form class="col-lg-12 p-0" id="search_detailed" enctype="multipart/form-data">
                <div class="form-group m-form__group row d-none" id="byDetailed">
                    <div class="col-lg-5">
                        <label class="title-custom">{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                        <div id="div_dep_id class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" id="dep_id" name="dep_id" style="width: 100%;" onchange="viewRecord('{{ route('report.bringSectionsByDepartment') }}','id='+this.value,'POST','section_code')">
                                <option value="" selected>{{ trans('global.select') }}</option>
                                @if($departments)
                                    @foreach($departments as $item)
                                        @if($item->id == departmentid())
                                            <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                        @elseif(departmentid() == 1)
                                            <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="dep_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-5">
                        <label class="title-custom">{{ trans('global.section') }}: <span style="color:red;">*</span></label>
                        <div id="div_department" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" id="section_code" name="section_code" style="width: 100%;">
                                <option value="" selected>{{ trans('global.select') }}</option>
                            </select>
                        </div>
                        <div class="section_code error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-2">
                        <label class="title-custom">&nbsp;</label><br>
                        <a href="#" class="btn btn-primary m-btn m-btn--icon btn-sm" onclick="storeData('{{route('project_status_report_detailed')}}','search_detailed','POST','search_result',put_content,true,'statusCollapse');">
                            <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                        </a>
                    </div>
                </div>
            </form>
            <!--end::Form Numeric-->
        </div>
    </div>
</div>
<div id="search_result" class="pt-0"></div>
