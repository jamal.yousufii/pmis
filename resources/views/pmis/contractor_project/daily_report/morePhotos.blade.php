<span id="targetDiv_{!!$number!!}" class="form-group m-form__group row">
    <div class="col-lg-1">
        {!!$number!!}.
    </div>
    <div class="col-lg-9">
        <div class="custom-file errorDiv">
            <input type="file" class="custom-file-input required" name="file[]" id="file_{!!$number!!}" onchange="chooseFile(this.id)" required>
            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
        </div>
        <div class="file_{!!$number!!} error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-2">
        <button type="button" id="btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-xs btn-sm" onclick="remove_moreAttachments('targetDiv_{!!$number!!}','{{$number}}')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</span>