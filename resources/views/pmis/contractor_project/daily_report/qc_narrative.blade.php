<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fl flaticon-list-1"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.qc_narrative')}}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and !$narrative and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsNarrativeBtn" data-toggle="collapse" href="#collapseNarrativeDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                    </a>
                </li>
            @elseif(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $narrative and $daily_report and $approval!=1)
               <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsNarrativeBtnEdit" data-toggle="collapse" href="#collapseNarrativeDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="fl flaticon-edit-1"></i> <span>{{ trans('global.edit') }}</span></span>
                    </a>
                </li>    
            @endif
        </ul>
    </div>
</div>
@if($daily_report)
    <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseNarrativeDiv">
        <!-- Add Report -->
        <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="qc_narrative">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-4">
                            <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="report_date" style="display:none;"></div>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                            <div id="div_location_id" class="errorDiv">
                                <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                    @if(!empty($location)) 
                                        <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                    @else
                                        <option value="">{{trans('global.select')}}</option>
                                    @endif
                                </select>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="location_id error-div" style="display:none;"></div>
                        </div>
                        <div class="col-lg-2">
                            <label class="title-custom">{{trans('daily_report.is_worked_stoped')}}</label><br>
                            <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                <label><input type="checkbox" name="is_worked_stoped" {{($narrative? ($narrative->is_worked_stoped==1 ? 'checked': '') : '')}}><span></span></label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.main_prob') }} : <span style="color:red;">*</span></label>
                            <textarea name="main_problems" class="form-control tinymce m-input errorDiv required" id="main_problems" cols="30" rows="10">{!!($narrative? $narrative->main_problems : '') !!}</textarea>
                            <div class="main_problems error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.solution') }} : <span style="color:red;">*</span></label>
                            <textarea name="solutions" class="form-control tinymce m-input errorDiv required" id="solutions" cols="30" rows="10">{!!($narrative? $narrative->solutions : '') !!}</textarea>
                            <div class="solutions error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            @if($narrative) <!-- Update the record --> 
                                <input type="hidden" name="id" value="{{encrypt($narrative->id)}}">
                                <button type="button" onclick="storeData('{{route('qcnarrative.update')}}','qc_narrative','POST','daily_narrative_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
                            @else  <!-- Insert the record -->
                                <button type="button" onclick="storeData('{{route('qcnarrative.store')}}','qc_narrative','POST','daily_narrative_content',put_content)" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            @endif
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!-- Display Report -->
    <div class="m-portlet__body" id="daily_narrative_content">
        @if($narrative)
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.is_worked_stoped') }} : </label><br>
                            <span>{{ ($narrative->is_worked_stoped==1? trans('global.yes') : trans('global.no')) }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.main_prob') }} : </label><br>
                            <span>{!! $narrative->main_problems !!} </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.solution') }} : </label><br>
                            <span>{!! $narrative->solutions !!}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@else
    <!--begin::Alert div -->
    <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
        <div class="m-alert__icon">
            <i class="la la-warning"></i>
        </div>
        <div class="m-alert__text">
            <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
        </div>
        <div class="m-alert__close"></div>
    </div>  
    <!--End::Alert div -->              
@endif