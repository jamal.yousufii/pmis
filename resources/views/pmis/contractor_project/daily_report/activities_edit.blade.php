<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="activitiesFormEdit" enctype="multipart/form-data">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input errorDiv datePicker required" name="daily_report" id="report_date" value="{{ ($activity ? dateCheck($activity->dailyReport->report_date,$lang): '') }}" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="report_date" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                <div id="div_location_id" class="errorDiv">
                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                        @if(!empty($location)) 
                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                        @else
                            <option value="">{{trans('global.select')}}</option>
                        @endif
                    </select>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="location_id error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.activity') }} : <span style="color:red;">*</span></label>
                <div id="div_bq_id" class="errorDiv">
                    <select class="form-control m-input select-2 errorDiv" name="bq_id" style="width: 100%" onchange="bringUnit(this.value,'unit_div'); bringAmount(this.value,'amount_div');">
                        <option value="">{{trans('global.select')}}</option>
                        @if($bq)
                            @foreach($bq as $item)
                                <option <?=$item->id==$activity->bq_id? 'selected': ''?> value="{!!$item->id!!}">{!!$item->operation_type!!}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="bq_id error-div" style="display:none;"></div>
            </div>
            <!-- <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.location') }} : </label>
                <input type="text" value="{{$activity->location}}" class="form-control m-input" id="location" name="location">
            </div> -->
            <div class="col-lg-6">
                <div id="unit_div">
                    <label class="title-custom">{{ trans('daily_report.units') }} : <span style="color:red;">*</span></label>
                    <div id="unit_id" class="errorDiv">
                        <select class="form-control m-input select-2 errorDiv" name="unit_id" style="width: 100%">
                            @if($activity->unit)
                                <option value="{{ $activity->unit->id }}" selected>{{ $activity->unit->{'name_'.$lang} }}</option>
                            @endif
                        </select>
                    </div>
                    <div class="unit_id error-div" style="display:none;"></div>
                </div>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-12">
                <div id="amount_div">
                    <label class="title-custom">{{ trans('daily_report.amount') }} : </label>
                    <div class="slidecontainer">
                        <!-- <input type="range" min="0" step="0.1" max="{{ getColumn('pmis','estimations_bill_quantities',['id' => $activity->bq_id],'amount') - $activity->amount }}" value="{{$activity->amount}}" class="slider" id="myRange">
                        <input type="hidden" name="amount" id="amount" value="{{$activity->amount}}"> -->

                        <input type="number" name="amount" id="amount" value="{{$activity->amount}}" min="0" step="0.01" max="{{ getColumn('pmis','estimations_bill_quantities',['id' => $activity->bq_id],'amount') - $activity->amount }}" class="form-control m-input errorDiv required">
                        <div class="amount" style="display:none;"></div>

                        <div class="m-portlet mt-3 bg-dark text-white row">
                            <div class="col-lg-4">
                                <label class="title-custom p-3">{{ trans('daily_report.amount_done') }} : <span id="range-value"></span></label>
                            </div>
                            <div class="col-lg-4">
                                <label class="title-custom p-3">{{ trans('daily_report.total_done') }} : <span>{{ $activity_amount - $activity->amount }}</span></label>
                            </div>
                            <div class="col-lg-4">
                                <label class="title-custom p-3">{{ trans('daily_report.total_amount') }} : {{getColumn('pmis','estimations_bill_quantities',['id' => $activity->bq_id],'amount')}}<span></span></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom bg-light">
            <div class="col-lg-12">
                <input type="hidden" name="id" value="{{encrypt($activity->id)}}">
                <button type="button" onclick="storeData('{{route('daily_activities.update')}}','activitiesFormEdit','POST','daily_activities_list',put_content);" class="btn btn-primary">{{ trans('global.edit') }}</button>
                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </div>
</form>
<!--end::Form-->
<script>
    // var slider = document.getElementById("myRange");
    // var output = document.getElementById("range-value");
    // var target = document.getElementById("amount");
    // output.innerHTML = slider.value; // Display the default slider value

    // // Update the current slider value (each time you drag the slider handle)
    // slider.oninput = function() {
    //     output.innerHTML = this.value;
    //     target.value = this.value;
    // }
    $('#collapsActivityBtn').css('display','none');
</script>
