@extends('master')
@section('head')
  <title>{{ trans('daily_report.daily_report') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{ trans('daily_report.list_daily_report') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        @include('pmis.contractor_project.daily_report.create')
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
                </a>
            </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <div class="m-portlet__body table-responsive" id="searchresult"> 
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr>
            <th width="15%">{{ trans('daily_report.report_urn') }}</th>
            <th width="10%">{{ trans('daily_report.reportDate') }}</th>
            <th width="25%">{{ trans('daily_report.project_location') }}</th>
            <th width="15%">{{ trans('global.owner') }}</th>
            <th width="15%">{{ trans('daily_report.status') }}</th>
            <th width="10%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
            @if($records)
              <?php $enc_id = encrypt($records->id); ?>
              <tr>
                <td>{{ $records->urn }}</td>
                <td>{{ dateCheck($records->report_date,$lang) }}</td>
                <td>{{ $records->location->province->{'name_'.$lang} }} @if($records->location->district)/ {{$records->location->district->{'name_'.$lang} }} @endif @if($records->location->village)/ {{$records->location->village->{'name_'.$lang} }} @endif @if($records->location->latitude)/ {{ $records->location->latitude }} @endif @if($records->location->longitude)/ {{ $records->location->longitude }} @endif</td>
                <td>{{ $records->users['name'] }}</td>
                <td>
                  @if($records->status==0)
                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('daily_report.pending') }}</span>
                  @elseif($records->status==1)
                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('daily_report.approved') }}</span>
                  @elseif($records->status==2)
                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('daily_report.rejected') }}</span>
                  @endif
                </td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_view"))
                          <a href="javascript:void()" onclick="viewRecord('{{route('location.index')}}','report_id={{$records->id}}&&project_id={{$records->project_id}}','POST','m_portlet_tools_2')" data-toggle="modal" data-target="#daily_report_create" class="dropdown-item"><i class="la la-edit"></i>{{ trans('global.view_details') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection
