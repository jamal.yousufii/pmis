<div class="row">
    <div class="col-lg-12">
        <img src="{{ asset('public/'.$record->path) }}" class="img img-thumbnail" style="width:100%; height:350px;">
    </div>
    <div class="col-lg-10 offset-lg-1">
        <a href="{{ route('DownloadAttachments', array($enc_id,'daily_photos')) }}" class="btn btn-sm btn-success btn-block mt-2 text-decoration-none">
            <span><i class="fa fa-download"></i> <span>{{ trans('global.download') }}</span></span>
        </a>
    </div>
</div>