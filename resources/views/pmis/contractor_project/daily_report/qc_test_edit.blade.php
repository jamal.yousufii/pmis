<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="testFormEdit" enctype="multipart/form-data">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="{{($daily_test ? dateCheck($daily_test->dailyReport->report_date,$lang): '')}}" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="report_date" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                <div id="div_location_id" class="errorDiv">
                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                        @if(!empty($location)) 
                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                        @else
                            <option value="">{{trans('global.select')}}</option>
                        @endif
                    </select>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="location_id error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-12">
                <label class="title-custom">{{ trans('daily_report.activity') }} : <span style="color:red;">*</span></label>
                <div id="bill_quant_id" class="errorDiv">
                    <select class="form-control select-2 m-input errorDiv" name="bill_quant_id" style="width: 100%">
                        <option value="">{{trans('global.select')}}</option>
                        @foreach ($bill_quantity as $item)
                            <option {{($item->id==$daily_test->bill_quant_id ? 'selected': '')}} value="{{ $item->id }}">{{ $item->operation_type }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="bill_quant_id error-div" style="display:none;"></div>
            </div>
        </div> 
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label class="title-custom">{{trans('daily_report.test_no')}} : <span style="color:red;">*</span></label>
                <input type="text" class="form-control m-input errorDiv" value="{{$daily_test->test_number}}" name="test_number" id="test_number" />
                <div class="test_number error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-8">
                <label class="title-custom">{{trans('daily_report.test_type')}} :</label>
                <input class="form-control m-input" name="test_type" value="{{$daily_test->test_type}}" id="test_type" />
            </div>
        </div> 
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{trans('global.location')}} :</label>
                <input type="text" class="form-control m-input" value="{{$daily_test->test_location}}" name="test_location" id="test_location" />
            </div>
            <div class="col-lg-2 pt-2">
                <label class="title-custom">{{trans('daily_report.completed')}} :</label><br>
                <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                    <label><input type="checkbox" {{($daily_test->is_completed=='completed' ? 'checked' : '')}} name="is_completed"><span></span></label>
                </span>
            </div>
            <div class="col-lg-4">
                <label class="title-custom">{{trans('daily_report.test_result')}} : <span style="color:red;">*</span></label>
                <div id="test_status" class="errorDiv">
                    <select class="form-control select-2 m-input" name="test_status" style="width: 100%">
                        <option value="">{{trans('global.select')}}</option>
                        @foreach (Config::get('static.'.$lang.'.test_result') as $item)
                            <option {{($item['value']==$daily_test->test_status? 'selected': '')}} value="{{$item['value']}}">{{trans($item['name'])}}</option> 
                        @endforeach
                    </select>   
                </div>
                <div class="test_status error-div" style="display:none;"></div>
            </div>
        </div> 
        <div class="form-group m-form__group row">
            <div class="col-lg-12">
                <label class="title-custom">{{trans('global.description')}} :</label>
                <textarea name="description" class="form-control tinymce m-input" id="description">{!! $daily_test->description !!}</textarea>
            </div>
        </div> 
        <input type="hidden" name="id" value="{{encrypt($daily_test->id)}}">
        <div class="form-group m-form__group row m-form__group_custom bg-light">
            <div class="col-lg-12">
                <input type="hidden" name="report_id" value="{{ session('report_id') }}">
                <button type="button" onclick="storeData('{{route('qcTest.update')}}','testFormEdit','POST','daily_test_content',put_content);" class="btn btn-primary">{{ trans('global.edit') }}</button>
                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </div>
</form>
<!--end::Form-->
<script type="text/javascript">
    $('#collapsTestBtn').css('display','none');
</script>