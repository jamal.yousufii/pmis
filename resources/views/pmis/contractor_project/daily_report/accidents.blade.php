<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fa fa-ambulance"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.accidents')}}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and !$accidents and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsAccidentsBtn" data-toggle="collapse" href="#collapseAccidentsDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                    </a>
                </li>
            @elseif(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $accidents and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsAccidentsBtnEdit" data-toggle="collapse" href="#collapseAccidentsDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="fl flaticon-edit-1"></i> <span>{{ trans('global.edit') }}</span></span>
                    </a>
                </li>   
            @endif
        </ul>
    </div>
</div>
@if($daily_report)
    <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseAccidentsDiv">
        <!-- Add Report -->
        <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="accidentForm" enctype="multipart/form-data">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input errorDiv datePicker required" name="daily_report" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="report_date" style="display:none;"></div>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                            <div id="div_location_id" class="errorDiv">
                                <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                    @if(!empty($location)) 
                                        <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                    @else
                                        <option value="">{{trans('global.select')}}</option>
                                    @endif
                                </select>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="location_id error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.accident_date') }} : <span style="color:red;">*</span></label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input datePicker required" name="accident_date" id="accident_date" value="{!!($accidents ? dateCheck($accidents->accident_date,$lang):'')!!}" style="width: 100%" placeholder="{{ trans('daily_report.accident_date') }}" id="m_datepicker_3_modal">
                                <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                            </div>
                            <div class="accident_date error-div" style="display:none;"></div>    
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.personal_injury') }} : <span style="color:red;">*</span></label>
                            <div id="div_personal_injury" class="errorDiv">
                                <select class="form-control m-input select-2 required" name="personal_injury" id="personal_injury" style="width: 100%">
                                    <option value="">{{trans('global.select')}}</option>
                                    <option value="1" {!! ($accidents? ($accidents->personal_injury==1? 'selected' : ''):'') !!} >{{trans('global.yes')}}</option>
                                    <option value="0" {!! ($accidents? ($accidents->personal_injury==0? 'selected' : ''):'') !!} >{{trans('global.no')}}</option>
                                </select>
                            </div>
                            <div class="personal_injury error-div" style="display:none;"></div>    
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.description') }} : <span style="color:red;">*</span></label>
                            <textarea name="description" class="form-control tinymce m-input errorDiv required" id="description" cols="30" rows="10">{!!($accidents ? $accidents->description : '') !!}</textarea>
                            <div class="description error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom bg-light">
                        <div class="col-lg-12">
                            @if($accidents) <!-- Update if daily report exist -->  
                                <input type="hidden" name="id" value="{{encrypt($accidents->id)}}">
                                <button type="button" onclick="storeData('{{route('accidents.update')}}','accidentForm','POST','daily_accident_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
                            @else  <!-- Otherwise insert the data -->
                                <input type="hidden" name="daily_report_id" value="{{ session('report_id') }}">
                                <button type="button" onclick="storeData('{{route('accidents.store')}}','accidentForm','POST','daily_accident_content',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            @endif
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>  
    </div>
    <div class="m-portlet__body" id="daily_accident_content">
        @if($accidents)
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.accident_date') }} : </label><br>
                            <span>{!! $accidents->accident_date !!}</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.personal_injury') }} : </label><br>
                            <span>{!! ($accidents->personal_injury==1? trans('global.yes') : trans('global.no')) !!}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.description') }} : </label><br>
                            <span>{!! $accidents->description !!}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@else
    <!--begin::Alert div -->
    <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
        <div class="m-alert__icon">
            <i class="la la-warning"></i>
        </div>
        <div class="m-alert__text">
            <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
        </div>
        <div class="m-alert__close"></div>
    </div>  
    <!--End::Alert div -->              
@endif
<script>
    $('.select-2').select2();
</script>
