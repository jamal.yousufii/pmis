<div id="main_content">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="fl flaticon-photo-camera"></i></span>
                <h3 class="m-portlet__head-text">{{trans('daily_report.photos')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and $daily_report and $approval!=1)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsPhotosBtn" data-toggle="collapse" href="#collapsePhotosDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($daily_report)
        <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapsePhotosDiv">
            <!-- Add Report -->
            <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="photosForm" enctype="multipart/form-data">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-1">1.</div>
                            <div class="col-lg-9">
                                <div class="custom-file errorDiv">
                                    <input type="file" class="custom-file-input required" name="file[]" id="file" onchange="chooseFile(this.id)" required>
                                    <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                                </div>
                                <div class="file error-div" style="display:none;"></div><br>
                                <span class="m-form__help small">{{ trans('global.photo_extention') }}</span>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" onclick="add_moreAttachments('{{ route("bringMorePhotos") }}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                            </div>
                        </div>
                        <span id="targetDiv"></span>
                        <div class="form-group m-form__group row m-form__group_custom bg-light">
                            <div class="col-lg-12">
                                <input type="hidden" name="report_id" value="{{ session('report_id') }}">
                                <button type="button" onclick="storeData('{{route('photos.store')}}','photosForm','POST','daily_photos_list',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>  
        </div>
        <!-- Display Report -->
        <div id="daily_photos_list">
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        @if($photos)
                            @foreach($photos as $item)
                                <div class="col-lg-2"><a href="javascript:void()" onclick="setContentToDiv('{{route('photos.view', encrypt($item->id))}}','GET','modal_content')" data-toggle="modal" data-target="#viewPhotos"><img src="{{ asset('public/'.$item->path) }}" class="img img-thumbnail" style="width:150px; height:100px;"></a></div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div id="viewPhotos" class="modal fade" role="dialog" aria-labelledby="viewPhotos" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-header pb-0 bg-dark">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem"><i class="fl flaticon-photo-camera"></i></h3>
                    </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close" id="close"><span aria-hidden="true">&times;</button>
                </div>
                <div class="modal-content">           
                    <div class="m-content p-1 py-2 text-center" id="modal_content"></div>
                </div>
            </div>
        </div>
    @else
        <!--begin::Alert div -->
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
            </div>
            <div class="m-alert__close"></div>
        </div>  
        <!--End::Alert div -->              
    @endif
</div>