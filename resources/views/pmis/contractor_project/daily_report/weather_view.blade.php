@alert
@endalert   
<!--begin::data-->
@if($weather)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.weather') }} : </label><br>
                    <span>{{ $weather->static_data->{'name_'.$lang} }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.high_temp') }} : </label><br>
                    <span> {{ $weather->high_temp }} </span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.low_temp') }} : </label><br>
                    <span>{{ $weather->low_temp }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.weather_precipitation') }} : </label><br>
                    <span>{{ $weather->weather_precipitation }}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.wind_speed') }} : </label><br>
                    <span>{{ $weather->wind_speed }}</span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#collapseWeatherDiv').removeClass('show');
        $('#collapsWeatherBtn').addClass('d-none');
    </script>
@endif