@extends('master')
@section('head')
  <title>{{ trans('daily_report.daily_report') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{ trans('daily_report.list_daily_report') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <!-- Include Summary Modal -->
        @include('pmis.contractor_project.daily_report.create')
        {{-- @include('pmis.contractor_project.daily_report.edit') --}}
        {{-- @include('pmis.contractor_project.daily_report.view') --}}
        <ul class="m-portlet__nav">
          @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add"))
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" data-toggle="modal" data-target="#daily_report_create" id="daily_report" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="la la-cart-plus"></i><span>{{ trans('daily_report.create_daily_report') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <a href="{{ route('contractor_project',['con_id'=>$con_id,'dep_id'=>$dep_id]) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <div class="m-portlet__body table-responsive" id="searchresult"> 
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr>
            <th width="15%">{{ trans('daily_report.report_urn') }}</th>
            <th width="10%">{{ trans('daily_report.reportDate') }}</th>
            <th width="25%">{{ trans('daily_report.project_location') }}</th>
            <th width="15%">{{ trans('global.owner') }}</th>
            <th width="15%">{{ trans('daily_report.status') }}</th>
            <th width="10%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <?php $enc_id = encrypt($rec->id); ?>
              <tr>
                <td>{{ $rec->urn }}</td>
                <td>{{ dateCheck($rec->report_date,$lang) }}</td>
                <td>{{ $rec->location->province->{'name_'.$lang} }} @if($rec->location->district)/ {{$rec->location->district->{'name_'.$lang} }} @endif @if($rec->location->village)/ {{$rec->location->village->{'name_'.$lang} }} @endif @if($rec->location->latitude)/ {{ $rec->location->latitude }} @endif @if($rec->location->longitude)/ {{ $rec->location->longitude }} @endif</td>
                <td>{{ $rec->users['name'] }}</td>
                <td>
                  @if($rec->status==0)
                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('daily_report.pending') }}</span>
                  @elseif($rec->status==1)
                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('daily_report.approved') }}</span>
                  @elseif($rec->status==2)
                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('daily_report.rejected') }}</span>
                  @endif
                </td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_view"))
                          <a href="javascript:void()" onclick="viewRecord('{{route('location.index')}}','report_id={{$rec->id}}&&project_id={{$rec->project_id}}','POST','m_portlet_tools_2')" data-toggle="modal" data-target="#daily_report_create" class="dropdown-item"><i class="la la-eye"></i>{{ trans('global.view_details') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {{ $records->links('pagination') }}
      @endif
    </div>
  </div>
@endsection
