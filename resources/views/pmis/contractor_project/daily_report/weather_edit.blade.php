<div class="m-portlet__head">
    <div class="m-portlet__head-caption col-xl-5 col-lg-5 col-md-5 col-sm-5">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fa fa-cloud"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.edit_weather')}}</h3>
        </div>
    </div>
</div>
<div class="m-portlet__body" id="daily_report_body">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="daily_weather">
        <div class="m-portlet__body">
             <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                    <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input datePicker" name="req_report_date" id="req_report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                        <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                    </div>
                    <div class="req_report_date"></div>
                    <span class="m-form__help">{{trans('daily_report.location_note')}}</span>
                    <input type="hidden" name="report_date" value='{{ $report_date }}'>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                    <select class="form-control m-input select-2" name="req_location_id" id="req_location_id" style="width: 100%" disabled>
                        <option value="">{{trans('global.select')}}</option>
                        @if($location)
                            @if($location->id==$project_location_id)
                                <option value="{{$location->id}}" selected>{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                            @else
                                <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                            @endif
                        @endif
                    </select>
                    <div class="req_location_id"></div>
                    <span class="m-form__help">{{trans('daily_report.location_note')}}</span>
                    <input type="hidden" name="project_location_id" value='{{ $project_location_id }}'>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.weather') }} : <span style="color:red;">*</span></label>
                    <select class="form-control m-input select-2 errorDiv" name="is_worked_stoped" id="is_worked_stoped">
                        {{staticOption('8',$daily_report->weather->first()->is_worked_stoped)}}
                    </select>
                    <div class="is_worked_stoped error-div" style="display:none;"></div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{trans('daily_report.high_temp')}} :</label>
                    <input type="number" class="form-control m-input" id="high_temp" value="{{$daily_report->weather->first()->high_temp}}" name="high_temp">
                    <span class="m-form__help">{{trans('daily_report.cg')}}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{trans('daily_report.low_temp')}} :</label>
                    <input type="number" class="form-control m-input" id="low_temp"  value="{{$daily_report->weather->first()->low_temp}}" name="low_temp">
                    <span class="m-form__help">{{trans('daily_report.cg')}}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{trans('daily_report.weather_precipitation')}} :</label>
                    <input type="number" class="form-control m-input" id="weather_precipitation"  value="{{$daily_report->weather->first()->weather_precipitation}}" name="weather_precipitation">
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{trans('daily_report.wind_speed')}} :</label>
                    <input type="number" class="form-control m-input" id="wind_speed" name="wind_speed" value="{{$daily_report->weather->first()->wind_speed}}">
                </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <button type="button" onclick="storeData('{{route('daily_weather.store')}}','daily_weather','POST','daily_weather_list',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
    <table class="table table-sm m-table">
        <thead class="thead-inverse bg-light">
            <tr>
                <th>#</th>
                <th>{{trans('daily_report.weather')}}</th>
                <th>{{trans('daily_report.high_temp')}}</th>
                <th>{{trans('daily_report.low_temp')}}</th>
                <th>{{trans('daily_report.weather_precipitation')}}</th>
                <th>{{trans('daily_report.wind_speed')}}</th>
            </tr>
        </thead>
        <tbody  id="daily_weather_list">
         @if(count($daily_report->weather)>0)
            @foreach ($daily_report->weather as $weather)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$weather->is_worked_stoped}}</td>
                    <td>{{$weather->high_temp}}</td>
                    <td>{{$weather->low_temp}}</td>
                    <td>{{$weather->weather_precipitation}}</td>
                    <td>{{$weather->wind_speed}}</td>
                </tr>
            @endforeach
           @endif
        </tbody>
    </table>
