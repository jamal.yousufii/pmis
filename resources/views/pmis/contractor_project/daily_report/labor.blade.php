<div id="main_content">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="la flaticon-users"></i></span>
                <h3 class="m-portlet__head-text">{{trans('daily_report.labor')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and $daily_report and $approval!=1)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsLaborBtn" data-toggle="collapse" href="#collapseLaborDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($daily_report)
        <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseLaborDiv">
            <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="laborForm" enctype="multipart/form-data">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="report_date" style="display:none;"></div>
                            </div>
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                                <div id="div_location_id" class="errorDiv">
                                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                        @if(!empty($location)) 
                                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                        @else
                                            <option value="">{{trans('global.select')}}</option>
                                        @endif
                                    </select>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="location_id error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.labor_clasification') }} : <span style="color:red;">*</span></label><br>
                                <div id="div_labor_clasification" class="errorDiv">
                                    <select class="form-control select-2 m-input errorDiv required" name="labor_clasification" id="labor_clasification" style="width: 100%">
                                        <option value="">{{trans('global.select')}}</option>
                                        @if($labor)
                                            @foreach($labor as $item)
                                                <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>    
                                <div class="labor_clasification error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('daily_report.emp_no') }} : <span style="color:red;">*</span></label>
                                <input type="number" min="0" class="form-control m-input errorDiv required" name="labor_number" id="labor_number" />
                                <div class="labor_number error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('daily_report.work_hours') }} : <span style="color:red;">*</span></label>
                                <input type="number" min="0" max="24" class="form-control m-input errorDiv required" name="work_hours" id="work_hours" />
                                <div class="work_hours error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom bg-light">
                            <div class="col-lg-12">
                                <input type="hidden" name="report_id" value="{{ session('report_id') }}">
                                <button type="button" onclick="storeData('{{route('labor.store')}}','laborForm','POST','daily_labor_list',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
        <!--end::Form-->
        <!-- Display Report -->
        <div id="delete_msg_div"></div>
        <div id="daily_labor_list">
            <table class="table table-striped- table-bordered table-hover table-checkable mt-2">
                <thead>
                    <tr class="bg-light">
                        <th width="10%">{{ trans('global.number') }}</th>
                        <th width="40%">{{ trans('daily_report.labor_clasification') }}</th>
                        <th width="20%">{{ trans('daily_report.emp_no') }}</th>
                        <th width="20%">{{ trans('daily_report.work_hours') }}</th>
                        <th width="20%">{{ trans('global.action') }}</th>
                    </tr>
                </thead>
                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                    @if($laborer)
                        @foreach($laborer AS $item)
                            <tr id="tr_{{$loop->iteration}}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->labor['name_dr'] }}</td>
                                <td>{{ $item->labor_number }}</td>
                                <td>{{ $item->work_hours }}</td>
                                <td>
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $approval!=1)
                                    <a href="javascript:void(0)" onclick="editRecord('{{route('labor.edit')}}','id={{$item->id}}','POST','daily_labor_list',put_content)"><i class="fl flaticon-edit-1"></i></a>
                                    @endif
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $approval!=1)
                                    | <a href="javascript:void(0)" onclick="destroy('{{route('labor.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$loop->iteration}}')" class="text-danger"><i class="la la-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    @else
        <!--begin::Alert div -->
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
            </div>
            <div class="m-alert__close"></div>
        </div>  
        <!--End::Alert div -->              
    @endif    
</div>