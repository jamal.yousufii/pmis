@alert
@endalert   
<!--begin::data-->
@if($qc_narrative)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.is_worked_stoped') }} : </label><br>
                    <span>{{ ($qc_narrative->is_worked_stoped==1? trans('global.yes') : trans('global.no')) }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.main_prob') }} : </label><br>
                    <span> {!! $qc_narrative->main_problems !!} </span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.solution') }} : </label><br>
                    <span>{!! $qc_narrative->solutions !!}</span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#collapseNarrativeDiv').removeClass('show');
        $('#collapsNarrativeBtn').addClass('d-none');
    </script>
@endif
