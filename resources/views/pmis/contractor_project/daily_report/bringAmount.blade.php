<label class="title-custom">{{ trans('daily_report.amount') }} : <span style="color:red;">*</span></label>
<div class="slidecontainer">
    <!-- <input type="range" min="0" step="0.01" max="{{ $BQRecord->amount }}" value="0" class="slider" id="myRange"> -->
    <input type="number" name="amount" id="amount" min="0" step="0.01" max="{{ $BQRecord->amount }}" class="form-control m-input errorDiv required">
    <div class="amount" style="display:none;"></div>
    <div class="m-portlet mt-3 bg-dark text-white row">
        <div class="col-lg-4">
            <label class="title-custom p-3">{{ trans('daily_report.amount_done') }} : <span id="range-value"></span></label>
        </div>
        <div class="col-lg-4">
            <label class="title-custom p-3">{{ trans('daily_report.total_done') }} : <span>{{ $activity }}</span></label>
        </div>
        <div class="col-lg-4">
            <label class="title-custom p-3">{{ trans('daily_report.total_amount') }} : <span>{{ $BQRecord->amount }}</span></label>
        </div>
    </div>
</div>
<script>
    // var slider = document.getElementById("myRange");
    // var output = document.getElementById("range-value");
    // var target = document.getElementById("amount");
    // output.innerHTML = slider.value; // Display the default slider value
    // // Update the current slider value (each time you drag the slider handle)
    // slider.oninput = function() {
    //     output.innerHTML = this.value;
    //     target.value = this.value;
    // }
</script>