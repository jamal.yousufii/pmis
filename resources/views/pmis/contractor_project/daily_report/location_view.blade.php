@alert()
@endalert
<!--begin::data-->
@if($daily_report)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.reportDate') }} : </label><br>
                    <span>{{ dateCheck($daily_report->report_date,$lang) }}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.project_location') }} : </label><br>
                    <span> {{ $daily_report->location->province->{'name_'.$lang} }} @if($daily_report->location->district)/ {{$daily_report->location->district->{'name_'.$lang} }} @endif @if($daily_report->location->village)/ {{$daily_report->location->village->{'name_'.$lang} }} @endif @if($daily_report->location->latitude)/ {{ $daily_report->location->latitude }} @endif @if($daily_report->location->longitude)/ {{ $daily_report->location->longitude }} @endif </span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#collapseLocationDiv').removeClass('show');
        $('#collapsLocationBtn').addClass('d-none');
    </script>
@endif
