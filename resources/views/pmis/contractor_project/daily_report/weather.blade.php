<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fl flaticon-multimedia-3"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.weather')}}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and !$weather and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsWeatherBtn" data-toggle="collapse" href="#collapseWeatherDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                    </a>
                </li>
            @elseif(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $weather and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsWeatherBtnEdit" data-toggle="collapse" href="#collapseWeatherDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="fl flaticon-edit-1"></i> <span>{{ trans('global.edit') }}</span></span>
                    </a>
                </li>    
            @endif
        </ul>
    </div>
</div>
@if($daily_report)
    <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseWeatherDiv">
        <!-- Add Report -->
        <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="daily_weather">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="report_date" style="display:none;"></div>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                            <div id="div_location_id" class="errorDiv">
                                <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                    @if(!empty($location)) 
                                        <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                    @else
                                        <option value="">{{trans('global.select')}}</option>
                                    @endif
                                </select>
                            </div>
                            <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                            <div class="location_id error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.weather') }} : <span style="color:red;">*</span></label>
                            <div id="is_worked_stoped" class="errorDiv">
                                <select class="form-control m-input select-2" name="is_worked_stoped" style="width: 100%">
                                    <option value="">{{trans('global.select')}}</option>
                                    @if($weather)
                                        {{staticOption('8',$weather->is_worked_stoped)}}   
                                    @else
                                        {{staticOption('8',0)}}
                                    @endif
                                </select>
                            </div>
                            <div class="is_worked_stoped error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label class="title-custom">{{trans('daily_report.high_temp')}} :</label>
                            <input type="text" class="form-control m-input" value="{{($weather? $weather->high_temp : '')}}"  id="high_temp" name="high_temp">
                            <span class="m-form__help">{{trans('daily_report.cg')}}</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{trans('daily_report.low_temp')}} :</label>
                            <input type="text" class="form-control m-input" value="{{($weather? $weather->low_temp : '')}}" id="low_temp" name="low_temp">
                            <span class="m-form__help">{{trans('daily_report.cg')}}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                            <label class="title-custom">{{trans('daily_report.weather_precipitation')}} :</label>
                            <input type="text" class="form-control m-input" value="{{($weather? $weather->weather_precipitation : '')}}" id="weather_precipitation" name="weather_precipitation">
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{trans('daily_report.wind_speed')}} :</label>
                            <input type="text" class="form-control m-input" value="{{($weather? $weather->wind_speed : '')}}" id="wind_speed" name="wind_speed">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                        @if($weather) <!-- Update if daily report exist -->  
                            <input type="hidden" name="id" value="{{encrypt($weather->id)}}">
                            <button type="button" onclick="storeData('{{route('daily_weather.update')}}','daily_weather','POST','daily_weather_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
                        @else  <!-- Otherwise insert the data -->
                            <button type="button" onclick="storeData('{{route('daily_weather.store')}}','daily_weather','POST','daily_weather_content',put_content)" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        @endif  
                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!-- Display Report -->
    <div class="m-portlet__body" id="daily_weather_content">
        @if($weather)
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('daily_report.weather') }} : </label><br>
                            <span>{{ $weather->static_data->{'name_'.$lang} }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.high_temp') }} : </label><br>
                            <span> {{ $weather->high_temp }} </span>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.low_temp') }} : </label><br>
                            <span>{{ $weather->low_temp }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">  
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.weather_precipitation') }} : </label><br>
                            <span>{{ $weather->weather_precipitation }}</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="title-custom">{{ trans('daily_report.wind_speed') }} : </label><br>
                            <span>{{ $weather->wind_speed }}</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@else
    <!--begin::Alert div -->
    <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
        <div class="m-alert__icon">
            <i class="la la-warning"></i>
        </div>
        <div class="m-alert__text">
            <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
        </div>
        <div class="m-alert__close"></div>
    </div>  
    <!--End::Alert div -->              
@endif
