<div id="main_content">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="fl flaticon-pie-chart"></i></span>
                <h3 class="m-portlet__head-text">{{trans('daily_report.activities')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and $daily_report and $approval!=1)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsActivityBtn" data-toggle="collapse" href="#collapseActivityDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($daily_report)
        <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseActivityDiv">
            <!-- Add Report -->
            <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="activitiesForm" enctype="multipart/form-data">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input errorDiv datePicker required" name="daily_report" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="report_date" style="display:none;"></div>
                            </div>
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                                <div id="div_location_id" class="errorDiv">
                                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                        @if(!empty($location)) 
                                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                        @else
                                            <option value="">{{trans('global.select')}}</option>
                                        @endif
                                    </select>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="location_id error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.activity') }} : <span style="color:red;">*</span></label>
                                <div id="div_bq_id" class="errorDiv">
                                    <select class="form-control m-input select-2 required" name="bq_id" id="bq_id" style="width: 100%" onchange="bringUnit(this.value,'unit_div'); bringAmount(this.value,'amount_div');">
                                        <option value="">{{trans('global.select')}}</option>
                                        @if($BQRecord)
                                            @foreach($BQRecord as $item)
                                                <option value="{!!$item->id!!}">{!!$item->operation_type!!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="bq_id error-div" style="display:none;"></div>
                            </div>
                            <!-- <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.location') }} : </label>
                                <input type="text" class="form-control m-input" id="location" name="location">
                            </div> -->
                            <div class="col-lg-6">
                                <div id="unit_div">
                                    <label class="title-custom">{{ trans('daily_report.units') }} : <span style="color:red;">*</span></label>
                                    <div id="div_unit_id" class="errorDiv">
                                        <select class="form-control m-input select-2 errorDiv" name="unit_id" id="unit_id" style="width: 100%">
                                            <option value="">{{trans('global.select')}}</option>
                                        </select>
                                    </div>
                                    <div class="unit_id error-div" style="display:none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12">
                                <div id="amount_div"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom bg-light">
                            <div class="col-lg-12">
                                <input type="hidden" name="daily_report_id" value="{{ session('report_id') }}">
                                <button type="button" onclick="storeData('{{route('daily_activities.store')}}','activitiesForm','POST','daily_activities_list',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>  
        </div>
        <div id="delete_msg_div"></div>
        <!-- Display Report -->
        <div id="daily_activities_list">
            <table class="table table-striped- table-bordered table-hover table-checkable mt-2">
                <thead>
                    <tr class="bg-light">
                        <th width="30%">{{ trans('daily_report.activity') }}</th>
                        <th width="13%">{{ trans('daily_report.total_done1') }}</th>
                        <th width="15%">{{ trans('daily_report.percentage_done') }}</th>
                        <th width="10%">{{ trans('daily_report.total_amount') }}</th>
                        <th width="17%">{{ trans('daily_report.percentage') }}</th>
                        <th width="10%">{{ trans('global.action') }}</th>
                    </tr>
                </thead>
                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                    @if($activities)
                        @foreach($activities AS $item)
                            <tr id="tr_{{$loop->iteration}}">
                                <td>{{ $item->bill_quantity['operation_type'] }}</td>
                                <td>{{ $item->amount }}</td>
                                <td>{{ getPercentage($item->bill_quantity['amount'],$item->amount) }} %</td>
                                <td>{{ $item->bill_quantity['amount'] }}</td>
                                <td>{{ getPercentage($procurement->contract_price,$item->bill_quantity['total_price']) }} %</td>
                                <td>
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $approval!=1)
                                        <a href="javascript:void(0)" onclick="editRecord('{{route('daily_activities.edit')}}','id={{$item->id}}','POST','daily_activities_list',put_content)"><i class="fl flaticon-edit-1"></i></a>
                                    @endif
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $approval!=1)
                                    | <a href="javascript:void(0)" onclick="destroy('{{route('daily_activities.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$loop->iteration}}')" class="text-danger"><i class="la la-trash"></i></a> 
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    @else
        <!--begin::Alert div -->
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
            </div>
            <div class="m-alert__close"></div>
        </div>  
        <!--End::Alert div -->              
    @endif
</div>