<label class="title-custom">{{ trans('daily_report.units') }} : <span style="color:red;">*</span></label>
<div id="div_unit_id" class="errorDiv">
  <select class="form-control m-input select-2 errorDiv" name="unit_id" id="unit_id">
    @if($BQRecord->unit)
      <option value="{{ $BQRecord->unit->id }}" selected>{{ $BQRecord->unit->{'name_'.$lang} }}</option>
    @endif
  </select>
</div>
<div class="unit_id error-div" style="display:none;"></div>
<script type="text/javascript">
  $(".select-2").select2();
</script>