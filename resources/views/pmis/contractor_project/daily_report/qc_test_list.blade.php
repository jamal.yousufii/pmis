@alert()
@endalert
<table class="table table-striped- table-bordered table-hover table-checkable mt-2">
    <thead>
        <tr class="bg-light">
            <th width="40%">{{ trans('daily_report.activity') }}</th>
            <th width="12%">{{ trans('daily_report.test_no') }}</th>
            <th width="15%">{{ trans('daily_report.test_type') }}</th>
            <th width="5%">{{ trans('daily_report.completed') }}</th>
            <th width="15%">{{ trans('daily_report.test_result') }}</th>
            <th width="15%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @if($test)
            @foreach($test AS $item)
                <tr id="tr_{{$item->id}}">
                    <td>{{ $item->bill_quantity['operation_type'] }}</td>
                    <td>{{ $item->test_number }}</td>
                    <td>{{ $item->test_type }}</td>
                    <td>{{ ($item->is_completed=='completed'? trans('global.yes') : trans('global.no')) }}</td>
                    <td>
                    {{trans(Config::get('static.'.$lang.'.test_result.'.$item->test_status.'.name'))}}
                    </td>
                    <td>
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $approval!=1)
                            <a href="#" onclick="editRecord('{{route('qcTest.edit')}}','id={{$item->id}}','POST','daily_test_content',put_content)"><i class="fl flaticon-edit-1"></i></a>
                        @endif
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $approval!=1)
                        | <a class="text-danger" href="#" onclick="destroy('{{route('qcTest.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$item->id}}')"><i class="la la-trash"></i></a> 
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<script type="text/javascript">
    $('#collapseTestDiv').removeClass('show');
    $('#collapseTestDivEdit').removeClass('show');
    $('#collapsTestBtn').css('display','inline');
</script>