<!-- Report Start-->
<div id="daily_report_create" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="SummaryModal" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">           
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption px-2">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white">{{ trans('daily_report.create_daily_report') }}</h3>
                    </div>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" onclick="redirectFunction()" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content" id="resutl_div">
                <div class="m-portlet m-portlet--full-height">
                    <!--begin: Portlet Body-->
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                            <div class="row m-row--no-padding">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m--padding-top-10 m--padding-bottom-15 main-border-div">
                                    <div class="m-wizard__head p-3">
                                        <div class="m-wizard__nav">
                                            @php 
                                                $daily_sections = get_module_sections(session('current_mod'),session('current_department'),'daily_report_menue',1);
                                                $i = 1;
                                            @endphp 
                                            @if($daily_sections)
                                                @foreach($daily_sections as $sec)
                                                    <div class="m-wizard__steps mt-1">
                                                        <a href="javascript:void()" onclick="changeStatus('{{ $sec->id }}','number','tickClass','tickId_{{$i}}'); viewRecord('{{route($sec->url_route)}}','project_id={{$project_id}}','POST','m_portlet_tools_2')" class="m-menu__link" style="text-decoration: none;">
                                                            <div class="m-wizard__step m-wizard__step--done m-wizard__step--current mt-3 mb-2">
                                                                <div class="m-wizard__step-info">
                                                                    <span class="m-wizard__step-number title-custom">
                                                                        <span id="{{ $sec->id }}" class="<?=$sec->code=='daily_location'? 'bg-warning': 'bg-success' ?> text-white number bg-success">
                                                                            {{ $loop->iteration }}
                                                                        </span>
                                                                    </span>
                                                                    <div class="m-wizard__step-label title-custom" >{{ $sec->{'name_'.$lang} }}</div>
                                                                    <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2 tickClass" id="tickId_{{$i}}" >
                                                                       <?=$sec->code=='daily_location'? '<i class="la la-check text-warning text-white"></i>':''?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         </a>
                                                    </div> 
                                                    @if(!$loop->last)<hr class="p-0 m-0">@endif
                                                    @php $i++ @endphp
                                                @endforeach
				                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 main-border-div">
                                    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm" id="m_portlet_tools_2">
                                        <div class="m-portlet__head">
                                            <div class="m-portlet__head-caption">
                                                <div class="m-portlet__head-title">
                                                    <span class="m-portlet__head-icon"><i class="flaticon-map-location"></i></span>
                                                    <h3 class="m-portlet__head-text">{{trans('daily_report.project_location')}}</h3>
                                                </div>
                                            </div>
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav">
                                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add"))
                                                        <li class="m-portlet__nav-item">
                                                            <a class="btn btn-default btn-sm " id="collapsLocationBtn" data-toggle="collapse" href="#collapseLocationDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                                                                <span><i class="la la-arrows-v"></i> <span>{{ trans('daily_report.add') }}</span></span>
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Add Report -->
                                        <div class="code notranslate cssHigh collapse show" id="collapseLocationDiv">
                                            <!-- Add Report -->
                                            <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
                                                <!--begin::Form-->
                                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
                                                    <div class="m-portlet__body">
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-lg-6">
                                                                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                                                                <div class="m-input-icon m-input-icon--left">
                                                                    <input type="text" class="form-control m-input datePicker required" name="report_date" id="report_date" style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                                                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                                                                </div>
                                                                <div class="report_date error-div" style="display:none;"></div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                                                                <div id="div_location_id" class="errorDiv">
                                                                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%">
                                                                        <option value="">{{trans('global.select')}}</option>
                                                                        @if($locations)
                                                                            @foreach($locations as $location)
                                                                                <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="location_id error-div" style="display:none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <input type="hidden" name="project_id" value="{{ $project_id }}">
                                                            <button type="button" onclick="storeData('{{route('location.store')}}','locationForm','POST','daily_location_content',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!--end::Form-->
                                            </div>
                                        </div>
                                        <!-- Display Report -->
                                        <div class="m-portlet__body" id="daily_location_content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        @if ($lang=="en")
            $(".datePicker").attr('type', 'date');
        @else
            $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
        @endif
    });

    function changeStatus(id,className,tickClass,tickId) {
        $("."+className).removeClass('bg-warning');
        $("#"+id).addClass('bg-warning');
        $("."+tickClass).html('');
        $("#"+tickId).html('<i class="la la-check text-warning text-white"></i>');
    }
</script>
<!--end::Report-->
