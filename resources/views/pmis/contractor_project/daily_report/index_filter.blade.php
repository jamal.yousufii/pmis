<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
        <tr>
        <th width="15%">{{ trans('daily_report.report_urn') }}</th>
        <th width="10%">{{ trans('daily_report.reportDate') }}</th>
        <th width="25%">{{ trans('daily_report.project_location') }}</th>
        <th width="15%">{{ trans('global.owner') }}</th>
        <th width="15%">{{ trans('daily_report.status') }}</th>
        <th width="10%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
        @foreach($records AS $rec)
            <?php $enc_id = encrypt($rec->id); ?>
            <tr>
            <td>{{ $rec->urn }}</td>
            <td>{{ dateCheck($rec->report_date,$lang) }}</td>
            <td>{{ $rec->location->province->{'name_'.$lang} }} @if($rec->location->district)/ {{$rec->location->district->{'name_'.$lang} }} @endif @if($rec->location->village)/ {{$rec->location->village->{'name_'.$lang} }} @endif @if($rec->location->latitude)/ {{ $rec->location->latitude }} @endif @if($rec->location->longitude)/ {{ $rec->location->longitude }} @endif</td>
            <td>{{ $rec->users['name'] }}</td>
            <td>
              @if($rec->status==0)
                <span class="m-badge m-badge--warning m-badge--wide">{{ trans('daily_report.pending') }}</span>
              @elseif($rec->status==1)
                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('daily_report.approved') }}</span>
              @elseif($rec->status==2)
                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('daily_report.rejected') }}</span>
              @endif
            </td>
            <td>
                <span class="dtr-data">
                <span class="dropdown">
                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_view"))
                        <a href="javascript:void()" onclick="viewRecord('{{route('location.index')}}','report_id={{$rec->id}}&&project_id={{$rec->project_id}}','POST','m_portlet_tools_2')" data-toggle="modal" data-target="#daily_report_create" class="dropdown-item"><i class="la la-edit"></i>{{ trans('global.view_details') }}</a>
                    @endif
                    </div>
                </span>
                </span>
            </td>
            </tr>
        @endforeach
        @endif
    </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
    {{ $records->links('pagination') }}
@endif
<script type="text/javascript">
  $(document).ready(function() {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
        document.cookie = "no="+$(this).text();
  			var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&project_id="+'{!!encrypt($project_id)!!}'+"&rep_status="+'{!!$status!!}';
  			$.ajax({
                  url: '{{ route("filterDailyReports") }}',
                  data: dataString,
                  type: 'get',
                  beforeSend: function(){
                      $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                  },
                  success: function(response)
                  {
                      $('#searchresult').html(response);
                  }
  	            }
  	    );
  		}
  	});
  });
</script>
