<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="flaticon-map-location"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.project_location')}}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and !$daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsLocationBtn" data-toggle="collapse" href="#collapseLocationDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                    </a>
                </li>
            @elseif(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $daily_report and $approval!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsEdit" data-toggle="collapse" href="#collapseLocationDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="fl flaticon-edit-1"></i> <span>{{ trans('global.edit') }}</span></span>
                    </a>
                </li>
            @endif
            @if($daily_report_log->count()>0)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-info btn-sm" id="collapseApprove" data-toggle="collapse" href="#collapseApproveDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-arrows-v"></i> <span>{{ trans('monitoring.status') }}</span></span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
@if($daily_report and $daily_report->created_by==userid() and $daily_report->completed==0)
    <div class="m-portlet__head mt-2" style="height:12%;">
        <form enctype="multipart/form-data" id="completeForm" method="POST">
            <table>
                <td>
                    <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                        <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                            <label><input type="checkbox" id="complete" name="complete" value="1" onclick="completeRecord('{{ route('completeRecords') }}','complete','completeForm','POST','response_div')"><span></span></label>
                        </span>
                    </span>
                </td>
                <td><label class="title-custom">{{ trans('global.complete') }}</label></td>
            </table>
            <input type="hidden" name="table" value="daily_reports" />
            <input type="hidden" name="rec_id" value="{{ encrypt($daily_report->id) }}" />
            <input type="hidden" name="project_id" value="{{ encrypt($daily_report->project_id) }}" />
        </form>
    </div>
@endif
<div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseLocationDiv">
    <!-- Add Report -->
    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                        <div class="m-input-icon m-input-icon--left">
                            <!--if Value record the set the value for update   -->
                            <input type="text" class="form-control m-input datePicker required" value="{{($daily_report? dateCheck($daily_report->report_date,$lang):'')}}" name="report_date" id="report_date"  style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                            <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                        </div>
                        <div class="report_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                        <div id="div_location_id" class="errorDiv">
                            <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%">
                                <option value="">{{trans('global.select')}}</option>
                                @if($locations)
                                    @foreach($locations as $location)
                                        <option {{($daily_report? ($daily_report->project_location_id==$location->id? 'selected': '') : '')}} value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="location_id error-div" style="display:none;"></div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <input type="hidden" name="project_id" value="{{ $project_id }}">
                    @if($daily_report) <!-- Update the reocrd  -->
                       <input type="hidden" name="report_id" value="{{($daily_report->id)}}">
                       <button type="button" onclick="storeData('{{route('location.update')}}','locationForm','POST','daily_location_content',put_content);" class="btn btn-primary">{{ trans('global.edit') }}</button>
                    @else  <!-- Insert the reocrd  -->
                       <button type="button" onclick="storeData('{{route('location.store')}}','locationForm','POST','daily_location_content',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    @endif  
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseApproveDiv">
    <!-- Add Report -->
    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                @if($daily_report_log)
                    @foreach($daily_report_log as $rec)
                        <div class="row m-form__group_custom pt-1">
                            <div class="col-lg-12 text-title">{{$loop->iteration}} .</div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('monitoring.status') }} : </label><br>
                                @if($rec->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('monitoring.pending') }}</span>
                                @elseif($rec->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('monitoring.approved') }}</span>
                                @elseif($rec->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('monitoring.rejected') }}</span>
                                @endif
                            </div>
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('monitoring.date') }} : </label><br>
                                <span>{{ dateCheck($rec->status_date,$lang) }}</span>
                            </div>
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('global.owner') }} : </label><br>
                                <span>{{ $rec->approved_by['name'] }}</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('monitoring.description1') }} : </label><br>
                                <span>{{ $rec->description }}</span>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<!-- Display Report -->
<div class="m-portlet__body" id="daily_location_content">
    @if($daily_report)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('daily_report.reportDate') }} : </label><br>
                        <span>{{ dateCheck($daily_report->report_date,$lang) }}</span>
                    </div>
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('daily_report.project_location') }} : </label><br>
                        <span> {{ $daily_report->location->province->{'name_'.$lang} }} @if($daily_report->location->district)/ {{$daily_report->location->district->{'name_'.$lang} }} @endif @if($daily_report->location->village)/ {{$daily_report->location->village->{'name_'.$lang} }} @endif @if($daily_report->location->latitude)/ {{ $daily_report->location->latitude }} @endif @if($daily_report->location->longitude)/ {{ $daily_report->location->longitude }} @endif </span>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

@section('js-code')
  <script type="text/javascript">
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
  </script>
@endsection