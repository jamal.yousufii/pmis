 <div class="row">
    <div class="col-lg-12">
      <div class="m-portlet">
        <table width="100%" border="1">
                <tr>
                    <th colspan="4" class="text-center">
                        ریاست جمهوری اسلامی افغانستان<br>
                        اداره عملیاتی و حمایوی انکشاف ملی ریاست جمهوری ا.ا<br>
                        ریاست تصدی بنای
                    </th>
                </tr>
                <tr>
                    <th width="20%">{{trans('daily_report.reported_date')}}</th>
                    <td width="30"><span style="font-weight:bold;">{{ trans('global.from_date')}} : </span>{{$from_report_date  }} <span style="font-weight:bold;">{{trans('global.to_date')}} : </span>{{$to_report_date}}</td>
                    <th width="20%">{{trans('daily_report.report_no')}}</th>
                    <th width="30%"></th>
                </tr>
                <tr>
                    <th>{{trans('daily_report.project_name')}}</th>
                    <td>{{$project->name}}</td>
                    <th>{{trans('daily_report.project_location')}}</th>
                    <td>{{$location->province->{'name_'.$lang} }}</td>
                </tr>
                <tr>
                    <th>{{trans('daily_report.project_code')}}</th>
                    <td>{{$project->code}}</td>
                    <th>{{trans('daily_report.contractor')}}</th>
                    <td>{{$project->project_contractor->id}}</td>
                </tr>
                <tr>
                    <th>{{trans('daily_report.start_date')}}</th>
                    <td>{{ datecheck($project->project_start_date,$lang )}}</td>
                    <th>{{trans('daily_report.end_date')}}</th>
                    <td>{{ datecheck($project->project_end_date,$lang )}}</td>
                </tr>              
        </table>
            <table width="100%" border="1">
                <tr class="text-center">
                    <th width="5%" class="text-center">#</th>
                    <th width="45%">{{trans('daily_report.activity')}}</th>
                    <th width="10%">{{trans('daily_report.location')}}</th>
                    <th width="10%">{{trans('daily_report.units')}}</th>
                    <th width="10%">{{trans('daily_report.total_done1')}}</th>
                    <th width="10%">{{trans('daily_report.remain_work')}}</th>
                    <th width="10%">{{trans('global.date')}}</th>
                </tr>
                @foreach ($daily_report as $dreport)
                  @if(count($dreport->activities)>0)
                    @foreach ($dreport->activities as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->bill_quantity->operation_type }}</td>
                            <td>{{$item->location}}</td>
                            <td>{{$item->bill_quantity->unit->name_dr}}</td>
                            <td>{{$item->amount}}</td>
                            <td>{{$item->bill_quantity->amount-$item->amount}}</td>
                            <td>{{datecheck($dreport->report_date,$lang)}}</td>
                        </tr>
                    @endforeach 
                   @endif 
                @endforeach    
            </table>
         <table width="100%" border="1">
            <tr>
                <th colspan="5" class="text-center bg-secondary">{{trans('daily_report.list_equipment')}}</th>
            </tr>
            <tr class="text-center">
                <th width="5%">#</th>
                <th width="40%">{{trans('daily_report.equipment')}}</th>
                <th width="10%">{{trans('daily_report.equipment_no')}}</th>
                <th width="10%">{{trans('daily_report.work_hours')}}</th>
                <th width="10%">{{trans('global.date')}}</th>
            </tr>
             @foreach ($daily_report as $dreport)
               @if(count($dreport->equipments)>0)                 
                    @foreach ($dreport->equipments as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $item->equipment->{'name_'.$lang} }}</td>
                            <td>{{ $item->equipment_no }}</td>
                            <td>{{ $item->work_hourse }}</td>
                            <td>{{datecheck($dreport->report_date,$lang)}}</td>
                        </tr>
                    @endforeach
                @endif    
            @endforeach
        </table>
        <table width="100%" border="1">
            <tr>
                <th colspan="5" class="text-center bg-secondary">{{trans('daily_report.list_labor')}}</th>
            </tr>
            <tr class="text-center">
                <th width="5%">#</th>
                <th width="65%">{{trans('daily_report.labor_clasification')}}</th>
                <th width="10%">{{trans('daily_report.labor_number')}}</th>
                <th width="10%">{{trans('daily_report.work_hours')}}</th>
                <th width="10%">{{trans('global.date')}}</th>

            </tr>
            @foreach ($daily_report as $dreport)
              @if(count($dreport->labour)>0)  
                @foreach($dreport->labour AS $item)
                    <tr id="tr_{{$loop->iteration}}">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->labor->{'name_'.$lang} }}</td>
                        <td>{{ $item->labor_number }}</td>
                        <td>{{ $item->work_hours }}</td>
                        <td>{{datecheck($dreport->report_date,$lang)}}</td>
                    </tr>
                @endforeach
              @endif      
            @endforeach  
        </table>
        <table width="100%" border="1">
            <tr>
                <th colspan="7" class="text-center bg-secondary">{{trans('daily_report.qc_test')}}</th>
            </tr>
            <tr class="text-center">
                <th width="5%">#</th>
                <th width="30%">{{trans('daily_report.activity')}}</th>
                <th width="10%">{{trans('daily_report.test_no')}}</th>
                <th width="20%">{{trans('daily_report.test_type')}}</th>
                <th width="10%">{{trans('daily_report.location')}}</th>
                <th width="10%">{{trans('daily_report.test_result')}}</th>
                <th width="10%">{{trans('global.date')}}</th>
            </tr>
            @foreach ($daily_report as $dreport)
             @if(count($dreport->test)>0) 
              @foreach($dreport->test AS $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ $item->bill_quantity->operation_type }}</td>
                    <td>{{ $item->test_number }}</td>
                    <td>{{ $item->test_type }}</td>
                    <td>{{ $item->test_location }}</td>
                    <td>
                        {{trans(Config::get('static.'.$lang.'.test_result.'.$item->test_status.'.name'))}}
                    </td>
                    <td>{{datecheck($dreport->report_date,$lang)}}</td>
                </tr>
               @endforeach
              @endif  
            @endforeach               
        </table>
        <table width="100%" border="1">
            <tr>
                <th colspan="8" class="text-center bg-secondary">{{trans('daily_report.weather')}}</th>
            </tr>
            <tr class="text-center">
                <th width="5%">#</th>
                <th width="30%">{{trans('daily_report.weather')}}</th>
                <th width="15%">{{trans('daily_report.high_temp')}}</th>
                <th width="15%">{{trans('daily_report.low_temp')}}</th>
                <th width="15%">{{trans('daily_report.weather_precipitation')}}</th>
                <th width="10%">{{trans('daily_report.wind_speed')}}</th>
                <th width="10%">{{trans('global.date')}}</th>
            </tr>
            @foreach ($daily_report as $dreport)              
             @if($dreport->weather)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$dreport->weather->static_data->{'name_'.$lang} }}</td>
                    <td>{{$dreport->weather->high_temp }}</td>
                    <td>{{$dreport->weather->low_temp }}</td>
                    <td>{{$dreport->weather->weather_precipitation }}</td>
                    <td>{{$dreport->weather->wind_speed }}</td>
                    <td>{{datecheck($dreport->report_date,$lang)}}</td>
                </tr>
              @endif  
            @endforeach
        </table>
        <table width="100%" border="1">
            <tr>
                <th colspan="6" class="text-center bg-secondary">{{trans('daily_report.weather')}}</th>
            </tr>
            <tr class="text-center">
                <th width="5%">#</th>
                <th width="40%">{{trans('daily_report.main_prob')}}</th>
                <th width="40%">{{trans('daily_report.solution')}}</th>
                <th width="10%">{{trans('global.date')}}</th>
            </tr>
            <tr>
            @foreach ($daily_report as $dreport)              
             @if($dreport->narrative)
                <tr>
                    <td>1</td>
                    <td>{!! $dreport->narrative->main_problems !!}</td>
                    <td>{!! $dreport->narrative->solutions !!}</td>
                    <td>{{datecheck($dreport->report_date,$lang)}}</td>
                </tr>
              @endif
            @endforeach    
        </table>
    </div>
</div>
</div>