@alert
@endalert
<table class="table table-striped- table-bordered table-hover table-checkable mt-4">
    <thead>
        <tr>
            <th width="30%">{{ trans('daily_report.activity') }}</th>
            <th width="15%">{{ trans('daily_report.total_done1') }}</th>
            <th width="15%">{{ trans('daily_report.percentage_done') }}</th>
            <th width="10%">{{ trans('daily_report.total_amount') }}</th>
            <th width="15%">{{ trans('daily_report.percentage') }}</th>
            <th width="10%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($daily_report)
            @foreach($daily_report->activities AS $item)
                <tr id="tr_{{$loop->iteration}}">
                    <td>{{ $item->bill_quantity['operation_type'] }}</td>
                    <td>{{ $item->amount }}</td>
                    <td>{{ getPercentage($item->bill_quantity['amount'],$item->amount) }} %</td>
                    <td>{{ $item->bill_quantity['amount'] }}</td>
                    <td>{{ getPercentage($procurement->contract_price,$item->bill_quantity['total_price']) }} %</td>
                    <td>
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $daily_report->status!=1)
                            <a href="javascript:void(0)" onclick="editRecord('{{route('daily_activities.edit')}}','id={{$item->id}}','POST','daily_activities_list',put_content)"><i class="fl flaticon-edit-1"></i></a>
                        @endif
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $daily_report->status!=1)
                         | <a href="javascript:void(0)" onclick="destroy('{{route('daily_activities.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$loop->iteration}}')" class="text-danger"><i class="la la-trash"></i></a> 
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<script type="text/javascript">
    $('#activitiesForm')[0].reset();
    $('#collapseActivityDiv').removeClass('show');
    $('#collapsActivityBtn').css('display','inline');
</script>