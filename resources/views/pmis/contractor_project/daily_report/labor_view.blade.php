@alert
@endalert
<table class="table table-striped- table-bordered table-hover table-checkable mt-4">
    <thead>
        <tr>
            <th width="10%">{{ trans('global.number') }}</th>
            <th width="40%">{{ trans('daily_report.labor_clasification') }}</th>
            <th width="20%">{{ trans('daily_report.emp_no') }}</th>
            <th width="20%">{{ trans('daily_report.work_hours') }}</th>
            <th width="20%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($laborer)
            @foreach($laborer AS $item)
                <tr id="tr_{{$loop->iteration}}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->labor['name_dr'] }}</td>
                    <td>{{ $item->labor_number }}</td>
                    <td>{{ $item->work_hours }}</td>
                    <td>
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $approval!=1)
                            <a href="javascript:void(0)" onclick="editRecord('{{route('labor.edit')}}','id={{$item->id}}','POST','daily_labor_list',put_content)"><i class="fl flaticon-edit-1"></i></a>
                        @endif
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $approval!=1)
                        | <a href="javascript:void(0)" onclick="destroy('{{route('labor.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$loop->iteration}}')" class="text-danger"><i class="la la-trash"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<script type="text/javascript">
    $('#collapseLaborDiv').removeClass('show');
    $('#collapsLaborBtn').css('display','inline');
</script>