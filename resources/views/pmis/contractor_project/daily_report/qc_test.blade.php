<div id="main_content">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="fl flaticon-time"></i></span>
                <h3 class="m-portlet__head-text">{{trans('daily_report.qc_test')}}</h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_add") and $daily_report and $approval!=1)
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" id="collapsTestBtn" data-toggle="collapse" href="#collapseTestDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                            <span><i class="la la-cart-plus"></i> <span>{{ trans('daily_report.add') }}</span></span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($daily_report)
        <div class="code notranslate cssHigh collapse <?= !$daily_report? 'show' : '' ?>" id="collapseTestDiv">
            <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="testForm" enctype="multipart/form-data">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="<?=$report_date!=''?dateCheck($report_date,$lang): '' ?>" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="report_date" style="display:none;"></div>
                            </div>
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                                <div id="div_location_id" class="errorDiv">
                                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                                        @if(!empty($location)) 
                                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                                        @else
                                            <option value="">{{trans('global.select')}}</option>
                                        @endif
                                    </select>
                                </div>
                                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                                <div class="location_id error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('daily_report.activity') }} : <span style="color:red;">*</span></label>
                                <div id="div_bill_quant_id" class="errorDiv">
                                    <select class="form-control select-2 m-input errorDiv required" name="bill_quant_id" id="bill_quant_id" style="width: 100%">
                                        <option value="">{{trans('global.select')}}</option>
                                        @foreach ($bill_quantity as $item)
                                            <option value="{{ $item->id }}">{{ $item->operation_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="bill_quant_id error-div" style="display:none;"></div>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label class="title-custom">{{trans('daily_report.test_no')}} : <span style="color:red;">*</span></label>
                                <input type="text" class="form-control m-input errorDiv required" name="test_number" id="test_number" />
                                <div class="test_number error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-8">
                                <label class="title-custom">{{trans('daily_report.test_type')}} : <span style="color:red;">*</span></label>
                                <input class="form-control m-input errorDiv required" name="test_type" id="test_type" />
                                <div class="test_type error-div" style="display:none;"></div>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label class="title-custom">{{trans('global.location')}} :</label>
                                <input type="text" class="form-control m-input" name="test_location" id="test_location" />
                            </div>
                            <div class="col-lg-2 pt-2">
                                <label class="title-custom">{{trans('daily_report.completed')}} :</label><br>
                                <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                    <label><input type="checkbox" name="is_completed"><span></span></label>
                                </span>
                            </div>
                            <div class="col-lg-4">
                                <label class="title-custom">{{trans('daily_report.test_result')}} : <span style="color:red;">*</span></label>
                                <div id="div_test_status" class="errorDiv">
                                    <select class="form-control select-2 m-input required" name="test_status" id="test_status" style="width: 100%">
                                        <option value="">{{trans('global.select')}}</option>
                                        <option value="waiting">{{trans('daily_report.waiting')}}</option>
                                        <option value="passed">{{trans('daily_report.passed')}}</option>
                                        <option value="failed">{{trans('daily_report.failed')}}</option>
                                    </select>   
                                </div>
                                <div class="test_status error-div" style="display:none;"></div>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12">
                                <label class="title-custom">{{trans('global.description')}} :</label>
                                <textarea name="description" class="form-control tinymce m-input" id="description"></textarea>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row m-form__group_custom bg-light">
                            <div class="col-lg-12">
                                <input type="hidden" name="report_id" value="{{ session('report_id') }}">
                                <button type="button" onclick="storeData('{{route('qcTest.store')}}','testForm','POST','daily_test_content',put_content);" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!-- Display Report -->
        <div id="delete_msg_div"></div>
        <div id="daily_test_content">
            <table class="table table-striped- table-bordered table-hover table-checkable mt-2">
                <thead>
                    <tr class="bg-light">
                        <th width="40%">{{ trans('daily_report.activity') }}</th>
                        <th width="12%">{{ trans('daily_report.test_no') }}</th>
                        <th width="15%">{{ trans('daily_report.test_type') }}</th>
                        <th width="5%">{{ trans('daily_report.completed') }}</th>
                        <th width="15%">{{ trans('daily_report.test_result') }}</th>
                        <th width="15%">{{ trans('global.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($test)
                        @foreach($test AS $item)
                            <tr id="tr_{{$item->id}}">
                                <td>{{ $item->bill_quantity['operation_type'] }}</td>
                                <td>{{ $item->test_number }}</td>
                                <td>{{ $item->test_type }}</td>
                                <td>{{ ($item->is_completed=='completed'? trans('global.yes') : trans('global.no')) }}</td>
                                <td>
                                {{trans(Config::get('static.'.$lang.'.test_result.'.$item->test_status.'.name'))}}
                                </td>
                                <td>
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_edit") and $approval!=1)
                                        <a href="#" onclick="editRecord('{{route('qcTest.edit')}}','id={{$item->id}}','POST','daily_test_content',put_content)"><i class="fl flaticon-edit-1"></i></a>
                                    @endif
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_daily_report","daily_report_delete") and $approval!=1)
                                    | <a class="text-danger" href="#" onclick="destroy('{{route('qcTest.delete')}}','id={{encrypt($item->id)}}','POST','delete_msg_div','tr_{{$item->id}}')"><i class="la la-trash"></i></a> 
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    @else
        <!--begin::Alert div -->
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <strong>{{trans('global.notice')}}:</strong> {{trans('daily_report.select_date_location')}}
            </div>
            <div class="m-alert__close"></div>
        </div>  
        <!--End::Alert div -->              
    @endif
</div>
