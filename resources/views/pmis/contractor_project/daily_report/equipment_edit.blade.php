<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="equipmentFormEdit" enctype="multipart/form-data">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.reportDate') }} : <span style="color:red;">*</span></label>
                <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input errorDiv datePicker required" name="report_date" id="report_date" value="{{($equipment?dateCheck($equipment->dailyReport->report_date,$lang): '')}}" disabled style="width: 100%" placeholder="{{ trans('daily_report.report_date') }}" id="m_datepicker_3_modal">
                    <span class="m-input-icon__icon m-input-icon__icon--left" id="showText"><span><i class="la la-calendar"></i></span></span>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="report_date" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.project_location') }} : <span style="color:red;">*</span></label>
                <div id="div_location_id" class="errorDiv">
                    <select class="form-control m-input select-2 required" name="location_id" id="location_id" style="width: 100%" disabled>
                        @if(!empty($location)) 
                            <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                        @else
                            <option value="">{{trans('global.select')}}</option>
                        @endif
                    </select>
                </div>
                <span class="m-form__help">{{trans('daily_report.location_note')}}</span><br>
                <div class="location_id error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('daily_report.equipment') }} : <span style="color:red;">*</span></label><br>
                <div id="equipment_type" class="errorDiv">
                    <select class="form-control select-2 m-input errorDiv" name="equipment_type" style="width: 100%">
                        <option value="">{{trans('global.select')}}</option>
                        @if($equipments_category)
                            @foreach($equipments_category as $item)
                                <option <?=$item->id==$equipment->equipment_type? 'selected' : ''?> value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                            @endforeach
                        @endif
                    </select>    
                </div>
                <div class="equipment_type error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('daily_report.equipment_no') }} : <span style="color:red;">*</span></label>
                <input type="number" min="0" class="form-control m-input errorDiv" value="{{$equipment->equipment_no}}" name="equipment_no" id="equipment_no" />
                <div class="equipment_no error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('daily_report.work_hours') }} : <span style="color:red;">*</span></label>
                <input type="number" min="0" max="24" class="form-control m-input errorDiv" value="{{$equipment->work_hourse}}" name="work_hourse" id="work_hourse" />
                <div class="work_hourse error-div" style="display:none;"></div>
            </div>
        </div>
        <input type="hidden" name="id" value="{{encrypt($equipment->id)}}">
        <div class="form-group m-form__group row m-form__group_custom bg-light">
            <div class="col-lg-12">
                <button type="button" onclick="storeData('{{route('equipment.update')}}','equipmentFormEdit','POST','daily_equipment_list',put_content);" class="btn btn-primary">{{ trans('global.edit') }}</button>
                <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </div> 
</form>
<!--end::Form-->
<script type="text/javascript">
    $(".select-2").select2();
    $('#collapsEquipmentBtn').css('display','none');
</script>
    