<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="BQForm_{{$item->id}}" method="post"> 
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3">
        <div class="col-lg-8">
            <label class="title-custom">{{ trans('estimation.operation_type') }}: <span style="color:red;">*</span></label>
            <textarea class="form-control m-input m-input--air errorDiv" name="operation_type" id="operation_type" rows="1" onkeypress="getKey(event);">{{ $item->operation_type }}</textarea>
            <div class="operation_type error-div" style="display:none;"></div>
            <a data-toggle="modal" data-target="#BQModal" id="findBQ"></a>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
            <div id="unit_id_{{$item->id}}" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" dir="rtl" name="unit_id" required style="width: 100%">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($units)
                        @foreach($units as $unit)
                            @if($unit->id==$item->unit_id)
                                <option value="{!!$unit->id!!}" selected="selected">{!!$unit->name!!}</option>
                            @else
                                <option value="{!!$unit->id!!}">{!!$unit->name!!}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="unit_id_{{$item->id}} error-div" style="display:none;"></div>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount_{{$item->id}}" value="{{ $item->amount }}" onchange="javascript: getTotal('amount_{{$item->id}}','price_{{$item->id}}','total_price_{{$item->id}}',{{$record->cost}},'percentage_{{$item->id}}','remaining_{{$item->id}}',{{ $remaining_percent }},{{ getPercentage($record->cost,$item->estimated_total_price) }},true,'erro_msg_edit')" onkeyup="javascript: getTotal('amount_{{$item->id}}','price_{{$item->id}}','total_price_{{$item->id}}',{{$record->cost}},'percentage_{{$item->id}}','remaining_{{$item->id}}',{{ $remaining_percent }},{{ getPercentage($record->cost,$item->estimated_total_price) }},true,'erro_msg_edit')" >
            <div class="amount_{{$item->id}} error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }} : <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv" type="number" min="0" name="price" id="price_{{$item->id}}" value="{{ $item->estimated_price }}" onchange="javascript: getTotal('amount_{{$item->id}}','price_{{$item->id}}','total_price_{{$item->id}}',{{$record->cost}},'percentage_{{$item->id}}','remaining_{{$item->id}}',{{ $remaining_percent }},{{ getPercentage($record->cost,$item->estimated_total_price) }},true,'erro_msg_edit')" onkeyup="javascript: getTotal('amount_{{$item->id}}','price_{{$item->id}}','total_price_{{$item->id}}',{{$record->cost}},'percentage_{{$item->id}}','remaining_{{$item->id}}',{{ $remaining_percent }},{{ getPercentage($record->cost,$item->estimated_total_price) }},true,'erro_msg_edit')" >                                                                                        
            <div class="price_{{$item->id}} error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }} : </label>
            <input class="form-control m-input" type="number" min="0" name="total_price" id="total_price_{{$item->id}}" value="{{ $item->estimated_total_price }}" readonly>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.percentage') }} (%) : <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv" type="number" min="0" name="percentage" id="percentage_{{$item->id}}" value="{{ getPercentage($record->cost,$item->estimated_total_price) }}" readonly>
            <div class="percentage_{{$item->id}} error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.remaining_percentage') }} (%) : </label>
            <input class="form-control m-input" type="number" value="{{ $remaining_percent }}" name="remaining_{{$item->id}}" id="remaining_{{$item->id}}" readonly>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('global.section') }}: <span style="color:red;">*</span></label>
            <div id="bq_section_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="bq_section_id" dir="rtl" style="width: 100%">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($bq_sections)
                        @foreach($bq_sections as $sections)
                            <option value="{{$sections->id}}" {{$sections->id==$item->bq_section_id ? 'selected' : '' }} >{{$sections->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="bq_section_id error-div" style="display:none;"></div>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom" id="erro_msg_edit"  style="display:none">
        <div class="col-lg-12">
            <div class="alert alert-danger alert-dismissible title-custom">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                {{ trans('global.percent_msg') }}
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-12">
            <label class="title-custom">{{ trans('global.location') }}: <span style="color:red;">*</span></label>
            <div id="div_project_location_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" dir="rtl" name="project_location_id" style="width: 100%">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($project_location)
                        @foreach($project_location as $loc)
                            <option value="{{$loc->id}}" @if($loc->id == $item->project_location_id) selected @endif>{{ $loc->province->{'name_'.$lang} }} @if($loc->district)/ {{ $loc->district->{'name_'.$lang} }} @endif @if($loc->village)/ {{ $loc->village->{'name_'.$lang} }} @endif @if($loc->latitude) / {{ $loc->latitude }} @endif @if($loc->longitude)/ {{ $loc->longitude }} @endif </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="project_location_id error-div" style="display:none;"></div>
        </div>
    </div>

    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-12">
            <label class="title-custom">{{ trans('estimation.remarks') }}: </label>
            <textarea class="form-control m-input m-input--air" name="remarks" rows="1">{{ $item->remarks }}</textarea>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
            <input type="hidden" name="pla_id" id="pla_id" value="{{ $enc_id }}"/>
            <input type="hidden" name="rec_id" value="{{ encrypt($item->id) }}" />
            <button type="button" class="btn btn-primary btn-sm" onclick="updateBQ('BQForm_{{$item->id}}','detail_content_{{$item->id}}','EditDivDetails-{{$item->id}}','ShowDivDetails-{{$item->id}}','msgDiv-{{$item->id}}')">{{ trans('global.submit') }}</button>
            <button type="button" class="btn btn-secondary btn-sm" onclick="redirectFunction()">{{ trans('global.cancel') }}</button>  
        </div>
    </div>
</form>
<script type="text/javascript">
    function updateBQ(form_id,response_div,editDiv,viewDiv,msgDiv)
    {
        $('.error-div').html('');
        $('.errorDiv').css('border-bottom','1px solid #ebedf2');
        var formData = new FormData($("#"+form_id)[0]);
        $.ajax({
            url: '{{ route("update_bill") }}',
            data: formData,
            contentType: false,
            type: 'POST',
            success: function(response)
            {
                redirectFunction();
            },
            error: function (request, status, error) {
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('.'+key).show();
                    $('.'+key).html('<span class="text-danger">'+value+'</span>');
                    $('#'+key).css('border-bottom','1px solid #dc3545');
                });
            },
            cache: false,
            processData: false
        });
    }
</script>