@if($bill_quantities->count()>0)
    <div class="form-group m-form__group row m-form__group_custom table-responsive" id="details-content">
        <table class="table table-striped-">
            <tbody>
                <tr>
                    <th width="40%" class="top-no-border">{{ trans('estimation.operation_type') }}</th>
                    <th width="15%" class="top-no-border">{{ trans('estimation.unit') }}</th>
                    <th width="15%" class="top-no-border">{{ trans('estimation.amount') }}</th>
                    <th width="20%" class="top-no-border">{{ trans('estimation.total_price') }}</th>
                    <th width="10%" class="top-no-border">{{ trans('global.action') }}</th>
                </tr>
            </tbody>
        </table>
        @foreach($bill_quantities as $item) 
            <table class="table table-striped-" id="detail_content_{{$item->id}}">
                <tbody>
                    <tr>
                        <td width="40%">{{ limit_text($item->operation_type) }}</td>
                        <td width="12%">@if($item->unit_id!=0){{ $item->unit->name_dr }}@else '' @endif</td>
                        <td width="12%">{{ $item->amount }}</td>
                        <td width="13%">{{ $item->total_price }}</td>
                        <td width="13%">{{ $item->percentage }}٪</td>
                        <td width="10%">
                            <span class="dtr-data">
                                <span class="dropdown">
                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit"))
                                            <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                        @endif
                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_view"))
                                            <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#ShowDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="ShowDivDetails-{{$item->id}}"><i class="la la-eye"></i>{{ trans('global.view') }}</span>
                                        @endif
                                    </div>
                                </span>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- Include BQ View and Edit -->
            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
                @include('pmis.estimations.quantities_edit')   
            </div>
            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="ShowDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
                @include('pmis.estimations.quantities_view')   
            </div>
        @endforeach 
        <!-- Pagination -->
        @if(!empty($bill_quantities))
            {!!$bill_quantities->links('pagination')!!}
        @endif  
    </div>
@else
    <div class="col-xl-9 col-lg-12">
        <div class="m-wizard__form">
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                    <dt>{{ trans('global.no_records') }}</dt>
                </div>
            </div>  
        </div>
    </div>
@endif