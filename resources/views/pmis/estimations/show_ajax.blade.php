<table class="table table-bordered">
    <thead class="bg-light">
        <tr>
            <th width="5%" class="py-3">{{ trans('global.number') }}</th>
            <th width="30%" class="py-3">{{ trans('estimation.operation_type') }}</th>
            <th width="9%" class="py-3">{{ trans('estimation.unit') }}</th>
            <th width="9%" class="py-3">{{ trans('estimation.amount') }}</th>
            <th width="9%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
            <th width="9%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
            <th width="9%" class="py-3">{{ trans('estimation.percentage') }}</th>
            <th width="15%" class="py-3">{{ trans('estimation.remarks') }}</th>
            <th width="5%" class="py-3">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @php 
        $x = iteration($counter);
        $bq_section = 0;
        @endphp
        @foreach($bill_quantities as $item)
            @if($bq_section != $item->bq_section_id)
                @php
                    $bq_section = $item->bq_section_id;
                @endphp
                <tr style="background: #5b99bf4a">
                    <td colspan="9" class="text-center title-custom">
                        {{ $item->bq_section->{'name_'.$lang} }}
                    </td>
                <tr>
            @endif
            <tr>
                <td>{{ $x }}</td>
                <td>{{ limit_text($item->operation_type) }}</td>
                <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
                <td>{{ $item->amount }}</td>
                <td>{{ $item->estimated_price }}</td>
                <td>{{ $item->estimated_total_price }}</td>
                <td>{{ getPercentage($total_price,$item->estimated_total_price,true,4) }}</td>
                <td>{{ $item->remarks }}</td>
                <td>
                    <span class="dtr-data">
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit") and $plan->share->count()==0)
                                    <a class="dropdown-item" id="collapsBtn" data-toggle="collapse" onclick="removeClass('EditDivDetails-{{$item->id}}','ShowDivDetails-{{$item->id}}')" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                @endif
                            </div>
                        </span>
                    </span>
                </td>
            </tr>
            <!-- Include BQ Edit -->
            <tr class="EditDivDetails-{{$item->id}} d-none">
                <td colspan="9">
                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #21252985;">
                        @include('pmis.estimations.quantities_edit')
                    </div>
                </td>
            </tr>
            @php $x++; @endphp
        @endforeach
    </tbody>
</table>
<!-- Pagination -->
@if(!empty($bill_quantities))
    {!!$bill_quantities->links('pagination')!!}
@endif
<script type="text/javascript">
    $(".select-2").select2();
    $(document).ready(function()
    {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if ($(this).attr('href') != '#') {
                // Get current URL route
                document.cookie = "no="+$(this).text();
                var dataString = '';
                counter = parseInt($(this).attr('id'));
                dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter;
                $.ajax({
                    url : '{{ url()->current() }}',
                    data: dataString,
                    type: 'get',
                    beforeSend: function(){
                    $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                    },
                    success: function(response)
                    {
                    $('#searchresult').html(response);
                    }
                });
            }
        });
    });
</script>
