<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead class="bg-light">
    <tr>
      <th width="15%">{{ trans('global.pro_urn') }}</th>
      <th width="20%">{{ trans('plans.project_name') }}</th>
      <th width="15%">{{ trans('plans.project_code') }}</th>
      <th width="20%">{{ trans('global.shared_from') }}</th>
      <th width="10%">{{ trans('global.shared_by') }}</th>
      <th width="10%">{{ trans('global.status') }}</th>
      <th width="10%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
      @foreach($records AS $rec)
        <?php $enc_id = encrypt($rec->id); ?>
        <tr @if(!isset($rec->estimation['id'])) style="color:red;" @endif>
          <td>{!!$rec->urn!!}</td>
          <td>{!!$rec->name!!}</td>
          <td>{!!$rec->code!!}</td>
          <td>{!!$rec->share()->where('share_to_code','pmis_estimation')->first()->from_department->{'name_'.$lang}!!}</td>
          <td>
              @php
                $share = $rec->share()->where('share_to_code','pmis_estimation')->first()->user; 
              @endphp
              @if($share!="")
                  {!!$share->name!!}
              @endif
          </td>
          <td>
            @if(isset($rec->estimation['process']) and $rec->estimation['process']=='0')
              <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
            @elseif(isset($rec->estimation['process']) and $rec->estimation['process']=='1')
              <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
            @else
              <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
            @endif
          </td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_view"))
                    <a class="dropdown-item" href="{{ route('estimations.show',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view_details') }}</a>
                  @endif
                </div>
              </span>
            </span>
          </td>  
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function() {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
        document.cookie = "no="+$(this).text();
  			var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&department_id="+'{!!$request->department_id!!}'+"&section="+'{!!$request->section!!}'+"&field="+'{!!$request->field!!}'+"&condition="+'{!! $request->condition!!}'+"&value="+'{!! $request->value !!}';
        serverRequest('{{ route("filter_survey") }}',dataString,'POST','searchresult',true);
  		}
  	});
  });
</script>
