@extends('master')
@section('head')
    <title>{{ trans('estimation.estimation') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile" id="showContent">
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <!-- Include Summary Modal -->
                        @include('pmis.summary.summary_modal')
                        <ul class="m-portlet__nav">
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_add") and !$record)
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:void()" onclick="addRecord('{{route('estimations.create')}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                        <span><i class="la la-cart-plus"></i><span>{{ trans('estimation.add') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit") and $record and $plan->share->count()==0)
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:void()" onclick="addRecord('{{route('estimations.edit',encrypt($record->id))}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                        <span><i class="la la-edit"></i><span>{{ trans('estimation.edit') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if($record)
                                <!-- Include Attachments Modal -->
                                @include('pmis.attachments.modal')
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&record_id={{encrypt($record->id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","view_pro_summary"))
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#SummaryModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('estimation',session('current_department')) }}">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if($record)
                    <div class="m-portlet__head table-responsive">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('estimation.view') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_add") && $plan->share->count()==0)
                                    <!-- Include Add of Bill of Quantities Modal -->
                                    @include('pmis.estimations.quantities_add')
                                     <!-- Include Excel import Modal -->
                                     @include('pmis.estimations.import_excel')
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" data-toggle="modal" data-target="#AddBillofQuantitiesModal">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('estimation.add_bill_quantitie') }}</span></span>
                                        </a>
                                    </li>
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" data-toggle="modal" data-target="#importModal">
                                            <span><i class="la la-file-excel-o"></i><span>{{ trans('estimation.estimation_import') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($bill_quantities->count()>0 && $plan->share->count()==0 and (doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_complete") || doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_stop")))
                                    <!-- Include complete & stop modal -->
                                    @include('pmis.estimations.complete_modal')
                                    @include('pmis.estimations.stop_modal')
                                    <div class="btn-group open">
                                        <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                            <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                                        </button>
                                        <ul class="dropdown-menu mt-2" role="menu">
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_complete"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                        <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_stop"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                        <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_share") and $bill_quantities->count()>0 && $plan->share->count()==0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($plan->share->count()>0 and $bill_quantities->count()>0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    @alert()
                    @endalert
                    <!-- Include sharing modal -->
                    @include('pmis.estimations.share_modal')
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="searchresult">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.start_date') }} :</label><br>
                                    <span>{!!dateCheck($record->start_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.end_date') }} :</label><br>
                                    <span>{!!dateCheck($record->end_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.estimated_cost') }} :</label><br>
                                    <span>{!! number_format($record->cost) !!}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
                                    <span>{{$record->employee->first_name}}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.employees') }} :</label><br>
                                    <span>
                                    @if($record->estimationTeam)
                                        @foreach($record->estimationTeam as $item)
                                        <span class="bg-default">
                                            {{ $item->employees->first_name }} 
                                            @if(!$loop->last), @endif
                                        </span>
                                        @endforeach
                                    @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12 col-md-12">
                                    <label class="title-custom">{{ trans('estimation.description') }} :</label><br>
                                    <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Display Project Details -->
                    <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
                        <div class="m-portlet__head-tools" style="background: #f7f9f6">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm  pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                        <div class="m-portlet__body">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-3" id="searchresult">
                                <div class="col-lg-12">
                                    <label class="title-custom">{{ trans('global.location') }}:</label>
                                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('location_bill_quantity.list',[$enc_id,'loc_id']) }}',this.value)" >
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($project_location)
                                            @foreach($project_location as $item)
                                                <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="bill_quantity_div">
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
