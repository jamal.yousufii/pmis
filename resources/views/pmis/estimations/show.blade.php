@extends('master')
@section('head')
    <title>{{ trans('estimation.estimation') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile" id="showContent">
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <!-- Include Summary Modal -->
                        @include('pmis.summary.summary_modal')
                        <ul class="m-portlet__nav">
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_add") and !$record)
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:void()" onclick="addRecord('{{route('estimations.create')}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                        <span><i class="la la-cart-plus"></i><span>{{ trans('estimation.add') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit") and $record and $plan->share->count()==0)
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:void()" onclick="addRecord('{{route('estimations.edit',encrypt($record->id))}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                        <span><i class="la la-edit"></i><span>{{ trans('estimation.edit') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            @if($record)
                                <!-- Include Attachments Modal -->
                                @include('pmis.attachments.modal')
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&record_id={{encrypt($record->id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","view_pro_summary"))
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#SummaryModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('estimation',session('current_department')) }}">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if($record)
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('estimation.view') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_add") && $plan->share->count()==0)
                                    <!-- Include Add of Bill of Quantities Modal -->
                                    @include('pmis.estimations.quantities_add')
                                     <!-- Include Excel import Modal -->
                                     @include('pmis.estimations.import_excel')
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" data-toggle="modal" data-target="#AddBillofQuantitiesModal">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('estimation.add_bill_quantitie') }}</span></span>
                                        </a>
                                    </li>
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" data-toggle="modal" data-target="#importModal">
                                            <span><i class="la la-file-excel-o"></i><span>{{ trans('estimation.estimation_import') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($bill_quantities->count()>0 && $plan->share->count()==0 and (doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_complete") || doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_stop")))
                                    <!-- Include complete & stop modal -->
                                    @include('pmis.estimations.complete_modal')
                                    @include('pmis.estimations.stop_modal')
                                    <div class="btn-group open">
                                        <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                            <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                                        </button>
                                        <ul class="dropdown-menu mt-2" role="menu">
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_complete"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                        <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_stop"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                        <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_share") and $bill_quantities->count()>0 && $plan->share->count()==0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($plan->share->count()>0 and $bill_quantities->count()>0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    @alert()
                    @endalert
                    <!-- Include sharing modal -->
                    @include('pmis.estimations.share_modal')
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.start_date') }} :</label><br>
                                    <span>{!!dateCheck($record->start_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.end_date') }} :</label><br>
                                    <span>{!!dateCheck($record->end_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.estimated_cost') }} :</label><br>
                                    <span>{!! number_format($record->cost) !!}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
                                    <span>{{$record->employee->first_name}}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.employees') }} :</label><br>
                                    <span>
                                    @if($record->estimationTeam)
                                        @foreach($record->estimationTeam as $item)
                                        <span class="bg-default">
                                            {{ $item->employees->first_name }} 
                                            @if(!$loop->last), @endif
                                        </span>
                                        @endforeach
                                    @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12 col-md-12">
                                    <label class="title-custom">{{ trans('estimation.description') }} :</label><br>
                                    <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Display Project Details -->
                    <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
                        <div class="m-portlet__head-tools" style="background: #f7f9f6;">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm  pt-3 pb-3" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                        <div class="m-portlet__body">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed row pt-3 pb-0">
                                <div class="col-lg-12 px-5">
                                    <label class="title-custom">{{ trans('global.location') }}:</label>
                                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('location_bill_quantity.list',[$enc_id,'loc_id']) }}',this.value)" >
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($project_location)
                                            @foreach($project_location as $item)
                                                <option value="{{$item->id}}" <?= $project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed row pt-3 pb-0">
                                <div class="col-lg-10 px-5">
                                    <div class="form-group m-form__group row m-form__group_custom p-1">
                                        <label class="title-custom px-2">{{ trans('estimation.total_billq_price') }} :  {!!$total_price!!}</label>
                                    </div>
                                </div>
                                <div class="col-lg-2 px-5">
                                    <a class="btn btn-success btn-sm m-btn float-right" href="{{ route('printExport', [$enc_id,encrypt($project_location_id)]) }}">
                                        <span><i class="la la-print"></i> <span>{{ trans('estimation.print_export') }}</span></span>
                                    </a>
                                </div>
                            </div>
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="bill_quantity_div">
                                @if($bill_quantities->count()>0)
                                    <div class="form-group m-form__group row m-form__group_custom table-responsive" id="searchresult">
                                        <table class="table table-bordered">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th width="5%" class="py-3">{{ trans('global.number') }}</th>
                                                    <th width="30%" class="py-3">{{ trans('estimation.operation_type') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.unit') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.amount') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.percentage') }}</th>
                                                    <th width="15%" class="py-3">{{ trans('estimation.remarks') }}</th>
                                                    <th width="5%" class="py-3">{{ trans('global.action') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $bq_section = 0;
                                                @endphp
                                                @foreach($bill_quantities as $item)
                                                    @if($bq_section != $item->bq_section_id)
                                                        @php
                                                            $bq_section = $item->bq_section_id;
                                                        @endphp
                                                        <tr style="background: #5b99bf4a">
                                                            <td colspan="9" class="text-center title-custom">
                                                                {{ $item->bq_section->{'name_'.$lang} }}
                                                            </td>
                                                        <tr>
                                                    @endif
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ limit_text($item->operation_type) }}</td>
                                                        <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
                                                        <td>{{ $item->amount }}</td>
                                                        <td>{{ $item->estimated_price }}</td>
                                                        <td>{{ $item->estimated_total_price }}</td>
                                                        <td>{{ getPercentage($total_price,$item->estimated_total_price,true,4) }}</td>
                                                        <td>{{ $item->remarks }}</td>
                                                        <td>
                                                            <span class="dtr-data">
                                                                <span class="dropdown">
                                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit") and $plan->share->count()==0)
                                                                            <a class="dropdown-item" id="collapsBtn" data-toggle="collapse" onclick="removeClass('EditDivDetails-{{$item->id}}','ShowDivDetails-{{$item->id}}')" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                                                        @endif
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <!-- Include BQ Edit -->
                                                    <tr class="EditDivDetails-{{$item->id}} d-none">
                                                        <td colspan="9">
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #21252985;">
                                                                @include('pmis.estimations.quantities_edit')
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- Pagination -->
                                        @if(!empty($bill_quantities))
                                            {!!$bill_quantities->links('pagination')!!}
                                        @endif
                                    </div>
                                @else
                                    <div class="form-group m-form__group row m-form__group_custom table-responsive" id="searchresult">
                                        <table class="table table-bordered">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th width="5%" class="py-3">{{ trans('global.number') }}</th>
                                                    <th width="30%" class="py-3">{{ trans('estimation.operation_type') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.unit') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.amount') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.percentage') }}</th>
                                                    <th width="15%" class="py-3">{{ trans('estimation.remarks') }}</th>
                                                    <th width="5%" class="py-3">{{ trans('global.action') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="9">
                                                        <span class="text-danger" style="font-size:1.2rem">{{ trans('global.no_records') }}</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--labeliteration-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function removeClass(id,sec_id)
    {
        if($('.'+id).hasClass('d-none')){
            $('.'+id).removeClass('d-none');
        }else{
            $('.'+id).addClass('d-none');
        }
        $('.'+sec_id).addClass('d-none');
    }
</script>
