<!-- Location Start-->
<div id="AddBillofQuantitiesModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="AddBillofQuantitiesModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
        <div class="m-portlet__head bg-color-dark">
          <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                  <span class="m-portlet__head-icon"><i class="la la-leanpub"></i></span>
                  <h3 class="m-portlet__head-text white-color">{{trans('estimation.add_bill_quantitie')}}</h3>
              </div>
          </div>
          <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item white-color">
                      <button type="button" class="close white-color" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                  </li>
              </ul>
          </div>
        </div>
        <div class="m-portlet__body">
          <!--begin::Form-->
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="BQForm" method="post">
              <div class="form-group m-form__group row m-form__group_custom p-0 pb-3">
                  <div class="col-lg-8">
                      <label class="title-custom">{{ trans('estimation.operation_type') }}: <span style="color:red;">*</span></label>
                      <textarea class="form-control m-input m-input--air errorDiv" name="operation_type" id="operation_type" rows="1" onkeypress="getKey(event);"></textarea>
                      <div class="operation_type error-div" style="display:none;"></div>
                      <a data-toggle="modal" data-target="#BQModal" id="findBQ"></a>
                  </div>
                  <div class="col-lg-4">
                    <label class="title-custom">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
                    <div id="unit_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="unit_id">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($units)
                                @foreach($units as $unit)
                                <option value="{!!$unit->id!!}">{!!$unit->name!!}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="unit_id error-div" style="display:none;"></div>
                  </div>
              </div>
              <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
                @if($remaining_percent==0 or $remaining_percent<0)
                  <div class="col-lg-4">
                    <label class="title-custom">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
                    <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount" readonly>
                    <div class="amount error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-4">
                      <label class="title-custom">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="number" min="0" name="price" id="price" readonly>
                      <div class="price error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-4">
                      <label class="title-custom">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }} : </label>
                      <input class="form-control m-input" type="number" min="0" name="total_price" id="total_price" readonly>
                  </div>
                @else
                  <div class="col-lg-4">
                      <label class="title-custom">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount" onchange="javascript: getTotal('amount','price','total_price',{{$record->cost}},'percentage','remaining',{{ $remaining_percent }},'0',false,'erro_msg_add')" onkeyup="javascript: getTotal('amount','price','total_price',{{$record->cost}},'percentage','remaining',{{ $remaining_percent }},'0',false,'erro_msg_add')">
                      <div class="amount error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-4">
                      <label class="title-custom">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }} : <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="number" min="0" name="price" id="price" onchange="javascript: getTotal('amount','price','total_price',{{$record->cost}},'percentage','remaining',{{ $remaining_percent }},'0',false,'erro_msg_add')" onkeyup="javascript: getTotal('amount','price','total_price',{{$record->cost}},'percentage','remaining',{{ $remaining_percent }},'0',false,'erro_msg_add')">
                      <div class="price error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-4">
                      <label class="title-custom">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }} : </label>
                      <input class="form-control m-input" type="number" min="0" name="total_price" id="total_price" readonly>
                  </div>
                @endif
              </div>
              <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('estimation.percentage') }} (%) : <span style="color:red;">*</span></label>
                  <input class="form-control m-input errorDiv" type="number" min="0" name="percentage" id="percentage" readonly>
                  <div class="percentage error-div" style="display:none;"></div>
                </div>
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('estimation.remaining_percentage') }} (%) : </label>
                  <input class="form-control m-input" type="number" value="{{ $remaining_percent }}" name="remaining" id="remaining" readonly>
                </div>
                <div class="col-lg-4">
                  <label class="title-custom">{{ trans('global.section') }}: <span style="color:red;">*</span></label>
                  <div id="bq_section_id" class="errorDiv">
                    <select class="form-control m-input m-input--air select-2" name="bq_section_id">
                      <option value="">{{ trans('global.select') }}</option>
                      @if($bq_sections)
                        @foreach($bq_sections as $item)
                          <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endforeach
                      @endif
                    </select>
                  </div>
                  <div class="bq_section_id error-div" style="display:none;"></div>
                </div>
              </div>
              @if($remaining_percent==0 or $remaining_percent < 0)
                <div class="alert alert-danger alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>{{ trans('global.percent_msg') }}
                </div>
              @endif
              <div class="form-group m-form__group row m-form__group_custom" id="erro_msg_add"  style="display:none">
                  <div class="col-lg-12">
                      <div class="alert alert-danger alert-dismissible title-custom">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                          {{ trans('global.percent_msg') }}
                      </div>
                  </div>
              </div>
              <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
                  <div class="col-lg-12">
                    <label class="title-custom">{{ trans('global.location') }}: <span style="color:red;">*</span></label>
                    <div id="project_location_id" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2 required" name="project_location_id" style="width: 100%">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($project_location)
                                @foreach($project_location as $item)
                                  <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="project_location_id error-div" style="display:none;"></div>
                  </div>
                </div>
              <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
                  <div class="col-lg-12">
                      <label class="title-custom">{{ trans('estimation.remarks') }}: </label>
                      <textarea class="form-control m-input m-input--air" name="remarks" rows="1"></textarea>
                  </div>
              </div>
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                  <input type="hidden" name="pla_id" id="pla_id" value="{{ $enc_id }}"/>
                  <input type="hidden" name="rec_id" id="rec_id" value="{{ encrypt($record->id) }}"/>
                  <button type="button" onclick="storeRecord('{{route('store_bill')}}','BQForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>
  </div>
</div>
