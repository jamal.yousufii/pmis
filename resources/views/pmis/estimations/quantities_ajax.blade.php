@foreach($bill_quantities as $item)
    <table class="table table-striped-" id="detail_content_{{$item->id}}">
        <tbody>  
            <tr>
                <td width="40%">{{ limit_text($item->operation_type) }}</td>
                <td width="15%">{{ $item->unit->name_dr }}</td>
                <td width="15%">{{ $item->amount }}</td>
                <td width="20%">{{ $item->total_price }}</td>
                <td width="20%">{{ $item->percentage }}٪</td>
                <td width="10%">
                    <span class="dtr-data">
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_edit"))
                                    <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","est_view"))
                                    <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#ShowDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="ShowDivDetails-{{$item->id}}"><i class="la la-eye"></i>{{ trans('global.view') }}</span>
                                @endif
                            </div>
                        </span>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- Include BQ View and Edit -->
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
        @include('pmis.estimations.quantities_edit')   
    </div>
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="ShowDivDetails-{{$item->id}}" style="border:1px solid #62b4f3;">
        @include('pmis.estimations.quantities_view')   
    </div>
    <div id="msgDiv-{{$item->id}}" class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
        @if (Session::has('success_detail'))
            <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-check-square"></i></div>
                <div class="m-alert__text">{!! Session::get('success_detail') !!}</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
        @elseif (Session::has('fail_detail'))
            <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-warning"></i></div>
                <div class="m-alert__text">{!! Session::get('success_detail') !!}</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
        @endif
    </div>
@endforeach 