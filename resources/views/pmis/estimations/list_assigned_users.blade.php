<table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
    <thead>
        <tr>
            <th width="10%">{{ trans('global.number') }}</th>
            <th width="90%">{{ trans('global.section_owner') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
            @foreach($records AS $rec)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $rec->user->name }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>