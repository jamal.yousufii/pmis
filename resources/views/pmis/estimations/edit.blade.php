<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('estimation.edit') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_estimation","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="estimationForm" method="post" enctype="multipart/form-data">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
              <label class="title-custom">{{ trans('estimation.start_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker errorDiv" type="text" value="{{ dateCheck($record->start_date,$lang) }}" name="start_date" id="start_date" required>
              <div class="start_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
              <label class="title-custom">{{ trans('estimation.end_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker errorDiv" type="text" value="{{ dateCheck($record->end_date,$lang) }}" name="end_date" id="end_date" required>
              <div class="end_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
              <label class="title-custom">{{ trans('estimation.estimated_cost') }}: </label>
              <input class="form-control m-input errorDiv" type="number" min="1" value="{{ $record->cost }}" name="cost" id="cost">
              <div class="cost error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
              <label class="title-custom">{{ trans('designs.responsible') }}: <span style="color:red;">*</span></label>
              <div id="employee_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="employee_id">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($employees)
                    @foreach($employees as $emp)
                      <option value="{!!$emp->id!!}" <?=$emp->id==$record->employee->id? 'selected': ''?>>{!!$emp->first_name!!}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="employee_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-8 col-sm-8 col-xsm-t col-md-8">
              <label class="title-custom">{{ trans('designs.employees') }}: </label>
              <select class="form-control m-input m-input--air select-2" name="employees[]" multiple>
                <option value="">{{ trans('global.select') }}</option>
                @if($employees)
                  @foreach($employees as $emp)
                    <option value="{!!$emp->id!!}" @if(in_array($emp->id,$team_members)) selected @endif >{!!$emp->first_name!!}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12 col-sm-12 col-xsm-t col-md-12">
              <label class="title-custom">{{ trans('estimation.description') }}:</label>
              <textarea class="form-control m-input m-input--air tinymce" name="description" rows="3">{!! $record->description !!}</textarea>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <input type="hidden" name="enc_id" value="{!!$enc_id!!}"/>
                  <button type="button" onclick="doEditRecord('{{route('estimations.update',$enc_id)}}','estimationForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
