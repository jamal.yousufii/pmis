<div id="district" class="errorDiv">
    <label class="title-custom">{{ trans('plans.district') }}: <span style="color:red;">*</span></label><br>
    <select class="form-control m-input m-input--air select-2" name="district" id="district_id_{{$counter}}" onchange="bringVillages('district_id_{{$counter}}','village_div_{{$counter}}',{{$counter}})" style="width:100%;">
        <option value="">{{ trans('global.select') }}</option>
        @if($types)
            @foreach($types as $type)
                <option value="{!!$type->id!!}">{!!$type->name!!}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="district error-div" style="display:none;"></div>
<script type="text/javascript">
    $(".select-2").select2();
</script>
