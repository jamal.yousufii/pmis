@extends('master')
@section('head')
  <title>{{ trans('plans.plan') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile" id="">
                <div class="m-portlet__head table-responsive">
                    <!-- Include Summary Modal -->
                    @include('pmis.summary.summary_modal')
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_add") and empty($record))
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" onclick="addRecord('{{route('plans.create')}}','req_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('plans.add') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        @if($record)
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{$record->name}}</h3>
                            </div>
                        @endif
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            @if(!empty($record))
                                <!-- Include Attachments Modal -->
                                @include('pmis.attachments.modal')
                                 <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&record_id={{encrypt($record->id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","view_pro_summary"))
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                    </a>
                                </li>
                            @endif
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('plan',session('current_department')) }}">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if($record)
                    <!-- Include Project location Add Modal -->
                    @include('pmis.plans.location_add')
                    <div class="m-portlet__head"> 
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('plans.view') }}</h3>
                            </div> 
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_add") and !empty($record) and !$shared)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="" data-toggle="modal" data-target="#AddLocationModal">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('plans.add_location') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_edit") and !$shared)
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" onclick="editRecord('{{route('plans.edit',encrypt($record->id))}}','','GET','response_div')" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm ">
                                            <span><i class="la la-edit"></i><span>{{ trans('plans.edit_plan') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                <li class="m-portlet__nav-item">
                                    @if($shared and count($location)>0)
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    @elseif(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_share") and count($location)>0)
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Include sharing modal -->
                    @include('pmis.plans.share_modal')
                    @alert()
                    @endalert
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="ajax_response">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.status') }}: </label><br>
                                    <span>{{$record->static_data['name_dr']}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.name') }}: </label><br>
                                    <span>{{$record->name}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.code') }}: </label><br>
                                    <span>{{$record->code}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.proj_category') }}: </label><br>
                                    <span>{{ $record->category->{'name_'.$lang} }}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3" id="ptype_div">
                                    <label class="title-custom">{{ trans('plans.project_type') }}: </label><br>
                                    <span>{{ $record->project_type->{'name_'.$lang} }}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.year') }}: </label><br>
                                    <span>{{yearCheck($record->year,$lang)}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.project_start_date') }}: </label><br>
                                    <span>{{dateCheck($record->project_start_date,$lang)}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.project_end_date') }}: </label><br>
                                    <span>{{dateCheck($record->project_end_date,$lang)}}</span>
                                </div>  
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                    <label class="title-custom">{{ trans('plans.goals') }}: </label><br>
                                    <span>{!! $record->goal !!}</span>
                                </div>
                                <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                    <label class="title-custom">{{ trans('plans.description') }}: </label><br>
                                    <span>{!! $record->description !!}</span>
                                </div>
                            </div>
                        </div>
                        <!-- Display Project Details -->
                        @if($location)
                            <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
                                <div class="m-portlet__head-tools">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">{{ trans('plans.view_location') }}</h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">
                                            <ul class="m-portlet__nav">
                                                <li class="m-portlet__nav-item">
                                                    <a class="btn btn-secondary btn-sm  pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                                <div class="m-portlet__body table-responsive">
                                    <div class="px-1" id="details-content">
                                        @foreach($location as $sub)
                                            <table class="table table-checkable head-has-color p-0" id="detail_content_{{$sub->id}}">
                                                @if($loop->first)
                                                    <thead>
                                                        <tr>
                                                            <th width="15%">{{ trans('plans.province') }}</th>
                                                            <th width="25%">{{ trans('plans.district') }}</th>
                                                            <th width="25%">{{ trans('plans.village') }}</th>
                                                            <th width="25%">{{ trans('plans.location') }}</th>
                                                            <th width="10%">{{ trans('global.action') }}</th>
                                                        </tr>
                                                    </thead>
                                                @endif
                                                <tbody>  
                                                    <tr>
                                                        <td width="15%">{{ $sub->province->{'name_'.$lang} }}</td>
                                                        <td width="25%">@if($sub->district_id){{ $sub->district->{'name_'.$lang} }} @endif</td>
                                                        <td width="25%">@if($sub->village_id) {{ $sub->village->{'name_'.$lang} }} @endif</td>
                                                        <td width="25%">@if($sub->latitude) {{ $sub->latitude }} @endif @if($sub->longitude), {{ $sub->longitude }} @endif</td>
                                                        <td width="10%">
                                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_edit") and !$shared)
                                                                <span class="dtr-data">
                                                                    <span class="dropdown">
                                                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                                                            <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$sub->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$sub->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                                                        </div>
                                                                    </span>
                                                                </span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse editDiv" id="EditDivDetails-{{$sub->id}}" style="border:1px solid #c0c4cc">
                                                <!-- Include Project location Edit Modal-->
                                                @include('pmis.plans.location_edit')   
                                            </div>
                                        @endforeach 
                                        <!-- Pagination -->
                                        <div class="px-3">
                                        @if(!empty($location))
                                            {!!$location->links('pagination')!!}
                                        @endif
                                        </div>           
                                    </div>         
                                </div>
                            </div>
                        @endif
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('js-code')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination a').on('click', function(event) {
                event.preventDefault();
                if ($(this).attr('href') != '#') {
                    document.cookie = "no="+$(this).text();
                    var dataString = '';
                    dataString += "&page="+$(this).attr('id')+"&ajax="+1;
                    $.ajax({
                        url: '{{ route("plans.show",$enc_id) }}',
                        data: dataString,
                        type: 'get',
                        beforeSend: function(){
                            $('#ajax_response').html('<span style="position:relative;left:10%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                        },
                        success: function(response)
                        {
                            $('#ajax_response').html(response);
                        }
                    });
                }
            });
        });
    </script>
@endsection
