<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('plans.add_plan') }}</h3>
            <!-- @include('breadcrumb_nav') -->
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="planForm" method="post" enctype="multipart/form-data">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.status') }}: <span style="color:red;">*</span></label>
              <div id="div_status" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" name="status" required id="status">
                  <option value="">{{ trans('global.select') }}</option>
                  @if($status)
                    @foreach($status as $item)
                      <option value="{{$item->code}}">{{ $item->{'name_'.$lang} }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="status error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.name') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv required" type="text" name="name" id="name" required>
              <div class="name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.code') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input required errorDiv" type="text" name="code" id="code" required>
              <div class="code error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.proj_category') }}: <span style="color:red;">*</span></label>
              <div id="div_category_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" name="category" id="category_id" onchange="bringTypes();" required>
  					      <option value="">{{ trans('global.select') }}</option>
                  @if($categories)
                    @foreach($categories as $cat)
                      <option value="{{$cat->id}}">{{ $cat->{'name_'.$lang} }}</option>
                    @endforeach
                  @endif
  				      </select>
              </div>
              <div class="category_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4" id="ptype_div">
              <label class="title-custom">{{ trans('plans.project_type') }}: <span style="color:red;">*</span></label>
              <div id="div_type" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" name="type" id="type">
  				        <option value="">{{ trans('global.select') }}</option>
                </select>
              </div>
              <div class="type error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.year') }}: </label><br>
              <select class="form-control m-input m-input--air select-2" name="year" id="year">
                <option>{{ trans('global.select') }}</option>
                @if($year)
                  @foreach($year as $key=>$item)
                    <option value="{{$key}}">{{$item}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.project_start_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker required errorDiv" type="text" name="project_start_date" id="project_start_date">
              <div class="project_start_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.project_end_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker required errorDiv" type="text" name="project_end_date" id="project_end_date">
              <div class="project_end_date error-div" style="display:none;"></div>
            </div>  
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.goals') }}: </label>
              <textarea class="form-control m-input m-input--air tinymce" name="goals" rows="4"></textarea>
              <span class="m-form__help">{{ trans('plans.enter_goals') }}</span>
            </div>
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.description') }}: </label>
              <textarea class="form-control m-input m-input--air tinymce" name="description" rows="4"></textarea>
              <span class="m-form__help">{{ trans('plans.enter_desc') }}</span>
            </div>
          </div>
          <div id="targetDiv"></div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('plans.store')}}','planForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="enc_req" value="{{$enc_req}}"/>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
