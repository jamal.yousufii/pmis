<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="ajax_response">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.status') }}: </label><br>
                <span>{{$record->static_data['name_dr']}}</span>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.name') }}: </label><br>
                <span>{{$record->name}}</span>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.code') }}: </label><br>
                <span>{{$record->code}}</span>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.proj_category') }}: </label><br>
                <span>{{ $record->category->{'name_'.$lang} }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3" id="ptype_div">
                <label class="title-custom">{{ trans('plans.project_type') }}: </label><br>
                <span>{{ $record->project_type->{'name_'.$lang} }}</span>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.year') }}: </label><br>
                <span>{{$record->year}}</span>
            </div>
            <div class="col-lg-3">
                <label class="title-custom">{{ trans('plans.project_start_date') }}: </label><br>
                <span>{{$record->project_start_date}}</span>
            </div>
            <div class="col-lg-3">view
                <label class="title-custom">{{ trans('plans.project_end_date') }}: </label><br>
                <span>{{$record->project_end_date}}</span>
            </div>  
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <!-- Include Attachments Modal -->    <div class="col-lg-6">
                @include('pmis.attachments.modal')        <label class="title-custom">{{ trans('plans.goals') }}: </label><br>
                <span>{!!$record->goal!!}</span>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('plans.description') }}: </label><br>
                <span>{!!$record->description!!}</span>
            </div>
        </div>
    </div>
    <!-- Display Project Details -->
    @if($location)
        <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
            <div class="m-portlet__head-tools">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('plans.view_location') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary btn-sm  pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body table-responsive">
                <div class="px-1" id="details-content">
                    @foreach($location as $sub)
                        <table class="table table-checkable head-has-color p-0" id="detail_content_{{$sub->id}}">
                            @if($loop->first)
                                <thead>
                                    <tr>
                                        <th width="15%">{{ trans('plans.province') }}</th>
                                        <th width="25%">{{ trans('plans.district') }}</th>
                                        <th width="25%">{{ trans('plans.village') }}</th>
                                        <th width="25%">{{ trans('plans.location') }}</th>
                                        <th width="10%">{{ trans('global.action') }}</th>
                                    </tr>
                                </thead>
                            @endif
                            <tbody>  
                                <tr>
                                    <td width="15%">{{ $sub->province->{'name_'.$lang} }}</td>
                                    <td width="25%">@if($sub->district_id){{ $sub->district->{'name_'.$lang} }} @endif</td>
                                    <td width="25%">@if($sub->village_id) {{ $sub->village->{'name_'.$lang} }} @endif</td>
                                    <td width="25%">@if($sub->latitude) {{ $sub->latitude }} @endif @if($sub->longitude), {{ $sub->longitude }} @endif</td>
                                    <td width="10%">
                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_edit") and !$shared)
                                            <span class="dtr-data">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                                        <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$sub->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$sub->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                                    </div>
                                                </span>
                                            </span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse editDiv" id="EditDivDetails-{{$sub->id}}" style="border:1px solid #c0c4cc">
                            <!-- Include Project location Edit Modal-->
                            @include('pmis.plans.location_edit')   
                        </div>
                    @endforeach 
                    <!-- Pagination -->
                    <div class="px-3">
                        @if(!empty($location))
                            {!!$location->links('pagination')!!}
                        @endif
                    </div>           
                </div>    
            </div>
        </div>
    @endif
</div>
       
<script type="text/javascript">
    $(document).ready(function() {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if ($(this).attr('href') != '#') {
            document.cookie = "no="+$(this).text();
                var dataString = '';
                dataString += "&page="+$(this).attr('id')+"&ajax="+1;
                $.ajax({
                    url: '{{ route("plans.show",$enc_id) }}',
                    data: dataString,
                    type: 'get',
                    beforeSend: function(){
                        $('#ajax_response').html('<span style="position:relative;left:10%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                    },
                    success: function(response)
                    {
                        $('#ajax_response').html(response);
                    }
            });
            }
        });
    });
</script>
