@foreach($location as $sub)
    <table class="table table-striped-">
        <tbody>  
            <tr>
                <td width="15%">{{ $sub->province->{'name_'.$lang} }}</td>
                <td width="25%">@if($sub->district_id){{ $sub->district->{'name_'.$lang} }} @endif</td>
                <td width="25%">@if($sub->village_id) {{ $sub->village->{'name_'.$lang} }} @endif</td>
                <td width="25%">{{ $sub->latitude }}, {{ $sub->longitude }}</td>
                <td width="10%">
                    <span class="dtr-data">
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_view"))
                                    <span class="dropdown-item btn btn-secondary btn-dark" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$sub->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$sub->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</span>
                                @endif
                            </div>
                        </span>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse editDiv" id="EditDivDetails-{{$sub->id}}" style="border:1px solid #ebedf2">
        <!-- Include Project location Edit Modal-->
        @include('pmis.plans.location_edit')   
    </div>
    <div id="msgDiv-{{$sub->id}}" class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
        @if (Session::has('success_detail'))
            <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-check-square"></i></div>
                <div class="m-alert__text">{!! Session::get('success_detail') !!}</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
        @elseif (Session::has('fail_detail'))
            <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-warning"></i></div>
                <div class="m-alert__text">{!! Session::get('fail_detail') !!}</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
        @endif
    </div>
@endforeach