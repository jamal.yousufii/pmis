<label class="title-custom">{{ trans('plans.project_type') }}: <span style="color:red;">*</span></label>
<div id="type" class="errorDiv">
  <?php //echo "<pre>"; print_r($types); exit; ?>
  <select class="form-control m-input m-input--air select-2" name="type" required>
    <option value="">{{ trans('global.select') }}</option>
      @foreach($types as $type)
        <option value="{!!$type->id!!}">{!!$type->name!!}</option>
      @endforeach
  </select>
</div>
<div class="type error-div" style="display:none;"></div>

<script type="text/javascript">
  $(".select-2").select2();
</script>
