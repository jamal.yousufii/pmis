<!--begin::Form-->
<?php
// Get Districts & Villages
$districts = getAllDistrictsByProvince($sub->province_id);
$villages  = getAllVillagesByDistrict($sub->district_id);
?>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="locationForm_{{$sub->id}}" method="post"> 
    <table class="table">
        <tr>
            <td width="50%">
                <div id="province" class="errorDiv">
                    <label class="title-custom">{{ trans('plans.province') }}: <span style="color:red;">*</span></label><br>
                    <select class="form-control m-input input-lg m-input--air select-2" name="province" id="province_id_{{$sub->id}}" style="width:100%;" onchange="bringDistricts('province_id_{{$sub->id}}','district_div_{{$sub->id}}',{{$sub->id}},'village_{{$sub->id}}');">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($provinces)
                            @foreach($provinces as $pro)
                                @if($pro->id==$sub->province_id)
                                    <option value="{!!$pro->id!!}" selected="selected">{!!$pro->name!!}</option>
                                @else
                                    <option value="{!!$pro->id!!}">{!!$pro->name!!}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="province error-div" style="display:none;"></div>
            </td>
            <td width="50%">
                <div id="district_div_{{$sub->id}}">
                    <label class="title-custom">{{ trans('plans.district') }}: <span style="color:red;">*</span></label><br>
                    <select class="form-control m-input m-input--air select-2" name="district" id="district_id_{{$sub->id}}" style="width:100%;" onchange="bringVillages('district_id_{{$sub->id}}','village_div_{{$sub->id}}',{{$sub->id}})">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($districts)
                            @foreach($districts as $dis)
                                @if($dis->id==$sub->district_id)
                                    <option value="{!!$dis->id!!}" selected="selected">{!!$dis->name!!}</option>
                                @else
                                    <option value="{!!$dis->id!!}">{!!$dis->name!!}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div id="village_div_{{$sub->id}}">
                    <label class="title-custom">{{ trans('plans.village') }}: <span style="color:red;">*</span></label><br>
                    <select class="form-control m-input m-input--air select-2" name="village" id="village_{{$sub->id}}" style="width:100%;">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($villages)
                            @foreach($villages as $vil)
                                @if($vil->id==$sub->village_id)
                                    <option value="{!!$vil->id!!}" selected="selected">{!!$vil->name!!}</option>
                                @else
                                    <option value="{!!$vil->id!!}">{!!$vil->name!!}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </td>
            <td width="50%">
                <label class="title-custom">{{ trans('plans.latitude') }}: </label><br>
                <input class="form-control m-input" type="number" name="latitude" id="latitude" min="0" style="width:100%;" value="{{$sub->latitude}}">
            </td>
        </tr>
        <tr>
            <td width="50%">
                <label class="title-custom">{{ trans('plans.longitude') }}: </label><br>
                <input class="form-control m-input" type="number" name="longitude" id="longitude" min="0" style="width:100%;" value="{{$sub->longitude}}">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <input type="hidden" name="enc_id" value="{{ encrypt($sub->id) }}" />
                <button type="button" class="btn btn-primary btn-sm" onclick="update_location('locationForm_{{$sub->id}}','detail_content_{{$sub->id}}','EditDivDetails-{{$sub->id}}','msgDiv-{{$sub->id}}')">{{ trans('global.submit') }}</button>
                <button type="button" class="btn btn-secondary btn-sm" id="collapsBtn" data-toggle="collapse" href="#EditDivDetails-{{$sub->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$sub->id}}">{{ trans('global.cancel') }}</button>  
            </td>
        </tr>
    </table>
</form>
<!--end::Form-->
<script type="text/javascript">
    function update_location(form_id,response_div,editDiv,msgDiv)
    {
        $('.error-div').html('');
        $('.errorDiv').css('border-bottom','1px solid #ebedf2');
        var formData = new FormData($("#"+form_id)[0]);
        $.ajax({
            url: '{{ route("update_location") }}',
            data: formData,
            contentType: false,
            type: 'post',
            success: function(response)
            {
                $('#'+editDiv).remove();
                $('#'+response_div).html(response);
                setTimeout(function() {
                    $('#'+msgDiv).remove();
                }, 2500);
            },
            error: function (request, status, error) {
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('.'+key).show();
                    $('.'+key).html('<span class="text-danger">'+value+'</span>');
                    $('#'+key).css('border-bottom','1px solid #dc3545');
                });
            },
            cache: false,
            processData: false
        });
    }
</script>