<!-- Location Start-->
<div id="AddLocationModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head bg-color-dark">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ trans('plans.add_location') }}</h3>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
      </div>
      <div class="m-portlet__body">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="locationForm" method="post"> 
          <table class="table ">
            <tr>
              <td width="50%">
                <label class="title-custom">{{ trans('plans.province') }}: <span style="color:red;">*</span></label>
                <div id="province" class="errorDiv">
                  <select class="form-control m-input input-lg m-input--air select-2" name="province" id="province_id_1" style="width:100%;" onchange="bringDistricts('province_id_1','district_div_1',1,'village_id_1');">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($provinces)
                      @foreach($provinces as $pro)
                        <option value="{!!$pro->id!!}">{!!$pro->name!!}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="province error-div" style="display:none;"></div>
              </td>
              <td width="50%">
                <label class="title-custom">{{ trans('plans.district') }}: <span style="color:red;">*</span></label>
                <div id="district_div_1">
                    <div id="district" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="district" id="district_id_1" style="width:100%;" onchange="bringVillages('district_id_1','village_div_1',1)">
                            <option value="">{{ trans('global.select') }}</option>
                        </select>
                    </div>
                    <div class="district error-div" style="display:none;"></div>
                </div>
              </td>
            </tr>  
            <tr>
              <td width="50%">
                <label class="title-custom">{{ trans('plans.village') }}: <span style="color:red;">*</span></label>
                <div id="village_div_1">
                    <div id="village" class="errorDiv">
                        <select class="form-control m-input m-input--air select-2" name="village" id="village_id_1" style="width:100%;">
                            <option value="">{{ trans('global.select') }}</option>
                        </select>
                    </div>
                    <div class="village error-div" style="display:none;"></div>
                </div>
              </td>
              <td  width="50%">
                <label class="title-custom">{{ trans('plans.latitude') }}: </label>
                <input class="form-control m-input" type="number" name="latitude" id="latitude" style="width:100%;">
              </td>
            </tr>
            <tr>
              <td width="50%">
                <label class="title-custom">{{ trans('plans.longitude') }}: </label>
                <input class="form-control m-input" type="number" name="longitude" id="longitude" style="width:100%;">
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <input type="hidden" name="enc_id" value="{!!encrypt($record->id)!!}"/>
                <button type="button" onclick="store_location()" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                <button type="button" data-dismiss="modal" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>  
              </td>
            </tr>
          </table>
        </form>
        <!--end::Form-->
      </div>    
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $(".select-2").select2();
  });
  function store_location()
  {
    $('.error-div').html('');
    $('.errorDiv').css('border-bottom','1px solid #ebedf2');
    var formData = new FormData($("#locationForm")[0]);
    $.ajax({
        url: '{{ route("store_location") }}',
        data: formData,
        contentType: false,
        type: 'post',
        beforeSend: function()
        {
          // disable submit button
          $('#add').attr('disabled','disabled');
        },
        success: function(response)
        {
          // enable submit button
          $('#add').removeAttr('disabled');
          redirectFunction();
        },
        error: function (request, status, error) {
          $('#add').removeAttr('disabled');
          json = $.parseJSON(request.responseText);
          $.each(json.errors, function(key, value){
              $('.'+key).show();
              $('.'+key).html('<span class="text-danger">'+value+'</span>');
              $('#'+key).css('border-bottom','1px solid #dc3545');
          });
        },
        cache: false,
        processData: false
    });
  }
</script>
