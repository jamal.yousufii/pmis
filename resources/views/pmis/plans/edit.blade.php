<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('plans.edit_plan') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(!empty($record))
                <!-- Include Attachments Modal -->
                @include('pmis.attachments.modal')
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#AttachmentModal">
                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                    </a>
                </li>
            @endif
            @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="planForm" method="PUT" enctype="multipart/form-data">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.status') }}: <span style="color:red;">*</span></label>
              <div id="status" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="status" required>
                  <option value="">{{ trans('global.select') }}</option>
                  @if($status)
                    @foreach($status as $item)
                      @if($item->id==$record->status)
                        <option value="{{$item->code}}" selected="selected">{{ $item->{'name_'.$lang} }}</option>
                      @else
                        <option value="{{$item->code}}">{{ $item->{'name_'.$lang} }}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="status error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.name') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name" id="name" value="{{$record->name}}" required>
              <div class="name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.code') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="code" id="code" value="{{$record->code}}" required>
              <div class="code error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.proj_category') }}: <span style="color:red;">*</span></label>
              <div id="category" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="category" onchange="bringTypes();" required>
  					      <option value="">{{ trans('global.select') }}</option>
                  @if($categories)
                    @foreach($categories as $item)
                      @if($item->id==$record->category_id)
                        <option value="{{$item->id}}" selected="selected">{{ $item->{'name_'.$lang} }}</option>
                      @else
                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                      @endif
                    @endforeach
                  @endif
  				      </select>
              </div>
              <div class="category error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4" id="ptype_div">
              <label class="title-custom">{{ trans('plans.project_type') }}: <span style="color:red;">*</span></label>
              <div id="type" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="type" required>
                  <option value="">{{ trans('global.select') }}</option>
                  @if($project_types)
                    @foreach($project_types as $item)
                      @if($item->id==$record->project_type_id)
                        <option value="{{$item->id}}" selected="selected">{{ $item->name }}</option>
                      @else
                        <option value="{{$item->id}}">{{ $item->name }}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="type error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
              <label class="title-custom">{{ trans('plans.year') }}: </label><br>
              <select class="form-control m-input m-input--air select-2" name="year" id="year">
                <option value="">{{ trans('global.select') }}</option>
                @if($year)
                  @foreach($year as $key=>$item)
                    @if($key==$record->year)
					    <option value="{!!$key!!}" selected="selected">{!!$item!!}</option>
                    @else
                        <option value="{!!$key!!}">{!!$item!!}</option>
                    @endif
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.project_start_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker errorDiv" type="text" name="project_start_date" id="project_start_date" value="{!!dateCheck($record->project_start_date,$lang)!!}" required>
              <div class="project_start_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.project_end_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker errorDiv" type="text" name="project_end_date" id="project_end_date" value="{!!dateCheck($record->project_end_date,$lang)!!}" required>
              <div class="project_end_date error-div" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.goals') }}: </label>
              <textarea class="form-control m-input m-input--air tinymce" name="goals" rows="4">{{$record->goal}}</textarea>
              <span class="m-form__help">{{ trans('plans.enter_goals') }}</span>
            </div>
            <div class="col-lg-6">
              <label class="title-custom">{{ trans('plans.description') }}: </label>
              <textarea class="form-control m-input m-input--air tinymce" name="description" rows="4">{{$record->description}}</textarea>
              <span class="m-form__help">{{ trans('plans.enter_desc') }}</span>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="doEditRecord('{{route('plans.update',$enc_id)}}','planForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
