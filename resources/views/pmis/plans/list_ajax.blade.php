<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead class="bg-light">
    <tr>
      <th width="15%">{{ trans('global.req_urn') }}</th>
      <th width="13%">{{ trans('global.pro_urn') }}</th>
      <th width="20%">{{ trans('plans.project_name') }}</th>
      <th width="10%">{{ trans('plans.project_code') }}</th>
      <th width="15%">{{ trans('global.shared_from') }}</th>
      <th width="7%">{{ trans('global.status') }}</th>
      <th width="12%">{{ trans('global.section_owner') }}</th>
      <th width="8%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
  @if($records)
    @foreach($records AS $rec)
      <?php $enc_id = encrypt($rec->id); ?>
      <tr @if(!isset($rec->project['id'])) style="color:red;" @endif >
        <td>{{ $rec->urn }}</td>
        <td>@if(isset($rec->project['urn'])) {{ $rec->project['urn'] }} @endif</td>
        <td>@if(isset($rec->project['name'])) {{ $rec->project['name'] }} @endif</td>
        <td>@if(isset($rec->project['code'])) {{ $rec->project['code'] }} @endif</td>
        <td>{{ $rec->share()->where('share_to_code','pmis_plan')->first()->from_department->{'name_'.$lang} }}</td>
        <td>
          @if(isset($rec->project['process']) and $rec->project['process']=='0')
            <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
          @elseif(isset($rec->project['process']) and $rec->project['process']=='1')
            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
          @else
            <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
          @endif
        </td>
        <td>
            @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_owner"))
                <button type="button" onclick="add_owner({{ $rec->id }},{{ $rec->urn }})" data-toggle="modal" data-target="#assign_project" id="add_owner_btn" class="btn btn-circle btn-icon btn-sm btn-info mx-2">
                    <i class="fa fa-plus-circle"></i>
                </button>
                @endif
                @if($rec->project_user()->where('section','pmis_plan')->first())
                <button type="button" onclick="list_owner('{{route('plans.list_assigned_users', $rec->id)}}', 'modal_content')" data-toggle="modal" data-target="#project_users" id="list_owner_btn" class="btn btn-circle btn-icon btn-sm mx-2 btn-success">
                    <i class="fa fa-users"></i>
                </button>
            @endif
        </td>
        <td>
          <span class="dtr-data">
            <span class="dropdown">
              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_plan","pla_view"))
                  <a class="dropdown-item" href="{{ route('plans.show',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                @endif
              </div>
            </span>
          </span>
        </td>
      </tr>
    @endforeach
  @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>
