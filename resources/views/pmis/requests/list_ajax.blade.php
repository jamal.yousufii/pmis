<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead class="bg-light">
    <tr class="font-title">
      <th width="12%">{{ trans('global.urn') }}</th>
      <th width="25%">{{ trans('requests.department') }}</th>
      <th width="15%">{{ trans('requests.req_date') }}</th>
      <th width="15%">{{ trans('requests.req_number') }}</th>
      <th width="15%">{{ trans('requests.req_owner') }}</th>
      <th width="10%">{{ trans('global.status') }}</th>
      <th width="8%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
  @if($records)
    @foreach($records AS $rec)
      <?php $enc_id = encrypt($rec->id); ?>
      <tr>
        <td>{!! $rec->urn !!}</td>
        <td>{!! $rec->hr_departments['name_'.$lang] !!}</td>
        <td>{!! dateCheck($rec->request_date,$lang) !!}</td>
        <td>{!! $rec->doc_number !!}</td>
        <td>{!! $rec->users['name'] !!}</td>
        <td>
          @if($rec->operation==0 and $rec->doc_type==6)
              <span class="m-badge bg-info m-badge--wide text-white">{{ trans('requests.verbal') }}</span>  
          @elseif($rec->operation==0)
              <span class="m-badge bg-warning m-badge--wide text-white">{{ trans('requests.pending') }}</span>
          @elseif($rec->operation==1)
              <span class="m-badge bg-success m-badge--wide text-white">{{ trans('requests.approved') }}</span>
          @elseif($rec->operation==2)
              <span class="m-badge bg-danger m-badge--wide text-white">{{ trans('requests.rejected') }}</span>
          @endif
        </td>
        <td>
          <span class="dtr-data">
            <span class="dropdown">
              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_dep_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_all_view"))
                  <a class="dropdown-item" href="{{route('requests.show',$enc_id)}}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                @endif
                @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_delete") and $rec->operation==0 and $rec->doc_type!=6)
                  <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('requests.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                @endif
              </div>
            </span>
          </span>
        </td>
      </tr>
    @endforeach
  @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>
