<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('requests.edit') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="requestForm" method="post" enctype="multipart/form-data">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3">
              <label class="title-custom">{{ trans('requests.doc_type') }}: <span style="color:red;">*</span></label>
              <div id="doc_type" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="doc_type" required>
                  <option value="">{{ trans('requests.select') }}</option>
                  @if($types)
                    @foreach($types as $type)
  								    <option value="{!!$type->id!!}" @if($record->doc_type==$type->id) selected="selected" @endif>{!!$type->name!!}</option>
                    @endforeach
                  @endif
  							</select>
              </div>
              <div class="doc_type error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
              <label class="title-custom">{{ trans('requests.department') }}:</label>
              <div id="department_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" name="department_id" required>
                  <option value="">{{ trans('requests.select') }}</option>
                  @if($deps)
                    @foreach($deps as $dep)
  								    <option value="{!!$dep->id!!}" @if($record->department_id==$dep->id) selected="selected" @endif>{!!$dep->name!!}</option>
                    @endforeach
                  @endif
  							</select>
              </div>
              <div class="department_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
              <label class="title-custom">{{ trans('requests.req_date') }}: <span style="color:red;">*</span></label>
              <input class="form-control m-input datePicker errorDiv" type="text" value="{!!dateCheck($record->request_date,$lang)!!}" name="request_date" required>
              <div class="request_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
              <label class="title-custom">{{ trans('requests.req_number') }}:</label>
              <input class="form-control m-input" type="number" value="{!!$record->doc_number!!}" name="request_number">
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('requests.goals') }}:</label>
              <textarea class="form-control m-input tinymce m-input--air" name="goals" rows="3">{!! $record->goals !!}</textarea>
              <span class="m-form__help">{{ trans('requests.goals_note') }}</span>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('requests.description') }}:</label>
              <textarea class="form-control m-input m-input--air tinymce" name="description" rows="3">{!!$record->description!!}</textarea>
              <span class="m-form__help">{{ trans('requests.desc_note') }}</span>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="doEditRecord('{{route('requests.update',$enc_id)}}','requestForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="enc_id" value="{!!$enc_id!!}"/>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>