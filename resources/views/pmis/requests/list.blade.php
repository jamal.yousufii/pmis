@extends('master')
@section('head')
  <title>{{ trans('requests.request') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <!-- <h3 class="m-portlet__head-text">{{trans('requests.list')}}</h3> -->
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_add"))
            <li class="m-portlet__nav-item">
              <a href="javascript:void()" onclick="addRecord('{{route('requests.create')}}','','GET','response_div')" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                <span><i class="la la-cart-plus"></i><span>{{ trans('requests.add') }}</span></span>
              </a>
            </li>
          @endif
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="field" id="field" style="width:100%;" onchange="bringSearchFiel('{{route('search.option')}}','POST',this.value,'value_input')">
                <option value="">{{ trans('global.select') }}</option>
                <option value="urn">{{ trans('global.urn') }}</option>
                <option value="department_id">{{ trans('requests.department') }}</option>
                <option value="request_date">{{ trans('requests.req_date') }} </option>
                <option value="doc_number">{{ trans('requests.req_number') }} </option>
                <option value="process">{{ trans('global.status') }} </option>
              </select>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="condition" id="condition" style="width:100%;">
                <option value="=">{{ trans('global.equail') }}</option>
                <option value="like">{{ trans('global.like') }} </option>
              </select>
            </div>
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12" id="value_input">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" name="value" class="form-control m-input" placeholder="{{trans('global.search_dote')}}" id="search_value">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span><i class="la la-search"></i></span>
                </span>
             </div>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <a href="javascript:void;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air" onclick="searchRecords('{{route('filterRequest')}}','search','POST','searchresult')">
                <span><i class="la la-search"></i><span>{{trans('global.search')}}</span></span>
              </a>
            </div>
          </div>
        </div>
      </form>  
    </div>
    <!-- Filter End -->
    @alert()
    @endalert
    <div class="m-portlet__body table-responsive" id="searchresult"> 
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead class="bg-light">
          <tr class="font-title">
            <th width="12%">{{ trans('global.urn') }}</th>
            <th width="25%">{{ trans('requests.department') }}</th>
            <th width="15%">{{ trans('requests.req_date') }}</th>
            <th width="15%">{{ trans('requests.req_number') }}</th>
            <th width="15%">{{ trans('requests.req_owner') }}</th>
            <th width="10%">{{ trans('global.status') }}</th>
            <th width="8%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <?php $enc_id = encrypt($rec->id); ?>
              <tr>
                <td>{!! $rec->urn !!}</td>
                <td>{!! $rec->hr_departments['name_'.$lang] !!}</td>
                <td>{!! dateCheck($rec->request_date,$lang) !!}</td>
                <td>{!! $rec->doc_number !!}</td>
                <td>{!! $rec->users['name'] !!}</td>
                <td>
                  @if($rec->operation==0 and $rec->doc_type==6)
                      <span class="m-badge bg-info m-badge--wide text-white">{{ trans('requests.verbal') }}</span>  
                  @elseif($rec->operation==0)
                    <span class="m-badge bg-warning m-badge--wide text-white">{{ trans('requests.pending') }}</span>
                  @elseif($rec->operation==1)
                    <span class="m-badge bg-success m-badge--wide text-white">{{ trans('requests.approved') }}</span>
                  @elseif($rec->operation==2)
                    <span class="m-badge bg-danger m-badge--wide text-white">{{ trans('requests.rejected') }}</span>
                  @endif
                </td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_dep_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_all_view"))
                          <a class="dropdown-item" href="{{route('requests.show',$enc_id)}}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                        @endif
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_delete") and $rec->operation==0 and $rec->doc_type!=6)
                          <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('requests.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection
