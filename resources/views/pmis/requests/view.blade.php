@extends('master')
@section('head')
  <title>{{ trans('requests.request') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--full-height ">
			<div class="m-portlet__head table-responsive">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">{{ trans('requests.view') }}</h3>
					</div>
				</div>
        <!-- Include Attachments Modal -->
        @include('pmis.attachments.modal')
        @include('pmis.requests.approval_view')
        <!-- Include Approval Modal -->
        @include('pmis.requests.approval_edit')
        <!-- Include Summary Modal -->
        @include('pmis.summary.summary_modal')
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_approval") AND $record->operation==0 AND $record->completed==1 AND $record->doc_type!=6)
              <li class="m-portlet__nav-item">
                <a class="btn btn-success m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#ApprovalEditModal">
                  <span><i class="fa fa-folder-open"></i> <span>{{ trans('requests.update_status') }}</span></span>
                </a>
              </li>
            @elseif($record->operation!=0 AND $record->doc_type!=6)
              <li class="m-portlet__nav-item">
                <a class="btn btn-success m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#ApprovalViewModal">
                  <span><i class="fa fa-folder-open"></i> <span>{{ trans('requests.view_status') }}</span></span>
                </a>
              </li>
            @endif
            @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_edit") and $record->operation==0 and !$shared)
              {{-- Edit Button  --}}
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="javascript:void()" onclick="editRecord('{{route('requests.edit',encrypt($record->id))}}','','GET','response_div')">
                  <i class="la la-edit"></i> {{ trans('global.edit') }}
                </a>
              </li>
            @endif
            <li class="m-portlet__nav-item">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&record_id={{encrypt($record->id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
              </a>
            </li>
            @if($record->project and doIHaveRoleInDep(session('current_department'),"pmis_request","view_pro_summary"))
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                  <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
            @endif
            <li class="m-portlet__nav-item">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('request',session('current_department')) }}">
                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
              </a>
            </li>
          </ul>
				</div>
      </div>
      @if($record->completed==0 and $record->created_by==Auth::user()->id) <!-- Check if user is the owner of record -->
        <div class="m-portlet__head h-50 pt-2">
          <form enctype="multipart/form-data" id="completeForm" method="POST">
            <table>
              <td>
                <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                  <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                    <label><input type="checkbox" id="complete" name="complete" value="1" onclick="completeRecord('{{ route('completeRecords') }}','complete','completeForm','POST','response_div')"><span></span></label>
                  </span>
                </span>
              </td>
              <td><label class="title-custom">{{ trans('global.complete') }}</label></td>
            </table>
            <input type="hidden" name="table" value="requests" />
            <input type="hidden" name="rec_id" value="{{ encrypt($record->id) }}" />
            <input type="hidden" name="project_id" value="{{ encrypt($record->id) }}" />
          </form>
        </div>
      @endif
      @alert()
      @endalert
      <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3 col-md-3">
              <label class="title-custom">{{ trans('requests.doc_type') }} :</label><br>
              <span>{!!$record->documents['name_'.$lang]!!}</span>
            </div>
            <div class="col-lg-3 col-md-3">
              <label class="title-custom">{{ trans('requests.department') }} :</label><br>
              <span>{!!$record->hr_departments['name_'.$lang]!!}</span>
            </div>
            <div class="col-lg-3 col-md-3">
              <label class="title-custom">{{ trans('requests.req_date') }} :</label><br>
              <span>{!! dateCheck($record->request_date,$lang) !!}</span>
            </div>
            <div class="col-lg-3 col-md-3">
              <label class="title-custom">{{ trans('requests.req_number') }} :</label><br>
              <span>{!!$record->doc_number!!}</span>
            </div>
          </div>
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6 col-md-6">
              <label class="title-custom">{{ trans('requests.goals') }} :</label><br>
              <span>{!!$record->goals!!}</span>
            </div>
            <div class="col-lg-6 col-md-6">
              <label class="title-custom">{{ trans('requests.description') }} :</label><br>
              <span>{!!$record->description!!}</span>
            </div>
          </div>
        </div>
      </div>
      @if($record->operation==1 OR ($record->doc_type==6 && $record->completed==1))
        {{-- Include Share Modal  --}}
        @include('pmis.requests.share_modal')
      @endif
    </div>
@endsection