{{-- begin::Collpas Div --}}
<div class="m-portlet__head footer-button-box footer-button-box">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      @if($shared)
        <h3 class="m-portlet__head-text">{{ trans('global.share_title') }}</h3>
      @endif 
    </div>
  </div>
  <div class="m-portlet__head-tools">
    @if($shared)
      <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
        <span><i class="la la-arrows-v"></i><span>{{ trans('global.view') }}</span></span>
      </a>
    @elseif(doIHaveRoleInDep(session('current_department'),"pmis_request","req_share"))
      <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#request_share_modal" href="javascript:void()">
        <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
      </a>
    @endif
  </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseDiv">
  <div class="m-portlet">
    <div class="m-portlet__body table-responsive">
      <table class="table m-table m-table--head-separator-default">
        <thead>
            <tr class="bg-color-dark white-color">
                <th width="15%">{{trans('global.shared_to')}}</th>
                <th width="10%">{{trans('global.section')}}</th>
                <th width="8%">{{trans('global.doc_number')}}</th>
                <th width="10%">{{trans('global.doc_date')}}</th>
                <th width="25%">{{trans('global.description')}}</th>
                <th width="10%">{{trans('global.shared_by')}}</th>
                <th width="15%">{{trans('global.shared_date')}}</th>
                <th width="7%">{{trans('global.unshare')}}</th>
            </tr>
        </thead>
        <tbody>
            @if($shared)
                <tr>
                    <td>{{ $shared->department->{'name_'.$lang} }}</td>
                    <td>{{ $shared->section->{'name_'.$lang} }}</td>
                    <td>{{ $shared->doc_number }}</td>
                    <td>{{ dateCheck($shared->doc_date,$lang) }}</td>
                    <td>{{ $shared->description }}</td>
                    <td>{{ $shared->user->name }}</td>
                    <td>{{ dateCheck($shared->created_at,$lang,true) }}</td>
                    <td>
                        @if($record->project()->count()>0)
                          <i class="flaticon-delete"></i>
                        @elseif(doIHaveRoleInDep(session('current_department'),"pmis_request","req_unshare"))
                          <a href="javascript:void()" class="btn btn-warning btn-sm" onclick="unshare('{{ route("share.destroy", ['id'=>encrypt($shared->id),'rec_id'=>encrypt($record->id),'table'=>'requests','column'=>'id']) }}')">
                            <i class="flaticon-delete"></i>
                          </a> 
                        @else
                          <i class="flaticon-delete"></i> 
                        @endif
                    </td>
                </tr>
            @endif
        </tbody>
      </table>  
    </div>
  </div>
</div> 
{{-- End of Collpas div  --}}

{{-- Start of the Share Modal --}}
<div id="request_share_modal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div id="share_content">
        <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
          <!--begin::head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon"><i class="la la-leanpub"></i></span>
                <h3 class="m-portlet__head-text">{{trans('global.share')}}</h3>
              </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="closeBtn" aria-hidden="true">&times;</button>
          </div>
          <!--end::Head-->
          <form class="m-form m-form--fit m-form--label-align-right" id="sharing_form">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-6">
                  <label class="title-custom">{{ trans('global.section') }} : <span style="color:red;">*</span></label>
                  <div id="share_to_code" class="errorDiv">
                    <select class="form-control m-input m-input--air select-2" name="share_to_code" id="code" onchange="bringData(this.id,'{{ route('bringDepartments') }}','POST','share_to')">
                      <option value="">{{trans('global.select')}}</option>
                      @foreach($sections as $item)
                        <option value="{{$item->allowedSections->code}}">{{ $item->allowedSections->{'name_'.$lang} }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="share_to_code error-div" style="display:none;"></div>
                </div>
                <div class="col-lg-6">
                  <label class="title-custom">{{ trans('authentication.department') }} : <span style="color:red;">*</span></label>
                  <div id="share_to" class="errorDiv">
                    <select class="form-control m-input m-input--air select-2" name="share_to">
                      <option value="">{{trans('global.select')}}</option>
                    </select>
                  </div>
                  <div class="share_to error-div" style="display:none;"></div>
                </div>  
              </div>
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-6">
                  <label class="title-custom">{{ trans('global.doc_number') }}:</label>
                  <input class="form-control m-input" type="text" value="" name="doc_number" id="doc_number">
                </div>
                <div class="col-lg-6">
                  <label class="title-custom">{{ trans('global.doc_date') }}:</label>
                  <input class="form-control m-input datePicker" type="text" value="" name="doc_date" id="doc_date">
                </div>
              </div>
              <div class="form-group m-form__group">
                <label class="title-custom" for="request_share_desc">{{trans('global.description')}} :</label>
                <textarea class="form-control m-input" id="request_share_desc" name="description" rows="3"></textarea>
              </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
              <div class="m-form__actions align">
                {{-- Hiddend Inputs Start --}}
                <input type="hidden" name='share_from' value="{{decrypt(session('current_department'))}}">
                <input type="hidden" name='share_from_code' value="{{session('current_section')}}">
                <input type="hidden" name='table' value="requests">
                <input type="hidden" name='column' value="id">
                <input type="hidden" name='record_id' value="{{$record->id}}">
                {{-- Hiddend Inputs End --}}
                <button type="button" class="btn btn-primary" onclick="shareRecord('{{route('share.store')}}','sharing_form',this.id);" id="shareBtn">{{trans('requests.share')}}</button>
                <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
              </div>
            </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>
  </div>
</div>
{{-- End of the Share Modal  --}}