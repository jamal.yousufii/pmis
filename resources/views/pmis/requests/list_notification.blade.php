@extends('master')
@section('head')
  <title>{{ trans('requests.request') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="{{ route('requests') }}" class="m-nav__link btn btn-default m-btn m-btn--pill m-btn--custom btn-sm">
              <span><i class="fa fa-reply-all"></i> {{ trans('global.back') }}</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    @alert()
    @endalert
    <div class="m-portlet__body table-responsive" id="searchresult">
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr class="font-title">
            <th width="12%">{{ trans('global.urn') }}</th>
            <th width="25%">{{ trans('requests.department') }}</th>
            <th width="15%">{{ trans('requests.req_date') }}</th>
            <th width="15%">{{ trans('requests.req_number') }}</th>
            <th width="15%">{{ trans('requests.req_owner') }}</th>
            <th width="10%">{{ trans('global.status') }}</th>
            <th width="8%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
          @foreach($records AS $rec)
            <?php $enc_id = encrypt($rec->id); ?>
            <tr>
              <td>{!! $rec->urn !!}</td>
              <td>{!! $rec->hr_departments['name_'.$lang] !!}</td>
              <td>{!! dateCheck($rec->request_date,$lang) !!}</td>
              <td>{!! $rec->doc_number !!}</td>
              <td>{!! $rec->users['name'] !!}</td>
              <td>
                @if($rec->operation==0)
                  <span class="m-badge m-badge--warning m-badge--wide">{{ trans('requests.pending') }}</span>
                @elseif($rec->operation==1)
                  <span class="m-badge  m-badge--success m-badge--wide">{{ trans('requests.approved') }}</span>
                @elseif($rec->operation==2)
                  <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('requests.rejected') }}</span>
                @endif
              </td>
              <td>
                <span class="dtr-data">
                  <span class="dropdown">
                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                      @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_dep_view") OR doIHaveRoleInDep(session('current_department'),"pmis_request","pmis_request_all_view"))
                          <a class="dropdown-item" href="{{route('requests.show',$enc_id)}}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                      @endif
                      @if(doIHaveRoleInDep(session('current_department'),"pmis_request","req_delete") and $rec->operation==0)
                        <a class="dropdown-item" href="javascript:void()" onclick="destroy('{{route('requests.destroy',$enc_id)}}','','DELETE','response_div')"><i class="la la-trash"></i>{{ trans('global.delete') }}</a>
                      @endif
                    </div>
                  </span>
                </span>
              </td>
            </tr>
          @endforeach
        @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection
@section('footer')
@endsection
