@extends('master')
@section('head')
    <title>{{ trans('portfolios.portfolios') }}</title>
    <style>
        .m-widget21__title{
            font-size: 1rem !important;
        }
        .table thead th {
            font-size: 10px !important;
        }
        .opacity{
            opacity: 0.7 !important;
        }
        .opacity1{
            opacity: 0.8 !important;
            font-size: 1.5 rem !important;
            font-weight: 600;
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile mb-0">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">@include('breadcrumb_nav')</div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                         <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="py-2">    
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
            <div class="col-lg-6 p-0 pt-3">
                <select class="form-control m-input m-input--air select-2 required" name="portfolio_id" onchange="viewRecord('{{route('portfolios.show')}}','portfolio_id='+this.value,'POST','portfolioContent')">
                    <option>{{ trans('portfolios.categories') }}</option>
                    @if($categories)
                        @foreach($categories as $item)
                            <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>   
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div id="portfolioContent"></div>
@endsection
@section('js-code')
    <script type="text/javascript">
        $(".select-2").select2();
    </script>
@endsection
