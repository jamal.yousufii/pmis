@if($excluded_locations->count()>0)
    <div class="accordion row m-1">
        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
            <div class="m-alert__text text-dark bg-warning">
                <div>
                    <i class="la la-warning text-dark"></i> &nbsp; {{ trans('portfolios.excluded_projects') }}: 
                    <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                        <span><i class="la la-angle-double-down"></i></span>
                    </a>
                </div>
                <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2 bg-warning">
                        <div class="m-portlet__body bg-warning">
                            <div class="m-portlet__body table-responsive bg-warning">
                                <ul class="list-group list-group-flush bg-warning">
                                    @foreach($excluded_locations as $item)
                                        <li class="list-group-item bg-warning">
                                            {{ $loop->iteration }} - <span>{{$item->Projects->name}}</span>
                                            <ul><li>
                                                {{ $item->province()->first()->{'name_'.$lang} }} 
                                                @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif 
                                                @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                @if($item->latitude)/ {{ $item->latitude }} @endif 
                                                @if($item->longitude)/ {{ $item->longitude }} @endif
                                                <ul><li>{{ trans('portfolios.excluded_baseline') }}</li></ul>
                                            </li></ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>  
                {{-- End of Collpas div --}} 
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{ trans('portfolios.budget_highlights') }}</div> 
    </div>
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_baseline') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($baseline_budget, 2, '.', ',') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_working') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($working_budget, 2, '.', ',')}}({{ round(getPercentage($baseline_budget,$working_budget),2) }}%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_expenditure') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($actual_budget, 2, '.', ',') }}({{ round(getPercentage($total_working,$actual_budget),2) }}%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 pb-3">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{ trans('portfolios.fiscal_year') }}</div> 
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_baseline') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($total_baseline, 2, '.', ',') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_working') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($total_working, 2, '.', ',')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.plan_budget_expenditure') }}</span><br>
                                <span class="m-widget21__sub">{{number_format($total_expenditure, 2, '.', ',')}} ( {{ round(getPercentage($total_baseline,$total_expenditure),2) }}% )</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('portfolios.est_plan_budget_working') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($estimated_budget, 2, '.', ',') }}({{ round(getPercentage($total_working,$estimated_budget),2) }}%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="row col-lg-12">
            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('portfolios.portfolio_schedual')}}</div>
            <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('project_dashboard.from_current_fiscal_to_end')}}</div>
        </div>
        <div class="row col-lg-12 pt-3">
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Cumulative Graph</div>
                <div id="BasicLineChart" style="width: 100%;height:400px;"></div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Curve Graph</div>
                <div id="StackedAreaChart" style="width: 100%;height:400px;"></div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="row col-lg-12 pb-3">
            <div class="col-lg-9">
                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('portfolios.portfolio_budget_share')}}</div>
                <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('project_dashboard.cfy_start_so_far')}}</div>
            </div>
            <div class="col-lg-3 pt-3">
                <button class="btn btn-sm btn-success opacity"></button> <span>{{ trans('portfolios.going_well') }}</span>
                <button class="btn btn-sm btn-warning opacity"></button> <span>{{ trans('portfolios.not_so_good') }}</span>
                <button class="btn btn-sm btn-danger opacity"></button> <span>{{ trans('portfolios.in_danger') }}</span>
            </div>
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-9">
                <div id="BarChartXLable" style="width: 100%;height:400px;"></div>
            </div>
            <div class="col-lg-3">
                <table>
                    @if($projects_detail)
                        @foreach($projects_detail as $item)
                            <?php
                            // Get project budget Share
                            $locationId = array_unique($item->bill_quantities_base->pluck('project_location_id')->toArray());
                            $locationId = array_values($locationId);
                            // Get project location  week days
                            $WeekDays = weekDaysAll($locationId);
                            // Get project location  holidays
                            $holidays = holidaysAll($locationId);
                            $projectWorkingBudget = getWorkingBudgetPlaned($locationId);
                            
                            // Calcualte working budget from current fiscal year start to end
                            $totalWorkingCFY = setCfyDateRange($projectWorkingBudget,$start_fiscal_date,$end_fiscal_date,0,$WeekDays,$holidays); 
                            $totalWorkingCFY = sortBudgetDataCfy($totalWorkingCFY,$WeekDays,$holidays);
                            $project_total_working = round((count($totalWorkingCFY) > 0 ? array_sum(array_column($totalWorkingCFY,'total_price')) : 0 ),4);
                            //ddd($project_total_working);
                            ?>
                            <tr><td><span class="m-badge m-badge--info m-badge--wide opacity1">{{round(getPercentage($total_working,$project_total_working),2)}}%</span> <span>({{$item->procurement->contract_code}}) {{$item->name}}</span></td></tr>
                        @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="row col-lg-12">
            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('portfolios.portfolio_status')}}</div>
            <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('project_dashboard.cfy_start_so_far')}}</div>
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-9">
                <div id="BarChart" style="width: 100%;height:500px;"></div>
            </div>
            <div class="col-lg-3 pt-5">
                <button class="btn btn-sm opacity" style="background: #64f062"></button> <span>{{ trans('portfolios.baseline_budget') }}</span>: {{ round(getPercentage($total_baseline,$budget_baseline_now,true),2) }}<br>
                <button class="btn btn-sm opacity" style="background: #36c5e9"></button> <span>{{ trans('portfolios.working_budget') }}</span>: {{ round(getPercentage($total_working,$total_budget_working_now,true),2) }}<br>
                <button class="btn btn-sm opacity" style="background: #fb9f7e"></button> <span>{{ trans('portfolios.actual_budget') }}</span>: {{ round(getPercentage($total_working,$total_actual_budget_now,true),2) }}<br>
                <button class="btn btn-sm opacity" style="background: #343a40"></button> <span>{{ trans('portfolios.invoiced_budget') }}</span>: {{ round(getPercentage($total_working,$budget_invoiced_now,true),2) }}<br>
                <button class="btn btn-sm opacity" style="background: #0d3366"></button> <span>{{ trans('portfolios.paid_budget') }}</span>: {{ round(getPercentage($total_working,$budget_paid_now,true),2) }}
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="row col-lg-12 pb-3">
            <div class="col-lg-9">
                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('portfolios.portfolio_detailed')}}</div>
                <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('portfolios.cfy_portfolio_detailed')}}</div>                        
            </div>
            <div class="col-lg-3 pt-3">
                <button class="btn btn-sm btn-success opacity"></button> <span>{{ trans('portfolios.going_well') }}</span>
                <button class="btn btn-sm btn-danger opacity"></button> <span>{{ trans('portfolios.in_danger') }}</span>
                <button class="btn btn-sm btn-warning opacity"></button> <span>{{ trans('portfolios.not_so_good') }}</span>
            </div>
        </div>
        <table class="table table-bordered table-hover table-checkable">
            <thead class="bg-light">
                <tr class="font-title">
                    <th>{{ trans('global.number') }}</th>
                    <th>{{ trans('procurement.contract_code') }}</th>
                    <th>{{ trans('procurement.project_name') }}</th>
                    <th>{{ trans('portfolios.spi') }}</th>
                    <th>{{ trans('portfolios.physical_progress') }}</th>
                    <th>{{ trans('portfolios.total_budget') }}</th>
                    <th>{{ trans('portfolios.plan_budget_cfY') }}</th>
                    <th>{{ trans('portfolios.plan_budget_exp_cfY') }}</th>
                    <th>{{ trans('portfolios.planed_comp_date') }}</th>
                    <th>{{ trans('portfolios.estimated_plan_comp_date') }}</th>
                </tr>
            </thead>
            <body>
                @if($projects_detail)
                    @foreach($projects_detail as $item)
                        <?php
                        /** 
                        * Start: SPI Calculation 
                        */
                            // Get first date of planed schedule
                            $getFirstDateOfProject = getFirstLastDateOfProject(array($item->id),'min(start_date)','project_id');
                            // Get last date of planed schedule
                            $getLastDateOfProject = getFirstLastDateOfProject(array($item->id),'max(end_date)','project_id');

                            // Get project actual performance in field
                            $spi = 0;
                            $budgetDone = actualCostDone($item->id);
                            $locationId = array_unique($item->bill_quantities_base->pluck('project_location_id')->toArray());
                            $locationId = array_values($locationId);
                            // Get project location  week days
                            $WeekDays = weekDaysAll($locationId);
                            // Get project location  holidays
                            $holidays = holidaysAll($locationId);
                            // Get project budget from working
                            $projectWorkingBudget = getWorkingBudgetPlaned($locationId);
                            $totalWorkingBudget = $projectWorkingBudget->sum('total_price');
                            // Calcualte working budget from current fiscal year start to end
                            $totalWorkingCFY = setCfyDateRange($projectWorkingBudget,$getFirstDateOfProject,$getLastDateOfProject,0,$WeekDays,$holidays);
                            $totalWorkingCFY = sortBudgetDataCfy($totalWorkingCFY,$WeekDays,$holidays); 
                            // Get planed budget in CFY
                            $Budgetplaned = (count($totalWorkingCFY) > 0 ? array_sum(array_column($totalWorkingCFY,'total_price')) : 0 );
                            if($budgetDone and $Budgetplaned>0)
                            {
                                $spi = round($budgetDone/$Budgetplaned,4); 
                            }
                        /** 
                        * End: SPI Calculation 
                        */

                        /** 
                        * Start: Physical Progress for one project in CFY
                        */
                            $budgetExp = 0;
                            $budgetActual = getBudgetActual($locationId,$start_fiscal_date,date('Y-m-d'));
                            $budgetActual = sortBudgetActual($budgetActual,$start_fiscal_date,date('Y-m-d'),false);
                            $budgetExpenditure = (count($budgetActual) > 0 ? array_sum(array_column($budgetActual,'total_price')) : 0 );
                            if($budgetExpenditure!="0")
                            {
                                // Get project working budget till now
                                $project_price = setCfyDateRange($projectWorkingBudget,$start_fiscal_date,date('Y-m-d'),0,$WeekDays,$holidays); 
                                $project_price = sortBudgetDataCfy($project_price,$WeekDays,$holidays);
                                $project_price = (count($project_price) > 0 ? array_sum(array_column($project_price,'total_price')) : 0 );
                                $budgetExp = getPercentage($project_price,$budgetExpenditure);
                            }
                        /** 
                        * End: Physical Progress 
                        */

                        /** 
                        * Start: Planed Budget in CFY 
                        */
                            // Get project working budget from CFY start to end
                            $planedBudget = setCfyDateRange($projectWorkingBudget,$start_fiscal_date,$end_fiscal_date,0,$WeekDays,$holidays); 
                            $planedBudget = sortBudgetDataCfy($planedBudget,$WeekDays,$holidays);
                            $planedBudget = (count($planedBudget) > 0 ? array_sum(array_column($planedBudget,'total_price')) : 0 );
                        /** 
                        * End: Planed Budget in CFY
                        */

                        /** 
                        * Start: Planed budget expenditure in CFY [Percentage] 
                        */
                            $planedExpenditure = 0;
                            if($planedBudget){
                                $planedExpenditure = getPercentage($planedBudget,$budgetExpenditure);
                            }
                        /** 
                        * End: Planed budget expenditure in CFY [Percentage] 
                        */

                        /** 
                        * Start: Estimated plan completion date 
                        */
                            // Get remaining working days of project
                            $project_remain_working_days = portfolioworkDays(array($locationId[0]),date('Y-m-d'),$getLastDateOfProject);
                            $estimatedCompletionDate = '';
                            $working_days = 0;
                            if($project_remain_working_days and $spi!=0){
                                $working_days = ceil($project_remain_working_days/$spi);
                            }
                            if($working_days!=0){
                                // Get estimated date
                                $estimatedCompletionDate = getDateByWrokginDays($working_days,$WeekDays[$locationId[0]],$holidays[$locationId[0]]);
                            }
                        /** 
                        * End: Estimated plan completion date 
                        */
                        ?>
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->procurement->contract_code }}</td>
                            <td>
                                <a href="{{ route('project_dashboard.show',['id'=>encrypt($item->id),'dep_id'=>session('current_department')]) }}" target="_blank" class="text-primary" style="cursor: pointer">{{ $item->name }}</a>
                            </td>
                            <td>
                                @if($spi<=0.55)
                                    <span class="m-badge m-badge--danger m-badge--wide opacity">{{ $spi }}</span>
                                @elseif($spi<=0.8)
                                    <span class="m-badge m-badge--warning m-badge--wide opacity">{{ $spi }}</span>
                                @else
                                    <span class="m-badge m-badge--success m-badge--wide opacity">{{ $spi }}</span>
                                @endif
                            </td>
                            <td>
                                @if($budgetExp<=55)
                                    <span class="m-badge m-badge--danger m-badge--wide opacity">{{ $budgetExp }}</span>
                                @elseif($budgetExp<=80)
                                    <span class="m-badge m-badge--warning m-badge--wide opacity">{{ $budgetExp }}</span>
                                @else
                                    <span class="m-badge m-badge--success m-badge--wide opacity">{{ $budgetExp }}</span>
                                @endif
                            </td>
                            <td class="text-right">{{ number_format($totalWorkingBudget, 2, '.', ',') }}</td>
                            <td class="text-right">{{ number_format($planedBudget, 2, '.', ',') }}</td>
                            <td>{{ round($planedExpenditure, 2) }}%</td>
                            <td>{{ dateCheck($getLastDateOfProject,$lang) }}</td>
                            <td>{{ dateCheck($estimatedCompletionDate,$lang) }}</td>
                        </tr>
                    @endforeach
                @endif
            </body>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Show graphs
        var BasicLineChart = echarts.init(document.getElementById('BasicLineChart'));
        var StackedAreaChart = echarts.init(document.getElementById('StackedAreaChart'));
        var BarChartXLable = echarts.init(document.getElementById('BarChartXLable'));
        var BarChart = echarts.init(document.getElementById('BarChart'));
        // show title. legend and mpty axis
        BasicLineChartOption = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align:  'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: 
                [
                    @php
                        $months = Config::get('static.dr.fiscal_year_month');
                        $i = 0; 
                    @endphp
                    @foreach($months as $item)
                        @if($i==0)
                            '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                        @else
                            '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                        @endif 
                        @php $i++ @endphp 
                    @endforeach
                ],
                axisLabel : {
                    rotate : 75,
                }
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    itemStyle: {color: '#64f062',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_base_cfy)
                            <?php $total_months = 0; ?>
                            @foreach($budget_base_cfy as $key=>$item)
                                @php $total_months += $item['total_price']; @endphp
                                {{ getPercentage($total_baseline,$total_months) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    itemStyle: {color: '#36c5e9',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_working_cfy)
                            <?php $total_months = 0; ?>
                            @foreach($budget_working_cfy as $key=>$item)
                                @php $total_months +=$item['total_price']; @endphp
                                {{ getPercentage($total_working,$total_months) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    itemStyle: {color: '#fb9f7e',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_actual)
                            <?php $total_months = 0; ?>
                            @foreach($budget_actual as $key=>$item)
                                @php $total_months += $item['total_price']; @endphp
                                {{ getPercentage($total_working,$total_months) }},
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        BasicLineChart.setOption(BasicLineChartOption);

        StackedAreaChartOption = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align: 'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        @php
                            $months = Config::get('static.dr.fiscal_year_month');
                            $i = 0; 
                        @endphp
                        @foreach($months as $item)
                            @if($i==0)
                                '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                            @else
                                '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                            @endif 
                            @php $i++ @endphp 
                        @endforeach
                    ],
                    axisLabel : {
                        rotate : 75,
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_base_cfy)
                            @foreach($budget_base_cfy as $key=>$item)
                                {{ getPercentage($total_baseline,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_working_cfy)
                            @foreach($budget_working_cfy as $key=>$item)
                                {{ getPercentage($total_working,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_actual)
                            @foreach($budget_actual as $key=>$item)
                                {{ getPercentage($total_working,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        StackedAreaChart.setOption(StackedAreaChartOption);

        BarChartXLableOption = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['{{ trans("portfolios.going_well") }}', '{{ trans("portfolios.in_danger") }}', '{{ trans("portfolios.not_so_good") }}'],
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: [
                    @if($projects_detail)
                        @foreach($projects_detail as $item)
                            '{{ $item->procurement->contract_code }}',
                        @endforeach
                    @endif
                ],
                axisLabel : {
                    rotate : 25,
                },
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        @if($projects_detail)
                            @foreach($projects_detail as $item)
                                <?php 
                                // Get project working budget till now
                                $locationId = array_unique($item->bill_quantities_base->pluck('project_location_id')->toArray());
                                $locationId = array_values($locationId);
                                // Get project location  week days
                                $WeekDays = weekDaysAll($locationId);
                                // Get project location  holidays
                                $holidays = holidaysAll($locationId);
                                $projectWorkingBudget = getWorkingBudgetPlaned($locationId);
                                // Calcualte working budget from current fiscal year start to end
                                $totalWorking = setCfyDateRange($projectWorkingBudget,$start_fiscal_date,date('Y-m-d'),0,$WeekDays,$holidays); 
                                $totalWorking = sortBudgetDataCfy($totalWorking,$WeekDays,$holidays);
                                $project_total_working = (count($totalWorking) > 0 ? array_sum(array_column($totalWorking,'total_price')) : 0 );
                                $project_total_working = round($project_total_working,4);

                                // Get project actual budget done till now
                                $budgetActual = getBudgetActual($locationId,$start_fiscal_date,date('Y-m-d'));
                                $budgetActual = sortBudgetActual($budgetActual,$start_fiscal_date,date('Y-m-d'),false);
                                $project_total_actual = (count($budgetActual) > 0 ? array_sum(array_column($budgetActual,'total_price')) : 0 );
                                $percentage_done = getPercentage($project_total_working,$project_total_actual);
                                ?>
                                {
                                    value:  {{ $percentage_done }},
                                    itemStyle: 
                                    {
                                        @if($percentage_done<=55)
                                            color: '#f4516c',
                                        @elseif($percentage_done<=80)
                                            color: '#ffb822',
                                        @else
                                            color: '#34bfa3',
                                        @endif
                                        opacity: 0.7, 
                                        barBorderRadius: [5, 5, 0, 0]
                                    },
                                },
                            @endforeach
                        @endif
                    ],
                    type: 'bar',
                },
            ],

        };
        BarChartXLable.setOption(BarChartXLableOption);

        BarChartXLable.on('click', function (param){                      
        });

        BarChartOption = {
            tooltip: {
                trigger: 'axis'
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: 
                [
                    "{{ trans('portfolios.baseline_budget') }}",
                    "{{ trans('portfolios.working_budget') }}",
                    "{{ trans('portfolios.actual_budget') }}",
                    "{{ trans('portfolios.invoiced_budget') }}",
                    "{{ trans('portfolios.paid_budget') }}",
                ],
                axisLabel : {
                    rotate : 25,
                },
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        {
                            value: {{ getPercentage($total_baseline,$budget_baseline_now) }},
                            itemStyle: {color: '#64f062',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_working,$total_budget_working_now) }},
                            smooth: true,
                            itemStyle: {color: '#36c5e9',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_working,$total_actual_budget_now) }},
                            smooth: true,
                            itemStyle: {color: '#fb9f7e',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_working,$budget_invoiced_now) }},
                            itemStyle: {color: '#343a40',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_working,$budget_paid_now) }},
                            itemStyle: {color: '#0d3366',opacity: 0.8, barBorderRadius: [5, 5, 0, 0]},
                        }
                    ],
                    type: 'bar',
                },
            ],

        };
        BarChart.setOption(BarChartOption);
    });
</script>