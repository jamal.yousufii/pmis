<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="la la-cogs"></i></span>
            <h3 class="m-portlet__head-text">{{trans('monitoring.equipment')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable mt-4">
                    <thead>
                        <tr class="bg-light">
                            <th width="10%">{{ trans('global.number') }}</th>
                            <th width="50%">{{ trans('daily_report.equipment') }}</th>
                            <th width="20%">{{ trans('daily_report.equipment_no') }}</th>
                            <th width="20%">{{ trans('daily_report.work_hours') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($daily_report)
                            @foreach($daily_report->equipments AS $item)
                                <tr id="tr_{{$loop->iteration}}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->equipment['name_dr'] }}</td>
                                    <td>{{ $item->equipment_no }}</td>
                                    <td>{{ $item->work_hourse }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
