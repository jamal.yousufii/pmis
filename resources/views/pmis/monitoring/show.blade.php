@extends('master')
@section('head')
  <title>{{ trans('monitoring.reports') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{ trans('monitoring.reports') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        @include('pmis.monitoring.view')
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="{{ route('daily_report_monitoring/index/',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
        </ul>
      </div>
    </div>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv"> 
        <div class="form-group pt-3 pl-3 pr-3 row">
            <div class="col-lg-12">
                <label class="title-custom" style="color:#575962">{{ trans('monitoring.report_by_location') }}:</label><br>
                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('daily_report_monitoring/show',[$enc_project_id,$dep_id,'loc_id']) }}',this.value)" >
                    <option value="">{{ trans('global.select') }}</option>
                    @if($locations)
                        @foreach($locations as $item)
                            <option value="{{$item->id}}" <?= $loc_id==$item->id? 'selected' : '' ?>>{{ $item->province->{'name_'.$lang} }} @if($item->district_id)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village_id)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    @alert()
    @endalert
    <div class="m-portlet__body table-responsive" id="searchresult"> 
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead>
          <tr class="bg-light">
            <th width="15%">{{ trans('monitoring.report_urn') }}</th>
            <th width="10%">{{ trans('monitoring.reportDate') }}</th>
            <th width="25%">{{ trans('monitoring.project_location') }}</th>
            <th width="15%">{{ trans('global.owner') }}</th>
            <th width="15%">{{ trans('monitoring.status') }}</th>
            <th width="10%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <tr>
                <td>{{ $rec->urn }}</td>
                <td>{{ dateCheck($rec->report_date,$lang) }}</td>
                <td>{{ $rec->location->province->{'name_'.$lang} }} @if($rec->location->district_id)/ {{ $rec->location->district->{'name_'.$lang} }} @endif @if($rec->location->village_id)/ {{ $rec->location->village->{'name_'.$lang} }} @endif @if($rec->location->latitude)/ {{ $rec->location->latitude }} @endif @if($rec->location->longitude)/ {{ $rec->location->longitude }} @endif</td>
                <td>{{ $rec->users['name'] }}</td>
                <td>
                  @if($rec->status==0)
                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('monitoring.pending') }}</span>
                  @elseif($rec->status==1)
                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('monitoring.approved') }}</span>
                  @elseif($rec->status==2)
                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('monitoring.rejected') }}</span>
                  @endif
                </td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"daily_report_monitoring","daily_monitoring_view"))
                            <a href="javascript:void()" onclick="viewRecord('{{route('daily_report_monitoring/location/',encrypt($rec->id))}}','project_id={{$rec->project_id}}','GET','display_content')" data-toggle="modal" data-target="#daily_reports" class="dropdown-item"><i class="la la-edit"></i>{{ trans('global.view_details') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {{ $records->links('pagination') }}
      @endif
    </div>
  </div>
@endsection