<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead class="bg-light">
    <tr>
      <th width="15%">{{ trans('global.pro_urn') }}</th>
      <th width="25%">{{ trans('monitoring.project_name') }}</th>
      <th width="20%">{{ trans('monitoring.contract_code') }}</th>
      <th width="15%">{{ trans('monitoring.contract_start_date') }}</th>
      <th width="15%">{{ trans('monitoring.contract_end_date') }}</th>
      <th width="10%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($records)
      @foreach($records AS $rec)
        <?php
        $enc_id = encrypt($rec->id); ?>
        <tr>
          <td>{{ $rec->urn }}</td>
          <td>{{ $rec->name }}</td>
          <td>{{ $rec->procurement->contract_code }}</td>
          <td>{{ dateCheck($rec->procurement->start_date,$lang)}}</td>
          <td>{{ dateCheck($rec->procurement->end_date,$lang)}}</td>
          <td>
            <span class="dtr-data">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                  @if(doIHaveRoleInDep(session('current_department'),"daily_report_monitoring","daily_monitoring_view"))
                      <a class="dropdown-item" href="{{ route('daily_report_monitoring/list',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view_details') }}</a>
                  @endif
                </div>
              </span>
            </span>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>
