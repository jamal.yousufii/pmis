<!-- Report Start-->
<div id="daily_reports" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="SummaryModal" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">           
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption px-2">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white">{{ trans('monitoring.reports') }}</h3>
                    </div>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" onclick="redirectFunction()" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content" id="resutl_div">
                <div class="m-portlet m-portlet--full-height">
                    <!--begin: Portlet Body-->
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                            <div class="row m-row--no-padding">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m--padding-top-15 m--padding-bottom-15 main-border-div">
                                    <div class="m-wizard__head p-3">
                                        <div class="m-wizard__nav">
                                            @php 
                                                $daily_sections = getSectionsByTabCode('daily_report_menue');
                                                $i = 1;
                                                $url = "";
                                            @endphp 
                                            @if($daily_sections)
                                                @foreach($daily_sections as $sec)
                                                    <div class="m-wizard__steps mt-1">
                                                        <a href="javascript:void()" onclick="changeStatus('{{ $sec->id }}','number','tickClass','tickId_{{$i}}'); viewRecord('{{ route('daily_report_monitoring/show_content') }}','project_id={{$enc_project_id}}&code={{$sec->code}}','POST','display_content')" class="m-menu__link" style="text-decoration: none;">
                                                            <div class="m-wizard__step m-wizard__step--done m-wizard__step--current mt-3 mb-2">
                                                                <div class="m-wizard__step-info">
                                                                    <span class="m-wizard__step-number title-custom">
                                                                        <span id="{{ $sec->id }}" class="<?=$sec->code=='daily_location'? 'bg-warning': 'bg-success' ?> text-white number bg-success">
                                                                            {{ $loop->iteration }}
                                                                        </span>
                                                                    </span>
                                                                    <div class="m-wizard__step-label title-custom" >{{ $sec->{'name_'.$lang} }}</div>
                                                                    <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2 tickClass" id="tickId_{{$i}}" >
                                                                       <?=$sec->code=='daily_location'? '<i class="la la-check text-warning text-white"></i>':''?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         </a>
                                                    </div>      
                                                    @if(!$loop->last)<hr class="p-0 m-0">@endif                                            
                                                    @php $i++ @endphp
                                                @endforeach
				                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 main-border-div">
                                    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm" id="display_content">
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeStatus(id,className,tickClass,tickId) {
        $("."+className).removeClass('bg-warning');
        $("#"+id).addClass('bg-warning');
        $("."+tickClass).html('');
        $("#"+tickId).html('<i class="la la-check text-warning text-white"></i>');
    }
</script>