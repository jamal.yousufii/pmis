<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="la la-commenting"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.qc_narrative')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report->narrative)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.is_worked_stoped') }} : </label><br>
                    <span>{{ ($daily_report->narrative->is_worked_stoped==1? trans('global.yes') : trans('global.no')) }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.main_prob') }} : </label><br>
                    <span> {!! $daily_report->narrative->main_problems !!} </span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.solution') }} : </label><br>
                    <span>{!! $daily_report->narrative->solutions !!}</span>
                </div>
            </div>
        </div>
    </div>
@endif
