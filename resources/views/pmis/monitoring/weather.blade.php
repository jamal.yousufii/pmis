<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fa fa-cloud"></i></span>
            <h3 class="m-portlet__head-text">{{trans('daily_report.weather')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report->weather)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.weather') }} : </label><br>
                    <span>{{ $daily_report->weather->static_data->{'name_'.$lang} }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.high_temp') }} : </label><br>
                    <span> {{ $daily_report->weather->high_temp }} </span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.low_temp') }} : </label><br>
                    <span>{{ $daily_report->weather->low_temp }}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.weather_precipitation') }} : </label><br>
                    <span>{{ $daily_report->weather->weather_precipitation }}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.wind_speed') }} : </label><br>
                    <span>{{ $daily_report->weather->wind_speed }}</span>
                </div>
            </div>
        </div>
    </div>
@endif