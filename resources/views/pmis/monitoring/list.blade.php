@extends('master')
@section('head')
  <title>{{ trans('monitoring.monitoring') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text">{{trans('monitoring.projects')}}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12">
              <div id="div_field" class="errorDiv"> <!-- Error Dive start for select box  -->  
                <select class="form-control m-input m-input--air select-2 required" name="field" id="field" style="width:100%;" onchange="bringSearchFiel('{{route('search.option')}}','POST',this.value,'value_input')">
                  <option value="">{{ trans('global.select') }}</option>
                  <option value="projects.urn"> {{trans('global.pro_urn') }} </option>
                  <option value="projects.name"> {{trans('plans.project_name') }} </option>
                  <option value="projects.code"> {{trans('plans.project_code') }} </option>
                </select>
              </div> 
              <div class="field error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="condition" id="condition" style="width:100%;">
                <option value="=">{{ trans('global.equail') }}</option>
                <option value="like">{{ trans('global.like') }} </option>
              </select>
            </div>
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12" id="value_input">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" name="value" class="form-control m-input required" placeholder="{{trans('global.search_dote')}}" id="search_value">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span><i class="la la-search"></i></span>
                </span>
                <div class="search_value error-div" style="display:none;"></div>
              </div>
            </div>
            {{-- Hidden Input  --}}
            <input type="hidden" name="department_id" value="{{$dep_id}}">
            <input type="hidden" name="section" value="pmis_progress">
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <a href="javascript:void;" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air" onclick="searchRecords('{{route('filter_monitoring')}}','search','POST','searchresult')">
                <span>
                  <i class="la la-search"></i><span>{{trans('global.search')}}</span>
                </span>
              </a>
            </div>
          </div>
        </div>
      </form>  
    </div>
    <!-- Filter End -->
    <div class="m-portlet__body table-responsive" id="searchresult">
      <table class="table table-striped- table-bordered table-hover table-checkable">
        <thead class="bg-light">
          <tr>
            <th width="15%">{{ trans('global.pro_urn') }}</th>
            <th width="25%">{{ trans('monitoring.project_name') }}</th>
            <th width="20%">{{ trans('monitoring.contract_code') }}</th>
            <th width="15%">{{ trans('monitoring.contract_start_date') }}</th>
            <th width="15%">{{ trans('monitoring.contract_end_date') }}</th>
            <th width="10%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <?php
              $enc_id = encrypt($rec->id); ?>
              <tr>
                <td>{{ $rec->urn }}</td>
                <td>{{ $rec->name }}</td>
                <td>{{ $rec->procurement->contract_code }}</td>
                <td>{{ dateCheck($rec->procurement->start_date,$lang)}}</td>
                <td>{{ dateCheck($rec->procurement->end_date,$lang)}}</td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"daily_report_monitoring","daily_monitoring_view"))
                            <a class="dropdown-item" href="{{ route('daily_report_monitoring/list',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view_details') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {{ $records->links('pagination') }}
      @endif
    </div>
  </div>
@endsection
