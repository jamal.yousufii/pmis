<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fa fa-ambulance"></i></span>
            <h3 class="m-portlet__head-text">{{trans('monitoring.accidents')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report->accidents)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">  
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.accident_date') }} : </label><br>
                    <span>{!! $daily_report->accidents->accident_date !!}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.personal_injury') }} : </label><br>
                    <span>{!! ($daily_report->accidents->personal_injury==1? trans('global.yes') : trans('global.no')) !!}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('daily_report.description') }} : </label><br>
                    <span>{!! $daily_report->accidents->description !!}</span>
                </div>
            </div>
        </div>
    </div>
@endif