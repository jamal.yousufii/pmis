@extends('master')
@section('head')
  <title>{{ trans('monitoring.reports') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">{{ trans('monitoring.reports') }}</h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        @include('pmis.monitoring.view')
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="{{ route('daily_report_monitoring/index/',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
        </ul>
      </div>
    </div>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv"> 
        <div class="form-group p-3 row">
            <div class="col-lg-12">
                <label class="title-custom" style="color:#575962">{{ trans('monitoring.report_by_location') }}:</label><br>
                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('daily_report_monitoring/show',[$enc_project_id,$dep_id,'loc_id']) }}',this.value)" >
                    <option value="">{{ trans('global.select') }}</option>
                    @if($locations)
                        @foreach($locations as $item)
                            <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district_id)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village_id)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude) / {{ $item->longitude }} @endif</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
  </div>
@endsection