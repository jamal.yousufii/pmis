<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="flaticon-map-location"></i></span>
            <h3 class="m-portlet__head-text">{{trans('monitoring.activities')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable mt-4">
                    <thead>
                        <tr class="bg-light">
                            <th width="40%">{{ trans('monitoring.activity') }}</th>
                            <th width="15%">{{ trans('monitoring.total_done1') }}</th>
                            <th width="15%">{{ trans('monitoring.percentage_done') }}</th>
                            <th width="10%">{{ trans('monitoring.total_amount') }}</th>
                            <th width="15%">{{ trans('monitoring.percentage') }}</th>
                        </tr>
                    </thead>
                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($daily_report)
                            @foreach($daily_report->activities AS $item)
                                <tr id="tr_{{$loop->iteration}}">
                                    <td>{{ $item->bill_quantity['operation_type'] }}</td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ getPercentage($item->bill_quantity['amount'],$item->amount) }} %</td>
                                    <td>{{ $item->bill_quantity['amount'] }}</td>
                                    <td>{{ getPercentage($item->bill_quantity->estimation['cost'],$item->bill_quantity['total_price']) }} %</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif