<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="flaticon-map-location"></i></span>
            <h3 class="m-portlet__head-text">{{trans('monitoring.project_location')}}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"daily_report_monitoring","daily_monitoring_approval") and $daily_report->status!=1)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-warning btn-sm" id="collapseApprove" data-toggle="collapse" href="#collapseApproveDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-arrows-v"></i> <span>{{ trans('monitoring.changeStatus') }}</span></span>
                    </a>
                </li>
            @endif
            @if($daily_report_log->count()>0)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-info btn-sm" id="collapseApproveView" data-toggle="collapse" href="#collapseApproveDivView" role="button" aria-expanded="false" aria-controls="collapseDiv">
                        <span><i class="la la-arrows-v"></i> <span>{{ trans('monitoring.status') }}</span></span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseApproveDiv">
    <!-- Add Report -->
    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="approvalForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('monitoring.status') }}: <span style="color:red;">*</span></label>
                        <div id="div_status" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" name="status" id="status" style="width:100%">
                                <option value="">{{ trans('global.select') }}</option>
                                <option value="1">{{ trans('monitoring.approve') }}</option>
                                <option value="2">{{ trans('monitoring.reject') }}</option>
                            </select>
                        </div>
                        <div class="status error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('monitoring.date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePicker errorDiv required" type="text" name="status_date" id="status_date">
                        <div class="status_date error-div" style="display:none;"></div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <label class="title-custom">{{ trans('monitoring.description1') }}:</label>
                <textarea class="form-control m-input m-input--air" name="description" rows="3"></textarea>
                <span class="m-form__help">{{ trans('monitoring.desc_note') }}</span>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <input type="hidden" name="project_id" value="{{ $project_id }}">
                    <input type="hidden" name="report_id" value="{{ session('report_id') }}">
                    <button type="button" onclick="storeRecord('{{route('daily_report_monitoring/store')}}','approvalForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" class="btn btn-secondary" data-toggle="collapse" href="#collapseApproveDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">{{ trans('global.cancel') }}</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseApproveDivView">
    <!-- Add Report -->
    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm">
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
            <div class="m-portlet__body">
                @if($daily_report_log)
                    @foreach($daily_report_log as $rec)
                        <div class="row m-form__group_custom pt-1">
                            <div class="col-lg-12 text-title">{{$loop->iteration}} .</div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('monitoring.status') }} : </label><br>
                                @if($rec->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('monitoring.pending') }}</span>
                                @elseif($rec->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('monitoring.approved') }}</span>
                                @elseif($rec->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('monitoring.rejected') }}</span>
                                @endif
                            </div>
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('monitoring.date') }} : </label><br>
                                <span>{{ dateCheck($rec->status_date,$lang) }}</span>
                            </div>
                            <div class="col-lg-2">
                                <label class="title-custom">{{ trans('global.owner') }} : </label><br>
                                <span>{{ $rec->approved_by['name'] }}</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="title-custom">{{ trans('monitoring.description1') }} : </label><br>
                                <span>{{ $rec->description }}</span>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationForm" enctype="multipart/form-data">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.reportDate') }} : </label><br>
                    <span>{{ dateCheck($daily_report->report_date,$lang) }}</span>
                </div>
                <div class="col-lg-6">
                    <label class="title-custom">{{ trans('daily_report.project_location') }} : </label><br>
                    <span> {{ $daily_report->location->province->{'name_'.$lang} }} @if($daily_report->location->district_id)/ {{ $daily_report->location->district->{'name_'.$lang} }} @endif @if($daily_report->location->village_id)/ {{ $daily_report->location->village->{'name_'.$lang} }} @endif @if($daily_report->location->latitude) / {{ $daily_report->location->latitude }} @endif @if($daily_report->location->longitude)/ {{ $daily_report->location->longitude }}@endif</span>
                </div>
            </div>
        </div>
    </div>
@endif