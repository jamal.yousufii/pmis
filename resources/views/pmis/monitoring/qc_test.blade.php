<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon"><i class="fa fa-coins"></i></span>
            <h3 class="m-portlet__head-text">{{trans('monitoring.qc_test')}}</h3>
        </div>
    </div>
</div>
<!--begin::data-->
@if($daily_report)
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable mt-4">
                    <thead>
                        <tr class="bg-light">
                            <th width="30%">{{ trans('monitoring.activity') }}</th>
                            <th width="10%">{{ trans('monitoring.test_no') }}</th>
                            <th width="15%">{{ trans('monitoring.test_type') }}</th>
                            <th width="5%">{{ trans('monitoring.completed') }}</th>
                            <th width="10%">{{ trans('monitoring.test_result') }}</th>
                            <th width="35%">{{ trans('monitoring.description') }}</th>
                        </tr>
                    </thead>
                        <tbody>
                            @if($daily_report)
                                @foreach($daily_report->test AS $item)
                                    <tr id="tr_{{$item->id}}">
                                        <td>{{ $item->bill_quantity['operation_type'] }}</td>
                                        <td>{{ $item->test_number }}</td>
                                        <td>{{ $item->test_type }}</td>
                                        <td>{{ ($item->is_completed=='completed'? trans('global.yes') : trans('global.no')) }}</td>
                                        <td>{{trans(Config::get('static.'.$lang.'.test_result.'.$item->test_status.'.name'))}}</td>
                                        <td>
                                            {!! $item->description !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@endif