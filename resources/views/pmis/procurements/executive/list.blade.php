@extends('master')
@section('head')
    <title>{{ trans('procurement.procurement') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile mb-2">
        <div class="m-portlet m-portlet--mobile mb-3">
            <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4 pt-2 mb-2" role="tablist">
                @if($tabs)
                    @foreach($tabs as $item)
                        @if(check_my_section(array(decrypt(session('current_department'))),$item->code))
                            <li class="nav-item m-tabs__item">
                                <a href="{{ route($item->url_route,$enc_id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                    <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet m-portlet--mobile" id="showContent">
            <!-- Include Summary Modal -->
            @include('pmis.summary.summary_modal')
            <div class="tab-content" id="content">
                <div class="tab-pane active" role="tabpanel">
                    <div class="m-portlet__head table-responsive" style="margin-top:-10px;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
                            </div> 
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"tab_executive","pro_exe_add") AND !$record)
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" onclick="addRecord('{{route('executive/create')}}','plan_id={{$enc_id}}','GET','content')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('procurement.add_execution') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"tab_executive","view_pro_summary"))
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                                            <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('procurement',session('current_department')) }}">
                                        <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @alert()
                    @endalert
                    <div class="m-portlet__body table-responsive" id="searchresult">
                        <table class="table table-striped- table-bordered table-hover table-checkable">
                            <thead>
                                <tr class="bg-light">
                                    <th width="20%">{{ trans('global.pro_urn') }}</th>
                                    <th width="20%">{{ trans('global.status') }}</th>
                                    <th width="25%">{{ trans('procurement.date') }}</th>
                                    <th width="25%">{{ trans('global.created_by') }}</th>
                                    <th width="10%">{{ trans('global.action') }}</th>
                                </tr>
                            </thead>
                            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                @if($record)
                                    @php $rec_id = encrypt($record->id); @endphp
                                    <tr>
                                        <td>{{ $record->plan->urn }}</td>
                                        <td>
                                            @if($record->executive==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->executive==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </td>
                                        <td>{{ dateCheck($record->executive_date,$lang) }}</td>
                                        <td>{{ $record->users->name }}</td>
                                        <td>
                                            <span class="dtr-data">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                        @if(doIHaveRoleInDep(session('current_department'),"tab_executive","pro_exe_view"))
                                                            <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('executive/show',$rec_id) }}','plan_id={{$enc_id}}','GET','content')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                                        @endif
                                                        @if(doIHaveRoleInDep(session('current_department'),"tab_executive","pro_exe_edit") and $record->planning===null)
                                                            <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('executive/edit',$rec_id)}}','plan_id={{$enc_id}}','GET','content')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                                        @endif
                                                    </div>
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection