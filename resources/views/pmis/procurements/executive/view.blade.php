<div class="m-portlet__head table-responsive" style="margin-top:-10px;">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('procurement.view_execution') }}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"tab_executive","view_pro_summary"))
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                    </a>
                </li>
            @endif
            <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="javascript:void()" onclick="redirectFunction()">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.is_approved') }} :</label><br>
                <span>
                    @if($record->executive==1)
                        <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                    @elseif($record->executive==0)
                        <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                    @endif
                </span>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.date') }} :</label><br>
                <span>{{ dateCheck($record->executive_date,$lang) }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                <span>{!! $record->executive_desc !!}</span>
            </div>
        </div>
    </div>
</div>
