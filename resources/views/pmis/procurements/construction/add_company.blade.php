<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">{{trans('procurement.send_letter')}}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
        @if(doIHaveRole("tab_construction","pro_con_add") and $record->company===null)
            <li class="m-portlet__nav-item">
              <a class="btn btn-default btn-sm " id="collapsCompanyBtnAdd" data-toggle="collapse" href="#collapseCompanyDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                  <span><i class="la la-cart-plus"></i> <span>{{ trans('global.add') }}</span></span>
              </a>
            </li>
        <!-- @if(doIHaveRole("tab_construction","pro_con_edit") and $record->completed==0) @endif -->
        @elseif(doIHaveRole("tab_construction","pro_con_edit"))
            <li class="m-portlet__nav-item">
              <a class="btn btn-default btn-sm " id="collapsCompanyBtnEdit" data-toggle="collapse" href="#collapseCompanyDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
                  <span><i class="la la-arrows-v"></i> <span>{{ trans('global.edit') }}</span></span>
              </a>
          </li>    
        @endif
    </ul>
  </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseCompanyDiv">
  <div class="m-wizard__form">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="construction_form" method="post" enctype="multipart/form-data">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.company') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="text" name="value[0]" id="company" value="{{ $record->company ? $record->company : '' }}">
                <div class="company error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.contract_code') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="text" name="value[1]" id="contract_code" value="{{ $record->contract_code ? $record->contract_code : '' }}">
                <div class="contract_code error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.start_date') }} : <span style="color:red;">*</span></label></label>
                <input class="form-control m-input errorDiv required datePicker" type="text" name="value[2]" id="start_date" value="<?=$record->start_date!=''?dateCheck($record->start_date,$lang): '' ?>">
                <div class="start_date error-div" style="display:none;"></div>
            </div>
        </div>  
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.end_date') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required datePicker" type="text" name="value[3]" id="end_date" value="<?=$record->end_date!=''?dateCheck($record->end_date,$lang): '' ?>">
                <div class="end_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.contract_price') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="number" min="0" name="value[4]" id="contract_price" value="{{ $record->contract_price ? $record->contract_price : '' }}">
                <span class="m-form__help">{{ trans('procurement.contract_price_note') }}</span>
                <div class="contract_price error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.discount') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="number" min="0" max="30" step="0.1" name="value[9]" id="discount" value="{{ $record->discount }}">
                <div class="discount error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
                <label class="title-custom">{{ trans('procurement.controle_time') }} : <span style="color:red;">*</span></label></label>
                <input class="form-control m-input errorDiv required datePicker" type="text" name="value[5]" id="controle_time" value="<?=$record->controle_time!=''?dateCheck($record->controle_time,$lang): '' ?>">
                <div class="controle_time error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-8">
                <label class="title-custom">{{ trans('procurement.description') }} :</label>
                <textarea class="form-control m-input m-input--air" name="value[6]" id="company_desc" rows="2">{{($record->company_desc ? $record->company_desc : '')}}</textarea>
                <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-12">
            @if($record->bids!==null) <!-- Update if daily report exist -->  
              <input type="hidden" name="id" value="{{encrypt($record->id)}}">
              <button type="button" onclick="storeData('{{route('construction/update')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
            @else  <!-- Otherwise insert the data -->
              <button type="button" onclick="storeData('{{route('construction/store')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.submit') }}</button>
            @endif  
            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
      <input type="hidden" name="plan_id" value="{{$enc_id}}"/>
      <!-- Submit database column names -->
      <input type="hidden" name="columns[0]" value="company"/>
      <input type="hidden" name="columns[1]" value="contract_code"/>
      <input type="hidden" name="columns[2]" value="start_date"/>
      <input type="hidden" name="columns[3]" value="end_date"/>
      <input type="hidden" name="columns[4]" value="contract_price"/>
      <input type="hidden" name="columns[5]" value="controle_time"/>
      <input type="hidden" name="columns[6]" value="company_desc"/>
      <input type="hidden" name="columns[7]" value="con_current"/>
      <input type="hidden" name="columns[8]" value="con_previous"/>
      <input type="hidden" name="columns[9]" value="discount"/>
      <input type="hidden" name="columns[10]" value="discount_at"/>
      <input type="hidden" name="columns[11]" value="discount_by"/>
      <input type="hidden" name="value[7]" value="controle"/>
      <input type="hidden" name="value[8]" value="company"/>
      <input type="hidden" name="current_view" value="company"/>
      <input type="hidden" name="value[10]" value="{{date('Y-m-d H:i:s')}}"/>
      <input type="hidden" name="value[11]" value="{{userid()}}"/>
      @csrf
    </form>
    <!--end::Form-->
  </div>
</div>
<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
    <div class="m-portlet__body" id="company_content">
        @if($record->company!==null)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.company') }} :</label><br>
                    <span>{{ $record->company }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.contract_code') }} :</label><br>
                    <span>{{ $record->contract_code }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.start_date') }} :</label><br>
                    <span>{{ dateCheck($record->start_date,$lang) }}</span>
                </div>
            </div>  
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.end_date') }} :</label><br>
                    <span>{{ dateCheck($record->end_date,$lang) }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.contract_price') }} :</label><br>
                    <span>{{ number_format($record->contract_price) }}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.discount') }} :</label><br>
                    <span>{{ $record->discount }} %</span>
                </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('procurement.controle_time') }} :</label><br>
                    <span>{{ dateCheck($record->controle_time,$lang) }} {{ dateDifference($record->controle_time,'%a') }}</span>
                </div>
                <div class="col-lg-8">
                    <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                    <span>{{ $record->company_desc }}</span>
                </div>
            </div>
        @endif
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        @if ($lang=="en")
            $(".datePicker").attr('type', 'date');
        @else
            $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
        @endif
    });
</script>
