<div id="discountModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="discountModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon"><i class="la la-tasks"></i></span>
                                <h3 class="m-portlet__head-text">{{trans('procurement.discount')}}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="closeBtn" aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="discount_form">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12">
                                    <label class="title-custom">{{ trans('procurement.discount') }} : <span style="color:red;">*</span></label>
                                    <div id="div_discount" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2 required" name="discount" id="discount" style="width: 100%">
                                            <option value="">{{trans('global.select')}}</option>
                                            @for($x=0.1; $x<=30; $x+= 0.1)
                                                <option value="{{$x}}">{{ $x }} %</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="discount error-div" style="display:none;"></div>
                                </div> 
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions align">
                                {{-- Hiddend Inputs Start --}}
                                <input type="hidden" name='record_id' value="{{$enc_id}}">
                                {{-- Hiddend Inputs End --}}
                                <button type="button" class="btn btn-primary" onclick="storeRecord('{{route('procurement.discount')}}','discount_form','POST','response_div',redirectFunction,false,this.id);" id="shareBtn">{{trans('global.save')}}</button>
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>