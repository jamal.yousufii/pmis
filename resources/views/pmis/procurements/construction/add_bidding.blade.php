<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">{{trans('procurement.bidding')}}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      @if(doIHaveRoleInDep(session('current_department'),"tab_construction","pro_con_add") and $record->bidding===null)
        <li class="m-portlet__nav-item">
          <a class="btn btn-default btn-sm " id="collapsBiddingBtnAdd" data-toggle="collapse" href="#collapseBiddingDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
              <span><i class="la la-cart-plus"></i> <span>{{ trans('global.add') }}</span></span>
          </a>
        </li>
      @elseif(doIHaveRoleInDep(session('current_department'),"tab_construction","pro_con_edit") and $record->completed==0)
        <li class="m-portlet__nav-item">
          <a class="btn btn-default btn-sm " id="collapsBiddingBtnEdit" data-toggle="collapse" href="#collapseBiddingDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
              <span><i class="la la-arrows-v"></i> <span>{{ trans('global.edit') }}</span></span>
          </a>
        </li>    
      @endif
    </ul>
  </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseBiddingDiv">
  <div class="m-wizard__form">
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="construction_form" method="post" enctype="multipart/form-data">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.bidding') }} : <span style="color:red;">*</span></label><br>
                <div id="div_bidding" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2 required" style="width: 100%" name="value[0]" id="bidding">
                    <option value="">{{trans('global.select')}}</option>
                    <option value="1" {{ $record->bidding == 1 ? 'selected': '' }}>{{ trans('procurement.announcement') }}</option>
                    <option value="2" {{ $record->bidding == 2 ? 'selected': '' }}>{{ trans('procurement.single_source') }}</option>
                    <option value="3" {{ $record->bidding == 3 ? 'selected': '' }}>{{ trans('procurement.conditional') }}</option>
                  </select>
                </div> 
                <div class="bidding error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.bidding_approval_time') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input datePicker errorDiv required" type="text" name="value[1]" id="bidding_approval_time" value="<?=$record->bidding_approval_time!=''?dateCheck($record->bidding_approval_time,$lang): '' ?>">
                <div class="bidding_approval_time error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <label class="title-custom">{{ trans('procurement.description') }} :</label>
                <textarea class="form-control m-input m-input--air" name="value[2]" id="bidding_desc" rows="2">{{($record->bidding_desc ? $record->bidding_desc : '')}}</textarea>
                <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-12">
            @if($record->bidding!==null) <!-- Update if daily report exist -->  
              <input type="hidden" name="id" value="{{encrypt($record->id)}}">
              <button type="button" onclick="storeData('{{route('construction/update')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
            @else  <!-- Otherwise insert the data -->
              <button type="button" onclick="storeData('{{route('construction/store')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.submit') }}</button>
            @endif  
              <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
      <input type="hidden" name="plan_id" value="{{$enc_id}}"/>
      <!-- Submit database column names -->
      <input type="hidden" name="columns[0]" value="bidding"/>
      <input type="hidden" name="columns[1]" value="bidding_approval_time"/>
      <input type="hidden" name="columns[2]" value="bidding_desc"/>
      <input type="hidden" name="columns[3]" value="con_current"/>
      <input type="hidden" name="columns[4]" value="con_previous"/>
      <input type="hidden" name="value[3]" value="bidding_approval"/>
      <input type="hidden" name="value[4]" value="bidding"/>
      <input type="hidden" name="current_view" value="bidding"/>
      @csrf
    </form>
  </div>
</div>
<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"> 
  <div class="m-portlet__body" id="bidding_content">
    @if($record->bidding!==null)
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-6">
          <label class="title-custom">{{ trans('procurement.bidding') }} :</label><br>
          <span>
              @if($record->bidding==1)
                {{ trans('procurement.announcement') }}
              @elseif($record->bidding==2)
                {{ trans('procurement.single_source') }}
              @elseif($record->bidding==3)
                {{ trans('procurement.conditional') }}
              @endif
          </span>
        </div>
        <div class="col-lg-6">
          <label class="title-custom">{{ trans('procurement.bidding_approval_time') }} :</label><br>
          <span>{{ dateCheck($record->bidding_approval_time,$lang) }} {{ dateDifference($record->bidding_approval_time,'%a') }}</span>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
          <span>{{ $record->bidding_desc }}</span>
        </div>
      </div>
    @endif
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });

  $( document ).ready(function() {
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif
  });
</script>
