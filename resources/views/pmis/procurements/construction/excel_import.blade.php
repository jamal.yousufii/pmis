<div id="importModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                <div class="m-portlet__head bg-color-dark">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon"><i class="la la-leanpub"></i></span>
                            <h3 class="m-portlet__head-text white-color">{{trans('estimation.estimation_import')}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item white-color">
                                <button type="button" class="close white-color" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="importFile" enctype="multipart/form-data">
                        <div class="form-group m-form__group row m-form__group_custom px-0">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('global.location') }}: <span style="color:red;">*</span></label>
                                <div id="div_project_loc_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" name="project_loc_id" id="project_loc_id" style="width:100%" onchange="viewRecord('{{route('procurement.bq_sections')}}','id='+this.value,'POST','bq_sec_id')">
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($project_location)
                                            @foreach($project_location as $item)
                                                <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="project_loc_id" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom px-0">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('global.section') }}: <span style="color:red;">*</span></label>
                                <div id="div_bq_sec_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" name="bq_sec_id" style="width:100%" id="bq_sec_id">
                                        <option value="">{{ trans('global.select') }}</option>
                                    </select>
                                </div>
                                <div class="bq_sec_id" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom p-3">
                            <div class="col-lg-12 col-md-12">
                                <input type="file" class="custom-file-input required " name="fileToUpload" id="req_inputfile" onchange="chooseFile(this.id)">
                                <label class="custom-file-label" id="file-label" for="file">{{ trans('global.select_file') }}</label>
                                <span class="m-form__help small" style="">{{ trans('global.file_import_ext') }}</span>
                            </div>
                            <div class="fileToUpload error-div" style="display:none;"></div>
                        </div>
                        <div class="p-3">
                            <input type="hidden" name="project_id" value="{{$enc_plan}}"/>
                            <input type="hidden" name="rec_id" value="{{$record->id}}"/>
                            <button type="button" onclick="storeRecord('{{ route('procurement.excel_import') }}','importFile','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    $(".select-2").select2({ width: '100%' });
</script>
















