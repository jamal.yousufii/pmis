<div id="construction_steps_create" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="constructionSteps" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">           
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption px-2">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white">{{ trans('procurement.add_execution') }}</h3>
                    </div>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" onclick="redirectFunction()" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content" id="resutl_div">
                <div class="m-portlet m-portlet--full-height">
                    <!--begin: Portlet Body-->
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                            <div class="row m-row--no-padding">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m--padding-top-10 m--padding-bottom-15 main-border-div">
                                    <div class="m-wizard__head p-3">
                                        <div class="m-wizard__nav">
                                            @php 
                                                $i = 1;
                                            @endphp 
                                            @if($record->con_current==0)
                                                @if($steps)
                                                    @foreach($steps as $step)
                                                        <div class="m-wizard__steps mt-1">
                                                            @if($loop->iteration==1)
                                                                <a href="javascript:void()" onclick="changeStatus('{{ $step['id'] }}','number','tickClass','tickId_{{$i}}'); viewRecord('{{ route('construction/create') }}','plan_id={{ $enc_id }}&con_current=financial_letter','GET','construction_steps_content')" class="m-menu__link" style="text-decoration: none;">
                                                                    <div class="m-wizard__step m-wizard__step--current">
                                                                        <div class="m-wizard__step-info">
                                                                            <span class="m-wizard__step-number title-custom">
                                                                                <span id="{{ $step['id']  }}" class="<?=$step['code'] =='financial_letter'? 'bg-warning': 'bg-success' ?> text-white number bg-success">
                                                                                    {{ $loop->iteration }}
                                                                                </span>
                                                                            </span>
                                                                            <div class="m-wizard__step-label title-custom" >{{ trans( $step['name'] ) }}</div>
                                                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2 tickClass" id="tickId_{{$i}}" >
                                                                            <?=$step['code'] == 'financial_letter'? '<i class="la la-check text-warning text-white"></i>':''?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            @else
                                                                <a href="javascript:void()" onclick="changeStatus('{{ $step['id'] }}','number','tickClass','tickId_{{$i}}'); viewRecord('{{ route('construction/create') }}','plan_id={{ $enc_id }}&con_current={{ $step['code'] }}','GET','construction_steps_content')" class="m-menu__link" style="text-decoration: none;">
                                                                    <div class="m-wizard__step m-wizard__step--current">
                                                                        <div class="m-wizard__step-info">
                                                                            <span class="m-wizard__step-number title-custom">
                                                                                <span id="{{ $step['id']  }}" class="bg-success text-white number bg-success">
                                                                                    {{ $loop->iteration }}
                                                                                </span>
                                                                            </span>
                                                                            <div class="m-wizard__step-label title-custom" >{{ trans( $step['name'] ) }}</div>
                                                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2 tickClass" id="tickId_{{$i}}" >
                                                                            <i class="text-white"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            @endif  
                                                        </div>
                                                        <hr class="@if($step['code']=='financial_letter') active-line @endif">
                                                        @php $i++ @endphp
                                                    @endforeach
                                                @endif
                                            @else
                                                @if($steps)
                                                    @foreach($steps as $step)
                                                        <div class="m-wizard__steps mt-1">
                                                            <a href="javascript:void()" onclick="changeStatus('{{ $step['id'] }}','number','tickClass','tickId_{{$i}}'); viewRecord('{{ route('construction/create') }}','plan_id={{ $enc_id }}&con_current={{ $step['code'] }}','GET','construction_steps_content')" class="m-menu__link" style="text-decoration: none;">
                                                                <div class="m-wizard__step m-wizard__step--current">
                                                                    <div class="m-wizard__step-info">
                                                                        <span class="m-wizard__step-number title-custom">
                                                                            <span id="{{ $step['id']  }}" class="<?=$step['code'] ==$record->con_current? 'bg-warning': 'bg-success' ?> text-white number bg-success">
                                                                                {{ $loop->iteration }}
                                                                            </span>
                                                                        </span>
                                                                        <div class="m-wizard__step-label title-custom" >{{ trans( $step['name'] ) }}</div>
                                                                        <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2 tickClass" id="tickId_{{$i}}" >
                                                                        <?=$step['code'] ==$record->con_current? '<i class="la la-check text-warning text-white"></i>':''?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        @php $i++ @endphp
                                                    @endforeach
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12">
                                    <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm" id="construction_steps_content">        
                                    </div>
                                    <input type="hidden" name="plan_id" value="{{$enc_id}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        @if ($lang=="en")
            $(".datePicker").attr('type', 'date');
        @else
            $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
        @endif
        
        @if($record->con_current==0)
            viewRecord('{{ route('construction/create') }}','plan_id={{ $enc_id }}&con_current=financial_letter','GET','construction_steps_content');
        @else
            viewRecord('{{ route('construction/create') }}','plan_id={{ $enc_id }}&con_current={{ $record->con_current }}','GET','construction_steps_content');
        @endif
    });

    function changeStatus(id,className,tickClass,tickId) {
        $("."+className).removeClass('bg-warning');
        $("#"+id).addClass('bg-warning');
        $("."+tickClass).html('');
        $("#"+tickId).html('<i class="la la-check text-warning text-white"></i>');
    }
</script>

