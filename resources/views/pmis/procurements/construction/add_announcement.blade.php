<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">{{trans('procurement.announcement')}}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      @if(doIHaveRoleInDep(session('current_department'),"tab_construction","pro_con_add") and $record->announcement===null)
        <li class="m-portlet__nav-item">
          <a class="btn btn-default btn-sm " id="collapsAnnouncementBtnAdd" data-toggle="collapse" href="#collapseAnnouncementDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
              <span><i class="la la-cart-plus"></i> <span>{{ trans('global.add') }}</span></span>
          </a>
        </li>
      @elseif(doIHaveRoleInDep(session('current_department'),"tab_construction","pro_con_edit") and $record->completed==0)
        <li class="m-portlet__nav-item">
          <a class="btn btn-default btn-sm " id="collapsAnnouncementBtnEdit" data-toggle="collapse" href="#collapseAnnouncementDiv" role="button" aria-expanded="false" aria-controls="collapseDiv">
              <span><i class="la la-arrows-v"></i> <span>{{ trans('global.edit') }}</span></span>
          </a>
        </li>    
      @endif
    </ul>
  </div>
</div>
<div class="code notranslate cssHigh collapse" id="collapseAnnouncementDiv">
  <!-- Add Report -->
  <div class="m-wizard__form">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="construction_form" method="post" enctype="multipart/form-data">
      <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-6">
            <label class="title-custom">{{ trans('procurement.announcement') }} : <span style="color:red;">*</span></label><br>
            <div id="div_announcement" class="errorDiv">
              <select class="form-control m-input m-input--air select-2 required" style="width: 100%" name="value[0]" id="announcement">
                <option value="">{{trans('global.select')}}</option>
                <option value="1" {{ $record->announcement === 1 ? 'selected': '' }} >{{ trans('global.yes') }}</option>
                <option value="0" {{ $record->announcement === 0 ? 'selected': '' }} >{{ trans('global.no') }}</option>
              </select>
            </div>
            <div class="announcement error-div" style="display:none;"></div>
          </div>
          <div class="col-lg-6">
              <label class="title-custom">{{ trans('procurement.bids_time') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv required datePicker" type="text" name="value[1]" id="bids_time" value="<?=$record->bids_time!=''?dateCheck($record->bids_time,$lang): '' ?>">
              <div class="bids_time error-div" style="display:none;"></div>
          </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-12">
            <label class="title-custom">{{ trans('procurement.description') }} :</label>
            <textarea class="form-control m-input m-input--air" name="value[2]" id="announcement_desc" rows="2">{{($record->announcement_desc ? $record->announcement_desc : '')}}</textarea>
            <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
          </div>
        </div>  
        <div class="form-group m-form__group row m-form__group_custom">
          <div class="col-lg-12">
            @if($record->announcement!==null) <!-- Update if daily report exist -->  
              <input type="hidden" name="id" value="{{encrypt($record->id)}}">
              <button type="button" onclick="storeData('{{route('construction/update')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.edit') }}</button>
            @else  <!-- Otherwise insert the data -->
              <button type="button" onclick="storeData('{{route('construction/store')}}','construction_form','POST','construction_steps_content',put_content)" class="btn btn-primary">{{ trans('global.submit') }}</button>
            @endif  
              <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
      <input type="hidden" name="plan_id" value="{{$enc_id}}"/>
      <!-- Submit database column names -->
      <input type="hidden" name="columns[0]" value="announcement"/>
      <input type="hidden" name="columns[1]" value="bids_time"/>
      <input type="hidden" name="columns[2]" value="announcement_desc"/>
      <input type="hidden" name="columns[3]" value="con_current"/>
      <input type="hidden" name="columns[4]" value="con_previous"/>
      <input type="hidden" name="value[3]" value="bids"/>
      <input type="hidden" name="value[4]" value="announcement"/>
      <input type="hidden" name="current_view" value="announcement"/>
      @csrf
    </form>
  </div>
</div>
<!--end::Form-->
<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
  <div class="m-portlet__body" id="announcement_content">
    @if($record->announcement!==null)
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-6">
          <label class="title-custom">{{ trans('procurement.announcement') }} :</label><br>
          <span>
            @if($record->announcement==1)
            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
            @elseif($record->announcement==0)
            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
            @endif
          </span>
        </div>
        <div class="col-lg-6">
          <label class="title-custom">{{ trans('procurement.bids_time') }} :</label><br>
          <span>{{ dateCheck($record->bids_time,$lang) }} {{ dateDifference($record->bids_time,'%a') }}</span>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">    
        <div class="col-lg-12">
          <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
          <span>{{ $record->announcement_desc }}</span>
        </div>
      </div>
    @endif
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });

  $( document ).ready(function() {
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif
  });
</script>
