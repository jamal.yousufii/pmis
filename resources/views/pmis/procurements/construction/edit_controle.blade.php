<div class="m-portlet__head m-portlet__head-bg table-responsive" style="margin-top:-10px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text text-white">{{ trans('procurement.edit_execution') }}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      @if(doIHaveRoleInDep(session('current_department'),"tab_construction","view_pro_summary"))
        <li class="m-portlet__nav-item">
          <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
              <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
          </a>
        </li>
      @endif
    </ul>
  </div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="construction_form" method="post" enctype="multipart/form-data">
  <div class="m-portlet__body">
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
            <label class="title-custom">{{ trans('procurement.controle') }} : <span style="color:red;">*</span></label><br>
            <div id="div_controle" class="errorDiv">
              <select class="form-control m-input m-input--air select-2 required" name="value[0]" id="controle">
                  <option value="">{{trans('global.select')}}</option>
                  <option value="1" {{ $record->controle == 1 ? 'selected': '' }}>{{ trans('global.yes') }}</option>
                  <option value="0" {{ $record->controle == 0 ? 'selected': '' }}>{{ trans('global.no') }}</option>
              </select>
            </div>
            <div class="controle error-div" style="display:none;"></div>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
            <label class="title-custom">{{ trans('procurement.description') }} : </label>
            <textarea class="form-control m-input m-input--air" name="value[1]" id="controle_desc" rows="2">{{ $record->controle_desc }}</textarea>
            <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions--solid">
            <button type="button" onclick="storeRecord('{{ route('construction/update') }}','construction_form','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="enc_id" value="{{$enc_id}}"/>
  <!-- Submit database column names -->
  <input type="hidden" name="columns[0]" value="controle"/>
  <input type="hidden" name="columns[1]" value="controle_desc"/>
  @csrf
</form>
<!--end::Form-->
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>
