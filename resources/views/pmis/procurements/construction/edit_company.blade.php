<div class="m-portlet__head m-portlet__head-bg table-responsive" style="margin-top:-10px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text text-white">{{ trans('procurement.edit_execution') }}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      @if(doIHaveRoleInDep(session('current_department'),"tab_construction","view_pro_summary"))
        <li class="m-portlet__nav-item">
          <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
              <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
          </a>
        </li>
      @endif
    </ul>
  </div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="construction_form" method="post" enctype="multipart/form-data">
  <div class="m-portlet__body">
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">{{ trans('procurement.send_letter') }}</div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.company') }} : <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv required" type="text" name="value[0]" id="company" value="{{$record->company}}">
            <div class="company error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.contract_code') }} : <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv required" type="text" name="value[1]" id="req_contract_code" value="{{$record->contract_code}}">
            <div class="contract_code error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.start_date') }} :<span style="color:red;">*</span></label></label>
            <input class="form-control m-input errorDiv datePicker required" type="text" name="value[2]" id="start_date" value="{{dateCheck($record->start_date,$lang)}}">
            <div class="start_date error-div" style="display:none;"></div>
        </div>
    </div>  
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.end_date') }} :<span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv datePicker required" type="text" name="value[3]" id="end_date" value="{{dateCheck($record->end_date,$lang)}}">
            <div class="end_date error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.contract_price') }} :<span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv required" type="number" min="0" name="value[4]" id="contract_price" value="{{$record->contract_price}}">
            <span class="m-form__help">{{ trans('procurement.contract_price_note') }}</span>
            <div class="contract_price error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.discount') }} : <span style="color:red;">*</span></label>
            <input class="form-control m-input errorDiv required" type="number" min="0" max="30" step="0.1" name="value[9]" id="discount" value="{{$record->discount}}">
            <div class="discount error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('procurement.controle_time') }} :<span style="color:red;">*</span></label></label>
            <input class="form-control m-input datePicker errorDiv required" type="text" name="value[5]" id="controle_time" value="{{dateCheck($record->controle_time,$lang)}}">
            <div class="controle_time error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-8">
            <label class="title-custom">{{ trans('procurement.description') }} :</label>
            <textarea class="form-control m-input m-input--air" name="value[6]" id="company_desc" rows="2">{{$record->company_desc}}</textarea>
            <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions--solid">
            <button type="button" onclick="storeRecord('{{ route('construction/update') }}','construction_form','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="enc_id" value="{{$enc_id}}"/>
  <!-- Submit database column names -->
  <input type="hidden" name="columns[0]" value="company"/>
  <input type="hidden" name="columns[1]" value="contract_code"/>
  <input type="hidden" name="columns[2]" value="start_date"/>
  <input type="hidden" name="columns[3]" value="end_date"/>
  <input type="hidden" name="columns[4]" value="contract_price"/>
  <input type="hidden" name="columns[5]" value="controle_time"/>
  <input type="hidden" name="columns[6]" value="company_desc"/>
  <input type="hidden" name="columns[9]" value="discount"/>
  <input type="hidden" name="columns[10]" value="discount_at"/>
  <input type="hidden" name="columns[11]" value="discount_by"/>
  <input type="hidden" name="value[10]" value="{{date('Y-m-d H:i:s')}}"/>
  <input type="hidden" name="value[11]" value="{{userid()}}"/>
  @csrf
</form>
<!--end::Form-->
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>
