<option value="" selected>{{ trans('global.select') }}</option>
@if($records)
    @foreach($records as $item)
        <option value="{!!$item->bq_section->id!!}">{{ $item->bq_section->{'name_'.$lang} }}</option>
    @endforeach
@endif