<div class="m-content" id="resutl_div">
    <div class="m-portlet m-portlet--full-height">
        <!--begin: Portlet Body-->
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                <div class="row m-row--no-padding">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m--padding-top-15 m--padding-bottom-15 main-border-div">
                        <div class="m-wizard__head">
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="" class="m-wizard__step-number title-custom text-white"><span><span>1</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.financial_letter') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step " m-wizard-target="m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>2</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.bidding') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>3</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.bidding_approval') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>4</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.announcement') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_5">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>5</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.bids') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_6">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>6</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.estimate') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_7">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>7</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.public_announce') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_8">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>8</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.send') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_9">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>9</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.contract_arrangement') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_10">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>10</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.contract_approval') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_11">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>11</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.company') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_12">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number title-custom text-white"><span><span>12</span></span></a>
                                            <div class="m-wizard__step-label title-custom">{{ trans('procurement.financial_letter') }}</div>
                                            <div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12">
                        <div class="m-wizard__form m-portlet m-portlet--primary m-portlet--head-sm" id="m_portlet_tools_2">
                            @if($record->financial_letter!==null)
                                @include('pmis.procurements.construction.add_financial')
                            @endif
                            @if($record->bidding!==null)
                                @include('pmis.procurements.construction.add_bidding')
                            @endif
                            @if($record->bidding_approval!==null)
                                @include('pmis.procurements.construction.add_bidding_approval')
                            @endif
                            @if($record->announcement!==null)
                                @include('pmis.procurements.construction.add_announcement')
                            @endif
                            @if($record->bids!==null)
                                @include('pmis.procurements.construction.add_bids')
                            @endif
                            @if($record->estimate!==null)
                                @include('pmis.procurements.construction.add_estimate')
                            @endif
                            @if($record->public_announce!==null)
                                @include('pmis.procurements.construction.add_public_announce')
                            @endif
                            @if($record->send!==null)
                                @include('pmis.procurements.construction.add_send')
                            @endif
                            @if($record->contract_arrangement!==null)
                                @include('pmis.procurements.construction.add_contract_arrangement')
                            @endif
                            @if($record->contract_approval!==null)
                                @include('pmis.procurements.construction.add_contract_approval')
                            @endif
                            @if($record->company!==null)
                                @include('pmis.procurements.construction.add_company')
                            @endif
                            @if($record->controle!==null)
                                @include('pmis.procurements.construction.add_controle')
                            @endif
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <input type="hidden" name="project_id" value="{{ $record->project_id }}">
                                    <button type="button" onclick="storeData('','constructionStepsForm','POST','');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                                </div>
                            </div>
                            <!-- Display Construction Steps -->
                            <div class="m-portlet__body" id="construction_step">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>