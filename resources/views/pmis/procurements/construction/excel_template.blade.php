<div id="templateModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="templateModalLable" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                <div class="m-portlet__head bg-color-dark">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon"><i class="flaticon-download"></i></span>
                            <h3 class="m-portlet__head-text white-color">{{trans('estimation.download_template')}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item white-color">
                                <button type="button" class="close white-color" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed px-0" action="{{ route('procurement.excel_template') }}" name="templateForm" method="post" enctype="multipart/form-data">
                        <div class="form-group m-form__group row m-form__group_custom px-0">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('global.location') }}: <span style="color:red;">*</span></label>
                                <div id="div_project_location_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" style="width:100%" onchange="viewRecord('{{route('procurement.bq_sections')}}','id='+this.value,'POST','bq_section_id')">
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($project_location)
                                            @foreach($project_location as $item)
                                                <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="project_location_id" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom px-0">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('global.section') }}: <span style="color:red;">*</span></label>
                                <div id="div_bq_section_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" name="bq_section_id" style="width:100%" id="bq_section_id">
                                        <option value="">{{ trans('global.select') }}</option>
                                    </select>
                                </div>
                                <div class="bq_section_id" style="display:none;"></div>
                            </div>
                        </div>
                        <div class=" row m-form__group_custom px-0 pt-2">
                            <div class="col-lg-12">
                                <input type="hidden" name="project_id" value="{{$enc_plan}}">
                                <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" type="submit">
                                    <span><i class="flaticon-download"></i><span>{{ trans('global.download') }}</span></span>
                                </button>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>