@extends('master')
@section('head')
    <title>{{ trans('procurement.procurement') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile mb-2">
        <div class="m-portlet m-portlet--mobile mb-3">
            <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4 pt-2 mb-2" role="tablist">
                @if($tabs)
                    @foreach($tabs as $item)
                        @if(check_my_section($item->code))
                            <li class="nav-item m-tabs__item">
                                <a href="{{ route($item->url_route,$enc_plan) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                    <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet m-portlet--mobile" id="showContent">
            <!-- Include Summary Modal -->
            @include('pmis.summary.summary_modal')
            <!-- Include Excel import Modal -->
            @include('pmis.procurements.construction.excel_store')
            @include('pmis.procurements.construction.excel_import')
            <!-- Include Excel template download modal -->
            @include('pmis.procurements.construction.excel_template')
            <div class="tab-content" id="content">
                <div class="tab-pane active" role="tabpanel">
                    <div class="m-portlet__head" style="margin-top:-10px;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">{{ trans('procurement.view_execution') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            @include('pmis.procurements.construction.create')
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"tab_construction","pro_con_add") and $record->completed==0)
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" data-toggle="modal" data-target="#construction_steps_create" id="construction_steps" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('procurement.add_execution') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"tab_construction","add_bq") and $record->company!='' and $record->completed==0)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" href="javascript:void()" data-toggle="modal" data-target="#storeBQModal">
                                            <span><i class="la la-file-excel-o"></i><span>{{ trans('estimation.estimation_import') }}</span></span>
                                        </a>
                                    </li>
                                    <li class="m-portlet__nav-item">
                                        <a href="{{ asset('public/excel_template/BillOfQuantity.xlsx') }}" class="btn btn-success m-btn--custom m-btn--icon btn-sm">
                                            <span><i class="flaticon-download"></i><span>{{ trans('estimation.download_template') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"tab_construction","view_bq") and $record->company!='')
                                    <li class="m-portlet__nav-item">
                                        <a href="{{route('construction.bq.list',['rec_id'=>$enc_id,'enc_plan'=>$enc_plan])}}" class="btn btn-success m-btn--custom m-btn--icon btn-sm">
                                            <span><i class="la la-tasks"></i><span>{{ trans('estimation.bill_quantitie') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($record)
                                    <!-- Include Attachments Modal -->
                                    @include('pmis.attachments.modal')
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_plan}}&record_id={{$enc_id}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                            <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($shared and $record)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    </li>
                                @elseif(doIHaveRoleInDep(session('current_department'),"pmis_procurement","pro_share") and $record->completed==1)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if(!$shared and (doIHaveRoleInDep(session('current_department'),"pmis_procurement","pro_complete") || doIHaveRoleInDep(session('current_department'),"pmis_procurement","pro_stop")))
                                    <!-- Include complete & stop modal -->
                                    @include('pmis.procurements.complete_modal')
                                    @include('pmis.procurements.stop_modal')
                                    <div class="btn-group open">
                                        <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                            <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                                        </button>
                                        <ul class="dropdown-menu mt-2" role="menu">
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_procurement","pro_complete"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                        <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                            <li>
                                                @if(doIHaveRoleInDep(session('current_department'),"pmis_procurement","pro_stop"))
                                                    <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                        <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                    </a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                                @if(doIHaveRoleInDep(session('current_department'),"tab_construction","view_pro_summary"))
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                                            <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{route('construction/list',$enc_plan)}}">
                                        <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @if($record->controle!==null and $record->completed==0)
                        <div class="m-portlet__head h-50 pt-2">
                            <form enctype="multipart/form-data" id="completeForm" method="POST">
                                <table>
                                <td>
                                    <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                                    <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                                        <label><input type="checkbox" id="complete" name="complete" value="1" onclick="completeRecord('{{ route('completeRecords') }}','complete','completeForm','POST','response_div')"><span></span></label>
                                    </span>
                                    </span>
                                </td>
                                <td><label class="title-custom">{{ trans('global.complete') }}</label></td>
                                </table>
                                <input type="hidden" name="table" value="procurements" />
                                <input type="hidden" name="rec_id" value="{{ $enc_id }}" />
                                <input type="hidden" name="project_id" value="{{ $enc_plan }}" />
                            </form>
                        </div>
                    @endif
                    @alert()
                    @endalert
                    <!-- Include sharing modal -->
                    @include('pmis.procurements.share_modal')
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <ul class="todo-list-wrapper list-group list-group-flush">
                                <li class="list-group-item border-top-0">
                                    <div class="m-portlet__body table-responsive px-0">
                                        <table class="table table-striped- table-bordered table-checkable">
                                            <tr class="bg-light">
                                                <th width="20%">{{ trans('global.section') }}</th>
                                                <th width="20%">{{ trans('global.status') }}</th>
                                                <th width="20%">{{ trans('procurement.next_step_time') }}</th>
                                                <th width="40%">{{ trans('procurement.description') }}</th>
                                            </tr>
                                            @if($record->financial_letter!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.financial_letter') }} </td>
                                                    <td>
                                                        @if($record->financial_letter==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->financial_letter==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->bidding_time,$lang) }} {{ dateDifference($record->bidding_time,'%a') }}</td>
                                                    <td>{{ $record->financial_letter_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->bidding!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.bidding') }} </td>
                                                    <td>
                                                        @if($record->bidding==1)
                                                            {{ trans('procurement.announcement') }}
                                                        @elseif($record->bidding==2)
                                                            {{ trans('procurement.single_source') }}
                                                        @elseif($record->bidding==3)
                                                        {{ trans('procurement.conditional') }}
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->bidding_approval_time,$lang) }} {{ dateDifference($record->bidding_approval_time,'%a') }}</td>
                                                    <td>{{ $record->bidding_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->bidding_approval!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.bidding_approval') }} </td>
                                                    <td>
                                                        @if($record->bidding_approval==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->bidding_approval==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->announcement_time,$lang) }} {{ dateDifference($record->announcement_time,'%a') }}</td>
                                                    <td>{{ $record->bidding_approval_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->announcement!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.announcement') }} </td>
                                                    <td>
                                                        @if($record->announcement==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->announcement==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->bids_time,$lang) }} {{ dateDifference($record->bids_time,'%a') }}</td>
                                                    <td>{{ $record->announcement_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->bids!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.bids') }} </td>
                                                    <td>
                                                        @if($record->bids==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->bids==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->estimate_time,$lang) }} {{ dateDifference($record->estimate_time,'%a') }}</td>
                                                    <td>{{ $record->bids_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->estimate!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.estimate') }} </td>
                                                    <td>
                                                        @if($record->estimate==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->estimate==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->public_announce_time,$lang) }} {{ dateDifference($record->public_announce_time,'%a') }}</td>
                                                    <td>{{ $record->estimate_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->public_announce!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.public_announce') }} </td>
                                                    <td>
                                                        @if($record->public_announce==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->public_announce==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->send_time,$lang) }} {{ dateDifference($record->send_time,'%a') }}</td>
                                                    <td>{{ $record->public_announce_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->send!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.send') }} </td>
                                                    <td>
                                                        @if($record->send==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->send==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->contract_arrangement_time,$lang) }} {{ dateDifference($record->contract_arrangement_time,'%a') }}</td>
                                                    <td>{{ $record->send_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->contract_arrangement!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.contract_arrangement') }} </td>
                                                    <td>
                                                        @if($record->contract_arrangement==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->contract_arrangement==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->contract_approval_time,$lang) }} {{ dateDifference($record->contract_approval_time,'%a') }}</td>
                                                    <td>{{ $record->contract_arrangement_desc }}</td>
                                                </tr>
                                            @endif
                                            @if($record->contract_approval!==null)
                                                <tr>
                                                    <td>{{ trans('procurement.contract_approval') }} </td>
                                                    <td>
                                                        @if($record->contract_approval==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->contract_approval==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ dateCheck($record->send_letter_time,$lang) }} {{ dateDifference($record->send_letter_time,'%a') }}</td>
                                                    <td>{{ $record->contract_approval_desc }}</td>
                                                </tr>
                                            @endif
                                        </table>
                                        
                                        <table class="table table-striped- table-bordered table-checkable">
                                            @if($record->company!==null)
                                                <tr class="bg-light">
                                                    <th width="10%">{{ trans('global.section') }}</th>
                                                    <th width="10%">{{ trans('procurement.company') }}</th>
                                                    <th width="10%">{{ trans('procurement.contract_code') }}</th>
                                                    <th width="10%">{{ trans('procurement.start_date') }}</th>
                                                    <th width="10%">{{ trans('procurement.end_date') }}</th>
                                                    <th width="10%">{{ trans('procurement.contract_price') }}</th>
                                                    <th width="10%">{{ trans('procurement.discount') }}</th>
                                                    <th width="10%">{{ trans('procurement.next_step_time') }}</th>
                                                    <th width="20%">{{ trans('procurement.description') }}</th>
                                                </tr>
                                                <tr>
                                                    <td>{{ trans('procurement.send_letter') }} </td>
                                                    <td>{{ $record->company }}</td>
                                                    <td>{{ $record->contract_code }}</td>
                                                    <td>{{ dateCheck($record->start_date,$lang) }}</td>
                                                    <td>{{ dateCheck($record->end_date,$lang) }}</td>
                                                    <td>{{ number_format($record->contract_price) }}</td>
                                                    <td>{{ $record->discount }} %</td>
                                                    <td>{{ dateCheck($record->controle_time,$lang) }} {{ dateDifference($record->controle_time,'%a') }}</td>
                                                    <td>{{ $record->company_desc }}</td>
                                                </tr>
                                            @endif
                                        </table>

                                        <table class="table table-striped- table-bordered table-checkable">
                                            @if($record->controle!==null)
                                                <tr class="bg-light">
                                                    <th width="20%">{{ trans('global.section') }}</th>
                                                    <th width="20%">{{ trans('global.status') }}</th>
                                                    <th width="60%">{{ trans('procurement.description') }}</th>
                                                </tr>
                                                <tr>
                                                    <td>{{ trans('procurement.controle') }} </td>
                                                    <td>
                                                        @if($record->controle==1)
                                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                        @elseif($record->controle==0)
                                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $record->controle_desc }}</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection