
<div class="m-portlet__head table-responsive">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('procurement.edit_execution') }}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"tab_planning","view_pro_summary"))
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="planning_form" method="post" enctype="multipart/form-data">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.is_approved') }} : <span style="color:red;">*</span></label><br>
                <div id="planning" class="errorDiv">
                    <select class="form-control m-input m-input--air select-2" name="planning">
                        <option value="">{{trans('global.select')}}</option>
                        <option value="1" {{ $record->planning == 1 ? 'selected': '' }} >{{ trans('global.yes') }}</option>
                        <option value="0" {{ $record->planning == 0 ? 'selected': '' }} >{{ trans('global.no') }}</option>
                    </select>
                </div>
                <div class="planning error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">{{ trans('procurement.tin') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input" type="text" name="tin" id="tin" value="{{ $record->tin }}">
            <div class="tin error-div" style="display:none;"></div>
        </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <label class="title-custom">{{ trans('procurement.description') }} : </label>
                <textarea class="form-control m-input m-input--air tinymce" name="planning_desc" rows="2">{{ $record->planning_desc }}</textarea>
                <span class="m-form__help">{{ trans('procurement.description_help') }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <input type="hidden" name="plan_id" value="{{$plan_id}}"/>
                        <input type="hidden" name="enc_id" value="{!!$enc_id!!}"/>
                        <button type="button" onclick="doEditRecord('{{ route('planning/update') }}','planning_form','POST');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @csrf
</form>
<!--end::Form-->
<script type="text/javascript">
    $(".select-2").select2({ width: '100%' });
</script>