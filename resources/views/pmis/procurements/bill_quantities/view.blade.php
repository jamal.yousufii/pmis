@extends('master')
@section('head')
    <title>{{ trans('estimation.bill_quantitie') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('construction/show',['rec_id'=>$enc_id,'plan_id'=>$pro_id]) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body">
                <div class="m-form m-form--fit row m-form--label-align-right m-form--group-seperator-dashed p-3 px-5">
                    <div class="col-lg-10">
                        <label class="title-custom">{{ trans('global.location') }}:</label>
                        <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('construction.bq.show',[$pro_id,'loc_id',$enc_id]) }}',this.value)" >
                            <option value="">{{ trans('global.select') }}</option>
                            @if($project_locations)
                                @foreach($project_locations as $item)
                                    <option value="{{$item->id}}" <?= $loc_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="title-custom">&nbsp;</label><br>
                        <a class="btn btn-success btn-sm m-btn" href="{{ route('construction.bq.download', [$pro_id,encrypt($loc_id)]) }}">
                            <span><i class="la la-print"></i> <span>{{ trans('global.report_excel') }}</span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @alert()
        @endalert
        <!-- Display Project Details -->
        @if($bill_quantities->count()>0)
            <div class="title-custom px-5 mt-3">{{ trans('estimation.total_billq_price') }} :  {{$total_price}}</div>
            <div class="m-portlet__body pt-1" id="searchresult">
                <table class="table table-bordered table-hover table-checkable">
                    <thead class="bg-light">
                        <tr>
                            <th width="5%" class="py-3">{{ trans('global.number') }}</th>
                            <th width="22%" class="py-3">{{ trans('estimation.operation_type') }}</th>
                            <th width="6%" class="py-3">{{ trans('estimation.unit') }}</th>
                            <th width="8%" class="py-3">{{ trans('estimation.amount') }}</th>
                            <th width="8%" class="py-3">{{ trans('estimation.price') }}</th>
                            <th width="10%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('procurement.before_contract') }}</th>
                            <th width="10%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('procurement.after_contract') }}</th>
                            <th width="8%" class="py-3">{{ trans('estimation.percentage') }}</th>
                            <th width="10%" class="py-3">{{ trans('estimation.remarks') }}</th>
                            <th width="5%" class="py-3">{{ trans('global.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $bq_section = 0;
                        @endphp
                        @foreach($bill_quantities as $item) 
                            @if($bq_section != $item->bq_section_id)
                                @php
                                    $bq_section = $item->bq_section_id;
                                @endphp
                                <tr style="background: #5b99bf4a">
                                    <td colspan="10" class="text-center title-custom">
                                        {{ $item->bq_section->{'name_'.$lang} }}
                                    </td>
                                <tr>
                            @endif
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ limit_text($item->operation_type) }}</td>
                                <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
                                <td>{{ $item->amount }}</td>
                                <td>{{ $item->price_before }}</td>
                                <td>{{ $item->total_price_before }}</td>
                                <td>{{ $item->total_price }}</td>
                                <td>{{ getPercentage($procurement->contract_price,$item->total_price,true,4) }}</td>
                                <td>{{ $item->remarks }}</td>
                                <td>
                                    <span class="dtr-data">
                                        <span class="dropdown">
                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                                @if(doIHaveRoleInDep(session('current_department'),"tab_construction","add_bq"))
                                                    <a class="dropdown-item" id="collapsBtn" data-toggle="collapse" onclick="removeClass('EditDivDetails-{{$item->id}}','ShowDivDetails-{{$item->id}}')" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                                @endif
                                            </div>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                            <!-- Include BQ Edit -->
                            <tr class="EditDivDetails-{{$item->id}} d-none">
                                <td colspan="10">
                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 pt-3 code notranslate cssHigh collapse showDiv" id="EditDivDetails-{{$item->id}}" style="border:1px solid #21252985;">
                                        @include('pmis.procurements.bill_quantities.edit')
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> 
                <!-- Pagination -->
                @if(!empty($bill_quantities))
                    {!!$bill_quantities->links('pagination')!!}
                @endif  
            </div>
        @endif
    </div>
@endsection
<script type="text/javascript">
    function removeClass(id)
    {
        if($('.'+id).hasClass('d-none')){
            $('.'+id).removeClass('d-none');
        }else{
            $('.'+id).addClass('d-none');
        }
    }
</script>