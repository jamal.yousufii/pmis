<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="bill_quantities_form_{{$item->id}}" method="post"> 
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3">
        <div class="col-lg-8">
            <label class="title-custom">{{ trans('estimation.operation_type') }}: <span style="color:red;">*</span></label>
            <textarea class="form-control m-input m-input--air errorDiv" name="operation_type" rows="1">{{ $item->operation_type }}</textarea>
            <div class="operation_type error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4">
            <label class="title-custom">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
            <div id="unit_id" class="errorDiv">
                <select class="form-control m-input m-input--air select-2" dir="rtl" name="unit_id" required style="width: 100%">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($units)
                        @foreach($units as $unit)
                            <option value="{!!$unit->id!!}" <?=$unit->id==$item->unit_id? 'selected' : ''?> >{!!$unit->name!!}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="unit_id error-div" style="display:none;"></div>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-12">
            <label class="title-custom">{{ trans('estimation.remarks') }}: </label>
            <textarea class="form-control m-input m-input--air" name="remarks" rows="1">{{ $item->remarks }}</textarea>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12">
            <input type="hidden" name="project_id" id="project_id" value="{{ $enc_id }}"/>
            <input type="hidden" name="record_id" value="{{ encrypt($item->id) }}" />
            <button type="button" class="btn btn-primary btn-sm" onclick="doEditRecord('{{route('construction.bq.update')}}','bill_quantities_form_{{$item->id}}','POST','response_div')">{{ trans('global.submit') }}</button>
            <a class="btn btn-secondary btn-sm" id="collapsBtn" data-toggle="collapse" onclick="removeClass('EditDivDetails-{{$item->id}}','ShowDivDetails-{{$item->id}}')" href="#EditDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="EditDivDetails-{{$item->id}}">{{ trans('global.cancel') }}</a>
        </div>
    </div>
</form>