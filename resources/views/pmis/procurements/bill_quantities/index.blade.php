@extends('master')
@section('head')
    <title>{{ trans('estimation.bill_quantitie') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('construction/show',['rec_id'=>$enc_id,'plan_id'=>$pro_id]) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body">
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-3" id="searchresult">
                    <div class="col-lg-12">
                        <label class="title-custom">{{ trans('global.location') }}:</label>
                        <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('construction.bq.show',[$pro_id,'loc_id',$enc_id]) }}',this.value)" >
                            <option value="">{{ trans('global.select') }}</option>
                            @if($project_locations)
                                @foreach($project_locations as $item)
                                    <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection