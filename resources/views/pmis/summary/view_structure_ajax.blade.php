<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('designs.structure_view') }}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                    <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                </a>
            </li>
            <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="javascript:void()" onclick="viewRecord('{{ route('tab_structure',$project_id)}}','','GET','show_content')">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
        </ul>
    </div>
</div>
@include('pmis.summary.attachment')
<div class="m-content" id="show-content">
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
                    <span>{{$summary->employee->first_name}} {{$summary->employee->last_name}}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('designs.start_date') }} :</label><br>
                    <span>{{dateCheck($summary->start_date,$lang)}}</span>
                </div>
                <div class="col-lg-4">
                    <label class="title-custom">{{ trans('designs.complete_date') }} :</label><br>
                    <span>{{dateCheck($summary->end_date,$lang)}}</span>
                </div>
            </div>  
            <div class="form-group m-form__group row m-form__group_custom">  
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('designs.location') }} :</label><br>
                    @foreach($summary->structureProjectLocation as $location)
                        <span class="bg-default">
                            <strong>{{ $loop->iteration }} -  </strong>
                            {{ $location->projectLocation()->first()->province()->first()->{'name_'.$lang} }}
                            @if($location->projectLocation()->first()->district_id) / {{ $location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                            @if($location->projectLocation()->first()->village_id) / {{ $location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif 
                            @if($location->projectLocation()->first()->latitude) / {{ $location->projectLocation()->first()->latitude }} @endif 
                            @if($location->projectLocation()->first()->longitude) / {{ $location->projectLocation()->first()->longitude }} @endif
                            @if(!$loop->last) <br> @endif
                        </span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
                  