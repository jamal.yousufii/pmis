<div class="m-portlet">
    <!--begin: Portlet Body-->
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15 main-border-div">
                    <div class="m-wizard__head">
                        <div class="m-wizard__nav">
                            @php 
                                $sections = get_module_sections_summary(session('current_mod'),'0');
                                $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
                                $current_sec = 'pmis_procurement';  
                                $rec_id = encrypt($project_id);  
                                $req_id = 0;
                                $x = 1;
                            @endphp
                            @if($sections)
                                @foreach($sections as $sec)
                                    @if(in_array($sec->code,$custom_sections))
                                        <?php 
                                        if($current_sec=="pmis_request"){
                                            $req_id = $summary->id;  
                                        }
                                        ?>
                                        <div class="m-wizard__steps">
                                            <a href="javascript:void()" onclick="viewRecord('{{route('summary.show',$rec_id)}}','code={{$sec->code}}&firstCode={{$current_sec}}&req_id={{$req_id}}','GET','resutl_div')" class="m-menu__link" style="text-decoration: none;">
                                                <div class="m-wizard__step m-wizard__step--done m-wizard__step--current">
                                                    <div class="m-wizard__step-info">
                                                        <span class="m-wizard__step-number title-custom"><span class="@if($sec->code==$current_sec) bg-success @else bg-info @endif text-white">{{ $x }}</span></span>
                                                        <div class="m-wizard__step-label title-custom"><span class="{{$sec->icon}} px-2" style="font-size: 1.3rem;"></span>{{ $sec->{'name_'.$lang} }}</div>
                                                        @if($sec->code==$current_sec)<div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check text-success"></i></div>@endif
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <hr class="@if($sec->code==$current_sec) active-line @endif">
                                        @php $x++; @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                @if($summary)
                    <div class="col-xl-9 col-lg-12 main-border-div">
                        <div class="m-wizard__form m-portlet--primary m-portlet--head-sm">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">{{ trans('global.view_execution') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="m-portlet__body">
                                    <div class="row m-form__group_custom pt-1">
                                        <div class="col-lg-12 text-title">{{ trans('procurement.executive') }}</div>
                                    </div>
                                    <ul class="todo-list-wrapper list-group list-group-flush">
                                        <li class="list-group-item border-top-0 px-0">
                                            <div class="m-portlet__body table-responsive px-0">
                                                <table class="table table-striped- table-bordered table-checkable">
                                                    <tr class="bg-light">
                                                        <th width="20%">{{ trans('global.status') }}</th>
                                                        <th width="20%">{{ trans('procurement.date') }}</th>
                                                        <th width="60%">{{ trans('procurement.description') }}</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @if($summary->executive==1)
                                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                            @elseif($summary->executive==0)
                                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ dateCheck($summary->executive_date,$lang) }}</td>
                                                        <td>{!! $summary->executive_desc !!}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="row m-form__group_custom pt-1">
                                        <div class="col-lg-12 text-title">{{ trans('procurement.planning') }}</div>
                                    </div>
                                    <ul class="todo-list-wrapper list-group list-group-flush">
                                        <li class="list-group-item border-top-0 px-0">
                                            <div class="m-portlet__body table-responsive px-0">
                                                <table class="table table-striped- table-bordered table-checkable">
                                                    <tr class="bg-light">
                                                        <th width="20%">{{ trans('global.status') }}</th>
                                                        <th width="20%">{{ trans('procurement.tin') }}</th>
                                                        <th width="60%">{{ trans('procurement.description') }}</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @if($summary->planning==1)
                                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                            @elseif($summary->planning==0)
                                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ $summary->tin }} </td>
                                                        <td>{!! $summary->planning_desc !!}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="row m-form__group_custom pt-12">
                                        <div class="col-lg-12 text-title">{{ trans('procurement.construction') }}</div>
                                    </div>
                                    <ul class="todo-list-wrapper list-group list-group-flush">
                                        <li class="list-group-item border-top-0 px-0">
                                            <div class="m-portlet__body table-responsive px-0">
                                                <table class="table table-striped- table-bordered table-checkable">
                                                    <tr class="bg-light">
                                                        <th width="20%">{{ trans('global.section') }}</th>
                                                        <th width="20%">{{ trans('global.status') }}</th>
                                                        <th width="20%">{{ trans('procurement.next_step_time') }}</th>
                                                        <th width="40%">{{ trans('procurement.description') }}</th>
                                                    </tr>
                                                    @if($summary->financial_letter!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.financial_letter') }} </td>
                                                            <td>
                                                                @if($summary->financial_letter==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->financial_letter==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->bidding_time,$lang) }} {{ dateDifference($summary->bidding_time,'%a') }}</td>
                                                            <td>{{ $summary->financial_letter_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->bidding!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.bidding') }} </td>
                                                            <td>
                                                                @if($summary->bidding==1)
                                                                    {{ trans('procurement.announcement') }}
                                                                @elseif($summary->bidding==2)
                                                                    {{ trans('procurement.single_source') }}
                                                                @elseif($summary->bidding==3)
                                                                {{ trans('procurement.conditional') }}
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->bidding_approval_time,$lang) }} {{ dateDifference($summary->bidding_approval_time,'%a') }}</td>
                                                            <td>{{ $summary->bidding_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->bidding_approval!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.bidding_approval') }} </td>
                                                            <td>
                                                                @if($summary->bidding_approval==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->bidding_approval==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->announcement_time,$lang) }} {{ dateDifference($summary->announcement_time,'%a') }}</td>
                                                            <td>{{ $summary->bidding_approval_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->announcement!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.announcement') }} </td>
                                                            <td>
                                                                @if($summary->announcement==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->announcement==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->bids_time,$lang) }} {{ dateDifference($summary->bids_time,'%a') }}</td>
                                                            <td>{{ $summary->announcement_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->bids!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.bids') }} </td>
                                                            <td>
                                                                @if($summary->bids==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->bids==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->estimate_time,$lang) }} {{ dateDifference($summary->estimate_time,'%a') }}</td>
                                                            <td>{{ $summary->bids_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->estimate!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.estimate') }} </td>
                                                            <td>
                                                                @if($summary->estimate==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->estimate==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->public_announce_time,$lang) }} {{ dateDifference($summary->public_announce_time,'%a') }}</td>
                                                            <td>{{ $summary->estimate_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->public_announce!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.public_announce') }} </td>
                                                            <td>
                                                                @if($summary->public_announce==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->public_announce==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->send_time,$lang) }} {{ dateDifference($summary->send_time,'%a') }}</td>
                                                            <td>{{ $summary->public_announce_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->send!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.send') }} </td>
                                                            <td>
                                                                @if($summary->send==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->send==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->contract_arrangement_time,$lang) }} {{ dateDifference($summary->contract_arrangement_time,'%a') }}</td>
                                                            <td>{{ $summary->send_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->contract_arrangement!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.contract_arrangement') }} </td>
                                                            <td>
                                                                @if($summary->contract_arrangement==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->contract_arrangement==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->contract_approval_time,$lang) }} {{ dateDifference($summary->contract_approval_time,'%a') }}</td>
                                                            <td>{{ $summary->contract_arrangement_desc }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($summary->contract_approval!==null)
                                                        <tr>
                                                            <td>{{ trans('procurement.contract_approval') }} </td>
                                                            <td>
                                                                @if($summary->contract_approval==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->contract_approval==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ dateCheck($summary->send_letter_time,$lang) }} {{ dateDifference($summary->send_letter_time,'%a') }}</td>
                                                            <td>{{ $summary->contract_approval_desc }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                                <table class="table table-striped- table-bordered table-checkable">
                                                    @if($summary->company!==null)
                                                        <tr class="bg-light">
                                                            <th width="10%">{{ trans('global.section') }}</th>
                                                            <th width="10%">{{ trans('procurement.company') }}</th>
                                                            <th width="10%">{{ trans('procurement.contract_code') }}</th>
                                                            <th width="10%">{{ trans('procurement.start_date') }}</th>
                                                            <th width="10%">{{ trans('procurement.end_date') }}</th>
                                                            <th width="10%">{{ trans('procurement.contract_price') }}</th>
                                                            <th width="10%">{{ trans('procurement.discount') }}</th>
                                                            <th width="10%">{{ trans('procurement.next_step_time') }}</th>
                                                            <th width="20%">{{ trans('procurement.description') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <td>{{ trans('procurement.send_letter') }} </td>
                                                            <td>{{ $summary->company }}</td>
                                                            <td>{{ $summary->contract_code }}</td>
                                                            <td>{{ dateCheck($summary->start_date,$lang) }}</td>
                                                            <td>{{ dateCheck($summary->end_date,$lang) }}</td>
                                                            <td>{{ number_format($summary->contract_price) }}</td>
                                                            <td>{{ $summary->discount }} %</td>
                                                            <td>{{ dateCheck($summary->controle_time,$lang) }} {{ dateDifference($summary->controle_time,'%a') }}</td>
                                                            <td>{{ $summary->company_desc }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                                <table class="table table-striped- table-bordered table-checkable">
                                                    @if($summary->controle!==null)
                                                        <tr class="bg-light">
                                                            <th width="20%">{{ trans('global.section') }}</th>
                                                            <th width="20%">{{ trans('global.status') }}</th>
                                                            <th width="60%">{{ trans('procurement.description') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <td>{{ trans('procurement.controle') }} </td>
                                                            <td>
                                                                @if($summary->controle==1)
                                                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                                                @elseif($summary->controle==0)
                                                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ $summary->controle_desc }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>