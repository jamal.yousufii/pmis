<table class="table table-striped-">
  <thead class="bg-light">
    <tr>
        <th width="5%" class="py-3">{{ trans('global.number') }}</th>
        <th width="15%" class="py-3">{{ trans('global.project_location') }}</th>
        <th width="25%" class="py-3">{{ trans('estimation.operation_type') }}</th>
        <th width="8%" class="py-3">{{ trans('estimation.unit') }}</th>
        <th width="8%" class="py-3">{{ trans('estimation.amount') }}</th>
        <th width="10%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
        <th width="10%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
        <th width="9%" class="py-3">{{ trans('estimation.percentage') }}</th>
        <th width="10%" class="py-3">{{ trans('estimation.remarks') }}</th>
    </tr>
  </thead>
  <tbody>
    @php 
    $x = iteration($counter);
    $bq_section = 0;
    @endphp  
    @foreach($bill_quantities as $item) 
      @if($bq_section != $item->bq_section_id)
          @php
              $bq_section = $item->bq_section_id;
          @endphp
          <tr style="background: #5b99bf4a">
              <td colspan="9" class="text-center title-custom">
                  {{ $item->bq_section->{'name_'.$lang} }}
              </td>
          <tr>
      @endif
      <tr>
          <td>{{ $x }}</td>
          <td>{{ $item->locations->province->{'name_'.$lang} }} @if($item->locations->district)/ {{ $item->locations->district->{'name_'.$lang} }} @endif @if($item->locations->village)/ {{ $item->locations->village->{'name_'.$lang} }} @endif @if($item->locations->latitude) / {{ $item->locations->latitude }} @endif @if($item->locations->longitude)/ {{ $item->locations->longitude }} @endif</td>
          <td>{{ limit_text($item->operation_type) }}</td>
          <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
          <td>{{ $item->amount }}</td>
          <td>{{ $item->estimated_price }}</td>
          <td>{{ $item->estimated_total_price }}</td>
          <td>{{ getPercentage($total_price,$item->estimated_total_price,true,4) }}</td>
          <td>{{ $item->description }}</td>
      </tr>
      @php $x++; @endphp
    @endforeach
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($bill_quantities))
    {!!$bill_quantities->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        counter = parseInt($(this).attr('id'));
        dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>