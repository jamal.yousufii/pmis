@extends('master')
@section('head')
    <title>{{ trans('estimation.bill_quantitie') }}</title>
    <style>
    .m-brand__tools{
        display: none !important;
    }
    #m_header{
        display: none !important;
    }
    #m_aside_left{
        display: none !important;  
    }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile" style="margin-top:-5%">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile" id="showContent">
                <div class="m-portlet__head m-portlet__head-bg">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text text-white">{{$plan->name}}</h3>
                        </div> 
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="#" onclick="window.close();">
                                    <span><i class="la la-times-circle"></i> <span>{{ trans('global.close') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <!-- Display Project Details -->
                    @if($bill_quantities->count()>0)
                        <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
                            <div class="m-portlet__head-tools">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                            <div class="m-portlet__body">
                                @if($bill_quantities->count()>0)
                                    <div class="title-custom px-5 mt-3">{{ trans('estimation.total_billq_price') }} :  {{$total_price}}</div>
                                    <div class="form-group m-form__group row m-form__group_custom table-responsive pt-1" id="searchresult">
                                        <table class="table table-striped-">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th width="5%" class="py-3">{{ trans('global.number') }}</th>
                                                    <th width="15%" class="py-3">{{ trans('global.project_location') }}</th>
                                                    <th width="25%" class="py-3">{{ trans('estimation.operation_type') }}</th>
                                                    <th width="8%" class="py-3">{{ trans('estimation.unit') }}</th>
                                                    <th width="8%" class="py-3">{{ trans('estimation.amount') }}</th>
                                                    <th width="10%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="10%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
                                                    <th width="9%" class="py-3">{{ trans('estimation.percentage') }}</th>
                                                    <th width="10%" class="py-3">{{ trans('estimation.remarks') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $bq_section = 0;
                                                @endphp
                                                @foreach($bill_quantities as $item) 
                                                    @if($bq_section != $item->bq_section_id)
                                                        @php
                                                            $bq_section = $item->bq_section_id;
                                                        @endphp
                                                        <tr style="background: #5b99bf4a">
                                                            <td colspan="9" class="text-center title-custom">
                                                                {{ $item->bq_section->{'name_'.$lang} }}
                                                            </td>
                                                        <tr>
                                                    @endif
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $item->locations->province->{'name_'.$lang} }} @if($item->locations->district)/ {{ $item->locations->district->{'name_'.$lang} }} @endif @if($item->locations->village)/ {{ $item->locations->village->{'name_'.$lang} }} @endif @if($item->locations->latitude) / {{ $item->locations->latitude }} @endif @if($item->locations->longitude)/ {{ $item->locations->longitude }} @endif</td>
                                                        <td>{{ limit_text($item->operation_type) }}</td>
                                                        <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
                                                        <td>{{ $item->amount }}</td>
                                                        <td>{{ $item->estimated_price }}</td>
                                                        <td>{{ $item->estimated_total_price }}</td>
                                                        <td>{{ getPercentage($total_price,$item->estimated_total_price,true,4) }}</td>
                                                        <td>{{ $item->description }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table> 
                                        <!-- Pagination -->
                                        @if(!empty($bill_quantities))
                                            {!!$bill_quantities->links('pagination')!!}
                                        @endif  
                                    </div>
                                @endif   
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function removeClass(id,sec_id)
    {
        if($('.'+id).hasClass('d-none')){
            $('.'+id).removeClass('d-none');
        }else{
            $('.'+id).addClass('d-none');
        }
    }
</script>