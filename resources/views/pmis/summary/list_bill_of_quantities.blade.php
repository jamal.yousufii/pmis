@extends('master')
@section('head')
    <title>{{ trans('estimation.bill_quantitie') }}</title>
    <style>
    .m-brand__tools{
        display: none !important;
    }
    #m_header{
        display: none !important;
    }
    #m_aside_left{
        display: none !important;  
    }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile" style="margin-top:-5%">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                        </div> 
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="#" onclick="window.close();">
                                    <span><i class="la la-times-circle"></i> <span>{{ trans('global.close') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row m-form__group_custom table-responsive pt-3">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('global.location') }}:</label>
                                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('summary.view_bill_of_quantities',[$enc_id,'loc_id',$enc_id]) }}',this.value)" >
                                    <option value="">{{ trans('global.select') }}</option>
                                    @if($locations)
                                        @foreach($locations as $item)
                                            <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection