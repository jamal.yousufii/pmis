<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"> 
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3">
        <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
            <label class="title-custom">{{ trans('estimation.operation_type') }}: </label><br>
            <span>{{ $item->operation_type }}</span>
        </div>
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('estimation.unit') }}: </label><br>
            <span>@if($item->unit_id!=0){{ $item->unit->name_dr }}@else '' @endif</span>
        </div>
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('estimation.amount') }}: </label><br>
            <span>{{ $item->amount }}</span>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('estimation.price') }}: </label><br>
            <span>{{ $item->price }}</span>
        </div>
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('estimation.total_price') }}: </label><br>
            <span>{{ $item->total_price }}</span>
        </div>
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('estimation.percentage') }}: </label><br>
            <span>{{ getPercentage($record->cost,$item->total_price) }} %</span>
        </div>
        <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
            <label class="title-custom">{{ trans('global.location') }}: </label><br>
            <span>{{ $item->locations->province->{'name_'.$lang} }} @if($item->locations->district)/ {{$item->locations->district->{'name_'.$lang} }} @endif @if($item->locations->village)/ {{$item->locations->village->{'name_'.$lang} }} @endif @if($item->locations->latitude) / {{ $item->locations->latitude }} @endif @if($item->locations->longitude) / {{ $item->locations->longitude }} @endif</span>
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom p-0 pb-3 pt-3">
        <div class="col-lg-10 col-xs-10 col-md-10 col-sm-10">
            <label class="title-custom">{{ trans('estimation.remarks') }}: </label><br>
            <span>{{ $item->remarks }}</span>
        </div>
        <div class="col-lg-2 col-xs-2 col-md-2 col-sm-2">
            <button type="button" class="btn btn-secondary btn-sm" id="collapsBtn" data-toggle="collapse" onclick="removeClass('ShowDivDetails-{{$item->id}}','','')" href="#ShowDivDetails-{{$item->id}}" role="button" aria-expanded="false" aria-controls="ShowDivDetails-{{$item->id}}">{{ trans('global.close') }}</button>  
        </div>
    </div>
</div>