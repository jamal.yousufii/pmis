@extends('master')
@section('head')
    <title>{{ trans('estimation.bill_quantitie') }}</title>
    <style>
    .m-brand__tools{
        display: none !important;
    }
    #m_header{
        display: none !important;
    }
    #m_aside_left{
        display: none !important;  
    }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile" style="margin-top:-5%">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('estimation.list_bill_quantitie') }}</h3>
                        </div> 
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="#" onclick="window.close();">
                                    <span><i class="la la-times-circle"></i> <span>{{ trans('global.close') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body">
                        <div class="col-lg-12 pt-3 px-3">
                            <label class="title-custom">{{ trans('global.location') }}:</label>
                            <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('summary.view_bill_of_quantities',[$enc_id,'loc_id',$enc_id]) }}',this.value)" >
                                <option value="">{{ trans('global.select') }}</option>
                                @if($locations)
                                    @foreach($locations as $item)
                                        <option value="{{$item->id}}" <?= $loc_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{ $item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }} @endif @if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                @if($bill_quantities->count()>0)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            @if($bill_quantities->count()>0)
                                <div class="title-custom px-3 mt-3">{{ trans('estimation.total_billq_price') }} :  {{$total_price}}</div>
                                <div class="table-responsive pt-1 px-3" id="searchresult">
                                    <table class="table table-striped-">
                                        <thead class="bg-light">
                                            <tr>
                                                <th width="5%" class="py-3">{{ trans('global.number') }}</th>
                                                <th width="30%" class="py-3">{{ trans('estimation.operation_type') }}</th>
                                                <th width="10%" class="py-3">{{ trans('estimation.unit') }}</th>
                                                <th width="10%" class="py-3">{{ trans('estimation.amount') }}</th>
                                                <th width="10%" class="py-3">{{ trans('estimation.price') }} {{ trans('estimation.estimated') }}</th>
                                                <th width="10%" class="py-3">{{ trans('estimation.total_price') }} {{ trans('estimation.estimated') }}</th>
                                                <th width="10%" class="py-3">{{ trans('estimation.percentage') }}</th>
                                                <th width="15%" class="py-3">{{ trans('estimation.remarks') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $bq_section = 0;
                                            @endphp
                                            @foreach($bill_quantities as $item) 
                                                @if($bq_section != $item->bq_section_id)
                                                    @php
                                                        $bq_section = $item->bq_section_id;
                                                    @endphp
                                                    <tr style="background: #5b99bf4a">
                                                        <td colspan="9" class="text-center title-custom">
                                                            {{ $item->bq_section->{'name_'.$lang} }}
                                                        </td>
                                                    <tr>
                                                @endif
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ limit_text($item->operation_type) }}</td>
                                                    <td>@if($item->unit_id!=0){{ $item->unit->{'name_'.$lang} }}@else '' @endif</td>
                                                    <td>{{ $item->amount }}</td>
                                                    <td>{{ $item->estimated_price }}</td>
                                                    <td>{{ $item->estimated_total_price }}</td>
                                                    <td>{{ getPercentage($total_price,$item->estimated_total_price,true,4) }}</td>
                                                    <td>{{ $item->description }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table> 
                                    <!-- Pagination -->
                                    @if(!empty($bill_quantities))
                                        {!!$bill_quantities->links('pagination')!!}
                                    @endif  
                                </div>
                            @endif   
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function removeClass(id,sec_id)
    {
        if($('.'+id).hasClass('d-none')){
            $('.'+id).removeClass('d-none');
        }else{
            $('.'+id).addClass('d-none');
        }
    }
</script>