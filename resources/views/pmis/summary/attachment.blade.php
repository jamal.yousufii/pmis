<div class="code notranslate cssHigh collapse" id="collapseAttDiv" style="border-bottom:1px solid #ebedf2;">
    <div class="m-portlet__head m-portlet__head-bg">
        <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ trans('global.attachment_list') }}</h3>
        </div>
        </div>
    </div>
    <div id="pagination-div" class="mt-2 mb-2 m-scrollable">
        <span id="content_modal"></span>
        <table class="table table-bordered">
        <thead>
            <tr class="bg-light">
            <th scope="col" width="10%">{{ trans('global.number') }}</th>
            <th scope="col" width="70%">{{ trans('global.name') }}</th>
            <th scope="col" width="20%">{{ trans('global.opreation') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($attachments)
                @foreach($attachments as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $item->filename }}.{{ $item->extension }}</td>
                        <td>
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments",array(encrypt($item->id),$attachment_table)) }}"><i class="fa fa-download"></i> {{ trans('global.download') }}</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
        </table>
    </div>
</div>