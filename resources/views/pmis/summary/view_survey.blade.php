<div class="m-portlet">
    <!--begin: Portlet Body-->
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15 main-border-div">
                    <div class="m-wizard__head">
                        <div class="m-wizard__nav">
                            @php 
                                $sections = get_module_sections_summary(session('current_mod'),'0');
                                $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
                                $current_sec = 'pmis_survey';  
                                $project_id = encrypt($project_id);  
                                $req_id = 0; 
                                $x=1;
                            @endphp
                            @if($sections)
                                @foreach($sections as $sec)
                                    @if(in_array($sec->code,$custom_sections))
                                        <?php 
                                        if($current_sec=="pmis_request"){
                                            $req_id = $summary->id;  
                                        }
                                        ?>
                                        <div class="m-wizard__steps">
                                            <a href="javascript:void()" onclick="viewRecord('{{route('summary.show',$project_id)}}','code={{$sec->code}}&firstCode={{$current_sec}}&req_id={{$req_id}}','GET','resutl_div')" class="m-menu__link" style="text-decoration: none;">
                                                <div class="m-wizard__step m-wizard__step--done m-wizard__step--current">
                                                    <div class="m-wizard__step-info">
                                                        <span class="m-wizard__step-number title-custom"><span class="@if($sec->code==$current_sec) bg-success @else bg-info @endif text-white">{{ $x }}</span></span>
                                                        <div class="m-wizard__step-label title-custom"><span class="{{$sec->icon}} px-2" style="font-size: 1.3rem;"></span>{{ $sec->{'name_'.$lang} }}</div>
                                                        @if($sec->code==$current_sec)<div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check text-success"></i></div>@endif
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <hr class="@if($sec->code==$current_sec) active-line @endif">
                                        @php $x++; @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                @if($summary->count()>0)
                    <div class="col-xl-9 col-lg-12 main-border-div">
                        <div class="m-wizard__form m-portlet--primary m-portlet--head-sm">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">{{ trans('surveys.list') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-content" id="show-content">
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                    <div class="m-portlet__body" id="response_show">
                                        @if($summary->first()->has_survey=='yes')  
                                            <table class="table table-striped- table-bordered table-hover table-checkable">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th width="15%">{{ trans('global.urn') }}</th>
                                                        <th width="20%">{{ trans('surveys.head_team') }}</th>
                                                        <th width="15%">{{ trans('surveys.start_date') }}</th>
                                                        <th width="15%">{{ trans('surveys.end_date') }}</th>
                                                        <th width="10%">{{ trans('global.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                                    @foreach($summary AS $rec)
                                                        <?php $rec_id = encrypt($rec->id); ?>
                                                        <tr>
                                                            <td>{!!$rec->urn!!}</td>
                                                            <td>{!!$rec->employee->first_name!!} {!!$rec->employee->last_name!!}</td>
                                                            <td>{!! dateCheck($rec->start_date,$lang) !!}</td>
                                                            <td>{!! dateCheck($rec->end_date,$lang) !!}</td>
                                                            <td>
                                                                <span class="dtr-data">
                                                                    <span class="dropdown">
                                                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                                            <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('summary.show_survey',[$rec_id,$project_id]) }}','','GET','resutl_div')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                                                        </div>
                                                                    </span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>  
                                        @else
                                            <div class="form-group m-form__group row m-form__group_custom">
                                                <div class="col-lg-12">
                                                    <label class="title-custom">{{ trans('surveys.data_exist') }}</label><br>
                                                    <span class="m-badge m-badge--warning m-badge--wide"> {{trans('global.'.$summary->first()->has_survey)}} </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row m-form__group_custom">
                                                <div class="col-lg-12">
                                                    <label class="title-custom">{{ trans('surveys.description') }}:</label>
                                                    {!! $summary->first()->description !!}
                                                </div>
                                            </div> 
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>