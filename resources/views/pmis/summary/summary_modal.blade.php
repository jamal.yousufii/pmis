
<!-- Summary Start-->
<div id="SummaryModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="SummaryModal" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">           
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption px-2">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white">{{ trans('global.project_summary') }}</h3>
                    </div>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content" id="resutl_div">
                <div class="m-portlet">
                    <!--begin: Portlet Body-->
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
                            <div class="row m-row--no-padding">
                                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15 main-border-div">
                                    <div class="m-wizard__head">
                                        <div class="m-wizard__nav">
                                            @php 
                                                $sections = get_module_sections_summary(session('current_mod'),'0');
                                                $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
                                                $current_sec = 'pmis_request';  
                                                $rec_id = 0; 
                                                $req_id = 0; 
                                                $x=1;
                                            @endphp
                                            @if($sections)
                                                @foreach($sections as $sec)
                                                    @if(in_array($sec->code,$custom_sections))
                                                        <?php 
                                                        if($current_sec=="pmis_request"){
                                                            $req_id = $summary->id;  
                                                        }
                                                        $rec_id = encrypt($summary->id);
                                                        ?>
                                                        <div class="m-wizard__steps">
                                                            <a href="javascript:void()" onclick="viewRecord('{{route('summary.show',$rec_id)}}','code={{$sec->code}}&firstCode={{$current_sec}}&req_id={{$req_id}}','GET','resutl_div')" class="m-menu__link" style="text-decoration: none;">
                                                                <div class="m-wizard__step m-wizard__step--done m-wizard__step--current">
                                                                    <div class="m-wizard__step-info">
                                                                        <span class="m-wizard__step-number title-custom"><span class="@if($sec->code==$current_sec) bg-success @else bg-info @endif text-white">{{ $x }}</span></span>
                                                                        <div class="m-wizard__step-label title-custom"><span class="{{$sec->icon}} px-2" style="font-size: 1.3rem;"></span>{{ $sec->{'name_'.$lang} }}</div>
                                                                        @if($sec->code==$current_sec)<div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check text-success"></i></div>@endif
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <hr class="@if($sec->code==$current_sec) active-line @endif">
                                                        @php $x++; @endphp
                                                    @endif
                                                @endforeach
				                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-12 main-border-div">
                                    <div class="m-wizard__form m-portlet--primary m-portlet--head-sm">
                                        <div class="m-portlet__head">
                                            <div class="m-portlet__head-caption">
                                                <div class="m-portlet__head-title">
                                                    <h3 class="m-portlet__head-text">{{ trans('requests.view') }}</h3>
                                                </div>
                                            </div>
                                            <!-- Include Attachments Modal -->
                                            @php 
                                                $attachments = getAttachments('requests_attachments',0,$summary->id,'request');  
                                                $attachment_table = 'requests_attachments';
                                            @endphp
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav">
                                                    @if($summary AND $summary->doc_type!=6)
                                                        <li class="m-portlet__nav-item">
                                                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseStatusDiv" role="button" aria-expanded="true" aria-controls="collapseStatusDiv">
                                                            <span><i class="la la-arrows-v"></i><span>{{ trans('requests.approval') }}</span></span>
                                                            </a>
                                                        </li>
                                                    @endif  
                                                    <li class="m-portlet__nav-item">
                                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        @if($attachments->count()>0)
                                            @include('pmis.summary.attachment')
                                        @endif
                                        @include('pmis.summary.approval_view')
                                        <div class="m-content" id="show-content">
                                            @if($summary)
                                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                                    <div class="m-portlet__body">
                                                        <div class="form-group m-form__group row m-form__group_custom">
                                                            <div class="col-lg-3 col-md-3">
                                                            <label class="title-custom">{{ trans('requests.doc_type') }} :</label><br>
                                                            <span>{!!$summary->documents['name_'.$lang]!!}</span>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            <label class="title-custom">{{ trans('requests.department') }} :</label><br>
                                                            <span>{!!$summary->hr_departments['name_'.$lang]!!}</span>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            <label class="title-custom">{{ trans('requests.req_date') }} :</label><br>
                                                            <span>{!! dateCheck($summary->request_date,$lang) !!}</span>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            <label class="title-custom">{{ trans('requests.req_number') }} :</label><br>
                                                            <span>{!!$summary->doc_number!!}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row m-form__group_custom">
                                                            <div class="col-lg-6 col-md-6">
                                                            <label class="title-custom">{{ trans('requests.goals') }} :</label><br>
                                                            <span>{!!$summary->goals!!}</span>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6">
                                                            <label class="title-custom">{{ trans('requests.description') }} :</label><br>
                                                            <span>{!!$summary->description!!}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-xl-9 col-lg-12">
                                                    <div class="m-wizard__form">
                                                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                                            <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                                                                <dt>{{ trans('global.not_done') }}</dt>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Summary End-->