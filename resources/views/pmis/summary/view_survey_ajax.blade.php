<div class="m-portlet">
    <!--begin: Portlet Body-->
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15 main-border-div">
                    <div class="m-wizard__head">
                        <div class="m-wizard__nav">
                            @php 
                                $sections = get_module_sections_summary(session('current_mod'),'0');
                                $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
                                $current_sec = 'pmis_survey';  
                                $project_id = encrypt($project_id);  
                                $req_id = 0;
                                $x = 1;
                            @endphp
                            @if($sections)
                                @foreach($sections as $sec)
                                    @if(in_array($sec->code,$custom_sections))
                                        <?php 
                                        if($current_sec=="pmis_request"){
                                            $req_id = $summary->id;  
                                        }
                                        ?>
                                        <div class="m-wizard__steps">
                                            <a href="javascript:void()" onclick="viewRecord('{{route('summary.show',$project_id)}}','code={{$sec->code}}&firstCode={{$current_sec}}&req_id={{$req_id}}','GET','resutl_div')" class="m-menu__link" style="text-decoration: none;">
                                                <div class="m-wizard__step m-wizard__step--done m-wizard__step--current">
                                                    <div class="m-wizard__step-info">
                                                        <span class="m-wizard__step-number title-custom"><span class="@if($sec->code==$current_sec) bg-success @else bg-info @endif text-white">{{ $x }}</span></span>
                                                        <div class="m-wizard__step-label title-custom"><span class="{{$sec->icon}} px-2" style="font-size: 1.3rem;"></span>{{ $sec->{'name_'.$lang} }}</div>
                                                        @if($sec->code==$current_sec)<div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check text-success"></i></div>@endif
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <hr class="@if($sec->code==$current_sec) active-line @endif">
                                        @php $x++; @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <!-- Include Attachments Modal -->
                @php 
                    $attachments = getAttachments('survey_attachments',decrypt($project_id),decrypt($rec_id),'survey');  
                    $attachment_table = 'survey_attachments';
                @endphp
                <div class="col-xl-9 col-lg-12 main-border-div">
                    <div class="m-wizard__form m-portlet--primary m-portlet--head-sm">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">{{ trans('surveys.view') }}</h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                                        </a>
                                    </li>
                                    <!-- Dcoument of Record -->
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseDocDiv" role="button" aria-expanded="true" aria-controls="collapseDocDiv">
                                                <span><i class="fa fa-folder-open"></i> <span>{{ trans('surveys.related_document') }}  [{{ $record->documents->count() }}] </span></span>
                                        </a>
                                    </li>
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="javascript:void()" onclick="viewRecord('{{route('summary.show',$project_id)}}','code=pmis_survey','GET','resutl_div')">
                                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @include('pmis.summary.attachment')
                        @include('pmis.summary.document')
                        <div class="m-content" id="show-content">
                            @if($record)
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                    <div class="m-portlet__body" id="response_show">
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-4">
                                                <label class="title-custom">{{ trans('surveys.project_location') }}:</label><br>
                                                <span>
                                                    {{$record->project_location->province->{'name_'.$lang} }} 
                                                    @if($record->project_location->district_id) / {{ $record->project_location->district->{'name_'.$lang} }} @endif 
                                                    @if($record->project_location->village_id) / {{ $record->project_location->village->{'name_'.$lang} }} @endif  
                                                    @if($record->project_location->latitude) / {{ $record->project_location->latitude }} @endif 
                                                    @if($record->project_location->longitude) / {{ $record->project_location->longitude }} @endif
                                                </span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="title-custom">{{ trans('surveys.start_date') }}:</label><br>
                                                <span>{{dateCheck($record->start_date,$lang)}}</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="title-custom">{{ trans('surveys.end_date') }}:</label><br>
                                                <span>{{dateCheck($record->end_date,$lang)}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row m-form__group_custom pt-0">
                                            <table class="table">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.measurement')}}</th>
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.unit')}}</th>
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.quantity')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($record->measurements)
                                                        @foreach($record->measurements as $item)
                                                            <tr>
                                                                <td>@if($item->static_measurement_type){{ $item->static_measurement_type->{'name_'.$lang} }}@endif</td>
                                                                <td>@if($item->static_measurement_type){{ $item->static_measurement_unit->{'name_'.$lang} }}@endif</td>
                                                                <td>{{ $item->quantity }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row m-form__group_custom px-4">
                                            <div class="col-lg-12 text-title px-4"><span>{{ trans('surveys.surv_team') }}</span></div>
                                        </div>
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-4">
                                                <label class="title-custom">{{ trans('surveys.surv_team') }}:</label><br>
                                                <span>{{ $record->employee->first_name }} {{ $record->employee->last_name }}</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <label class="title-custom">{{ trans('surveys.emp_name') }}:</label><br>
                                                <span>{{ $employees }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row m-form__group_custom pt-0">
                                            <table class="table">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.beneficiary')}}</th>
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.total')}}</th>
                                                        <th width="33.33%" style="border-top:0px;">{{trans('surveys.comment')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($record->beneficiaries)
                                                        @foreach($record->beneficiaries as $item)
                                                            <tr>
                                                                <td>@if($item->users){{ $item->users->{'name_'.$lang} }}@endif</td>
                                                                <td>{{ $item->total }}</td>
                                                                <td>{{ $item->comments }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- Aabedat Details -->
                                        @if($plan->category_id==3)
                                            @if($aabedat) 
                                                @php $entity = ''; @endphp
                                                @foreach($aabedat as $item)
                                                    @if($entity!=$item->entity_id)
                                                        @php 
                                                            $data['aabedat'] = $aabedat;
                                                            $data['code']    = $item->entity_id;
                                                            $data['lang']    = $lang;
                                                            $data['is_edit'] = '';
                                                        @endphp
                                                        @include('pmis/surveys/survey_aabedat/'.$item->entity->code,$data)
                                                    @endif
                                                    @php $entity = $item->entity_id; @endphp
                                                @endforeach
                                            @endif
                                            <div class="form-group m-form__group row m-form__group_custom">
                                                <div class="col-lg-3">
                                                    <label class="title-custom">{{ trans('surveys.water_well') }}:</label><br>
                                                    <span>{{ $record->well }}</span>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="title-custom">{{ trans('surveys.naldawani') }}:</label><br>
                                                    <span>{{ $record->piping }}</span>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="title-custom">{{ trans('surveys.pool') }}:</label><br>
                                                    <span>{{$record->pool}}</span>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="title-custom">{{ trans('surveys.sewage') }}:</label><br>
                                                    <span>{{$record->wastewater}}</span>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-12">
                                                <label class="title-custom">{{ trans('surveys.description') }}:</label><br>
                                                <span>{!! $record->description !!}</span>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>