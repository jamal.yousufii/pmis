<div class="m-portlet">
    <!--begin: Portlet Body-->
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="m-wizard m-wizard--4 m-wizard--brand" id="m_wizard">
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15 main-border-div">
                    <div class="m-wizard__head">
                        <div class="m-wizard__nav">
                            @php 
                                $sections = get_module_sections_summary(session('current_mod'),'0');
                                $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
                                $current_sec = 'pmis_plan';
                                $rec_id = encrypt($summary->id);
                                $req_id = 0;
                                $x = 1;
                            @endphp
                            @if($sections)
                                @foreach($sections as $sec)
                                    @if(in_array($sec->code,$custom_sections))
                                        <?php 
                                        if($current_sec=="pmis_request"){
                                            $req_id = $summary->id; 
                                        }
                                        ?>
                                        <div class="m-wizard__steps">
                                            <a href="javascript:void()" onclick="viewRecord('{{route('summary.show',$rec_id)}}','code={{$sec->code}}&firstCode={{$current_sec}}&req_id={{$req_id}}','GET','resutl_div')" class="m-menu__link" style="text-decoration: none;">
                                                <div class="m-wizard__step m-wizard__step--done m-wizard__step--current">
                                                    <div class="m-wizard__step-info">
                                                        <span class="m-wizard__step-number title-custom"><span class="@if($sec->code==$current_sec) bg-success @else bg-info @endif text-white">{{ $x }}</span></span>
                                                        <div class="m-wizard__step-label title-custom"><span class="{{$sec->icon}} px-2" style="font-size: 1.3rem;"></span>{{ $sec->{'name_'.$lang} }}</div>
                                                        @if($sec->code==$current_sec)<div class="m-wizard__step-icon m-wizard__step-icon-custom pt-2 pl-2 pr-2"><i class="la la-check text-success"></i></div>@endif
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <hr class="@if($sec->code==$current_sec) active-line @endif">
                                        @php $x++; @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-12 main-border-div">
                    <div class="m-wizard__form m-portlet--primary m-portlet--head-sm">
                        @if($summary and $summary->count()>0)
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">{{ trans('plans.view_plan') }}</h3>
                                    </div>
                                </div>
                                <!-- Include Attachments Modal -->
                                @php 
                                    $attachments = getAttachments('project_attachments',0,$summary->id,'plan');  
                                    $attachment_table = 'project_attachments';
                                @endphp
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                                <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @include('pmis.summary.attachment')
                            <div class="m-content" id="show-content">
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.status') }}: </label><br>
                                                <span>{{$summary->static_data['name_dr']}}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.name') }}: </label><br>
                                                <span>{{$summary->name}}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.code') }}: </label><br>
                                                <span>{{$summary->code}}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.proj_category') }}: </label><br>
                                                <span>{{ $summary->category->{'name_'.$lang} }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3" id="ptype_div">
                                                <label class="title-custom">{{ trans('plans.project_type') }}: </label><br>
                                                <span>{{ $summary->project_type->{'name_'.$lang} }}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.year') }}: </label><br>
                                                <span>{{yearCheck($summary->year,$lang)}}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.project_start_date') }}: </label><br>
                                                <span>{{dateCheck($summary->project_start_date,$lang)}}</span>
                                            </div>
                                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                                <label class="title-custom">{{ trans('plans.project_end_date') }}: </label><br>
                                                <span>{{dateCheck($summary->project_end_date,$lang)}}</span>
                                            </div>  
                                        </div>
                                        <div class="form-group m-form__group row m-form__group_custom">
                                            <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                                <label class="title-custom">{{ trans('plans.goals') }}: </label><br>
                                                <span>{!!$summary->goal!!}</span>
                                            </div>
                                            <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                                <label class="title-custom">{{ trans('plans.description') }}: </label><br>
                                                <span>{!!$summary->description!!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Display Project Details -->
                                    @if($location)
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title">
                                                        <h3 class="m-portlet__head-text">{{ trans('plans.view_location') }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row m-form__group_custom" id="details-content">
                                                    <table class="table table-striped-">
                                                        <tbody>
                                                            <tr class="bg-light">
                                                                <th width="25%" class="top-no-border">{{ trans('plans.province') }}</th>
                                                                <th width="25%" class="top-no-border">{{ trans('plans.district') }}</th>
                                                                <th width="25%" class="top-no-border">{{ trans('plans.village') }}</th>
                                                                <th width="25%" class="top-no-border">{{ trans('plans.location') }}</th>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>  
                                                            @foreach($location as $sub)
                                                                <tr>
                                                                    <td width="25%">{{ $sub->province->{'name_'.$lang} }}</td>
                                                                    <td width="25%">@if($sub->district_id){{ $sub->district->{'name_'.$lang} }} @endif</td>
                                                                    <td width="25%">@if($sub->village_id) {{ $sub->village->{'name_'.$lang} }} @endif</td>
                                                                    <td width="25%">@if($sub->latitude) / {{ $sub->latitude }} @endif @if($sub->longitude) / {{ $sub->longitude }} @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>         
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>