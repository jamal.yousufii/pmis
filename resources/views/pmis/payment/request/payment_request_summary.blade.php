<div class="mt-2 inline-headers">
    <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.activity_no')}} :</h5>
    <h6 id="activity_counter">{{$counter}}</h6>   
</div>
<div class="mt-2 inline-headers">
    <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.reqtotal_money')}} :</h5>
    <h6>{{$total_request_amount}}</h6>   
</div> 
<div class="mt-2 inline-headers">
    <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.reqtotal_percentage')}} :</h5>
    <h6>{{getPercentage($current_inovice,$total_request_amount)}} %</h6>   
</div> 