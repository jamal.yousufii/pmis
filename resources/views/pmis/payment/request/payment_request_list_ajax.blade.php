 <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
    <thead>
    <tr class="font-title">
        <th width="12%">{{ trans('global.urn') }}</th>
        <th width="25%">{{ trans('payment.description') }}</th>
        <th width="15%">{{ trans('requests.req_date') }}</th>
        <th width="15%">{{ trans('requests.req_number') }}</th>
        <th width="15%">{{ trans('requests.req_owner') }}</th>
        <th width="10%">{{ trans('global.status') }}</th>
        <th width="8%">{{ trans('global.action') }}</th>
    </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
    @if($payments)
        @foreach ($payments as $item)
            <tr>
                <td>{{$item->urn}}</td>
                <td>{{$item->description}}</td>
                <td>{{dateCheck($item->request_date,$lang)}} </td>
                <td>{{$item->request_number}} </td>
                <td>{{$item->user->name}} </td>
                <td>
                    @if($item->status==6)
                        <span class="m-badge  m-badge--success m-badge--wide">{{ trans('requests.approved') }}</span>
                    @elseif($item->status==-1)
                        <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('requests.rejected') }}</span>
                    @else
                        <span class="m-badge m-badge--warning m-badge--wide">{{ trans('requests.pending') }}</span>
                    @endif
                </td>
                <td>
                    <span class="dtr-data">
                    <span class="dropdown">
                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                            @if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_view_payment'))  
                            <a class="dropdown-item" href="{{route('payment_request.show',$item->id)}}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                            @endif 
                            @if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_edit_payment'))    
                            <a class="dropdown-item" href="{{route('payment_request.show',$item->id)}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                            @endif 
                        </div>
                    </span>
                    </span>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
    @if(!empty($payments))
    {{ $payments->links('pagination') }}
@endif
