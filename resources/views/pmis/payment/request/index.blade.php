@extends('master')
@section('head')
    <title>{{ trans('contractor_projects.contractor_project') }}</title>
@endsection
@section('content')
    {{-- @include('breadcrumb_nav')     --}}
    @alert()
    @endalert
     <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">
                <div class="m-accordion__item m-2 mb-3 filter_top_header">
                    <div class="m-accordion__item-head collapsed filter_header" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                        <a href="#" class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill pull-left">
                            <i class="fa fa-filter"></i>
                        </a>
                    </div>
                    <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3" style="">
                         <div class="code notranslate cssHigh collapse show" id="progressCollapse">
                            <!--begin::Form-->
                            <form method="post" action="{{route('payment_request')}}" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="search_form" enctype="multipart/form-data">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-5">
                                            <label class="title-custom">{{trans('global.projects')}}: <span style="color:red;">*</span></label>
                                            <div id="div_project_id" class="errorDiv">
                                                <select name="project_id" onchange="viewRecord('{{route('project_location.option')}}','id='+this.value,'POST','project_location_id')" class="form-control m-input required select-2" id="project_id" style="width: 100%;">
                                                    <option value="">{{trans('global.select')}}</option>
                                                     @if($projects)
                                                        @foreach ($projects as $item)
                                                            <option <?=$item->id==session('project_id') ? 'selected' : '' ?> value="{{$item->id}}">{{$item->name}}</option>
                                                        @endforeach
                                                     @endif
                                                </select>
                                            </div>
                                            <div class="project_id error-div" style="display:none;"></div>
                                        </div>
                                        <div class="col-lg-5">
                                            <label class="title-custom">{{trans('daily_report.project_location')}} : <span style="color:red;">*</span></label>
                                            <div id="div_project_location_id" class="errorDiv">
                                                <select name="project_location_id" class="form-control m-input select-2 required" id="project_location_id" style="width: 100%;">
                                                    {{getProjectLocation(0,session('project_id'))}}
                                                </select>
                                            </div>
                                            <div class="project_location_id error-div" style="display:none;"></div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="submit"  class="btn btn-success m-btn m-btn--icon btn-sm mt-37">
                                                <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
    <h4 class="m-portlet__head-text custom-heading">{{ trans('global.project_summary') }} </h4>
    <div class="row mt-4">
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">
                                        {{trans('plans.project_name')}}
                                    </span><br>
                                    <span class="m-widget21__sub">
                                        {{$plan->name}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                 <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-signs m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">
                                    {{trans('plans.project_code')}}
                                </span><br>
                                <span class="m-widget21__sub">
                                    {{$plan->code}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-business m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_type')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{ $plan->project_type->{'name_'.$lang} }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-coins m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">
                                        {{trans('plans.view_location')}}
                                    </span><br>
                                    <span class="m-widget21__sub">
                                        {{ projectLocationDetails(session('project_location_id')) }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-calendar-2 m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_start_date')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{$plan->project_start_date}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-calendar-3 m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_end_date')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{$plan->project_end_date}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
    </div>
    <div id="show_content">
      <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="m-btn-group m-btn-group--pill btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="Large button group">
                   @if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_add_payment')) 
                    <button type="button" class="btn btn-outline-info active"  onclick="payment_request('{{route('payment_request.billquantity')}}','{{session('project_location_id')}}','POST','show_content')"> <span><i class="fa fa-plus"></i> <span>{{ trans('payment.request') }}</span></span></button>
                   @endif  
                    <button type="button" class="btn btn-outline-info" onclick="replaceUrl('<?=decrypt($dep_id)==6 ? route('contractor_project',['con_id'=>$contractor_id,'dep_id'=>$dep_id]) : route('bringSections',$dep_id) ?>')"><span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span></button>
                </div>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom" id="searchresult">
                    <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
                        <thead>
                        <tr class="font-title">
                            <th width="12%">{{ trans('global.urn') }}</th>
                            <th width="25%">{{ trans('payment.description') }}</th>
                            <th width="15%">{{ trans('requests.req_date') }}</th>
                            <th width="15%">{{ trans('requests.req_number') }}</th>
                            <th width="15%">{{ trans('requests.req_owner') }}</th>
                            <th width="10%">{{ trans('global.status') }}</th>
                            <th width="8%">{{ trans('global.action') }}</th>
                        </tr>
                        </thead>
                        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                        @if($payments)
                            @foreach ($payments as $item)
                                <tr>
                                    <td>{{$item->urn}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{dateCheck($item->request_date,$lang)}} </td>
                                    <td>{{$item->request_number}} </td>
                                    <td>{{$item->user->name}} </td>
                                    <td>
                                        @if($item->status==6)
                                         <span class="m-badge  m-badge--success m-badge--wide">{{ trans('requests.approved') }}</span>
                                        @elseif($item->status==-1)
                                         <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('requests.rejected') }}</span>
                                        @else
                                         <span class="m-badge m-badge--warning m-badge--wide">{{ trans('requests.pending') }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="dtr-data">
                                        <span class="dropdown">
                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                               @if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_view_payment'))  
                                                <a class="dropdown-item" href="{{route('payment_request.show',$item->id)}}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                               @endif 
                                               @if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_edit_payment'))    
                                                <a class="dropdown-item" href="{{route('payment_request.show',$item->id)}}"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                               @endif 
                                            </div>
                                        </span>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                     @if(!empty($payments))
                        {{ $payments->links('pagination') }}
                    @endif
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Start of the payment request modal -->
    <div id="payment_request_modal" class="modal fade bd-example-modal-md" role="dialog" aria-labelledby="SummaryModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header profile-head bg-color-dark">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text text-white">{{ trans('payment.store_payment_request') }}</h3>
                        </div>
                    </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                </div>
                <div class="m-content" id="resutl_div">
                <form class="m-form m-form--fit m-form--label-align-right" id="payment_request_form" method="POST" action="{{route('payment_request.store')}}" >
                    @csrf
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="request_number">{{trans('payment.request_number')}} <span style="color:red;">*</span></label>
                                <input type="text" class="form-control errorDiv required m-input" id="request_number" name="request_number"  placeholder="{{trans('payment.request_number')}}">
                                <div class="request_number error-div" style="display:none;"></div>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="request_date">{{trans('payment.request_date')}} <span style="color:red;">*</span></label>
                                <input type="text" class="form-control errorDiv required datePicker m-input" id="request_date" name="request_date" placeholder="{{trans('payment.request_date')}}">
                                <div class="request_date" style="display:none;"></div>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="description">{{trans('payment.description')}}</label>
                                <textarea type="text" class="form-control m-input" name="description" id="description"></textarea>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions pull-left">
                                <button type="button" onclick="validateSubmit('payment_request_form')" class="btn btn-primary">{{trans('global.submit')}}</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal  End-->

    <!-- workpackage div container, search result of the search bq  --> 
    <div  id="workpackage_content">
   
    </div>
    <!--  End of the workpackage-->

@endsection
