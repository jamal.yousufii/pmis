@extends('master')
@section('head')
    <title>{{ trans('contractor_projects.contractor_project') }}</title>
@endsection
@section('content')
    @alert()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    @endalert
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
            <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">
                <div class="m-accordion__item m-2 mb-3 filter_top_header">
                    <div class="m-accordion__item-head collapsed filter_header" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                        <a href="javascript:void()" class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill pull-left">
                            <i class="fa fa-filter"></i>
                        </a>
                    </div>
                    <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3" style="">
                        <div class="code notranslate cssHigh collapse show" id="progressCollapse">
                            <!--begin::Form-->
                            <form method="post" action="{{route('payment_request')}}" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="search_form" enctype="multipart/form-data">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-6">
                                            <label class="title-custom">{{trans('global.projects')}}: <span style="color:red;">*</span></label>
                                            <div id="div_project_id" class="errorDiv">
                                                <select name="project_id" onchange="viewRecord('{{route('project_location.option')}}','id='+this.value,'POST','project_location_id')" class="form-control m-input required select-2" id="project_id" style="width: 100%;">
                                                    <option value="">{{trans('global.select')}}</option>
                                                        @if($projects)
                                                            @foreach ($projects as $item)
                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                            @endforeach
                                                        @endif
                                                </select>
                                            </div>
                                            <div class="project_id error-div" style="display:none;"></div>
                                        </div>
                                        <div class="col-lg-5">
                                            <label class="title-custom">{{trans('daily_report.project_location')}} : <span style="color:red;">*</span></label>
                                            <div id="div_project_location_id" class="errorDiv">
                                                <select name="project_location_id" class="form-control m-input select-2 required" id="project_location_id" style="width: 100%;">
                                                    <option value="">{{trans('global.select')}}</option>
                                                </select>
                                            </div>
                                            <div class="project_location_id error-div" style="display:none;"></div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="submit"  class="btn btn-success m-btn m-btn--icon btn-sm mt-37">
                                                <span><i class="la la-search"></i><span>{{trans('global.search')}}</span> </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
