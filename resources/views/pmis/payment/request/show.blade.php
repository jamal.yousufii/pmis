@extends('master')
@section('head')
    <title>{{ trans('contractor_projects.contractor_project') }}</title>
@endsection
@section('content')
    <h4 class="m-portlet__head-text custom-heading">{{ trans('global.project_summary') }} </h4>
    <div class="row mt-4">
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">
                                        {{trans('plans.project_name')}}
                                    </span><br>
                                    <span class="m-widget21__sub">
                                        {{$plan->name}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                 <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-signs m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">
                                    {{trans('plans.project_code')}}
                                </span><br>
                                <span class="m-widget21__sub">
                                    {{$plan->code}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-coins m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.proj_category')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{ $plan->category->{'name_'.$lang} }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-business m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_type')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{ $plan->project_type->{'name_'.$lang} }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-calendar-2 m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_start_date')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{$plan->project_start_date}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
        <div class="col-lg-4">
            <!--begin:: Widgets/Adwords Stats-->
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                    <div class="m-widget21">
                        <div class="row">
                            <div class="col">
                                <div class="m-widget21__item proj_sum_wdg">
                                    <span class="m-widget21__icon">
                                        <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="fa flaticon-calendar-3 m--font-light"></i>
                                        </a>
                                    </span>
                                    <div class="m-widget21__info">
                                        <span class="m-widget21__title">
                                          {{trans('plans.project_end_date')}}
                                        </span><br>
                                        <span class="m-widget21__sub">
                                            {{$plan->project_end_date}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!--end:: Widgets/Adwords Stats-->
        </div>
    </div>
    <!--start::invoice detials -->
    <div id="invoice_details">     
      <div class="m-portlet m-portlet--tab border_radiuse">
        <div class="m-portlet__body pb-1">
            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
                 <h3 class="m-portlet__head-text custom-heading">{{trans('payment.inv_details')}}</h3>
                <div class="row">
                    <div class="col-lg-4">
                        <h6>{{trans('payment.inv_req_numb')}} :‌ {{$payment_request->first()->request_number}}</h6>
                     </div>
                     <div class="colo-lg-4">
                         <h6>{{trans('payment.inv_req_status')}} :  
                               
                                @if($paymentRequest->status==6)
                                    <span class="">{{ trans('requests.approved') }}</span>
                                @elseif($paymentRequest->status==-1)
                                    <span class="ba">{{ trans('requests.rejected') }}</span>
                                @else
                                    <span class="">{{ trans('requests.pending') }}</span>
                                @endif
                         </h6>
                     </div>
                     <div class="col-lg-4 text-right">
                      @if(canRejectRecievePayments($paymentRequest->status))    
                        <button type="button" onclick="confirmServerRequest('{{route('payment_recieve')}}','payment_request_id={{$paymentRequest->id}}&&status=approve','POST','invoice_details','<?=in_array($paymentRequest->status,config('static.inv_rcv.send')) ? trans('payment.send_msg') : ($paymentRequest->status==5) ? trans('payment.complt_msg') : trans('payment.inv_recieve_msg')?>')" class="btn m-btn--pill btn-primary btn-sm"><?=in_array($paymentRequest->status,config('static.inv_rcv.send')) ? trans('payment.send') : ($paymentRequest->status==5) ? trans('global.completed') :  trans('payment.recieve')?></button> |
                        <button type="button"  onclick="confirmServerRequest('{{route('payment_recieve')}}','payment_request_id={{$paymentRequest->id}}&&status=reject','POST','invoice_details','{{trans('payment.rej_inv_recieve')}}')" class="btn m-btn--pill btn-danger btn-sm">{{trans('global.reject')}}</button>
                      @endif 
                     </div>
                </div>
                <!--begin: Message container -->
                <div class="m-portlet__padding-x">
                    
                    <!-- Here you can put a message or alert -->
                </div>
                <!--end: Message container -->

                <!--begin: Form Wizard Head -->
                <div class="m-wizard__head m-portlet__padding-x m-wizard__head_custome">

                    <!--begin: Form Wizard Progress -->
                    <div class="m-wizard__progress">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: {{config('static.inv_rcv.'.$paymentRequest->status)}}%" aria-valuenow="100" aria-valuemin="50" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <!--end: Form Wizard Progress -->

                    <!--begin: Form Wizard Nav -->
                    <div class="m-wizard__nav">
                        <div class="m-wizard__steps">
                            <div class="m-wizard__step <?=!in_array($paymentRequest->status,[0,-1]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_1">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-link"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        1. {{trans('payment.ensijam')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.first_step')}}
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step <?=!in_array($paymentRequest->status,[-1,0,1,2]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_2">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-project-diagram"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        2. {{trans('payment.procurement')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.procurement_step')}}
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step <?=!in_array($paymentRequest->status,[-1,0,1,2,3,4]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_3">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-money-bill-alt"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        {{trans('payment.finance')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.finance_step')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>
                <!--end: Form Wizard Head -->
            </div>
            <!--end: Form Wizard-->
        </div>
     </div>
    </div> 
     <!--end::invoice detials -->

     <h4 class="m-portlet__head-text custom-heading">{{ trans('payment.cost') }} </h4>
     <!--Begin::Section-->
        <!--begin:: Widgets/Stats-->
        <div class="m-portlet mt-4">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::Total Profit-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    {{trans('payment.progress')}}
                                </h4><br>
                                <span class="m-widget24__desc">
                                    {{trans('payment.prog_note')}}
                                </span>
                                <span class="m-widget24__stats m--font-success">
                                     {{$all_done_act_price * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: {{$all_done_act_price * 100 / $project_bq->first()->bq_total_price}}%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    {{$all_done_act_price}}
                                </span>
                                <span class="m-widget24__number">
                                    {{$all_done_act_price * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                            </div>
                        </div>

                        <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    {{trans('payment.current_invo')}}
                                </h4><br>
                                <span class="m-widget24__desc">
                                    {{trans('payment.current_invo_note')}}
                                </span>
                                <span class="m-widget24__stats m--font-success">
                                   {{($all_done_act_price - $previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width:  {{($all_done_act_price - $previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    {{$all_done_act_price - $previouse_inv_total_money}}
                                </span>
                                <span class="m-widget24__number">
                                   {{($all_done_act_price - $previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                            </div>
                        </div>

                        <!--end::New Feedbacks-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Orders-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    {{trans('payment.pre_invo')}}
                                </h4><br>
                                <span class="m-widget24__desc">
                                    {{trans('payment.pre_invo_note')}}
                                </span>
                                <span class="m-widget24__stats m--font-info">
                                     {{($previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: {{($previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    {{$previouse_inv_total_money}}
                                </span>
                                <span class="m-widget24__number">
                                    {{($previouse_inv_total_money) * 100 / $project_bq->first()->bq_total_price}} %
                                </span>
                            </div>
                        </div>

                        <!--end::New Orders-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Users-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    {{trans('payment.remain_invo')}}
                                </h4><br>
                                <span class="m-widget24__desc">
                                     {{trans('payment.remain_invo_note')}}
                                </span>
                                <span class="m-widget24__stats m--font-danger">
                                    {{($project_bq->first()->bq_total_price - $all_done_act_price) * 100 / $project_bq->first()->bq_total_price }}%
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: {{($project_bq->first()->bq_total_price - $all_done_act_price) * 100 / $project_bq->first()->bq_total_price }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    {{$project_bq->first()->bq_total_price - $all_done_act_price}}
                                </span>
                                <span class="m-widget24__number">
                                    {{($project_bq->first()->bq_total_price - $all_done_act_price) * 100 / $project_bq->first()->bq_total_price }}%
                                </span>
                            </div>
                        </div>
                        <!--end::New Users-->
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Stats-->
     <!--End::Section-->
    
    <div class="m-portlet m-portlet--mobile"> 
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <h4 class="m-portlet__head-text custom-heading">{{trans('estimation.bill_quantitie')}}</h2>
                </div>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="20%">{{ trans('estimation.operation_type') }}</th>
                        <th width="10%">{{ trans('estimation.amount') }}</th>
                        <th width="10%">{{ trans('estimation.unit') }}</th>
                        <th width="10%">{{ trans('estimation.cost') }}</th>
                        <th width="10%">{{ trans('estimation.activity_done') }} %</th>
                        <th width="10%">{{ trans('payment.to_request') }}</th>
                        <th width="10%">{{ trans('payment.request_money') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total_done_price = 0; 
                    @endphp
                    @if($payment_request)
                        @foreach ($payment_request as $item)
                            @php
                                $total_done_price +=$item->request_amount * $item->price;
                            @endphp
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->operation_type}}</td>
                                <td>{{$item->amount}}</td>
                                <td>{{$item->amount}}</td>
                                <td>{{$item->price}}</td>
                                <td>{{$item->activity_total * 100 / $item->amount }} %</td>
                                <td>{{$item->request_amount}}</td>
                                <td>{{$item->request_amount * $item->price}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="7" class="inv_total_txt">{{trans('global.total')}}</td>
                                <td class="inv_total_total">{{$total_done_price}}</td>
                            </tr>
                    @endif
                </tbody>
            </table>
        </div>
   </div>

    <!--begin::Portlet-->
    {{-- <div class="m-portlet m-portlet--full-height">
        <div class="m-portlet__head bg_pmis">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h6 class="m-portlet__head-text text-white">
                        {{trans('payment.payment_view')}}
                    </h6>
                </div>
            </div>
        </div>
        <div class="m-portlet__body_custome">

            <!--begin::Section-->
            <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">

                <!--begin::Item-->
                <div class="m-accordion__item">
                    <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                        <span class="m-accordion__item-icon"><i class="fa flaticon-interface-4"></i></span>
                        <span class="m-accordion__item-title">{{trans('payment.request_detail')}}</span>
                        <span class="m-accordion__item-mode"></span>
                    </div>
                    <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3" style="">
                        <div class="m-accordion__item-content">
                            <table class="table m-table m-table--head-separator-primary">
                                <thead>
                                     <tr class="font-title">
                                        <th width="12%">{{ trans('global.urn') }}</th>
                                        <th width="25%">{{ trans('payment.description') }}</th>
                                        <th width="15%">{{ trans('requests.req_date') }}</th>
                                        <th width="15%">{{ trans('requests.req_number') }}</th>
                                        <th width="15%">{{ trans('requests.req_owner') }}</th>
                                        <th width="10%">{{ trans('global.status') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>{{$paymentRequest->urn}}</td>
                                    <td>{{$paymentRequest->description}}</td>
                                    <td>{{dateCheck($paymentRequest->request_date,$lang)}} </td>
                                    <td>{{$paymentRequest->request_number}} </td>
                                    <td>{{$paymentRequest->user->name}} </td>
                                    <td>
                                        @if($paymentRequest->status==0)
                                            <span class="m-badge m-badge--warning m-badge--wide">{{ trans('requests.pending') }}</span>
                                        @elseif($paymentRequest->status==1)
                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('requests.approved') }}</span>
                                        @elseif($paymentRequest->status==2)
                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('requests.rejected') }}</span>
                                        @endif
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="m-accordion__item">
                    <div class="m-accordion__item-head" role="tab" id="m_accordion_4_item_1_head" data-toggle="collapse" href="#m_accordion_4_item_1_body" aria-expanded="false">
                        <span class="m-accordion__item-icon"><i class="fa flaticon-list"></i></span>
                        <span class="m-accordion__item-title">{{trans('payment.payment_act_detls')}}</span>
                        <span class="m-accordion__item-mode"></span>
                    </div>
                    <div class="m-accordion__item-body" id="m_accordion_4_item_1_body" role="tabpanel" aria-labelledby="m_accordion_4_item_1_head" data-parent="#m_accordion_3" style="">
                        <div class="m-accordion__item-content" style="padding: 0px">
                             <table class="table m-table m-table--head-separator-primary">
                                <thead>
                                    <th width="5%">{{trans('global.number')}}</th>
                                    <th width="30%">{{ trans('estimation.operation_type') }}</th>
                                    <th width="">{{ trans('estimation.amount') }}</th>
                                    <th width="">{{ trans('estimation.unit') }}</th>
                                    <th width="">{{ trans('estimation.total_cost') }}</th>
                                    <th width="">{{ trans('payment.to_recieve') }}</th>
                                    <th width="">{{ trans('payment.to_request') }}</th>
                                </thead>     
                                <tbody>
                                   @if($paymentRequest->paymentRequestAcitvity()->count() > 0)
                                    @foreach ($paymentRequest->paymentRequestAcitvity()->get() as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->billofQuantity()->first()->operation_type}}</td>
                                        </tr>
                                    @endforeach
                                   @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Item-->
            </div>
            <!--end::Section-->
        

        </div>
    </div> --}}
    <!--end::Portlet-->
@endsection