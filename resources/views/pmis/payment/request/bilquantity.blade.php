@if($bill_quantities)
    {{-- Start of the content div --}}
    <div class="m-portlet m-portlet--mobile border_radiuse"> 
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn m-btn--pill btn-success btn-sm" href="javascript:void(0)" data-toggle="modal" data-target="#payment_request_modal">
                            <span><i class="fa fa-plus"></i> <span>{{ trans('payment.store_payment') }}</span></span>
                        </a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('payment_request.index') }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom">
                    <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="20%">{{ trans('estimation.operation_type') }}</th>
                                <th width="10%">{{ trans('estimation.amount') }}</th>
                                <th width="10%">{{ trans('estimation.unit') }}</th>
                                <th width="10%">{{ trans('estimation.cost') }}</th>
                                <th width="10%">{{ trans('estimation.total_cost') }}</th>
                                <th width="10%">{{ trans('estimation.activity_done') }} %</th>
                                <th width="15%">{{ trans('payment.to_request') }}</th>
                                <th width="10%">{{ trans('global.choose') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $prev_invo = 0 ; 
                                $requested_act = 0; 
                            @endphp
                            @foreach ($bill_quantities as $item) {{-- Foreach all done activity   --}}    
                                @foreach ($requested_activities as $ractv)  {{-- Foreach Prevoise payment  --}}
                                    @if($ractv->bq_id == $item->id)
                                        @php
                                            $requested_act = $ractv->request_amount; 
                                            $prev_invo     += $requested_act * $item->price; //total price of previose invoice of this project       
                                        @endphp
                                    @endif                                       
                                @endforeach
                                <tr>
                                    @php 
                                        if($requested_act < $item->activity_total){
                                            $val = $item->activity_total-$requested_act;
                                        }
                                        else{
                                            $val = 0;
                                        }
                                    @endphp
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->operation_type}}</td>
                                    <td>{{$item->amount}}</td>
                                    <td>{{ $item->{'name_'.$lang} }}</td>
                                    <td>{{$item->price }}</td>
                                    <td>{{$item->price * $item->amount }}</td>
                                    <td>{{$item->activity_total * 100 / $item->amount }} %</td>
                                    <td><input id="request_amount_{{$item->id}}" name="request_amount[]" style="width:100%;" type="number" onblur="filterPaymentMaxValue('{{$item->activity_total}}','{{$requested_act}}',this)" value="{{$val}}"  min="1" max="{{$item->activity_total - $requested_act}}" step="0.1"></td>
                                    <td>
                                        <label class="m-checkbox m-checkbox--air m-checkbox--state-success">
                                            <input type="checkbox" <?=in_array($item->id,array_pluck($selected_bq,'bq_id'))==true ? 'checked' : ''?>  id="check_{{$item->id}}"  onchange="addActivityPayment('{{route('add_activity_to_payment')}}','project_id={{$project_id}}&&project_location_id={{$project_location_id}}&&bq_id={{$item->id}}&&proj_price={{$estimation->cost}}&&activity_price={{$project_bq->first()->bq_total_price}}&&item_price={{$item->price}}&&all_done_act_price={{$all_done_act_price}}&&prev_invo={{$previouse_inv_total_money}}','POST','request_act_package','{{$item->id}}')">
                                            <span class="rotate"></span>
                                        </label>
                                    </td>
                                </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xsm-12">
            <div class="m-section__content">
                <!--begin::Preview-->
                <div class="m-demo">
                    <div class="m-demo__preview m-demo__preview_cutome">
                        <h3 class="m-section__heading">{{trans('payment.prequest_summary')}}</h3>
                        <div class="m-list__content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="request_act_package"> 
                                        <div class="mt-2 inline-headers">
                                            <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.activity_no')}} :</h5>
                                            <h6>{{session('payment_request.counter')}}</h6>   
                                        </div>
                                        <div class="mt-2 inline-headers">
                                            <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.reqtotal_money')}} :</h5>
                                            <h6>{{session('payment_request.money_total')}}</h6>   
                                        </div> 
                                        <div class="mt-2 inline-headers">
                                            <h5 class="mr-4 font-weight-lighter text-default">{{trans('payment.reqtotal_percentage')}} :</h5>
                                            <h6>0</h6>   
                                        </div> 
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Preview-->
            </div>
        </div>
    </div>
    <!--Begin::Section-->
    <div class="m-portlet">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-xl-6">
                    <!--begin:: Widgets/Stats2-1 -->
                    <div class="m-widget1">
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">{{trans('payment.project_price')}}</h3>
                                    <span class="m-widget1__desc">{{trans('payment.project_price_contract')}}</span>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-brand">{{number_format($procurement->contract_price)}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">{{trans('payment.act_total_money')}}</h3>
                                    <span class="m-widget1__desc">{{trans('payment.acto_total_m_note')}}</span>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">{{$project_bq->first()->bq_total_price}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">{{trans('payment.progress')}}</h3>
                                    <span class="m-widget1__desc">{{trans('payment.progress_note')}}</span>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-danger">{{$all_done_act_price * 100 / $project_bq->first()->bq_total_price}}%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Stats2-1 -->
                </div>
                <div class="col-xl-4">
                    <!--begin:: Widgets/Stats2-1 -->
                    <div class="row m-row--no-padding align-items-center inv-heading">
                        <div class="col">
                            <h4 class="m-widget1__title">{{trans('payment.inv_sum_info')}}</h4>
                        </div>
                    </div>                
                    <!--end:: Widgets/Stats2-1 -->
                    <!--begin:: echart donut-->
                    <div  id="invoice_chart"></div>
                    <!--end:: echart donut-->
                </div>
            </div>
        </div>
    </div>
    <!--End::Section-->
@else
    <div class="col-xl-9 col-lg-12">
        <div class="m-wizard__form">
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="p-5 font-weight-bold text-danger" style="font-size:1.2rem">
                    <dt>{{ trans('global.no_records') }}</dt>
                </div>
            </div>  
        </div>
    </div>
@endif
@include('assets.apexcharts')
    <script>
     
         var options = {
          series: [{{$all_done_act_price - $prev_invo}}, {{$prev_invo}}, {{$project_bq->first()->bq_total_price - $all_done_act_price}}],
          labels: ["{{ trans('payment.inv_requestable') }}", "{{ trans('payment.inv_requested') }}", "{{ trans('payment.remained_inv') }}"],
          chart: {
          type: 'donut',
        },
        responsive: [{
          breakpoint: 100,
          options: {
            chart: {
              width: 200,
              offsetY: 50
            },
            legend: {
              position: 'top'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#invoice_chart"), options);
        chart.render();
    </script>
