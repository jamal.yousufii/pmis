
<div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
    <div class="m-portlet__body">
        <form class="m-form m-form--fit m-form--label-align-right" id="payment_bquantity">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xsm-12">
                        <label for="example-text-input" class="title-custom">{{ trans('global.location') }}:</label>
                        <div id="div_project_location_id" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2 required" name="project_location_id" id="project_location_id" style="width: 100%;">
                                <option value="">{{ trans('global.select') }}</option>
                                @if($project_location)
                                    @foreach($project_location as $item)
                                    <option value="{{$item->id}}">{{ ($item->province? $item->province->{'name_'.$lang} : '') }} / {{ ($item->district? $item->district->{'name_'.$lang} : '') }} / {{ ( $item->village ? $item->village->{'name_'.$lang} : '' ) }}  / {{ ( $item->latitude ? $item->latitude : '') }} / {{ ($item->longitude ? $item->longitude : '') }} </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="project_location_id error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xsm-12">
                        <label for="example-text-input" class="title-custom">{{ trans('global.status') }}:</label>
                        <select class="form-control m-input m-input--air select-2" name="bq_status" >
                            <option value="">{{ trans('global.select') }}</option>
                            <option value="">{{trans('global.completed')}}</option>
                            <option value="">{{trans('global.under_process')}}</option>
                            <option value="">{{trans('global.not_started')}}</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xsm-12 pt-32">
                        <a href="#" class="btn btn-success m-btn s-btn--custom m-btn--icon">
                            <span>
                                <i class="la la-search"></i>
                                <span>{{trans('global.search')}}</span>
                            </span>
                        </a>
                    </div>  
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Start of the payment request modal -->
<div id="payment_request_modal" class="modal fade bd-example-modal-md" role="dialog" aria-labelledby="SummaryModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">           
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white">{{ trans('payment.store_payment_request') }}</h3>
                    </div>
                </div>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
            </div>
            <div class="m-content" id="resutl_div">
               <form class="m-form m-form--fit m-form--label-align-right" id="payment_request_form" method="POST" action="{{route('payment_request.store')}}" >
                    @csrf
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="request_number">{{trans('payment.request_number')}} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control errorDiv required m-input" id="request_number" name="request_number"  placeholder="{{trans('payment.request_number')}}">
                            <div class="request_number error-div" style="display:none;"></div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="request_date">{{trans('payment.request_date')}} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control errorDiv required datePicker m-input" id="request_date" name="request_date" placeholder="{{trans('payment.request_date')}}">
                            <div class="request_date" style="display:none;"></div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="description">{{trans('payment.description')}}</label>
                            <textarea type="text" class="form-control m-input" name="description" id="description"></textarea>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions pull-left">
                            <button type="button" onclick="validateSubmit('payment_request_form')" class="btn btn-primary">{{trans('global.submit')}}</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal  End-->

