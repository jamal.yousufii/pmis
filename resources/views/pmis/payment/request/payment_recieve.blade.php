@alert()
@endalert

<div class="m-portlet m-portlet--tab border_radiuse">
        <div class="m-portlet__body pb-1">
            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
                 <h3 class="m-portlet__head-text custom-heading">{{trans('payment.inv_details')}}</h3>
                 <div class="row">
                    <div class="col-lg-4">
                        <h6>{{trans('payment.inv_req_numb')}} :‌ {{$payment_request->first()->request_number}}</h6>
                     </div>
                     <div class="colo-lg-4">
                         <h6>{{trans('payment.inv_req_status')}} :  
                               
                                @if($payment_request->status==6)
                                    <span class="">{{ trans('requests.approved') }}</span>
                                @elseif($payment_request->status==-1)
                                    <span class="ba">{{ trans('requests.rejected') }}</span>
                                @else
                                    <span class="">{{ trans('requests.pending') }}</span>
                                @endif
                         </h6>
                     </div>
                     <div class="col-lg-4 text-right">
                       @if(canRejectRecievePayments($paymentRequest->status))  
                        <button type="button" onclick="confirmServerRequest('{{route('payment_recieve')}}','payment_request_id={{$payment_request->id}}&&status=approve','POST','invoice_details','<?=in_array($payment_request->status,config('static.inv_rcv.send')) ? trans('payment.send_msg') : ($payment_request->status==5) ? trans('payment.complt_msg') : trans('payment.inv_recieve_msg')?>')" class="btn m-btn--pill btn-primary btn-sm"><?=in_array($payment_request->status,config('static.inv_rcv.send')) ? trans('payment.send') : ($payment_request->status==5) ? trans('global.completed') :  trans('payment.recieve')?></button> |
                        <button type="button"  onclick="confirmServerRequest('{{route('payment_recieve')}}','payment_request_id={{$payment_request->id}}&&status=reject','POST','invoice_details','{{trans('payment.rej_inv_recieve')}}')" class="btn m-btn--pill btn-danger btn-sm">{{trans('global.reject')}}</button>
                       @endif 
                     </div>
                </div>
                <!--begin: Message container -->
                <div class="m-portlet__padding-x">
                    
                    <!-- Here you can put a message or alert -->
                </div>
                <!--end: Message container -->

                <!--begin: Form Wizard Head -->
                <div class="m-wizard__head m-portlet__padding-x m-wizard__head_custome">

                    <!--begin: Form Wizard Progress -->
                    <div class="m-wizard__progress">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: {{config('static.inv_rcv.'.$payment_request->status)}}%" aria-valuenow="100" aria-valuemin="50" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <!--end: Form Wizard Progress -->

                    <!--begin: Form Wizard Nav -->
                    <div class="m-wizard__nav">
                        <div class="m-wizard__steps">
                            <div class="m-wizard__step <?=!in_array($payment_request->status,[0,-1]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_1">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-link"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        1. {{trans('payment.ensijam')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.first_step')}}
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step <?=!in_array($payment_request->status,[-1,0,1,2]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_2">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-project-diagram"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        2. {{trans('payment.procurement')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.procurement_step')}}
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step <?=!in_array($payment_request->status,[-1,0,1,2,3,4]) ? 'm-wizard__step--current' : ''?>" m-wizard-target="m_wizard_form_step_3">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-money-bill-alt"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        {{trans('payment.finance')}}
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        {{trans('payment.finance_step')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>
                <!--end: Form Wizard Head -->
            </div>
            <!--end: Form Wizard-->
        </div>
     </div>