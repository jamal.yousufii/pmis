<div id="beneficiary_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-4">
        <select class="form-control m-input m-input--air select-2" name="beneficiary[]">
            <option value="">{{ trans('global.select') }}</option>
            @if($beneficiary)
                @foreach($beneficiary as $item)
                    <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-lg-3">
        <input class="form-control m-input" type="number" min="0" name="total[]">
    </div>
    <div class="col-lg-4">
        <input class="form-control m-input" type="text" name="comments[]">
    </div>
    <div class="col-lg-1">
        <button type="button" id="beneficiary_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('beneficiary_div_{!!$number!!}',{!!$number!!},'beneficiary_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>