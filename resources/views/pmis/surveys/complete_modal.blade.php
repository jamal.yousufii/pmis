{{-- Start of the Modal --}}
<div id="completeModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="completeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
                <!--begin::head-->
                <div class="m-portlet__head table-responsive bg-dark">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text text-white" style="font-size: 1.3rem">{{ trans('progress.complete') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Head-->
            </div>
            <div class="m-portlet__body">
                <!--begin::content-->
                <form class="m-form m-form--label-align-right" enctype="multipart/form-data" id="completedForm">
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            <label class="">{{ trans('plans.description') }}: <span style="color:red;">*</span></label>
                            <textarea class="form-control m-input m-input--air errorDiv" name="description" id="description" rows="4"></textarea>
                            <div class="description error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label class="">{{ trans('global.attachment') }}: <span style="color:red;">*</span></label>
                            <div class="custom-file p-0">
                                <input type="file" class="custom-file-input errorDiv" name="file" id="file" onchange="chooseFile(this.id)" required>
                                <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                            </div>
                            <div class="file error-div" style="display:none;"></div>
                            <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions align">
                            <input name="project_id" type="hidden" value="{{ $enc_id }}"/>
                            <input name="section" type="hidden" value="pmis_survey"/>
                            <input name="status" type="hidden" value="1"/>
                            <button type="button" onclick="storeRecord('{{route('projectStatusAll')}}','completedForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
</div>