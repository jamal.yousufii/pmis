<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
    <div class="col-lg-12">
        <div class="m-portlet">
            <div class="m-portlet__head table-responsive">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('surveys.view') }}</h3>
                    </div>
                </div>
                 <!-- Include Attachments Modal -->
                @include('pmis.attachments.modal')
                @include('pmis.attachments.documnet_modal')
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-success btn-sm m-btn float-right" href="{{ route('survey_print', [$enc_id, encrypt($record->id)]) }}">
                                <span><i class="la la-print"></i> <span>{{ trans('global.report_pdf') }}</span></span>
                            </a>
                        </li>
                        <li class="m-portlet__nav-item">
                          <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','record_id={{encrypt($record->id)}}&&parent_id={{encrypt($record->project_id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
                           </a>
                        </li>
                        <!-- Dcoument of Record -->
                        <li class="m-portlet__nav-item">
                          <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('document.create')}}','record_id={{encrypt($record->id)}}&table={{encrypt('survey_documents')}}&section={{encrypt($section)}}','POST','document-div')" data-toggle="modal" data-target="#documnet_modals">
                                <span><i class="fa fa-folder-open"></i> <span>{{ trans('surveys.related_document') }}  [{{ $record->documents->count() }}] </span></span>
                           </a>
                        </li>
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","view_pro_summary"))
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                </a>
                            </li>
                        @endif
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="javascript:void()" onclick="redirectFunction()">
                                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-4">
                            <label class="title-custom">{{ trans('surveys.project_location') }}:</label><br>
                            <span>
                                {{$record->project_location->province->{'name_'.$lang} }} 
                                @if($record->project_location->district_id) / {{ $record->project_location->district->{'name_'.$lang} }} @endif 
                                @if($record->project_location->village_id) / {{ $record->project_location->village->{'name_'.$lang} }} @endif  
                                @if($record->project_location->latitude) / {{ $record->project_location->latitude }} @endif 
                                @if($record->project_location->longitude) / {{ $record->project_location->longitude }} @endif
                            </span>
                        </div>
                        <div class="col-lg-4">
                            <label class="title-custom">{{ trans('surveys.start_date') }}:</label><br>
                            <span>{{dateCheck($record->start_date,$lang)}}</span>
                        </div>
                        <div class="col-lg-4">
                            <label class="title-custom">{{ trans('surveys.end_date') }}:</label><br>
                            <span>{{dateCheck($record->end_date,$lang)}}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom table-responsive pt-0">
                        <table class="table">
                            <thead>
                                <tr class="text-title">
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.measurement')}}</th>
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.unit')}}</th>
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.quantity')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($record->measurements)
                                    @foreach($record->measurements as $item)
                                        <tr>
                                            <td>@if($item->static_measurement_type){{ $item->static_measurement_type->{'name_'.$lang} }}@endif</td>
                                            <td>@if($item->static_measurement_unit){{ $item->static_measurement_unit->{'name_'.$lang} }}@endif</td>
                                            <td>{{ $item->quantity }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="row m-form__group_custom px-4">
                        <div class="col-lg-12 text-title px-4"><span>{{ trans('surveys.surv_team') }}</span></div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-4">
                            <label class="title-custom">{{ trans('surveys.head_team') }}:</label><br>
                            <span>{{ $record->employee->first_name }}</span>
                        </div>
                        <div class="col-lg-8">
                            <label class="title-custom">{{ trans('surveys.emp_name') }}:</label><br>
                            <span>{{ $employees }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom table-responsive pt-0">
                        <table class="table">
                            <thead>
                                <tr class="text-title">
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.beneficiary')}}</th>
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.total')}}</th>
                                    <th width="33.33%" style="border-top:0px;">{{trans('surveys.comment')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($record->beneficiaries)
                                    @foreach($record->beneficiaries as $item)
                                        <tr>
                                            <td>@if($item->users){{ $item->users->{'name_'.$lang} }}@endif</td>
                                            <td>{{ $item->total }}</td>
                                            <td>{{ $item->comments }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- Aabedat Details -->
                    @if($plan->category_id==3)
                        @if($aabedat) 
                            @php $entity = ''; @endphp
                            @foreach($aabedat as $item)
                                @if($entity!=$item->entity_id)
                                    @php 
                                        $data['aabedat'] = $aabedat;
                                        $data['code']    = $item->entity_id;
                                        $data['lang']    = $lang;
                                        $data['is_edit'] = '';
                                    @endphp
                                    @include('pmis/surveys/survey_aabedat/'.$item->entity->code,$data)
                                @endif
                                @php $entity = $item->entity_id; @endphp
                            @endforeach
                        @endif
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('surveys.water_well') }}:</label><br>
                                <span>{{ $record->well }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('surveys.naldawani') }}:</label><br>
                                <span>{{ $record->piping }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('surveys.pool') }}:</label><br>
                                <span>{{$record->pool}}</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="title-custom">{{ trans('surveys.sewage') }}:</label><br>
                                <span>{{$record->wastewater}}</span>
                            </div>
                        </div>
                    @endif
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            <label class="title-custom">{{ trans('surveys.description') }}:</label><br>
                            <span>{!! $record->description !!}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
