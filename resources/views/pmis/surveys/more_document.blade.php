<div id="document_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-4">
        <select class="form-control m-input m-input--air select-2" name="document_type[]">
            <option value="">{{ trans('global.select') }}</option>
            @if($documents)
                @foreach($documents as $item)
                    <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-lg-3">
        <input class="form-control m-input" type="text" name="document_name[]">
    </div>
    <div class="col-lg-4">
        <div class="custom-file">
            <input type="file" class="custom-file-input errorDiv" name="document[]" id="document_{!!$number!!}" onchange="chooseFile(this.id)" required>
            <label class="custom-file-label" id="file-label" for="document_{!!$number!!}">{{ trans('requests.att_choose') }}</label>
        </div>
    </div>
    <div class="col-lg-1">
        <button type="button" id="document_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('document_div_{!!$number!!}',{!!$number!!},'document_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>