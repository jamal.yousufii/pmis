<html>
    <head>
        <?php
        $lang = get_language();
        if($lang=="en"){
            $dir = "";
            $direction = "";

        }else {
            $dir = ".rtl"; 
            $direction = "rtl";
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('surveys.surveys') }}</title>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('public/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public/css/custome.css')}}">      
    </head>
    <body dir="{{$direction}}">
        <table width="100%">
            <tr class="text-center">
                <td width="100%" class="text-center"><img src="{!!asset('public/img/logo.png')!!}" style="height: 115px; width: 115px"/></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-1') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-2') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-3') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ $department_name }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.project_status_report') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.pmis_survey') }}</h3></td>
            </tr>
        </table>
        <br>
        @if($record)
            <table border="1" width="100%">
                <tr>
                    <td class="text-center py-4" width="15%">{{ trans('daily_report.project_name') }}</td>
                    <td class="text-center py-4" width="35%">{{ $record->project->name }}</td>
                    <td class="text-center py-4" width="15%">{{ trans('global.status') }}</td>
                    <td class="text-center py-4" width="35%">
                        @if($record->process=='0')
                            <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
                        @elseif($record->process=='1')
                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
                        @else
                            <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center py-4">{{ trans('progress.report_date') }}</td>
                    <td class="text-center py-4">{{ dateCheck(date('Y-m-d'),$lang) }}</td>
                    <td class="text-center py-4">{{ trans('global.urn') }}</td>
                    <td class="text-center py-4">{{ $record->urn }}</td>
                </tr>
                <tr>
                    <td class="text-center py-4">{{ trans('daily_report.project_location') }}</td>
                    <td class="text-center py-4">
                        {{$record->project_location->province->{'name_'.$lang} }} 
                        @if($record->project_location->district_id) / {{ $record->project_location->district->{'name_'.$lang} }} @endif 
                        @if($record->project_location->village_id) / {{ $record->project_location->village->{'name_'.$lang} }} @endif  
                        @if($record->project_location->latitude) / {{ $record->project_location->latitude }} @endif 
                        @if($record->project_location->longitude) / {{ $record->project_location->longitude }} @endif
                    </td>
                    <td class="text-center py-4">{{ trans('plans.project_type') }}</td>
                    <td class="text-center py-4">{{ $record->project->project_type->{'name_'.$lang} }}</td>
                </tr>
                <tr>
                    <td class="text-center py-4">{{ trans('surveys.start_date') }}</td>
                    <td class="text-center py-4">{{ dateCheck($record->start_date,$lang) }}</td>
                    <td class="text-center py-4">{{ trans('surveys.end_date') }}</td>
                    <td class="text-center py-4">{{ dateCheck($record->end_date,$lang) }}</td>
                </tr>
                <tr>
                    <td class="text-center py-4">{{ trans('surveys.head_team') }}</td>
                    <td class="text-center py-4">{{ $record->employee->first_name }}</td>
                    <td class="text-center py-4">{{ trans('surveys.surv_team') }}</td>
                    <td class="text-center py-4">{{ $employees }}</td>
                </tr>
                <tr>
                    <td class="text-center py-3 title-custom" colspan="4">{{ trans('report.survey_info') }}</td>
                </tr>
                <tr>
                    <td class="text-center py-3 title-custom" colspan="2">{{ trans('surveys.measurement') }}</td>
                    <td class="text-center py-3 title-custom">{{ trans('surveys.unit') }}</td>
                    <td class="text-center py-3 title-custom">{{ trans('surveys.quantity') }}</td>
                </tr>
                @if($record->measurements)
                    @foreach($record->measurements as $item)
                        <tr>
                            <td class="text-center py-3" colspan="2">@if($item->static_measurement_type){{ $item->static_measurement_type->{'name_'.$lang} }}@endif</td>
                            <td class="text-center py-3">@if($item->static_measurement_unit){{ $item->static_measurement_unit->{'name_'.$lang} }}@endif</td>
                            <td class="text-center py-3">{{ $item->quantity }}</td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td class="text-center py-3 title-custom" colspan="2">{{ trans('surveys.beneficiary') }}</td>
                    <td class="text-center py-3 title-custom">{{ trans('surveys.total') }}</td>
                    <td class="text-center py-3 title-custom">{{ trans('surveys.comment') }}</td>
                </tr>
                @if($record->beneficiaries)
                    @foreach($record->beneficiaries as $item)
                        <tr>
                            <td class="text-center py-3" colspan="2">@if($item->users){{ $item->users->{'name_'.$lang} }}@endif</td>
                            <td class="text-center py-3">{{ $item->total }}</td>
                            <td class="text-center py-3">{{ $item->comments }}</td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td class="text-center py-3 title-custom" colspan="4">{{ trans('surveys.description') }}</td>
                </tr>
                <tr>
                    <td class="text-center py-3" colspan="4">{{ $record->description }}</td>
                </tr>
            </table>
        @endif
    </body>
</html>
