@extends('master')
@section('head')
  <title>{{ trans('surveys.surveys') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text">{{trans('global.project_list')}}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12">
              <div id="div_field" class="errorDiv"> <!-- Error Dive start for select box  -->  
                <select class="form-control m-input m-input--air select-2 required" name="field" id="field" style="width:100%;" onchange="bringSearchFiel('{{route('search.option')}}','POST',this.value,'value_input')">
                  <option value="">{{ trans('global.select') }}</option>
                  <option value="projects.urn"> {{trans('global.pro_urn') }} </option>
                  <option value="projects.name"> {{trans('plans.project_name') }} </option>
                  <option value="projects.code"> {{trans('plans.project_code') }} </option>
                </select>
              </div> 
              <div class="field error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="condition" id="condition" style="width:100%;">
                <option value="=">{{ trans('global.equail') }}</option>
                <option value="like">{{ trans('global.like') }} </option>
              </select>
            </div>
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12" id="value_input">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" name="value" class="form-control m-input required" placeholder="{{trans('global.search_dote')}}" id="search_value">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span><i class="la la-search"></i></span>
                </span>
                <div class="search_value error-div" style="display:none;"></div>
              </div>
            </div>
            {{-- Hidden Input  --}}
            <input type="hidden" name="department_id" value="{{$dep_id}}">
            <input type="hidden" name="section" value="{{session('current_section')}}">
            <input type="hidden" name="role" value="sur_all_view" />
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <a href="javascript:void;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air" onclick="searchRecords('{{route('filter_survey')}}','search','POST','searchresult')">
                <span><i class="la la-search"></i><span>{{trans('global.search')}}</span></span>
              </a>
            </div>
          </div>
        </div>
      </form>  
    </div>
    <!-- Filter End -->
    @alert()
    @endalert
    <!-- Assign project to users: Start -->
    <div id="assign_project" class="modal fade bd-example-modal-md" role="dialog" aria-labelledby="assign_project" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header bg-color-dark pb-0">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white"><i class="fa flaticon-user-add"></i></h3>
                    </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                </div>
                <div class="m-content pt-0">
                    <form class="m-form m-form--fit m-form--label-align-right" id="assign_form" method="POST" enctype="multipart/form-data">
                        <div class="m-portlet__body py-3">
                            <div class="form-group m-form__group  py-0 pt-3 px-0"">
                                <label for="request_number">{{ trans("global.section_owner") }}<span style="color:red;">*</span></label>
                                <div id="user_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" multiple name="user_id[]" style="width: 100%">
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($users)
                                            @foreach($users as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="user_id error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions pull-left py-0 pt-3 px-0"">
                              <input type="hidden" name="department_id" value="{{$dep_id}}" />
                              <input type="hidden" name="section" value="pmis_survey" />
                              <input type="hidden" name="record_id" id="rec_id" value="" />
                              <input type="hidden" name="record_urn" id="rec_urn" value="" />
                              <button type="button" onclick="storeRecord('{{ route('survey.assign_users') }}','assign_form','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans("global.submit") }}</button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{ trans("global.cancel") }}</button>
                            </div>
                        </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Assign project to users: End -->
    <!-- List project`s users: Start -->
    <div id="project_users" class="modal fade bd-example-modal-md" role="dialog" aria-labelledby="project_users" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header bg-color-dark pb-0">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text text-white"><i class="fa flaticon-user-add"></i></h3>
                    </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                </div>
                <div class="m-content p-1" id="modal_content"></div>
            </div>
        </div>
    </div>
    <!-- Assign project to users: End -->
    <div class="m-portlet__body table-responsive" id="searchresult">
      <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
        <thead>
          <tr>
            <th width="13%">{{ trans('global.pro_urn') }}</th>
            <th width="20%">{{ trans('plans.project_name') }}</th>
            <th width="12%">{{ trans('plans.project_code') }}</th>
            <th width="15%">{{ trans('global.shared_from') }}</th>
            <th width="11%">{{ trans('global.shared_by') }}</th>
            <th width="9%">{{ trans('global.status') }}</th>
            <th width="12%">{{ trans('global.section_owner') }}</th>
            <th width="8%">{{ trans('global.action') }}</th>
          </tr>
        </thead>
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          @if($records)
            @foreach($records AS $rec)
              <?php
              $enc_id = encrypt($rec->id); ?>
              <tr @if(!isset($rec->survey['id'])) style="color:red;" @endif>
                <td>{!!$rec->urn!!}</td>
                <td>{!!$rec->name!!}</td>
                <td>{!!$rec->code!!}</td>
                <td>{!!$rec->share()->where('share_to_code','pmis_survey')->first()->from_department->{'name_'.$lang}!!}</td>
                <td>{!!$rec->share()->where('share_to_code','pmis_survey')->first()->user->name!!}</td>
                <td>
                  @if(isset($rec->survey['process']) and $rec->survey['process']=='0')
                    <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
                  @elseif(isset($rec->survey['process']) and $rec->survey['process']=='1')
                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
                  @else
                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
                  @endif
                </td>
                <td>
                  @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_owner"))
                    <button type="button" onclick="add_owner({{ $rec->id }},{{ $rec->urn }})" data-toggle="modal" data-target="#assign_project" id="add_owner_btn" class="btn btn-circle btn-icon btn-sm btn-info mx-2"> 
                      <i class="fa fa-plus-circle"></i>
                    </button>
                  @endif
                  @if($rec->project_user()->where('section','pmis_survey')->first())
                    <button type="button" onclick="list_owner('{{route('survey.list_assigned_users', $rec->id)}}', 'modal_content')" data-toggle="modal" data-target="#project_users" id="list_owner_btn" class="btn btn-circle btn-icon btn-sm mx-2 btn-success"> 
                      <i class="fa fa-users"></i>
                    </button>
                  @endif
                </td>
                <td>
                  <span class="dtr-data">
                    <span class="dropdown">
                      <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_view"))
                          <a class="dropdown-item" href="{{ route('list_survey',$enc_id) }}"><i class="la la-eye"></i>{{ trans('global.view_details') }}</a>
                        @endif
                      </div>
                    </span>
                  </span>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection