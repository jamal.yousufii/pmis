<!-- Add Aabedat Entities -->
@if(!$aabedat and $is_edit=='')
    <div id="{{$div_id}}_{{$counter}}">
        <div class="row m-form__group_custom">
            <div class="col-lg-12 text-title">
                {{ trans('surveys.masalah_sangi') }}
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="">{{ trans('surveys.material') }}:</label>
                <input class="form-control m-input" type="text" name="material[]">
            </div>
            <div class="col-lg-6">
                <label class="">{{ trans('surveys.resistance') }}:</label>
                <input class="form-control m-input" type="text" name="strenght[]">
            </div>
        </div>
        <input type="hidden" name="entities[]" value="{{$div_id}}" />
        <input type="hidden" name="sub_id[]" value="" />
        <input type="hidden" name="type[]" value="">
        <input type="hidden" name="thickness[]" value="">
        <input type="hidden" name="number[]" value="">
        <input type="hidden" name="quality[]" value="">
        <input type="hidden" name="destroy_component[]" value="">
        <input type="hidden" name="height[]" value="">
        <input type="hidden" name="width[]" value="">
        <input type="hidden" name="length[]" value="">
        <input type="hidden" name="stair[]" value="">
        <input type="hidden" name="engine_specification[]" value="">
        <input type="hidden" name="shape[]" value="">
        <input type="hidden" name="perseverance[]" value="">
        <input type="hidden" name="moisture_status[]" value="">
        <input type="hidden" name="water_well[]" value="">
        <input type="hidden" name="plumbery[]" value="">
        <input type="hidden" name="pool[]" value="">
        <input type="hidden" name="sewage[]" value="">
    </div>
<!-- Edit Aabedat Entities -->
@elseif($aabedat and $is_edit=='1')
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">
            {{ trans('surveys.masalah_sangi') }}
        </div>
    </div>
    @foreach($aabedat as $aabedatitem)
        @if($aabedatitem->entity_id==$code)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-6">
                    <label class="">{{ trans('surveys.material') }}:</label>
                    <input class="form-control m-input" type="text" name="material[]" value="{{$aabedatitem->material}}">
                </div>
                <div class="col-lg-6">
                    <label class="">{{ trans('surveys.resistance') }}:</label>
                    <input class="form-control m-input" type="text" name="strenght[]" value="{{$aabedatitem->strenght}}">
                </div>
            </div>
            <input type="hidden" name="entities[]" value="{{$aabedatitem->entity_id}}" />
            <input type="hidden" name="sub_id[]" value="{{$aabedatitem->id}}" />
            <input type="hidden" name="type[]" value="">
            <input type="hidden" name="thickness[]" value="">
            <input type="hidden" name="number[]" value="">
            <input type="hidden" name="quality[]" value="">
            <input type="hidden" name="destroy_component[]" value="">
            <input type="hidden" name="height[]" value="">
            <input type="hidden" name="width[]" value="">
            <input type="hidden" name="length[]" value="">
            <input type="hidden" name="stair[]" value="">
            <input type="hidden" name="engine_specification[]" value="">
            <input type="hidden" name="shape[]" value="">
            <input type="hidden" name="perseverance[]" value="">
            <input type="hidden" name="moisture_status[]" value="">
            <input type="hidden" name="water_well[]" value="">
            <input type="hidden" name="plumbery[]" value="">
            <input type="hidden" name="pool[]" value="">
            <input type="hidden" name="sewage[]" value="">
        @endif
    @endforeach
<!-- View Aabedat Entities -->        
@elseif($aabedat and $is_edit=='')
<div class="col-lg-12 text-title">{{ trans('surveys.masalah_sangi') }}</div>
<div class="form-group m-form__group row m-form__group_custom">
    <table class="table">
        <thead>
            <tr>
                <th width="50%" style="border-top:0px;">{{trans('surveys.material')}}</th>
                <th width="50%" style="border-top:0px;">{{trans('surveys.resistance')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($aabedat as $item)
                @if($item->entity_id==$code)
                    <tr>  
                        <td>{{$item->material}}</td>
                        <td>{{$item->strenght}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endif