<!-- Add Aabedat Entities -->
@if(!$aabedat and $is_edit=='')
    <div id="{{$div_id}}_{{$counter}}">
        <div class="row m-form__group_custom">
            <div class="col-lg-12 text-title">
                {{ trans('surveys.walls') }}
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4">
                <label>{{ trans('surveys.type') }}:</label>
                <select class="form-control m-input m-input--air select-2" name="type[]">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($wall)
                        @foreach($wall as $item)
                            <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-lg-4">
                <label>{{ trans('surveys.shape') }}:</label>
                <select class="form-control m-input m-input--air select-2" name="shape[]">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($wall_shap)
                        @foreach($wall_shap as $item)
                            <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-lg-4">
                <label class="">{{ trans('surveys.width') }}:</label>
                <input class="form-control m-input" type="text" name="width[]">
            </div>
        </div>
        <input type="hidden" name="entities[]" value="{{$div_id}}" />
        <input type="hidden" name="sub_id[]" value="" />
        <input type="hidden" name="thickness[]" value="">
        <input type="hidden" name="strenght[]" value="">
        <input type="hidden" name="number[]" value="">
        <input type="hidden" name="quality[]" value="">
        <input type="hidden" name="destroy_component[]" value="">
        <input type="hidden" name="height[]" value="">
        <input type="hidden" name="length[]" value="">
        <input type="hidden" name="stair[]" value="">
        <input type="hidden" name="engine_specification[]" value="">
        <input type="hidden" name="perseverance[]" value="">
        <input type="hidden" name="material[]" value="">
        <input type="hidden" name="moisture_status[]" value="">
        <input type="hidden" name="water_well[]" value="">
        <input type="hidden" name="plumbery[]" value="">
        <input type="hidden" name="pool[]" value="">
        <input type="hidden" name="sewage[]" value="">
    </div>
<!-- Edit Aabedat Entities -->
@elseif($aabedat and $is_edit=='1')
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">
            {{ trans('surveys.walls') }}
        </div>
    </div>
    @foreach($aabedat as $aabedatitem)
        @if($aabedatitem->entity_id==$code)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-4">
                    <label>{{ trans('surveys.type') }}:</label>
                    <select class="form-control m-input m-input--air select-2" name="type[]">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($wall)
                            @foreach($wall as $item)
                                @if($item->id==$aabedatitem->type)
                                    <option value="{{$item->id}}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-4">
                    <label>{{ trans('surveys.shape') }}:</label>
                    <select class="form-control m-input m-input--air select-2" name="shape[]">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($wall_shap)
                            @foreach($wall_shap as $item)
                                @if($item->id==$aabedatitem->type)
                                    <option value="{{$item->id}}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-4">
                    <label class="">{{ trans('surveys.width') }}:</label>
                    <input class="form-control m-input" type="text" name="width[]" value="{{$aabedatitem->width}}">
                </div>
            </div>
            <input type="hidden" name="entities[]" value="{{$aabedatitem->entity_id}}" />
            <input type="hidden" name="sub_id[]" value="{{$aabedatitem->id}}" />
            <input type="hidden" name="thickness[]" value="">
            <input type="hidden" name="strenght[]" value="">
            <input type="hidden" name="number[]" value="">
            <input type="hidden" name="quality[]" value="">
            <input type="hidden" name="destroy_component[]" value="">
            <input type="hidden" name="height[]" value="">
            <input type="hidden" name="length[]" value="">
            <input type="hidden" name="stair[]" value="">
            <input type="hidden" name="engine_specification[]" value="">
            <input type="hidden" name="perseverance[]" value="">
            <input type="hidden" name="material[]" value="">
            <input type="hidden" name="moisture_status[]" value="">
            <input type="hidden" name="water_well[]" value="">
            <input type="hidden" name="plumbery[]" value="">
            <input type="hidden" name="pool[]" value="">
            <input type="hidden" name="sewage[]" value="">
        @endif
    @endforeach
<!-- View Aabedat Entities -->        
@elseif($aabedat and $is_edit=='')
<div class="col-lg-12 text-title">{{ trans('surveys.walls') }}</div>
<div class="form-group m-form__group row m-form__group_custom">
    <table class="table">
        <thead>
            <tr>
                <th width="33.3%" style="border-top:0px;">{{trans('surveys.type')}}</th>
                <th width="33.3%" style="border-top:0px;">{{trans('surveys.shape')}}</th>
                <th width="33.3%" style="border-top:0px;">{{trans('surveys.width')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($aabedat as $item)
                @if($item->entity_id==$code)
                    <tr>  
                        <td>{{getStaticNameByID('wall',$item->type,$lang)}}</td>
                        <td>{{getStaticNameByID('wall_shape',$item->shape,$lang)}}</td>
                        <td>{{$item->width}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endif