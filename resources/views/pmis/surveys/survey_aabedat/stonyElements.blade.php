<!-- Add Aabedat Entities -->
@if(!$aabedat and $is_edit=='')
    <div id="{{$div_id}}_{{$counter}}">
        <div class="row m-form__group_custom">
            <div class="col-lg-12 text-title">
                {{ trans('surveys.anasur_sangi') }}
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.width') }}:</label>
                <input class="form-control m-input" type="text" name="width[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.quality') }}:</label>
                <input class="form-control m-input" type="text" name="quality[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.resistance') }}:</label>
                <input class="form-control m-input" type="text" name="strenght[]">
            </div>
                <div class="col-lg-3">
                <label class="">{{ trans('surveys.durability') }}:</label>
                <input class="form-control m-input" type="text" name="perseverance[]">
            </div>
        </div>
        <input type="hidden" name="entities[]" value="{{$div_id}}" />
        <input type="hidden" name="sub_id[]" value="" />
        <input type="hidden" name="type[]" value="">
        <input type="hidden" name="thickness[]" value="">
        <input type="hidden" name="number[]" value="">
        <input type="hidden" name="destroy_component[]" value="">
        <input type="hidden" name="height[]" value="">
        <input type="hidden" name="length[]" value="">
        <input type="hidden" name="stair[]" value="">
        <input type="hidden" name="engine_specification[]" value="">
        <input type="hidden" name="shape[]" value="">
        <input type="hidden" name="material[]" value="">
        <input type="hidden" name="moisture_status[]" value="">
        <input type="hidden" name="water_well[]" value="">
        <input type="hidden" name="plumbery[]" value="">
        <input type="hidden" name="pool[]" value="">
        <input type="hidden" name="sewage[]" value="">
    </div>
<!-- Edit Aabedat Entities -->
@elseif($aabedat and $is_edit=='1')
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">
            {{ trans('surveys.anasur_sangi') }}
        </div>
    </div>
    @foreach($aabedat as $aabedatitem)
        @if($aabedatitem->entity_id==$code)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.width') }}:</label>
                    <input class="form-control m-input" type="text" name="width[]" value="{{$aabedatitem->width}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.quality') }}:</label>
                    <input class="form-control m-input" type="text" name="quality[]" value="{{$aabedatitem->quality}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.resistance') }}:</label>
                    <input class="form-control m-input" type="text" name="strenght[]" value="{{$aabedatitem->strenght}}">
                </div>
                    <div class="col-lg-3">
                    <label class="">{{ trans('surveys.durability') }}:</label>
                    <input class="form-control m-input" type="text" name="perseverance[]" value="{{$aabedatitem->perseverance}}">
                </div>
            </div>

            <input type="hidden" name="entities[]" value="{{$aabedatitem->entity_id}}" />
            <input type="hidden" name="sub_id[]" value="{{$aabedatitem->id}}" />
            <input type="hidden" name="type[]" value="">
            <input type="hidden" name="thickness[]" value="">
            <input type="hidden" name="number[]" value="">
            <input type="hidden" name="destroy_component[]" value="">
            <input type="hidden" name="height[]" value="">
            <input type="hidden" name="length[]" value="">
            <input type="hidden" name="stair[]" value="">
            <input type="hidden" name="engine_specification[]" value="">
            <input type="hidden" name="shape[]" value="">
            <input type="hidden" name="material[]" value="">
            <input type="hidden" name="moisture_status[]" value="">
            <input type="hidden" name="water_well[]" value="">
            <input type="hidden" name="plumbery[]" value="">
            <input type="hidden" name="pool[]" value="">
            <input type="hidden" name="sewage[]" value="">
        @endif
    @endforeach
<!-- View Aabedat Entities -->        
@elseif($aabedat and $is_edit=='')
<div class="col-lg-12 text-title">{{ trans('surveys.anasur_sangi') }}</div>
<div class="form-group m-form__group row m-form__group_custom">
    <table class="table">
        <thead>
            <tr>
                <th width="25%" style="border-top:0px;">{{trans('surveys.width')}}</th>
                <th width="25%" style="border-top:0px;">{{trans('surveys.quality')}}</th>
                <th width="25%" style="border-top:0px;">{{trans('surveys.resistance')}}</th>
                <th width="25%" style="border-top:0px;">{{trans('surveys.durability')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($aabedat as $item)
                @if($item->entity_id==$code)
                    <tr>  
                        <td>{{$item->width}}</td>
                        <td>{{$item->quality}}</td>
                        <td>{{$item->strenght}}</td>
                        <td>{{$item->perseverance}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endif