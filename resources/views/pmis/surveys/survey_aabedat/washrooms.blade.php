<!-- Add Aabedat Entities -->
@if(!$aabedat and $is_edit=='')
    <div id="{{$div_id}}_{{$counter}}">
        <div class="row m-form__group_custom">
            <div class="col-lg-12 text-title">
                {{ trans('surveys.toilets') }}
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.number') }}:</label>
                <input class="form-control m-input" type="number" min="0" name="number[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.hight') }}:</label>
                <input class="form-control m-input" type="text" name="height[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.length') }}:</label>
                <input class="form-control m-input" type="text" name="length[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.width') }}:</label>
                <input class="form-control m-input" type="text" name="width[]">
            </div>
        </div>
        <input type="hidden" name="entities[]" value="{{$div_id}}" />
        <input type="hidden" name="sub_id[]" value="" />
        <input type="hidden" name="type[]" value="">
        <input type="hidden" name="thickness[]" value="">
        <input type="hidden" name="strenght[]" value="">
        <input type="hidden" name="quality[]" value="">
        <input type="hidden" name="destroy_component[]" value="">
        <input type="hidden" name="stair[]" value="">
        <input type="hidden" name="engine_specification[]" value="">
        <input type="hidden" name="shape[]" value="">
        <input type="hidden" name="perseverance[]" value="">
        <input type="hidden" name="material[]" value="">
        <input type="hidden" name="moisture_status[]" value="">
        <input type="hidden" name="water_well[]" value="">
        <input type="hidden" name="plumbery[]" value="">
        <input type="hidden" name="pool[]" value="">
        <input type="hidden" name="sewage[]" value="">
</div>
<!-- Edit Aabedat Entities -->
@elseif($aabedat and $is_edit=='1')
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">
            {{ trans('surveys.toilets') }}
        </div>
    </div>
    @foreach($aabedat as $aabedatitem)
        @if($aabedatitem->entity_id==$code)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.number') }}:</label>
                    <input class="form-control m-input" type="number" min="0" name="number[]" value="{{$aabedatitem->number}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.hight') }}:</label>
                    <input class="form-control m-input" type="text" name="height[]" value="{{$aabedatitem->height}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.length') }}:</label>
                    <input class="form-control m-input" type="text" name="length[]" value="{{$aabedatitem->length}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.width') }}:</label>
                    <input class="form-control m-input" type="text" name="width[]" value="{{$aabedatitem->width}}">
                </div>
            </div>
            <input type="hidden" name="entities[]" value="{{$aabedatitem->entity_id}}" />
            <input type="hidden" name="sub_id[]" value="{{$aabedatitem->id}}" />
            <input type="hidden" name="type[]" value="">
            <input type="hidden" name="thickness[]" value="">
            <input type="hidden" name="strenght[]" value="">
            <input type="hidden" name="quality[]" value="">
            <input type="hidden" name="destroy_component[]" value="">
            <input type="hidden" name="stair[]" value="">
            <input type="hidden" name="engine_specification[]" value="">
            <input type="hidden" name="shape[]" value="">
            <input type="hidden" name="perseverance[]" value="">
            <input type="hidden" name="material[]" value="">
            <input type="hidden" name="moisture_status[]" value="">
            <input type="hidden" name="water_well[]" value="">
            <input type="hidden" name="plumbery[]" value="">
            <input type="hidden" name="pool[]" value="">
            <input type="hidden" name="sewage[]" value="">
        @endif
    @endforeach
<!-- View Aabedat Entities -->        
@elseif($aabedat and $is_edit=='')
    <div class="col-lg-12 text-title">{{ trans('surveys.toilets') }}</div>
    <div class="form-group m-form__group row m-form__group_custom">
        <table class="table">
            <thead>
                <tr>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.number')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.hight')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.length')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.width')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($aabedat as $item)
                    @if($item->entity_id==$code)
                        <tr>  
                            <td>{{$item->number}}</td>
                            <td>{{$item->height}}</td>
                            <td>{{$item->length}}</td>
                            <td>{{$item->width}}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endif