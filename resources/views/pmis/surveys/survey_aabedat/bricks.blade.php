<!-- Add Aabedat Entities -->
@if(!$aabedat and $is_edit=='')
    <div id="{{$div_id}}_{{$counter}}">
        <div class="row m-form__group_custom">
            <div class="col-lg-12 text-title">
            {{ trans('surveys.brick') }}
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-3">
                <label>{{ trans('surveys.type') }}:</label>
                <select class="form-control m-input m-input--air select-2" name="type[]">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($brick)
                        @foreach($brick as $item)
                            <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.halat_martobiat') }}:</label>
                <input class="form-control m-input" type="text"  name="moisture_status[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.quality') }}:</label>
                <input class="form-control m-input" type="text" name="quality[]">
            </div>
            <div class="col-lg-3">
                <label class="">{{ trans('surveys.resistance') }}:</label>
                <input class="form-control m-input" type="text" name="strenght[]">
            </div>
        </div>
        <input type="hidden" name="entities[]" value="{{$div_id}}" />
        <input type="hidden" name="sub_id[]" value="" />
        <input type="hidden" name="thickness[]" value="">
        <input type="hidden" name="destroy_component[]" value="">
        <input type="hidden" name="height[]" value="">
        <input type="hidden" name="length[]" value="">
        <input type="hidden" name="number[]" value="">
        <input type="hidden" name="width[]" value="">
        <input type="hidden" name="stair[]" value="">
        <input type="hidden" name="engine_specification[]" value="">
        <input type="hidden" name="shape[]" value="">
        <input type="hidden" name="perseverance[]" value="">
        <input type="hidden" name="material[]" value="">
    </div>
<!-- Edit Aabedat Entities -->
@elseif($aabedat and $is_edit=='1')
    <div class="row m-form__group_custom">
        <div class="col-lg-12 text-title">
            {{ trans('surveys.brick') }}
        </div>
    </div>
    @foreach($aabedat as $aabedatitem)
        @if($aabedatitem->entity_id==$code)
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-3">
                    <label>{{ trans('surveys.type') }}:</label>
                    <select class="form-control m-input m-input--air select-2" name="type[]">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($brick)
                            @foreach($brick as $item)
                                @if($item->id==$aabedatitem->type)
                                    <option value="{{$item->id}}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.halat_martobiat') }}:</label>
                    <input class="form-control m-input" type="text"  name="moisture_status[]" value="{{$aabedatitem->moisture_status}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.quality') }}:</label>
                    <input class="form-control m-input" type="text" name="quality[]" value="{{$aabedatitem->quality}}">
                </div>
                <div class="col-lg-3">
                    <label class="">{{ trans('surveys.resistance') }}:</label>
                    <input class="form-control m-input" type="text" name="strenght[]" value="{{$aabedatitem->strenght}}">
                </div>
            </div>
            <input type="hidden" name="entities[]" value="{{$aabedatitem->entity_id}}" />
            <input type="hidden" name="sub_id[]" value="{{$aabedatitem->id}}" />
            <input type="hidden" name="thickness[]" value="">
            <input type="hidden" name="destroy_component[]" value="">
            <input type="hidden" name="height[]" value="">
            <input type="hidden" name="length[]" value="">
            <input type="hidden" name="number[]" value="">
            <input type="hidden" name="width[]" value="">
            <input type="hidden" name="stair[]" value="">
            <input type="hidden" name="engine_specification[]" value="">
            <input type="hidden" name="shape[]" value="">
            <input type="hidden" name="perseverance[]" value="">
            <input type="hidden" name="material[]" value="">
        @endif
    @endforeach
<!-- View Aabedat Entities -->    
@elseif($aabedat and $is_edit=='')
    <div class="col-lg-12 text-title">{{ trans('surveys.brick') }}</div>
    <div class="form-group m-form__group row m-form__group_custom">
        <table class="table">
            <thead>
                <tr>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.type')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.halat_martobiat')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.quality')}}</th>
                    <th width="25%" style="border-top:0px;">{{trans('surveys.resistance')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($aabedat as $item)
                    @if($item->entity_id==$code)
                        <tr>  
                            <td>{{getStaticNameByID('brick',$item->type,$lang)}}</td>
                            <td>{{$item->moisture_status}}</td>
                            <td>{{$item->quality}}</td>
                            <td>{{$item->strenght}}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endif