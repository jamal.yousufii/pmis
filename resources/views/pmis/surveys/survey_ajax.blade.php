<table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
    <thead>
        <tr>
            <th width="15%">{{ trans('global.urn') }}</th>
            <th width="35%">{{ trans('surveys.project_location') }}</th>
            <th width="15%">{{ trans('surveys.head_team') }}</th>
            <th width="13%">{{ trans('surveys.start_date') }}</th>
            <th width="13%">{{ trans('surveys.end_date') }}</th>
            <th width="9%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
            @foreach($records AS $rec)                    
                <?php $rec_id = encrypt($rec->id); ?>
                <tr>
                    <td>{!!$rec->urn!!}</td>
                    <td>
                        @if($rec->project_location->province_id){{$rec->project_location->province->{'name_'.$lang} }} @endif
                        @if($rec->project_location->district_id) / {{ $rec->project_location->district->{'name_'.$lang} }} @endif 
                        @if($rec->project_location->village_id) / {{ $rec->project_location->village->{'name_'.$lang} }} @endif  
                        @if($rec->project_location->latitude) / {{ $rec->project_location->latitude }} @endif 
                        @if($rec->project_location->longitude) / {{ $rec->project_location->longitude }} @endif
                    </td>
                    <td>@if($rec->employee){!!$rec->employee->first_name!!}@endif</td>
                    <td>{!! dateCheck($rec->start_date,$lang) !!}</td>
                    <td>{!! dateCheck($rec->end_date,$lang) !!}</td>
                    <td>
                        
                        <span class="dtr-data">
                            <span class="dropdown">
                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_view"))
                                        <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('surveys.show',$rec_id) }}','','GET','response_div')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                    @endif
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_edit") and !$shared)
                                        <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('surveys.edit',$rec_id )}}','','GET','response_div')"><i class="la la-edit"></i> {{ trans('global.edit') }}</a>
                                    @endif
                                </div>
                            </span>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
    </table>
<!-- Pagination -->
@if(!empty($records))
    {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if ($(this).attr('href') != '#') {
            document.cookie = "no="+$(this).text();
                var dataString = '';
                dataString += "&page="+$(this).attr('id')+"&ajax="+1;
                $.ajax({
                    url: '{{ route("list_survey",$enc_id) }}',
                    data: dataString,
                    type: 'get',
                    beforeSend: function(){
                        $('#ajax_response').html('<span style="position:relative;left:10%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                    },
                    success: function(response)
                    {
                        $('#ajax_response').html(response);
                    }
            });
            }
        });
    });
</script>