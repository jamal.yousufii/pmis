<div class="form-group m-form__group row" id="surveyEmp_{!!$total!!}">
  <div class="col-lg-2">
    <label class="">{!!$total!!}:</label>
  </div>
  <div class="col-lg-4">
    <label>{{ trans('surveys.emp_name') }}:</label>
    <select class="form-control m-input m-input--air" name="emp_name_{!!$total!!}">
      <option>select an item</option>
      @if($employees)
        @foreach($employees as $emp)
        <option value="{!!$emp->id!!}">{!!$emp->first_name!!}</option>
        @endforeach
      @endif
    </select>
  </div>
  <div class="col-lg-2">
    <label class="">{{ trans('surveys.emp_remove') }}</label>
    <input class="form-control m-input btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm" onclick="loadMoreEmp();" type="button" value=" - ">
  </div>
</div>
