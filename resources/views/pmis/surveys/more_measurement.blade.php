<div id="measurement_div_{!!$number!!}" class="row form-group m-form__group row m-form__group_custom">
    <div class="col-lg-4">
        <div id="measurement" class="errorDiv">
            <select class="form-control m-input m-input--air select-2" name="measurement[]" required>
                <option value="">{{ trans('global.select') }}</option>
                @if($measurement)
                    @foreach($measurement as $meas)
                        <option value="{{$meas->id}}">{{ $meas->{'name_'.$lang} }} </option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="measurement error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <div id="measurement" class="errorDiv">
            <select class="form-control m-input m-input--air select-2" name="unit[]" required>
                <option value="">{{ trans('global.select') }}</option>
                @if($unit)
                    @foreach($unit as $item)
                        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="measurement error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-4">
        <input class="form-control m-input" type="number" min="0" name="quantity[]">
        <div class="quantity error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-1">
        <button type="button"id="measurement_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="remove_more('measurement_div_{!!$number!!}',{!!$number!!},'measurement_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>