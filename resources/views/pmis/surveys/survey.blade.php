@extends('master')
@section('head')
    <title>{{ trans('surveys.surveys') }}</title>
@endsection
@section('content')
    <!-- Include Summary Modal -->
    @include('pmis.summary.summary_modal')
    <div class="m-portlet m-portlet--mobile">
        <div class="tab-pane active" role="tabpanel">
            <div class="m-portlet m-portlet--mobile"id="showContent">
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
                        </div> 
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            @if($records->count()>0)
                                <!-- Include Attachments Modal -->
                                @include('pmis.attachments.modal')
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&table={{encrypt($table)}}&section={{encrypt('survey')}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.general_attachment') }}  [{{ $attachments->count() }}] </span></span>
                                    </a>
                                </li>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","view_pro_summary"))
                                <li class="m-portlet__nav-item">
                                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="" data-toggle="modal" data-target="#SummaryModal">
                                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                                    </a>
                                </li>   
                            @endif
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('survey',session('current_department')) }}">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div> 
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{ trans('surveys.list') }}</h3>
                        </div>    
                    </div>
                    <div class="m-portlet__head-tools">
                        <div class="m-portlet__head-tools">
                            @if(($allRecords->count()==$locations->count() || (hasSurvey($records)==2)? true: false) and !$shared  and (doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_complete") || doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_stop")))
                                <!-- Include complete & stop modal -->
                                @include('pmis.surveys.complete_modal')
                                @include('pmis.surveys.stop_modal')
                                <div class="btn-group open">
                                    <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                        <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                                    </button>
                                    <ul class="dropdown-menu mt-2" role="menu">
                                        <li>
                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_complete"))
                                                <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                    <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                </a>
                                            @endif
                                        </li>
                                        <li>
                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_stop"))
                                                <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                    <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            @endif
                            <ul class="m-portlet__nav">
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_add") and ($allRecords->count()<$locations->count() || (hasSurvey($records)==0)? true: false))
                                    <li class="m-portlet__nav-item">
                                        <a href="javascript:void()" onclick="addRecord('{{route('surveys.create')}}','plan_id={{$enc_id}}','GET','response_div')" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                            <span><i class="la la-cart-plus"></i><span>{{ trans('surveys.surv_add') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                                @if($shared and $records)
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                            <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                                        </a>
                                    </li>
                                @elseif(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_share") and ($allRecords->count()>0 || (hasSurvey($records)==2)? true: false))
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                                            <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>  
                    </div>
                </div>
                @if($records)
                    <!-- Include sharing modal -->
                    @include('pmis.surveys.share_modal')
                @endif
                @alert()
                @endalert
                @if((hasSurvey($records)==1)? true: false)   
                <div class="m-portlet__body" id="ajax_response">
                    <table class="table table-striped- table-bordered table-hover table-checkable head-has-color">
                        <thead>
                            <tr>
                                <th width="15%">{{ trans('global.urn') }}</th>
                                <th width="35%">{{ trans('surveys.project_location') }}</th>
                                <th width="15%">{{ trans('surveys.head_team') }}</th>
                                <th width="13%">{{ trans('surveys.start_date') }}</th>
                                <th width="13%">{{ trans('surveys.end_date') }}</th>
                                <th width="9%">{{ trans('global.action') }}</th>
                            </tr>
                        </thead>
                        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                            @if($records)
                                @foreach($records AS $rec)                         
                                    <?php $rec_id = encrypt($rec->id); ?>
                                    <tr>
                                        <td>{!!$rec->urn!!}</td>
                                        <td>
                                            @if($rec->project_location) {{$rec->project_location->province->{'name_'.$lang} }}
                                            @if($rec->project_location->district_id) / {{ $rec->project_location->district->{'name_'.$lang} }} @endif
                                            @if($rec->project_location->village_id) / {{ $rec->project_location->village->{'name_'.$lang} }} @endif
                                            @if($rec->project_location->latitude) / {{ $rec->project_location->latitude }} @endif 
                                            @if($rec->project_location->longitude) / {{ $rec->project_location->longitude }} @endif @endif
                                        </td>
                                        <td>@if($rec->employee){!!$rec->employee->first_name!!}@endif</td>
                                        <td>{!! dateCheck($rec->start_date,$lang) !!}</td>
                                        <td>{!! dateCheck($rec->end_date,$lang) !!}</td>
                                        <td>
                                            
                                            <span class="dtr-data">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_view"))
                                                            <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route('surveys.show',$rec_id) }}','','GET','response_div')"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                                        @endif
                                                        @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","sur_edit") and !$shared)
                                                            <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('surveys.edit',$rec_id )}}','','GET','response_div')"><i class="la la-edit"></i> {{ trans('global.edit') }}</a>
                                                        @endif
                                                    </div>
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <!-- Pagination -->
                    @if(!empty($records))
                        {!!$records->links('pagination')!!}
                    @endif
                </div>
                @elseif((hasSurvey($records)==2)? true: false) 
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-2">
                        <div class="m-portlet__body">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('surveys.data_exist') }}</label><br>
                                <span class="m-badge m-badge--warning m-badge--wide"> {{trans('global.'.$records->first()->has_survey)}} </span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label class="title-custom">{{ trans('surveys.description') }}:</label>
                                {!! $records->first()->description !!}
                            </div>
                        </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('js-code')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination a').on('click', function(event) {
                event.preventDefault();
                if ($(this).attr('href') != '#') {
                    document.cookie = "no="+$(this).text();
                    var dataString = '';
                    dataString += "&page="+$(this).attr('id')+"&ajax="+1;
                    $.ajax({
                        url: '{{ route("list_survey",$enc_id) }}',
                        data: dataString,
                        type: 'get',
                        beforeSend: function(){
                            $('#ajax_response').html('<span style="position:relative;left:10%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                        },
                        success: function(response)
                        {
                            $('#ajax_response').html(response);
                        }
                    });
                }
            });
        });
    </script>
@endsection