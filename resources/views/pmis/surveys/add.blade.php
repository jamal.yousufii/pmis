<!-- Include Summary Modal -->
@include('pmis.summary.summary_modal')
<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('surveys.surv_add') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            @if(doIHaveRoleInDep(session('current_department'),"pmis_survey","view_pro_summary"))
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                    </a>
                </li>
            @endif
          </ul>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="surveyForm" enctype="multipart/form-data">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('surveys.data_exist') }}</label>
                    <select class="form-control m-input m-input--air select-2" name="has_survey" onchange="hasSurvey(this.value,'content_fields')">
                        <option value="">{{ trans('global.select') }}</option>
                        <option value="yes" selected>{{trans('global.yes')}}</option>
                        <option value="no">{{trans('global.no')}}</option>
                    </select>
                </div>
            </div>
            <div class="content_fields" id="content_fields">   
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.project_location') }}: <span style="color:red;">*</span></label>
                        <div id="location" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2" name="location" required>
                                <option value="">{{ trans('global.select') }}</option>
                                @if($location)
                                    @foreach($location as $loc)
                                        @if(!in_array($loc->id,$locations))
                                            <option value="{{$loc->id}}">{{ $loc->province->{'name_'.$lang} }} @if($loc->district_id) / {{ $loc->district->{'name_'.$lang} }} @endif @if($loc->village_id) / {{ $loc->village->{'name_'.$lang} }} @endif  @if($loc->latitude) / {{ $loc->latitude }} @endif @if($loc->longitude) / {{ $loc->longitude }} @endif </option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="location error-div" style="display:none;"></div>
                        <span class="m-form__help">{{ trans('surveys.location_note') }}</span>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.start_date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePicker errorDiv" type="text" name="start_date" id="start_date" required>
                        <div class="start_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.end_date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePicker errorDiv" type="text" name="end_date" id="end_date" required>
                        <div class="end_date error-div" style="display:none;"></div>
                    </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.measurement') }}:</label>
                        <select class="form-control m-input m-input--air select-2" name="measurement[]">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($measurement)
                                @foreach($measurement as $meas)
                                    <option value="{{$meas->id}}">{{ $meas->{'name_'.$lang} }} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('surveys.unit') }}:</label>
                        <select class="form-control m-input m-input--air select-2" name="unit[]">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($unit)
                                @foreach($unit as $item)
                                    <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.quantity') }}:</label>
                        <input class="form-control m-input" type="number" min="0" name="quantity[]">
                    </div>
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-primary m-btn m-btn--air btn-sm mt-33" onclick="add_more('measurement_div','{{route('more_measurement')}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                    </div>
                </div>
                <div id="measurement_div"></div><!-- Display mor measurement  -->
                <div class="">
                    <div class="col-lg-12 text-title">{{ trans('surveys.surv_team') }}</div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.head_team') }}: <span style="color:red;">*</span></label>
                        <div id="survey_head" class="errorDiv">
                            <select class="form-control m-input m-input--air select-2" name="survey_head" required>
                                <option value="">{{ trans('global.select') }}</option>
                                @if($employees)
                                    @foreach($employees as $emp)
                                    <option value="{!!$emp->id!!}">{!!$emp->first_name!!}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="survey_head error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-8">
                        <label class="title-custom">{{ trans('surveys.emp_name') }}:</label>
                        <select class="form-control m-input m-input--air select-2" name="emp_name[]" multiple id="emp_name">
                            <option>{{ trans('global.select') }}</option>
                            @if($employees)
                                @foreach($employees as $emp)
                                    <option value="{!!$emp->id!!}">{!!$emp->first_name!!}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.beneficiary') }}:</label>
                        <select class="form-control m-input m-input--air select-2" name="beneficiary[]">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($beneficiary)
                                @foreach($beneficiary as $item)
                                    <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label class="title-custom">{{ trans('surveys.total') }}:</label>
                        <input class="form-control m-input" type="number" min="0" name="total[]">
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('surveys.comment') }}:</label>
                        <input class="form-control m-input" type="text" name="comments[]">
                    </div>
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-primary m-btn m-btn--air btn-sm mt-33" onclick="add_more('beneficiary_div','{{route('more_beneficairy')}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                    </div>
                </div>
                <div id="beneficiary_div"></div><!-- Display mor beneficiary  -->
                <!-- new Aabedat -->
                @if($plan->category_id==3)
                    <div id="aabedat_div"></div><!-- Display mor Aaabedat entities  -->
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-11">
                            <label class="title-custom">{{ trans('surveys.abedat_specification') }}:</label>                      
                            <select class="form-control m-input m-input--air select-2" id="code" name="code[]" onchange="bringSurveyEntities('0')">
                                <option value="0">{{ trans('global.select') }}</option>
                                @if($survey_entities)
                                    @foreach($survey_entities as $item)
                                        <option value="{{$item->code}}">{{ $item->{'name_'.$lang} }} </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-lg-1">
                            <label class="title-custom">&nbsp;</label><br>
                            <button type="button" class="btn btn-primary m-btn m-btn--air btn-xs" onclick="AddSurveyRecord()"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                        </div>
                    </div>
                    <div id="showContent"></div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-3">
                            <label class="">{{ trans('surveys.water_well') }}:</label>
                            <textarea class="form-control m-input m-input--air" name="water_well" rows="2"/>
                        </div>
                        <div class="col-lg-3">
                            <label class="">{{ trans('surveys.naldawani') }}:</label>
                            <textarea class="form-control m-input m-input--air" name="plumbery" rows="2"/>
                        </div>
                        <div class="col-lg-3">
                            <label class="">{{ trans('surveys.pool') }}:</label>
                            <textarea class="form-control m-input m-input--air" name="pool" rows="2"/>
                        </div>
                        <div class="col-lg-3">
                            <label class="">{{ trans('surveys.sewage') }}:</label>
                            <textarea class="form-control m-input m-input--air" name="sewage" rows="2"/>
                        </div>
                    </div>
                @endif
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <label class="title-custom">{{ trans('surveys.description') }}:</label>
                    <textarea class="form-control tinymce m-input m-input--air" name="description" rows="2"></textarea>
                </div>
            </div>
            <div id="targetDiv"></div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <input type="hidden" name="enc_plan" value="{{$enc_plan}}"/>
                            <button type="button" onclick="storeRecord('{{route('surveys.store')}}','surveyForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2({ width: '100%' });
</script>