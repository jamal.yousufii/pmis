<select class="form-control m-input m-input--air select-2" name="share_to">
    <option value="">{{trans('global.select')}}</option>
    @if($previous_dep!="-")
      @foreach($section->departments as $item)
        @if($item->id==$previous_dep)
          <option value="{{ $item->id }}">{{ $item->{'name_'.$lang} }}</option>
        @endif
      @endforeach
    @else
      @foreach($section->departments as $item)
        <option value="{{ $item->id }}">{{ $item->{'name_'.$lang} }}</option>
      @endforeach
    @endif
</select>
<script type="text/javascript">
  $(".select-2").select2();
</script>