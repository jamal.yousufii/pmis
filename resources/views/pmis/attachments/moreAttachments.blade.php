<span id="targetDiv_{!!$number!!}">
    <table class="table">
        <tr>
            <td width="10%" class="text-center">
                {!!$number!!}.
            </td>
            <td width="38.5%">
                <input class="form-control m-input" type="text" name="file_name[]" id="file_name_{!!$number!!}">
            </td>
            <td width="39%">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="file[]" id="file_{!!$number!!}" onchange="chooseFile(this.id)">
                    <label class="custom-file-label" for="customFile">{{ trans('requests.att_choose') }}</label>
                </div>
            </td>
            <td width="12.5%">
                <button type="button" id="btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-xs btn-sm" onclick="remove_moreAttachments('targetDiv_{!!$number!!}','{{$number}}')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
            </td>
        </tr>
    </table>
</span>