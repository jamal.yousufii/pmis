<span id="document_content_modal">
    <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
        <div class="m-alert__icon"><i class="la la-check-square"></i></div>
        <div class="m-alert__text">{!! __("global.success_att_msg") !!}</div>
        <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
    </div>
</span>
 <table class="table table-bordered">
    <thead>
        <tr class="bg-light">
        <th scope="col" width="10%">{{ trans('global.number') }}</th>
        <th scope="col" width="35%">{{ trans('surveys.document_type') }}</th>
        <th scope="col" width="35%">{{ trans('global.name') }}</th>
        <th scope="col" width="20%">{{ trans('global.opreation') }}</th>
        </tr>
    </thead>
    <tbody>
        @if($documents)
        @foreach($documents as $item)
            <tr id="document_{{$item->id}}">
            <td>{!! $item->id !!}</td>
            <td>{!! $item->documents_type->name_dr !!}</td>
            <td>{!! $item->filename !!}.{!! $item->extension !!}</td>
             <td>
                <a class="text-decoration-none" href="{{ route("DownloadAttachments",array(encrypt($item->id),Session('document_table'))) }}"><i class="fa fa-download"></i></a> |
                <a class="text-decoration-none" href="#" onclick="serverRequest('{{route('document.edit')}}','id={{encrypt($item->id)}}&&record_id={{encrypt($item->record_id)}}&&table={{encrypt($table)}}&&section={{encrypt('survey')}}&&file_name={{$item->filename}}&&document_type={{$item->document_type}}','POST','document-div')"><i class="fa fa-edit text-dark"></i></a> |
                <a class="text-decoration-none" href="#" onclick="destroy('{{route('attachment.destroy')}}','table={{encrypt(Session('document_table'))}}&&record_id={{encrypt($item->id)}}','POST','document_content_modal','document_{{$item->id}}')"><i class="fa fa-trash text-danger"></i></a> 
            </td>
    
            </tr>
        @endforeach
        @endif
  </tbody>
</table>