<!-- Documents Start-->
<div id="documnet_modals" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head bg-color-dark">
        <div class="m-portlet__head-caption px-2">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text text-white">{{ trans('global.documents') }}</h3>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;
        </button>
      </div>
      <div class="document-div" id="document-div">
      </div>
      <div class="document-div">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('global.document_list') }}</h3>
            </div>
          </div>
        </div>  
        <div id="document_modal_body" class="mt-2 mb-2 m-scrollable" data-scrollable="true" data-height="350" data-mobile-height="200">
          <span id="document_content_modal"></span>
          <table class="table table-bordered">
            <thead>
              <tr class="bg-light">
                <th scope="col" width="10%">{{ trans('global.number') }}</th>
                <th scope="col" width="35%">{{ trans('surveys.document_type') }}</th>
                <th scope="col" width="35%">{{ trans('global.name') }}</th>
                <th scope="col" width="20%">{{ trans('global.opreation') }}</th>
              </tr>
            </thead>
            <tbody>
              @if($record->documents)
                @foreach($record->documents as $item)
                  <tr id="document_{{$item->id}}">
                    <td>{!! $item->id !!}</td>
                    <td>{!! $item->documents_type->name_dr !!}</td>
                    <td>{!! $item->filename !!}.{!! $item->extension !!}</td>
                    <td>
                       <a class="text-decoration-none" href="{{ route("DownloadAttachments",array(encrypt($item->id),Session('document_table'))) }}"><i class="fa fa-download"></i></a> |
                       <a class="text-decoration-none" href="#" onclick="serverRequest('{{route('document.edit')}}','id={{encrypt($item->id)}}&&record_id={{encrypt($item->record_id)}}&&table={{encrypt($table)}}&&section={{encrypt('survey')}}&&file_name={{$item->filename}}&&document_type={{$item->document_type}}','POST','document-div')"><i class="fa fa-edit text-dark"></i></a> |
                       <a class="text-decoration-none" href="#" onclick="destroy('{{route('attachment.destroy')}}','table={{encrypt(Session('document_table'))}}&&record_id={{encrypt($item->id)}}','POST','document_content_modal','document_{{$item->id}}')"><i class="fa fa-trash text-danger"></i></a> 
                    </td>
                    
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function store_document()
  {
    $('.error-div').html('');
    var formData = new FormData($("#documentForm")[0]);
    $.ajax({
        url: '{{ route("document.store") }}',
        data: formData,
        contentType: false,
        type: 'post',
        beforeSend: function()
        {
          $("#loader-attachment").css("display","block");
        },
        success: function(response)
        {
          $('#file_name').val('');
          $('#file').val('');
          $('#file-label').html('');
          $('#document_modal_body').html(response);
          $("#collapseDiv").removeClass('show');
          $("#loader-attachment").css("display","none"); 
          $("#collapseDiv").removeClass('show');
        },
        error: function (request, status, error) {
            json = $.parseJSON(request.responseText);
            $.each(json.errors, function(key, value){
                $("#loader-attachment").css("display","none");
                $('.'+key).show();
                $('.'+key).html('<span class="text-danger">'+value+'</span>');
            });
        },
        cache: false,
        processData: false
    });
  }
</script>
<!-- Documents End-->
