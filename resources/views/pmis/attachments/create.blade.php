<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('global.attachment_add') }}</h3>
        </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a class="btn btn-default btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="false" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
            </li>
        </ul>
    </div>
</div>
<!-- begin::Page loader -->
<div class="m-page-loader m-page-loader--base" id="loader-attachment">
    <div class="progress" style="height: 10px;">
        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>
</div>
<!-- end::Page Loader -->
<div class="code notranslate cssHigh collapse mt-1" id="collapseDiv">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="attachmentForm" method="post"> 
        <table class="table table-bordered">
            <tbody>  
                <tr>
                    <td scope="col" width="20%"><h5 class="font-weight-bold">{{ trans('global.name') }} <span style="color:red;">*</span></h5></td>
                    <td>
                        <input class="form-control m-input errorDiv" type="text" name="file_name" id="file_name" required>
                        <div class="file_name error-div" style="display:none;"></div>
                    </td>
                </tr>
                <tr>
                    <td scope="col" width="20%"><h5 class="font-weight-bold">{{ trans('global.attachment') }} <span style="color:red;">*</span></h5></td>
                    <td>
                        <div class="custom-file errorDiv">
                            <input type="file" class="custom-file-input" name="file" id="file" onchange="chooseFile(this.id)" required>
                            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <div class="file error-div" style="display:none;"></div>
                        <span class="m-form__help small">{{ trans('global.file_extention') }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="parent_id" value="{{$parent_id}}"/>
        <input type="hidden" name="table" value="{!!$table!!}"/>
        <input type="hidden" name="record_id" value="{{$record_id}}">
        <input type="hidden" name="section" value="{{$section}}">
        <div class="col-lg-12">
            <div class="m-form__actions">
                <button type="button" onclick="store();" class="btn btn-primary">{{ trans('global.submit') }}</button>
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>
