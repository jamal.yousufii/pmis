<html>
    <head>
        <?php
        $lang = get_language();
        if($lang=="en"){
            $dir = "";
            $direction = "";

        }else {
            $dir = ".rtl"; 
            $direction = "rtl";
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('designs.designs') }}</title>
        <!--begin:: Styles -->
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('public/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
        <link rel="stylesheet" href="{{asset('public/css/custome.css')}}">      
    </head>
    <body dir="{{$direction}}">
        <table width="100%">
            <tr class="text-center">
                <td width="100%" class="text-center"><img src="{!!asset('public/img/logo.png')!!}" style="height: 115px; width: 115px"/></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-1') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-2') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('home.title-3') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ $department_name }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.project_status_report') }}</h3></td>
            </tr>
            <tr class="text-center">
                <td><h3 class="m-portlet__head-text">{{ trans('report.pmis_design') }}</h3></td>
            </tr>
        </table>
        <br>
        @if($record)
            <table border="1" width="100%">
                <tr>
                    <td class="text-center py-4" width="15%">{{ trans('daily_report.project_name') }}</td>
                    <td class="text-center py-4" width="35%" colspan="2">{{ $record->name }}</td>
                    <td class="text-center py-4" width="15%">{{ trans('global.status') }}</td>
                    <td class="text-center py-4" width="35%" colspan="2">
                        @if($record->architecture->first()->process=='0')
                            <span class="m-badge  m-badge--info m-badge--wide">{{ trans('global.under_process') }}</span>
                        @elseif($record->architecture->first()->process=='1')
                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.completed') }}</span>
                        @else
                            <span class="m-badge m-badge--warning m-badge--wide">{{ trans('global.pending') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center py-4">{{ trans('progress.report_date') }}</td>
                    <td class="text-center py-4" colspan="2">{{ dateCheck(date('Y-m-d'),$lang) }}</td>
                    <td class="text-center py-4">{{ trans('plans.project_type') }}</td>
                    <td class="text-center py-4" colspan="2">{{ $record->project_type->{'name_'.$lang} }}</td>
                </tr>
            </table>
            <table border="1" width="100%" style="border-top: 0px;">
                @if($record->architecture->count()>0)
                    <tr class="bg-light">
                        <td class="text-center py-3 title-custom" colspan="12">{{ trans('designs.architecture') }}</td>
                    </tr>
                    <tr>
                        <td class="text-center py-3" width="10%">{{ trans('global.urn') }}</td>
                        <td class="text-center py-3" width="20%">{{ trans('daily_report.project_location') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.responsible') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.employees1') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.start_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.end_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('global.attachments') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.visa') }}</td>
                    </tr>
                    @foreach($record->architecture as $item)
                        <tr>
                            <td class="text-center py-3">{{ $item->urn }}</td>
                            <td class="py-3">
                                @foreach($item->architectureProjectLocation as $arch_location)
                                    <span class="bg-default">
                                        @if(isset($arch_location->projectLocation()->first()->province_id))/ {{ $arch_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($arch_location->projectLocation()->first()->district_id))/ {{ $arch_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                                        @if(isset($arch_location->projectLocation()->first()->village_id))/ {{ $arch_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($arch_location->projectLocation()->first()->province_id) and !$loop->last) <br> @endif
                                    </span>
                                @endforeach
                            </td>
                            <td class="text-center py-3">{{ $item->employee->first_name }}</td>
                            <td class="text-center py-3">
                                @if($item->architectureTeam)
                                    @foreach($item->architectureTeam as $emp)
                                        <span class="bg-default">
                                            {{ $emp->employees->first_name }} 
                                            @if(!$loop->last), @endif
                                        </span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center py-3">{{ dateCheck($item->start_date,$lang) }}</td>
                            <td class="text-center py-3">{{ dateCheck($item->end_date,$lang) }}</td>
                            <td class="text-center py-3">
                                @php
                                    $attachments = getAttachments('design_attachments',$pro_id,0,'architecture');
                                @endphp
                                @if($attachments->count()>0)
                                    <span class="tab_custome">&#10003;</span>
                                @else
                                    <span class="tab_custome">&#10539;</span>
                                @endif
                            </td>
                            <td class="text-center py-3">
                                @if($item->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                                @elseif($item->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                                @elseif($item->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                @if($record->structure->count()>0)
                    <tr class="bg-light">
                        <td class="text-center py-3 title-custom" colspan="12">{{ trans('designs.structure') }}</td>
                    </tr>
                    <tr>
                        <td class="text-center py-3" width="10%">{{ trans('global.urn') }}</td>
                        <td class="text-center py-3" width="20%">{{ trans('daily_report.project_location') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.responsible') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.employees1') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.start_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.end_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('global.attachments') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.visa') }}</td>
                    </tr>
                    @foreach($record->structure as $item)
                        <tr>
                            <td class="text-center py-3">{{ $item->urn }}</td>
                            <td class="py-3">
                                @foreach($item->structureProjectLocation as $str_location)
                                    <span class="bg-default">
                                        @if(isset($str_location->projectLocation()->first()->province_id))/ {{ $str_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($str_location->projectLocation()->first()->district_id))/ {{ $str_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                                        @if(isset($str_location->projectLocation()->first()->village_id))/ {{ $str_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($str_location->projectLocation()->first()->province_id) and !$loop->last) <br> @endif
                                    </span>
                                @endforeach
                            </td>
                            <td class="text-center py-3">{{ $item->employee->first_name }}</td>
                            <td class="text-center py-3">
                                @if($item->structureTeam)
                                    @foreach($item->structureTeam as $emp)
                                    <span class="bg-default">
                                        {{ $emp->employees->first_name }} 
                                        @if(!$loop->last), @endif
                                    </span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center py-3">{{ dateCheck($item->start_date,$lang) }}</td>
                            <td class="text-center py-3">{{ dateCheck($item->end_date,$lang) }}</td>
                            <td class="text-center py-3">
                                @php
                                    $attachments = getAttachments('design_attachments',$pro_id,0,'structure');
                                @endphp
                                @if($attachments->count()>0)
                                    <span class="tab_custome">&#10003;</span>
                                @else
                                    <span class="tab_custome">&#10539;</span>
                                @endif
                            </td>
                            <td class="text-center py-3">
                                @if($item->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                                @elseif($item->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                                @elseif($item->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                @if($record->electricity->count()>0)
                    <tr class="bg-light">
                        <td class="text-center py-3 title-custom" colspan="12">{{ trans('designs.electricity') }}</td>
                    </tr>
                    <tr>
                        <td class="text-center py-3" width="10%">{{ trans('global.urn') }}</td>
                        <td class="text-center py-3" width="20%">{{ trans('daily_report.project_location') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.responsible') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.employees1') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.start_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.end_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('global.attachments') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.visa') }}</td>
                    </tr>
                    @foreach($record->electricity as $item)
                        <tr>
                            <td class="text-center py-3" width="8%">{{ $item->urn }}</td>
                            <td class="py-3">
                                @foreach($item->electricityProjectLocation as $elec_location)
                                    <span class="bg-default">
                                        @if(isset($elec_location->projectLocation()->first()->province_id))/ {{ $elec_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($elec_location->projectLocation()->first()->district_id))/ {{ $elec_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                                        @if(isset($elec_location->projectLocation()->first()->village_id))/ {{ $elec_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($elec_location->projectLocation()->first()->province_id) and !$loop->last) <br> @endif
                                    </span>
                                @endforeach
                            </td>
                            <td class="text-center py-3">{{ $item->employee->first_name }}</td>
                            <td class="text-center py-3">
                                @if($item->electricityTeam)
                                    @foreach($item->electricityTeam as $emp)
                                    <span class="bg-default">
                                        {{ $emp->employees->first_name }} 
                                        @if(!$loop->last), @endif
                                    </span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center py-3">{{ dateCheck($item->start_date,$lang) }}</td>
                            <td class="text-center py-3">{{ dateCheck($item->end_date,$lang) }}</td>
                            <td class="text-center py-3">
                                @php
                                    $attachments = getAttachments('design_attachments',$pro_id,0,'electricity');
                                @endphp
                                @if($attachments->count()>0)
                                    <span class="tab_custome">&#10003;</span>
                                @else
                                    <span class="tab_custome">&#10539;</span>
                                @endif
                            </td>
                            <td class="text-center py-3">
                                @if($item->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                                @elseif($item->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                                @elseif($item->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                @if($record->water->count()>0)
                    <tr class="bg-light">
                        <td class="text-center py-3 title-custom" colspan="12">{{ trans('designs.water') }}</td>
                    </tr>
                    <tr>
                        <td class="text-center py-3" width="10%">{{ trans('global.urn') }}</td>
                        <td class="text-center py-3" width="20%">{{ trans('daily_report.project_location') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.responsible') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.employees1') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.start_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.end_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('global.attachments') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.visa') }}</td>
                    </tr>
                    @foreach($record->water as $item)
                        <tr>
                            <td class="text-center py-3">{{ $item->urn }}</td>
                            <td class="py-3">
                                @foreach($item->waterProjectLocation as $wat_location)
                                    <span class="bg-default">
                                        @if(isset($wat_location->projectLocation()->first()->province_id))/ {{ $wat_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($wat_location->projectLocation()->first()->district_id))/ {{ $wat_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                                        @if(isset($wat_location->projectLocation()->first()->village_id))/ {{ $wat_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($wat_location->projectLocation()->first()->province_id) and !$loop->last) <br> @endif
                                    </span>
                                @endforeach
                            </td>
                            <td class="text-center py-3">{{ $item->employee->first_name }}</td>
                            <td class="text-center py-3">
                                @if($item->waterTeam)
                                    @foreach($item->waterTeam as $emp)
                                    <span class="bg-default">
                                        {{ $emp->employees->first_name }} 
                                        @if(!$loop->last), @endif
                                    </span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center py-3">{{ dateCheck($item->start_date,$lang) }}</td>
                            <td class="text-center py-3">{{ dateCheck($item->end_date,$lang) }}</td>
                            <td class="text-center py-3">
                                @php
                                    $attachments = getAttachments('design_attachments',$pro_id,0,'water');
                                @endphp
                                @if($attachments->count()>0)
                                    <span class="tab_custome">&#10003;</span>
                                @else
                                    <span class="tab_custome">&#10539;</span>
                                @endif
                            </td>
                            <td class="text-center py-3">
                                @if($item->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                                @elseif($item->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                                @elseif($item->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                @if($record->mechanic->count()>0)
                    <tr class="bg-light">
                        <td class="text-center py-3 title-custom" colspan="12">{{ trans('designs.mechanic') }}</td>
                    </tr>
                    <tr>
                        <td class="text-center py-3" width="10%">{{ trans('global.urn') }}</td>
                        <td class="text-center py-3" width="20%">{{ trans('daily_report.project_location') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.responsible') }}</td>
                        <td class="text-center py-3" width="15%">{{ trans('designs.employees1') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.start_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.end_date') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('global.attachments') }}</td>
                        <td class="text-center py-3" width="10%">{{ trans('designs.visa') }}</td>
                    </tr>
                    @foreach($record->mechanic as $item)
                        <tr>
                            <td class="text-center py-3">{{ $item->urn }}</td>
                            <td class="py-3">
                                @foreach($item->mechanicProjectLocation as $mech_location)
                                    <span class="bg-default">
                                        @if(isset($mech_location->projectLocation()->first()->province_id))/ {{ $mech_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($mech_location->projectLocation()->first()->district_id))/ {{ $mech_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                                        @if(isset($mech_location->projectLocation()->first()->village_id))/ {{ $mech_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                                        @if(isset($mech_location->projectLocation()->first()->province_id) and !$loop->last) <br> @endif
                                    </span>
                                @endforeach
                            </td>
                            <td class="text-center py-3">{{ $item->employee->first_name }}</td>
                            <td class="text-center py-3">
                                @if($item->mechanicTeam)
                                    @foreach($item->mechanicTeam as $emp)
                                        <span class="bg-default">
                                            {{ $emp->employees->first_name }} 
                                            @if(!$loop->last), @endif
                                        </span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center py-3">{{ dateCheck($item->start_date,$lang) }}</td>
                            <td class="text-center py-3">{{ dateCheck($item->end_date,$lang) }}</td>
                            <td class="text-center py-3">
                                @php
                                    $attachments = getAttachments('design_attachments',$pro_id,0,'mechanic');
                                @endphp
                                @if($attachments->count()>0)
                                    <span class="tab_custome">&#10003;</span>
                                @else
                                    <span class="tab_custome">&#10539;</span>
                                @endif
                            </td>
                            <td class="text-center py-3">
                                @if($item->status==0)
                                    <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                                @elseif($item->status==1)
                                    <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                                @elseif($item->status==2)
                                    <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        @endif
    </body>
</html>
