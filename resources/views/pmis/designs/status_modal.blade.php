<!-- Comments Start-->
<div id="status_modal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head bg-color-dark">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h5 class="m-portlet__head-text text-white"></h5>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
      </div>
      <div class="attachment-div">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h6 class="m-portlet__head-text">{{ trans('designs.change_status_'.$section) }}</h6>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <a class="btn btn-default btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="false" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
              </li>
            </ul>
          </div>
        </div>
        <div class="code notranslate cssHigh collapse" id="collapseDiv">
          <!--begin::Form-->
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="change_status_form" method="post"> 
             <div class="form-group m-form__group row m-form__group_custom">        
                <div class="col-lg-12 col-sm-12 col-xsm-t col-md-12">
                    <label class="title-custom">{{ trans('designs.change_status_type') }}: <span style="color:red;">*</span></label>
                    <div id="status" class="errorDiv">
                    <select class="form-control m-input m-input--air select-2" name="status" style="width: 100%">
                        <option value="">{{ trans('global.select') }}</option>
                        {{architectureStatus(${$section}->status,'option')}}
                    </select>
                    </div>
                    <div class="status error-div" style="display:none;"></div>
                </div>   
             </div>    
             <div class="form-group m-form__group row m-form__group_custom">        
                <div class="col-lg-12 col-sm-12 col-xsm-t col-md-12">
                    <label class="title-custom">{{ trans('contracts.committee_comment') }}: <span style="color:red;">*</span></label>
                    <div id="changed_comment" class="errorDiv">
                      <textarea class="form-control m-input m-input--air" name="changed_comment"></textarea>
                    </div>
                    <div class="changed_comment error-div" style="display:none;"></div>
                </div>   
             </div>    
            <input type="hidden" name="project_id" value="{!!$enc_id!!}"/>
            <input type="hidden" name="id" value="{{${$section}->id}}">
            <div class="col-lg-12">
                <div class="m-form__actions">
                  <button type="button" onclick="storeRecord('{{route($section.'.doapprove')}}','change_status_form','POST','content',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
            </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
      <div class="attachment-div">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('designs.comment_list') }}</h3>
            </div>
          </div>
        </div>  
        <div id="pagination-div" class="mt-2 mb-2 m-scrollable" data-scrollable="true" data-height="350" data-mobile-height="200">
           <table width="100%" class="table table-bordered">
             <thead>
              <tr class="bg-light">
                <th width="10%">{{ trans('global.number') }}</th>
                <th width="20%">{{ trans('designs.status') }}</th>
                <th width="70%">{{ trans('designs.comment') }}</th>
              </tr>
            </thead>
            <tbody>
              @php $i = 1; @endphp 
              @foreach ($version_comments as $cmnt)
                  <tr>
                     <td>{{$i++}}</td>
                     <td>
                      @if($cmnt->status==1)
                        <span class="m-badge m-badge--success m-badge--wide">{{trans('designs.approved')}}</span>
                      @elseif($cmnt->status==2)
                        <span class="m-badge m-badge--danger m-badge--wide">{{trans('designs.rejected')}}</span>
                      @endif
                     </td>
                     <td>{{$cmnt->changed_comment}}</td>
                  </tr>
              @endforeach
            </tbody>
           </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Comments End-->
<script>
  $('.select-2').select2({ width: '100%' });
</script>