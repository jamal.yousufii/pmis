@extends('master')
@section('head')
  <title>{{ trans('designs.designs') }}</title>
@endsection
@section('content')
  <!-- Include Summary Modal -->
  @include('pmis.summary.summary_modal')
  <div class="m-portlet m-portlet--mobile">
    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist" style="padding-right:10px; padding-left:10px;">
      {{-- <li class="nav-item m-tabs__item">
        <a href="{{ route('design',$enc_id) }}" class="nav-link m-tabs__link @if(session('current_tab')=='9') active @endif">
          <span class="m-portlet__head-text"><strong>{{ trans('designs.designs') }}</strong></span>
        </a>
      </li> --}}
      @if($tabs)
        @foreach($tabs as $item)
          @if(check_my_section(array(decrypt(session('current_department'))),$item->code))
            <li class="nav-item m-tabs__item">
              <a href="{{ route($item->url_route,$enc_id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
              </a>
            </li>
          @endif
        @endforeach
      @endif
    </ul>
    <div class="tab-content" id="content">
      <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet__head table-responsive" style="margin-top:-10px;">
          <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
              </div> 
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              @if(doIHaveRoleInDep(session('current_department'),"tab_mechanic","mec_add") and !$shared and $plan_loc > $inserted_loc and hasData($architecture)==1)<!-- Check if any location not inserted -->
                <li class="m-portlet__nav-item">
                  <a href="javascript:void();" onclick="serverRequest('{{route('mechanic.create')}}','id={{$enc_id}}','POST','content')" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                    <span><i class="la la-cart-plus"></i> {{ trans('designs.mechanic_add') }}</span>
                  </a>
                </li>
              @endif
              @if((hasData($architecture)==1)? true: false)
                <li class="m-portlet__nav-item">
                  <a class="btn btn-success btn-sm m-btn float-right" href="{{ route('design_print', [$enc_id]) }}">
                    <span><i class="la la-print"></i> <span>{{ trans('global.report_pdf') }}</span></span>
                  </a>
                </li>
              @endif
              @if(COUNT($mechanic)>0 and !empty($enc_id))
                <!-- Include Attachments Modal -->
                @include('pmis.attachments.modal')
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
                        <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.general_attachment') }}  [{{ $attachments->count() }}] </span></span>
                    </a>
                </li>
              @endif
              <li class="m-portlet__nav-item">
                <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                  <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                </a>
              </li>
              <li class="m-portlet__nav-item">
                <a href="{{ route('design',session('current_department')) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                  <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
              </li>
            </ul>
          </div>
        </div> 
        <div class="m-portlet__head m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('designs.mechanic_list') }}</h3>
            </div>
          </div>
        </div>
        @alert()
        @endalert
        <div class="m-portlet__body table-responsive" id="searchresult">
          <table class="table table-striped- table-bordered table-hover table-checkable">
            <thead>
              <tr class="bg-light">
                <th width="20%">{{ trans('global.urn') }}</th>
                <th>{{ trans('designs.responsible') }}</th>
                <th>{{ trans('designs.start_date') }}</th>
                <th>{{ trans('designs.complete_date') }}</th>
                <th>{{ trans('designs.status') }}</th>
                <th>{{ trans('designs.approved_by') }}</th>
                <th width="10%">{{ trans('global.action') }}</th>
              </tr>
            </thead>
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
              @if($mechanic)
                @foreach($mechanic AS $rec)
                  <tr>
                    <td>{!!$rec->urn!!}</td>
                    <td>{!!$rec->employee['first_name']!!}</td>
                    <td>{!!dateCheck($rec->start_date,$lang)!!}</td>
                    <td>{!!dateCheck($rec->end_date,$lang)!!}</td>
                    <td>
                      @if($rec->status==0)
                        <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                      @elseif($rec->status==1)
                        <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                      @elseif($rec->status==2)
                        <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                      @endif
                    </td>
                    <td>
                      {{$rec->changeStatusBy['name']}}
                    </td>
                    <td>
                      <span class="dtr-data">
                        <span class="dropdown">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                          <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                            @if(doIHaveRoleInDep(session('current_department'),"tab_mechanic","mec_view"))
                              <a class="dropdown-item" href="javascript:void()"  onclick="viewRecord('{{route('mechanic.view')}}','project_id={{$enc_id}}&&id={{$rec->id}}','POST','content')"><i class="la la-eye"></i> {{ trans('global.view') }}</a>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"tab_mechanic","mec_edit") and $rec->status!=1)
                              <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('mechanic.edit')}}','project_id={{$enc_id}}&&id={{$rec->id}}','POST','content')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                            @endif
                          </div>
                        </span>
                      </span>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
