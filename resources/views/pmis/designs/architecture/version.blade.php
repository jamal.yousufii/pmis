@if($versions)
 @foreach ($versions as $version)     
    <div class="form-group m-form__group row m-form__group_custom sperator_div">
        {{trans('designs.version')}} {{$loop->iteration}} .
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
            <label class="title-custom">{{ trans('designs.responsible') }} :</label>
            <br>
            <span>{!!$version->employee->first_name!!} {!!$version->employee->last_name!!}</span>
        </div>
        <div class="col-lg-3">
            <label class="title-custom">{{ trans('designs.start_date') }} :</label>
            <br>
            <span>{!!dateCheck($version->start_date,$lang)!!}</span>
        </div>
        <div class="col-lg-3">
            <label class="title-custom">{{ trans('designs.complete_date') }} :</label>
            <br>
            <span>{!!dateCheck($version->end_date,$lang)!!}</span>
        </div>
        <div class="col-lg-3">
            <label class="title-custom">{{ trans('designs.location') }} :</label><br>
            @foreach($version->architectureProjectLocation as $arch_location)
                <span class="bg-default">
                    <strong>{{ $loop->iteration }} -  </strong>
                    {{ $arch_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} 
                    @if($arch_location->projectLocation()->first()->district_id)/ {{ $arch_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                    @if($arch_location->projectLocation()->first()->village_id)/ {{ $arch_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                    @if($arch_location->projectLocation()->first()->latitude)/ {{ $arch_location->projectLocation()->first()->latitude }} @endif 
                    @if($arch_location->projectLocation()->first()->longitude)/ {{ $arch_location->projectLocation()->first()->longitude }} @endif
                    @if(!$loop->last) <br> @endif
                </span> 
            @endforeach
        </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-3">
            <label class="title-custom" for="">{{trans('designs.status')}}</label>
            <br> @if($version->status==1)
            <span class="m-badge m-badge--info m-badge--wide">{{trans('designs.approved')}}</span> @elseif($version->status==2)
            <span class="m-badge m-badge--danger m-badge--wide">{{trans('designs.rejected')}}</span> @else
            <span class="m-badge m-badge--warning m-badge--wide">{{trans('designs.pending')}}</span> @endif
        </div>
        @if(!empty($version->changed_comment))
            <div class="col-lg-9">
                <label class="title-custom" for="">{{trans('designs.comment')}}</label><br>
                <span>{{$version->changed_comment}}</span>
            </div>
        @endif
    </div>
 @endforeach
@endif