<div class="m-portlet__head table-responsive" style="margin-top:-10px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text">{{ trans('designs.architecture_view') }}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" onclick="serverRequest('{{route('attachment.create')}}','parent_id={{$enc_id}}&record_id={{encrypt($architecture->id)}}&table={{encrypt($table)}}&section={{encrypt($section)}}','POST','attachment-div')" data-toggle="modal" data-target="#AttachmentModal">
          <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
        </a>
      </li>
      @if(doIHaveRoleInDep(session('current_department'),"tab_architecture","view_pro_summary"))
        <li class="m-portlet__nav-item">
          <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
            <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
          </a>
        </li>
      @endif
      <li class="m-portlet__nav-item">
        <a href="javascript:void();" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
          <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
        </a>
      </li>
    </ul>
  </div>
</div>
@if($architecture)
  @if($architecture->completed=='0' and $architecture->created_by==userid()) <!-- Check if user is the owner of record -->
    <div class="m-portlet__head mt-2" style="height:12%;">
      <form enctype="multipart/form-data" id="completeForm" method="POST">
        <table>
          <td>
            <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
              <span class="m-switch small m-switch--outline m-switch--icon-check m-switch--brand">
                <label><input type="checkbox" id="complete" name="complete" value="1" onclick="completeRecord('{{ route('completeRecords') }}','complete','completeForm','POST','response_div')"><span></span></label>
              </span>
            </span>
          </td>
          <td><label class="title-custom">{{ trans('global.complete') }}</label></td>
        </table>
        <input type="hidden" name="table" value="architectures" />
        <input type="hidden" name="rec_id" value="{{ encrypt($architecture->id) }}" />
        <input type="hidden" name="project_id" value="{{ encrypt($architecture->project_id) }}" />
      </form>
    </div>
  @endif
  <!-- Include Attachments Modal -->
  @include('pmis.attachments.modal')
  <?php $enc_rec_id = encrypt($architecture->id); ?>
  <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
    <div class="m-portlet__body">
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.start_date') }} :</label><br>
          <span>{{dateCheck($architecture->start_date,$lang)}}</span>
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.complete_date') }} :</label><br>
          <span>{{dateCheck($architecture->end_date,$lang)}}</span>
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
          <span>{{$architecture->employee->first_name}}</span>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.employees1') }} :</label><br>
          @if($architecture->architectureTeam)
            @foreach($architecture->architectureTeam as $item)
              <span class="bg-default">
                <strong>{{ $loop->iteration }} -  </strong>
                {{ $item->employees->first_name }} 
                @if(!$loop->last) <br> @endif
              </span>
            @endforeach
          @endif
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.location') }} :</label><br>
          @foreach($architecture->architectureProjectLocation as $arch_location)
            <span class="bg-default">
              <strong>{{ $loop->iteration }} -  </strong>
              {{ $arch_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} 
              @if($arch_location->projectLocation()->first()->district_id)/ {{ $arch_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
              @if($arch_location->projectLocation()->first()->village_id)/ {{ $arch_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
              @if($arch_location->projectLocation()->first()->latitude)/ {{ $arch_location->projectLocation()->first()->latitude }} @endif 
              @if($arch_location->projectLocation()->first()->longitude)/ {{ $arch_location->projectLocation()->first()->longitude }} @endif
              @if(!$loop->last) <br> @endif
            </span>
          @endforeach
        </div>  
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.description') }} :</label><br>
          <span>{!! $architecture->description !!}</span>
        </div>
      </div> 
      <div class="row m-form__group_custom pt-3 pl-3 pr-3">
          <div class="col-lg-12 text-title">{{ trans('contracts.committee_comment') }}</div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">  
        <div class="col-lg-3">
          <label class="title-custom" for="">{{trans('designs.status')}}</label><br>
          @if($architecture->status==1)
            <span class="m-badge m-badge--success m-badge--wide">{{trans('designs.approved')}}</span>
          @elseif($architecture->status==2)
            <span class="m-badge m-badge--danger m-badge--wide">{{trans('designs.rejected')}}</span>
          @else
            <span class="m-badge m-badge--warning m-badge--wide">{{trans('designs.pending')}}</span>
          @endif
        </div>
        <div class="col-lg-9">
          <label class="title-custom" for="">{{trans('designs.comment')}}</label><br>
          <span>{{$architecture->changed_comment}}</span>
        </div>
      </div> 
      <!-- Versions -->
      @if($versions->count()>0)
        <div class="m-portlet__head-caption" style="border-top:1px solid #ebedf2">
          <div class="m-portlet__head-tools">
              <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                          <h3 class="m-portlet__head-text">{{ trans('designs.show_version') }}</h3>
                      </div>
                  </div>
                  <div class="m-portlet__head-tools">
                      <ul class="m-portlet__nav">
                          <li class="m-portlet__nav-item">
                              <a class="btn btn-secondary btn-sm  pt-2 pb-2" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv"><i class="la la-arrows-v"></i></a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
        </div>
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse" id="collapseDiv">
          <div class="m-portlet__body">
              @foreach ($versions as $version)     
                  <div class="row m-form__group_custom pt-3">
                      <div class="col-lg-12 text-title">{{trans('designs.version')}} {{$loop->iteration}} .</div>
                  </div>  
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('designs.start_date') }} :</label>
                        <br>
                        <span>{!!dateCheck($version->start_date,$lang)!!}</span>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('designs.complete_date') }} :</label>
                        <br>
                        <span>{!!dateCheck($version->end_date,$lang)!!}</span>
                    </div>
                    <div class="col-lg-4">
                        <label class="title-custom">{{ trans('designs.responsible') }} :</label>
                        <br>
                        <span>{!!$version->employee->first_name!!}</span>
                    </div>
                  </div>  
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('designs.employees1') }} :</label><br>
                      @if($version->architectureTeam)
                        @foreach($version->architectureTeam as $item)
                          <span class="bg-default">
                            <strong>{{ $loop->iteration }} -  </strong>
                            {{ $item->employees->first_name }} 
                            @if(!$loop->last) <br> @endif
                          </span>
                        @endforeach
                      @endif
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('designs.location') }} :</label><br>
                      @foreach($version->architectureProjectLocation as $arch_location)
                          <span class="bg-default">
                            <strong>{{ $loop->iteration }} -  </strong>
                            {{ $arch_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} 
                            @if($arch_location->projectLocation()->first()->district_id)/ {{ $arch_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
                            @if($arch_location->projectLocation()->first()->village_id)/ {{ $arch_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
                            @if($arch_location->projectLocation()->first()->latitude)/ {{ $arch_location->projectLocation()->first()->latitude }} @endif 
                            @if($arch_location->projectLocation()->first()->longitude)/ {{ $arch_location->projectLocation()->first()->longitude }} @endif
                            @if(!$loop->last) <br> @endif
                          </span> 
                      @endforeach
                    </div>
                    <div class="col-lg-4">
                      <label class="title-custom">{{ trans('designs.description') }} :</label><br>
                      <span>{!! $version->description !!}</span>
                    </div>
                  </div> 
                  <div class="row m-form__group_custom pt-3 pl-3 pr-3">
                      <div class="col-lg-12 text-title">{{ trans('contracts.committee_comment') }}</div>
                  </div>
                  <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-3">
                      <label class="title-custom" for="">{{trans('designs.status')}}</label>
                      <br> @if($version->status==1)
                      <span class="m-badge m-badge--success m-badge--wide">{{trans('designs.approved')}}</span> @elseif($version->status==2)
                      <span class="m-badge m-badge--danger m-badge--wide">{{trans('designs.rejected')}}</span> @else
                      <span class="m-badge m-badge--warning m-badge--wide">{{trans('designs.pending')}}</span> @endif
                    </div>
                    <div class="col-lg-9">
                      <label class="title-custom" for="">{{trans('designs.comment')}}</label><br>
                      <span>{{$version->changed_comment}}</span>
                    </div>
                  </div>
              @endforeach
          </div>
      @endif
    </div>
  </div>
@endif
