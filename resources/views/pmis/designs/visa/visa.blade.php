@extends('master')
@section('head')
  <title>{{ trans('designs.designs') }}</title>
@endsection
@section('content')
  <!-- Include Summary Modal -->
  @include('pmis.summary.summary_modal')
  <div class="m-portlet m-portlet--mobile">
    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand pl-4 pr-4" role="tablist">
      @if($tabs)
        @foreach($tabs as $item)
          @if(check_my_section(array(decrypt(session('current_department'))),$item->code))
            <li class="nav-item m-tabs__item">
              <a href="{{ route($item->url_route,$enc_id) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
              </a>
            </li>
          @endif
        @endforeach
      @endif
    </ul>
    <div class="tab-content" id="content">
      <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet__head table-responsive" style="margin-top:-10px;">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
            </div> 
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              @if(doIHaveRoleInDep(session('current_department'),"tab_visa","view_pro_summary"))
                <li class="m-portlet__nav-item">
                  <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
                    <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                  </a>
                </li>
              @endif
              <li class="m-portlet__nav-item">
                <a href="{{ route('design',session('current_department')) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                  <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
              </li>
            </ul>  
          </div>
        </div> 
        <div class="m-portlet__head m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ trans('designs.visa_list') }}</h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            @if($all_design and !$shared and (doIHaveRoleInDep(session('current_department'),"pmis_design","des_complete") || doIHaveRoleInDep(session('current_department'),"pmis_design","des_stop")))
              <!-- Include complete & stop modal -->
              @include('pmis.designs.complete_modal')
              @include('pmis.designs.stop_modal')
              <div class="btn-group open">
                <button class="btn btn-success m-btn--custom m-btn--icon btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                  <span><i class="fl flaticon-more-1"></i> <span>{{ trans('global.project_status') }}</span></span>
                </button>
                <ul class="dropdown-menu mt-2" role="menu">
                  <li>
                    @if(doIHaveRoleInDep(session('current_department'),"pmis_design","des_complete"))
                      <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                          <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                      </a>
                    @endif
                  </li>
                  <li>
                    @if(doIHaveRoleInDep(session('current_department'),"pmis_design","des_stop"))
                      <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                        <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                      </a>
                    @endif
                  </li>
                </ul>
              </div>
            @endif
            <ul class="m-portlet__nav">
              @if($shared and $all_design)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                        <span><i class="la la-arrows-v"></i><span>{{ trans('global.share_title') }}</span></span>
                    </a>
                </li>
              @elseif(doIHaveRoleInDep(session('current_department'),"tab_visa","vis_share") and $all_design and $architecture)
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " data-toggle="modal" data-target="#shareModal" href="javascript:void()">
                      <span><i class="fa fa-share-square"></i> <span>{{ trans('global.share') }}</span></span>
                    </a>
                </li>
              @endif
            </ul>  
          </div>
        </div>
        @if($all_design)
          <!-- Include sharing modal -->
          @include('pmis.designs.share_modal')
        @endif
        @alert()
        @endalert
        <div class="m-portlet__body table-responsive" id="searchresult">
          <table class="table table-striped- table-bordered table-hover table-checkable">
            <thead>
              <tr class="bg-light">
                <th width="20%">{{ trans('global.urn') }}</th>
                <th width="15%">{{ trans('designs.section') }}</th>
                <th width="20%">{{ trans('designs.responsible') }}</th>
                <th width="15%">{{ trans('designs.status') }}</th>
                <th width="20%">{{ trans('designs.approved_by') }}</th>
                <th width="10%">{{ trans('global.action') }}</th>
              </tr>
            </thead>
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
              @if($all_design)
                @foreach($all_design AS $rec)
                  <tr @if($rec->is_adjustment=='1') class="bg-info text-white" @endif >
                    <td>{{$rec->urn}}</td>
                    <td>{{trans($rec->section)}}</td>
                    <td>{{$rec->employee['first_name']}}</td>
                    <td>
                      @if($rec->status==0)
                        <span class="m-badge m-badge--warning m-badge--wide">{{ trans('designs.pending') }}</span>
                      @elseif($rec->status==1)
                        <span class="m-badge  m-badge--success m-badge--wide">{{ trans('designs.approved') }}</span>
                      @elseif($rec->status==2)
                        <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('designs.rejected') }}</span>
                      @endif
                    </td>
                    <td>
                      @if(isset($rec->changeStatusBy['name']))
                        {{ $rec->changeStatusBy['name'] }}
                      @endif
                    </td>
                    <td>
                      <span class="dtr-data">
                        <span class="dropdown">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                          <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                            @if(doIHaveRoleInDep(session('current_department'),"tab_visa","vis_approval"))
                              <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{ route($rec->route_view) }}','project_id={{$enc_id}}&&id={{$rec->id}}','POST','content')"><i class="la la-eye"></i> {{ trans('global.view') }}</a>
                            @endif
                            @if(doIHaveRoleInDep(session('current_department'),"tab_visa","vis_approval") and $rec->status!='1' and $rec->completed=='1') <!-- For Pending and rejected records -->
                              <a class="dropdown-item" href="javascript:void()" onclick="serverRequest('{{ route($rec->route_approve) }}','id={{$rec->id}}&&project_id={{$enc_id}}','POST','content')"><i class="la la-exchange"></i>{{ trans('designs.update_status') }}</a>
                            @endif
                          </div>
                        </span>
                      </span>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
