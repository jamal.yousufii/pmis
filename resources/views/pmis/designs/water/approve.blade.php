<div class="m-portlet__head table-responsive" style="margin-top:-10px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text">{{ trans('designs.update_status') }}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-success m-btn--custom m-btn--icon btn-sm mr-2" href="" data-toggle="modal" data-target="#status_modal">
          <span><i class="fa fa-folder-open"></i> <span>{{architectureStatus($water->status)}} </span></span>
        </a> 
      </li>
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="#" data-toggle="modal" data-target="#AttachmentModal">
          <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.related_attachment') }}  [{{ $attachments->count() }}] </span></span>
        </a>
      </li>
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
          <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
        </a>
      </li>
      <li class="m-portlet__nav-item">
        <a href="javascript:void();" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
          <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
        </a>
      </li>
    </ul>
  </div>
</div>
@if($water)
  <!-- Include Attachments Modal -->
  @include('pmis.attachments.modal')
  @include('pmis.designs.status_modal')<!-- Include change status Modal -->   
  <?php $enc_rec_id = encrypt($water->id); ?>
  <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
    <div class="m-portlet__body">
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.start_date') }} :</label><br>
          <span>{!!dateCheck($water->start_date,$lang)!!}</span>
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.complete_date') }} :</label><br>
          <span>{!!dateCheck($water->end_date,$lang)!!}</span>
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
          <span>{!!$water->employee->first_name!!}</span>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.employees2') }} :</label><br>
          @if($water->waterTeam)
            @foreach($water->waterTeam as $item)
              <span class="bg-default">
                <strong>{{ $loop->iteration }} -  </strong>
                {{ $item->employees->first_name }} 
                @if(!$loop->last) <br> @endif
              </span>
            @endforeach
          @endif
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.location') }} :</label><br>
          @foreach($water->waterProjectLocation as $arch_location)
            <span class="bg-default">
              <strong>{{ $loop->iteration }} -  </strong>
              {{ $arch_location->projectLocation()->first()->province()->first()->{'name_'.$lang} }} 
              @if($arch_location->projectLocation()->first()->district_id)/ {{ $arch_location->projectLocation()->first()->district()->first()->{'name_'.$lang} }} @endif 
              @if($arch_location->projectLocation()->first()->village_id)/ {{ $arch_location->projectLocation()->first()->village()->first()->{'name_'.$lang} }} @endif
              @if($arch_location->projectLocation()->first()->latitude)/ {{ $arch_location->projectLocation()->first()->latitude }} @endif 
              @if($arch_location->projectLocation()->first()->longitude)/ {{ $arch_location->projectLocation()->first()->longitude }} @endif
              @if(!$loop->last) <br> @endif
            </span>
          @endforeach
        </div>
        <div class="col-lg-4">
          <label class="title-custom">{{ trans('designs.description') }} :</label><br>
          <span>{!! $water->description !!}</span>
        </div>
      </div>  
    </div>
  </div>
@endif
