<div class="m-portlet__head table-responsive" style="margin-top:-10px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text">{{ trans('designs.water_add') }}</h3>
    </div>
  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm " href="" data-toggle="modal" data-target="#SummaryModal">
          <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
        </a>
      </li>
    </ul>
  </div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="water_form" method="post" enctype="multipart/form-data">
  <div class="m-portlet__body">
    <div class="package"> 
      <div class="form-group m-form__group row m-form__group_custom">        
        <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
          <label class="title-custom">{{ trans('designs.start_date') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input datePicker errorDiv" type="text" value="" name="start_date" id="start_date">
          <div class="start_date error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
          <label class="title-custom">{{ trans('designs.complete_date') }}: <span style="color:red;">*</span></label>
          <input class="form-control m-input datePicker errorDiv" type="text" value="" name="end_date" id="end_date">
          <div class="end_date error-div" style="display:none;"></div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xsm-t col-md-4">
          <label class="title-custom">{{ trans('designs.responsible') }}: <span style="color:red;">*</span></label>
          <div id="employee_id" class="errorDiv">
            <select class="form-control m-input m-input--air select2" name="employee_id">
              <option value="">{{ trans('global.select') }}</option>
              @if($employees)
                @foreach($employees as $emp)
                  <option value="{!!$emp->id!!}">{!!$emp->first_name!!}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="employee_id error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-6 col-sm-6 col-xsm-t col-md-6">
          <label class="title-custom">{{ trans('designs.employees2') }}: </label>
          <select class="form-control m-input m-input--air select2" name="employees[]" multiple>
            <option value="">{{ trans('global.select') }}</option>
            @if($employees)
              @foreach($employees as $emp)
                <option value="{!!$emp->id!!}">{!!$emp->first_name!!}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="col-lg-6 col-sm-6 col-xsm-t col-md-6">
          <label class="title-custom">{{ trans('designs.location') }}: <span style="color:red;">*</span></label>
          <div id="project_location_id" class="errorDiv">
            <select multiple class="form-control m-input m-input--air select2" name="project_location_id[]">
              <option value="">{{ trans('global.select') }}</option>
                @foreach($locations as $location)
                  @if(!in_array($location->id,$loc_data))
                    <option value="{{$location->id}}">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{ $location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{ $location->village->{'name_'.$lang} }} @endif @if($location->latitude) / {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</option>
                  @endif
                @endforeach
            </select>
          </div>
          <div class="project_location_id error-div" style="display:none;"></div>
        </div>
      </div>
      <div class="form-group m-form__group row m-form__group_custom">
        <div class="col-lg-12 col-sm-12 col-xsm-t col-md-12">
          <label class="title-custom">{{ trans('designs.description') }}: </label>
          <textarea class="form-control m-input m-input--air tinymce" name="description" rows="3"></textarea>
        </div>
      </div>
      <div class="col-lg-12">
        <!-- Start Attachments -->
        <table class="table">
          <tr>
            <th colspan="3">{{ trans('global.attachments') }}</th>
          </tr>
          <tr>
            <th width="10%" class="text-center">{{ trans('global.number') }}</th>
            <th width="40%">{{ trans('global.name') }}</th>
            <th width="50%" colspan="2">{{ trans('global.attachment') }}</th>
            <th></th>
          </tr>
          <tr>
            <td class="text-center">
              1.
            </td>
            <td>
              <input class="form-control m-input" type="text" name="file_name[]" id="file_name_1">
            </td>
            <td>
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="file[]" id="file_1" onchange="chooseFile(this.id)">
                <label class="custom-file-label" for="file">{{ trans('requests.att_choose') }}</label>
              </div>
              <span class="m-form__help">{{ trans('global.file_extention') }}</span>
            </td>
            <td width="10%">
              <button type="button" onclick="add_moreAttachments('{{ route("bringMoreAttachments") }}')" class="btn btn-info m-btn m-btn--air btn-xs btn-sm"><i class="fa fa-plus" style="font-size:10px;"></i></button>
            </td>
          </tr>
        </table>
        <div id="targetDiv"></div>
        <!-- End of Attachments -->
      </div>
    </div>
    <div class="form-group m-form__group row m-form__group_custom">
      <div class="col-lg-12">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions--solid">
            <button type="button" onclick="storeRecord('{{route('water.store')}}','water_form','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="project_id" value="{{$enc_id}}"/>
  @csrf
</form>
<!--end::Form-->
<script>
  $('.select2').select2();
</script>
