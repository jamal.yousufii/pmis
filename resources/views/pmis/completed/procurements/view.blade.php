<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('completed.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet--primary m-portlet--head-sm">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('global.view_execution') }}</h3>
                    </div>
                </div>
            </div>
            <div class="m-content" id="show-content">
                @if($record)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="row m-form__group_custom">
                                <div class="col-lg-12 text-title">{{ trans('procurement.executive') }}</div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.is_approved') }} :</label><br>
                                    <span>
                                        @if($record->executive==1)
                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                        @elseif($record->executive==0)
                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                        @endif
                                    </span>
                                </div>
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.date') }} :</label><br>
                                    <span>{{ dateCheck($record->executive_date,$lang) }}</span>
                                </div>
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                    <span>{!! $record->executive_desc !!}</span>
                                </div>
                            </div>
                            <div class="row m-form__group_custom pt-1">
                                <div class="col-lg-12 text-title">{{ trans('procurement.planning') }}</div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.is_approved') }} :</label><br>
                                    <span>
                                        @if($record->planning==1)
                                            <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                        @elseif($record->planning==0)
                                            <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                        @endif
                                    </span>
                                </div>
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.tin') }} :</label><br>
                                    <span>{{ $record->tin }}</span>
                                </div>
                                <div class="col-lg-4">
                                    <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                    <span>{!! $record->planning_desc !!}</span>
                                </div>
                            </div>
                            @if($record->financial_letter!==null)
                                <div class="row m-form__group_custom pt-12">
                                    <div class="col-lg-12 text-title">{{ trans('procurement.construction') }}</div>
                                </div>
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.financial_letter') }} :</label><br>
                                        <span>
                                            @if($record->financial_letter==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->financial_letter==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bidding_time') }} :</label><br>
                                        <span>{{ dateCheck($record->bidding_time,$lang) }} {{ dateDifference($record->bidding_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->financial_letter_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->bidding!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bidding') }} :</label><br>
                                        <span>
                                            @if($record->financial_letter==1)
                                                {{ trans('procurement.announcement') }}
                                            @elseif($record->financial_letter==2)
                                                {{ trans('procurement.single_source') }}
                                            @elseif($record->financial_letter==3)
                                            {{ trans('procurement.conditional') }}
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bidding_approval_time') }} :</label><br>
                                        <span>{{ dateCheck($record->bidding_approval_time,$lang) }} {{ dateDifference($record->bidding_approval_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->bidding_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->bidding_approval!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bidding_approval') }} :</label><br>
                                        <span>
                                            @if($record->bidding_approval==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->bidding_approval==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.announcement_time') }} :</label><br>
                                        <span>{{ dateCheck($record->announcement_time,$lang) }} {{ dateDifference($record->announcement_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->bidding_approval_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->announcement!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.announcement') }} :</label><br>
                                        <span>
                                            @if($record->announcement==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->announcement==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bids_time') }} :</label><br>
                                        <span>{{ dateCheck($record->bids_time,$lang) }} {{ dateDifference($record->bids_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->announcement_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->bids!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.bids') }} :</label><br>
                                        <span>
                                            @if($record->bids==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->bids==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.estimate_time') }} :</label><br>
                                        <span>{{ dateCheck($record->estimate_time,$lang) }} {{ dateDifference($record->estimate_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->bids_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->estimate!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.estimate') }} :</label><br>
                                        <span>
                                            @if($record->estimate==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->estimate==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.public_announce_time') }} :</label><br>
                                        <span>{{ dateCheck($record->public_announce_time,$lang) }} {{ dateDifference($record->public_announce_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->estimate_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->public_announce!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.public_announce') }} :</label><br>
                                        <span>
                                            @if($record->public_announce==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->public_announce==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.send_time') }} :</label><br>
                                        <span>{{ dateCheck($record->send_time,$lang) }} {{ dateDifference($record->send_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->public_announce_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->send!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.send') }} :</label><br>
                                        <span>
                                            @if($record->send==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->send==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_arrangement_time') }} :</label><br>
                                        <span>{{ dateCheck($record->contract_arrangement_time,$lang) }} {{ dateDifference($record->contract_arrangement_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->send_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->contract_arrangement!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_arrangement') }} :</label><br>
                                        <span>
                                            @if($record->contract_arrangement==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->contract_arrangement==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_approval_time') }} :</label><br>
                                        <span>{{ dateCheck($record->contract_approval_time,$lang) }} {{ dateDifference($record->contract_approval_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->contract_arrangement_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->contract_approval!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_approval') }} :</label><br>
                                        <span>
                                            @if($record->contract_approval==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->contract_approval==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.send_letter_time') }} :</label><br>
                                        <span>{{ dateCheck($record->send_letter_time,$lang) }} {{ dateDifference($record->send_letter_time,'%a') }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->contract_approval_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->company!==null)
                                <div class="row m-form__group_custom">
                                    <div class="col-lg-12 text-title pl-5 pr-5">{{ trans('procurement.send_letter') }}</div>
                                </div>
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.company') }} :</label><br>
                                        <span>{{ $record->company }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_code') }} :</label><br>
                                        <span>{{ $record->contract_code }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.start_date') }} :</label><br>
                                        <span>{{ dateCheck($record->start_date,$lang) }}</span>
                                    </div>
                                </div>  
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.end_date') }} :</label><br>
                                        <span>{{ dateCheck($record->end_date,$lang) }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.contract_price') }} :</label><br>
                                        <span>{{ $record->contract_price }}</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.controle_time') }} :</label><br>
                                        <span>{{ dateCheck($record->controle_time,$lang) }} {{ dateDifference($record->controle_time,'%a') }}</span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-12">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->company_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                            @if($record->controle!==null)
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-4">
                                        <label class="title-custom">{{ trans('procurement.controle') }} :</label><br>
                                        <span>
                                            @if($record->controle==1)
                                                <span class="m-badge  m-badge--success m-badge--wide">{{ trans('global.yes') }}</span>
                                            @elseif($record->controle==0)
                                                <span class="m-badge  m-badge--danger m-badge--wide">{{ trans('global.no') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-lg-8">
                                        <label class="title-custom">{{ trans('procurement.description') }} :</label><br>
                                        <span>{!! $record->controle_desc !!}</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @else
                    <div class="col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>       
    </div>
</div>