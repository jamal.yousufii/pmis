<div class="code notranslate cssHigh collapse" id="collapseStatusDiv" style="border-bottom:1px solid #ebedf2;">
    <div class="m-portlet__head bg-light pt-2" style="height: 3rem;">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
              <h3 class="title-custom">{{ trans('requests.approval') }}</h3>
          </div>
        </div>
    </div>
    <div id="pagination-div" class="mt-2 mb-2 m-scrollable">
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.status') }}: </label><br>
                <span>
                    @if($record->operation==1)
                        {{ trans('requests.req_approve') }}
                    @elseif($record->operation==2)
                        {{ trans('requests.req_reject') }}
                    @endif
                </span>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.date') }}: </label><br>
                <span>{{ dateCheck($record->operation_date,$lang) }}</span>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('requests.number') }}: </label><br>
                <span>{!!$record->operation_number!!}</span>
              </div>
            </div> 
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <label class="title-custom">{{ trans('requests.description') }}:</label><br>
                <span>{!!$record->operation_description!!}</span>
              </div>
            </div>
          </div>
        </div>
        
    </div>
</div>