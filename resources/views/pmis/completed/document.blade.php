<div class="code notranslate cssHigh collapse" id="collapseDocDiv" style="border-bottom:1px solid #ebedf2;">
    <div class="m-portlet__head bg-light pt-2" style="height: 3rem;">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="title-custom">{{ trans('global.document_list') }}</h3>
            </div>
        </div>
    </div>
    <div id="pagination-div" class="mt-2 mb-2 m-scrollable">
        <table class="table table-bordered">
            <thead>
                <tr class="bg-light">
                    <th scope="col" width="10%">{{ trans('global.number') }}</th>
                    <th scope="col" width="35%">{{ trans('surveys.document_type') }}</th>
                    <th scope="col" width="35%">{{ trans('global.name') }}</th>
                    <th scope="col" width="20%">{{ trans('global.opreation') }}</th>
                </tr>
            </thead>
            <tbody>
                @if($record->documents)
                    @foreach($record->documents as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->documents_type->name_dr }}</td>
                        <td>{{ $item->filename }}.{{ $item->extension }}</td>
                        <td>
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments",array(encrypt($item->id),$document_table)) }}"><i class="fa fa-download"></i> {{ trans('global.download') }}</a>
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>