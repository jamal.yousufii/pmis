<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('completed.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet--primary m-portlet--head-sm">
            <div class="m-portlet__head">
                <!-- Include Attachments Modal -->
                @php 
                    $attachments = getAttachments('implement_attachments',0,decrypt($enc_id),'implement');  
                    $attachment_table = 'implement_attachments';
                @endphp
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('implements.view') }}</h3>
                    </div>
                </div>                                   
                <!-- Include Attachments Modal -->
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        @if($record and $record->operation != 0)
                            <li class="m-portlet__nav-item">
                                <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseStatusDiv" role="button" aria-expanded="true" aria-controls="collapseStatusDiv">
                                    <span><i class="la la-arrows-v"></i><span>{{ trans('requests.approval') }}</span></span>
                                </a>
                            </li> 
                        @endif
                    </ul>
                </div>
            </div>
            @include('pmis.completed.approval_view')
            <div class="m-content" id="show-content">
                @if($record)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <table class="table">
                                <thead>
                                    <tr class="bg-light">
                                        <th width="33.33%" style="border-top:0px;">{{trans('implements.attachment_type')}}</th>
                                        <th width="33.33%" style="border-top:0px;">{{trans('implements.attachment_name')}}</th>
                                        <th width="33.33%" style="border-top:0px;">{{trans('global.attachment')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($attachments)
                                        @foreach($attachments as $item)
                                            <tr>
                                                <td>@if($item->type) {{ $item->static_data->{'name_'.$lang} }} @endif</td>
                                                <td>{{ $item->filename }}</td>
                                                <td>
                                                    @if($item->path!=null)
                                                        <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($item->id),'implement_attachments')) }}">
                                                            <i class="fa fa-download"></i> {{ $item->filename }}.{{ $item->extension }} 
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="col-lg-12 col-md-12 pt-3">
                                <label class="title-custom">{{ trans('implements.description') }} :</label><br>
                                <span>{!!$record->description!!}</span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>       
    </div>
</div>