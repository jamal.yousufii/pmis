<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('completed.projecct_status')}}</div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom" style="height: calc(100% - 2.8rem)">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="fl flaticon-line-graph" data-toggle="tooltip" title="{{ trans('project_dashboard.spi_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.schedule_performance_index') }}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">
                                    @if($spi<=0.55)
                                        <span class="m-badge m-badge--danger m-badge--wide opacity">{{ round($spi,2) }}</span>
                                    @elseif($spi<=0.8)
                                        <span class="m-badge m-badge--warning m-badge--wide opacity">{{ round($spi,2) }}</span>
                                    @else
                                        <span class="m-badge m-badge--success m-badge--wide opacity">{{ round($spi,2) }}</span>
                                    @endif    
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom" style="height: calc(100% - 2.8rem)">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="fl flaticon-map-location" data-toggle="tooltip" title="{{ trans('plans.view_location') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('plans.view_location') }}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1">{{ $location->province->{'name_'.$lang} }} @if($location->district)/ {{$location->district->{'name_'.$lang} }} @endif @if($location->village)/ {{$location->village->{'name_'.$lang} }} @endif @if($location->latitude)/ {{ $location->latitude }} @endif @if($location->longitude)/ {{ $location->longitude }} @endif</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom" style="height: calc(100% - 2.8rem)">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="fl flaticon-users" data-toggle="tooltip" title="{{ trans('completed.project_owner') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('completed.project_owner') }}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1"> @if($owner) {{ $owner->users->name }} @endif </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom" style="height: calc(100% - 2.8rem)">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-check-square" data-toggle="tooltip" title="{{ trans('completed.complete_section') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('completed.complete_section') }}</span><br>
                                <span class="m-widget21__sub m-widget21__sub1"> @if($project_status->section!='') {{ getSectionName($project_status->section) }} @endif </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--mobile m-0" style="height:490px">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('completed.summary_progress')}}</div>
                        <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('completed.from_start_to_end')}}</div>                        
                        <div class="row">
                            <div class="col-lg-12 pt-3 mx-3">
                                <button class="btn btn-sm opacity" style="background: #64f062"></button> <span>{{ trans('completed.progress_percentage') }}</span>: {{ getPercentage($total_price,$budget_expenditure) }} % ( {{number_format($budget_expenditure)}} {{trans('completed.afghani')}} )<br>
                                <button class="btn btn-sm opacity" style="background: #ffc107"></button> <span>{{ trans('completed.remained_percentage') }}</span>: {{ getPercentage($total_price,$remained_budget) }} % ( {{number_format($remained_budget)}} {{trans('completed.afghani')}} )<br>
                                <button class="btn btn-sm opacity" style="background: #f4516c"></button> <span>{{ trans('completed.extra_percentage') }}</span>: {{ getPercentage($total_price,$extra_budget) }} % ( {{number_format($extra_budget)}} {{trans('completed.afghani')}} )<br>
                            </div>
                            <div class="col-lg-12" id="progress_chart" style="width: 100%;height:300px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--mobile m-0" style="height:490px">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('completed.change_request')}}</div>
                        <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa fa-caret-right mx-1"></span>{{trans('completed.from_start_to_end')}}</div>                        
                        <div class="row">
                            <div class="col-lg-12 pt-3 mx-3">
                                <button class="btn btn-sm opacity" style="background: #00c5dc"></button> <span>{{ trans('completed.scope_percentage') }}</span>: {{ getPercentage($total_price,$scope_chnages) }} % ( {{number_format($scope_chnages)}} {{trans('completed.afghani')}} )<br>
                                <button class="btn btn-sm opacity" style="background: #fd7e14"></button> <span>{{ trans('completed.time_percentage') }}</span>: {{ getPercentage($working_days,$time_extention) }} % ( {{$time_extention}} {{trans('completed.working_days')}} )<br>
                                <button class="btn btn-sm opacity" style="background: #343a40"></button> <span>{{ trans('completed.stop_percentage') }}</span>: {{ getPercentage($working_days,$stop_extention) }} % ( {{$stop_extention}} {{trans('completed.working_days')}} )<br>
                            </div>
                            <div class="col-lg-12" id="change_request_chart" style="width: 100%;height:300px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <div class="col-lg-4">
        <div class="m-portlet m-portlet--mobile m-0" style="height:490px">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('completed.summary_log')}}</div>
                        <div class="m-widget21">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon"><i class="fl flaticon-users"></i></span>
                                            <div class="m-widget21__info px-1">
                                                <span class="m-widget21__title py-0">{{ trans('completed.work_end_by') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ $project_status->users->name }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon"><i class="fl flaticon-calendar-1"></i></span>
                                            <div class="m-widget21__info px-1">
                                                <span class="m-widget21__title py-0">{{ trans('completed.work_end_at') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ dateCheck($project_status->created_at,$lang,true) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="m-widget21">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon"><i class="la	la-align-justify"></i></span>
                                            <div class="m-widget21__info px-1">
                                                <span class="m-widget21__title py-0">{{ trans('completed.work_end_desc') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ $project_status->description }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="m-widget21">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon"><i class="fl flaticon-list-1"></i></span>
                                            <div class="m-widget21__info px-1">
                                                <span class="m-widget21__title py-0">{{ trans('global.attachment') }}</span><br>
                                                @if($project_status_attachment)
                                                    <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($project_status_attachment->id),'project_location_status_attachments')) }}">
                                                        <i class="fa fa-download"></i> {{ $project_status_attachment->filename }}.{{ $project_status_attachment->extension }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Show graphs
        var progress_chart = echarts.init(document.getElementById('progress_chart'));
        var change_request_chart = echarts.init(document.getElementById('change_request_chart'));
        // Graph options
        progress_chart_option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['{{ trans("portfolios.good_well") }}', '{{ trans("portfolios.in_danger") }}', '{{ trans("portfolios.not_so_good") }}'],
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: [
                    '{{ trans("completed.progress_percentage") }}',
                    '{{ trans("completed.remained_percentage") }}',
                    '{{ trans("completed.extra_percentage") }}',
                ],
                axisLabel : {
                    rotate : 10,
                },
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        {
                            value:  {{ getPercentage($total_price,$budget_expenditure) }},
                            itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_price,$remained_budget) }},
                            itemStyle: {color: '#ffc107',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_price,$extra_budget) }},
                            itemStyle: {color: '#f4516c',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                    ],
                    type: 'bar',
                },
            ],

        };
        progress_chart.setOption(progress_chart_option);
        
        change_request_chart_option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['{{ trans("completed.scope_percentage") }}', '{{ trans("completed.time_percentage") }}', '{{ trans("completed.stop_percentage") }}'],
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: [
                    '{{ trans("completed.scope_percentage") }} ',
                    '{{ trans("completed.time_percentage") }} ',
                    '{{ trans("completed.stop_percentage") }} ',
                ],
                axisLabel : {
                    rotate : 10,
                },
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        {
                            name: '{{ trans("completed.scope_percentage") }}',
                            value:  {{ getPercentage($total_price,$scope_chnages) }},
                            itemStyle: {color: '#00c5dc',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            name: '{{ trans("completed.time_percentage") }}',
                            value:  {{ getPercentage($working_days,$time_extention) }},
                            itemStyle: {color: '#fd7e14',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            name: '{{ trans("completed.stop_percentage") }}',
                            value:  {{ getPercentage($working_days,$stop_extention) }},
                            itemStyle: {color: '#343a40',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                    ],
                    type: 'bar',
                },
            ],

        };
        change_request_chart.setOption(change_request_chart_option);
    });
</script>