<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('completed.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet--primary m-portlet--head-sm">
            <div class="m-portlet__head">
                <!-- Include Attachments Modal -->
                @php 
                    $attachments = getAttachments('project_attachments',0,decrypt($enc_id),'plan');  
                    $attachment_table = 'project_attachments';
                @endphp
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('plans.view') }}</h3>
                    </div>
                </div>                                   
                <!-- Include Attachments Modal -->
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            @include('pmis.completed.attachment')
            <div class="m-content" id="show-content">
                @if($record)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.status') }}: </label><br>
                                    <span>{{$record->static_data['name_dr']}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.name') }}: </label><br>
                                    <span>{{$record->name}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.code') }}: </label><br>
                                    <span>{{$record->code}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.proj_category') }}: </label><br>
                                    <span>{{ $record->category->{'name_'.$lang} }}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3" id="ptype_div">
                                    <label class="title-custom">{{ trans('plans.project_type') }}: </label><br>
                                    <span>{{ $record->project_type->{'name_'.$lang} }}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.year') }}: </label><br>
                                    <span>{{yearCheck($record->year,$lang)}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.project_start_date') }}: </label><br>
                                    <span>{{dateCheck($record->project_start_date,$lang)}}</span>
                                </div>
                                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                    <label class="title-custom">{{ trans('plans.project_end_date') }}: </label><br>
                                    <span>{{dateCheck($record->project_end_date,$lang)}}</span>
                                </div>  
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                    <label class="title-custom">{{ trans('plans.goals') }}: </label><br>
                                    <span>{!!$record->goal!!}</span>
                                </div>
                                <div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">
                                    <label class="title-custom">{{ trans('plans.description') }}: </label><br>
                                    <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                        <!-- List project locations -->
                        <div class="m-portlet__head bg-light pt-2" style="height: 3rem;">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="title-custom">{{ trans('plans.view_location') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row m-form__group_custom" id="details-content">
                                    <table class="table table-striped-">
                                        <tbody>
                                            <tr>
                                                <th width="25%" class="top-no-border">{{ trans('plans.province') }}</th>
                                                <th width="25%" class="top-no-border">{{ trans('plans.district') }}</th>
                                                <th width="25%" class="top-no-border">{{ trans('plans.village') }}</th>
                                                <th width="25%" class="top-no-border">{{ trans('plans.location') }}</th>
                                            </tr>
                                        </tbody>
                                        @if($locations)
                                            @foreach($locations as $item)
                                                <tbody>  
                                                    <tr>
                                                        <td width="25%">{{ $item->province->{'name_'.$lang} }}</td>
                                                        <td width="25%">@if($item->district_id){{ $item->district->{'name_'.$lang} }} @endif</td>
                                                        <td width="25%">@if($item->village_id) {{ $item->village->{'name_'.$lang} }} @endif</td>
                                                        <td width="25%">@if($item->latitude) / {{ $item->latitude }} @endif @if($item->longitude) / {{ $item->longitude }} @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        @endforeach
                                    @endif
                                </div>         
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>       
    </div>
</div>