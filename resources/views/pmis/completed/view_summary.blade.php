<!-- Summary Start-->
<div id="SummaryModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header profile-head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('projects.view') }}</h3>
          </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;
        </button>
      </div>
      <table class="table" width="100%">
      	<tr>
      		<th>{{ trans('progress.project_name') }} :<h5 class="m-portlet__head-text"><strong>{!!$summary->name!!}</strong></h5></th>
          <th>{{ trans('progress.project_code') }} :<h5 class="m-portlet__head-text"><strong>{!!$summary->code!!}</strong></h5></th>
          <th>{{ trans('progress.company') }} :<h5 class="m-portlet__head-text"><strong>{!!$summary->procurement['company']!!}</strong></h5></th>
        </tr>
        <tr>
          <th>{{ trans('progress.start_date') }} :<h5 class="m-portlet__head-text"><strong>{!!dateCheck($summary->procurement['start_date'],$lang)!!}</strong></h5></th>
          <th>{{ trans('progress.end_date') }} :<h5 class="m-portlet__head-text"><strong>{!!dateCheck($summary->procurement['end_date'],$lang)!!}</strong></h5></th>
          <th>{{ trans('progress.incharge') }} :<h5 class="m-portlet__head-text"><strong>{!!getProjectIncharge($summary->id)!!}</strong></h5></th>
        </tr>
      </table>
    </div>
  </div>
</div>
<!-- Summary End-->
