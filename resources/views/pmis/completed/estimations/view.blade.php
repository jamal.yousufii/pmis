<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand px-2 mb-0" role="tablist">
    @if($sections)
        @foreach($sections as $item)
            @if(check_my_section(array(decrypt(session('current_department'))),$item->code) and in_array($item->code,$custom_sections))
                <li class="nav-item m-tabs__item">
                    <a href="javascript:void()" class="nav-link m-tabs__link @if(session('current_view')==$item->code) active @endif" onclick="viewRecord('{{route('completed.show',$enc_id)}}','code={{$item->code}}','GET','content')">
                        <span class="{{$item->icon}} px-2" style="font-size: 1.3rem;"></span><strong>{{ $item->name_dr }}</strong>
                    </a>
                </li>
                @if($item->code==$project_status->section)
                    @break
                @endif
            @endif
        @endforeach
    @endif
</ul>
<div class="tab-content">
    <div class="tab-pane active" role="tabpanel">
        <div class="m-portlet--primary m-portlet--head-sm">
            <div class="m-portlet__head">
                <!-- Include Attachments Modal -->
                @php 
                    $attachments = getAttachments('estimation_attachments',0,decrypt($enc_id),'estimation');  
                    $attachment_table = 'estimation_attachments';
                @endphp
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('estimation.view') }}</h3>
                    </div>
                </div>                                   
                <!-- Include Attachments Modal -->
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        @if($bill_quantities->count()>0)
                            <li class="m-portlet__nav-item">
                                <a href="javascript:void()" class="btn btn-accent m-btn--custom m-btn--icon btn-sm" onclick="openWindow('{{route('summary.list_bill_of_quantities',$enc_id)}}')">
                                    <span><i class="fa fa-folder-open"></i><span>{{ trans('estimation.bill_quantitie') }}</span></span>
                                </a>
                            </li>
                        @endif
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-accent m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseAttDiv" role="button" aria-expanded="true" aria-controls="collapseAttDiv">
                                <span><i class="la la-arrows-v"></i><span>{{ trans('global.related_attachment') }} [{{ $attachments->count() }}]</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            @include('pmis.completed.attachment')
            <div class="m-content" id="show-content">
                @if($record)
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.start_date') }} :</label><br>
                                    <span>{!!dateCheck($record->start_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.end_date') }} :</label><br>
                                    <span>{!!dateCheck($record->end_date,$lang)!!}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('estimation.estimated_cost') }} :</label><br>
                                    <span>{!!$record->cost!!}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.responsible') }} :</label><br>
                                    <span>{{$record->employee->first_name}}</span>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <label class="title-custom">{{ trans('designs.employees') }} :</label><br>
                                    <span>
                                        @if($record->estimationTeam)
                                            @foreach($record->estimationTeam as $item)
                                                <span class="bg-default">
                                                    {{ $item->employees->first_name }} 
                                                    @if(!$loop->last), @endif
                                                </span>
                                            @endforeach
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12 col-md-12">
                                    <label class="title-custom">{{ trans('estimation.description') }} :</label><br>
                                    <span>{!!$record->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-12">
                        <div class="m-wizard__form">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                    <dt>{{ trans('global.not_done') }}</dt>
                                </div>
                            </div>  
                        </div>
                    </div>
                @endif
            </div>
        </div>       
    </div>
</div>