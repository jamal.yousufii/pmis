@extends('master')
@section('head')
    <title>{{ trans('calendar.calendar') }}</title>
    <!--begin:: Fullcalendar Custom CSS -->
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.print.css')}}" rel="stylesheet" type="text/css" media='print' />
    <!--end:: Fullcalendar Custom CSS -->
    <style>
        #calendar {
            margin      : 40px auto;
            padding     : 0 10px;
        }
        .fc-right h2{
            font-size: 1.4rem !important;
        }
        .fc-unthemed .fc-toolbar .fc-button{
            border: 1px solid #ddd !important;
        }
        .m-switch input:empty ~ span {
            height: 16px !important;
            border: 2px solid #ffb822 !important;
        }
        .m-switch input:empty ~ span:after {
            top: -6px !important;
        }
        .fc-event{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile mb-2">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">@include('breadcrumb_nav')</div>                
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="p-0 pb-2 m-portlet m-portlet--mobile">
        <div id='calendar' class="p-4 pt-0 mt-0"></div>
    </div>
@endsection
@section('js-code')
    <!--begin:: Fullcalendar Custom JS -->
    <script src="{{asset('public/assets/fullcalendar-custom/lib/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/lib/moment-jalaali.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/locale-all.js')}}" type="text/javascript"></script>
    <!--end:: Fullcalendar Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {
            var lang = '{{$lang}}';
            if(lang!='en'){
                lang = 'fa';
            }
            console.log('asdfasdf'+'{{$tasks}}');
            var initialLocaleCode = lang;
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: '',
                    right: 'title'
                },
                @if($lang!='en')
                    isJalaali : true,
                @else
                    isJalaali : false,
                @endif
                defaultDate: '{{date("Y-m-d")}}',
                locale: lang,
                buttonIcons: true, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    @foreach($tasks as $item)
                        @php 
                        $end_date = date('Y-m-d', strtotime($item->start_date. ' + '.$item->duration.' days'));
                        @endphp
                        {
                            id: '{{$item->id}}',
                            title: '{{$item->title}}',
                            start: '{{$item->start_date}}',
                            end: '{{$end_date}}',
                            allDay: false,
                            @if($item->priority==39)
                                backgroundColor: '#f4516c',
                            @else
                                backgroundColor: '#ffb822',
                            @endif
                            textColor: '#000',
                            weekend: false,
                            display: 'inverse-background',
                        },
                    @endforeach
                ],
                eventClick:function(event){
                    var id = event.id;
                    var url = '{{ route("assignedTasks",":id") }}';
                    url = url.replace(':id',id);
                    var win = window.open(url);
                    win.focus();
                },
            });
	    });

    </script>
@endsection
