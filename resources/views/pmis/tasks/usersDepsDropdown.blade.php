<label>{{ trans('global.users') }}: <span style="color:red;">*</span></label>
<div id="div_user_id" class="errorDiv">
    <select class="form-control m-input m-input--air select-2 required" name="user_id" id="user_id" style="width:100%;">
        <option value="" selected>{{ trans('global.select') }}</option>
        @if($users)
            @foreach($users as $item)
                <option value="{!!$item->id!!}">{{ $item->name }}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="user_id error-div" style="display:none;"></div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
