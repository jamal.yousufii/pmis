
<div class="m-quick-sidebar__content">
    <span id="m_quick_sidebar_close" onclick="closeEditPanel()" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
    <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
            <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('tasks.edit') }}</h3>
        </div><hr>
        <div class="m-portlet__head-tools">
            <!--begin::Form-->
            <form enctype="multipart/form-data" id="taksFormEdit_{{ $task->id }}" method="post">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label >{{ trans('tasks.title') }}: <span style="color:red;">*</span></label><br>
                            <input class="form-control m-input errorDiv required" value="{{ $task->title }}" type="text" name="title" id="title">
                            <div class="title error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label >{{ trans('tasks.from') }}: <span style="color:red;">*</span></label><br>
                            <input class="form-control m-input datePicker errorDiv required" value="{{ dateCheck($task->start_date,$lang) }}" type="text" name="start_date" id="start_date">
                            <div class="start_date error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label >{{ trans('tasks.to') }}: <span style="color:red;">*</span></label><br>
                            <input class="form-control m-input errorDiv required" value="{{ $task->duration }}" type="number" min="0" name="duration" id="duration">
                            <div class="duration error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label >{{ trans('tasks.priority') }}: <span style="color:red;">*</span></label><br>
                            <div id="div_priority" class="errorDiv">
                                <select class="form-control m-input m-input--air select-2 required" id="priority" name="priority" style="width:100%">
                                    @if($priority)
                                            <option value="" selected>{{ trans('global.select') }}</option>
                                        @foreach($priority as $item)
                                            @if($item->code==$task->priority)
                                                <option value="{!! $item->code !!}" selected="selected">{{ $item->{'name_'.$lang} }}</option>
                                            @else
                                                <option value="{!! $item->code !!}">{{ $item->{'name_'.$lang} }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="priority error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-md-12">
                            <label>{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                            <div id="div_departments" class="errorDiv">
                                <select class="form-control m-input m-input--air select-2 required" id="departments" name="departments" onchange="bringDepartmentUsers('departments','div_users')" style="width:100%">
                                    @if($departments)
                                        <option value="" selected>{{ trans('global.select') }}</option>
                                        @foreach($departments as $item)
                                            @if($item->id==$task->dep_id)
                                                <option value="{!! $item->id !!}" selected="selected">{{ $item->{'name_'.$lang} }}</option>
                                            @else
                                                <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="departments error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-md-12" id="div_users">
                            <label>{{ trans('global.users') }}: <span style="color:red;">*</span></label>
                            <div id="users" class="errorDiv">
                                <select class="form-control m-input m-input--air select-2 required" id="user_id" name="user_id" style="width:100%">
                                    @if($users)
                                        <option value="" selected>{{ trans('global.select') }}</option>
                                        @foreach($users as $item)
                                            @if($item->id==$task->user)
                                                <option value="{!! $item->id !!}" selected="selected">{{ $item->name }}</option>
                                            @else
                                                <option value="{!! $item->id !!}">{{ $item->name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="user_id error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label>{{ trans('tasks.description') }}: <span style="color:red;">*</span></label>
                            <textarea class="form-control m-input m-input--air errorDiv required" name="description" id="description" rows="2">{{ $task->description }}</textarea>
                            <div class="description error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <input type="hidden" name="record_id" value="{{ $task->id }}"/>
                        <div class="col-lg-12">
                            <button type="button" class="btn btn-primary" onclick="storeRecord('{{route('updateTasks')}}','taksFormEdit_{{$task->id}}','POST','response_div',redirectFunction,false,this.id);" id="add">{{trans('global.submit')}}</button>
                            <button type="button" onclick="closeEditPanel()" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
    $(document).ready(function() {
        $(".select-2").select2();
        @if(get_language()=="en")
            $(".datePicker").attr('type', 'date');
        @else
            $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
        @endif
    });

    function closeEditPanel()
    {
        $("#m_quick_sidebar").removeClass("m-quick-sidebar--on")
    }

</script>
