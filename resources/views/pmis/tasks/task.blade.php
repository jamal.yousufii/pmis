@extends('master')
@section('head')
    <title>{{ trans('tasks.tasks') }}</title>
    <!--begin:: Fullcalendar Custom CSS -->
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.print.css')}}" rel="stylesheet" type="text/css" media='print' />
    <!--end:: Fullcalendar Custom CSS -->
    <style>
        .opacity{
           opacity: 0.5 !important ;
        }
        #calendar {
            margin      : 40px auto;
            padding     : 0 10px;
        }
        .list{
            cursor: pointer;
        }
        .list-item{
            border: 1px solid #e5e5e5;
            border-radius: 6px;
        }
        .card_header_custome{
            padding: .75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0,0,0,0.03);
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">@include('breadcrumb_nav')</div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    @if(doIHaveRoleInDep(session('current_department'),'pmis_tasks','task_add'))
                        <!-- Include Tasks Add Modal -->
                        @include('pmis.tasks.add')
                        <li class="m-portlet__nav-item">
                            <a href="javascript:void()" class="btn btn-primary btn-sm m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air" data-toggle="modal" data-target="#addTaskModal">
                                <span><i class="la la-cart-plus"></i><span>{{ trans('tasks.add') }}</span></span>
                            </a>
                        </li>
                    @endif
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('bringSections',$dep_id) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        @alert()
        @endalert
        {{-- Messages --}}
        <div id="alertmessage"></div>
        {{-- Tasks list as cards --}}
        <div class="row">
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__head" style="height:60px;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-warning align-center">{{ trans('global.pending') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="pending" class="m-portlet__body list">
                        @if($tasks)
                            @foreach($tasks as $item)
                                @if($item->status==1)
                                    <div id="pending_{{$item->id}}" class="m-nav__item">
                                        <div id="{{ $item->id }}" draggable="true" class="card rounded list-item pendingdiv m-0 m-nav__link m-dropdown__toggle">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                            <div class="row card_header_custome {{$background}} mx-0 py-2">
                                                <div class="col-md-10 col-lg-10 col-sm-10">
                                                    <div class=" collapsed " data-toggle="collapse" href="#collapsePending_{{$item->id}}" role="button" aria-controls="collapsePending_{{$item->id}}">{{ $item->title }}</div>
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-sm-2" >
                                                    @if(doIHaveRoleInDep(session('current_department'),'pmis_tasks','task_edit'))
                                                        <a class="btn btn-icon btn-sm btn-default btn-circle" onclick="tasksEdit({{ $item->id }})"><span><i class="fl flaticon-edit-1" ></i></span></a>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row m-form__group_custom">
                                                <div id="collapsePending_{{$item->id}}" class="collapse bg-white col-md-12">
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="card-text col-md-6">
                                                            <label >{{ trans('tasks.from') }} : </label><br>
                                                            {{ dateCheck($item->start_date,$lang) }}
                                                        </div>
                                                        <div class="card-text col-md-6">
                                                            <label >{{ trans('tasks.time_left') }} : </label><br>
                                                            <?php
                                                            $date = new DateTime($item->start_date);
                                                            $date->add(new DateInterval('P'.$item->duration.'D'));
                                                            $end_date = $date->format('Y-m-d') . "\n";
                                                            ?>
                                                            <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                            <label>{{ trans('tasks.assigned_by') }}:</label><br>
                                                            {{ $item->assigned_by->name}}
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                            <label >{{ trans('tasks.assigned_to') }}:</label><br>
                                                            {{ $item->assigned_to->name}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="col-md-12 card-text">
                                                            <label >{{ trans('tasks.description') }} : </label><br>
                                                            <p class="text-justify">{{ $item->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height ">
                    <div class="m-portlet__head" style="height:60px; text-align:center">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-info">{{ trans('global.under_process') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="progress" class="m-portlet__body list">
                        @if($tasks)
                            @foreach ($tasks as $item)
                                @if($item->status==2)
                                    <div id="progress_{{$item->id}}" class="p-0">
                                        <div id="{{ $item->id }}" draggable="true" class="card rounded list-item progressdiv m-0">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                            <div class="card_header_custome {{$background}} collapsed py-3" data-toggle="collapse" href="#collapseProgress_{{$item->id}}" role="button" aria-controls="collapseProgress_{{$item->id}}">{{ $item->title }}</div>
                                            <div id="collapseProgress_{{$item->id}}" class="collapse bg-white">
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.from') }} : </label><br>
                                                        {{ dateCheck($item->start_date,$lang) }}
                                                    </div>
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.time_left') }} : </label><br>
                                                        <?php
                                                        $date = new DateTime($item->start_date);
                                                        $date->add(new DateInterval('P'.$item->duration.'D'));
                                                        $end_date = $date->format('Y-m-d') . "\n";
                                                        ?>
                                                        <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                        <label>{{ trans('tasks.assigned_by') }}:</label><br>
                                                        {{ $item->assigned_by->name}}
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                        <label >{{ trans('tasks.assigned_to') }}:</label><br>
                                                        {{ $item->assigned_to->name}}
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('tasks.description') }} : </label><br>
                                                        <p class="text-justify">{{ $item->description }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12">
                                                        <a href="#demo_{{ $item->id}}" class="flaticon-comment" data-toggle="collapse" ></a>
                                                        <div id="demo_{{ $item->id}}" class="collapse">
                                                            <p class="text-justify">{{ $item->progress_comment }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__head" style="height:60px;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-success">{{ trans('global.completed') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="completed" class="m-portlet__body list">
                        @if($tasks)
                            @foreach ($tasks as $item)
                                @if($item->status==3)
                                    <div id="completed_{{$item->id}} class="p-0">
                                        <div id="{{ $item->id }}" draggable="true" class="card rounded list-item completeddiv m-0">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                        <div class="card_header_custome collapsed {{ $background }} py-3" data-toggle="collapse" href="#collapseCompelete_{{$item->id}}" role="button" aria-controls="collapseCompelete_{{$item->id}}">{{ $item->title }}</div>
                                            <div id="collapseCompelete_{{$item->id}}" class="collapse bg-white">
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.from') }} : </label><br>
                                                        {{ dateCheck($item->start_date,$lang) }}
                                                    </div>
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.time_left') }} : </label><br>
                                                        <?php
                                                            $date = new DateTime($item->start_date);
                                                            $date->add(new DateInterval('P'.$item->duration.'D'));
                                                            $end_date = $date->format('Y-m-d') . "\n";
                                                        ?>
                                                        <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                        <label>{{ trans('tasks.assigned_by') }}:</label><br>
                                                        {{ $item->assigned_by->name}}
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 card-text">
                                                        <label >{{ trans('tasks.assigned_to') }}:</label><br>
                                                        {{ $item->assigned_to->name}}
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('tasks.description') }} : </label><br>
                                                        <p class="text-justify">{{ $item->description }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12">
                                                        <a href="#demo_{{ $item->id}}" class="flaticon-comment" data-toggle="collapse" ></a>
                                                        <div id="demo_{{ $item->id}}" class="collapse">
                                                            <p class="text-justify">{{ $item->complete_comment }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-code')
    <script type="text/javascript">
        $(".select-2").select2();
        //Get all the list items in the view
        const list_items = document.querySelectorAll('.list-item');
        //Get all the list in the view
        const lists = document.querySelectorAll('.list');
        //Get all list items having class m-nav__item
        const nam_items = document.querySelectorAll('.m-nav__item');
        let draggedItem = null;
        let status = '';
        function tasksEdit(id){
            $.ajax({
                url : '{{ route('bringEditView')}}',
                data: {
                    "id" : id,
                },
                type: 'POST',
                success: function(response)
                {
                    $(".select-2").select2();
                    var d = document.getElementById("m_quick_sidebar");
                    d.className += " m-quick-sidebar--on";
                    $('#m_quick_sidebar').html(response);
                }
            });
        }
    </script>
@endsection
