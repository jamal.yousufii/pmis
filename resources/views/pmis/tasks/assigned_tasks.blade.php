@extends('master')
@section('head')
    <title>{{ trans('tasks.tasks') }}</title>
    <!--begin:: Fullcalendar Custom CSS -->
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.print.css')}}" rel="stylesheet" type="text/css" media='print' />
    <!--end:: Fullcalendar Custom CSS -->
    <style>
        .opacity{
           opacity: 0.5 !important ;
        }
        #calendar {
            margin      : 40px auto;
            padding     : 0 10px;
        }
        .list{
            cursor: pointer;
        }
        .list-item{
            border: 1px solid #e5e5e5;
            border-radius: 6px;
        }
        .card_header_custome{
            padding: .75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0,0,0,0.03);
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">@include('breadcrumb_nav')</div>
            </div>
        </div>
        <!-- Include Tasks Add Modal -->
        @alert()
        @endalert
        {{-- comment Section After moving from one status to another --}}
        <div id="OperationModal" class="modal fade" role="dialog" aria-labelledby="costScopeEditModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div id="share_content">
                        <div class="m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                            <!--begin::head-->
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('tasks.description') }}</h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <button type="button" class="close" id="closeEdit" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <form class="" enctype="multipart/form-data" id="operationmodalform">
                            <div class="m-portlet__body m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-0 p-0">
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <div class="col-lg-12">
                                        <label >{{ trans('tasks.description') }}: </label>
                                        <textarea class="form-control m-input m-input--air" name="operation_description" id="operation_description" rows="2"></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row m-form__group_custom">
                                    <input type="hidden" id="task_id"/>
                                    <input type="hidden" id="status"/>
                                    <div class="col-lg-12">
                                        <button type="button" id="submitbtn"  class="btn btn-primary">{{ trans('global.submit') }}</button>
                                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{ trans('global.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
        {{-- Messages --}}
        <div id="alertmessage"></div>
        {{-- Tasks list as cards --}}
        <div class="row">
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-warning">{{ trans('global.pending') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="pending" class="m-portlet__body list">
                        @if($tasks)
                            @foreach ($tasks as $item)
                                @if($item->status==1)
                                    <div id="pending_{{$item->id}}" class="m-nav__item">
                                        <div  id="{{ $item->id }}" @if($item->user == userid()) draggable="true" @endif class="card rounded list-item  pendingdiv m-0 m-nav__link m-dropdown__toggle">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                            <div class="card_header_custome {{$background}} collapsed" data-toggle="collapse" href="#collapsePending_{{$item->id}}" role="button" aria-controls="collapseProgress_{{$item->id}}">{{ $item->title }}</div>
                                            <div class="form-group m-form__group row m-form__group_custom">
                                                <div id="collapsePending_{{$item->id}}" class="collapse bg-white col-md-12">
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="card-text col-md-6">
                                                            <label >{{ trans('tasks.from') }} : </label><br>
                                                            {{ dateCheck($item->start_date,$lang) }}
                                                        </div>
                                                        <div class="card-text col-md-6">
                                                            <label >{{ trans('tasks.time_left') }} : </label><br>
                                                            <?php
                                                            $date = new DateTime($item->start_date);
                                                            $date->add(new DateInterval('P'.$item->duration.'D'));
                                                            $end_date = $date->format('Y-m-d') . "\n";
                                                            ?>
                                                            <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="col-md-12 card-text">
                                                            <label >{{ trans('global.assigned_by') }} : </label>
                                                            {{ $item->assigned_by->name }}
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom py-2">
                                                        <div class="col-md-12 card-text">
                                                            <label >{{ trans('tasks.description') }} : </label><br>
                                                            <p class="text-justify">{{ $item->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-info">{{ trans('global.under_process') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="progress" class="m-portlet__body list">
                        @if($tasks)
                            @foreach ($tasks as $item)
                                @if($item->status==2)
                                    <div id="progress_{{$item->id}}" class="p-0">
                                        <div id="{{ $item->id }}" @if($item->user == userid()) draggable="true" @endif class="card rounded list-item progressdiv m-0">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                            <div class="card_header_custome {{$background}} collapsed" data-toggle="collapse" href="#collapseProgress_{{$item->id}}" role="button" aria-controls="collapseProgress_{{$item->id}}">{{ $item->title }}</div>
                                            <div id="collapseProgress_{{$item->id}}" class="collapse bg-white">
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.from') }} : </label><br>
                                                        {{ dateCheck($item->start_date,$lang) }}
                                                    </div>
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.time_left') }} : </label><br>
                                                        <?php
                                                            $date = new DateTime($item->start_date);
                                                            $date->add(new DateInterval('P'.$item->duration.'D'));
                                                            $end_date = $date->format('Y-m-d') . "\n";
                                                        ?>
                                                        <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('global.assigned_by') }} : </label>
                                                        {{ $item->assigned_by->name }}
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('tasks.description') }} : </label><br>
                                                        <p class="text-justify">{{ $item->description }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12">
                                                        <a href="#demo_{{ $item->id}}" class="flaticon-comment" data-toggle="collapse" ></a>
                                                        <div id="demo_{{ $item->id}}" class="collapse">
                                                            <p class="text-justify">{{ $item->progress_comment }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 m-0">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text text-success">{{ trans('global.completed') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div id="completed" class="m-portlet__body list">
                        @if($tasks)
                            @foreach ($tasks as $item)
                                @if($item->status==3)
                                    <div id="completed_{{$item->id}} class="p-0">
                                        <div id="{{ $item->id }}" draggable="true" class="card rounded list-item completeddiv m-0">
                                            @php $background = 'bg-light'; @endphp
                                            @if($item->priority === 39)
                                                @php $background = 'bg-danger'; @endphp
                                            @endif
                                            <div class="card_header_custome collapsed {{$background}}" data-toggle="collapse" href="#collapseCompelete_{{$item->id}}" role="button" aria-controls="collapseCompelete_{{$item->id}}">{{ $item->title }}</div>
                                            <div id="collapseCompelete_{{$item->id}}" class="collapse bg-white">
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.from') }} : </label><br>
                                                        {{ dateCheck($item->start_date,$lang) }}
                                                    </div>
                                                    <div class="card-text col-md-6">
                                                        <label >{{ trans('tasks.time_left') }} : </label><br>
                                                            <?php
                                                                $date = new DateTime($item->start_date);
                                                                $date->add(new DateInterval('P'.$item->duration.'D'));
                                                                $end_date = $date->format('Y-m-d') . "\n";
                                                            ?>
                                                        <span class="m-list-timeline__time">{!!dateDifference($end_date,'%a',false)!!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('global.assigned_by') }} : </label>
                                                        {{ $item->assigned_by->name }}
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12 card-text">
                                                        <label >{{ trans('tasks.description') }} : </label><br>
                                                        <p class="text-justify">{{ $item->description }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row m-form__group_custom py-2">
                                                    <div class="col-md-12">
                                                        <a href="#demo_{{ $item->id}}" class="flaticon-comment" data-toggle="collapse" ></a>
                                                        <div id="demo_{{ $item->id}}" class="collapse">
                                                            <p class="text-justify">1. {{ $item->progress_comment }}</p>
                                                            <hr>
                                                            <p class="text-justify">2. {{ $item->complete_comment }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-code')
    <script type="text/javascript">
        $(".select-2").select2();
        //Get all the list items in the view
        const list_items = document.querySelectorAll('.list-item');
        //Get all the list in the view
        const lists = document.querySelectorAll('.list');
        //Get all list items having class m-nav__item
        const nam_items = document.querySelectorAll('.m-nav__item');

        let draggedItem = null;
        let status = '';

        for(let i=0; i < list_items.length; i++){
            const item = list_items[i];
            //Drag Start
            item.addEventListener('dragstart',function(){
                draggedItem = item;
                setTimeout(function(){
                    item.style.display='none';
                },0);

            });
            //Drag End
            item.addEventListener('dragend', function(){
                setTimeout(function (){
                    draggedItem.style.display = 'block';
                    draggedItem = null;
                },0);
            });

            //Append list item to the list
            for(let j = 0; j<lists.length; j++){
                const list = lists[j];
                list.addEventListener('dragover', function(e){
                    e.preventDefault();
                });
                list.addEventListener('dragenter', function(e){
                    this.style.backgroundColor = 'rgba(0,0,0,0.1)'
                });
                list.addEventListener('dragleave', function(e){
                    this.style.backgroundColor = '#fff'
                });
                list.addEventListener('drop', function(e){
                    //list.append(draggedItem);
                    this.style.backgroundColor = '#fff';

                    if(list.id=="pending"){
                        status = 1;
                        $section = '';
                    }
                    else if(list.id=="progress"){
                        if($("#"+draggedItem.id).hasClass("pendingdiv")){
                            status = 2;
                            $section = 'progress';
                            removeClassByPrefix(draggedItem,'text');
                            draggedItem.classList.add('text-info');
                            list.append(draggedItem);
                            $("#OperationModal").modal();
                        }
                    }
                    else if(list.id=="completed"){
                        if($("#"+draggedItem.id).hasClass("progressdiv") ||	$("#"+draggedItem.id).hasClass("pendingdiv")){
                            status = 3;
                            $section = 'complete';
                            removeClassByPrefix(draggedItem,'text');
                            draggedItem.classList.add('text-success');
                            list.append(draggedItem);
                            $("#OperationModal").modal();
                        }
                    }
                    else{

                    }
                    var record_id = draggedItem.id;
                    $("#submitbtn").click(function() {
                        $.ajax({
                            url : '{{ route('updateTaskStatus') }}',
                            data: {
                                "id" : record_id,
                                "status" : status,
                                "description" : $("#operation_description").val()
                            },
                            type: 'POST',
                            error : function(error)
                            {
                                $('#alertmessage').html('<div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert"><div class="m-alert__icon"><i class="la la-warning"></i></div><div class="m-alert__text">{{ trans('global.status_success_msg') }}</div><div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div></div>');
                            },
                            success: function(response)
                            {
                                redirectFunction();
                                //$('#alertmessage').html('<div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert"><div class="m-alert__icon"><i class="la la-check-square"></i></div><div class="m-alert__text">{{ trans('global.status_success_msg') }}</div><div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div></div>');
                            }
                        });
                    });
                });
            }
        }
        //Function to remove classes having same prefix
        function removeClassByPrefix(el, prefix) {
            var regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
            [...el.classList].forEach(className => {
                regx.test(className) && el.classList.remove(className);
            });
        }

        function tasksEdit(id){
            $.ajax({
                url : '{{ route('bringEditView')}}',
                data: {
                    "id" : id,
                },
                type: 'POST',
                success: function(response)
                {
                    //redirectFunction();
                    var d = document.getElementById("m_quick_sidebar");
                    d.className += " m-quick-sidebar--on";
                    $('#m_quick_sidebar').html(response);
                }
            });
        }
    </script>
@endsection
