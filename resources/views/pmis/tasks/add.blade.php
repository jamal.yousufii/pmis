{{-- Start of the Modal --}}
<div id="addTaskModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="addTaskModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head table-responsive">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                              <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('tasks.add') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="requestForm">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom py-2">
                                <div class="col-lg-12">
                                    <label >{{ trans('tasks.title') }}: <span style="color:red;">*</span></label><br>
                                    <input class="form-control m-input errorDiv required" type="text" name="title" id="title">
                                    <div class="title error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom py-2">
                                <div class="col-lg-4">
                                    <label >{{ trans('tasks.from') }}: <span style="color:red;">*</span></label><br>
                                    <input class="form-control m-input datePicker errorDiv required" type="text" name="start_date" id="start_date">
                                    <div class="start_date error-div" style="display:none;"></div>
                                </div>
                                <div class="col-lg-4">
                                    <label >{{ trans('tasks.to') }}: <span style="color:red;">*</span></label><br>
                                    <input class="form-control m-input errorDiv required" type="number" min="0" name="duration" id="duration">
                                    <div class="duration error-div" style="display:none;"></div>
                                </div>
                                <div class="col-lg-4">
                                    <label >{{ trans('tasks.priority') }}: <span style="color:red;">*</span></label><br>
                                    <div id="div_priority" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2 required" id="priority" name="priority" style="width:100%">
                                            @if($priority)
                                                    <option value="" selected>{{ trans('global.select') }}</option>
                                                @foreach($priority as $item)
                                                    <option value="{!! $item->code !!}">{{ $item->{'name_'.$lang} }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="priority error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom py-2" id="dateContent">
                                <div class="col-md-6">
                                    <label>{{ trans('global.departments') }}: <span style="color:red;">*</span></label>
                                    <div id="div_departments" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2 required" id="departments" name="departments" onchange="bringDepartmentUsers('departments','div_users')" style="width:100%">
                                            @if($departments)
                                                <option value="" selected>{{ trans('global.select') }}</option>
                                                @foreach($departments as $item)
                                                    <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="departments error-div" style="display:none;"></div>
                                </div>
                                <div class="col-md-6" id="div_users">
                                    <label>{{ trans('global.users') }}: <span style="color:red;">*</span></label>
                                    <div id="users" class="errorDiv">
                                        <select class="form-control m-input m-input--air select-2 required" id="user" name="user" style="width:100%">
                                            <option value="" selected>{{ trans('global.select') }}</option>
                                        </select>
                                    </div>
                                    <div class="departments error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom py-2">
                                <div class="col-lg-12">
                                    <label>{{ trans('tasks.description') }}: <span style="color:red;">*</span></label>
                                    <textarea class="form-control m-input m-input--air errorDiv required" name="description" id="description" rows="2"></textarea>
                                    <div class="description error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom py-2">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-primary" onclick="storeRecord('{{route('tasks.store')}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add">{{trans('global.submit')}}</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
  </div>
  <script type="text/javascript">
      $(document).ready(function() {
          @if(get_language()=="en")
              $(".datePicker").attr('type', 'date');
          @else
              $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
          @endif
      });
  </script>
