
    <div id="main" style="width: 100%;height:auto;"></div>
    @include('assets.apexcharts')
    <script type="text/javascript">
        var options = {
            title: {
                text: "{{ trans('global.location_year_graph') }}",
                align: 'center',
                margin: 10,
                style: {
                fontSize:  '18px',
                fontWeight:  'bold',
                fontFamily:  undefined,
                color:  '#263238'
                }
            },
          series: [{
          name: "{{ trans('global.province_num') }}",
          data: [
            @if($location)
                @foreach($location as $key=>$value)
                    "{{ $value }}",
                @endforeach
            @endif
          ]
        }],
        chart: {
          height: 400,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            columnWidth: '50%',
            endingShape: 'flat',  
            distributed: true
          }
        },
        colors: [
            @if($location)
                @foreach($location as $key=>$value)
                    @php
                        $color= Config::get('static.'.$lang.'.province.'.$key.'.color');
                    @endphp
                        '{{$color}}',
                @endforeach
            @endif
         ],
         dataLabels: {
          enabled: true
        },
        stroke: {
          width: 1
        },
        xaxis: {
          labels: {
            rotate: -45
          },
          categories: [
            @if($location)
                @foreach($location as $key=>$value)
                    "{{ getProvinceNameByID($key,$lang) }}",
                @endforeach
            @endif
          ],
          tickPlacement: 'on'
        },
        yaxis: {
          title: {
            text: "{{ trans('global.tedad') }}",
          },
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        },
        };
        var chart = new ApexCharts(document.querySelector("#main"), options);
        chart.render();
    </script>
