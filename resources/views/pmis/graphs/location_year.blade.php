<div class="row">
    <div class="col-xl-12">
        <div class="m-portlet  m-portlet--full-height  m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                           {{ trans('global.search') }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" id="collapsBtn" data-toggle="collapse" href="#graphCollapse" role="button" aria-expanded="true" aria-controls="graphCollapse">
                                <span><i class="la la-arrows-v"></i><span>{{ trans('global.view') }}</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="code notranslate cssHigh collapse show" id="graphCollapse">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="locationyearForm" method="post" enctype="multipart/form-data">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-md-6">
                                <label class="title-custom">{{ trans('global.province') }}: </label>
                                <select class="form-control m-input m-input--air select-2" multiple id="province" style="width:100%">
                                    @if($province)
                                        <option value="0" selected>{{ trans('global.all') }}</option>
                                        @foreach($province as $item)
                                        <option value="{!! $item->id !!}">{{ $item->{'name_'.$lang} }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="title-custom">{{ trans('global.year') }}: </label>
                                <div id="div_year" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 required" multiple id="year" style="width:100%">
                                        <option value="0" selected>{{ trans('global.all') }}</option>
                                        @for($year = 2000; $year <= 2050 ; $year++)
                                            <option value="{!! $year !!}">{{ yearCheck($year,$lang) }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="year error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom">
                            <button type="button" onclick="bringLocationYearGraph('locationyearForm'); showHideClass('graphCollapse');inVisible('print','visible')" class="btn btn-primary">{{ trans('global.search') }}</button>
                        </div>
                        @csrf
                    </div>
                </form>
            </div>
            <div class="form-group m-form__group row m-form__group_custom" id="showLocationYearContent"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".select-2").select2();
</script>
