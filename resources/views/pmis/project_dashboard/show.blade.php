<?php
$class = "fa-caret-left";
if($lang=="en"){
    $class = "fa-caret-right";
}
?>
<div class="row mt-4">
    <div class="col-lg-12 pb-3">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-box m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{ trans('project_dashboard.project' )}}</span><br>
                                <span class="m-widget21__sub">{{ $plan->name }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-interface-6 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('project_dashboard.portfolio')}}</span><br>
                                <span class="m-widget21__sub">
                                @if($portfolios)
                                    @foreach($portfolios as $item)
                                        {{ $item->{'name_'.$lang} }},
                                    @endforeach
                                @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-coins m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{ trans('project_dashboard.project_category') }}</span><br>
                                <span class="m-widget21__sub">{{ $plan->category->{'name_'.$lang} }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-notes m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('project_dashboard.project_type')}}</span><br>
                                <span class="m-widget21__sub">{{ $plan->project_type->{'name_'.$lang} }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-signs m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{ trans('procurement.contract_code') }}</span><br>
                                <span class="m-widget21__sub">{{ $plan->procurement->contract_code }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-2 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('project_dashboard.contract_start_date')}}</span><br>
                                <span class="m-widget21__sub">{{ dateCheck($plan->procurement->start_date,$lang) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-2 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('project_dashboard.contract_end_date')}}</span><br>
                                <span class="m-widget21__sub">{{ dateCheck($plan->procurement->end_date,$lang) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-3 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('project_dashboard.contract_amount')}}</span><br>
                                <span class="m-widget21__sub">{{number_format($plan->procurement->contract_price)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row py-4">
    <div class="col-lg-12 pb-3">
        <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('project_dashboard.project_fiscal_year')}}</div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.plan_budget_baseline_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.plan_budget_baseline') }}</span><br>
                                <span class="m-widget21__sub">{{number_format($total_baseline, 2, '.', ',')}}</span> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.plan_budget_working_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.plan_budget_working') }}</span><br>
                                <span class="m-widget21__sub">{{number_format($total_working, 2, '.', ',')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.plan_budget_expenditure_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.plan_budget_expenditure') }}</span><br>
                                <span class="m-widget21__sub">{{number_format($total_expenditure, 2, '.', ',') }} ({{ getPercentage($total_working,$total_expenditure) }}%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.spi_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.schedule_performance_index') }}</span><br>
                                <span class="m-widget21__sub">{{round($spi,2)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.estimated_plan_bugdet_expenditure_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.estimated_plan_bugdet_expenditure') }}</span><br>
                                <span class="m-widget21__sub">{{ number_format($estimated_budget, 2, '.', ',')}} ({{ getPercentage($total_working,$estimated_budget) }}%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.completion_date_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.completion_date') }}</span><br>
                                <span class="m-widget21__sub"> {{ dateCheck($getLastDateOfProject,$lang) }} </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit m-portlet-custom py-2">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon"><i class="la la-info-circle" data-toggle="tooltip" title="{{ trans('project_dashboard.estimated_completion_date_tooltip') }}"></i></span>
                            <div class="m-widget21__info px-1">
                                <span class="m-widget21__title py-0">{{ trans('project_dashboard.estimated_completion_date') }}</span><br>
                                <span class="m-widget21__sub"> {{ dateCheck($estimated_completion_date,$lang) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="col-lg-12 pb-3">
            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('project_dashboard.project_budget_report')}}</div>
            <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa {{ $class }} mx-1"></span>{{trans('project_dashboard.from_start_to_end_cfy')}}</div>
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Cumulative Graph</div>
                <div id="CumulativeLineChart" style="width: 100%;height:300px;"></div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Curve Graph</div>
                <div id="CurveLineChart" style="width: 100%;height:300px;"></div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="col-lg-12 pb-3">
            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('project_dashboard.project_fiscal_year_report')}}</div>
            <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa {{ $class }} mx-1"></span>{{trans('project_dashboard.from_current_fiscal_to_end')}}</div>
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Cumulative Graph</div>
                <div id="CumulativeFiscalYearLineChart" style="width: 100%;height:300px;"></div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12 widget21__sub text-primary"><span class="fa fa-crosshairs mx-2"></span>Curve Graph</div>
                <div id="CurveFiscalYearLineChart" style="width: 100%;height:300px;"></div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-lg-7">
                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('project_dashboard.project_bugdet_weekly')}}</div>
                <div class="col-lg-12 widget21__sub" style="color: #575962;">&nbsp;&nbsp;&nbsp;<span class="fa {{ $class }} mx-1"></span>{{trans('project_dashboard.from_given_report_start')}}</div>
            </div>
            <div class="col-lg-5">
                <br>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="weeklyForm" method="post" enctype="multipart/form-data">
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-7">
                            <input class="form-control m-input input-circle datePicker" type="text" name="date" id="date" value="{{ dateCheck(date('Y-m-01'),$lang) }}">
                        </div>
                        <div class="col-lg-3">
                            <input class="form-control m-input input-circle" type="number" name="months" id="months" min="1" max="12" value="1" placeholder="{{ trans('global.month') }}">
                        </div>
                        <div class="col-lg-2">
                            <input type="hidden" name="total_baseline" value="{{ $total_baseline }}">
                            <input type="hidden" name="location_id" value="{{implode(',',$location_id)}}">
                            <a class="btn btn-icon btn-sm btn-primary btn-circle" href="#!" class="accordion-toggle" onclick="storeData('{{route('project_dashboard.weekly_report')}}','weeklyForm','POST','weekly_response',put_content,true);">
                                <span class="fa flaticon-refresh"></span>
                            </a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
        <div class="row" id="weekly_response">
            <div class="col-lg-12 m-0">
                <div class="col-lg-12 text-center" style="color: #575962;">
                    <?php
                    $base   = 0;
                    $work   = 0;
                    $actual = 0; 
                    $index  = 0; 
                    foreach($weekly_report_base as $item)
                    {
                        $base   += $item['total_price'];
                        $work   += $weekly_report_working[$index]['total_price'];
                        $actual += $weekly_report_actual[$index]['total_price'];

                        $index++; 
                    }
                    ?>
                    <button class="btn btn-sm opacity" style="background: #64f062"></button> <span>{{ trans('portfolios.baseline_budget') }} : </span>{{round($base,3)}}
                    <button class="btn btn-sm opacity" style="background: #36c5e9"></button> <span>{{ trans('portfolios.working_budget') }} : </span>{{round($work,3)}}
                    <button class="btn btn-sm opacity" style="background: #fb9f7e"></button> <span>{{ trans('portfolios.actual_budget') }} : </span>{{round($actual,3)}}
                </div>
            </div>
            <div class="col-lg-12">
                <div id="project_budget_weekly" style="width:100%;height:500px;"></div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="m-portlet m-portlet--mobile m-0">
    <div class="m-portlet__body">
        <div class="col-lg-12">
            <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('project_dashboard.project_bugdet_status')}}</div>
            <div class="row col-lg-12">
                <div class="col-lg-6 widget21__sub" style="color: #575962;"><span class="fa {{ $class }} mx-1"></span>{{trans('project_dashboard.project_start_so_far')}}</div>
                <div class="col-lg-6 widget21__sub" style="color: #575962;"><span class="fa {{ $class }} mx-1"></span>{{trans('project_dashboard.cfy_start_so_far')}}</div>
            </div>
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-6">
                <div class="col-lg-12" id="projectBudgetStatusReport" style="width: 100%;height:300px;"></div>
                <div class="col-lg-12">
                    <button class="btn btn-sm opacity" style="background: #64f062"></button><span class="px-1">{{ trans('portfolios.baseline_budget') }}: </span> {{ getPercentage($total_baseline,$total_baseline_SOP) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm opacity" style="background: #36c5e9"></button><span class="px-1">{{ trans('portfolios.working_budget') }}: </span> {{ getPercentage($total_baseline,$total_working_SOP) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm opacity" style="background: #fb9f7e"></button><span class="px-1">{{ trans('portfolios.actual_budget') }}:</span> {{ getPercentage($total_baseline,$budget_expenditure_SOP) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-dark opacity" style="background: #343a40"></button><span class="px-1">{{ trans('portfolios.invoiced_budget') }}:</span> {{ getPercentage($total_baseline,$budget_invoiced_SOP) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-info opacity" style="background: #0d3366"></button><span class="px-1">{{ trans('portfolios.paid_budget') }}:</span> {{ getPercentage($total_baseline,$budget_paid_SOP) }}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12" id="projectBudgetStatusFiscalReport" style="width: 100%;height:300px;"></div>
                <div class="col-lg-12">
                    <button class="btn btn-sm opacity" style="background: #64f062"></button><span class="px-1" >{{ trans('portfolios.baseline_budget') }}: </span> {{ getPercentage($total_baseline,$total_baseline_CFY) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm opacity" style="background: #36c5e9"></button><span class="px-1" >{{ trans('portfolios.working_budget') }}: </span> {{ getPercentage($total_baseline,$total_working_CFY) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm opacity" style="background: #fb9f7e"></button><span class="px-1" >{{ trans('portfolios.actual_budget') }}:</span> {{ getPercentage($total_baseline,$budget_expenditure_CFY) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-dark opacity" style="background: #343a40"></button><span class="px-1" >{{ trans('portfolios.invoiced_budget') }}:</span> {{ getPercentage($total_baseline,$budget_invoiced_CFY) }} &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-info opacity" style="background: #0d3366"></button><span class="px-1" >{{ trans('portfolios.paid_budget') }}:</span> {{ getPercentage($total_baseline,$budget_paid_CFY) }}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    $(document).ready(function(){
        // Show graphs
        var CumulativeLineChart = echarts.init(document.getElementById('CumulativeLineChart'));
        var CumulativeFiscalYearLineChart = echarts.init(document.getElementById('CumulativeFiscalYearLineChart'));
        var CurveLineChart = echarts.init(document.getElementById('CurveLineChart'));
        var CurveLineFiscalChart = echarts.init(document.getElementById('CurveFiscalYearLineChart'));
        var ProjectBudgetWeekly = echarts.init(document.getElementById('project_budget_weekly'));
        var projectBudgetStatusReport = echarts.init(document.getElementById('projectBudgetStatusReport'));
        var projectBudgetStatusFiscalReport = echarts.init(document.getElementById('projectBudgetStatusFiscalReport'));

        CumulativeOption = {
            tooltip: {
            trigger: 'axis'
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align:  'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: 
                [
                    @php
                        $months = Config::get('static.dr.fiscal_year_month');
                        $i = 0; 
                    @endphp
                    @foreach($months as $item)
                        @if($i==0)
                            '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                        @else
                            '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                        @endif 
                        @php $i++ @endphp 
                    @endforeach
                ],
                axisLabel : {
                    rotate : 45,
                }
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_base)
                        {
                            // $all_months = 0;
                            // foreach($budget_base as $key=>$item)
                            // {
                            //     $all_months += $item['total_price']; 
                            // }
                            $total_months = 0;
                            foreach($budget_base as $key=>$item)
                            {
                                $total_months += $item['total_price'];
                                //echo getPercentage($all_months,$total_months).',';
                                echo getPercentage($BQ_total_price,$total_months).',';
                            }
                        }
                        ?>
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_working)
                        {
                            // $all_months = 0;
                            // foreach($budget_working as $key=>$item)
                            // {   
                            //     $all_months += $item['total_price'];
                            // }
                            $total_months = 0;
                            foreach($budget_working as $key=>$item)
                            {
                                $total_months += $item['total_price'];
                                //echo getPercentage($all_months,$total_months).',';
                                echo getPercentage($BQ_total_price,$total_months).',';
                            }
                        }
                        ?>
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_actual and $budget_base)
                        {
                            // $all_months = 0; 
                            // foreach($budget_base as $item)
                            // {
                            //     $all_months += $item['total_price'];
                            // }
                            $total_months = 0;
                            foreach($budget_actual as $key=>$item)
                            {
                                $total_months += $item['total_price'];
                                //echo getPercentage($all_months,$total_months).',';
                                echo getPercentage($BQ_total_price,$total_months).',';
                            }
                        }
                        ?>
                    ]
                }
            ]
        };
        CumulativeLineChart.setOption(CumulativeOption);

        CurveOption = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align: 'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: [
                            @php
                            $months = Config::get('static.dr.fiscal_year_month');
                            $i = 0; 
                        @endphp
                        @foreach($months as $item)
                            @if($i==0)
                                '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                            @else
                                '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                            @endif 
                            @php $i++ @endphp 
                        @endforeach
                    ],
                    axisLabel : {
                        rotate : 45,
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_base_curve)
                        {
                            foreach($budget_base_curve as $key=>$item)
                            {
                                echo getPercentage($BQ_total_price,$item['total_price']).',';
                            }
                        }
                        ?>
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_working_curve)
                        {
                            foreach($budget_working_curve as $key=>$item)
                            {
                                echo getPercentage($BQ_total_price,$item['total_price']).',';
                            }
                        }
                        ?>
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        <?php
                        if($budget_actual_curve and $budget_base)
                        {
                            foreach($budget_actual_curve as $key=>$item)
                            {
                                echo getPercentage($BQ_total_price,$item['total_price']).',';
                            }
                        }
                        ?>
                    ]
                }
            ]
        };
        CurveLineChart.setOption(CurveOption);

        CumulativeFiscalYearOption =  {
            tooltip: {
            trigger: 'axis'
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align:  'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: 
                [
                    @php
                        $months = Config::get('static.dr.fiscal_year_month');
                        $i = 0; 
                    @endphp
                    @foreach($months as $item)
                        @if($i==0)
                            '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                        @else
                            '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                        @endif 
                        @php $i++ @endphp 
                    @endforeach
                ],
                axisLabel : {
                    rotate : 45,
                }
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_base_cfy)
                            <?php $total_months = 0; ?>
                            @foreach($budget_base_cfy as $key=>$item)
                                @php $total_months = $total_months+$item['total_price']; @endphp
                                {{ getPercentage($total_baseline,$total_months) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_working_cfy)
                            <?php $total_months = 0; ?>
                            @foreach($budget_working_cfy as $key=>$item)
                                @php $total_months = $total_months+$item['total_price']; @endphp
                                {{ getPercentage($total_working_budget_cfy,$total_months) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_actual_cfy and $total_working_budget_cfy)
                            <?php $total_months = 0; ?>
                            @foreach($budget_actual_cfy as $key=>$item)
                                @php $total_months +=$item['total_price']; @endphp
                                {{ getPercentage($total_working_budget_cfy,$total_months) }},
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        CumulativeFiscalYearLineChart.setOption(CumulativeFiscalYearOption);

        CurveLineFiscalYearChart = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data:   ['{{ trans("portfolios.base") }}', '{{ trans("portfolios.working") }}', '{{ trans("portfolios.actual") }}'],
                layout: 'vertical',
                align: 'right',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        
                        @php
                            $months = Config::get('static.dr.fiscal_year_month');
                            $i = 0; 
                        @endphp
                        @foreach($months as $item)
                            @if($i==0)
                                '{{yearCheck(date("Y")-1,"dr")}}-{{ $item }}',
                            @else
                                '{{yearCheck(date("Y"),"dr")}}-{{ $item }}',
                            @endif 
                            @php $i++ @endphp 
                        @endforeach
                    ],
                    axisLabel : {
                        rotate : 45,
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '{{ trans("portfolios.base") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_base_cfy)
                            @foreach($budget_base_cfy as $key=>$item)
                                {{ getPercentage($total_baseline,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.working") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_working_cfy)
                            @foreach($budget_working_cfy as $key=>$item)
                                {{ getPercentage($total_working_budget_cfy,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                },
                {
                    name: '{{ trans("portfolios.actual") }}',
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                    data: [
                        @if($budget_actual_cfy && $total_working_budget_cfy)
                            @foreach($budget_actual_cfy as $key=>$item)
                                {{ getPercentage($total_working_budget_cfy,$item['total_price']) }},
                            @endforeach
                        @endif
                    ]
                }
            ]
        };
        CurveLineFiscalChart.setOption(CurveLineFiscalYearChart);

        ProjectBudgetWeeklyoption = {
            legend: {},
            tooltip: {},
            dataset: {
                source: [
                    @php $index=0; @endphp
                    @foreach($weekly_report_base as $item)
                        @php 
                        $work_value = 0;
                        @endphp
                        [
                            '{{substr(dateCheck($item['week_end_date'],$lang),0,7)}} [{{ trans("global.weeks") }} {{$item['week']}}]', {{round($item['total_price'],3)}}, {{round($weekly_report_working[$index]['total_price'],3)}} , {{round($weekly_report_actual[$index]['total_price'],3)}}
                        ],
                        @php $index++; @endphp
                    @endforeach
                ]
            },
            xAxis: {
                type: 'category',
                axisLabel: {
                    rotate : 25,
                },
            },
            yAxis: {},
            // Declare several bar series, each will be mapped
            // to a column of dataset.source by default.
            series: 
            [
                {
                    type: 'bar',
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                },
                {
                    type: 'bar',
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                },
                {
                    type: 'bar',
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                }
            ]
        };
        ProjectBudgetWeekly.setOption(ProjectBudgetWeeklyoption);

        StatusReportOption = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['{{ trans("portfolios.good_well") }}', '{{ trans("portfolios.in_danger") }}', '{{ trans("portfolios.not_so_good") }}'],
            },
            toolbox: {
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: 
                [
                    "","","","","",
                ],
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        {
                            value:  {{ getPercentage($total_baseline,$total_baseline_SOP) }},
                            itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_baseline,$total_working_SOP) }},
                            smooth: true,
                            itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_expenditure_SOP) }},
                            smooth: true,
                            itemStyle: {color: '#fb9f7e',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_invoiced_SOP) }},
                            smooth: true,
                            itemStyle: {color: '#343a40',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_paid_SOP) }},
                            smooth: true,
                            itemStyle: {color: '#0d3366',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        }
                    ],
                    type: 'bar',
                },
            ],

        };
        projectBudgetStatusReport.setOption(StatusReportOption);

        StatusFiscalReportOption = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['{{ trans("portfolios.good_well") }}', '{{ trans("portfolios.in_danger") }}', '{{ trans("portfolios.not_so_good") }}'],
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: 
                [
                    "","","","","",
                ],
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [
                        {
                            value:  {{ getPercentage($total_baseline,$total_baseline_CFY) }},
                            itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value: {{ getPercentage($total_baseline,$total_working_CFY) }},
                            smooth: true,
                            itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_expenditure_CFY) }},
                            smooth: true,
                            itemStyle: {color: '#fb9f7e',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_invoiced_CFY) }},
                            smooth: true,
                            itemStyle: {color: '#343a40',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        },
                        {
                            value:  {{ getPercentage($total_baseline,$budget_paid_CFY) }},
                            smooth: true,
                            itemStyle: {color: '#0d3366',opacity: 0.5, barBorderRadius: [5, 5, 0, 0]},
                        }
                    ],
                    type: 'bar',
                },
            ],

        };
        projectBudgetStatusFiscalReport.setOption(StatusFiscalReportOption);
    });
</script>