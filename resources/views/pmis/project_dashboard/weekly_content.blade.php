<div class="col-lg-12 m-0">
    <div class="col-lg-12 text-center" style="color: #575962;">
        <?php
            $base   = 0;
            $work   = 0;
            $actual = 0; 
            $index  = 0; 
            foreach($weekly_report_base as $item)
            {
                $base   += $item['total_price'];
                $work   += $weekly_report_working[$index]['total_price'];
                $actual += $weekly_report_actual[$index]['total_price'];

                $index++; 
            }
        ?>
        <button class="btn btn-sm opacity" style="background: #64f062"></button> <span>{{ trans('portfolios.baseline_budget') }} : </span>{{round($base,3)}}
        <button class="btn btn-sm opacity" style="background: #36c5e9"></button> <span>{{ trans('portfolios.working_budget') }} : </span>{{round($work,3)}}
        <button class="btn btn-sm opacity" style="background: #fb9f7e"></button> <span>{{ trans('portfolios.actual_budget') }} : </span>{{round($actual,3)}}
    </div>
</div>
<div class="col-lg-12">
    <div id="project_budget_weekly" style="width:100%;height:500px;"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var ProjectBudgetWeekly = echarts.init(document.getElementById('project_budget_weekly'));
        ProjectBudgetWeeklyoption = 
        {
            legend: {},
            tooltip: {},
            dataset: {
                source: [
                    @php $index=0; @endphp
                        @foreach($weekly_report_base as $item)

                            @php 
                            $work_value = 0;
                            @endphp
                            [
                                '{{substr(dateCheck($item['week_end_date'],$lang),0,7)}} [{{ trans("global.weeks") }} {{$item['week']}}]', {{round($item['total_price'],3)}}, {{round($weekly_report_working[$index]['total_price'],3)}} , {{round($weekly_report_actual[$index]['total_price'],3)}}
                            ],
                            @php $index++; @endphp
                        @endforeach
                ]
            },
            xAxis: {
                type: 'category',
                axisLabel: {
                    rotate : 25,
                },
            },
            yAxis: {},
            // Declare several bar series, each will be mapped
            // to a column of dataset.source by default.
            series: 
            [
                {
                    type: 'bar',
                    itemStyle: {color: '#64f062',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                },
                {
                    type: 'bar',
                    itemStyle: {color: '#36c5e9',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                },
                {
                    type: 'bar',
                    itemStyle: {color: '#fb9f7e',opacity: 0.7, barBorderRadius: [5, 5, 0, 0]}
                }
            ]
        };
        ProjectBudgetWeekly.setOption(ProjectBudgetWeeklyoption);
    });
</script>