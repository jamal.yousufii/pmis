<form class="m-form m-form--fit m-form--label-align-right" id="priorityForm" method="POST" enctype="multipart/form-data">
    <div class="m-portlet__body p-0">
        <div class="form-group m-form__group px-0"">
            <label>{{ trans('global.project_priority') }}<span style="color:red;">*</span></label>
            <div id="priority-div" class="errorDiv">
                <select class="form-control m-input m-input--air select-2 required" name="priority" style="width: 100%">
                    <option value="1" <?=$record->priority=='1'? 'selected': ''?> >{{ trans('report.high') }}</option>
                    <option value="2" <?=$record->priority=='2'? 'selected': ''?> >{{ trans('report.medium') }}</option>
                    <option value="3" <?=$record->priority=='3'? 'selected': ''?> >{{ trans('report.low') }}</option>
                </select>
            </div>
            <div class="priority error-div" style="display:none;"></div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions pull-left px-0"">
            <input type="hidden" name="record_id" value="{{$record_id}}" />
            <button type="button" onclick="storeRecord('{{ route('progress.priority') }}','priorityForm','POST','response_div',redirectFunction,true,this.id);" id="add" class="btn btn-primary btn-sm">{{ trans("global.submit") }}</button>
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">{{ trans("global.cancel") }}</button>
        </div>
    </div>
</form>
