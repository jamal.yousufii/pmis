@extends('master')
@section('head')
    <title>{{ trans('calendar.calendar') }}</title>
    <!--begin:: Fullcalendar Custom CSS -->
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/fullcalendar-custom/fullcalendar.print.css')}}" rel="stylesheet" type="text/css" media='print' />
    <!--end:: Fullcalendar Custom CSS -->
@endsection
@section('content')
    <div class="row mt-1">
        <div class="col-lg-12 pb-1">
            <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('project_dashboard.project' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{$plan->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-symbol m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('monitoring.contractor') }}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $plan->procurement->company }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-2 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('progress.contraction_duration')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ differenceBetweenDate($plan->procurement->start_date,$plan->procurement->end_date,'%a',true) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-3 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('project_dashboard.contract_amount')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{number_format($plan->procurement->contract_price)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile mb-2" id="showContent">
        <div class="m-portlet__head table-responsive border-0">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand border-0 mb-0" role="tablist">
                        @if($tabs)
                            @foreach($tabs as $item)
                            @if(check_my_section(array($depID),$item->code))
                                <li class="nav-item m-tabs__item">
                                    <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>encrypt($depID)]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                        <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                    </a>
                                </li>
                            @endif
                            @endforeach
                        @endif
                    </ul>
                </div> 
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',encrypt($depID)) }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile mb-0">
        @if($excluded_locations->count()>0)
            <div class="accordion row mx-1 mt-1">
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                    <div class="m-alert__text text-dark bg-warning">
                        <div>
                            <i class="la la-warning text-dark"></i> &nbsp; {{ trans('progress.excluded_projects') }}: 
                            <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                <span><i class="la la-angle-double-down"></i></span>
                            </a>
                        </div>
                        <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2 bg-warning">
                                <div class="m-portlet__body bg-warning">
                                    <div class="m-portlet__body table-responsive bg-warning">
                                        <ul class="list-group list-group-flush bg-warning">
                                            @foreach($excluded_locations as $item)
                                                <li>
                                                    @if($item->status==1 and check_my_section(array($depID),'pmis_completed'))
                                                    <a href="{{ route('completed.projects',['id'=>$enc_id,'dep_id'=>encrypt($depID)]) }}" target="_blank">
                                                    @elseif($item->status==2 and check_my_section(array($depID),'pmis_stopped'))
                                                    <a href="{{ route('stopped.projects',['id'=>$enc_id,'dep_id'=>encrypt($depID)]) }}" target="_blank">
                                                    @endif
                                                        {{ $loop->iteration }} - 
                                                        {{ $item->province()->first()->{'name_'.$lang} }} 
                                                        @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif 
                                                        @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                        @if($item->latitude)/ {{ $item->latitude }} @endif 
                                                        @if($item->longitude)/ {{ $item->longitude }} @endif
                                                    </a>
                                                    <ul>
                                                        <li>
                                                            @if($item->status==1)
                                                                {{ trans('global.excluded_description',['status'=> __('global.completed') ]) }}
                                                            @elseif($item->status==2)
                                                                {{ trans('progress.excluded_description',['status'=> __('global.stopped') ]) }}
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        {{-- End of Collpas div --}} 
                    </div>
                </div>
            </div>
        @endif
        <div class="">
            <!-- Include Attachments Modal -->
            @include('pmis.progress.calendar.publicHolidays')
            @include('pmis.progress.calendar.workingDays')
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv"> 
                <div class="form-group pb-3 px-3 row">
                    <div class="col-lg-9">
                        <label class="title-custom pt-3" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                        <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('calendars.show',[$enc_id,encrypt($depID),'loc_id']) }}',this.value)" >
                            <option value="">{{ trans('global.select') }}</option>
                            @if($project_location)
                                @foreach($project_location as $item)
                                    <option value="{{$item->id}}" <?=$project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-3 text-center">
                        <label class="title-custom pt-3" style="color:#575962">&nbsp;</label><br>
                        <div class="m-btn-group m-btn-group--pill btn-group m-btn-group btn-group-sm" role="group" aria-label="Large button group">
                            <button type="button" data-toggle="modal" data-target="#WorkingDaysModal" class="btn btn-primary btn-sm"> <span><i class="la la-cart-plus"></i> <span>{{ trans('calendar.working_days') }}</span></span></button>
                            <button type="button" data-toggle="modal" data-target="#PublicHolidaysModal" class="btn btn-primary border-left btn-sm"><span><i class="la la-cart-plus"></i> <span>{{ trans('calendar.public_holidays') }}</span></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @alert()
        @endalert
    </div>
    @if($holidays)
        @foreach($holidays as $item)
            <button type="button" data-toggle="modal" data-target="#PublicHolidaysModalContent_{{$item->id}}" id="eventBtn_{{$item->id}}" class="btn btn-primary border-left btn-sm d-none"><span><i class="la la-cart-plus"></i> <span>{{ trans('calendar.public_holidays') }}</span></span></button>
            <div id="PublicHolidaysModalContent_{{$item->id}}" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="PublicHolidaysModalContent_{{$item->id}}" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
                            <!--begin::head-->
                            <div class="m-portlet__head table-responsive">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title"><h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('calendar.public_holidays') }}</h3></div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button></li>
                                    </ul>
                                </div>
                            </div>
                            <!--end::Head--> 
                            <!--begin:: Content-->
                            <div class="m-portlet__body">
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                        <tr>
                                            <th width="15%">{{ trans('calendar.from') }}</th>
                                            <th width="15%">{{ trans('calendar.to') }}</th>
                                            <th width="60%">{{ trans('calendar.description') }}</th>
                                            <th width="20%">{{ trans('global.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                        <tr>
                                            <td>{{ dateCheck($item->from_date,$lang) }}</td>
                                            <td>{{ dateCheck($item->to_date,$lang) }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                @if($schedule and doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_cal_delete"))
                                                    <a href="javascript:void()" onclick="destroy('{{route('calendar.destroy',encrypt($item->id))}}','','DELETE','response_div')" class="btn btn-icon btn-sm btn-danger btn-circle mx-1 m-1"><i class="la la-trash"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end:: Content-->
                        </div>
                    </div>
                </div>
            </div>   
        @endforeach
    @endif
    <div class="m-portlet m-portlet--mobile px-2 pb-2">
        <div id='calendar' class="p-4 pt-0 mt-0"></div>   
    </div>
@endsection
@section('js-code')
    <!--begin:: Fullcalendar Custom JS -->
    <script src="{{asset('public/assets/fullcalendar-custom/lib/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/lib/moment-jalaali.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/fullcalendar-custom/locale-all.js')}}" type="text/javascript"></script>
    <!--end:: Fullcalendar Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {
            var lang = '{{$lang}}';
            if(lang!='en'){
                lang = 'fa';
            }
            var initialLocaleCode = lang;
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: '',
                    right: 'title'
                },
                @if($lang!='en')
                    isJalaali : true,
                @else
                    isJalaali : false,
                @endif
                defaultDate: '{{date("Y-m-d")}}',
                locale: lang,
                buttonIcons: true, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    @if($holidays)
                        @foreach($holidays as $item)
                            <?php 
                            $to_date = new DateTime($item->to_date); 
                            $to_date = $to_date->format('Y-m-d');
                            ?>
                            {
                                id: '{{$item->id}}',
                                title: '{{$item->description}}',
                                start: '{{$item->from_date}}',
                                end: '{{$to_date}}',
                                allDay: false,
                                backgroundColor: '#f4516c',
                                textColor: '#FFF',
                                weekend: false,
                                display: 'inverse-background',
                            },
                        @endforeach
                    @endif
                ],
                eventClick:function(event){
                    $('#eventBtn_'+event.id).click();
                    
                }, 
                
            });
            
            @if($working_days)
                @if($working_days->saturday==0)
                    $('.fc-sat').css('color','red');
                    $('.fc-sat').css('background','#ebedf24a');
                    $('.fc-sat').css('border-color','#ddd');
                @endif
                @if($working_days->sunday==0)
                    $('.fc-sun').css('color','red');
                    $('.fc-sun').css('background','#ebedf24a');
                    $('.fc-sun').css('border-color','#ddd');
                @endif
                @if($working_days->monday==0)
                    $('.fc-mon').css('color','red');
                    $('.fc-mon').css('background','#ebedf24a');
                    $('.fc-mon').css('border-color','#ddd');
                @endif
                @if($working_days->tuesday==0)
                    $('.fc-tue').css('color','red');
                    $('.fc-tue').css('background','#ebedf24a');
                    $('.fc-tue').css('border-color','#ddd');
                @endif
                @if($working_days->wednesday==0)
                    $('.fc-wed').css('color','red');
                    $('.fc-wed').css('background','#ebedf24a');
                    $('.fc-wed').css('border-color','#ddd');
                @endif
                @if($working_days->thursday==0)
                    $('.fc-thu').css('color','red');
                    $('.fc-thu').css('background','#ebedf24a');
                    $('.fc-thu').css('border-color','#ddd');
                @endif
                @if($working_days->friday==0)
                    $('.fc-fri').css('color','red');
                    $('.fc-fri').css('background','#ebedf24a');
                    $('.fc-fri').css('border-color','#ddd');
                @endif
            @endif

            $('.fc-prev-button').click(function(){
                @if($working_days)
                    @if($working_days->saturday==0)
                        $('.fc-sat').css('color','red');
                        $('.fc-sat').css('background','#ebedf24a');
                        $('.fc-sat').css('border-color','#ddd');
                    @endif
                    @if($working_days->sunday==0)
                        $('.fc-sun').css('color','red');
                        $('.fc-sun').css('background','#ebedf24a');
                        $('.fc-sun').css('border-color','#ddd');
                    @endif
                    @if($working_days->monday==0)
                        $('.fc-mon').css('color','red');
                        $('.fc-mon').css('background','#ebedf24a');
                        $('.fc-mon').css('border-color','#ddd');
                    @endif
                    @if($working_days->tuesday==0)
                        $('.fc-tue').css('color','red');
                        $('.fc-tue').css('background','#ebedf24a');
                        $('.fc-tue').css('border-color','#ddd');
                    @endif
                    @if($working_days->wednesday==0)
                        $('.fc-wed').css('color','red');
                        $('.fc-wed').css('background','#ebedf24a');
                        $('.fc-wed').css('border-color','#ddd');
                    @endif
                    @if($working_days->thursday==0)
                        $('.fc-thu').css('color','red');
                        $('.fc-thu').css('background','#ebedf24a');
                        $('.fc-thu').css('border-color','#ddd');
                    @endif
                    @if($working_days->friday==0)
                        $('.fc-fri').css('color','red');
                        $('.fc-fri').css('background','#ebedf24a');
                        $('.fc-fri').css('border-color','#ddd');
                    @endif
                @endif
            });

            $('.fc-next-button').click(function(){
                @if($working_days)
                    @if($working_days->saturday==0)
                        $('.fc-sat').css('color','red');
                        $('.fc-sat').css('background','#ebedf24a');
                        $('.fc-sat').css('border-color','#ddd');
                    @endif
                    @if($working_days->sunday==0)
                        $('.fc-sun').css('color','red');
                        $('.fc-sun').css('background','#ebedf24a');
                        $('.fc-sun').css('border-color','#ddd');
                    @endif
                    @if($working_days->monday==0)
                        $('.fc-mon').css('color','red');
                        $('.fc-mon').css('background','#ebedf24a');
                        $('.fc-mon').css('border-color','#ddd');
                    @endif
                    @if($working_days->tuesday==0)
                        $('.fc-tue').css('color','red');
                        $('.fc-tue').css('background','#ebedf24a');
                        $('.fc-tue').css('border-color','#ddd');
                    @endif
                    @if($working_days->wednesday==0)
                        $('.fc-wed').css('color','red');
                        $('.fc-wed').css('background','#ebedf24a');
                        $('.fc-wed').css('border-color','#ddd');
                    @endif
                    @if($working_days->thursday==0)
                        $('.fc-thu').css('color','red');
                        $('.fc-thu').css('background','#ebedf24a');
                        $('.fc-thu').css('border-color','#ddd');
                    @endif
                    @if($working_days->friday==0)
                        $('.fc-fri').css('color','red');
                        $('.fc-fri').css('background','#ebedf24a');
                        $('.fc-fri').css('border-color','#ddd');
                    @endif
                @endif
            });
	    });

    </script>
@endsection
