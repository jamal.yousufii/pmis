{{-- Start of the Modal --}}
<div id="WorkingDaysModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="WorkingDaysModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head table-responsive">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                              <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('calendar.working_days') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" enctype="multipart/form-data" id="workingDaysForm">
                      <div class="col-12">
                        <table class="table">
                          <thead>
                            <tr>
                              <td>{{ trans('calendar.day') }}</td>
                              <td>{{ trans('calendar.full_day') }}</td>
                              <td>{{ trans('calendar.half_day') }}</td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{{ trans('calendar.saturday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="saturday" value="8" id="saturday" <?=$working_days?($working_days->saturday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="saturday" value="4" id="saturday" <?=$working_days?($working_days->saturday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.sunday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="sunday" value="8" id="sunday" <?=$working_days?($working_days->sunday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="sunday" value="4" id="sunday" <?=$working_days?($working_days->sunday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.monday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="monday" value="8" id="monday" <?=$working_days?($working_days->monday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="monday" value="4" id="monday" <?=$working_days?($working_days->monday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.tuesday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="tuesday" value="8" id="tuesday" <?=$working_days?($working_days->tuesday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="tuesday" value="4" id="tuesday" <?=$working_days?($working_days->tuesday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.wednesday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="wednesday" value="8" id="wednesday" <?=$working_days?($working_days->wednesday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="wednesday" value="4" id="wednesday" <?=$working_days?($working_days->wednesday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.thursday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="thursday" value="8" id="thursday" <?=$working_days?($working_days->thursday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="thursday" value="4" id="thursday" <?=$working_days?($working_days->thursday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                            <tr>
                              <td>{{ trans('calendar.friday') }}</td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="friday" value="8" id="friday" <?=$working_days?($working_days->friday=='8'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                              <td>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="friday" value="4" id="friday" <?=$working_days?($working_days->friday=='4'? 'checked' : '') : ''?> /><span class="checkbox" @if($lang!='en') style="transform: rotate(90deg);" @endif></span>
                                </label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions align">
                          <input name="project_id" type="hidden" value="{{ decrypt($enc_id) }}"/>
                          <input name="location_id" type="hidden" value="{{ $project_location_id }}"/>
                          @if($working_days)
                            <button type="button" onclick="doEditRecord('{{route('calendar.update',encrypt($working_days->id))}}','workingDaysForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                          @else
                            <button type="button" onclick="storeRecord('{{route('calendar.store')}}','workingDaysForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                          @endif
                          <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                        </div>
                      </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
{{-- End of the Modal  --}}
<script type="text/javascript">
  function makeGroupBox(element){
    var element_id   = element.id;
    var element_name = element.name;
    if (document.getElementById(element_id).checked) {
      document.getElementByName(element_name).checked=false;
      document.getElementById(element_id).checked=true;
    } else {
      document.getElementById(element_id).checked=false;
    }
  };
</script>