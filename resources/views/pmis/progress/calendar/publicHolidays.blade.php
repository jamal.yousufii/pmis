{{-- Start of the Modal --}}
<div id="PublicHolidaysModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="PublicHolidaysModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div id="share_content">
            <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
              <!--begin::head-->
              <div class="m-portlet__head table-responsive">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('calendar.public_holidays') }}</h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                  <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                    </li>
                  </ul>
                </div>
              </div>
              <!--end::Head-->
              <div class="m-form m-form--label-align-right p-2">
                <div class="form-group m-form__group row m-form__group_custom">
                  <div class="col-lg-12">
                    <label class="">{{ trans('calendar.import_project') }} : <span style="color:red;">*</span></label><br>
                    <span class=" m-switch m-switch--icon">
                      <label><input type="checkbox" class="form-control m-input" name="request" id="request" onclick="holidaysContent(this.id,'publicHolidaysFormNew','publicHolidaysFormImport')"><span></span></label>
                    </span>
                  </div>
                </div>
              </div>  
              <!--begin::Form-->
              <form class="m-form m-form--label-align-right p-2" enctype="multipart/form-data" id="publicHolidaysFormNew">
                <div class="form-group m-form__group row m-form__group_custom">
                  <div class="col-lg-6">
                    <label class="">{{ trans('calendar.from') }}: <span style="color:red;">*</span></label>
                    <input class="form-control m-input datePicker errorDiv" type="text" value="" name="from_date" id="from_date" required>
                    <div class="from_date error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-6">
                    <label class="">{{ trans('calendar.to') }}: <span style="color:red;">*</span></label>
                    <input class="form-control m-input datePicker errorDiv" type="text" value="" name="to_date" id="to_date" required>
                    <div class="to_date error-div" style="display:none;"></div>
                  </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                  <div class="col-lg-12">
                    <label class="">{{ trans('calendar.description') }}: <span style="color:red;">*</span></label>
                      <input class="form-control m-input errorDiv" type="text" value="" name="description" id="description" required>
                    <div class="description error-div" style="display:none;"></div>
                  </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions align">
                    <input name="project_id" type="hidden" value="{{ decrypt($enc_id) }}"/>
                    <input name="location_id" type="hidden" value="{{ $project_location_id }}"/>
                    <button type="button" onclick="storeRecord('{{route('calendar.store_holidays')}}','publicHolidaysFormNew','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                  </div>
                </div>
              </form>
              <!--end::Form-->

              <!--begin::Form-->
              <form class="m-form m-form--label-align-right p-2 d-none" enctype="multipart/form-data" id="publicHolidaysFormImport">
                <div class="form-group m-form__group row m-form__group_custom">
                  <div class="col-lg-6">
                    <label class="">{{ trans('calendar.project') }}: <span style="color:red;">*</span></label>
                    <div id="imp_project_id" class="errorDiv">
                      <select class="form-control m-input m-input--air select-2" name="imp_project_id" id="project_id" style="width: 100%" onchange="viewRecord('{{route('calendar.locations')}}','id='+this.value,'POST','project_location_id')">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($projects)
                          @foreach($projects as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                    <div class="imp_project_id error-div" style="display:none;"></div>
                  </div>
                  <div class="col-lg-6">
                    <label class="">{{ trans('calendar.location') }}: <span style="color:red;">*</span></label>
                    <div id="imp_location_id" class="errorDiv">
                      <select class="form-control m-input m-input--air select-2" name="imp_location_id" style="width: 100%" id="project_location_id">  
                        <option value="">{{ trans('global.select') }}</option>  
                      </select>
                    </div>
                    <div class="imp_location_id error-div" style="display:none;"></div>
                  </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions align">
                    <input name="project_id" type="hidden" value="{{ decrypt($enc_id) }}"/>
                    <input name="location_id" type="hidden" value="{{ $project_location_id }}"/>
                    <button type="button" onclick="storeRecord('{{route('calendar.import_holidays')}}','publicHolidaysFormImport','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        @if(get_language()=="en")
            $(".datePicker").attr('type', 'date');
        @else
            $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
        @endif
        $(".select-2").select2();
    });
</script>
