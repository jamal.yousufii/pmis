@extends('master')
@section('head')
  <title>{{ trans('plans.plan') }}</title>
@endsection
@section('content')
  <!-- Include Project location Add Modal -->
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
          </div> 
      </div>
      <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                  <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',$dep_id) }}">
                      <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                  </a>
              </li>
          </ul>
      </div>
    </div>
    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist" style="padding-right:10px; padding-left:10px;">
        @if($tabs)
            @foreach($tabs as $item)
              @if(check_my_section(array(decrypt($dep_id)),$item->code))
                <li class="nav-item m-tabs__item">
                    <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>$dep_id]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                        <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                    </a>
                </li>
              @endif
            @endforeach
        @endif
    </ul>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
        <div class="form-group pl-3 pr-3 pb-3 row">
            <div class="col-lg-12">
                <label class="title-custom" style="color:#575962">{{ trans('global.graph_by_bill') }}:</label><br>
                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('charts/BQChart',[$enc_id,$dep_id,'loc_id']) }}',this.value)" >
                    <option value="">{{ trans('global.select') }}</option>
                    @if($project_location)
                        @foreach($project_location as $item)
                            <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
  </div>
@endsection
