@extends('master')
@section('head')
  <title>{{ trans('plans.plan') }}</title>
@endsection
@section('content')
  <!-- Include Project location Add Modal -->
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{$plan->name}}</h3>
          </div> 
      </div>
      <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                  <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',$dep_id) }}">
                      <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                  </a>
              </li>
          </ul>
      </div>
    </div>
    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist" style="padding-right:10px; padding-left:10px;">
        @if($tabs)
            @foreach($tabs as $item)
              @if(check_my_section(array(decrypt($dep_id)),$item->code))
                <li class="nav-item m-tabs__item">
                    <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>$dep_id]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                        <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                    </a>
                </li>
              @endif
            @endforeach
        @endif
    </ul>
    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv">
        <div class="form-group pl-3 pr-3 pb-3 row">
            <div class="col-lg-12">
                <label class="title-custom" style="color:#575962">{{ trans('global.graph_by_bill') }}:</label><br>
                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('charts/BQChart',[$enc_id,$dep_id,'loc_id']) }}',this.value)" >
                    <option value="">{{ trans('global.select') }}</option>
                    @if($project_location)
                        @foreach($project_location as $item)
                            <option value="{{$item->id}}" <?= $project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
      <div class="chart-container p-1" id="chart-display" style="height:50vh;">
        <canvas id="chart"></canvas>
      </div>
    </div>
  </div>
@endsection

@section('js-code')
  <script type="text/javascript">

      window.addEventListener('resize', function(event){
        $("#chart").css('width',$(window).width());
        location.reload();
      });
      var Dashboard=function(){
        var e=function(e,t,a,r){if(0!=e.length){return new Chart(e,o)}};
        return{
          init:function(){
            var a,r;!function(){
              var e=$("#chart");
              if(0!=e.length)
              {
                var t={
                  labels:[
                    @if($records)
                      @foreach($records as $rec)
                        @foreach($rec->dailyActivity as $item)
                          "{{ $item->bill_quantity['operation_type'] }}",
                          <?php $remained[$item->id]= $item->bill_quantity['amount']; ?>//Get total BQ
                        @endforeach
                      @endforeach
                    @endif
                  ],
                  datasets:[
                    {
                      backgroundColor:mApp.getColor("success"),
                      data:[
                        @if($records)
                          @foreach($records as $rec)
                            @foreach($rec->dailyActivity as $item)
                              "{{ $item->amount }}",
                              <?php $remained[$item->id] = $remained[$item->id] - $item->amount; ?>//Get remained BQ
                            @endforeach
                          @endforeach
                        @endif
                      ]
                    },
                    {
                      backgroundColor:mApp.getColor("warning"),
                      data:[
                        @foreach($remained as $item)
                          "{{ $item }}",//set remained BQ
                        @endforeach
                      ]
                    }
                  ]
                };
                new Chart(e,{
                  type:"bar",
                  data:t,
                  options:{
                    title: {
                      display: !1,
                    },
                    tooltips:{
                      intersect:!1,
                      mode:"nearest",
                      xPadding:5,
                      yPadding:5,
                      x:50,
                      caretPadding:10,
                      titleFontFamily: 'B Nazanin',
                      backgroundColor: '#17a2b8',
                      borderColor: '#343a40',
                      titleFontSize: 15,
                      titleFontColor: '#fff',
                      bodyFontColor: '#fff',
                      bodyFontSize: 12,
                    },
                    legend:{
                      display:!1
                    },
                    scales:{
                      xAxes: [{
                        type: 'category',
                        display:!1,
                        gridLines:!1,
                        stacked:!0
                      }],
                      yAxes:[{
                        stacked:!0,
                      }]
                    },
                    layout:{
                      padding:{
                        left:0,
                        right:0,
                        top:0,
                        bottom:0
                      }
                    },
                    responsive: true,
                    maintainAspectRatio:false,
                    barRadius:2,
                  }
                })
              }
            }(),
            a=$(".owl-carousel"),r=$("#m_widget_body_owlcarousel_items"),a.children().each(function(e){$(this).attr("data-position",e)}),r.owlCarousel({rtl:mUtil.isRTL(),items:1,animateIn:"fadeIn(100)",loop:!0}),a.owlCarousel({rtl:mUtil.isRTL(),center:!0,loop:!0,items:2}),$(document).on("click",".carousel",function(){a.trigger("to.owl.carousel",$(this).data("position"))})
          }
        }
      }();
      jQuery(document).ready(function(){Dashboard.init()});
  </script>

@endsection
