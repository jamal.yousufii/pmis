<div id="startStopModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="startStopModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                <!--begin::head-->
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{trans('change_request.startstop_add')}}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Head-->
            </div>
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="startStopForm">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-3">
                            <label>{{ trans('change_request.startstop') }}</label><br>
                            <span class=" m-switch m-switch--icon"><label><input type="checkbox" class="form-control m-input" onchange="display_content(this.id,'{{$request->project_id}}','{{$request->project_location_id}}','{{ route('get_start_stop_content')}}','GET','dateContent','startStop_stop','startStop_start')" name="startstop" id="startstop"><span></span></label></span>
                        </div>
                        <div class="col-lg-9">
                            <label class="">{{ trans('daily_report.project_location') }}: <span style="color:red;">*</span></label>
                            <select class="form-control m-input m-input--air select-2" style="width: 100%;" name="locations" id="locations">
                                <option value="{{$request->project_location_id}}" selected>{{ $project_locations->province->{'name_'.$lang} }} @if($project_locations->district_id) / {{ $project_locations->district->{'name_'.$lang} }} @endif @if($project_locations->village_id) / {{ $project_locations->village->{'name_'.$lang} }} @endif</option>
                                <option value="-">{{ trans('global.all') }}</option>
                            </select>
                            <span class="m-form__help">{{ trans('change_request.change_all') }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2" id="dateContent">
                        <div class="col-lg-12">
                            <label >{{ trans('change_request.date_start') }}: <span style="color:red;">*</span></label><br>
                            <input class="form-control m-input datePicker errorDiv" type="text" name="start_date" id="start_date">
                            <div class="start_date error-div" style="display:none;"></div>
                            <!-- custom value for end date -->
                            <input type="hidden" name="end_date" id="end_date" value="1"> 
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label>{{ trans('estimation.remarks') }}: <span style="color:red;">*</span></label>
                            <textarea class="form-control m-input m-input--air errorDiv" name="remarks" id="remarks" rows="2"></textarea>
                            <div class="remarks error-div" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <label >{{ trans('global.attachment') }}:</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file" id="file" onchange="chooseFile(this.id)">
                                <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                            </div>
                            <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom py-2">
                        <div class="col-lg-12">
                            <!-- Hidden inputs -->
                            <input type="hidden" name="project_id" value="{{ $request->project_id }}"/>
                            <input type="hidden" name="location_id" value="{{ $request->project_location_id }}"/>
                            <input type="hidden" name="project_name" value="{{$plan->name}}"/>
                            <input type="hidden" name="attachment_type" value="3"/>
                            <button type="button" class="btn btn-primary btn-sm" onclick="storeData('{{route('store_start_stop')}}','startStopForm','POST','response_div',getChangeRequest,false,'startStopModal');">{{trans('global.submit')}}</button>
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
