<div id="start_stop_edit_modal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="start_stop_edit_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{trans('change_request.startstop_add')}}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <div id="edit_start_stop_content">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
