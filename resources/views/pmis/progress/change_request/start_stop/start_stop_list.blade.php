<!-- start:: project summary   -->
<h4 class="m-portlet__head-text custom-heading">{{ trans('global.project_summary') }} </h4>
<div class="row mt-4">
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-box m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.project_name')}}</span><br>
                                <span class="m-widget21__sub">{{$plan->name}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-signs m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.project_code')}}</span><br>
                                <span class="m-widget21__sub">{{ $plan->procurement->contract_code }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-coins m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.proj_category')}}</span><br>
                                <span class="m-widget21__sub">{{ $plan->category->{'name_'.$lang} }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-business m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.project_type')}}</span><br>
                                <span class="m-widget21__sub">{{ $plan->project_type->{'name_'.$lang} }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-2 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.project_start_date')}}</span><br>
                                <span class="m-widget21__sub">{{ dateCheck($plan->procurement->start_date,$lang)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
    <div class="col-lg-4">
        <!--begin:: Widgets/Adwords Stats-->
        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
            <div class="m-widget21">
                <div class="row">
                    <div class="col">
                        <div class="m-widget21__item proj_sum_wdg">
                            <span class="m-widget21__icon">
                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="fa flaticon-calendar-3 m--font-light"></i>
                                </a>
                            </span>
                            <div class="m-widget21__info">
                                <span class="m-widget21__title">{{trans('plans.project_end_date')}}</span><br>
                                <span class="m-widget21__sub">{{ dateCheck($plan->procurement->start_date,$lang)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Adwords Stats-->
    </div>
</div>
<!-- end:: project summary --> 
<!-- start:list content  -->
<div class="m-portlet m-portlet--mobile">
    {{-- include the navigation  --}}
    @include('pmis.progress.change_request.start_stop.start_stop_operation_modal')
    @include('pmis.progress.change_request.cost_scope.cost_scope_navigation') 
    @include('pmis.progress.change_request.start_stop.start_stop_edit')
    <div class="tab-content">
        <div class="tab-pane active show m-portlet__body p-0">
            <div class="tab-pane active show">
                <div class="m-portlet__head my-0">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">{{trans('change_request.list_scop_cost')}}</h3>
                        </div>
                    </div>
                    @include('pmis.progress.change_request.start_stop.start_stop_add')
                    <div class="m-portlet__head-tools">
                        @if(doIHaveRoleInDep(session('current_department'),'pmis_change_request','pmis_change_request_add'))  
                            <button type="button" class="btn btn-info m-btn m-btn--icon btn-sm" data-toggle="modal" data-target="#startStopModal"> <span><i class="fa fa-plus"></i> <span>{{ trans('change_request.add') }}</span></span></button>
                        @endif
                    </div>
                </div>
                @alert()
                @endalert
                <div class="m-portlet__body" id="searchresult">
                    <table class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr class="bg-light">
                                <th width="5%">{{ trans('global.number') }}</th>
                                <th width="15%">{{ trans('global.request_type') }}</th>
                                <th width="10%">{{ trans('change_request.date_start') }}</th>
                                <th width="10%">{{ trans('change_request.date_stop') }}</th>
                                <th width="30%">{{ trans('change_request.description') }}</th>
                                <th width="6%">{{ trans('global.status') }}</th>
                                <th width="6%">{{ trans('global.attachment') }}</th>
                                <th width="6%">{{ trans('global.action') }}</th>
                            </tr>
                        </thead>
                        @if($request_changes->count()>0)
                            @foreach($request_changes AS $item)
                                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                    <tr class="category">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if($item->start_stop=='0')
                                                {{ trans('change_request.start') }}
                                            @else
                                                {{ trans('change_request.stop') }}
                                            @endif
                                        </td>
                                        <td>{{ $item->start_date }}</td>
                                        <td>{{ $item->end_date }}</td>
                                        <td>{{ $item->remarks }}</td>
                                        <td>
                                            @if($item->operation=='0')
                                                @if(doIHaveRoleInDep(session('current_department'),'pmis_change_request','pmis_change_request_approval'))
                                                    <a class="btn btn-icon btn-sm btn-success btn-circle mx-1 m-1" onclick="viewRecord('{{route('change_request_approval_load')}}','id={{$item->id}}&&type=start_stop','POST','opreation_modal_content')" href="javascript:void()" data-toggle="modal" data-target="#start_stop_change_modal"><i class="la la-plus-circle"></i></a>
                                                @endif
                                            @elseif($item->operation==1)
                                                <span class="m-badge m-badge--success m-badge--wide">{{ trans('global.approve') }}</span>
                                            @elseif($item->operation==2)
                                                <span class="m-badge m-badge--danger m-badge--wide">{{ trans('global.reject') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->attachment()->where('type',3)->first())
                                                <a class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1 btn-outline-primary" href="{{ route("DownloadAttachments", array(encrypt($item->attachment()->where('type',3)->first()->id),'change_request_attachment')) }}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1" onclick="viewRecord('{{route('start_stop_edit_load')}}','id={{$item->id}}&&project_id={{encrypt($request->project_id)}}&&project_location_id={{encrypt($request->project_location_id)}}','POST','edit_start_stop_content')" href="javascript:void()" data-toggle="modal" data-target="#start_stop_edit_modal"><i class="la la-edit"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        @endif
                    </table>
                    @if(!empty($request_changes))
                        {{ $request_changes->links('pagination') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    @if ($lang=="en")
    $(".datePicker").attr('type', 'date');
    @else
    $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif

    // Paginate cost scope list 
    $(document).ready(function()
    {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if($(this).attr('href') != '#') {
                // Get current URL route
                document.cookie = "no="+$(this).text();
                var dataString = '';
                counter = parseInt($(this).attr('id'));
                dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter+'&&project_id='+{{$request->project_id}}+'&&project_location_id='+{{$request->project_location_id}};
                $.ajax({
                    url:  '{{ route("list_start_stop") }}',
                    data: dataString,
                    type: 'get',
                    beforeSend: function(){
                        $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
                    },
                    success: function(response)
                    {
                        $('#searchresult').html(response);
                    }
                });
            }
        });
    });
</script>

  