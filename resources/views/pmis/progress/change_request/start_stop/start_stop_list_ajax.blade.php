 <table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
        <tr class="bg-light">
            <th width="5%">{{ trans('global.number') }}</th>
            <th width="15%">{{ trans('global.request_type') }}</th>
            <th width="10%">{{ trans('change_request.date_start') }}</th>
            <th width="10%">{{ trans('change_request.date_stop') }}</th>
            <th width="30%">{{ trans('change_request.description') }}</th>
            <th width="6%">{{ trans('global.status') }}</th>
            <th width="6%">{{ trans('global.attachment') }}</th>
            <th width="6%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    @if($request_changes->count()>0)
        @foreach($request_changes AS $item)
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                <tr class="category">
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        @if($item->start_stop=='0')
                            {{ trans('change_request.start') }}
                        @else
                            {{ trans('change_request.stop') }}
                        @endif
                    </td>
                    <td>{{ $item->start_date }}</td>
                    <td>{{ $item->end_date }}</td>
                    <td>{{ $item->remarks }}</td>
                    <td>
                        @if($item->operation=='0')
                            @if(doIHaveRoleInDep(session('current_department'),'pmis_change_request','change_req_approval'))
                                <a class="btn btn-icon btn-sm btn-success btn-circle mx-1 m-1" onclick="viewRecord('{{route('change_request_approval_load')}}','id={{$item->id}}&&type=start_stop','POST','opreation_modal_content')" href="javascript:void()" data-toggle="modal" data-target="#start_stop_change_modal"><i class="la la-plus-circle"></i></a>
                            @endif
                        @elseif($item->operation==1)
                            <span class="m-badge m-badge--success m-badge--wide">{{ trans('global.approve') }}</span>
                        @elseif($item->operation==2)
                            <span class="m-badge m-badge--danger m-badge--wide">{{ trans('global.reject') }}</span>
                        @endif
                    </td>
                    <td>
                        @if($item->attachment()->where('type',3)->first())
                            <a class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1 btn-outline-primary" href="{{ route("DownloadAttachments", array(encrypt($item->attachment()->where('type',3)->first()->id),'change_request_attachment')) }}">
                                <i class="fa fa-download"></i>
                            </a>
                        @endif
                    </td>
                    <td>
                        <a href="#" class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1" onclick="viewRecord('{{route('start_stop_edit_load')}}','id={{$item->id}}&&project_id={{encrypt($request->project_id)}}&&project_location_id={{encrypt($request->project_location_id)}}','POST','edit_start_stop_content')" href="javascript:void()" data-toggle="modal" data-target="#start_stop_edit_modal"><i class="la la-edit"></i></a>
                    </td>
                </tr>
            </tbody>
        @endforeach
    @endif
</table>
 @if(!empty($request_changes))
    {{ $request_changes->links('pagination') }}
@endif
<script type="text/javascript">
// Paginate cost scope list 
$(document).ready(function()
{
    $('.pagination a').on('click', function(event) {
        event.preventDefault();
        if($(this).attr('href') != '#') {
            // Get current URL route
            document.cookie = "no="+$(this).text();
            var dataString = '';
            counter = parseInt($(this).attr('id'));
            dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter+'&&project_id='+{{$request->project_id}}+'&&project_location_id='+{{$request->project_location_id}};
            $.ajax({
                url:  '{{ route("list_start_stop") }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                    $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
                },
                success: function(response)
                {
                    $('#searchresult').html(response);
                }
            });
        }
    });
});
</script>
