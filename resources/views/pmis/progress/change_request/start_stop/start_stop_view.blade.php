<div id="startStopViewModal_{{ $item->id }}" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="startStopViewModal_{{ $item->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon"><i class="la la-leanpub"></i></span>
                                <h3 class="m-portlet__head-text">{{trans('change_request.startstop_add')}}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="closeBtn" aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="startstopform">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-2">
                                    <label class=" ">{{ trans('change_request.startstop') }}</label>
                                </div>
                                <div class="col-lg-2">
                                    <span class=" m-switch m-switch--icon"><label><input type="checkbox" class="form-control m-input" onchange="OnStartStopChange(this.id)" name="startstop" id="startstop"><span></span></label></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom" id="enddatediv">
                                <div class="col-lg-8 row" >
                                    <div class="col-lg-3" >
                                        <label >{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input datePicker errorDiv" type="text" name="start_date_start" id="start_date_start">
                                        <div class="start_date_start error-div" style="display:none;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom" id="startdatediv" style="display: none" >
                                <div class="col-lg-6 row">
                                    <label>{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                                    <input class="form-control m-input datePicker errorDiv" type="text" name="start_date_startstop" id="start_date_startstop">
                                    <div class="start_date_startstop error-div" style="display:none;"></div>
                                </div>
                                <div class="col-lg-6">
                                    <label >{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                                    <input class="form-control m-input datePicker errorDiv" type="text" name="end_date_startstop" id="end_date_startstop">
                                    <div class="end_date_startstop error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12">
                                    <label>{{ trans('estimation.remarks') }}: <span style="color:red;">*</span></label>
                                    <textarea class="form-control m-input m-input--air" name="remarks" rows="1"></textarea>
                                    <div class="remarks error-div" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m-form__group_custom">
                                <div class="col-lg-12">
                                    <label >{{ trans('global.attachment') }}:</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file" id="file" onchange="chooseFile(this.id)">
                                        <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                                    </div>
                                    <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                                </div>
                            </div>
                            <input type="hidden" name="project_id" value="{{ $enc_id }}"/>
                            <input type="hidden" name="location_id" value="{{ $project_location_id }}"/>
                            <input type="hidden" name="attachment_type" value="3"/>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="form-group m-form__group row m-form__group_custom">
                                <button type="button" class="btn btn-primary btn-sm" onclick="addValueToHidden();storeRecord('{{route('store_change_start_stop')}}','startstopform','POST','response_div',redirectFunction,false,this.id);" id="add">{{trans('global.submit')}}</button>
                                <button type="reset" class="btn btn-secondary btn-sm" onclick="redirectFunction()">{{trans('global.cancel')}}</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
