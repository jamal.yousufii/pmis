<form enctype="multipart/form-data" id="start_stop_form">
    <div class="m-portlet__body m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-0 p-0">
        <div class="form-group m-form__group row m-form__group_custom py-2">
            @if($start_stop_change->start_stop==0)
                <div class="col-lg-12">
                    <label>{{ trans('change_request.date_start') }}: <span style="color:red;">*</span></label><br>
                    <input class="form-control m-input datePicker errorDiv" type="text" value="{{ $start_stop_change->start_date }}" name="start_date" id="start_date">
                    <div class="start_date error-div" style="display:none;"></div>
                    <!-- custom value for end date -->
                    <input type="hidden" name="end_date" id="end_date" value="1">
                </div>
            @else
                <div class="col-lg-6">
                    <label>{{ trans('change_request.date_start') }}: <span style="color:red;">*</span></label><br>
                    <input class="form-control m-input datePicker errorDiv" type="text" value="{{ $start_stop_change->start_date }}" name="start_date" id="start_date">
                    <div class="start_date error-div" style="display:none;"></div>
                </div>
                <div class="col-lg-6">
                    <label >{{ trans('change_request.date_stop') }}: <span style="color:red;">*</span></label><br>
                    <input class="form-control m-input datePicker errorDiv" type="text" value="{{ $start_stop_change->end_date }}" name="end_date" id="end_date">
                    <div class="end_date error-div" style="display:none;"></div>
                </div>
            @endif
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label>{{ trans('estimation.remarks') }}: <span style="color:red;">*</span></label>
                <textarea class="form-control m-input m-input--air" name="remarks" rows="2">{{ $start_stop_change->remarks }}</textarea>
                <div class="remarks error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label> {{ trans('global.attachment') }}: </label>
                @if($start_stop_change->attachment()->where('type',3)->first())
                    <div id="new_file_{{$start_stop_change->id}}" @if($start_stop_change->attachment()->where('type',3)->first()->path!=null) style="display:none;" @endif>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                    </div>
                    <div id="exist_file_{{$start_stop_change->id}}" @if($start_stop_change->attachment()->where('type',3)->first()->path==null) style="display:none;" @endif>
                        <div class="custom-file">
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($start_stop_change->id),'change_request_attachment')) }}">
                                <i class="fa fa-download"></i> {{ $start_stop_change->attachment()->where('type',3)->first()->filename }}.{{ $start_stop_change->attachment()->where('type',3)->first()->extension }}
                            </a><br>
                            <a href="javascript:void();" onclick="destroyUpdatedFile('{{ route("destroyAttachments", array(encrypt($start_stop_change->attachment()->where('type',3)->first()->id),'change_request_attachment')) }}','exist_file_{{$start_stop_change->id}}','new_file_{{$start_stop_change->id}}');"><i class="flaticon-delete"></i></a>
                            <span class="m-form__help">{{ trans('designs.new_file') }}</span>
                        </div>
                    </div>
                @else
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                        <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                    </div>
                    <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <!-- Hidden inputs -->
                <input type="hidden" name="project_id" value="{{ $request->project_id }}"/>
                <input type="hidden" name="location_id" value="{{ $request->project_location_id }}"/>
                <input type="hidden" name="attachment_type" value="3"/>
                <input type="hidden" name="record_id" value="{{ $start_stop_change->id }}"/>
                <button type="button" class="btn btn-primary btn-sm"  onclick="doEditRecordWithAttachments('{{route('edit_start_stop')}}','start_stop_form','POST',getChangeRequest);">{{trans('global.submit')}}</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </div>
</form>