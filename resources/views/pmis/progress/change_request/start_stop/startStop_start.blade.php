<div class="col-lg-12">
    <label >{{ trans('change_request.date_start') }}: <span style="color:red;">*</span></label><br>
    <input class="form-control m-input datePicker errorDiv" type="text" name="start_date" id="start_date">
    <div class="start_date error-div" style="display:none;"></div>
    <!-- custom value for end date -->
    <input type="hidden" name="end_date" id="end_date" value="1"> 
</div>
<script type="text/javascript">
    @if($lang=="en")
        $(".datePicker").attr('type', 'date');
    @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
    @endif
</script>