<form class="" enctype="multipart/form-data" id="operationmodalform">
    <div class="m-portlet__body m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-0 p-0">
        <div class="form-group m-form__group row m-form__group_custom py-0">
            <div class="col-lg-12">
                <label class="col-form-label">{{ trans('change_request.appro_reject') }}</label><br>
                <span class="m-switch m-switch--icon">
                    <label>
                        <input type="checkbox" name="operation" id="operation"><span></span>
                    </label>
                </span>
            </div>
        </div>

        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <label >{{ trans('estimation.remarks') }}: </label>
                <textarea class="form-control m-input m-input--air" name="operation_description" id="operation_description" rows="2"></textarea>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <input type="hidden" name="record_id" value="{{ $change_request->id }}"/>
                <input type="hidden" name="project_id" value="{{$change_request->project_id}}"/>
                <input type="hidden" name="location_id" value="{{$change_request->location_id}}"/>
                <input type="hidden" name="created_by" value="{{ $change_request->created_by }}"/>
                <input type="hidden" name="modal_id" value="start_stop_change_modal"/>
                <input type="hidden" name="table" value="change_request_start_stop"/>
                <input type="hidden" name="route" value="list_start_stop"/>
                <button type="button" onclick="storeData('{{route('change_request_approval')}}','operationmodalform','POST','response_div',getChangeRequest,false,'start_stop_change_modal');" class="btn btn-primary btn-sm">{{ trans('global.submit') }}</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </div>
</form>