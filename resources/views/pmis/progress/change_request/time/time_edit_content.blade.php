<form class="" enctype="multipart/form-data" id="timeEditForm" method="post">
    <div class="m-portlet__body m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-0 p-0">
        <div class="form-group m-form__group row m-form__group_custom m-2">
            <div class="col-lg-6" id="ModalForm">
                <label class="">{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                <input class="form-control m-input datePicker errorDiv" type="text" readonly value="{{ dateCheck($item->start_date,$lang) }}" name="start_date" id="start_date">
                <div class="start_date error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="">{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                <input class="form-control m-input datePicker errorDiv" type="text" value="{{ dateCheck($item->end_date,$lang) }}" name="end_date" id="end_date">
                <div class="end_date error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label >{{ trans('estimation.remarks') }}: <span style="color:red;">*</span></label>
                <textarea class="form-control m-input m-input--air errorDiv" name="remarks" id="remarks" rows="2">{{ $item->remarks }}</textarea>
                <div class="remarks error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label>{{ trans('global.attachment') }}: </label>
                @if($item->attachment()->where('type',2)->first())
                    <div id="new_file_{{$item->id}}" @if($item->attachment()->where('type',2)->first()->path!=null) style="display:none;" @endif>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                    </div>
                    <div id="exist_file_{{$item->id}}" @if($item->attachment()->where('type',2)->first()->path==null) style="display:none;" @endif>
                        <div class="custom-file">
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($item->id),'change_request_attachment')) }}">
                                <i class="fa fa-download"></i> {{ $item->attachment()->where('type',2)->first()->filename }}.{{ $item->attachment()->where('type',2)->first()->extension }}
                            </a><br>
                            <a href="javascript:void();" onclick="destroyUpdatedFile('{{ route("destroyAttachments", array(encrypt($item->attachment()->where('type',2)->first()->id),'change_request_attachment')) }}','exist_file_{{$item->id}}','new_file_{{$item->id}}');"><i class="flaticon-delete"></i></a>
                            <span class="m-form__help">{{ trans('designs.new_file') }}</span>
                        </div>
                    </div>
                @else
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                        <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                    </div>
                    <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <!-- Hidden Inputs -->
                <input type="hidden" name="project_id_edit" value="{{ $request->project_id }}"/>
                <input type="hidden" name="location_id_edit" value="{{ $request->project_location_id }}"/>
                <input type="hidden" name="attachment_type_edit" value="2"/>
                <input type="hidden" name="record_id" value="{{ $item->id }}"/>

                <button type="button" onclick="doEditRecordWithAttachments('{{route('edit_time')}}','timeEditForm','POST',getChangeRequest);" class="btn btn-primary btn-sm">{{ trans('global.submit') }}</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">{{ trans('global.cancel') }}</button>
            </div>
        </div>
    </div>
</form>