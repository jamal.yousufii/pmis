<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
        <tr class="bg-light">
            <th width="4%">{{ trans('global.number') }}</th>
            <th width="10%">{{ trans('change_request.start_date') }}</th>
            <th width="10%">{{ trans('change_request.end_date') }}</th>
            <th width="34%">{{ trans('estimation.description') }}</th>
            <th width="6%">{{ trans('global.status') }}</th>
            <th width="6%">{{ trans('global.attachment') }}</th>
            <th width="6%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    @if($request_changes->count()>0)
        @foreach($request_changes AS $item)
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ dateCheck($item->start_date,$lang)}}</td>
                    <td>{{ dateCheck($item->end_date,$lang)}}</td>
                    <td>{{ $item->remarks }}</td>
                    <td>
                        @if($item->operation=='0')
                            @if(doIHaveRoleInDep(session('current_department'),'pmis_change_request','change_req_approval'))
                                <a href="#" class="btn btn-icon btn-sm btn-success btn-circle mx-1 m-1" onclick="viewRecord('{{route('change_request_approval_load')}}','id={{$item->id}}&&type=time','POST','opreation_modal_content')" href="javascript:void()" data-toggle="modal" data-target="#time_change_request"><i class="la la-plus-circle"></i></a>
                            @endif
                        @elseif($item->operation==1)
                            <span class="m-badge m-badge--success m-badge--wide">{{ trans('global.approve') }}</span>
                        @elseif($item->operation==2)
                            <span class="m-badge m-badge--danger m-badge--wide">{{ trans('global.reject') }}</span>
                        @endif
                    </td>
                    <td>
                        @if($item->attachment()->where('type',2)->first())
                            <a class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1 btn-outline-primary" href="{{ route("DownloadAttachments", array(encrypt($item->attachment()->where('type',2)->first()->id),'change_request_attachment')) }}">
                                <i class="fa fa-download"></i>
                            </a>
                        @endif
                    </td>
                    <td>
                        @if($item->operation!='1') 
                        <a class="btn btn-icon btn-sm btn-info btn-circle mx-1 m-1" onclick="viewRecord('{{route('edit_time_load')}}','id={{$item->id}}&&project_id={{encrypt($request->project_id)}}&&project_location_id={{encrypt($request->project_location_id)}}','POST','time_edit_content_modal')" href="javascript:void()" data-toggle="modal" data-target="#timeEditModal"><i class="la la-edit"></i></a>
                        @endif
                    </td>
                </tr>
            </tbody>
        @endforeach
    @endif
</table>
@if(!empty($request_changes))
    {{ $request_changes->links('pagination') }}
@endif
<script>
       // Paginate cost scope list 
        $(document).ready(function()
        {
            $('.pagination a').on('click', function(event) {
                event.preventDefault();
                if($(this).attr('href') != '#') {
                    // Get current URL route
                    document.cookie = "no="+$(this).text();
                    var dataString = '';
                    counter = parseInt($(this).attr('id'));
                    dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&counter="+counter+'&&project_id='+{{$request->project_id}}+'&&project_location_id='+{{$request->project_location_id}};
                    $.ajax({
                        url:  '{{ route("list_time") }}',
                        data: dataString,
                        type: 'get',
                        beforeSend: function(){
                            $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
                        },
                        success: function(response)
                        {
                            $('#searchresult').html(response);
                        }
                    });
                }
            });
        });
</script>