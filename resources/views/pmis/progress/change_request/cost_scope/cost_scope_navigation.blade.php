<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand m-0" style="justify-content: center;">
    <li class="nav-item m-tabs__item" style="width: 30.33%">
        <a class="nav-link m-tabs__link <?=session('cost_scope_tabe')=='bq' ? 'active': ''?>" onclick="serverRequest('{{route('list_cost_scope')}}','project_id={{$request->project_id}}&&dep_id={{$request->dep_id}}&&project_location_id={{$request->project_location_id}}','GET','list_content',false);" style="text-align:center"><strong>{{ trans('change_request.scope') }}</strong></a>
    </li>
    <li class="nav-item m-tabs__item " style="width: 30.33%">
        <a class="nav-link m-tabs__link <?=session('cost_scope_tabe')=='time' ? 'active': ''?>" onclick="serverRequest('{{route('list_time')}}','project_id={{$request->project_id}}&&dep_id={{$request->dep_id}}&&project_location_id={{$request->project_location_id}}','GET','list_content',false);"  style="text-align:center"><strong>{{ trans('change_request.time') }}</strong></a>
    </li>
    <li class="nav-item m-tabs__item" style="width: 30.33%">
        <a class="nav-link m-tabs__link <?=session('cost_scope_tabe')=='start_stop' ? 'active': ''?>" onclick="serverRequest('{{route('list_start_stop')}}','project_id={{$request->project_id}}&&dep_id={{$request->dep_id}}&&project_location_id={{$request->project_location_id}}','GET','list_content',false);" style="text-align:center"><strong>{{ trans('change_request.startstop') }}</strong></a>
    </li>
</ul>