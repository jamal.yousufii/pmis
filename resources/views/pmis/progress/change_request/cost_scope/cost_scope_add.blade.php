<div id="costScopeModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="costScopeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class=" m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                <!--begin::head-->
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('change_request.scope_add') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="costScopeForm" method="post">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row m-form__group_custom py-0">
                        <div class="col-lg-12">
                            <label class="col-form-label">{{ trans('change_request.newaalready') }}</label><br>
                            <span class="m-switch m-switch--icon">
                                <label>
                                    <input type="checkbox" onchange="display_content(this.id,'{{$request->project_id}}','{{$request->project_location_id}}','{{ route('getCostScopeContent')}}','GET','newBqContent','cost_scope_newBQ','cost_scope_existBQ')" name="newbill[]" id="newbill"><span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div id="newBqContent">
                        <div class="form-group m-form__group row m-form__group_custom py-2">
                            <div class="col-lg-12">
                                <label class="col-form-label">{{ trans('change_request.billofquantities') }}: <span style="color:red;">*</span></label>
                                <div id="operation_type" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2" name="operation_type" id="operation_type_id" onchange="getBillOfQuantities(this.id,'{{route('getBillOfQuantities')}}','billqdetails')" style="width:100%">
                                        <option value="">{{ trans('global.select') }}</option>
                                        @if($billQuantity)
                                            @foreach($billQuantity as $item)
                                                <option value="{!!$item->id!!}">{!!$item->operation_type!!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="operation_type error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom py-2" id="billqdetails">
                            <div class="col-lg-3">
                                <label class="">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
                                <div id="unit_id" class="errorDiv">
                                    <select class="form-control m-input m-input--air select-2 errorDiv" name="unit_id" style="widht:100%">
                                        <option value="">{{ trans('global.select') }}</option>
                                    </select>
                                </div>
                                <div class="unit_id error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')" >
                                <div class="amount error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">{{ trans('estimation.price') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input errorDiv" value="" type="number" min="0" name="price" id="price" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')">
                                <div class="price error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">{{ trans('estimation.total_price') }}: </label>
                                <input class="form-control m-input" value="" type="number" min="0" readonly name="total_price" id="total_price" >
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom py-2">
                            <div class="col-lg-12">
                                <label class="">{{ trans('estimation.description') }}: <span style="color:red;">*</span></label>
                                <textarea class="form-control m-input m-input--air errorDiv" name="remarks" id="remarks" rows="2"></textarea>
                                <div class="remarks error-div" style="display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom py-2">
                            <div class="col-lg-12">
                                <label class="">{{ trans('global.attachment') }}:</label>
                                <div class="custom-file p-0">
                                    <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                                    <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                                </div>
                                <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-form__group_custom">
                        <div class="col-lg-12">
                            <!-- Hidden inputs -->
                            <input type="hidden" name="project_id" value="{{ encrypt($request->project_id) }}"/>
                            <input type="hidden" name="location_id" value="{{ $request->project_location_id }}"/>
                            <input type="hidden" name="project_name" value="{{$plan->name}}"/>
                            <input type="hidden" name="attachment_type" value="1"/>
                            <button type="button" onclick="storeData('{{route('store_cost_scope')}}','costScopeForm','POST','response_div',getChangeRequest,false,'costScopeModal');" class="btn btn-primary btn-sm">{{ trans('global.submit') }}</button>
                            <button type="button"data-dismiss="modal" class="btn btn-secondary btn-sm">{{ trans('global.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>

