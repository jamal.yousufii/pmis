<div class="col-lg-3">
    <label class="">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
    <select class="form-control m-input m-input--air select-2" name="unit_id">
        <option value="">{{ trans('global.select') }}</option>
        @if($units)
            @foreach ($units as $unit)
                @if($unit->id==$records->unit_id)
                    <option value="{{$unit->id}}" selected >{{ $unit->name }}</option>
                @endif
            @endforeach
        @endif
    </select>
    <div class="unit_id error-div" style="display:none;"></div>
</div>
<div class="col-lg-3">
    <label class="">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
    <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')">
    <div class="amount error-div" style="display:none;"></div>
</div>
<div class="col-lg-3">
    <label class="">{{ trans('estimation.price') }}: <span style="color:red;">*</span></label>
    <input class="form-control m-input errorDiv" value="{{ $records->price }}" type="number" min="0" name="price" id="price" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')">
    <div class="price error-div" style="display:none;"></div>
</div>
<div class="col-lg-3">
    <label class="">{{ trans('estimation.total_price') }}: </label>
    <input class="form-control m-input" type="number" min="0" readonly name="total_price" id="total_price" >
    <div class="total_price error-div" style="display:none;"></div>
</div>
<script type="text/javascript">
    $(".select-2").select2();
</script>
