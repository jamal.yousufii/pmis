<div id="cost_scope_edit_modal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="cost_scope_edit_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('change_request.scope_edit') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                 <div id="cost_scope_edit_content_modal">
                 </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
</div>
