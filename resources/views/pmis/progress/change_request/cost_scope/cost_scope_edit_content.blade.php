<form class="" enctype="multipart/form-data" id="scope_edit_form">
    <div class="m-portlet__body m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-0 p-0">
        <div class="form-group m-form__group row m-form__group_custom my-0">
            <div class="col-lg-12">
                @if($scop_change_request->new_old_bill==1)
                    <div>
                        <label class="col-form-label">{{ trans('estimation.operation_type') }}: </label>
                        <textarea class="form-control m-input m-input--air" name="operation_type" id="operation_type" rows="1" onkeypress="getKey(event);">{{ $scop_change_request->operation_type }}</textarea>
                    </div>
                @else
                    <div id="billOfQuantityedit">
                        <label class="  col-form-label">{{ trans('change_request.billofquantities') }} :</label>
                        <select class="form-control m-input m-input--air select-2 errorDiv" id="operation_type" name="operation_type" onchange="getBillOfQuantities(this.id,'{{route('getBillOfQuantities')}}','billqdetails')" style="width: 100%">
                            <option value="">{{ trans('global.select') }}</option>
                            @if($bill_quantities)
                                @foreach($bill_quantities as $record)
                                        <option value="{{ $scop_change_request->bill_quantity_id }}" <?=$scop_change_request->bill_quantity_id==$record->id ? 'selected' : ''?> >{{ $record->operation_type }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2" id="billqdetailsedit">
            <div class="col-lg-3">
                <label >{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
                <select class="form-control m-input m-input--air select-2 errorDiv" style="width: 100%" name="unit_id">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($units)
                        @foreach($units as $unit)
                            @if($unit->id==$scop_change_request->unit_id)
                                <option value="{{$unit->id}}" selected >{{ $unit->name }}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
                <div class="unit_id error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
                <label >{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="number" min="1" name="amount" id="amount_{{ $scop_change_request->id }}" value="{{ $scop_change_request->amount }}" onchange="getTotalBQ('amount_{{ $scop_change_request->id }}','price_{{ $scop_change_request->id }}','total_price_{{ $scop_change_request->id }}')" onkeyup="getTotalBQ('amount_{{ $scop_change_request->id }}','price_{{ $scop_change_request->id }}','total_price_{{ $scop_change_request->id }}')" >
                <div class="amount error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
                <label >{{ trans('estimation.price') }}: <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="number" min="1" name="price" <?=$scop_change_request->new_old_bill==0? 'readonly' : ''?> id="price_{{ $scop_change_request->id }}" value="{{ $scop_change_request->price }}" onchange="getTotalBQ('amount_{{ $scop_change_request->id }}','price_{{ $scop_change_request->id }}','total_price_{{ $scop_change_request->id }}')" onkeyup="getTotalBQ('amount_{{ $scop_change_request->id }}','price_{{ $scop_change_request->id }}','total_price_{{ $scop_change_request->id }}')">
                <div class="price error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-3">
                <label >{{ trans('estimation.total_price') }}: </label>
                <input class="form-control m-input" type="number" min="1" name="total_price" readonly id="total_price_{{ $scop_change_request->id }}" value="{{ $scop_change_request->total_price }}" >
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label >{{ trans('estimation.remarks') }}: <span style="color:red;">*</span></label>
                <textarea class="form-control m-input m-input--air errorDiv" name="remarks" rows="2">{{ $scop_change_request->remarks }}</textarea>
                <div class="remarks error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom py-2">
            <div class="col-lg-12">
                <label class="title-custom"> {{ trans('global.attachment') }}: </label>
                @if($scop_change_request->attachment()->where('type',1)->first())
                    <div id="new_file_{{$scop_change_request->id}}" @if($scop_change_request->attachment()->where('type',1)->first()->path!=null) style="display:none;" @endif>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                    </div>
                    <div id="exist_file_{{$scop_change_request->id}}" @if($scop_change_request->attachment()->where('type',1)->first()->path==null) style="display:none;" @endif>
                        <div class="custom-file">
                            <a class="text-decoration-none" href="{{ route("DownloadAttachments", array(encrypt($scop_change_request->id),'change_request_attachment')) }}">
                                <i class="fa fa-download"></i> {{ $scop_change_request->attachment()->where('type',1)->first()->filename }}.{{ $scop_change_request->attachment()->where('type',1)->first()->extension }}
                            </a><br>
                            <a href="javascript:void();" onclick="destroyUpdatedFile('{{ route("destroyAttachments", array(encrypt($scop_change_request->attachment()->where('type',1)->first()->id),'change_request_attachment')) }}','exist_file_{{$scop_change_request->id}}','new_file_{{$scop_change_request->id}}');"><i class="flaticon-delete"></i></a>
                            <span class="m-form__help">{{ trans('designs.new_file') }}</span>
                        </div>
                    </div>
                @else
                    <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
                            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
                        </div>
                        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
                    </div>
                @endif
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                    <input type="hidden" name="attachment_type" value="1"/>
                    <input type="hidden" name="record_id" value="{{ $scop_change_request->id }}"/>
                    <button type="button" onclick="doEditRecordWithAttachments('{{route('edit_cost_scope')}}','scope_edit_form','POST',getChangeRequest);" class="btn btn-primary btn-sm">{{ trans('global.submit') }}</button>
                    <button type="button"data-dismiss="modal" class="btn btn-secondary btn-sm">{{ trans('global.cancel') }}</button>
                </div>
            </div>
        </div>
        
    </div>
</form>