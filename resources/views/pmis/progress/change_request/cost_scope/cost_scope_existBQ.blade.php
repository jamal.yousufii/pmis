<div class="form-group m-form__group row m-form__group_custom py-2">    
    <div class="col-lg-12">
        <label class="col-form-label">{{ trans('change_request.billofquantities') }} :</label>
        <select class="form-control m-input m-input--air select-2" id="operation_type" name="operation_type" onchange="getBillOfQuantities(this.id,'{{route('getBillOfQuantities')}}','billqdetails')" style="width:100%">
            <option value="">{{ trans('global.select') }}</option>
            @if($records)
                @foreach ($records as $item)
                    <option value="{!!$item->id!!}">{!!$item->operation_type!!}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group m-form__group row m-form__group_custom py-2" id="billqdetails">
    <div class="col-lg-3">
        <label class="">{{ trans('estimation.unit') }}: <span style="color:red;">*</span></label>
        <div id="unit_id" class="errorDiv">
            <select class="form-control m-input m-input--air select-2" name="unit_id" style="width:100%">
                <option value="">{{ trans('global.select') }}</option>
            </select>
        </div>
        <div class="unit_id error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <label class="">{{ trans('estimation.amount') }}: <span style="color:red;">*</span></label>
        <input class="form-control m-input errorDiv" type="number" min="0" name="amount" id="amount" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')">
        <div class="amount error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <label class="">{{ trans('estimation.price') }}: <span style="color:red;">*</span></label>
        <input class="form-control m-input errorDiv" type="number" min="0" name="price" id="price" onchange="getTotalBQ('amount','price','total_price')" onkeyup="getTotalBQ('amount','price','total_price')">
        <div class="price error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-3">
        <label class="">{{ trans('estimation.total_price') }}: </label>
        <input class="form-control m-input" type="number" min="0" readonly name="total_price" id="total_price" >
    </div>
</div>
<div class="form-group m-form__group row m-form__group_custom py-2">
    <div class="col-lg-12">
        <label class="">{{ trans('estimation.description') }}: <span style="color:red;">*</span></label>
        <textarea class="form-control m-input m-input--air errorDiv" name="remarks" id="remarks" rows="2"></textarea>
        <div class="remarks error-div" style="display:none;"></div>
    </div>
</div>
<div class="form-group m-form__group row m-form__group_custom py-2">
    <div class="col-lg-12">
        <label class="">{{ trans('global.attachment') }}:</label>
        <div class="custom-file p-0">
            <input type="file" class="custom-file-input" name="file[]" id="file" onchange="chooseFile(this.id)">
            <label class="custom-file-label" id="file-label" for="file">{{ trans('requests.att_choose') }}</label>
        </div>
        <span class="m-form__help">{{ trans('global.file_extention') }}</span>
    </div>
        
</div>
<script type="text/javascript">
$('.select-2').select2();
</script>