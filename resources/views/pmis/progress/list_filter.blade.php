<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead class="bg-light">
        <tr>
            <th width="12%">{{ trans('global.pro_urn') }}</th>
            <th width="20%">{{ trans('plans.project_name') }}</th>
            <th width="10%">{{ trans('plans.project_type') }}</th>
            <th width="14%">{{ trans('contractor.company') }}</th>
            <th width="13%">{{ trans('procurement.contract_code') }}</th>
            <th width="10%">{{ trans('procurement.contract_price') }}</th>
            <th width="10%">{{ trans('progress.contraction_duration') }}</th>
            <th width="5%">{{ trans('global.project_priority') }}</th>
            <th width="6%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @if($records)
            @foreach($records AS $rec)
                <?php $enc_id = encrypt($rec->id); ?>
                <tr>
                    <td>{{ $rec->urn }}</td>
                    <td>{{ $rec->name }}</td>
                    <td>{{ $rec->project_type->{'name_'.$lang} }}</td>
                    <td>{{ $rec->procurement->company }}</td>
                    <td>{{ $rec->procurement->contract_code }}</td>
                    <td>{{ number_format($rec->procurement->contract_price) }}</td>
                    <td>{{ differenceBetweenDate($rec->procurement->start_date,$rec->procurement->end_date,'%a',false) }}</td>
                    <td class="text-center">
                        @php
                            $class = 'btn-default';
                            if($rec->priority=='1'){
                                $class = 'btn-danger';
                            }elseif ($rec->priority=='2') {
                                $class = 'btn-success';
                            }elseif ($rec->priority=='3') {
                                $class = 'btn-primary';
                            }
                        @endphp
                        @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_priority"))
                            <button class="btn {{$class}} btn-icon btn-sm btn-circle mx-2" onclick="viewRecord('{{route('progress.priority_add',$enc_id)}}','','GET','content')" data-toggle="modal" data-target="#assign_priority" id="add_owner_btn">
                                <span><i class="fas fa-exchange-alt" style="transform: rotate(90deg);"></i> <span></span></span>
                            </button>
                        @endif
                    </td>
                    <td>
                        <span class="dtr-data">
                            <span class="dropdown">
                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_view"))
                                        <a class="dropdown-item" href="{{ route('calendars',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                    @endif
                                </div>
                            </span>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function() {
  	$('.pagination a').on('click', function(event) {
  		event.preventDefault();
  		if ($(this).attr('href') != '#') {
        document.cookie = "no="+$(this).text();
  			var dataString = '';
  			dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&department_id="+'{!!$request->department_id!!}'+"&section="+'{!!$request->section!!}'+"&field="+'{!!$request->field!!}'+"&condition="+'{!! $request->condition!!}'+"&value="+'{!! $request->value !!}';
        serverRequest('{{ route("filter_progress") }}',dataString,'POST','searchresult',true);
  		}
  	});
  });
</script>
