@extends('master')
@section('head')
  <title>{{ trans('progress.progress') }}</title>
@endsection
@section('content')
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          {{-- <h3 class="m-portlet__head-text">{{ trans('progress.list') }}</h3> --}}
          @include('breadcrumb_nav')
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <div class="m-input-icon m-input-icon--left">
              <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" id="collapsBtn" data-toggle="collapse" href="#collapseDiv" role="button" aria-expanded="true" aria-controls="collapseDiv">
                <span><i class="la la-arrows-v"></i><span>{{ trans('global.search') }}</span></span>
              </a>
            </div>
          </li>
          <li class="m-portlet__nav-item">
            <a href="{{ route('bringSections',encrypt($dep_id)) }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
              <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Filter Start -->
    <div class="code notranslate cssHigh collapse" id="collapseDiv" style="border-bottom:1px solid #ebedf2">
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="search">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12">
              <div id="div_field" class="errorDiv"> <!-- Error Dive start for select box  -->
                <select class="form-control m-input m-input--air select-2 required" name="field" id="field" style="width:100%;" onchange="bringSearchFiel('{{route('search.option')}}','POST',this.value,'value_input')">
                  <option value="">{{ trans('global.select') }}</option>
                  <option value="projects.urn"> {{trans('global.pro_urn') }} </option>
                  <option value="projects.name"> {{trans('plans.project_name') }} </option>
                  <option value="pro.contract_code"> {{trans('procurement.contract_code') }} </option>
                </select>
              </div>
              <div class="field error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <select class="form-control m-input m-input--air select-2" name="condition" id="condition" style="width:100%;">
                <option value="=">{{ trans('global.equail') }}</option>
                <option value="like">{{ trans('global.like') }} </option>
              </select>
            </div>
            <div class="col-lg-4 col-xs-4 col-md-4 col-sm-12" id="value_input">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" name="value" class="form-control m-input required" placeholder="{{trans('global.search_dote')}}" id="search_value">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span><i class="la la-search"></i></span>
                </span>
                <div class="search_value error-div" style="display:none;"></div>
              </div>
            </div>
            {{-- Hidden Input  --}}
            <input type="hidden" name="department_id" value="{{encrypt($dep_id)}}">
            <input type="hidden" name="section" value="{{session('current_section')}}">
            <div class="col-lg-2 col-xs-2 col-md-2 col-sm-12">
              <a href="javascript:void;" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air" onclick="searchRecords('{{route('filter_progress')}}','search','POST','searchresult')">
                <span>
                  <i class="la la-search"></i><span>{{trans('global.search')}}</span>
                </span>
              </a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- Filter End -->
    <!-- Assign project priority: Start -->
    <div id="assign_priority" class="modal fade bd-example-modal-md" role="dialog" aria-labelledby="assign_priority" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="m-nav__link m-nav__link--icon"><i class="fas fa-exchange-alt"></i></span>
                    <button type="button" class="close p-4" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ki ki-close"></i></button>
                </div>
                <div class="modal-body" id="content"></div>
            </div>
        </div>
    </div>
    @alert()
    @endalert
    <!-- Assign project priority: End -->
    <div class="m-portlet__body table-responsive" id="searchresult">
        <table class="table table-striped- table-bordered table-hover table-checkable">
            <thead class="bg-light">
                <tr>
                    <th width="12%">{{ trans('global.pro_urn') }}</th>
                    <th width="20%">{{ trans('plans.project_name') }}</th>
                    <th width="10%">{{ trans('plans.project_type') }}</th>
                    <th width="14%">{{ trans('contractor.company') }}</th>
                    <th width="13%">{{ trans('procurement.contract_code') }}</th>
                    <th width="10%">{{ trans('procurement.contract_price') }}</th>
                    <th width="10%">{{ trans('progress.contraction_duration') }}</th>
                    <th width="5%">{{ trans('global.project_priority') }}</th>
                    <th width="6%">{{ trans('global.action') }}</th>
                </tr>
            </thead>
            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                @if($records)
                    @foreach($records AS $rec)
                        <?php $enc_id = encrypt($rec->id); ?>
                        <tr>
                            <td>{{ $rec->urn }}</td>
                            <td>{{ $rec->name }}</td>
                            <td>{{ $rec->project_type->{'name_'.$lang} }}</td>
                            <td>{{ $rec->procurement->company }}</td>
                            <td>{{ $rec->procurement->contract_code }}</td>
                            <td>{{ number_format($rec->procurement->contract_price) }}</td>
                            <td>{{ differenceBetweenDate($rec->procurement->start_date,$rec->procurement->end_date,'%a',false) }}</td>
                            <td class="text-center">
                                @php
                                    $class = 'btn-default';
                                    if($rec->priority=='1'){
                                        $class = 'btn-danger';
                                    }elseif ($rec->priority=='2') {
                                        $class = 'btn-success';
                                    }elseif ($rec->priority=='3') {
                                        $class = 'btn-primary';
                                    }
                                @endphp
                                @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_priority"))
                                    <button class="btn {{$class}} btn-icon btn-sm btn-circle mx-2" onclick="viewRecord('{{route('progress.priority_add',$enc_id)}}','','GET','content')" data-toggle="modal" data-target="#assign_priority" id="add_owner_btn">
                                        <span><i class="fas fa-exchange-alt" style="transform: rotate(90deg);"></i> <span></span></span>
                                    </button>
                                @endif
                            </td>
                            <td>
                                <span class="dtr-data">
                                    <span class="dropdown">
                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_view"))
                                                <a class="dropdown-item" href="{{ route('calendars',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}"><i class="la la-eye"></i>{{ trans('global.view') }}</a>
                                            @endif
                                        </div>
                                    </span>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
      <!-- Pagination -->
      @if(!empty($records))
        {!!$records->links('pagination')!!}
      @endif
    </div>
  </div>
@endsection
