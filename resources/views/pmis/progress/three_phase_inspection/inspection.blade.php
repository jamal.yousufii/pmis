@extends('master_window')
@section('head')
  <title>{{ trans('progress.progress') }}</title>
@endsection
@section('content')  
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__body">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">{{ $plan->name }}</h3>
            </div> 
        </div>
      </div>
      <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
        <!--begin::Item-->
        <div class="m-accordion__item m-accordion__item--dark">
          <div class="m-accordion__item-head collapsed " srole="tab" id="inspection" data-toggle="collapse" href="#inspection_add" aria-expanded="false">
            <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('inspection.add_sub_inspection') }}</h4></span>
            <span class="m-accordion__item-mode"></span>
          </div>
          <div class="m-accordion__item-body collapse" id="inspection_add" role="tabpanel" aria-labelledby="inspection" data-parent="#m_accordion_5" style="">
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="inspectionForm" enctype="multipart/form-data">    
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-xl-4 col-lg-4">  
                    <label class="title-custom">{{ trans('inspection.list_main') }}: <span style="color:red;">*</span></label>
                    <div id="main_insepction" class="errorDiv">
                      <select class="form-control m-input m-input--air select-2" name="main_insepction" id="req_main_insepction" onchange="bringSubInspection('req_main_insepction','req_sub_inspection',1)">
                          <option value="">{{ trans('global.select') }}</option>
                          @if($result)
                            @foreach($result as $item)
                              <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                            @endforeach
                          @endif
                      </select>
                    </div>
                    <div class="main_insepction error-div" style="display:none;"></div>
                </div>
                <div class="col-xl-4 col-lg-4">  
                    <label class="title-custom">{{ trans('inspection.list_sub') }}: <span style="color:red;">*</span></label>
                    <div id="sub_inspection" class="errorDiv">
                      <select class="form-control m-input m-input--air select-2" name="sub_inspection" id="req_sub_inspection">
                          <option value="">{{ trans('global.select') }}</option>
                      </select>
                    </div>
                    <div class="sub_inspection error-div" style="display:none;"></div>
                </div>
                <div class="col-xl-4 col-lg-4">
                  <label class="title-custom">{{ trans('plans.status') }}: <span style="color:red;">*</span></label>
                  <div id="status" class="errorDiv">
                      <select class="form-control m-input m-input--air select-2" name="status" id="req_status">
                        <option value="">{{ trans('global.select') }}</option>
                        @if($status)
                          @foreach($status as $item)
                            <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
                          @endforeach
                        @endif
                      </select>
                  </div>
                  <div class="status error-div" style="display:none;"></div>
                </div>     
              </div>
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-xl-12 col-lg-12">
                  <label class="title-custom">{{ trans('requests.description') }}: <span style="color:red;">*</span></label>
                  <textarea class="form-control m-input m-input--air errorDiv" name="description" id="description" rows="3"></textarea>
                  <div class="description error-div" style="display:none;"></div>
                </div>
              </div>
              <div class="form-group m-form__group row m-form__group_custom">
                <div class="col-lg-12">
                  <input name="bill_quant_id" type="hidden" value="{{ decrypt($bq_id) }}"/>
                  <input name="project_id" type="hidden" value="{{ $pro_id }}"/>
                  <input name="location_id" type="hidden" value="{{ $locId }}"/>
                  <button type="button" onclick="storeRecord('{{route('three_phase_Inspection.store')}}','inspectionForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!--end::Item-->
        <div class="m-portlet m-portlet--full-height">
          <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item m-accordion__item--dark">
              <div class="m-accordion__item-head collapse show" srole="tab" id="inspection_tree" data-toggle="collapse" href="#inspection_tree" aria-expanded="true">
                  <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('inspection.list_categories') }}</h4></span>
                  <span class="m-accordion__item-mode"></span>
              </div>
              <div class="m-accordion__item-body collapse show" id="inspection_tree" role="tabpanel" aria-labelledby="inspection_tree" data-parent="#m_accordion_5" style="">
                <div class="m-accordion__item-content">
                  <!--begin: Portlet Body-->
                  <div class="form-group m-form__group row m-form__group_custom">  
                    <div class="col-lg-12" id="inspection_tree_div">
                      <ul>
                        @if($inspection)
                          @foreach($inspection as $res)
                            <li>{{ $res->{'name_'.$lang} }}
                              @if($res->inspectionSub)
                                <ul>
                                  @foreach($res->inspectionSub as $inspectionSub)
                                    <li id="{{ $inspectionSub->id }}">{{ $inspectionSub->{'name_'.$lang} }}</li>                                    
                                  @endforeach    
                                </ul>
                              @endif
                            </li>
                          @endforeach
                        @endif
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-accordion__item-body collapse show" id="inspection_tree" role="tabpanel" aria-labelledby="inspection_tree" data-parent="#m_accordion_5" style="">
                <div class="m-accordion__item-content">
                  <!--begin: Portlet Body-->
                  <div class="col-lg-12" id="details_div"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
@endsection
@section('js-code')
  <script type="text/javascript">
    $(function () {
      $('#inspection_tree_div').jstree();
      $('#inspection_tree_div').on("changed.jstree", function (e, data) {
        var nodeId = $('#inspection_tree_div').jstree().get_selected("id")[0].id;
          $.ajax({
            url: '{{ route("getInspectionDetails") }}',
            data: {
                "_method" : 'GET',
                "id"      : nodeId,
                "bq_id"   : "{{ decrypt($bq_id) }}",
            },
            type: 'GET',
            success: function(response)
            {
              $("#details_div").html(response);
            }
          });
      });
    });
  </script>
@endsection
 
