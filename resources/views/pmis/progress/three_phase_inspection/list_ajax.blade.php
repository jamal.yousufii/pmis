<table class="table table-bordered table-hover table-checkable">
    <thead>
        <tr class="bg-light">
            <th width="40%">{{ trans('estimation.operation_type') }}</th>
            <th width="10%">{{ trans('estimation.unit') }}</th>
            <th width="10%">{{ trans('estimation.amount') }}</th>
            <th width="10%">{{ trans('estimation.total_price') }}</th>
            <th width="15%">{{ trans('estimation.percentage') }}</th>
            <th width="10%">{{ trans('inspection.total_inspection') }}</th>
            <th width="5%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
        @php
            $bq_section = 0;
        @endphp
        @foreach($records as $item)
            @if($bq_section != $item->bq_section_id)
                @php
                    $bq_section = $item->bq_section_id;
                @endphp
                <tr style="background: #5b99bf4a">
                    <td colspan="10" class="text-center title-custom">
                        {{ $item->bq_section->{'name_'.$lang} }}
                    </td>
                <tr>
            @endif
            <tr>
                <td>{{ limit_text($item->operation_type) }}</td>
                <td>@if($item->unit) {{ $item->unit->name_dr }} @endif</td>
                <td>{{ $item->amount }}</td>
                <td>{{ $item->total_price }}</td>
                <td>{{ getPercentage($plan->procurement['contract_price'],$item->total_price) }} %</td>
                <td>{{ $item->inspection->count() }}</td>
                <td>
                    <span class="dtr-data">
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"  aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px;">
                                <span class="dropdown-item btn btn-secondary btn-dark" onclick="openWindow('{{ route('three_phase_Inspection/show',[$enc_id,encrypt($item->id),encrypt($project_location_id)] )}} ')"><i class="la la-eye"></i>{{ trans('global.inspect_bill_of_quantity') }}</span>
                                </div>
                        </span>
                    </span>
                </td>
            </tr>      
        @endforeach
    </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
    {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>