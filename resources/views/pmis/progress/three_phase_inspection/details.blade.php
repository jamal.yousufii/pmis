@if(count($details)>0)
    <div class="m-portlet m-portlet--mobile table-responsive">
        <table class="table table-bordered overflow-auto ">
            <thead>
                <tr class="bg-dark text-white">
                    <th scope="col" width="15%">{{ trans('global.number') }}</th>
                    <th scope="col" width="25%">{{ trans('global.status') }}</th>
                    <th scope="col" width="60%">{{ trans('requests.description') }}</th>
                </tr>
            </thead>
            <tbody>
                @if(count($details)>0)
                    @foreach($details as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ getStaticNameByID("static_data",$item->status,$lang) }}</td>
                            <td class="wrap">{{ $item->description }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endif