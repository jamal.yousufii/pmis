<div id="sub_div" class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('inspection.add_new_inspection') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m-form__group_custom">  
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('inspection.inspection_name_dr') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_dr" id="name_dr">
              <div class="name_dr error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('inspection.inspection_name_pa') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_pa" id="name_pa">
              <div class="name_pa error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-12">
              <label class="title-custom">{{ trans('inspection.inspection_name_en') }} : <span style="color:red;">*</span></label>
              <input class="form-control m-input errorDiv" type="text" name="name_en" id="name_en">
              <div class="name_en error-div" style="display:none;"></div>
            </div>
          </div>  
          <input class="form-control m-input errorDiv" type="hidden" name="enc_id" id="enc_id" value="{!! $enc_id !!}">
          <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
              <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                  <button type="button" onclick="storeRecord('{{route('storeSubInspection',$enc_id)}}','requestForm','POST','response_div',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                  <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        @csrf
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".select-2").select2();
</script>
