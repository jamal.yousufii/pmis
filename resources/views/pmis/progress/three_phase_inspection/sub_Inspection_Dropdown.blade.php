
<option value="">{{ trans('global.select') }}</option>
@if($result)
    @foreach($result as $item)
        <option value="{{$item->id}}">{{ $item->{'name_'.$lang} }}</option>
    @endforeach
@endif
<script type="text/javascript">
    $(".select-2").select2();
</script>
