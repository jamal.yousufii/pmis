{{-- Start of the Modal --}}
<div id="summarySchedule_{{ $item->id }}" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="summarySchedule_{{ $item->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
                <!--begin::head-->
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('schedule.versions') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Head-->
                <!--begin::content-->
                <div class="m-form m-form--label-align-right p-2" enctype="multipart/form-data" id="publicHolidaysForm">
                    <div class="form-group m-form__group row m-form__group_custom">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="bg-light">
                                    <th width="6%">{{ trans('schedule.version') }}</th>
                                    <th width="12%">{{ trans('schedule.start_date') }}</th>
                                    <th width="13%">{{ trans('schedule.end_date') }}</th>
                                    <th width="9%">{{ trans('schedule.duration') }}</th>
                                    <th width="10%">{{ trans('schedule.shift') }}</th>
                                    <th width="10%">{{ trans('schedule.resources') }}</th>
                                    <th width="30%">{{ trans('schedule.progress') }}</th>
                                    <th width="15%"><i class="m-nav__link-icon flaticon-users-1"></i></th>
                                </tr>
                            </thead>
                            <body>
                                @if($versions)
                                    @php $x=1; @endphp
                                    @foreach($versions as $data)
                                        @if($data->id==$item->id)
                                            <tr>
                                                <td>{{ $x }}</td>
                                                <td>{{ datecheck($data->start_date,$lang) }}</td>
                                                <td>{{ datecheck($data->end_date,$lang) }}</td>
                                                <td>{{ $data->duration }}</td>
                                                <td>{{ $data->shift }}</td>
                                                <td>{{ $data->resources }}</td>
                                                <td>
                                                    <?php
                                                    // Get all public holidays
                                                    $holidayDays = holidays(decrypt($enc_id));
                                                    // Get working days only
                                                    $workingDays = workingDays(decrypt($enc_id));
                                                    $total_working_days = working_days($data->start_date, $data->end_date, $workingDays, $holidayDays);
                                                    $working = calculate_working($data->start_date, date('Y-m-d'), $workingDays, $holidayDays,$total_working_days);
                                                    ?>
                                                    <div class="d-flex flex-column w-100 mr-2">
                                                        <span class="text-muted mr-2 font-size-sm" style="font-size:1rem;">{{$working}}%</span>
                                                        <div class="progress rounded">
                                                            <div class="progress-bar bg-info" role="progressbar" style="width: {{$working}}%;" aria-valuenow="{{$working}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $data->user->name }}</td>
                                            </tr> 
                                            @php $x++; @endphp
                                        @endif
                                    @endforeach
                                @endif
                            </body>
                        </table>
                    </div>
                </div>
                <!--end::content-->
            </div>
        </div>
    </div>
</div>