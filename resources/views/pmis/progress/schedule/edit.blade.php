<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('global.edit') }}</h3>
            </div> 
        </div>
    </div>
    @if($record)
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" id="ModalFormEdit">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePickeredit errorDiv" type="text" name="start_date" id="start_date" value="{!! dateCheck($record->start_date,$lang) !!}">
                        <div class="start_date error-div" style="display:none;"></div>
                    </div>
                    <div class="col-lg-6">
                        <label class="title-custom">{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                        <input class="form-control m-input datePickeredit errorDiv" type="text" name="end_date" id="end_date" value="{!! dateCheck($record->end_date,$lang) !!}">
                        <div class="end_date error-div" style="display:none;"></div>
                    </div>  
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions align">
                    <button type="button" class="btn btn-primary" onclick="doEditRecord('{{route('BQSchedules.update',$enc_id)}}','ModalFormEdit','PUT','searchresult');">{{trans('global.submit')}}</button>
                    <button type="reset" class="btn btn-secondary" onclick="redirectFunction()">{{trans('global.cancel')}}</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    @endif
</div>
<script type="text/javascript">
    @if ($lang=="en")
      $(".datePickeredit").attr('type', 'date');
    @else
      $(".datePickeredit").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif
  </script>