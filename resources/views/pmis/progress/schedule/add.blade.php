{{-- Start of the Modal --}}
<div id="AddModal" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="AddModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="share_content">
                <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true" id="m_portlet_tools_6">
                    <!--begin::head-->
                    <div class="m-portlet__head table-responsive">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="" style="font-size: 1.3rem">{{trans('schedule.schedule_add')}}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Head-->
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right pt-2" id="ModalForm">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-3">
                                <label class="">{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input datePicker errorDiv" type="text" name="start_date" id="start_date" />
                                <div class="start_date error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input datePicker errorDiv" type="text" name="end_date" id="end_date" />
                                <div class="end_date error-div" style="display:none;"></div>
                            </div>  
                            <div class="col-lg-3">
                                <label class="">{{ trans('schedule.duration') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input errorDiv" type="number" min="0" name="duration" id="duration" onchange="calculate('{{route('BQSchedules.calculate_schedules')}}','POST','')" />
                                <div class="duration error-div" style="display:none;"></div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">{{ trans('schedule.shift') }}: <span style="color:red;">*</span></label>
                                <input class="form-control m-input errorDiv" type="number" max="8" min="4" step="4" value="8" name="shift" id="shift" />
                                <div class="shift error-div" style="display:none;"></div>
                            </div>  
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions align">
                                <input type="hidden" name="selectedBQ" id="selectedBQ" value="" />
                                <button type="button" class="btn btn-primary" onclick="storeRecord('{{route('BQSchedules.store')}}','ModalForm','POST','response_div',redirectFunction,false,this.id);" id="add">{{trans('global.submit')}}</button>
                                <button type="reset" class="btn btn-secondary" onclick="redirectFunction()">{{trans('global.cancel')}}</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>
{{-- End of the Modal  --}}
<script type="text/javascript">
    function calculate(url,method,response_div){
        start_date = $('#start_date').val();
        end_date   = $('#end_date').val();
        duration   = $('#duration').val();
        shift      = $('#shift').val();
        selectedBQ = $('#selectedBQ').val();
        $.ajax({
            url: url,
            data: {
                '_method'    : method,
                "start_date" : start_date,
                "end_date"   : end_date,
                "duration"   : duration,
                "shift"      : shift,
                "selectedBQ" : selectedBQ,
                'project_id' : '{{$enc_id}}'
            },
            type:method,
            beforeSend: function()
            {
                $(".m-page-loader.m-page-loader--base").css("display","block");
            },
            success: function(response)
            {
                alert(response); return;
                $(".m-page-loader.m-page-loader--base").css("display","none");
                redirectFunction(response,response_div);
            },
        })
    }
</script>