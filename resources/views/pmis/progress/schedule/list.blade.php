@extends('master')
@section('head')
    <title>{{ trans('schedule.schedules') }}</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile" id="showContent">
        <div class="m-portlet__head m-portlet__head-bg table-responsive">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text text-white">{{$plan->name}}</h3>
                </div> 
            </div>
            <div class="m-portlet__head-tools">
                <!-- Include Summary Modal -->
                @include('pmis.summary.summary_modal')
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm border-dark" href="" data-toggle="modal" data-target="#SummaryModal">
                            <span><i class="fa fa-folder-open"></i> <span>{{ trans('global.project_summary') }}</span></span>
                        </a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',encrypt($dep_id)) }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist" style="padding-right:10px; padding-left:10px;">
            @if($tabs)
                @foreach($tabs as $item)
                    @if(check_my_section($item->code))
                        <li class="nav-item m-tabs__item">
                            <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
        <div id="content">
            <div class="form-group pl-3 pr-3 row">
                <div class="col-lg-9">
                    <label class="title-custom" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('BQSchedule/list',[$enc_id,encrypt($dep_id),'loc_id']) }}',this.value)" >
                        <option value="">{{ trans('global.select') }}</option>
                        @if($project_location)
                            @foreach($project_location as $item)
                                <option value="{{$item->id}}" <?= $project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="title-custom">&nbsp;</label><br>
                    <!-- Include Project location Add Modal -->
                    @if(doIHaveRole("tab_bq_schedule","tab_bq_schedule_add"))
                        @include('pmis.progress.schedule.add')  
                        <a class="btn btn-info m-btn--custom m-btn--icon btn-sm float-right ml-4 mr-4 mb-3 disabled" id="submitBTN" data-toggle="modal" data-target="#AddModal" href="javascript:void()" disabled="disabled">
                            <span><i class="la la-check-square"></i> <span>{{ trans('schedule.schedule_add') }}</span></span>
                        </a>
                    @endif
                </div>
            </div>   
            @alert()
            @endalert 
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-3">
                <div class="m-portlet__body table-responsive" id="searchresult">
                    @if($records->count()>0)
                        <table class="table table-bordered table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th width="30%">{{ trans('estimation.operation_type') }}</th>
                                    <th width="10%">{{ trans('estimation.amount') }}</th>
                                    <th width="12%">{{ trans('estimation.total_price') }}</th>
                                    <th width="13%">{{ trans('estimation.percentage') }}</th>
                                    <th width="11%">{{ trans('schedule.start_date') }}</th>
                                    <th width="10%">{{ trans('schedule.end_date') }}</th>
                                    <th width="4%">{{ trans('global.choose') }}</th>
                                    <th width="6%">{{ trans('global.action') }}</th>
                                </tr>
                            </thead>
                            @foreach($records AS $item)
                                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                    <tr>
                                        <td>{{ $item->operation_type }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->total_price }}</td>
                                        <td>{{ getPercentage($item->estimation['cost'],$item->total_price) }} %</td>
                                        <td>{{ datecheck($item->start_date,$lang) }}</td>
                                        <td>{{ datecheck($item->end_date,$lang) }}</td>
                                        <td>
                                            <div class="m-checkbox-list">
                                                @if($item->start_date=="")
                                                    <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                                        <input type="checkbox" name="BQSelected[]" value="{{ $item->id }}" id="{{ $item->id }}" class="BQSelected" onchange="BQSelected(this.id)"> <span style="transform: rotate(90deg);"></span>
                                                    </label>
                                                @else
                                                    <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                                        <input type="checkbox" checked disabled> <span style="transform: rotate(90deg);"></span>
                                                    </label>
                                                @endif
                                            </div>    
                                        </td>
                                        <td>
                                            <span class="dtr-data">
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                                        @if(doIHaveRole("tab_bq_schedule","tab_bq_schedule_view"))
                                                            <a class="dropdown-item" href="javascript:void()"  onclick="viewRecord('{{route('BQSchedules.show', encrypt($item->id))}}','','GET','searchresult')"><i class="la la-eye"></i> {{ trans('global.view') }}</a>
                                                        @endif
                                                        @if(doIHaveRole("tab_bq_schedule","tab_bq_schedule_edit") and $item->start_date!="")
                                                            <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('BQSchedules.edit', encrypt($item->id))}}','','GET','searchresult')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                                        @endif
                                                    </div>
                                                </span>
                                            </span>
                                        </td>
                                    </tr>    
                                </tbody>
                            @endforeach
                        </table>
                        @if(!empty($records))
                            {{ $records->links('pagination') }}
                        @endif
                    @else
                        <div class="col-xl-9 col-lg-12">
                            <div class="m-wizard__form">
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                    <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                        <dt>{{ trans('global.no_records') }}</dt>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    @endif
                </div>
            </div>  
        </div>
    </div>
@endsection
@section('js-code')
  <script type="text/javascript">
    @if ($lang=="en")
      $(".datePicker").attr('type', 'date');
    @else
      $(".datePicker").persianDatepicker({cellWidth: 45, cellHeight: 28, fontSize: 11});
    @endif
  </script>
@endsection