<table class="table table-bordered table-checkable head-has-color">
  <thead>
      <tr class="bg-light">
          <th width="25%">{{ trans('estimation.operation_type') }}</th>
          <th width="7%">{{ trans('estimation.amount') }}</th>
          <th width="7%">{{ trans('estimation.total_price') }}</th>
          <th width="7%">{{ trans('estimation.percentage') }}</th>
          <th width="10%">{{ trans('schedule.start_date') }}</th>
          <th width="10%">{{ trans('schedule.end_date') }}</th>
          <th width="7%">{{ trans('schedule.duration') }}</th>
          <th width="7%">{{ trans('schedule.shift') }}</th>
          <th width="7%">{{ trans('schedule.resources') }}</th>
          <th width="13%">{{ trans('schedule.progress') }}</th>
      </tr>
  </thead>
  @php
      $bq_section = 0;
  @endphp
  @foreach($records AS $item)
      @if($bq_section != $item->bq_section_id)
          @php
              $bq_section = $item->bq_section_id;
          @endphp
          <tr style="background: #5b99bf4a">
              <td colspan="10" class="text-center title-custom">
                  {{ $item->bq_section->{'name_'.$lang} }}
              </td>
          <tr>
      @endif
      <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
          <tr>
              <td>{{ $item->operation_type }}</td>
              <td>{{ $item->amount }}</td>
              <td>{{ $item->total_price }}</td>
              <td>{{ getPercentage($plan->procurement['contract_price'],$item->total_price) }} %</td>
              <td>{{ datecheck($item->start_date,$lang) }}</td>
              <td>{{ datecheck($item->end_date,$lang) }}</td>
              <td>{{ $item->duration }}</td>
              <td>{{ $item->shift }}</td>
              <td>{{ $item->resources }}</td>
              <td>
                  <?php
                  // Get all public holidays
                  $holidayDays = holidays($project_location_id);
                  // Get working days only
                  $workingDays = workingDays($project_location_id);
                  $total_working_days = working_days($item->start_date, $item->end_date, $workingDays, $holidayDays);
                  $working = calculate_working($item->start_date, date('Y-m-d'), $workingDays, $holidayDays,$total_working_days);
                  ?>
                  <div class="d-flex flex-column w-100 mr-2">
                      <span class="text-muted mr-2 font-size-sm" style="font-size:1rem;">{{$working}}%</span>
                      <div class="progress rounded">
                          <div class="progress-bar bg-info" role="progressbar" style="width: {{$working}}%;" aria-valuenow="{{$working}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </td>
          </tr>
      </tbody>
  @endforeach
</table>
@if(!empty($records))
    {{ $records->links('pagination') }}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>