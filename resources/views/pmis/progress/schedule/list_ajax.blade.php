<table class="table table-bordered table-hover table-checkable">
    <thead>
        <tr>
            <th width="30%">{{ trans('estimation.operation_type') }}</th>
            <th width="10%">{{ trans('estimation.amount') }}</th>
            <th width="12%">{{ trans('estimation.total_price') }}</th>
            <th width="13%">{{ trans('estimation.percentage') }}</th>
            <th width="11%">{{ trans('schedule.start_date') }}</th>
            <th width="10%">{{ trans('schedule.end_date') }}</th>
            <th width="4%">{{ trans('global.choose') }}</th>
            <th width="6%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    @foreach($records AS $item)
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
            <tr>
                <td>{{ $item->operation_type }}</td>
                <td>{{ $item->amount }}</td>
                <td>{{ $item->total_price }}</td>
                <td>{{ getPercentage($item->estimation['cost'],$item->total_price) }} %</td>
                <td>{{ datecheck($item->start_date,$lang) }}</td>
                <td>{{ datecheck($item->end_date,$lang) }}</td>
                <td>
                    <div class="m-checkbox-list">
                        @if($item->start_date=="")
                            <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                <input type="checkbox" name="BQSelected[]" value="{{ $item->id }}" id="{{ $item->id }}" class="BQSelected" onchange="BQSelected(this.id)"> <span style="transform: rotate(90deg);"></span>
                            </label>
                        @else
                            <label class="m-checkbox m-checkbox--air m-checkbox--state-brand">
                                <input type="checkbox" checked disabled> <span style="transform: rotate(90deg);"></span>
                            </label>
                        @endif
                    </div>    
                </td>
                <td>
                    <span class="dtr-data">
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                                @if(doIHaveRole("tab_bq_schedule","tab_bq_schedule_view"))
                                <a class="dropdown-item" href="javascript:void()"  onclick="viewRecord('{{route('BQSchedules.show', encrypt($item->id))}}','','GET','showContent')"><i class="la la-eye"></i> {{ trans('global.view') }}</a>
                                @endif
                                @if(doIHaveRole("tab_bq_schedule","tab_bq_schedule_edit") and $item->start_date!="")
                                    <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('BQSchedules.edit', encrypt($item->id))}}','','GET','showContent')"><i class="la la-edit"></i>{{ trans('global.edit') }}</a>
                                @endif
                            </div>
                        </span>
                    </span>
                </td>
            </tr>    
        </tbody>
    @endforeach
</table>
@if(!empty($records))
    {{ $records->links('pagination') }}
@endif
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.pagination a').on('click', function(event) {
      event.preventDefault();
      if ($(this).attr('href') != '#') {
        // Get current URL route
        document.cookie = "no="+$(this).text();
        var dataString = '';
        dataString += "&page="+$(this).attr('id')+"&ajax="+1;
        $.ajax({
                url:  '{{ url()->current() }}',
                data: dataString,
                type: 'get',
                beforeSend: function(){
                  $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></span>');
                },
                success: function(response)
                {
                  $('#searchresult').html(response);
                }
            }
        );
      }
    });
  });
</script>