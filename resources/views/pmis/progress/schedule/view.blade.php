<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head table-responsive">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">{{ trans('global.view') }}</h3>
            </div> 
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('BQSchedule/list',[encrypt($plan->id),session('current_department'),session('project_location_id')]) }}" onclick="">
                        <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"> 
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.operation_type') }}: </label><br>
                        <span>{{ $record->operation_type }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.unit') }}: </label><br>
                        <span>{{ $record->unit->{'name_'.$lang} }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.amount') }}: </label><br>
                        <span>{{ $record->amount }}</span>
                    </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.price') }}: </label><br>
                        <span>{{ $record->price }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.total_price') }}: </label><br>
                        <span>{{ $record->total_price }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.percentage') }}: </label><br>
                        <span>{{ getPercentage($record->estimation['cost'],$record->total_price) }} %</span>
                    </div>
                </div>
                <div class="form-group m-form__group row m-form__group_custom">
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('estimation.remarks') }}: </label><br>
                        <span>{{ $record->remarks }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('schedule.start_date') }}: </label><br>
                        <span>{{ datecheck($record->start_date,$lang) }}</span>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                        <label class="title-custom">{{ trans('schedule.end_date') }}: </label><br>
                        <span>{{ datecheck($record->end_date,$lang) }}</span>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
