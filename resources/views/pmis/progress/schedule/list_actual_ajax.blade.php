<table class="table table-bordered">
    <thead>
        <tr class="bg-light">
            <th width="23%">{{ trans('estimation.operation_type') }}</th>
            <th width="10%">{{ trans('schedule.start_date') }}</th>
            <th width="10%">{{ trans('schedule.end_date') }}</th>
            <th width="5%">{{ trans('schedule.duration') }}</th>
            <th width="5%">{{ trans('schedule.shift') }}</th>
            <th width="5%">{{ trans('schedule.resources') }}</th>
            <th width="21%">{{ trans('schedule.progress') }}</th>
            <th width="10%">{{ trans('schedule.status') }}</th>
            <th width="5%">{{ trans('schedule.versions') }}</th>
            <th width="5%">{{ trans('global.action') }}</th>
        </tr>
    </thead>
    @php
        $bq_section = 0;
    @endphp
    @foreach($records AS $item)
        @if($bq_section != $item->bq_section_id)
            @php
                $bq_section = $item->bq_section_id;
            @endphp
            <tr style="background: #5b99bf4a">
                <td colspan="10" class="text-center title-custom">
                    {{ $item->bq_section->{'name_'.$lang} }}
                </td>
            <tr>
        @endif
        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
            <tr>
                <td>
                    <!-- Include Schedule Versions -->
                    @include('pmis.progress.schedule.details_modal')
                    <span class="btn-link cursor-pointer" data-toggle="modal" data-target="#summaryDetails_{{ $item->id }}">{{ $item->operation_type }}</span>
                </td>
                <td>{{ datecheck($item->start_date,$lang) }}</td>
                <td>{{ datecheck($item->end_date,$lang) }}</td>
                <td>{{ $item->duration }}</td>
                <td>{{ $item->shift }}</td>
                <td>{{ $item->resources }}</td>
                <td>
                    <?php
                    if($item->bq_base)
                    {
                        // Estimated progress     
                        $holidayDays = holidays($project_location_id);// Get all public holidays
                        $workingDays = workingDays($project_location_id);// Get working days only
                        $total_working_days = working_days($item->bq_base->start_date, $item->bq_base->end_date, $workingDays, $holidayDays);
                        $base = calculate_working($item->bq_base->start_date, date('Y-m-d'), $workingDays, $holidayDays,$total_working_days);
                        // Actual progress from fields
                        $actual = getBQDone($item->id,$item->amount);
                    }
                    else{
                        // Estimated progress     
                        $base = 0;
                        // Actual progress from fields
                        $actual = 0;
                    }
                    ?>
                    <div class="d-flex flex-column w-100 mr-2">
                        <div class="row">     
                            <span class="text-muted font-size-sm col-lg-7 col-md7 col-sm7 col-xs7" style="font-size:1rem;">{{trans('schedule.base')}}</span>
                            <span class="text-muted font-size-sm col-lg-5 col-md-5 col-sm-5 col-xs-5" style="font-size:1rem;">{{$base}}%</span>
                        </div>
                        <div class="progress rounded">
                            <div class="progress-bar bg-info" role="progressbar" style="width: {{$base}}%;" aria-valuenow="{{$base}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="d-flex flex-column w-100 mr-2">
                        <div class="row">     
                            <span class="text-muted font-size-sm col-lg-7 col-md-7 col-sm-7 col-xs-7" style="font-size:1rem;">{{trans('schedule.actual')}}</span>
                            <span class="text-muted font-size-sm col-lg-5 col-md-5 col-sm-5 col-xs-5" style="font-size:1rem;">{{$actual}}%</span>
                        </div>
                        <div class="progress rounded">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$actual}}%;" aria-valuenow="{{$actual}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </td>
                <td>
                    @if($item->started==1)
                        <span class="m-badge m-badge--warning m-badge--wide opacity1">{{ trans('schedule.started') }}</span>
                    @else
                        <span class="m-badge m-badge--danger m-badge--wide opacity1">{{ trans('schedule.not_started') }}</span>
                    @endif
                </td>
                <td>
                    <!-- Include Schedule Versions -->
                    @include('pmis.progress.schedule.shcedule_modal')
                    <button type="button" data-toggle="modal" data-target="#summarySchedule_{{ $item->id }}" class="btn btn-icon btn-sm btn-info btn-circle mx-2"> 
                        <span><i class="fa fa-th-list"></i></span>
                    </button>
                </td>
                <td>
                    <!-- Add Button -->
                    @if(doIHaveRoleInDep(session('current_department'),"tab_bq_schedule","tab_bq_schedule_add"))
                        <a href="#" class="btn btn-icon btn-sm btn-success btn-circle mx-2" data-toggle="collapse" data-target="#demo_{{ $item->id }}" class="accordion-toggle" @click="newBQSchedule('{{$item->id}}', '{{ dateCheck($item->start_date,$lang) }}', '{{ dateCheck($item->end_date,$lang) }}', '{{ $item->duration }}', '{{ $item->shift }}', '{{ $item->resources }}', '{{ $item->started }}')">
                            <i class="la la-plus"></i>
                        </a>
                    @endif
                    
                </td>
            </tr>
            <!--begin::Form-->
            <form id="scheduleForm_{{ $item->id }}">
                <tr class="accordian-body collapse bg-light border-d" id="demo_{{ $item->id }}">
                    <td colspan="1">
                        <label>{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                        <date-picker v-model="list.start_date" locale="{{($lang=='en' ? 'en' : 'fa')}}" format="{{($lang=='en' ? 'YYYY-MM-DD' : 'jYYYY-jMM-jDD')}}"></date-picker>
                    </td>
                    <td colspan="2">
                        <label>{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                        <date-picker v-model="list.end_date" locale="{{($lang=='en' ? 'en' : 'fa')}}" format="{{($lang=='en' ? 'YYYY-MM-DD' : 'jYYYY-jMM-jDD')}}" v-on:change="calculate('end_date')"></date-picker>
                    </td>
                    <td colspan="3">
                        <label>{{ trans('schedule.duration') }}: <span style="color:red;">*</span></label>
                        <input v-model="list.duration" class="form-control m-input errorDiv required" type="number" min="1" v-on:change="calculate('duration')" />
                        <input v-model="list.def_duration" class="form-control m-input errorDiv required" type="hidden" />
                    </td>
                    <td colspan="1">
                        <label>{{ trans('schedule.shift') }}: <span style="color:red;">*</span></label>
                        <input v-model="list.shift" class="form-control m-input errorDiv required" type="number" max="1" min="0.5" step="0.5" v-on:change="calculate('shift')" />
                    </td>
                    <td colspan="1">
                        <label>{{ trans('schedule.resources') }}: <span style="color:red;">*</span></label>
                        <input v-model="list.resources" class="form-control m-input errorDiv required" :max='list.def_duration' type="number" :min='1' v-on:change="calculate('resources')"/>
                        <input v-model="list.def_resources" class="form-control m-input errorDiv required" type="hidden" />
                    </td>
                    <td colspan="1">
                        <label class="">{{ trans('schedule.start') }}: </label><br>
                        <span class=" m-switch m-switch--icon">
                            <label><input <?=$item->started=='1'? 'v-model="list.started"' : ''?> type="checkbox" class="form-control m-input" name="started" id="started" v-on:change="change_status('started')" ><span></span></label>
                        </span>
                    </td>
                    <td>
                        <input type="hidden" name="bq_id" value="{{ $item->id }}" />
                        <a href="#" class="btn btn-icon btn-sm btn-success btn-circle mx-1 m-1" v-on:click="storeRecord()"><i class="la la-save"></i></a><br>
                        <a href="#" class="btn btn-icon btn-sm btn-warning btn-circle mx-1 m-1" data-toggle="collapse" data-target="#demo_{{ $item->id }}" class="accordion-toggle"><i class="la la-minus"></i></a>
                    </td>
                </tr>
            </form>
            <!--end::Form-->
        </tbody>
    @endforeach
</table>
@if(!empty($records))
    {{ $records->links('pagination_no_ajax') }}
@endif

@section('js-code')
    <script src="{!!asset('public/assets/vue-datepicker/vue.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/moment.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/moment-jalali.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/vue-persian-datetime-picker-browser.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/axios.min.js')!!}" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $("[type='number']").keypress(function (evt) {evt.preventDefault();});
            $('[data-toggle="popover"]').popover();   
        });
        new Vue({
            el: '#list',
            data: {
                list: [
                    //
                ]
            },
            methods: {
                newBQSchedule(id, start_date, end_date, duration, shift, resources, started) {
                    if(start_date){
                        this.list = {
                            id:             id,
                            start_date:     start_date,
                            end_date:       end_date,
                            duration:       duration,
                            project_id:     '{{ $enc_id }}',
                            shift:          shift,
                            resources:      resources,
                            def_duration:   duration,
                            def_resources:  resources,
                            started:        started,
                            status:         'end_date'
                        }
                    }else{
                        this.list = {
                            id:             id,
                            start_date:     '{{ dateCheck(date("Y-m-d"),$lang) }}',
                            end_date:       '',
                            duration:       0,
                            project_id:     '{{ $enc_id }}',
                            shift:          1,
                            resources:      1,
                            def_duration:   0,
                            started:        0,
                            status:         'end_date'
                        }
                    }
                },
                calculate(flag) {
                    this.list.status = flag;
                    axios.post("{{route('BQSchedules.calculate_schedules')}}", this.list)
                        .then(res => {
                            if(flag=='duration'){
                                var data = res.data.split("_");
                                this.list.end_date = data[1];
                                this.list.def_duration = data[0];
                            }else if(flag=='end_date'){
                                var data = res.data.split("_");
                                if(data[1]==1){
                                    if(data[0]>=1){
                                        this.list.resources = data[0];
                                    }else{
                                        this.list.resources = 1; 
                                    }
                                }else{
                                    this.list.resources = 0;
                                }
                                //this.list.def_duration = res.data;
                            }else if(flag=='shift'){
                                this.list.end_date = res.data;
                            }else if(flag=='resources'){
                                this.list.end_date = res.data;
                            }
                        })
                        .catch(err => {
                            //
                        })
                },
                storeRecord() {
                    axios.post("{{route('BQSchedules.store')}}", this.list)
                        .then(res => {
                            location.reload();
                        })
                        .catch(err => {
                            console.log(err)
                        })
                },
                change_status(id){
                    if(this.list.started==0){
                        this.list.started=1;
                    }else{
                        this.list.started=0;
                    }
                }
            },
            components: {
                DatePicker: VuePersianDatetimePicker
            }
        });
    </script>
@endsection