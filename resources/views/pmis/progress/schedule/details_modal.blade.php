{{-- Start of the Modal --}}
<div id="summaryDetails_{{ $item->id }}" class="modal fade bd-example-modal-sm" role="dialog" aria-labelledby="summaryDetails_{{ $item->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="m-portlet m-portlet--head-sm m-0" m-portlet="true">
                <!--begin::head-->
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text" style="font-size: 1.3rem">{{ trans('schedule.details') }}</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Head-->
                <!--begin::content-->
                <div class="m-form m-form--label-align-right p-2" enctype="multipart/form-data" id="publicHolidaysForm">
                    <div class="form-group m-form__group row m-form__group_custom">
                    <table class="table table-bordered">
                            <thead>
                                <tr class="bg-light">
                                    <th width="55%">{{ trans('estimation.operation_type') }}</th>
                                    <th width="15%">{{ trans('estimation.amount') }}</th>
                                    <th width="15%">{{ trans('estimation.total_price') }}</th>
                                    <th width="15%">{{ trans('estimation.percentage') }}</th>
                                </tr>
                            </thead>
                            <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                <tr>
                                    <td>{{ $item->operation_type }}</td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ $item->total_price }}</td>
                                    <td>{{ getPercentage($plan->procurement['contract_price'],$item->total_price) }} %</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::content-->
            </div>
        </div>
    </div>
</div>