@extends('master')
@section('head')
    <title>{{ trans('schedule.schedules') }}</title>
    <style>
        .btn-shadow {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19) !important;
        }
    </style>
@endsection
@section('content')
    <div class="row mt-1">
        <div class="col-lg-12 pb-1">
            <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('project_dashboard.project' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{$plan->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-symbol m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('monitoring.contractor') }}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $plan->procurement->company }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-2 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('progress.contraction_duration')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ differenceBetweenDate($plan->procurement->start_date,$plan->procurement->end_date,'%a',true) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-3 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('project_dashboard.contract_amount')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{number_format($plan->procurement->contract_price)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="list">
        <div class="m-portlet m-portlet--mobile mb-2" id="showContent">
            <div class="m-portlet__head table-responsive border-0">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand border-0 mb-0" role="tablist">
                            @if($tabs)
                                @foreach($tabs as $item)
                                    @if(check_my_section(array($dep_id),$item->code))
                                        <li class="nav-item m-tabs__item">
                                            <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                                <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div> 
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',encrypt($dep_id)) }}">
                                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="m-portlet m-portlet--mobile">   
            @if($excluded_locations->count()>0)
                <div class="accordion row mx-1 mt-1">
                    <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                        <div class="m-alert__text text-dark bg-warning">
                            <div>
                                <i class="la la-warning text-dark"></i> &nbsp; {{ trans('progress.excluded_projects') }}: 
                                <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                    <span><i class="la la-angle-double-down"></i></span>
                                </a>
                            </div>
                            <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2 bg-warning">
                                    <div class="m-portlet__body bg-warning">
                                        <div class="m-portlet__body table-responsive bg-warning">
                                            <ul class="list-group list-group-flush bg-warning">
                                                @foreach($excluded_locations as $item)
                                                    <li>
                                                        @if($item->status==1 and check_my_section(array($dep_id),'pmis_completed'))
                                                        <a href="{{ route('completed.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                        @elseif($item->status==2 and check_my_section(array($dep_id),'pmis_stopped'))
                                                        <a href="{{ route('stopped.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                        @endif
                                                            {{ $loop->iteration }} - 
                                                            {{ $item->province()->first()->{'name_'.$lang} }} 
                                                            @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif 
                                                            @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                            @if($item->latitude)/ {{ $item->latitude }} @endif 
                                                            @if($item->longitude)/ {{ $item->longitude }} @endif
                                                        </a>
                                                        <ul>
                                                            <li>
                                                                @if($item->status==1)
                                                                    {{ trans('global.excluded_description',['status'=> __('global.completed') ]) }}
                                                                @elseif($item->status==2)
                                                                    {{ trans('progress.excluded_description',['status'=> __('global.stopped') ]) }}
                                                                @endif
                                                            </li>
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            {{-- End of Collpas div --}} 
                        </div>
                    </div>
                </div>
            @endif
            <!-- Include complete modal -->
            @include('pmis.progress.schedule.complete_modal')
            <!-- Include stop modal -->
            @include('pmis.progress.schedule.stop_modal')
            <div id="content">
                @if(!in_array($project_location_id,$excluded_locations_array))
                    <div class="form-group pb-3 px-3 row">
                        <div class="col-lg-9">
                            <label class="title-custom pt-3" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                            <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('BQSchedule/list',[$enc_id,encrypt($dep_id),'loc_id']) }}',this.value)" >
                                <option value="">{{ trans('global.select') }}</option>
                                @if($project_location)
                                    @foreach($project_location as $item)
                                        <option value="{{$item->id}}" <?=$project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-lg-3 text-center">
                            @if($records->count()>0)
                                <label class="title-custom pt-3" style="color:#575962">&nbsp;</label><br>
                                <div class="m-btn-group m-btn-group--pill btn-group m-btn-group btn-group-sm" role="group" aria-label="Large button group">
                                    @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_base"))
                                        @if($base->count()==0) 
                                            <button type="button" onclick="makeBase('{{ route('BQSchedule.make_base',['pro_id'=>$enc_id,'loc_id'=>encrypt($project_location_id)]) }}','','GET','');" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom"><span><i class="la la-chain"></i> <span>{{ trans('schedule.make_base') }}</span></span></button>
                                        @else
                                            <button type="button" onclick="relocate_home('{{ route('BQSchedule.base',['id'=>$enc_id,'dep_id'=>encrypt($dep_id),'loc_id'=>$project_location_id]) }}')" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom"><span><i class="la la-chain"></i> <span>{{ trans('schedule.base') }}</span></span></button>
                                        @endif
                                    @endif                        
                                    <button type="button" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom active"><span><i class="la la-map"></i> <span>{{ trans('schedule.working') }}</span></span></button>
                                </div>              
                            @endif
                            @if($base->count()>0)
                                <div class="btn-group open">
                                    <button data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true" class="btn btn-outline-accent m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air p-3">
                                        <i class="fa flaticon-folder-1"></i>
                                    </button>
                                    <ul class="dropdown-menu mt-2" role="menu">
                                        <li>
                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_complete"))
                                                <a href="javascript:;" data-toggle="modal" data-target="#completeModal">
                                                    <span><i class="la la-check-square"></i> <span>{{ trans('progress.complete') }}</span></span>
                                                </a>
                                            @endif
                                        </li>
                                        <li>
                                            @if(doIHaveRoleInDep(session('current_department'),"pmis_progress","progress_stop"))
                                                <a href="javascript:;" data-toggle="modal" data-target="#stopModal">
                                                    <span><i class="fl flaticon-danger"></i> <span>{{ trans('progress.stop') }}</span></span>
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>   
                    @alert()
                    @endalert       
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-3">
                        <div class="m-portlet__body table-responsive" id="searchresult">
                            @if($records->count()>0)
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="bg-light">
                                            <th width="23%">{{ trans('estimation.operation_type') }}</th>
                                            <th width="10%">{{ trans('schedule.start_date') }}</th>
                                            <th width="10%">{{ trans('schedule.end_date') }}</th>
                                            <th width="5%">{{ trans('schedule.duration') }}</th>
                                            <th width="5%">{{ trans('schedule.shift') }}</th>
                                            <th width="5%">{{ trans('schedule.resources') }}</th>
                                            <th width="21%">{{ trans('schedule.progress') }}</th>
                                            <th width="10%">{{ trans('schedule.status') }}</th>
                                            <th width="5%">{{ trans('schedule.versions') }}</th>
                                            <th width="5%">{{ trans('global.action') }}</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $bq_section = 0;
                                    @endphp
                                    @foreach($records AS $item)
                                        @if($bq_section != $item->bq_section_id)
                                            @php
                                                $bq_section = $item->bq_section_id;
                                            @endphp
                                            <tr style="background: #5b99bf4a">
                                                <td colspan="10" class="text-center title-custom">
                                                    {{ $item->bq_section->{'name_'.$lang} }}
                                                </td>
                                            <tr>
                                        @endif
                                        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                            <tr>
                                                <td>
                                                    <!-- Include Schedule Versions -->
                                                    @include('pmis.progress.schedule.details_modal')
                                                    <span class="btn-link cursor-pointer" data-toggle="modal" data-target="#summaryDetails_{{ $item->id }}">{{ $item->operation_type }}</span>
                                                </td>
                                                <td>{{ datecheck($item->start_date,$lang) }}</td>
                                                <td>{{ datecheck($item->end_date,$lang) }}</td>
                                                <td>{{ $item->duration }}</td>
                                                <td>{{ $item->shift }}</td>
                                                <td>{{ $item->resources }}</td>
                                                <td>
                                                    <?php
                                                    if($item->bq_base)
                                                    {
                                                        // Estimated progress     
                                                        $holidayDays = holidays($project_location_id);// Get all public holidays
                                                        $workingDays = workingDays($project_location_id);// Get working days only
                                                        $total_working_days = working_days($item->bq_base->start_date, $item->bq_base->end_date, $workingDays, $holidayDays);
                                                        $working = calculate_working($item->bq_base->start_date, date('Y-m-d'), $workingDays, $holidayDays,$total_working_days);
                                                        // Actual progress from fields
                                                        $actual = getBQDone($item->id,$item->amount);
                                                    }
                                                    else{
                                                        // Estimated progress     
                                                        $working = 0;
                                                        // Actual progress from fields
                                                        $actual = 0;
                                                    }
                                                    ?>
                                                    <div class="d-flex flex-column w-100 mr-2">
                                                        <div class="row">     
                                                            <span class="text-muted font-size-sm col-lg-7 col-md7 col-sm7 col-xs7" style="font-size:1rem;">{{trans('schedule.base')}}</span>
                                                            <span class="text-muted font-size-sm col-lg-5 col-md-5 col-sm-5 col-xs-5" style="font-size:1rem;">{{$working}}%</span>
                                                        </div>
                                                        <div class="progress rounded">
                                                            <div class="progress-bar bg-info" role="progressbar" style="width: {{$working}}%;" aria-valuenow="{{$working}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex flex-column w-100 mr-2">
                                                        <div class="row">     
                                                            <span class="text-muted font-size-sm col-lg-7 col-md-7 col-sm-7 col-xs-7" style="font-size:1rem;">{{trans('schedule.actual')}} ({{trans('schedule.work_done')}})</span>
                                                            <span class="text-muted font-size-sm col-lg-5 col-md-5 col-sm-5 col-xs-5" style="font-size:1rem;">{{$actual}}%</span>
                                                        </div>
                                                        <div class="progress rounded">
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$actual}}%;" aria-valuenow="{{$actual}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($item->started==1)
                                                        <span class="m-badge m-badge--warning m-badge--wide opacity1">{{ trans('schedule.started') }}</span>
                                                    @else
                                                        <span class="m-badge m-badge--danger m-badge--wide opacity1">{{ trans('schedule.not_started') }}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <!-- Include Schedule Versions -->
                                                    @include('pmis.progress.schedule.shcedule_modal')
                                                    <button type="button" data-toggle="modal" data-target="#summarySchedule_{{ $item->id }}" class="btn btn-icon btn-sm btn-info btn-circle mx-2"> 
                                                        <span><i class="fa fa-th-list"></i></span>
                                                    </button>
                                                </td>
                                                <td>
                                                    <!-- Add Button -->
                                                    @if(doIHaveRoleInDep(session('current_department'),"tab_bq_schedule","tab_bq_schedule_add"))
                                                        <a href="#" class="btn btn-icon btn-sm btn-success btn-circle mx-2" data-toggle="collapse" data-target="#demo_{{ $item->id }}" class="accordion-toggle" @click="newBQSchedule('{{$item->id}}', '{{ dateCheck($item->start_date,$lang) }}', '{{ dateCheck($item->end_date,$lang) }}', '{{ $item->duration }}', '{{ $item->shift }}', '{{ $item->resources }}', '{{ $item->started }}')">
                                                            <i class="la la-plus"></i>
                                                        </a>
                                                    @endif 
                                                </td>
                                            </tr>
                                            <!--begin::Form-->
                                            <form id="scheduleForm_{{ $item->id }}">
                                                <tr class="accordian-body collapse bg-light border-d" id="demo_{{ $item->id }}">
                                                    <td colspan="1">
                                                        <label>{{ trans('schedule.start_date') }}: <span style="color:red;">*</span></label>
                                                        <date-picker v-model="list.start_date" locale="{{($lang=='en' ? 'en' : 'fa')}}" format="{{($lang=='en' ? 'YYYY-MM-DD' : 'jYYYY-jMM-jDD')}}"></date-picker>
                                                    </td>
                                                    <td colspan="2">
                                                        <label>{{ trans('schedule.end_date') }}: <span style="color:red;">*</span></label>
                                                        <date-picker v-model="list.end_date" locale="{{($lang=='en' ? 'en' : 'fa')}}" format="{{($lang=='en' ? 'YYYY-MM-DD' : 'jYYYY-jMM-jDD')}}" v-on:change="calculate('end_date')"></date-picker>
                                                    </td>
                                                    <td colspan="3">
                                                        <label>{{ trans('schedule.duration') }}: <span style="color:red;">*</span></label>
                                                        <input v-model="list.duration" class="form-control m-input errorDiv required" type="number" min="1" v-on:change="calculate('duration')" />
                                                        <input v-model="list.def_duration" class="form-control m-input errorDiv required" type="hidden" />
                                                    </td>
                                                    <td colspan="1">
                                                        <label>{{ trans('schedule.shift') }}: <span style="color:red;">*</span></label>
                                                        <input v-model="list.shift" class="form-control m-input errorDiv required" type="number" max="2" min="1" step="1" v-on:change="calculate('shift')" />
                                                    </td>
                                                    <td colspan="1">
                                                        <label>{{ trans('schedule.resources') }}: <span style="color:red;">*</span></label>
                                                        <input v-model="list.resources" class="form-control m-input errorDiv required" :max='list.def_duration' type="number" :min='1' step="0.1" v-on:change="calculate('resources')"/>
                                                        <input v-model="list.def_resources" class="form-control m-input errorDiv required" type="hidden" />
                                                    </td>
                                                    <td colspan="1">
                                                        <label class="">{{ trans('schedule.start') }}: </label><br>
                                                        <span class=" m-switch m-switch--icon">
                                                            <label><input <?=$item->started=='1'? 'v-model="list.started"' : ''?> type="checkbox" class="form-control m-input" name="started" id="started" v-on:change="change_status('started')" ><span></span></label>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="bq_id" value="{{ $item->id }}" />
                                                        <a href="#" class="btn btn-icon btn-sm btn-success btn-circle mx-1 m-1" v-on:click="storeRecord()"><i class="la la-save"></i></a><br>
                                                        <a href="#" class="btn btn-icon btn-sm btn-warning btn-circle mx-1 m-1" data-toggle="collapse" data-target="#demo_{{ $item->id }}" class="accordion-toggle"><i class="la la-minus"></i></a>
                                                    </td>
                                                </tr>
                                            </form>
                                            <!--end::Form-->
                                        </tbody>
                                    @endforeach
                                </table>
                                @if(!empty($records))
                                    {{ $records->links('pagination_no_ajax') }}
                                @endif
                            @else
                                <div class="col-xl-9 col-lg-12">
                                    <div class="m-wizard__form">
                                        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                            <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                                <dt>{{ trans('global.no_records') }}</dt>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div> 
                @else
                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv"> 
                        <div class="form-group pb-3 px-3 row">
                            <div class="col-lg-12">
                                <label class="title-custom pt-3" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                                <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('BQSchedule/list',[$enc_id,encrypt($dep_id),'loc_id']) }}',this.value)" >
                                    <option value="">{{ trans('global.select') }}</option>
                                    @if($project_location)
                                        @foreach($project_location as $item)
                                            <option value="{{$item->id}}" <?=$project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            @alert()
                            @endalert
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('js-code')
    <script src="{!!asset('public/assets/vue-datepicker/vue.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/moment.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/moment-jalali.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/vue-persian-datetime-picker-browser.js')!!}" type="text/javascript"></script>
    <script src="{!!asset('public/assets/vue-datepicker/axios.min.js')!!}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("[type='number']").keypress(function (evt) {evt.preventDefault();});
            $('[data-toggle="popover"]').popover();   
        });
        new Vue({
            el: '#list',
            data: {
                list: [
                    //
                ]
            },
            methods: {
                newBQSchedule(id, start_date, end_date, duration, shift, resources, started) {
                    if(start_date){
                        this.list = {
                            id:             id,
                            start_date:     start_date,
                            end_date:       end_date,
                            duration:       duration,
                            project_id:     '{{ $enc_id }}',
                            shift:          shift,
                            resources:      resources,
                            def_duration:   duration,
                            def_resources:  resources,
                            started:        started,
                            status:         'end_date',
                            location_id:    '{{$project_location_id}}',
                        }
                    }else{
                        this.list = {
                            id:             id,
                            start_date:     '{{ dateCheck(date("Y-m-d"),$lang) }}',
                            end_date:       '',
                            duration:       0,
                            project_id:     '{{ $enc_id }}',
                            shift:          1,
                            resources:      1,
                            def_duration:   0,
                            started:        0,
                            status:         'end_date',
                            location_id:    '{{$project_location_id}}',
                        }
                    }
                },
                calculate(flag) {
                    this.list.status = flag;
                    axios.post("{{route('BQSchedules.calculate_schedules')}}", this.list)
                        .then(res => {
                            if(flag=='duration'){
                                var data = res.data.split("_");
                                this.list.end_date = data[1];
                                this.list.def_duration = data[0];
                            }else if(flag=='end_date'){
                                var data = res.data.split("_");
                                if(data[1]==1){
                                    if(data[0]>=1){
                                        this.list.resources = data[0];
                                    }else{
                                        this.list.resources = 1; 
                                    }
                                }else{
                                    this.list.resources = 0;
                                }
                                //this.list.def_duration = res.data;
                            }else if(flag=='shift'){
                                this.list.end_date = res.data;
                            }else if(flag=='resources'){
                                this.list.end_date = res.data;
                            }
                        })
                        .catch(err => {
                            //
                        })
                },
                storeRecord() {
                    axios.post("{{route('BQSchedules.store')}}", this.list)
                        .then(res => {
                            location.reload();
                        })
                        .catch(err => {
                            console.log(err)
                        })
                },
                change_status(id){
                    if(this.list.started==0){
                        this.list.started=1;
                    }else{
                        this.list.started=0;
                    }
                }
            },
            components: {
                DatePicker: VuePersianDatetimePicker
            }
        });
    </script>
@endsection