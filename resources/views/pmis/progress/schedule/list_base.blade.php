@extends('master')
@section('head')
    <title>{{ trans('schedule.schedules') }}</title>
@endsection
@section('content')
    <div class="row mt-1">
        <div class="col-lg-12 pb-1">
            <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('project_dashboard.project' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{$plan->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-symbol m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('monitoring.contractor') }}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $plan->procurement->company }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-2 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('progress.contraction_duration')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ differenceBetweenDate($plan->procurement->start_date,$plan->procurement->end_date,'%a',true) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-3 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('project_dashboard.contract_amount')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{number_format($plan->procurement->contract_price)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile mb-2" id="showContent">
        <div class="m-portlet__head table-responsive border-0">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand border-0 mb-0" role="tablist">
                        @if($tabs)
                            @foreach($tabs as $item)
                                @if(check_my_section(array($dep_id),$item->code))
                                    <li class="nav-item m-tabs__item">
                                        <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                            <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div> 
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',encrypt($dep_id)) }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        @if($excluded_locations->count()>0)
            <div class="accordion row mx-1 mt-1">
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                    <div class="m-alert__text text-dark bg-warning">
                        <div>
                            <i class="la la-warning text-dark"></i> &nbsp; {{ trans('progress.excluded_projects') }}: 
                            <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                <span><i class="la la-angle-double-down"></i></span>
                            </a>
                        </div>
                        <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2 bg-warning">
                                <div class="m-portlet__body bg-warning">
                                    <div class="m-portlet__body table-responsive bg-warning">
                                        <ul class="list-group list-group-flush bg-warning">
                                            @foreach($excluded_locations as $item)
                                                <li>
                                                    @if($item->status==1 and check_my_section(array($dep_id),'pmis_completed'))
                                                    <a href="{{ route('completed.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                    @elseif($item->status==2 and check_my_section(array($dep_id),'pmis_stopped'))
                                                    <a href="{{ route('stopped.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                    @endif
                                                        {{ $loop->iteration }} - 
                                                        {{ $item->province()->first()->{'name_'.$lang} }} 
                                                        @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif 
                                                        @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                        @if($item->latitude)/ {{ $item->latitude }} @endif 
                                                        @if($item->longitude)/ {{ $item->longitude }} @endif
                                                    </a>
                                                    <ul>
                                                        <li>
                                                            @if($item->status==1)
                                                                {{ trans('global.excluded_description',['status'=> __('global.completed') ]) }}
                                                            @elseif($item->status==2)
                                                                {{ trans('progress.excluded_description',['status'=> __('global.stopped') ]) }}
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        {{-- End of Collpas div --}} 
                    </div>
                </div>
            </div>
        @endif
        <div id="content">
            <div class="form-group pl-3 pr-3 row">
                <div class="col-lg-9">
                    <label class="title-custom pt-3"" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('BQSchedule/list',[$enc_id,encrypt($dep_id),'loc_id']) }}',this.value)" >
                        <option value="">{{ trans('global.select') }}</option>
                        @if($project_location)
                            @foreach($project_location as $item)
                                <option value="{{$item->id}}" <?=$project_location_id==$item->id? 'selected' : '' ?> >{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-3 text-center">
                    <label class="title-custom pt-3" style="color:#575962">&nbsp;</label><br>
                    <div class="m-btn-group m-btn-group--pill btn-group m-btn-group btn-group-sm" role="group" aria-label="Large button group">
                        @if($records->count()==0) 
                            <button type="button" onclick="makeBase('{{ route('BQSchedule.make_base',['pro_id'=>$enc_id,'loc_id'=>encrypt($project_location_id)]) }}','','GET','');" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom"><span><i class="la la-chain"></i> <span>{{ trans('schedule.make_base') }}</span></span></button>
                        @else
                            <button type="button" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom active"><span><i class="la la-chain"></i> <span>{{ trans('schedule.base') }}</span></span></button>
                        @endif                        
                        <button type="button" onclick="relocate_home('{{ route('BQSchedule/list',['id'=>$enc_id,'dep_id'=>encrypt($dep_id),'loc_id'=>$project_location_id]) }}')" class="btn btn-outline-primary m-btn--custom m-btn--icon btn-sm border-custom"><span><i class="la la-map"></i> <span>{{ trans('schedule.working') }}</span></span></button>
                    </div>              
                </div>
            </div>        
            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed p-3">
                <div class="m-portlet__body table-responsive" id="searchresult">
                    @if($records->count()>0)
                        <table class="table table-bordered table-checkable head-has-color">
                            <thead>
                                <tr class="bg-light">
                                    <th width="25%">{{ trans('estimation.operation_type') }}</th>
                                    <th width="7%">{{ trans('estimation.amount') }}</th>
                                    <th width="7%">{{ trans('estimation.total_price') }}</th>
                                    <th width="7%">{{ trans('estimation.percentage') }}</th>
                                    <th width="10%">{{ trans('schedule.start_date') }}</th>
                                    <th width="10%">{{ trans('schedule.end_date') }}</th>
                                    <th width="7%">{{ trans('schedule.duration') }}</th>
                                    <th width="7%">{{ trans('schedule.shift') }}</th>
                                    <th width="7%">{{ trans('schedule.resources') }}</th>
                                    <th width="13%">{{ trans('schedule.progress') }}</th>
                                </tr>
                            </thead>
                            @php
                                $bq_section = 0;
                            @endphp
                            @foreach($records AS $item)
                                @if($bq_section != $item->bq_section_id)
                                    @php
                                        $bq_section = $item->bq_section_id;
                                    @endphp
                                    <tr style="background: #5b99bf4a">
                                        <td colspan="10" class="text-center title-custom">
                                            {{ $item->bq_section->{'name_'.$lang} }}
                                        </td>
                                    <tr>
                                @endif
                                <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                    <tr>
                                        <td>{{ $item->operation_type }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->total_price }}</td>
                                        <td>{{ getPercentage($plan->procurement['contract_price'],$item->total_price) }} %</td>
                                        <td>{{ datecheck($item->start_date,$lang) }}</td>
                                        <td>{{ datecheck($item->end_date,$lang) }}</td>
                                        <td>{{ $item->duration }}</td>
                                        <td>{{ $item->shift }}</td>
                                        <td>{{ $item->resources }}</td>
                                        <td>
                                            <?php
                                            // Get all public holidays
                                            $holidayDays = holidays($project_location_id);
                                            // Get working days only
                                            $workingDays = workingDays($project_location_id);
                                            $total_working_days = working_days($item->start_date, $item->end_date, $workingDays, $holidayDays);
                                            $working = calculate_working($item->start_date, date('Y-m-d'), $workingDays, $holidayDays,$total_working_days);
                                            ?>
                                            <div class="d-flex flex-column w-100 mr-2">
                                                <span class="text-muted mr-2 font-size-sm" style="font-size:1rem;">{{$working}}%</span>
                                                <div class="progress rounded">
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: {{$working}}%;" aria-valuenow="{{$working}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                        @if(!empty($records))
                            {{ $records->links('pagination') }}
                        @endif
                    @else
                        <div class="col-xl-9 col-lg-12">
                            <div class="m-wizard__form">
                                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                    <div class="font-weight-bold text-danger" style="font-size:1.2rem">
                                        <dt>{{ trans('global.no_records') }}</dt>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    @endif
                </div>
            </div>  
        </div>
    </div>
@endsection