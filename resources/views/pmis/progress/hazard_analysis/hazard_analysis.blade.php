@extends('master_window')
@section('head')
    <title>{{ trans('hazard_analysis.hazard_analysis') }}</title>
@endsection
@section('content')
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h2 class="m-portlet__head-text">{{ $plan->name }}</h2>
                        </div> 
                    </div>
                </div>
                <ul class="nav nav-tabs  m-tabs-line m-tabs-line--primary" role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active m-portlet__head-text tab_custome" data-toggle="tab" href="#m_tabs_2_1" role="tab">{{ trans('hazard_analysis.hazard_analysis') }}</a>
                    </li>
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link m-portlet__head-text tab_custome" data-toggle="tab" href="#m_tabs_2_2" role="tab">{{ trans('hazard_analysis.hazard_inspection') }}</a>
                    </li>
                </ul>
                @alert()
                @endalert
                <div class="tab-content">
                    <div class="tab-pane active" id="m_tabs_2_1" role="tabpanel">
                        <div class="m-portlet m-portlet--full-height">
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_1" role="tablist">
                                <!--begin::Item-->
                                    <div class="m-accordion__item m-accordion__item--dark">
                                        <div class="m-accordion__item-head collapsed " srole="tab" id="hazard_analysis" data-toggle="collapse" href="#hazard_analysis_add" aria-expanded="false">
                                            <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('hazard_analysis.add_hazard_analysis') }}</h4></span>
                                            <span class="m-accordion__item-mode"></span>
                                        </div>
                                        <div class="m-accordion__item-body collapse" id="hazard_analysis_add" role="tabpanel" aria-labelledby="hazard_analysis_sub" data-parent="#m_accordion_1" style="">
                                            <div class="m-accordion__item-content">
                                                <form id="hazardAnalysisForm" enctype="multipart/form-data">
                                                    <div class="m-wizard m-wizard--6 m-wizard--brand" id="m_wizard">
                                                        <div class="form-group m-form__group row m-form__group_custom m--padding-bottom-20">
                                                            <div class="col-lg-11">
                                                                <label class="title-custom">{{ trans('hazard_analysis.hazard_analysis_category') }}: <span style="color:red;">*</span></label><br>
                                                                <input class="form-control m-input m-input--air errorDiv required" type="text" name="name[]" id="name" placeholder="{{ trans('global.name') }}"/>
                                                                <div class="name error-div" style="display:none;"></div>
                                                            </div>
                                                            <div class="col-lg-1">
                                                                <label class="title-custom">&nbsp</label><br>
                                                                <button type="button" class="btn btn-primary m-btn m-btn--air btn-sm" onclick="add_more('hazard_analysis_div','{{route('more_hazard_analysis')}}')"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                                            </div>
                                                        </div>
                                                        <div id="hazard_analysis_div"></div><!-- Display more Hazard Analysis  -->
                                                        <input name="bill_quant_id" type="hidden" value="{{ decrypt($bq_id) }}"/>
                                                        <input name="project_id" type="hidden" value="{{ decrypt($pro_id) }}"/>
                                                        <div class="form-group m-form__group row m-form__group_custom">
                                                            <div class="col-lg-12">
                                                                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                                    <div class="m-form__actions m-form__actions--solid">
                                                                        <button type="button" onclick="storeRecord('{{route('hazard_analysis.store')}}','hazardAnalysisForm','POST','div-content',redirectFunction,false,this.id);" id="add" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                                                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                <!--end::Item-->
                                <div id="hazard_analysis_div_btn"></div>
                            </div>
                        </div>
                        <div class="m-portlet m-portlet--full-height" id="div-content">
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
                                <!--begin::Item-->
                                <div class="m-accordion__item m-accordion__item--dark">
                                    <div class="m-accordion__item-head collapsed " srole="tab" id="hazard_analysis_sub" data-toggle="collapse" href="#hazard_analysis_sub_add" aria-expanded="false">
                                        <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('hazard_analysis.add_hazard_analysis_sub') }}</h4></span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="hazard_analysis_sub_add" role="tabpanel" aria-labelledby="hazard_analysis_sub" data-parent="#m_accordion_5">
                                        <div class="m-accordion__item-content">
                                            <form id="hazardAnalysisSubForm" enctype="multipart/form-data">
                                                <div class="m-wizard m-wizard--6 m-wizard--brand" id="m_wizard">
                                                    <div class="form-group m-form__group row m-form__group_custom">
                                                        <div class="col-xl-12 col-lg-12 m--padding-top-10 m--padding-bottom-5">  
                                                            <label class="title-custom">{{ trans('hazard_analysis.hazard_analysis_category') }}: <span style="color:red;">*</></label>
                                                            <div id="hazard_analysis_dropdown" class="errorDiv">
                                                                <select class="form-control m-input m-input--air select-2" name="hazard_analysis_dropdown">
                                                                    <option value="">{{ trans('global.select') }}</option>
                                                                    @if($hazardAnalysis)
                                                                        @foreach($hazardAnalysis as $item)
                                                                            <option value="{{$item->id}}">{{ $item->name }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="hazard_analysis_dropdown error-div" style="display:none;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom">
                                                        <div class="col-xl-12 col-lg-12 m--padding-top-10 m--padding-bottom-5">  
                                                            <label class="title-custom">{{ trans('hazard_analysis.description') }}: <span style="color:red;">*</span></label>
                                                            <input class="form-control m-input m-input--air errorDiv required" type="text" name="name_sub" id="name_sub"/>
                                                            <div class="name_sub error-div" style="display:none;"></div>
                                                        </div>   
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom">
                                                        <div class="col-lg-12 col-lg-12 m--padding-top-10 m--padding-bottom-5">
                                                            <input name="bill_quant_id" type="hidden" value="{{ decrypt($bq_id) }}"/>
                                                            <input name="project_id" type="hidden" value="{{ decrypt($pro_id) }}"/>
                                                            <input name="location_id" type="hidden" value="{{ $locId }}"/>
                                                            <button type="button" onclick="storeRecord('{{route('hazard_analysis/store_sub')}}','hazardAnalysisSubForm','POST','response_div',redirectFunction,false,this.id);" id="add_sub" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                                            <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet m-portlet--full-height">
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
                                <!--begin::Item-->
                                <div class="m-accordion__item m-accordion__item--dark">
                                    <div class="m-accordion__item-head collapse show" srole="tab" id="hazard_analysis_tree" data-toggle="collapse" href="#hazard_analysis_tree" aria-expanded="true">
                                        <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('hazard_analysis.view') }}</h4></span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse show" id="hazard_analysis_tree" role="tabpanel" aria-labelledby="hazard_analysis_tree" data-parent="#m_accordion_5" style="">
                                        <div class="m-accordion__item-content">
                                            <!--begin: Portlet Body-->
                                            <div class="m-portlet__body m-portlet__body--no-padding">
                                                <div class="m-wizard m-wizard--6 m-wizard--brand" id="m_wizard" id="div-content">
                                                    <div id="tree">
                                                        <ul>
                                                            @if($result)
                                                                @foreach($result as $res)
                                                                    <li>{{ $res->name }}
                                                                        @if($res->hazardAnalysisSub)
                                                                            <ul>
                                                                                @foreach($res->hazardAnalysisSub as $item)
                                                                                    <li>{{ $item->description }}</li>
                                                                                @endforeach    
                                                                            </ul>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                    </div>

                    <div class="tab-pane" id="m_tabs_2_2" role="tabpanel">
                        <div class="m-portlet m-portlet--full-height">
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
                                <div class="m-accordion__item m-accordion__item--dark">
                                    <div class="m-accordion__item-head collapsed " srole="tab" id="hazard_analysis_training" data-toggle="collapse" href="#hazard_analysis_training_add" aria-expanded="false">
                                        <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('hazard_analysis.hazard_te_add') }}</h4></span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="hazard_analysis_training_add" role="tabpanel" aria-labelledby="hazard_analysis_training" data-parent="#m_accordion_5" style="">
                                        <div class="m-accordion__item-content ">
                                            <form id="hazardInspectionForm" enctype="multipart/form-data">
                                                <div class="m-wizard m-wizard--6 m-wizard--brand" id="m_wizard">
                                                    <div class="form-group m-form__group row m-form__group_custom m--padding-bottom-20">
                                                        <div class="col-lg-12">
                                                            <label class="title-custom">{{ trans('hazard_analysis.hazard_equipment') }}: <span style="color:red;">*</span></label>
                                                            <textarea class="form-control m-input m-input--air errorDiv required"  rows="2" type="text" id="equipment" name="equipment"></textarea>
                                                            <div class="equipment error-div" style="display:none;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom m--padding-bottom-20">
                                                        <div class="col-lg-12">
                                                            <label class="title-custom">{{ trans('hazard_analysis.hazard_training') }}: <span style="color:red;">*</span></label>
                                                            <textarea class="form-control m-input m-input--air errorDiv required" rows="2" type="text" name="training" id="training"></textarea>
                                                            <div class="training error-div" style="display:none;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row m-form__group_custom">
                                                        <div class="col-lg-12">
                                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                                <div class="m-form__actions m-form__actions--solid">
                                                                    <input name="bill_quant_id" type="hidden" value="{{ decrypt($bq_id) }}"/>
                                                                    <input name="project_id" type="hidden" value="{{ decrypt($pro_id) }}"/>
                                                                    <input name="pro_location_id" type="hidden" value="{{ $locId }}"/>
                                                                    <button type="button" onclick="storeRecord('{{route('store_hazard_inspection')}}','hazardInspectionForm','POST','response_div',redirectFunction,false,this.id);" id="add_inspection" class="btn btn-primary">{{ trans('global.submit') }}</button>
                                                                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet m-portlet--full-height">
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
                                <div class="m-accordion__item m-accordion__item--dark">
                                    <div class="m-accordion__item-head collapsed " srole="tab" id="hazard_inspection" data-toggle="collapse" href="#hazard_inspection_view" aria-expanded="true">
                                        <span class="m-accordion__item-title"><h4 class="m-portlet__head-text">{{ trans('hazard_analysis.hazard_te_view') }}</h4></span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body " id="hazard_inspection_view" role="tabpanel" aria-labelledby="hazard_inspection" data-parent="#m_accordion_5" style="">
                                        <div class="m-accordion__item-content ">
                                            <div class="form-group m-form__group row m-form__group_custom m--padding-bottom-20">
                                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                                    <thead>
                                                        <tr class="bg-dark text-white">
                                                            <th width="10%">{{ trans('global.number') }}</th>
                                                            <th width="45%">{{ trans('hazard_analysis.hazard_equipment') }}</th>
                                                            <th width="45%">{{ trans('hazard_analysis.hazard_training') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                                        @if($hazard_inspection)
                                                            @foreach($hazard_inspection as $item)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{ $item->equipment }}</td>
                                                                    <td>{{ $item->training }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('js-code')
  <script type="text/javascript">
    $(document).ready (function() {
        $('#tree').jstree();
    });
  </script>
@endsection 