<div id="hazard_analysis_{!!$number!!}" class="form-group m-form__group row m-form__group_custom m--padding-bottom-20">
    <div class="col-lg-11">
        <label class="title-custom">&nbsp</label><br>
        <input class="form-control m-input m-input--air errorDiv required" type="text" name="name[]" id="name_{!!$number!!}" placeholder="{{ trans('global.name') }}"/>
        <div class="name_{!!$number!!} error-div" style="display:none;"></div>
    </div>
    <div class="col-lg-1">
        <label class="title-custom">&nbsp</label><br>
        <button type="button"id="hazard_analysis_div_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="confirmRemoveMore('hazard_analysis_{!!$number!!}',{!!$number!!},'hazard_analysis_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
    </div>
</div>