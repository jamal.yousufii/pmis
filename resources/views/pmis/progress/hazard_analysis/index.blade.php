@extends('master')
@section('head')
    <title>{{ trans('hazard_analysis.hazard_analysis') }}</title>
@endsection
@section('content')
    <div class="row mt-1">
        <div class="col-lg-12 pb-1">
            <div class="col-lg-6 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{trans('global.project_summary')}}</div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-box m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('project_dashboard.project' )}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{$plan->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-symbol m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{ trans('monitoring.contractor') }}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ $plan->procurement->company }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-2 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('progress.contraction_duration')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{ differenceBetweenDate($plan->procurement->start_date,$plan->procurement->end_date,'%a',true) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                <div class="m-widget21">
                    <div class="row">
                        <div class="col">
                            <div class="m-widget21__item proj_sum_wdg">
                                <span class="m-widget21__icon">
                                    <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="fa flaticon-calendar-3 m--font-light"></i>
                                    </a>
                                </span>
                                <div class="m-widget21__info">
                                    <span class="m-widget21__title">{{trans('project_dashboard.contract_amount')}}</span><br>
                                    <span class="m-widget21__sub m-widget21__sub1">{{number_format($plan->procurement->contract_price)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile mb-2" id="showContent">
        <div class="m-portlet__head table-responsive border-0">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand border-0 mb-0" role="tablist">
                        @if($tabs)
                            @foreach($tabs as $item)
                                @if(check_my_section(array($dep_id),$item->code))
                                    <li class="nav-item m-tabs__item">
                                        <a href="{{ route($item->url_route, ['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" class="nav-link m-tabs__link @if(session('current_tab')==$item->code) active @endif">
                                            <span class="m-portlet__head-text tab_custome"><strong>{!! $item->name !!}</strong></span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div> 
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a class="btn btn-secondary m-btn--custom m-btn--icon btn-sm" href="{{ route('progress',encrypt($dep_id)) }}">
                            <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        @if($excluded_locations->count()>0)
            <div class="accordion row mx-1 mt-1">
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                    <div class="m-alert__text text-dark bg-warning">
                        <div>
                            <i class="la la-warning text-dark"></i> &nbsp; {{ trans('progress.excluded_projects') }}: 
                            <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                <span><i class="la la-angle-double-down"></i></span>
                            </a>
                        </div>
                        <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                            <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2 bg-warning">
                                <div class="m-portlet__body bg-warning">
                                    <div class="m-portlet__body table-responsive bg-warning">
                                        <ul class="list-group list-group-flush bg-warning">
                                            @foreach($excluded_locations as $item)
                                                <li>
                                                    @if($item->status==1 and check_my_section(array($dep_id),'pmis_completed'))
                                                    <a href="{{ route('completed.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                    @elseif($item->status==2 and check_my_section(array($dep_id),'pmis_stopped'))
                                                    <a href="{{ route('stopped.projects',['id'=>$enc_id,'dep_id'=>encrypt($dep_id)]) }}" target="_blank">
                                                    @endif
                                                        {{ $loop->iteration }} - 
                                                        {{ $item->province()->first()->{'name_'.$lang} }} 
                                                        @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif 
                                                        @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                        @if($item->latitude)/ {{ $item->latitude }} @endif 
                                                        @if($item->longitude)/ {{ $item->longitude }} @endif
                                                    </a>
                                                    <ul>
                                                        <li>
                                                            @if($item->status==1)
                                                                {{ trans('global.excluded_description',['status'=> __('global.completed') ]) }}
                                                            @elseif($item->status==2)
                                                                {{ trans('progress.excluded_description',['status'=> __('global.stopped') ]) }}
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        {{-- End of Collpas div --}} 
                    </div>
                </div>
            </div>
        @endif
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed code notranslate cssHigh collapse show" id="collapseDiv"> 
            <div class="form-group pl-3 pr-3 pb-3 row">
                <div class="col-lg-12">
                    <label class="title-custom pt-3" style="color:#575962">{{ trans('global.by_bill') }}:</label><br>
                    <select class="form-control m-input m-input--air select-2 required" name="project_location_id" onchange="requestPage('{{ route('hazard_analysis/list',[$enc_id,encrypt($dep_id),'loc_id']) }}',this.value)" >
                        <option value="">{{ trans('global.select') }}</option>
                        @if($project_location)
                            @foreach($project_location as $item)
                                <option value="{{$item->id}}">{{ $item->province->{'name_'.$lang} }} @if($item->district)/ {{$item->district->{'name_'.$lang} }} @endif @if($item->village)/ {{$item->village->{'name_'.$lang} }} @endif @if($item->latitude)/ {{ $item->latitude }} @endif @if($item->longitude)/ {{ $item->longitude }} @endif</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
@endsection