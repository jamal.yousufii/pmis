
@extends('master')
@section('head')
    <title>{{ trans('global.project_status_dashboard') }}</title>
    <style>
        .flex-stack {
            justify-content: space-between;
            align-items: center;
        }
        .me-2 {
            margin-right: .5rem!important;
        }
        .align-items-center {
            align-items: center!important;
        }
        .d-flex {
            display: flex!important;
        }
        .symbol {
            display: inline-block;
            flex-shrink: 0;
            position: relative;
            border-radius: .475rem;
        }
        .me-3 {
            margin-right: .75rem!important;
        }

        .badge-light {
            color: #7e8299;
            background-color: #f5f8fa;
        }
        .fw-bold {
            font-weight: 500!important;
        }
        .border-item {
            border-bottom-width: 1px !important;
            border-bottom-style: dashed !important;
            border-bottom-color: #eff2f5 !important;
        }
        .zone-link :hover{
            /* background: #f1f4f5 !important; */
            text-decoration: none !important;
        }
    </style>
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-page__container" id="DivIdToPrint">
        <div class="m-grid__item m-grid__item--fluid m-wrapper mb-0">
            <!-- Page title: Start -->
            <div class="m-subheader pt-1">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="{{ route('home.dashboard') }}" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon fas fa-home"></i>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="{{ route('home',session('current_mod')) }}" class="m-nav__link">
                                    <span class="m-subheader__title m-nav__link-text" style="font-family: B Nazanin; font-size:1.5rem">{{ trans('global.project_status_dashboard') }}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <span class="m-subheader__daterange">
                            <span class="m-subheader__daterange-label">
                                <span class="title-custom">{{ trans('global.search') }}</span>
                            </span>
                            <a data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="true" aria-controls="collapseFilter" class="btn btn-sm btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                            </a>
                        </span>
                        @if (Auth::user()->is_dashboard==0)
                            <span class="mx-1">
                                <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon">
                                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back_home') }}</span></span>
                                </a>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Page title: End -->

            <!-- Filter: Start -->
            <div class="code notranslate cssHigh collapse py-0" id="collapseFilter">
                <div class="m-content py-0 pt-3">
                    <div class="m-portlet py-0 my-0"">
                        <div class="m-portlet__body m-portlet__body--no-padding">
                            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="filterForm">
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row m-form__group_custom">
                                        <div class="col-xl-5 col-lg-5 col-xs-5 col-md-5 col-sm-12">
                                            <div id="div_zone" class="errorDiv">
                                                <label class="title-custom">{{ trans('home.zone') }}: <span style="color:red;">*</span></label>
                                                <select class="form-control select-2 required" name="zone[]" id="zone" multiple onchange="provinceByZone();">
                                                    <option value="-" selected>{{ trans('global.all') }}</option>
                                                    @if($zone)
                                                        @foreach ($zone as $item)
                                                            <option value="{{ $item->zcode }}">{{ $item->zname }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="zone error-div" style="display:none;"></div>
                                        </div>
                                        <div class="col-xl-5 col-lg-5 col-xs-5 col-md-5 col-sm-12">
                                            <label class="title-custom">{{ trans('global.province') }}:</label>
                                            <select class="form-control select-2 required" name="province[]" id="province" multiple>
                                                <option value="-" selected>{{ trans('global.all') }}</option>
                                                @if($provinces)
                                                    @foreach ($provinces as $item)
                                                        <option value="{{ $item->id }}">{{ $item->{'name_'.$lang} }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-2 col-xs-2 col-md-2 col-sm-12">
                                            <div class="m-input-icon m-input-icon--left">
                                                <label class="title-custom">&nbsp;</label><br>
                                                <a href="javascript:void;" onclick="storeData('{{route('home.dashboardByFilter')}}','filterForm','POST','filter_result',put_content,true)" class="btn btn-primary m-btn m-btn--custom btn-sm m-btn--icon m-btn--air" onclick="">
                                                    <span><i class="la la-search"></i><span>{{ trans('global.search') }}</span></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Filter: End -->
            <div id="filter_result">
                <!-- Locations without schedules: Start -->
                @if($excluded_locations->count()>0)
                    <div class="accordion m-content m-1 pb-0">
                        <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                            <div class="m-alert__text text-dark bg-warning">
                                <div>
                                    <i class="la la-warning text-dark"></i> &nbsp; {{ trans('portfolios.excluded_projects') }}:
                                    <a class="float-right" id="collapsBtn" data-toggle="collapse" href="#collapseShareDiv" role="button" aria-expanded="true" aria-controls="collapseShareDiv">
                                        <span><i class="la la-angle-double-down"></i></span>
                                    </a>
                                </div>
                                <div class="code notranslate cssHigh collapse bg-warning" id="collapseShareDiv">
                                    <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed pt-2">
                                        <div class="m-portlet__body">
                                            <div class="m-portlet__body table-responsive" style="max-height:600px">
                                                <ul class="list-group list-group-flush">
                                                    @foreach($excluded_locations as $item)
                                                        <li class="list-group-item">
                                                            {{ $loop->iteration }} - <span>{{$item->Projects->name}}</span>
                                                            <ul><li>
                                                                {{ $item->province()->first()->{'name_'.$lang} }}
                                                                @if($item->district_id)/ {{ $item->district()->first()->{'name_'.$lang} }} @endif
                                                                @if($item->village_id)/ {{ $item->village()->first()->{'name_'.$lang} }} @endif
                                                                @if($item->latitude)/ {{ $item->latitude }} @endif
                                                                @if($item->longitude)/ {{ $item->longitude }} @endif
                                                                <ul><li>{{ trans('portfolios.excluded_baseline') }}</li></ul>
                                                            </li></ul>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- End of Collpas div --}}
                            </div>
                        </div>
                    </div>
                @endif
                <!-- Locations without schedules: End -->

                <!-- Project summary: Start -->
                <div class="row m-content pb-1">
                    <div class="col-lg-12">
                        <div class="col-lg-6 text-title-custom breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers px-2"></span>{{ trans('home.project_info_summary') }}</div>
                    </div>
                    <div class="col-lg-4">
                        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon">
                                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                                    <i class="fa flaticon-box m--font-light"></i>
                                                </a>
                                            </span>
                                            <div class="m-widget21__info">
                                                <span class="m-widget21__title">{{ trans('report.total_projects') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ $projects[1]['total']+$projects[2]['total']+$projects[3]['total'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon">
                                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                                    <i class="fas fa-money-bill-alt m--font-light"></i>
                                                </a>
                                            </span>
                                            <div class="m-widget21__info">
                                                <span class="m-widget21__title">{{ trans('report.total_budget') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ $projects[1]['price']+$projects[2]['price']+$projects[3]['price'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit ">
                            <div class="m-widget21">
                                <div class="row">
                                    <div class="col">
                                        <div class="m-widget21__item proj_sum_wdg">
                                            <span class="m-widget21__icon">
                                                <a href="javascript:void()" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                                    <i class="fas fa-chart-bar m--font-light"></i>
                                                </a>
                                            </span>
                                            <div class="m-widget21__info">
                                                <span class="m-widget21__title">{{ trans('report.total_progress') }}</span><br>
                                                <span class="m-widget21__sub m-widget21__sub1">{{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_high+$physical_progress_medium+$physical_progress_low) }}%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Project summary: End -->

                <!-- Content: Start -->
                <div class="m-content pt-0">
                    <!-- Projects by priority: Start -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-title-custom breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers px-2"></span>{{ trans('global.project_status_priority') }}</div>
                    <div class="m-portlet">
                        <div class="m-portlet__body m-portlet__body--no-padding">
                            <div class="row m-row--no-padding m-row--col-separator-xl">
                                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                                    <div class="m-widget24">
                                        <div class="m-widget24__item">
                                            <h4 class="m-widget24__title">{{ trans('report.high') }}</h4><br>
                                            <span class="m-widget24__desc">{{ trans('report.high_desc') }}</span>
                                            <span class="m-widget24__stats m--font-danger">{{ number_format($projects[1]['price'], 3) }} {{ trans('completed.afghani') }}</span>
                                            <div class="m--space-10"></div>
                                            <div class="progress m-progress--sm">
                                                <div class="progress-bar m--bg-danger" role="progressbar" style="width: {{ getPercentage($projects[1]['total']+$projects[2]['total']+$projects[3]['total'],$projects[1]['total']) }}%;" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="m-widget24__change mb-1">{{ trans('home.number_of_projects') }}</span>
                                            <span class="m-widget24__number mb-1">{{ $projects[1]['total'] }}</span>
                                        </div>
                                    </div>
                                    <div class="m-widget24">
                                        <div class="m-widget24__item row ">
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{ trans('report.physical_progress') }}</div>
                                                <div class="col-lg-12 mt-3">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-dark text-white">
                                                                <th class="p-1 text-center title-custom" width="40">{{ trans('global.status') }}</th>
                                                                <th class="p-1 text-center title-custom" width="60">{{ trans('report.progress_percentage') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#34bfa3"></button> {{ trans('report.work_done') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_high) }}%</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#f9c859"></button> {{ trans('report.work_remained') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_high) }}%</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12"">
                                                <div id="high_priority" style="width: 100%; height:300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                                    <div class="m-widget24">
                                        <div class="m-widget24__item">
                                            <h4 class="m-widget24__title">{{ trans('report.medium') }}</h4><br>
                                            <span class="m-widget24__desc">{{ trans('report.medium_desc') }}</span>
                                            <span class="m-widget24__stats m--font-success">{{ number_format($projects[2]['price'], 3) }} {{ trans('completed.afghani') }}</span>
                                            <div class="m--space-10"></div>
                                            <div class="progress m-progress--sm">
                                                <div class="progress-bar m--bg-success" role="progressbar" style="width: {{ getPercentage($projects[1]['total']+$projects[2]['total']+$projects[3]['total'],$projects[2]['total']) }}%;" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="m-widget24__change mb-1">{{ trans('home.number_of_projects') }}</span>
                                            <span class="m-widget24__number mb-1">{{ $projects[2]['total'] }}</span>
                                        </div>
                                    </div>
                                    <div class="m-widget24">
                                        <div class="m-widget24__item row ">
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{ trans('report.physical_progress') }}</div>
                                                <div class="col-lg-12 mt-3">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-dark text-white">
                                                                <th class="p-1 text-center title-custom" width="40">{{ trans('global.status') }}</th>
                                                                <th class="p-1 text-center title-custom" width="60">{{ trans('report.progress_percentage') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#34bfa3"></button> {{ trans('report.work_done') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_medium) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#f9c859"></button> {{ trans('report.work_remained') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_medium) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div id="medium_priority" style="width: 100%; height:300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                                    <div class="m-widget24">
                                        <div class="m-widget24__item">
                                            <h4 class="m-widget24__title">{{ trans('report.low') }}</h4><br>
                                            <span class="m-widget24__desc">{{ trans('report.low_desc') }}</span>
                                            <span class="m-widget24__stats m--font-info">{{ number_format($projects[3]['price'], 3) }} {{ trans('completed.afghani') }}</span>
                                            <div class="m--space-10"></div>
                                            <div class="progress m-progress--sm">
                                                <div class="progress-bar m--bg-info" role="progressbar" style="width: {{ getPercentage($projects[1]['total']+$projects[2]['total']+$projects[3]['total'],$projects[3]['total']) }}%;" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="m-widget24__change mb-1">{{ trans('home.number_of_projects') }}</span>
                                            <span class="m-widget24__number mb-1">{{ $projects[3]['total'] }}</span>
                                        </div>
                                    </div>
                                    <div class="m-widget24">
                                        <div class="m-widget24__item row ">
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div class="col-lg-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers mx-2"></span>{{ trans('report.physical_progress') }}</div>
                                                <div class="col-lg-12 mt-3">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-dark text-white">
                                                                <th class="p-1 text-center title-custom" width="40">{{ trans('global.status') }}</th>
                                                                <th class="p-1 text-center title-custom" width="60">{{ trans('report.progress_percentage') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#34bfa3"></button> {{ trans('report.work_done') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_low) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="p-1" width="50%"><button class="btn btn-sm" style="background:#f9c859"></button> {{ trans('report.work_remained') }}</td>
                                                                <td class="p-1 text-center" width="50%">{{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_low) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div id="low_priority" style="width: 100%; height:300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Projects by priority: End -->

                    <!-- Projects by filter: Start -->
                    <div class="row">
                        <!-- Projects by provinces: Start -->
                        <div class="col-lg-3 col-lg-3 col-md-12 col-sm-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers px-2"></span>{{ trans('home.project_by_zone') }}</div>
                        <div class="col-lg-9 col-lg-9 col-md-12 col-sm-12 text-title-custom px-2 breadcrumb_nav__head-text" style="color: #575962;"><span class="fa flaticon-layers px-2"></span>{{ trans('report.project_status')}}</div>
                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                            <div class="m-portlet mb-5 mb-xl-8">
                                <div class="card-body d-flex flex-column" style="position: relative;">
                                    <div class="mt-5">
                                        @if($zone)
                                            @foreach ($zone as $item)
                                                <a href="javascript:void()" onclick="showModalContent('{{route('home.projectsByZone')}}','zone={{$item->zcode}}&all_locations={{implode(',',$all_locations)}}','POST','zone_content','zone_title','{{$item->zname}}')" class="zone-link" data-toggle="modal" data-target="#project_by_zone" id="list_owner_btn">
                                                    <div class="d-flex flex-stack mb-2 border-item">
                                                        <div class="d-flex align-items-center me-2" style="width: 80%">
                                                            <div class="symbol symbol-50px me-3">
                                                                <div class="symbol-label bg-light p-3"><i class="fas fa-map-marked-alt h-50"></i></div>
                                                            </div>
                                                            <div class="px-2">
                                                                <span class="fs-6 text-gray-800 text-hover-primary fw-bolder title-custom">{{ trans('home.zone') }} {{ $item->zname }}</span>
                                                                <div class="fs-7 text-muted fw-bold mt-1">
                                                                    @php
                                                                        $provinces = getProvinceByZone($item->zcode);
                                                                        $prov = "";
                                                                        if($provinces){
                                                                            foreach ($provinces as $pro) {
                                                                                $prov .= $pro->name.', ';
                                                                            }
                                                                        }
                                                                        echo rtrim($prov, ", ");
                                                                    @endphp
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="badge badge-light fw-bold py-2 title-custom" style="width: 20%">{{ $zones[$item->zcode] }}</div>
                                                    </div>
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Projects by provinces: End -->
                        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12">
                            <div class="m-portlet">
                                <div class="m-portlet__body" style="min-height:700px;">
                                    <div class="row">
                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                            <table class="table table-bordered mt-5">
                                                <thead>
                                                    <tr class="bg-dark text-white">
                                                        <th class="p-1 text-center title-custom" width="20">{{ trans("plans.status") }}</th>
                                                        <th class="p-1 text-center title-custom" width="80">{{ trans("global.tedad") }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($project_spi as $item=>$value)
                                                        <tr>
                                                            @if ($item == 'good')
                                                                <td class="p-1 text-success title-custom"><button class="btn btn-sm btn-success"></button>&nbsp;{{ trans("report.good_condition") }}</td>
                                                            @elseif ($item == 'normal')
                                                                <td class="p-1 text-success title-custom"><button class="btn btn-sm btn-warning"></button>&nbsp;{{ trans("report.normal") }}</td>
                                                            @elseif ($item == 'risk')
                                                                <td class="p-1 text-success title-custom"><button class="btn btn-sm btn-danger"></button>&nbsp;{{ trans("report.at_risk") }}</td>
                                                            @endif
                                                            <td class="p-1 text-center text-success title-custom">{{ $value }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-xl-9 col-lg-9 col-md-6 col-sm-12">
                                            <div id="projects_status" style="width: 100%; height:600px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Projects by filter: End -->
                </div>
                <!-- Content: End -->
            </div>
        </div>
    </div>
    <!-- Project by zone: Start -->
    <div id="project_by_zone" class="modal fade" role="dialog" aria-labelledby="project_by_zone" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <span class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon fas fa-home"></i></span>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item px-1">
                            <span class="m-subheader__title m-nav__link-text" style="font-family: B Nazanin; font-size:1.5rem">{{ trans('global.project_status_dashboard') }}</span>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item px-1">
                            <span class="m-subheader__title m-nav__link-text" style="font-family: B Nazanin; font-size:1.5rem" id="zone_title">{{ trans('home.zone') }}</span>
                        </li>
                    </ul>
                    <button type="button" class="close p-4" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ki ki-close"></i></button>
                </div>
                <div class="modal-body" id="zone_content">
                </div>
            </div>
        </div>
    </div>
    <!-- Project by zone: End -->

@endsection
@section('js-code')
    <script type="text/javascript">
        /**
         * @Desc: Get all selected zones from dropdown list
         */
        function provinceByZone(){
            var values = $('#zone').val();
            viewRecord('{{route('home.bringProvinces')}}','id='+values,'POST','province');
        }

        function showModalContent(url,params,method,result_div,title,val)
        {
            var zone = '{{ trans('home.zone') }}';
            $('#'+result_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('public/img/loader.gif')!!}" /></div>');
            $('#'+title).html(zone+" "+val);
            $.ajax({
                url: url,
                data:params,
                type:method,
                success: function(response)
                {
                    $('#'+result_div).html(response);
                },
                complete: function(){

                },
                cache: false,
                processData: false
            })
        }

        /**
         * @Desc: Graph data
         */
        $(document).ready(function(){
            var high_priority = echarts.init(document.getElementById('high_priority'));
            high_priority_option = {
                tooltip: {
                    borderColor: null,
                    borderThickness: 2,
                    cornerRadius: 5,
                    fontSize: 14,
                    fontColor: "black",
                    fontFamily: "B Nazanin",
                    fontWeight: "normal",
                    fontStyle: "italic",
                },
                series: [
                    {
                        name: '{{ trans('report.physical_progress') }}',
                        fontFamily: 'B Nazanin',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 30,
                            borderColor: '#fff',
                            borderWidth: 1
                        },
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '20',
                                fontWeight: 'bold',
                                fontFamily: 'B Nazanin'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: [
                            {
                                value: {{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_high) }},
                                name: '{{ trans('report.work_done') }}',
                                itemStyle: {color: '#34bfa3',opacity: 0.8},
                            },
                            {
                                value: {{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_high) }},
                                name: '{{ trans('report.work_remained') }}',
                                itemStyle: {color: '#f9c859'},
                            },
                        ],

                    }
                ]
            };
            high_priority.setOption(high_priority_option);

            var medium_priority = echarts.init(document.getElementById('medium_priority'));
            medium_priority_option = {
                tooltip: {
                    trigger: 'item'
                },
                series: [
                    {
                        name: '{{ trans('report.physical_progress') }}',
                        fontFamily: 'B Nazanin',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 30,
                            borderColor: '#fff',
                            borderWidth: 1
                        },
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '20',
                                fontWeight: 'bold',
                                fontFamily: 'B Nazanin'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: [
                            {
                                value: {{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_medium) }},
                                name: '{{ trans('report.work_done') }}',
                                itemStyle: {color: '#34bfa3',opacity: 0.8},
                            },
                            {
                                value: {{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_medium) }},
                                name: '{{ trans('report.work_remained') }}',
                                itemStyle: {color: '#f9c859',opacity: 0.8},
                            },
                        ],

                    }
                ]
            };
            medium_priority.setOption(medium_priority_option);

            var low_priority = echarts.init(document.getElementById('low_priority'));
            low_priority_option = {
                tooltip: {
                    trigger: 'item'
                },
                series: [
                    {
                        name: '{{ trans('report.physical_progress') }}',
                        fontFamily: 'B Nazanin',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 30,
                            borderColor: '#fff',
                            borderWidth: 1
                        },
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '20',
                                fontWeight: 'bold',
                                fontFamily: 'B Nazanin'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: [
                            {
                                value: {{ getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_low) }},
                                name: '{{ trans('report.work_done') }}',
                                itemStyle: {color: '#34bfa3',opacity: 0.8},
                            },
                            {
                                value: {{ 100-getPercentage($projects[1]['price']+$projects[2]['price']+$projects[3]['price'],$physical_progress_low) }},
                                name: '{{ trans('report.work_remained') }}',
                                itemStyle: {color: '#f9c859',opacity: 0.8},
                            },
                        ],

                    }
                ]
            };
            low_priority.setOption(low_priority_option);

            var projects_status = echarts.init(document.getElementById('projects_status'));
            projects_status_option = {
                tooltip: {
                    trigger: 'item',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                xAxis: {
                    type: 'category',
                    data: ['{{ trans("report.good_condition") }}', '{{ trans("report.normal") }}', '{{ trans("report.at_risk") }}']
                },
                yAxis: {
                    type: 'value'
                },
                series: [{

                    data: [
                        @foreach ($project_spi as $item=>$value)
                            {
                                value: {{ $value }},
                                itemStyle: {
                                    @if ($item == 'good')
                                        color: '#34bfa3'
                                    @elseif ($item == 'normal')
                                        color: '#ffb822'
                                    @elseif ($item == 'risk')
                                        color: '#f4516c'
                                    @endif
                                }
                            },
                        @endforeach
                    ],
                    type: 'bar',
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(180, 180, 180, 0.2)'
                    }
                }]


            };
            projects_status.setOption(projects_status_option);
        });
    </script>
@endsection

