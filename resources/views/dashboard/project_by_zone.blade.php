
    <div class="m-portlet__body table-responsive" style="max-height:700px; ">
        <table class="table table-bordered table-hover table-checkable">
            <thead class="bg-light">
                <tr class="font-title">
                    <th width="5%" class="text-center">{{ trans('global.number') }}</th>
                    <th width="15%">{{ trans('global.pro_urn') }}</th>
                    <th width="30%">{{ trans('plans.project_name') }}</th>
                    <th width="25%">{{ trans('report.location') }}</th>
                    <th width="15%">{{ trans('report.departments') }}</th>
                    <th width="10%">{{ trans('report.activity_progress') }}</th>
                </tr>
            </thead>
            <body>
                @if($records)
                    @php
                        // Get all locations in array
                        $locations = $records->pluck('id')->toArray();
                        // Get total price of project by all locations
                        $getTotalPriceByLocations = getTotalPriceByLocations($locations);
                        // Calcualte actual budget done from project start to end
                        $getOneLocationBudgetDone = getOneLocationBudgetDone($locations);
                    @endphp
                    @foreach($records as $item)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td>{{ $item->Projects->urn }}</td>
                            <td>{{ $item->Projects->name }}</td>
                            <td>{{ $item->province->{'name_'.$lang} }}@if($item->district)/ {{ $item->district->{'name_'.$lang} }}@endif @if($item->village)/ {{ $item->village->{'name_'.$lang} }}@endif</td>
                            <td>{{ $item->Projects->departments->{'name_'.$lang} }}</td>
                            <td>
                                @php
                                $total_price    = 0;
                                $total_done     = 0;
                                $activity_done  = 0;
                                // Get total price of location
                                if(isset($getOneLocationBudgetDone[$item->id])){
                                    $total_price = $getTotalPriceByLocations[$item->id];
                                }
                                // Get actual budget done from location start to end
                                if(isset($getOneLocationBudgetDone[$item->id])){
                                    $total_done = $getOneLocationBudgetDone[$item->id];
                                }
                                // Set default value
                                $activity_done = 0;
                                if($total_price > 0){
                                    $activity_done = getPercentage($total_price,$total_done,true);
                                }
                                echo $activity_done;
                                @endphp
                            </td>
                        </tr>
                    @endforeach
                @endif
            </body>
        </table>
    </div>
