@if($provinces)
    <option value="-" selected>{{ trans('global.all') }}</option>
    @foreach($provinces as $item)
        <option value="{{ $item->id }}">{{ $item->{'name_'.$lang} }}</option>
    @endforeach
@endif
