@if(!isset($form_buttons))
<button type="submit" class="btn btn-primary">
    @if (strpos(Route::currentRouteName(), 'create'))
        {{localize('global.save')}}

    @else
        {{localize('global.edit')}}
    @endif
</button>
<button type="reset" class="btn btn-secondary">{{trans('global.delete')}}</button>
@endif
<a href="{{URL::previous()}}" class="btn btn-danger">{{trans('global.cancel')}}</a>
