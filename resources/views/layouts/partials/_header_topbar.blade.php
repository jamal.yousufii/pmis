<a href="{{ route('home',session('current_mod')) }}" class="m-brand__logo-wrapper">
	<!-- BEGIN: Logo -->
	<div class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
		<div class="m-stack__item m-stack__item--middle m-brand__logo p-1 px-3">
			<img alt="" src="{!!asset('public/img/logo.png')!!}" width="60px;"/>
		</div>
	</div>
</a>
<!-- BEGIN: Horizontal Menu -->
<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
	<ul class="m-menu__nav ">
		<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
			<a href="javascript:;" class="m-menu__link m-menu__toggle">
				<i class="m-menu__link-icon la la-gear"></i>
				<span class="m-menu__link-text m-menu__link-text-custom">{!!session('current_mod_name')!!}</span>
				<i class="m-menu__hor-arrow la la-angle-down"></i>
				<i class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
				<ul class="m-menu__subnav">
					@php $modules = get_user_modules(); @endphp
					@if($modules)
						@foreach($modules as $mod)
							<li class="m-menu__item " aria-haspopup="true">
								<a href="{!! route($mod->url,$mod->code) !!}" class="m-menu__link ">
									<i class="m-menu__link-icon la la-gear"></i>
									<span class="m-menu__link-text m-menu__link-text-custom">{!! $mod->{'name_'.$lang} !!}</span>
								</a>
							</li>
						@endforeach
					@endif

				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
			<a href="javascript:;" class="m-menu__link m-menu__toggle">
				<i class="m-menu__link-icon flaticon-share"></i>
				<span class="m-menu__link-text m-menu__link-text-custom">{{ get_language_name($lang) }}</span>
				<i class="m-menu__hor-arrow la la-angle-down"></i>
				<i class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
				<ul class="m-menu__subnav">
					<li class="m-menu__item " aria-haspopup="true">
						<a href="{{ route('language','en') }}" class="m-menu__link ">
							<i class="m-menu__link-icon"><img alt="" src="{!!asset('public/img/en.jpg')!!}" width="15" /></i>
							<span class="m-menu__link-text m-menu__link-text-custom">{{ trans('home.english') }}</span>
						</a>
					</li>
					<li class="m-menu__item " aria-haspopup="true">
						<a href="{{ route('language','dr') }}" class="m-menu__link ">
							<i class="m-menu__link-icon"><img alt="" src="{!!asset('public/img/dr.jpg')!!}" width="15" /></i>
							<span class="m-menu__link-text m-menu__link-text-custom">{{ trans('home.dari') }}</span>
						</a>
					</li>
					<li class="m-menu__item " aria-haspopup="true">
						<a href="{{ route('language','pa') }}" class="m-menu__link ">
							<i class="m-menu__link-icon"><img alt="" src="{!!asset('public/img/dr.jpg')!!}" width="15" /></i>
							<span class="m-menu__link-text m-menu__link-text-custom">{{ trans('home.pashto') }}</span>
						</a>
					</li>
				</ul>
			</div>
		</li>
	</ul>

</div>
<!-- END: Horizontal Menu -->
<!-- BEGIN: Topbar -->
<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
	<div class="m-stack__item m-topbar__nav-wrapper">
		<ul class="m-topbar__nav m-nav m-nav--inline">
			{{-- Notifications Start --}}
			<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
				<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
					<span class="m-nav__link-badge m-badge m-badge--warning">{!!count(Auth::user()->unreadNotifications)!!}</span>
					<span class="m-nav__link-icon">
						<span class="m-nav__link-icon-wrapper">
							<i class="flaticon-alarm"></i>
						</span>
					</span>
				</a>
				<div class="m-dropdown__wrapper">
					<?php
					$leftRightClass = "m-dropdown__arrow--left";
					if($lang=="en"){
						$leftRightClass = "m-dropdown__arrow--center";
					}
					?>
					<span class="m-dropdown__arrow {{ $leftRightClass }}"></span>
					<div class="m-dropdown__inner">
						<div class="m-dropdown__header m--align-center">
							<span class="m-dropdown__header-subtitle text-dark m-menu__link-text-custom">{{ trans('home.notification') }}</span>
						</div>
						<hr>
						<div class="m-dropdown__body">
							<div class="m-dropdown__content m-dropdown__arrow--right">
								<div class="tab-content">
									<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
										<div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
											<div class="m-list-timeline m-list-timeline--skin-light">
												<div class="m-list-timeline__items p-3">
                                                    @foreach(Auth::user()->unreadNotifications as $notify)
                                                        <div class="m-list-timeline__item">
                                                            <a class="link" onclick="readNotifications('{!!$notify->id!!}')" href="{!!$notify->data['redirect_url']!!}">
                                                                <span class="m-list-timeline__text" style="font-size: 1.15rem">{{ trans($notify->data['message']) }}</span>
                                                            </a>
                                                            <span class="m-list-timeline__time">{!!dateDifference($notify->created_at,'%a',false)!!}</span>
                                                        </div>
                                                    @endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
			{{-- Notifications End --}}
			{{-- Flag Start --}}
			<li class="m-nav__item">
				<span class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
					<span class="m-nav__link-icon">
						<span class="m-nav__link-icon-wrapper">
							<img alt="" src="{!!asset('public/img/dr.jpg')!!}" width="30px;"/>
						</span>
					</span>
				</span>
			</li>
			{{-- Flag End --}}
			{{-- User Profile Start --}}
			<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
				<a href="#" class="m-nav__link m-dropdown__toggle">
					<span class="m-topbar__welcome"></span>
					<span class="m-topbar__username kt-menu__link-text text-default m-menu__link-text-custom">{{ Auth::user()->name }}</span>&nbsp;&nbsp;
					<span class="m-topbar__userpic">
					<img src="{!!asset('public/attachments/users/'.Auth::user()->profile_pic)!!}" class="m--img-rounded m--marginless m--img-centered" alt="" />
					</span>
				</a>
				<div class="m-dropdown__wrapper">
					<span class="m-dropdown__arrow m-dropdown__arrow--center m-dropdown__arrow--adjust"></span>
					<div class="m-dropdown__inner">
						<div class="m-dropdown__header m--align-center text-dark">
							<div class="m-card-user m-card-user--skin-dark">
								<div class="m-card-user__pic">
									<img src="{!!asset('public/attachments/users/'.Auth::user()->profile_pic)!!}" class="m--img-rounded m--marginless" alt="" />
								</div>
								<div class="m-card-user__details">
									<span class="m-card-user__name font-weight-bold m--font-weight-500 text-dark m-menu__link-text-custom">{{ Auth::user()->name }}</span>
									<span class="m-card-user__name text-dark m-menu__link-text-custom">{{ Auth::user()->position }}</span>
								</div>
							</div>
						</div>
						<hr>
						<div class="m-dropdown__body">
							<div class="m-dropdown__content">
								<ul class="m-nav m-nav--skin-light">
									<li class="m-nav__item">
										<a class="m-nav__link" href="" data-toggle="modal" data-target="#myModal">
											<i class="m-nav__link-icon flaticon-profile-1"></i>
											<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-menu__link-text-custom">{{ trans('home.my_profile') }}</span>
											</span>
											</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a class="m-nav__link" href="" data-toggle="modal" data-target="#passwordPopup">
											<i class="m-nav__link-icon flaticon-lock"></i>
											<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-menu__link-text-custom">{{ trans('authentication.change_pass') }}</span>
											</span>
											</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a class="m-nav__link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											<i class="m-nav__link-icon fa flaticon-logout"></i>
											<span class="m-nav__link-title">
												<span class="m-nav__link-wrap">
													<span class="m-menu__link-text-custom">{{ trans('home.logout') }}</span>
												</span>
											</span>
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</li>
			{{-- User Profile End --}}
		</ul>
	</div>
</div>
<!-- END: Topbar -->
