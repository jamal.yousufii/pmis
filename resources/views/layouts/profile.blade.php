<!-- User Profile Start-->
<div id="myModal" class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header profile-head bg-color-dark">
                <div class="m-portlet__head-caption px-2">
                    <div class="m-portlet__head-title">
                        <h4><span class="profile-main-title text-white">{{ trans('authentication.user_profile') }}</span></h4>
                    </div>
                </div>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                </button>
            </div>
            <div class="modal-body bg-light pb-0">
                <span id="msg-profile"></span>
                <div class="row">
                    <div class="col-xl-4 col-lg-5">
                        <div class="m-portlet m-portlet--full-height bg-white">
                            <div class="m-portlet__body">
                                <div class="m-card-profile" style="width=100% !important">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="m-card-profile__pic">
                                                        <div class="m-card-profile__pic-wrapper">
                                                            <img src="{!!asset('public/attachments/users/'.Auth::user()->profile_pic)!!}" id="user-img" alt="" class="img img-thumbnail" style="width:150px; height:150px;"/>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="m-card-profile__pic">
                                                        <a href="javascript:void()" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" onclick="callToPressBtn('profile_pic')">
                                                            <span><i class="m-nav__link-icon la la-image"></i>&nbsp;<span>{{ trans('authentication.change_photo') }}</span></span>
                                                        </a>
                                                        <form enctype="multipart/form-data" id="docForm" method="post">
                                                            <input type="file" style="display:none" name="profile_pic" id="profile_pic" onchange="readImg(this)"/>
                                                            <input type="hidden" id="userid" name="userid" value="{{ Auth::user()->id }}"/>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7">
                        <div class="m-portlet m-portlet--full-height m-portlet--tabs bg-white">
                            <div class="tab-content">
                                <div class="m-form m-form--fit">
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-8">
                                                <div class="profile-head">
                                                    <h3 style="font-size:1.5rem; font-weight:600">{{ Auth::user()->name }}</h3>
                                                    <h4>{{ Auth::user()->position }}</h4>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="javascript:void()" class="btn btn-warning m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air btn-sm" data-toggle="modal" data-target="#passwordPopup">
                                                    <span><i class="m-nav__link-icon flaticon-lock"></i>&nbsp;<span>{{ trans('authentication.change_pass') }}</span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <table width="100%" class="mx-4">
                                            <tbody>
                                                <tr>
                                                    <td width="30%"><label class="title-custom px-4">{{ trans('authentication.user_dep') }}</label></td>
                                                    <td width="70%"><p>{{ Auth::user()->department['name_'.$lang]}}</p></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%"><label class="title-custom px-4">{{ trans('authentication.name') }}</label></td>
                                                    <td width="70%">{{ Auth::user()->name }}</td>
                                                </tr>
                                                <!-- <tr>
                                                    <td width="30%"><label class="title-custom">{{ trans('authentication.father') }}</label></td>
                                                    <td width="70%">{{ Auth::user()->father }}</td>
                                                </tr> -->
                                                <tr>
                                                    <td width="30%"><label class="title-custom px-4">{{ trans('authentication.email') }}</label></td>
                                                    <td width="70%">{{ Auth::user()->email }}</td>
                                                </tr>
                                                <!-- <tr>
                                                    <td width="30%"><label class="title-custom">{{ trans('authentication.position') }}</label></td>
                                                    <td width="70%">{{ Auth::user()->position }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="30%"><label class="title-custom">{{ trans('authentication.user_type') }}</label></td>
                                                    <td width="70%">
                                                        <?php
                                                        $checked = trans('authentication.normal');
                                                        if(Auth::user()->is_admin==1)
                                                        $checked = trans('authentication.admin');
                                                        ?>
                                                        {{ $checked }}</p>
                                                    </td>
                                                </tr>-->
                                            </tbody>
                                        </table>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void()" onclick="applyChanges()" class="btn btn-success btn-sm" style="display:none" id="apply_changes">
                    <span>{{ trans('authentication.apply_changes') }}</span></span>
                </a>
                <button class="btn btn-dark btn-sm" data-dismiss="modal" aria-label="Close" id="close" onclick="javascript: location.reload();">
                    <span aria-hidden="true">{{ trans('global.close') }}</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="passwordPopup" style="z-index:999999" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-color-dark">
                <h3 class="modal-title profile-main-title white-color" id="exampleModalLongTitle">&nbsp;{{ trans('authentication.change_pass') }}</h3>
                <button type="button" class="close white-color" id="btn-password" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="changePassword" method="post">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label title-custom">{{ trans('authentication.password') }}:</label>
                        <div class="col-sm-9">
                            <div class="m-input-icon m-input-icon--left">
                                <input class="form-control m-input" type="password" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
                                <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label title-custom">{{ trans('authentication.confirm') }}:</label>
                        <div class="col-sm-9">
                            <div class="m-input-icon m-input-icon--left">
                                <input class="form-control m-input" type="password" name="confirm_password" id="conf_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
                                <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf()"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
                            </div>
                            <span id="msg"><span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button onclick="editPassword()" class="btn btn-warning btn-sm">{{ trans('authentication.change_pass') }}</button>
                <button class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">{{ trans('global.close') }}</span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- User Profile End-->