<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- Include header -->
    @include('layouts.partials._header_base')
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- Hide menu for main page -->
        @if(session('current_section')!="")
            <!-- Include aside -->
            @include('layouts.partials._aside_left')
        @endif
        <div class="m-grid__item m-grid__item--fluid m-wrapper mb-0">
            <!-- Include subheader -->
            {{-- @include('layouts.partials._subheader_default') --}}
            <div class="m-content" id="response_div">
              @yield('content')
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- Include footer -->
    @include('layouts.partials._footer_default')



</div>


<!-- end:: Page -->
<!--[html-partial:include:{"file":"partials\/_layout-quick-sidebar.html"}]/-->
{{-- @include('layouts.partials._layout_quick_sidebar') --}}

<!--[html-partial:include:{"file":"partials\/_layout-scroll-top.html"}]/-->
@include('layouts.partials._layout_scroll_top')

<!--[html-partial:include:{"file":"partials\/_layout-tooltips.html"}]/-->
{{-- @include('layouts.partials._layout_tooltips') --}}
