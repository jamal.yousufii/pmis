<?php
return [
    'contractor_project' 	=> "پر‌وژه ها",
    'projects' 		        => "لیست پر‌وژه ها",
    'project_name' 		    => "نام پروژه",  
    'contract_start_date'   => "تارخ آغاز قرارداد",
    'contract_end_date' 	=> "تاریخ ختم قرارداد",
    'contract_code'         => "کود قرارداد",
    'search' 		        => "جستجو بر اساس نام پروژه",
];
?>