<?php
return [
  'statics' 		          => "تنظیمات معلومات ثابت",
  'select' 	                  => "لطفآ یک بخش را اتنخاب نمایید",
  'category' 	              => "کتگوری",
  'add_category' 	          => "ثبت کتگوری",
  'edit_category' 	          => "تجدید کتگوری",
  'view_category'             => "نمایش کتگوری",
  'document' 	              => "نوعیت اسناد",
  'add_document' 	          => "ثبت نوعیت سند",
  'edit_document' 	          => "تجدید نوعیت سند",
  'view_document' 	          => "نمایش نوعیت سند",
  'project' 	              => "نوعیت پروژه",
  'add_project' 	          => "ثبت نوعیت پروژه",
  'edit_project' 	          => "تجدید نوعیت پروژه",
  'view_project' 	          => "نمایش نوعیت پروژه",
  'project_section' 	      => "بخش",
  'units' 	                  => "واحد ها",
  'add_units' 	              => "ثبت واحد",
  'edit_units' 	              => "تجدید واحد",
  'view_units' 	              => "نمایش واحد",
  'rec_id' 		              => "آی دی",
  'name_en' 	              => "نام به انگلیسی",
  'name_dr' 	              => "نام به دری",
  'name_pa' 	              => "نام به پشتو",
  'success_msg' 	          => "ریکارد موفقانه علاوه گردید",
  'failed_msg' 	              => "عملیه موفقانه",
  'success_edit_msg' 	      => "ریکارد موفقانه تجدید گردید",
  'success_delete_msg' 	      => "ریکارد موفقانه حذف گردید",
  'financial_source' 	      => "منابع مالی",
  'add_financial_source'      => "ثبت منبع مالی",
  'edit_financial_source'     => "تجدید منبع مالی",
  'view_financial_source'     => "نمایش منبع مالی",

  'employees' 	              => "کارمندان",
  'add_employees' 	          => "ثبت کارمند",
  'edit_employees'            => "تجدید کارمند",
  'view_employees'            => "نمایش کارمند",
  'first_name'                => "اسم",
  'last_name'                 => "تخلص",
  'job'                       => "وظیفه",
  'father'                    => "ولد",

  'roof' 	                  => "نوعیت سقف",
  'add_roof' 	              => "ثبت نوعیت سقف",
  'edit_roof'                 => "تجدید نوعیت سقف",
  'view_roof'                 => "نمایش نوعیت سقف",

  'fear' 	                  => "نوعیت ترس",
  'add_fear' 	              => "ثبت نوعیت ترس",
  'edit_fear'                 => "تجدید نوعیت ترس",
  'view_fear'                 => "نمایش نوعیت ترس",

  'foundation' 	              => "نوعیت تهداب",
  'add_foundation' 	          => "ثبت نوعیت تهداب",
  'edit_foundation'           => "تجدید نوعیت تهداب",
  'view_foundation'           => "نمایش نوعیت تهداب",

  'stair' 	                  => "نوعیت زینه",
  'add_stair' 	              => "ثبت نوعیت زینه",
  'edit_stair'                => "تجدید نوعیت زینه",
  'view_stair'                => "نمایش نوعیت زینه",

  'wall' 	                  => "نوعیت دیوار",
  'add_wall' 	              => "ثبت نوعیت دیوار",
  'edit_wall'                 => "تجدید نوعیت دیوار",
  'view_wall'                 => "نمایش نوعیت دیوار",

  'wall_shape' 	              => "شکل دیوار",
  'add_wall_shape' 	          => "ثبت شکل دیوار",
  'edit_wall_shape'           => "تجدید شکل دیوار",
  'view_wall_shape'           => "نمایش شکل دیوار",

  'column' 	                  => "نوعیت پایه ها",
  'add_column' 	              => "ثبت نوعیت پایه ها",
  'edit_column'               => "تجدید نوعیت پایه ها",
  'view_column'               => "نمایش نوعیت پایه ها",

  'column_shape' 	          => "شکل پایه ها",
  'add_column_shape'          => "ثبت شکل پایه ها",
  'edit_column_shape'         => "تجدید شکل پایه ها",
  'view_column_shape'         => "نمایش شکل پایه ها",

  'brick' 	                  => "نوعیت خشت",
  'add_brick' 	              => "ثبت نوعیت خشت",
  'edit_brick'                => "تجدید نوعیت خشت",
  'view_brick'                => "نمایش نوعیت خشت",

  'building' 	              => "ساختمان",
  'bridge' 	                  => "پل",
  'road' 	                  => "سرک",
  'well' 	                  => "چاه",

  'machinery' 	              => "ماشینری / وسایط",
  'add_machinery' 	          => "ثبت ماشینری / وسایط",
  'edit_machinery'            => "تجدید ماشینری / وسایط",
  'view_machinery'            => "نمایش ماشینری / وسایط",

  'labor_clasification'       => "مسلک",
  'add_labor_clasification'   => "ثبت مسلک",
  'edit_labor_clasification'  => "تجدید مسلک",
  'view_labor_clasification'  => "نمایش مسلک",


  'bq_section'       => "کتگوری احجام کاری",
  'add_bq_section'   => "ثبت کتگوری احجام کاری",
  'edit_bq_section'  => "تجدید کتگوری احجام کاری",
  'view_bq_section'  => "نمایش کتگوری احجام کاری",



  'activities' 	                  => "فعالیت ها",
  'add_activities' 	              => "ثبت فعالیت ها",
  'edit_activities'                => "تجدید فعالیت ها",
  'view_activities'                => "نمایش فعالیت ها",

  'workers' 	                  => "قوای بشری",
  'add_workers' 	              => "ثبت قوای بشری",
  'edit_workers'                => "تجدید قوای بشری",
  'view_workers'                => "نمایش قوای بشری",

  'equipments' 	                  => "تجهیزات و وسایل",
  'add_equipments' 	              => "ثبت تجهیزات و وسایل",
  'edit_equipments'                => "تجدید تجهیزات و وسایل",
  'view_equipments'                => "نمایش تجهیزات و وسایل",

  'payments' 	                  => "پرداخت ها",
  'add_payments' 	              => "ثبت پرداخت ها",
  'edit_payments'                => "تجدید پرداخت ها",
  'view_payments'                => "نمایش پرداخت ها",

  'problems' 	                  => "مشکلات",
  'add_problems' 	              => "ثبت مشکلات",
  'edit_problems'                => "تجدید مشکلات",
  'view_problems'                => "نمایش مشکلات",
  
];
?>
