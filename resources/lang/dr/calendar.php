<?php
return [
    'calendar'          => 'جنتری',
    'working_days'      => 'روزهای کاری',
    'public_holidays'   => 'رخصتی های',
    'normal_holidays'   => 'رخصتی ها',
    'day'               => 'یوم',
    'full_day'          => 'روز کاری',
    'half_day'          => 'نیمه روز',
    'saturday'          => 'شنبه',
    'sunday'            => 'یکشنبه',
    'monday'            => 'دو شنبه',
    'tuesday'           => 'سه شنبه',
    'wednesday'         => 'چهار شنبه',
    'thursday'          => 'پنجشنبه',
    'friday'            => 'جمعه',
    'from'              => 'از(تاریخ)',
    'to'                => 'الی(تاریخ)',
    'description'       => 'تفصیلات',
    'base'              => 'اساس',
    'actual'            => 'واقعی',
    'progress'          => 'اجراات',
    'import_project'    => 'وارد کردن رخصتی ها از پروژه دیگر؟',
    'yesNo'             => 'بلی / نخیر',
    'project'           => 'پروژه',
    'location'          => 'موقعیت',
];
