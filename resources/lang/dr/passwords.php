<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'password'  => 'رمز حد اقل باید ۶ حرف باشد و با تایید رمز مطابقت داشته باشد.',
    'reset'     => 'رمز شما تغیر نمود',
    'sent'      => 'لینک تغیر رمز برای شما ایمیل گردید',
    'token'     => 'This password reset token is invalid.',
    'user'      => "یوزر با این ایمیل آدرس دریافت نگردید",
];
