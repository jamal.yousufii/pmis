
<?php
return [
  'list' 	                    => "لیست کتگوری کار",
  'category_name'   		      => "نام کتگوری کار",
  'category_code' 		        => "کود کتگوری کار",
  'add_new_category' 	        => "ثبت کتگوری کار",
  'search' 		                => "جستجو بر اساس نام کتگوری کار",
  'view_category'     		    => "نمایش کتگوری کار",
  'edit_category'     		    => "تجدید کتگوری کار",
  'edit'     		              => "تجدید",
  'view'     	    	          => "نمایش",
  'delete'             		    => "حذف",
];
?>
