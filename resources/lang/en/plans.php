<?php
return [
  'request_summary'	        => "خلاصه درخواست",
  'project_summary'	        => "خلاصه پروژه",
  'plan' 		                => "Plans",
  'list' 		                => "لیست پروژه ها",
  'rec_id' 		              => "آی دی درخواست",
  'add_plan' 		            => "ثبت پلان جدید",
  'req_date' 		            => "تاریخ درخواست",
  'department' 		          => "شعبه درخواست کننده",
  'project_name' 		        => "Project Name",
  'project_code' 		        => "Project Code",
  'location' 		            => "Latitude\Longitude",
  'project_type' 		        => "Project Type",
  'actions' 		            => "عملیات",
  'search' 		              => "جستجو بر اساس نام و کود پروژه",
  'add' 		                => "ثبت پلان",
  'edit' 		                => "تجدید",
  'view' 		                => "نمایش",
  'delete' 		              => "حذف",
  'view' 		                => "نمایش پروژه",
  'view_plan' 		          => "نمایش پلان",
  'attached_file' 	        => "فایل ضمیمه شده",
  'req_attached_file'       => "فایل ضمیمه شده درخواست",
  'code' 		                => "کود",
  'name' 		                => "نام",
  'category' 		            => "کتگوری",
  'type' 		                => "تایپ",
  'year' 		                => "سال",
  'goals' 		              => "اهداف",
  'description' 		        => "تشریحات",
  'edit_plan' 		          => "تجدید پلان",
  'status' 		              => "وضعیت",
  'type_code' 		          => "لطفآ کود را بنویسید",
  'type_name' 		          => "لطفآ نام را بنویسید",
  'select_year' 		        => "لطفآ سال را انتخاب نمایید",
  'province' 		            => "ولایت",
  'district' 		            => "ولسوالی/ناحیه",
  'village' 		            => "قریه",
  'type_location' 	        => "لطفآ موقعیت را بنویسید",
  'financial' 		          => "منبع مالی",
  'project_start_date'      => "تاریخ آغاز",
  'project_end_date'        => "تاریخ ختم",
  'proj_category' 	        => "Project Category",
  'select_category'         => "لطفآ یک کتگوری را انتخاب نمایید",
  'select_type' 		        => "لطفآ نوعیت پروژه را انتخاب نمایید",
  'attachment' 		          => "ضمیمه",
  'choose' 		              => "انتخاب فایل",
  'attach_file' 		        => "لطفآ فایل را انتخاب نمایید",
  'new_file' 		            => "برای اظافه نمودن فایل تجدید لطفآ فایل قبلی را حذف نمایید",
  'enter_goals' 		        => "لطفآ اهداف را بنویسید",
  'enter_desc' 		          => "لطفآ تشریحات را بنویسید",
  'normal' 		              => "عادی",
  'delivery' 		            => "انتقالی",
  'urgent' 		              => "عاجل",
  'doc_type' 	              => "نوعیت سند",
  'hokm_date' 		          => "تاریخ حکم",
  'hokm_number' 	          => "شماره حکم",
  'att_file' 		            => "فایل ضمیمه شده",
  'add_location' 		        => "ثبت موقعیت پروژه",
  'view_location' 	        => "موقعیت پروژه",
  'edit_location' 	        => "تجدید موقعیت پروژه",
  'approve_date' 	          => "تاریخ تایید",
  'approve_number' 	        => "شماره تایید",
  'contract_start_date'     => "تارخ آغاز قرارداد",
  'contract_end_date' 	    => "تاریخ ختم قرارداد",
  'contract_code'           => "کود قرارداد",
  'longitude'               => "Longitude",
  'latitude'                => "Latitude",
];
?>