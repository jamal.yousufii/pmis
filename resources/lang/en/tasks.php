<?php
return [
    'calendar'          => 'Calendar',
    'tasks'             => 'Tasks',
    'my_tasks'          => 'My Tasks',
    'title'             => 'Title',
    'working_days'      => 'Working Days',
    'public_holidays'   => 'Public Holidays',
    'normal_holidays'   => 'Holidays',
    'day'               => 'Day',
    'full_day'          => 'Full Day',
    'half_day'          => 'Half Day',
    'saturday'          => 'Saturday',
    'sunday'            => 'Sunday',
    'monday'            => 'Monday',
    'tuesday'           => 'Tuesday',
    'wednesday'         => 'Wednesday',
    'thursday'          => 'Thursday',
    'friday'            => 'Friday',
    'from'              => 'From (Date)',
    'to'                => 'Duration',
    'time_left'         => 'Time Left',
    'priority'          => 'Priority',
    'description'       => 'Description',
    'base'              => 'Base',
    'actual'            => 'Actual',
    'progress'          => 'Progress',
    'add'               => 'Add Task',
    'edit'              => 'Edit Task',
    'assigned_by'       =>"Assigned By",
    'assigned_to'       =>"Assigned To",
];
