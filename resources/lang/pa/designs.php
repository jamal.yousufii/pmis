<?php
return [
  'designs' 		                => "دیزاین",
  'list' 		                    => "لیست دیزاین",
  'add' 		                    => "ثبت دیزاین",
  'rec_id' 		                  => "آی دی پروژه",
  'project_name' 		            => "نام پروژه",
  'project_code' 		            => "کود پروژه",
  'category' 		                => "کتگوری",
  'project_category' 	          => "کتگوری پروژه",
  'head_team' 		              => "مسئول تیم دیزاین",
  'start_date' 		              => "تاریخ آغاز",
  'start_date_note' 	          => "لطفآ تاریخ آغاز را انتخاب نمایید",
  'end_date' 		                => "تاریخ ختم",
  'end_date_note' 	            => "لطفآ تاریخ ختم را انتخاب نمایید",
  'search' 		                  => "جستجو بر اساس نام و کود پروژه",
  'edit_design' 		            => "تجدید دیزاین",
  'view_design' 		            => "نمایش دیزاین",
  'emp_name' 		                => "نام کارمند",
  'head_team_survey' 	          => "مسئول تیم سروی",
  'design_team' 		            => "تیم دیزاین",
  'attachment' 		              => "ضمیمه",
  'choose_file' 		            => "انتخاب فایل",
  'att_file' 		                => "لطفآ فایل را اضافه نمایید",
  'new_file' 		                => "برای اضافه نمودن فایل جدید لطفآ فایل قبلی را حذف نمایید!",
  'architecture' 		            => "مهندسی",
  'architecture_add' 	          => "ثبت مهندسی",
  'architecture_view'           => "نمایش مهندسی",
  'architecture_edit'           => "تجدید مهندسی",
  'responsible'                 => "مسئول بخش",
  'complete_date'               => "تاریخ تکمیل",
  'structure' 		              => "سترکچر",
  'structure_add' 	            => "ثبت سترکچر",
  'structure_view'              => "نمایش سترکچر",
  'structure_edit'              => "تجدید سترکچر",
  'electricity' 		            => "برق",
  'electricity_add'             => "ثبت برق",
  'electricity_view'            => "نمایش برق",
  'electricity_edit'            => "تجدید برق",
  'water' 		                  => "آبرسانی",
  'water_add'                   => "ثبت آبرسانی",
  'water_view'                  => "نمایش آبرسانی",
  'water_edit'                  => "تجدید آبرسانی",
  'mechanic'  		              => "میخانیک",
  'mechanic_add'                => "ثبت میخانیک",
  'mechanic_view'               => "نمایش میخانیک",
  'mechanic_edit'               => "تجدید میخانیک",
  'description' 		            => "تشریحات",
  'operation_type' 		          => "نوعیت عملیه",
  'unit' 		                    => "یونت",
  'amount' 		                  => "کميت",
  'price' 		                  => "قیمت / یونت",
  'add_more' 		                => "علاوه نمودن بیشتر",
  'remove' 		                  => "حذف",
  'bill_quantitie' 		          => "احجام کاری",
  'send_estimation' 	          => "ارسال به برآورد",
  'visa' 		                    => "ویزه",
  'visa_add'                    => "ثبت ویزه",
  'visa_view'                   => "نمایش ویزه",
  'visa_edit'                   => "تجدید ویزه",
  'created_by' 		              => "مسئول ثبت",
  'created_at' 		              => "تاریخ و وقت ثبت",
  'status' 		                  => "حالت",
  'approved' 		                => "تاييد شده",
  'rejected' 		                => "رد شده",
  'pending' 		                => "منتظره",
  'req_approve' 	              => "تایید",
  'req_reject'                  => "رد",
  'req_app_rej'                 => "تغییر حالت",
  'approved_msg' 	              => "شما با موفقیت اسناد تخنیکی را برای اجراات بیشتر تایید نمودید.",
  'rejected_msg'                => "اسناد تخنیکی توسط شما رد گردید",
  'update_status'               => "تغییر حالت",
  'approved_by'                 => "مسئول تائید / رد",
  'urn'                         => "شماره مسلسل",
  'location'                    => "موقعیت پروژه",
  'change_status_architecture'  => 'تغیر وضعیت مهندسی شریک شده',
  'change_status_structure'     => 'تغیر وضعیت سترکچر شریک شده',
  'change_status_electricity'   => 'تغیر وضعیت برق شریک شده',
  'change_status_water'         => 'تغیر وضعیت آبرسانی شریک شده',
  'change_status_mechanic'      => 'تغیر وضعیت آبرسانی شریک شده',
  'change_status_type'          => 'انتخاب وضعیت',
  'status'                      => 'وضعیت',
  'comment_list'                => 'لیست نظریه ها',
  'previouse_attachment'        => 'فایل های قبلی',
  'comment'                     => 'نظریه',
  'show_version'                => 'نمایش نسخه های قبلی',
  'version'                     => 'نسخه',
  'section'                     => 'بخش',
  'architecture_list'           => 'لیست مهندسی',
  'structure_list'              => 'لیست سترکچر',
  'electricity_list'            => 'لیست برق',
  'water_list'                  => 'لیست آبرسانی',
  'mechanic_list'               => 'لیست میخانیک',
  'visa_list'                   => 'لیست ویزه',
  'arch_rej_update'             => "دیزاین مهندسی رد شده پروژه :pro تجدید گردید",
  'structure_rej_update'        => "دیزاین سترکچر رد شده پروژه :pro تجدید گردید",
  'electricity_rej_update'      => "دیزاین برق رد شده پروژه :pro تجدید گردید",
  'water_rej_update'            => "دیزاین آبرسانی رد شده پروژه :pro تجدید گردید",
  'mechanic_rej_update'         => "دیزاین میخانیک رد شده پروژه :pro تجدید گردید",
  // Adjustment msg
  'arch_rej_update_adjustment'        => "تعدیل در دیزاین مهندسی پروژه :pro علاوه گردید",
  'structure_rej_update_adjustment'   => "تعدیل در دیزاین سترکچر پروژه :pro علاوه گردید",
  'electricity_rej_update_adjustment' => "تعدیل در دیزاین برق پروژه :pro علاوه گردید",
  'water_rej_update_adjustment'       => "تعدیل در دیزاین آبرسانی پروژه :pro علاوه گردید",
  'mechanic_rej_update_adjustment'    => "تعدیل در دیزاین میخانیک پروژه :pro علاوه گردید",
  
  'employees' 		                => "کارمندان",
  'employees1' 		                => "طراح",
  'employees2' 		                => "دیزاینر",

  'data_exist'                    => "ایا این پروژه دیزاین دارد؟",
  'data_exist1'                    => "دیزاین دارد؟",
  
];
?>
