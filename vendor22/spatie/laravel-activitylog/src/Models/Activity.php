<?php
namespace Spatie\Activitylog\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\Activitylog\Contracts\Activity as ActivityContract;
class Activity extends Model implements ActivityContract
{
    public $guarded = [];
    protected $casts = [
        'properties' => 'collection',
    ];
    public function __construct(array $attributes = [])
    {
        if (! isset($this->connection)) {
            $this->setConnection(config('activitylog.database_connection'));
        }
        if (! isset($this->table)) {
            $this->setTable(config('activitylog.table_name'));
        }
        parent::__construct($attributes);
    }
    public function subject(): MorphTo
    {
        if (config('activitylog.subject_returns_soft_deleted_models')) {
            return $this->morphTo()->withTrashed();
        }
        return $this->morphTo();
    }
    public function causer(): MorphTo
    {
        return $this->morphTo();
    }
    public function getExtraProperty(string $propertyName)
    {
        return Arr::get($this->properties->toArray(), $propertyName);
    }
    public function changes(): Collection
    {
        if (! $this->properties instanceof Collection) {
            return new Collection();
        }
        return $this->properties->only(['attributes', 'old']);
    }
    public function getChangesAttribute(): Collection
    {
        return $this->changes();
    }
    public function scopeInLog(Builder $query, ...$logNames): Builder
    {
        if (is_array($logNames[0])) {
            $logNames = $logNames[0];
        }
        return $query->whereIn('log_name', $logNames);
    }
    public function scopeCausedBy(Builder $query, Model $causer): Builder
    {
        return $query
            ->where('causer_type', $causer->getMorphClass())
            ->where('causer_id', $causer->getKey());
    }
    public function scopeForSubject(Builder $query, Model $subject): Builder
    {
        return $query
            ->where('subject_type', $subject->getMorphClass())
            ->where('subject_id', $subject->getKey());
    }
    //Users relation
    public function users()
    {
        return $this->hasOne('App\models\Authentication\Users','id','causer_id');
    }
    //Request Relation
    public function request()
    {
        return $this->belongsTo('App\models\Requests','subject_id','id');
    }
    //Plans Relation
    public function plan()
    {
        return $this->belongsTo('App\models\Plans','subject_id','id');
    }
    //Implements Relation
    public function implement()
    {
        return $this->belongsTo('App\models\Implement','subject_id','id');
    }
    //ThreePhaseInspection Relation
    public function threePhaseInspection()
    {
        return $this->belongsTo('App\models\ThreePhaseInspection','subject_id','id');
    }
    //HazardAnalysis Relation
    public function hazardAnalysis()
    {
        return $this->belongsTo('App\models\HazardAnalysis','subject_id','id');
    }
    //Survey Relation
    public function survey()
    {
        return $this->belongsTo('App\models\Survey','subject_id','id');
    }
    //Estimation Relation
    public function estimation()
    {
        return $this->belongsTo('App\models\Estimation','subject_id','id');
    }
    //Architecture Relation
    public function architecture()
    {
        return $this->belongsTo('App\models\Architecture','subject_id','id');
    }
    //Structure Relation
    public function structure()
    {
        return $this->belongsTo('App\models\Structure','subject_id','id');
    }
    //Electricity Relation
    public function electricity()
    {
        return $this->belongsTo('App\models\Electricity','subject_id','id');
    }
    //Water Relation
    public function water()
    {
        return $this->belongsTo('App\models\Water','subject_id','id');
    }
    //Mechanic Relation
    public function mechanic()
    {
        return $this->belongsTo('App\models\Mechanic','subject_id','id');
    }
    //Procurement Relation
    public function procurement()
    {
        return $this->belongsTo('App\models\Procurement','subject_id','id');
    }
    //Procurement Relation
    public function daily_report()
    {
        return $this->belongsTo('App\models\Daily_report','subject_id','id');
    }
}