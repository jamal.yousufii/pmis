<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
class Termination extends Model
{
  protected $connection = 'pmis';
  protected $table = 'termination';

  public function project()
  {
    return $this->belongsTo('App\models\Project','project_id','id');
  }
}
?>
