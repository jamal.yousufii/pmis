<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class EstimationTeam extends Model
{
  protected $connection = 'pmis';
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];

  public function estimations()
  {
      return $this->belongsTo('App\models\Estimation','id','estimation_id'); 
  }

  public function employees()
  {
      return $this->belongsTo('App\models\Settings\Statics\Employees','employee_id','id'); 
  }

  
}
?>