<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB; 
use Carbon\Carbon;
use DateTime;

class BillOfQuantitiesBase extends Model
{
    protected $connection = 'pmis';
    protected $table = 'estimations_bill_quantities_base';

    protected $fillable = ['id','rec_id','project_id','project_location_id','operation_type','unit_id','amount','price','total_price','percentage','remarks','start_date','end_date','actual_start_date','actual_end_date','created_by','updated_by','deleted_at','created_at','updated_at','logged_at'];
    
    public function plans()
    {
        return $this->belongsTo('App\models\plans','project_id');
    }

    public function estimation()
    {
        return $this->belongsTo('App\models\Estimation','rec_id','id');
    }

    public function unit()
    {
        return $this->belongsTo('App\models\Settings\Statics\Units','unit_id','id');
    }

    public function bq_section()
    {
      return $this->belongsTo('App\models\Settings\Statics\Bq_section','bq_section_id','id');
    }

    public function inspection()
    {
        return $this->belongsToMany('App\models\Settings\Inspection','three_phase_inspections','bill_quant_id','inspection_id');
    }

    public function hazard_analysis()
    {
        return $this->hasMany('App\models\Settings\HazardAnalysisSub','bill_quant_id','id');
    }

    public function locations()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
    }

    public function dailyActivity()
    {
        return $this->hasMany('App\models\Daily_activities','bq_id','id');
    }

    public static function activities_calculations($project_id,$project_location_id)
    {
        $query = DB::connection('pmis')
                ->table('daily_reports as dr')
                ->select('bq.*','s.name_dr','s.name_en','s.name_pa',DB::raw('SUM(dac.amount) as activity_total'))
                ->leftjoin('daily_activities as dac','dr.id','=','dac.daily_report_id')
                ->leftjoin('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
                ->join('static_data as s','s.code','=','bq.unit_id')
                ->where('dr.project_id',$project_id)
                ->where('dr.project_location_id',$project_location_id)
                ->where('dr.completed',1)
                ->groupBy('dac.bq_id')
                ->get(); 
        
        return $query; 
    }

    /**
     * Get the Bq that owns the payment request activity.
    */
    public function paymentRequestActivity()
    {
        return $this->belongsTo('App\models\PaymentRequestActivity','bq_id','id');
    }

    public static function getBudgetBase($location_id,$start_fiscal_date,$end_fiscal_date)
    {
        return DB::connection('pmis')
                  ->table('estimations_bill_quantities_base')
                  ->select(DB::raw("SUM(total_price) as total_price"),DB::raw("DATE_FORMAT(end_date, '%m') as month"))
                  ->whereIn('project_location_id',$location_id)
                  ->where('start_date','>=',$start_fiscal_date)
                  ->where('end_date','<=',$end_fiscal_date)
                  ->groupBy(DB::raw("DATE_FORMAT(end_date, '%Y-%m')"))
                  ->get();
    }

    /** 
     * @Date: 2020-09-14 11:36:58 
     * @Desc: get Based budget based on current fiscal year  
     */
    public static function getBudgetBaseCfy($location_id)
    {
        return DB::connection('pmis')
                  ->table('estimations_bill_quantities_base')
                  ->whereIn('project_location_id',$location_id)
                  ->get();
    }

    public static function getBudgetBaseStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date)
    {
        return DB::connection('pmis')
                  ->table('estimations_bill_quantities_base')
                  ->select(DB::raw("SUM(total_price) as total_price"),DB::raw("DATE_FORMAT(end_date, '%Y-%m') as month"),DB::raw("DATE_FORMAT(end_date, '%Y') as year"))
                  ->whereIn('project_location_id',$location_id)
                  ->where('start_date','>=',$start_fiscal_date)
                  ->where('end_date','<=',$end_fiscal_date)
                  ->groupBy(DB::raw("DATE_FORMAT(end_date, '%Y-%m')"))
                  ->get();
    }
    
    /** 
     * @Date: 2020-09-21 11:19:54 
     * @Desc: Calculate Basline budget based on start of the project to the end of current fiscal year 
     */
    public static function getBudgetBaseStartProjectToEndCfy($location_id,$start_fiscal_date,$end_fiscal_date)
    {
        return DB::connection('pmis')
                  ->table('estimations_bill_quantities_base')
                  ->select(DB::raw("SUM(total_price) as total_price"),DB::raw("DATE_FORMAT(end_date, '%Y-%m') as month"),DB::raw("DATE_FORMAT(end_date, '%Y') as year"))
                  ->whereIn('project_location_id',$location_id)
                  ->groupBy(DB::raw("DATE_FORMAT(end_date, '%Y-%m')"))
                  ->get();
    }

    public static function getPlanedBudget($id,$start_fiscal_date,$end_fiscal_date,$column){
        return DB::connection('pmis')
                    ->table('estimations_bill_quantities_base')
                    ->whereIn($column,$id)
                    ->where('start_date','>=',$start_fiscal_date)
                    ->where('end_date','<=',$end_fiscal_date)
                    ->sum('total_price');
    }

    public static function getFirstLastDateOfProject($id,$condition,$column){
        $date = DB::connection('pmis')->table('estimations_bill_quantities_base')
        ->select(DB::raw($condition." as date"))
        ->whereIn($column,$id)
        ->value('date');
        if($date)
        {
            return $date;
        }
        else
        {
            return '0000-00-00';
        }
    }
}
?>