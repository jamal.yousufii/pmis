<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ChangeRequestCostScope extends Model
{
    protected $connection = 'pmis';
    protected $table = 'change_request_cost_scope';

    public function attachment(){
        return $this->hasOne('App\models\ChangeRequestAttachment','record_id');
    }

    public function estimation()
    {
        return $this->belongsTo('App\models\Estimation','rec_id','id');
    }

    public function unit()
    {
        return $this->belongsTo('App\models\Settings\Statics\Units','unit_id','id');
    }

    public function bill_of_quantity()
    {
        return $this->hasOne('App\models\Billofquantities','id','bill_quantity_id');
    }
}
