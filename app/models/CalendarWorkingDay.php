<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class CalendarWorkingDay extends Model
{
    protected $connection = 'pmis';

    protected $fillable = ['id','project_id','location_id','saturday','sunday','monday','tuesday','wednesday','thursday','friday','created_by','department_id','updated_by','created_at','updated_at'];
}
