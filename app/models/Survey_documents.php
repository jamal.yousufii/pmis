<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Survey_documents extends Model
{
   //Database Connection
   protected $connection = 'pmis';

   public function survey()
   {
      return $this->belongsTo('App\models\Survey','record_id','id');
   }
   
   public function documents_type()
   {
      return $this->hasOne('App\models\Static_data','id','document_type');
   }

}
