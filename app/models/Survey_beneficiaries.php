<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Survey_beneficiaries extends Model
{
    //Database Connection
    protected $connection = 'pmis';

    public function survey()
    {
        return $this->belongsTo('App\models\Survey','record_id','id');
    }
    
    public function users()
    {
        return $this->hasOne('App\models\Static_data','id','users_type');
    }
}
