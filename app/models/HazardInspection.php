<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HazardInspection extends Model
{
    protected $connection = 'pmis';
    protected $table = 'hazard_inspection';

    public function locations()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id');
    }
}
