<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HazardAnalysisSub extends Model
{
    protected $connection = 'pmis';
    protected $table = 'hazard_analysis_sub';

    public function hazardAnalysis()
    {
        return $this->belongsTo('App\models\HazardAnalysis','hazard_analysis_id','id');
    }

    public function billOfQuantities()
    {
        return $this->belongsTo('App\models\Billofquantities','bill_quant_id','id');
    }

    public function locations()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id');
    }
}
