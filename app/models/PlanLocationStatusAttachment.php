<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PlanLocationStatusAttachment extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  protected $table = 'project_location_status_attachments';
  
  /**
   * Get the request that owns the project.
   */
  public function Projects()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }
  
  public function project_locations()
  {
    return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
  }

}


