<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Daily_activities extends Model
{
  protected $connection = 'pmis';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable  = ['daily_report_id','bq_id','location','unit_id','amount'];


  public function location()
  {
    return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
  }

  public function bill_quantity()
  {
    return $this->belongsTo('App\models\Billofquantities','bq_id','id');
  }

  /**
   * Get the daily report that owns the daily activities.
   */
  public function dailyReport()
  {
      return $this->belongsTo('App\models\Daily_report');
  }
  
   /**
   * Get the unite record associated with the Daily activity.
   */
   public function unit()
   {
      return $this->hasOne('App\models\Settings\Statics\Units','id','unit_id');
   }

  /** 
   * @Date: 2020-08-13 11:48:37 
   * @Desc:  Caculate done activity based on bq 
   */
  public function activity_done()
  {
    return $this->belongsTo('App\models\Billofquantities','bq_id','id')->groupBy('bq_id')->sum('amount');
  }
   

}
?>
