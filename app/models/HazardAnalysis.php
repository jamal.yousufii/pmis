<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HazardAnalysis extends Model
{
    use LogsActivity;
    protected $connection = 'pmis';
    protected $table = 'hazard_analysis';

    public function hazardAnalysisSub()
    {
        return $this->hasMany('App\models\HazardAnalysisSub','hazard_analysis_id','id');
    }
    public function project()
    {
        return $this->belongsTo('App\models\Plans','project_id','id');
    }
}
