<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Survey extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';

  //The users that belong to the survey.
  public function employee()
  {
    return $this->hasOne('App\models\Employee','id','team_head');
  }

  public function project()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

  public function design()
  {
    return $this->hasOne('App\models\Design','id','survey_id');
  }

  // Shared Records
  public function share()
  {
    return $this->hasMany('App\models\Share','record_id','id');
  }

  // Shared Records
  public function project_location()
  {
    return $this->belongsTo('App\models\Plan_locations','location_id','id');
  }

  // Measurements
  public function measurements()
  {
    return $this->hasMany('App\models\Survey_measurements','record_id','id');
  }

  // Beneficiaries
  public function beneficiaries()
  {
    return $this->hasMany('App\models\Survey_beneficiaries','record_id','id');
  }

  // All Employees
  public function survey_teams()
  {
    return $this->hasMany('App\models\Survey_teams','survey_id','id');
  }

  // All Employees
  public function documents()
  {
    return $this->hasMany('App\models\Survey_documents','record_id','id');
  }

  public static function getTeamEmployeesName($id=0)
  {
    return DB::table('pmis.survey_teams')->leftJoin('pmis.employees as emp','survey_teams.user_id','=','emp.id')->where('survey_id',$id)->where('emp.deleted_at',null)->get();
  }

  public function survey_progress()
  {
    return $this->belongsTo('App\models\survey_progress');
  }

  public static function deleteRecord($table="",$where=array())
  {
  	return DB::connection('pmis')->table($table)->where($where)->delete();
  }

  public static function insertRecord($table="",$data=array())
  {
  	return DB::connection('pmis')->table($table)->insert($data);
  }

  public static function getTeamEmployees($id=0)
  {
    return DB::connection('pmis')->table('survey_teams')->select('user_id')->where('survey_id',$id)->get();
  }

  public static function getFileName($id=0,$fieldName='')
  {
    return DB::connection('pmis')->table('surveys')->select($fieldName)->where('id',$id)->first();
  }

  public static function update_record($where=array(),$data=array())
  {
    return DB::connection('pmis')->table('surveys')->where($where)->update($data);
  }

  public static function getFileNameForDownload($table='',$id=0,$field_name='')
  {
    return DB::connection('pmis')->table($table)->where('id',$id)->first()->$field_name;
  }



}
