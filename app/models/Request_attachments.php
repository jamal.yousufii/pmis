<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Request_attachments extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  protected $table = 'requests_attachments';
  /**
   * Get the request that owns the project.
   */
  public function Requests()
  {
    return $this->belongsTo('App\models\Requests','rec_id','id');
  }
    
}
