<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;

class DailyEquipments extends Model
{
     protected $connection = 'pmis';
     protected $guarded = ['_token'];

     public function equipment()
     {
          return $this->belongsTo('App\models\Settings\Statics\Machinery','equipment_type','id');
     }

    /**
    * Get the post that owns the comment.
    */
    public function dailyReport()
    {
        return $this->belongsTo('App\models\Daily_report');
    }
}
?>