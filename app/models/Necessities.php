<?php
namespace App\models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Necessities extends Model
{
  protected $connection = 'pmis';
  protected $table = 'necessities';

  public function units()
  {
    return $this->hasOne('App\models\Settings\Statics\Units','id','unit');
  }

}
?>
