<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Estimation_attachments extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  
  /**
   * Get the request that owns the project.
   */
  public function estimation()
  {
    return $this->belongsTo('App\models\Estimations','rec_id','id');
  }
    
}
