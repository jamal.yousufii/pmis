<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Survey_progress extends Model
{
  protected $connection = 'pmis';
  /**
   * The users that belong to the survey.
   */
  public function survey()
  {
      return $this->belongsTo('App\models\Survey');
  }
}
