<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class ProcurementsLog extends Model
{
    protected $connection = 'pmis';
    protected $table = 'procurements_log';

    protected $fillable = ['id','urn','project_id','executive','executive_date','executive_desc','planning','planning_desc','tin','financial_letter','financial_letter_desc','bidding','bidding_time','bidding_approval','bidding_approval_desc','bidding_approval_time','announcement','announcement_desc','announcement_time','bids','bids_desc','bids_time','estimate','estimate_desc','estimate_time','public_announce','public_announce_desc','public_announce_time','send','send_desc','send_time','contract_arrangement','contract_arrangement_desc','contract_arrangement_time','contract_approval','contract_approval_desc','contract_approval_time','company','contract_code','start_date','end_date','estimated_price','file_name','start_construction','send_letter_time','controle','controle_desc','controle_time','created_by','department','updated_by','deleted_at','created_at','updated_at','completed','process','logged_at'];
  
}
