<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
class Progression extends Model
{
  protected $connection = 'pmis';
  protected $table = 'progressions';
  /**
   * Get the request that owns the project.
   */
  public function project()
  {
      return $this->belongsTo('App\models\Project');
  }
}
?>
