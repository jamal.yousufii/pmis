<?php

namespace App\models\auth;

use Illuminate\Database\Eloquent\Model;
use DB;
class Module extends Model
{
  /**
   * Get the request that owns the project.
   */
  public function users()
  {
      return $this->belongsToMany('App\User');
  }

  public function sections()
  {
      return $this->hasMany('App\models\auth\Section');
  }

  public static function get_module_id($code='')
  {
    return DB::table('modules')->where('code',$code)->first();
  }

  public static function getAllSectionsByModules($mod=array(),$userid="0")
  {
    return DB::table('user_section')->select('sec.id','sec.code')->leftJoin('sections as sec','section_id','=','sec.id')->whereIn('module_id',$mod)->where('user_id',$userid)->where('sec.deleted_at',null)->get();
  }

  public static function getSections($mod=0)
  {
    return DB::table('sections')->select('*')->where('module_id',$mod)->where('tab','0')->where('deleted_at',null)->orderBy('orders','asc')->get();
  }

  public static function getSectionsByModule($module='0',$userid="0",$department_id=0,$extra_section='0',$is_tab=0)
  {
    if($department_id!=0)
    {
        return DB::table('sections')->select('sections.*')
                 ->join('section_deps as sdep','sdep.section_id','=','sections.id')
                 ->where('module_id',$module)
                 ->where('sections.tab',$is_tab)
                 ->where('sdep.department_id',$department_id)
                 ->where('sections.deleted_at',null)
                 ->where(function($q) use($extra_section,$is_tab)
                 {
                   if($extra_section!='0' && $extra_section!='')
                   {
                     $q->where('sections.tab_code',$extra_section);
                     $q->orWhereNull('sections.tab_code');
                   }
                   else
                   {
                     $q->whereNull('sections.tab_code');
                   }
                 })
                 ->orderBy('sections.orders')
                 ->get();
                 
    }
    else
    {
      return DB::table('sections')->select('sections.*')
               ->where('module_id',$module)
               ->where('sections.tab','0')
               ->where('sections.deleted_at',null)
               ->orderBy('sections.orders')
               ->get();  
    }
  }

  public static function getSectionsByModuleAndUser($module='0',$userid="0",$department_id=0,$extra_section='0',$is_tab=0)
  {
    if($department_id!=0)
    {
      return DB::table('sections')->select('sections.*')
                ->join('user_section as us','us.section_id','=','sections.id')
                ->join('section_deps as sdep','sdep.section_id','=','sections.id')
                ->where('module_id',$module)
                ->where('sections.tab',$is_tab)
                ->where('sdep.department_id',$department_id)
                ->where('us.user_id',$userid)
                ->where('sections.deleted_at',null)
                ->where('sdep.deleted_at',null)
                ->where(function($q) use($extra_section,$is_tab)
                {
                  if($extra_section!='0' && $extra_section!='')
                  {
                    $q->where('sections.tab_code',$extra_section);
                    $q->orWhereNull('sections.tab_code');
                  }
                  else
                  {
                    $q->whereNull('sections.tab_code');
                  }
                })
                ->groupBy('sections.code')
                ->orderBy('sections.orders')
                ->get();        
    }
    else
    {
      return DB::table('sections')->select('sections.*')
               ->join('user_section as us','us.section_id','=','sections.id')
               ->where('module_id',$module)
               ->where('sections.tab','0')
               ->where('us.user_id',$userid)
               ->where('sections.deleted_at',null)
               ->groupBy('sections.code')
               ->orderBy('sections.orders')
               ->get();  
    }
  }

  public static function getAllRolesBySections($section="0",$userid="0")
  {
    return DB::table('user_role')->select('rol.id','rol.code')->leftJoin('roles as rol','role_id','=','rol.id')->where('section_id',$section)->where('user_id',$userid)->where('rol.deleted_at',null)->get();
  }

  public static function check_my_section($section="",$userid="0",$departments=0)
  {
    if($section!="")
    {
      $access = DB::table('user_section')
               ->select('sec.code')
               ->join('sections as sec','section_id','=','sec.id')
               ->where('sec.code',$section)
               ->whereIn('department_id',$departments)
               ->where('user_id',$userid)
               ->where('sec.deleted_at',null)
               ->get();
      if(count($access)>0)
      {
        return true;
      }
      else {
        return false;
      }
    }
    else 
    {
      return false;
    }
  }

  public static function check_my_auth_section($section="",$userid="0")
  {
    if($section!="")
    {
      $access = DB::table('user_section')
               ->select('sec.code')
               ->join('sections as sec','section_id','=','sec.id')
               ->where('sec.code',$section)
               ->where('user_id',$userid)
               ->where('sec.deleted_at',null)
               ->get();
      if(count($access)>0)
      {
        return true;
      }
      else {
        return false;
      }
    }
    else 
    {
      return false;
    }
  }

  public static function check_my_contractor($contractor=0,$userid="0")
  {
    if($contractor!=""){
      $access = DB::table('users')->select('contractor_id')->where('contractor_id',$contractor)->where('id',$userid)->where('deleted_at',null)->get();
      if(count($access)>0){
        return true;
      }
      else {
        return false;
      }
    }else {
      return false;
    }
  }
}
