<?php
namespace App\models\auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Section extends Model
{
  use SoftDeletes;

  /**
   * Get the request that owns the project.
   */
  public function module()
  {
      return $this->belongsTo('App\models\auth\Module');
  }
  public function roles()
  {
      return $this->hasMany('App\models\auth\Role');
  }

   /**
    * Get all of the Departments for the Section.
    */  
   public function departments()
   {
     return $this->belongsToMany('App\models\Authentication\Departments','section_deps','section_id','department_id')->whereNull('section_deps.deleted_at');
   }

   /**
    * Get the comments for the blog post.
    */

}
