<?php
namespace App\models\auth;
use Illuminate\Database\Eloquent\Model;
class Lang extends Model
{
  /**
   * Get the request that owns the project.
   */
  public function module()
  {
      return $this->belongsTo('App\models\auth\Module');
  }
  public function roles()
  {
      return $this->hasMany('App\models\auth\Role');
  }
}
