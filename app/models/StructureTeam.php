<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class StructureTeam extends Model
{
  protected $connection = 'pmis';
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];

  public function stractures()
  {
      return $this->belongsTo('App\models\Stractures','id','stractures_id'); 
  }

  public function employees()
  {
      return $this->belongsTo('App\models\Settings\Statics\Employees','employee_id','id'); 
  }

  
}
?>