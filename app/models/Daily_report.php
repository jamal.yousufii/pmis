<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Daily_report extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';

  public function project()
  {
    return $this->belongsTo('App\models\Project','project_id','id');
  }
  
  public function location()
  {
    return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
  }

  public function users()
  {
    return $this->hasOne('App\models\Authentication\Users','id','created_by');
  }

  /**
   * Get the weathers for the daily report.
   */
  public function weather()
  {
     return $this->hasOne('App\models\DailyWeather','daily_report_id','id');
  }

  /**
   * Get the QC narrative for the daily report.
   */
  public function narrative()
  {
     return $this->hasOne('App\models\DailyMainProblem','daily_report_id','id');
  }

  /**
   * Get the QC test for the daily report.
   */
  public function test()
  {
     return $this->hasMany('App\models\DailyTest');
  }

  /**
   * Get the activity for the daily report.
   */
  public function activities()
  {
     return $this->hasMany('App\models\Daily_activities');
  }

  /**
   * Get the Labour for the daily report.
   */
  public function labour()
  {
     return $this->hasMany('App\models\DailyLabour');
  }

  /**
   * Get the Equipments for the daily report.
   */
  public function equipments()
  {
     return $this->hasMany('App\models\DailyEquipments');
  }

  /**
   * Get the accidents for the daily report.
   */
   public function accidents()
   {
     return $this->hasOne('App\models\DailyAccidents');
   }


  public function approved_by()
  {
    return $this->hasOne('App\models\Authentication\Users','id','status_by');
  }

  /**
   * Get the photos for the daily report.
   */
  public function photos()
  {
    return $this->hasMany('App\models\DailyAccidents');
  }

}
?>
