<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class BillOfQuantitiesLog extends Model
{
    protected $connection = 'pmis';
    protected $table = 'estimations_bill_quantities_log';

    protected $fillable = ['id','rec_id','project_id','project_location_id','operation_type','unit_id','amount','price','total_price','percentage','remarks','start_date','end_date','actual_start_date','actual_end_date','created_by','updated_by','deleted_at','created_at','updated_at','logged_at'];
    
    public function plans()
    {
        return $this->belongsTo('App\models\plans','project_id');
    }

    public function estimation()
    {
        return $this->belongsTo('App\models\Estimation','rec_id','id');
    }

    public function unit()
    {
        return $this->belongsTo('App\models\Settings\Statics\Units','unit_id','id');
    }

    public function inspection()
    {
        return $this->belongsToMany('App\models\Settings\Inspection','three_phase_inspections','bill_quant_id','inspection_id');
    }

    public function hazard_analysis()
    {
        return $this->hasMany('App\models\Settings\HazardAnalysisSub','bill_quant_id','id');
    }

    public function locations()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
    }

    public function dailyActivity()
    {
        return $this->hasMany('App\models\Daily_activities','bq_id','id');
    }

    public static function activities_calculations($project_id,$project_location_id)
    {
        $query = DB::connection('pmis')
                ->table('daily_reports as dr')
                ->select('bq.*','s.name_dr','s.name_en','s.name_pa',DB::raw('SUM(dac.amount) as activity_total'))
                ->leftjoin('daily_activities as dac','dr.id','=','dac.daily_report_id')
                ->leftjoin('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
                ->join('static_data as s','s.code','=','bq.unit_id')
                ->where('dr.project_id',$project_id)
                ->where('dr.project_location_id',$project_location_id)
                ->where('dr.completed',1)
                ->groupBy('dac.bq_id')
                ->get(); 
        
        return $query; 
    }

    /**
     * Get the Bq that owns the payment request activity.
    */
    public function paymentRequestActivity()
    {
        return $this->belongsTo('App\models\PaymentRequestActivity','bq_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\models\Authentication\Users','logged_by','id');
    }
}
