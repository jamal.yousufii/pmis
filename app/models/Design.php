<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Design extends Model
{
  protected $connection = 'pmis';

  //The users that belong to the Design.
  public function employee()
  {
    return $this->hasOne('App\models\Employee','id','team_head');
  }

  public function project()
  {
    return $this->belongsTo('App\models\Project','project_id','id');
  }

  public function survey()
  {
    return $this->belongsTo('App\models\Survey','survey_id','id');
  }

  public static function getTeamEmployees($id=0)
  {
    return DB::connection('pmis')->table('design_teams')->select('user_id')->where('design_id',$id)->get();
  }

  public static function insertRecord($table="",$data=array())
	{
		return DB::connection('pmis')->table($table)->insert($data);
	}

  public static function deleteRecord($table="",$where=array())
	{
		return DB::connection('pmis')->table($table)->where($where)->delete();
	}

  public static function getTeamEmployeesName($id=0)
  {
    return DB::table('pmis.survey_teams')->leftJoin('pmis.employees as emp','survey_teams.user_id','=','emp.id')->where('survey_id',$id)->where('emp.deleted_at',null)->get();
  }

  public static function getDesignTeamEmployeesName($id=0)
  {
    return DB::table('pmis.design_teams')->leftJoin('pmis.employees as emp','design_teams.user_id','=','emp.id')->where('design_id',$id)->where('emp.deleted_at',null)->get();
  }
}
