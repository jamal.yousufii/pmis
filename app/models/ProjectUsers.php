<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class ProjectUsers extends Model
{
  protected $connection = 'pmis';
  protected $table = 'projects_users';
  
  /**
   * Get the request that owns the project.
   */
  public function Projects()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

  public function user()
  {
    return $this->hasOne('App\models\Authentication\Users','id','user_id');
  }
  

  
}


