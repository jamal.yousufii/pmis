<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Share extends Model
{
    use SoftDeletes;
    protected $connection = 'pmis';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['record_id','share_from','share_from_code','share_to','share_to_code','doc_number','doc_date','description','user_id','department_id'];

    // Get the Request 
    public function request()
    {
        return $this->belongsTo('App\models\Requests','id','record_id'); 
    }

    // Get Project 
    public function plan()
    {
        return $this->belongsTo('App\models\Plans','id','record_id'); 
    }

    // Get Survey 
    public function survey()
    {
        return $this->belongsTo('App\models\Survey','id','record_id'); 
    }

    // Get Section
    public function section()
    {
        return $this->belongsTo('App\models\Authentication\Sections','share_to_code','code'); 
    }

    // Get Department
    public function department()
    {
        return $this->belongsTo('App\models\Authentication\Departments','share_to','id'); 
    }

    // Get Department
    public function from_department()
    {
        return $this->belongsTo('App\models\Authentication\Departments','share_from','id'); 
    }

    // Get User
    public function user()
    {
        return $this->belongsTo('App\models\Authentication\Users','user_id','id'); 
    }

    public function scopeRequestPlan($query,$share_to,$share_to_code)
    {
        return $query->where('share_to',$share_to)->where('share_to_code',$share_to_code);      
    }
    
}
