<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class ArchitectureTeam extends Model
{
  protected $connection = 'pmis';
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];

  public function Architecture()
  {
      return $this->belongsTo('App\models\Architecture','id','architecture_id'); 
  }

  public function employees()
  {
      return $this->belongsTo('App\models\Settings\Statics\Employees','employee_id','id'); 
  }

  
}
?>