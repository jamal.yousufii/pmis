<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Plans_attachment extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  protected $table = 'project_attachments';
  
  /**
   * Get the request that owns the project.
   */
  public function Projects()
  {
    return $this->belongsTo('App\models\Plans','rec_id','id');
  }
    
}
