<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ArchitectureProjectLocationLog extends Model
{
    protected $connection = 'pmis'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','record_id','project_id','project_location_id','section','logged_at','logged_by']; 
    
}
