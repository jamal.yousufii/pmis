<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ChangeRequestStartStop extends Model
{
    protected $connection = 'pmis';
    protected $table = 'change_request_start_stop';

    public function attachment(){
        return $this->hasOne('App\models\ChangeRequestAttachment','record_id');
    }
}
