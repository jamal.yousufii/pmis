<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
class Visa extends Model
{
  protected $connection = 'pmis';
  protected $table = 'design_visa';

  public function users()
  {
    return $this->hasOne('App\models\Authentication\Users','id','created_by');
  }

}
?>
