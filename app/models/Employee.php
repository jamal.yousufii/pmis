<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The survey that belong to the user.
     */
    public function survey()
    {
      return $this->belongsTo('App\models\Survey','team_head','id');
    }

    public function designs()
    {
      return $this->belongsToMany('App\models\Design','design_teams');
    }
}
