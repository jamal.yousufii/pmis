<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Survey_attachments extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  
  /**
   * Get the request that owns the project.
   */
  public function survey()
  {
    return $this->belongsTo('App\models\Survey','rec_id','id');
  }
    
}
