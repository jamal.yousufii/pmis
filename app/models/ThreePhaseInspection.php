<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ThreePhaseInspection extends Model
{
    use LogsActivity;
    protected $connection = 'pmis';
    public function threephaseinspection()
    {
        $this->belongsTo('App\models\Settings\Inspection');
    }

    public function inspectionSub()
    {
        return $this->belongsTo('App\models\Settings\InspectionSub','inspection_sub_id');
    }

    public function locations()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id');
    }

    public function project()
    {
        return $this->belongsTo('App\models\Plans','project_id','id');
    }


}
