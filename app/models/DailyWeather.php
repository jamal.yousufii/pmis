<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class DailyWeather extends Model
{
     protected $connection = 'pmis';
     protected $table = 'daily_weathers';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_token'];

    public static function projectDailyWeather($project_id,$location_id)
    {
        $result = dailyWeather::where('project_id',$project_id)
                                ->where('project_location_id',$location_id)
                                ->get(); 
        return $result;                        
    }

    /**
     * Get the Daily Report that owns the Daily Weather.
     */
    public function dailyReport()
    {
        return $this->belongsTo('App\models\Daily_report');
    }

    public function static_data()
    {
      return $this->hasOne('App\models\Static_data','code','is_worked_stoped');
    }

}
