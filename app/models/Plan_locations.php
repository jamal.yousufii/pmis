<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Plan_locations extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  protected $table = 'project_locations';

  /**
   * Get the request that owns the project.
   */
  public function Projects()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

  public function province()
  {
    return $this->hasOne('App\models\Provinces','id','province_id');
  }

  public function district()
  {
    return $this->hasOne('App\models\Districts','id','district_id');
  }

  public function village()
  {
    return $this->hasOne('App\models\Villages','id','village_id');
  }

  public function survey()
  {
    return $this->hasOne('App\models\Survey','id','location_id');
  }

  public function contractor_staff()
  {
    return $this->hasMany('App\models\Settings\Project_contractor_staff','location_id','id');
  }

  public function department_staff()
  {
    return $this->hasMany('App\models\Settings\ProjectDepartmentStaff','location_id','id');
  }

  /**
   * @Author: Jamal Yousufi
   * @Date: 2019-12-05 14:00:59
   * @Desc: Get Architecture project Location
   */
  public function ArchitectureProjectLocation()
  {
    return $this->hasMany('App\models\ArchitectureProjectLocation','project_location_id','id');
  }

  public function bill_quantities()
  {
    return $this->belongsToMany('App\models\Billofquantities','bill_quantities_schedule','project_location','bill_of_quant');
  }

  function bill_quantities_base()
  {
    return $this->hasMany('App\models\BillOfQuantitiesBase','project_location_id','id');
  }

  function bill_quantities_est()
  {
    return $this->hasMany('App\models\BillofquantitiesEst','project_location_id','id');
  }

  public function procurement()
  {
    return $this->hasOne('App\models\Procurement','project_id','project_id');
  }

  // Holidays of Projects
  public function CalendarHoliday()
  {
    return $this->hasMany('App\models\CalendarHoliday','location_id','id');
  }

  public function location_status()
  {
    return $this->hasMany('App\models\PlanLocationStatus','location_id','id');
  }

  public function location_status_attachment()
  {
    return $this->hasOne('App\models\PlanLocationStatusAttachment','project_location_id','id');
  }

  public function daily_reports()
  {
    return $this->hasMany('App\models\Daily_report','project_location_id','id');
  }

  function change_request_time()
  {
    return $this->hasMany('App\models\ChangeRequestTime','location_id','id');
  }
}


