<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class DailyPhotos extends Model
{
  protected $connection = 'pmis';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable  = ['daily_report_id','accident_date','personal_injury','description'];

  /**
   * Get the daily report that owns the daily activities.
   */
  public function dailyReport()
  {
      return $this->belongsTo('App\models\Daily_report');
  }

}
?>
