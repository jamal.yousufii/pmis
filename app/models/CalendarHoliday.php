<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class CalendarHoliday extends Model
{
    protected $connection = 'pmis';

    protected $fillable = ['id','project_id','location_id','from_date','to_date','description','created_by','department_id','updated_by','created_at','updated_at'];
}
