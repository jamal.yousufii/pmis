<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class DailyMainProblem extends Model
{
     protected $connection = 'pmis';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['daily_report_id','main_problems','solutions','is_worked_stoped','created_by'];

}

