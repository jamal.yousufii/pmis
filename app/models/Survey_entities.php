<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Survey_entities extends Model
{
    //Connection
    protected $connection = 'pmis';
    //Table 
    protected $table = 'survey_entities';
}
