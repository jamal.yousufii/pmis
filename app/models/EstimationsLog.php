<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class EstimationsLog extends Model
{
    protected $connection = 'pmis';
    protected $table = 'estimations_log';

    protected $fillable = ['id','urn','project_id','start_date','end_date','cost','description','created_by','department','updated_by','deleted_at','created_at','updated_at','process','logged_at'];
  
}
