<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $connection = 'pmis';
    protected $table = 'tasks';

    public function assigned_by()
    {
      return $this->hasOne('App\models\Authentication\Users','id','created_by');
    }

    public function assigned_to()
    {
      return $this->hasOne('App\models\Authentication\Users','id','user');
    }    
}