<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Estimation extends Model
{
    use LogsActivity;
    protected $connection = 'pmis';

	public function project()
	{
		return $this->belongsTo('App\models\Project','project_id','id');
	}

	public static function getAllQuantities($id=0)
	{
		return DB::connection('pmis')->table('estimations_bill_quantities')->where('project_id',$id)->get();
	}

	public static function insertRecord($table="",$data=array())
	{
		return DB::connection('pmis')->table($table)->insert($data);
	}

	public static function deleteRecord($table="",$where=array())
	{
		return DB::connection('pmis')->table($table)->where($where)->delete();
	}

	public function estimationTeam()
	{
		return $this->hasMany('App\models\EstimationTeam','estimation_id','id');
	}

	public function employee()
	{
		return $this->hasOne('App\models\Employee','id','employee_id');
	}
}
