<?php

namespace App\models;
use DB; 

use Illuminate\Database\Eloquent\Model;

class PaymentRequest extends Model
{
    protected $connection = 'pmis';

 /**
  * Get the user that owns the Payment Request.
 */
  public function user()
  {
    return $this->belongsTo('App\models\Authentication\Users','created_by','id');
  }


  /**
  * Get the payment Activities for the payment request.
  */
  public function paymentRequestAcitvity()
  {
      return $this->hasMany('App\models\PaymentRequestActivity');
  }

  /** 
   * Caculate specfic invocie request payment and previouse payment   
   */
  public static function calculate_invocie_payment($request_id)
  {
    $result = DB::connection('pmis')
              ->table('payment_requests as pr')
              ->select('bq.*','s.name_dr','s.name_en','s.name_pa','pra.request_amount','pr.request_number','pr.status',DB::raw('SUM(dac.amount) as activity_total'))
              ->join('payment_request_activities as pra','pr.id','=','pra.payment_request_id')
              ->join('estimations_bill_quantities as bq','pra.bq_id','=','bq.id')
              ->leftjoin('static_data as s','s.code','=','bq.unit_id')
              ->leftjoin('daily_activities as dac','bq.id','=','dac.bq_id')
              ->leftjoin('daily_reports as dr','dac.daily_report_id','=','dr.id')
              ->where('pr.id',$request_id)
              // ->where('pr.project_id',$project_id)
              ->where('dr.completed',1)
              ->groupBy('dac.bq_id')
              ->get(); 

    return $result;           
  }


}
