<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Mechanic extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';
  /**
   * The attributes that aren't mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];

  //The users that belong to the Mechanic.
  public function employee()
  {
    return $this->hasOne('App\models\Employee','id','employee_id');
  }

  public function changeStatusBy()
  {
    return $this->hasOne('App\models\Authentication\Users','id','changed_status_by');
  }

  /**
   * @Author: Jamal Yousufi
   * @Date: 2019-12-05 13:21:10
   * @Desc: Get Mechanic Project Location
   */
  public function mechanicProjectLocation()
  {
    return $this->hasMany('App\models\MechanicProjectLocation','record_id','id');
  }

  public function project()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

  public function mechanicTeam()
  {
    return $this->hasMany('App\models\MechanicTeam','mechanic_id','id');
  }
}
?>
