<?php
namespace App\models;
use DB;
use Illuminate\Database\Eloquent\Model;

class DailyReportStatusLog extends Model
{
  protected $connection = 'pmis';
  protected $table = 'daily_reports_status_log';

  public function approved_by()
  {
    return $this->hasOne('App\models\Authentication\Users','id','status_by');
  }

}
