<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ChangeRequestTime extends Model
{
    protected $connection = 'pmis';
    protected $table = 'change_request_time';

    public function attachment(){
        return $this->hasOne('App\models\ChangeRequestAttachment','record_id');
    }
    public function bill_of_quantity()
    {
        return $this->hasOne('App\models\Billofquantities','id','bill_quantity_id');
    }
}
