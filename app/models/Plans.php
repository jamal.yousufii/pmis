<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Plans extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';
  protected $table = 'projects';
  /**
   * Get the request that owns the project.
   */
  public function Requests()
  {
    return $this->belongsTo('App\models\Requests','request_id','id');
  }

  public function project_type()
  {
    return $this->hasOne('App\models\Settings\Statics\Projects','id','project_type_id');
  }

  public function financial_source()
  {
    return $this->hasOne('App\models\Settings\Statics\Financial_source','id','budget');
  }

  public function category()
  {
    return $this->hasOne('App\models\Settings\Statics\Categories','id','category_id');
  }

  public function province()
  {
    return $this->hasOne('App\models\Provinces','id','province_id');
  }

  public function district()
  {
    return $this->hasOne('App\models\Districts','id','district_id');
  }

  public function village()
  {
    return $this->hasOne('App\models\Villages','id','village_id');
  }

  public function survey()
  {
    return $this->hasOne('App\models\Survey','project_id','id');
  }

  public function design()
  {
    return $this->hasOne('App\models\Design','project_id','id');
  }

  public function architecture()
  {
    return $this->hasMany('App\models\Architecture','project_id','id');
  }

  public function structure()
  {
    return $this->hasMany('App\models\Structure','project_id','id');
  }

  public function electricity()
  {
    return $this->hasMany('App\models\Electricity','project_id','id');
  }

  public function water()
  {
    return $this->hasMany('App\models\Water','project_id','id');
  }

  public function mechanic()
  {
    return $this->hasMany('App\models\Mechanic','project_id','id');
  }

  public function estimation()
  {
    return $this->hasOne('App\models\Estimation','project_id','id');
  }

  public function implement()
  {
    return $this->hasOne('App\models\Implement','project_id','id');
  }

  public function procurement()
  {
    return $this->hasOne('App\models\Procurement','project_id','id');
  }

  function BQuantities()
  {
    return $this->hasMany('App\models\Billofquantities','project_id','id');
  }

  function ReportBQ()
  {
    return $this->hasMany('App\models\Daily_report_bq','project_id','id');
  }

  public function progression()
  {
    return $this->hasOne('App\models\Progression','project_id','id');
  }

  public function modification()
  {
    return $this->hasOne('App\models\Modification','project_id','id');
  }

  public function estimate()
  {
    return $this->hasOne('App\models\Estimate','project_id','id');
  }

  public function termination()
  {
    return $this->hasOne('App\models\Termination','project_id','id');
  }

  public function emp_project()
  {
    return $this->hasMany('App\models\Settings\Projects','project_id','id');
  }

  public function static_data()
  {
    return $this->hasOne('App\models\Static_data','code','status');
  }

  // Shared Records
  public function share()
  {
    return $this->hasMany('App\models\Share','record_id','id');
  }

  // Shared Adjustment Records
  public function share_adjustment()
  {
    return $this->hasMany('App\models\AdjustmentShares','project_id','id');
  }

  // projects of contractor
  public function project_contractor()
  {
    return $this->hasOne('App\models\Settings\Project_contractor','project_id','id');
  }

  // projects of contractor's staff
  public function project_contractor_staff()
  {
    return $this->hasOne('App\models\Settings\Project_contractor_staff','project_id','id');
  }

  // projects of contractor's staff
  public function project_department_staff()
  {
    return $this->hasOne('App\models\Settings\ProjectDepartmentStaff','project_id','id');
  }

  public function departments()
  {
    return $this->belongsTo('App\models\Authentication\Departments','department','id');
  }

  // projects of contractor's staff
  public function daily_report()
  {
    return $this->hasMany('App\models\Settings\Daily_report','project_id','id');
  }


  public function plan_location_status()
  {
    return $this->hasMany('App\models\PlanLocationStatus','project_id','id');
  }

  // Project Adjustments
  public function adjustments()
  {
    return $this->hasMany('App\models\Adjustments','project_id','id');
  }

  // Request Adjustments for Projects
  public function adjustment_requests()
  {
    return $this->hasMany('App\models\AdjustmentRequests','project_id','id');
  }

  // Projects Locaiton
  public function project_location()
  {
    return $this->hasMany('App\models\Plan_locations','project_id','id');
  }

  // Holidays of Projects
  public function CalendarHoliday()
  {
    return $this->hasMany('App\models\CalendarHoliday','project_id','id');
  }


  // projects of contractor's staff
  public function PortfoliosSub()
  {
    return $this->hasMany('App\models\Settings\PortfoliosSub','project_id','id');
  }

  function bill_quantities()
  {
    return $this->hasMany('App\models\BillOfQuantities','project_id','id');
  }

  function bill_quantities_base()
  {
    return $this->hasMany('App\models\BillOfQuantitiesBase','project_id','id');
  }

  public function project_user()
  {
    return $this->hasOne('App\models\ProjectUsers','record_id','id');
  }

  /**
   * Search shared projects with
   *
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchSharedPlan($condition,$field,$value='',$department_id=0,$section="",$role="")
  {
    $query = Plans::join('shares','projects.id','=','shares.record_id')
              ->leftJoin('surveys as s','projects.id','=','s.project_id')
              ->leftJoin('pmis_auth.departments as dept','dept.id','=','shares.share_from')
              ->leftJoin('pmis_auth.users as u','u.id','=','shares.user_id')
              ->where('share_to',$department_id)->where('share_to_code',$section)
              ->when($condition=='like',function ($query) use($field,$value)
                {
                  return $query->where($field,'like','%'.$value.'%');
                },
                function ($query) use($field,$value)
                {
                  return $query->where($field,$value);
                }
              )
              ->where(function($query) use ($department_id,$section,$role){
                if(!doIHaveRoleInDep(encrypt($department_id),$section,$role))//if users can't view department's all records
                {
                  $query->whereHas('project_user', function($query) use ($department_id,$section){
                    //Can view assigned records
                    $query->where('user_id',userid());
                    $query->where('department_id',$department_id);
                    $query->where('section',$section);
                  });
                }
              })
              ->whereHas('project_location', function($query){
                $query->where('status','0');
              })
              ->select('projects.*','dept.name_dr','dept.name_en','dept.name_pa','u.name as username')
              ->groupBy('projects.id')->orderBy('projects.urn','desc')->paginate(10)->onEachSide(1);

    return $query;
  }

  /**
   * Search under progress projects
   *
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchProgressProjects($condition,$field,$value='',$department_id=0,$section="")
  {
    $query = Plans::join('shares','projects.id','=','shares.record_id')
              ->join('procurements as pro','projects.id','=','pro.project_id')
              ->where('share_to',$department_id)
              ->where('share_to_code',$section)
              ->when($condition=='like',function ($query) use($field,$value)
                {
                  return $query->where($field,'like','%'.$value.'%');
                },
                function ($query) use($field,$value)
                {
                  return $query->where($field,$value);
                }
              )
              ->select('projects.*','pro.company','pro.contract_code','pro.contract_price')
              ->groupBy('projects.id')->orderBy('projects.urn','desc')->paginate(10)->onEachSide(1);
    return $query;
  }

  /**
   * Search Contractor projects
   *
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchContractorProjects($condition,$field,$value='',$contractor_id=0)
  {
    $userid = userid();
    $is_admin = is_admin();
    $query = Plans::when($condition=='like',function ($query) use($field,$value)
                                            {
                                                return $query->where($field,'like','%'.$value.'%');
                                            },
                                                function ($query) use($field,$value)
                                            {
                                                return $query->where($field,$value);
                                            }
                                        )->whereHas('project_location', function($query){
                                            $query->where('status','0');
                                        })->whereHas('project_contractor', function($query) use ($contractor_id){
                                            $query->where('contractor_id',$contractor_id);
                                        })->whereHas('project_contractor_staff', function($query) use ($userid,$is_admin){
                                            if($is_admin!=1)
                                            {
                                                $query->where('user_id',$userid);// Check for admin if not then set user id
                                            }
                                        })
                                        ->orderBy('id','desc')->paginate(1)->onEachSide(1);
    return $query;
  }

  /**
   * Search Project Dashboard
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchProjectDashboard($condition,$field,$value='',$department_id=0,$section="",$dep_code)
  {
    $query = Plans::whereHas('share', function($query) use ($department_id,$dep_code){
                    if($dep_code!='dep_01')
                      $query->where('share_to',$department_id);
                    $query->where('share_to_code','pmis_progress');
                  })
                  ->when($condition=='like',function ($query) use($field,$value)
                    {
                      return $query->where($field,'like','%'.$value.'%');
                    },
                    function ($query) use($field,$value)
                    {
                      return $query->where($field,$value);
                    }
                  )->orderBy('id','desc')->paginate(10)->onEachSide(1);
    return $query;
  }

  /**
   * Search completed and stopped projects
   *
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchCompleteStopProjects($condition,$field,$value='',$department_id=0,$status=0)
  {
    $lang = get_language();
    $query = Plans::join('project_locations as loc','projects.id','=','loc.project_id')
                    ->join('project_location_status as st','projects.id','=','st.project_id')
                    ->join('pmis_auth.sections as sc','sc.code','=','st.section')
                    ->where('loc.status',$status)
                    ->where('department',$department_id)
                    ->when($condition=='like',function ($query) use($field,$value)
                      {
                        return $query->where($field,'like','%'.$value.'%');
                      },
                      function ($query) use($field,$value)
                      {
                        return $query->where($field,$value);
                      }
                    )
                    ->select('projects.*','st.created_at as created_date','sc.name_'.$lang.' as section')
                    ->groupBy('projects.id')->orderBy('projects.urn','desc')->paginate(10)->onEachSide(3);
    return $query;
  }


}
