<?php
namespace App\models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Daily_report_bq extends Model
{
  protected $connection = 'pmis';
  protected $table = 'daily_report_bq';

  public function plans()
  {
      return $this->belongsTo('App\Models\plans','project_id');
  }

}
