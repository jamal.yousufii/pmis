<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Requests extends Model
{
  use SoftDeletes;
  use LogsActivity;
  protected $connection = 'pmis';
  /**
   * Get the project record associated with the request.
   */
  public function project()
  {
    return $this->hasOne('App\models\Plans','request_id','id');
  }

  public function departments()
  {
    return $this->hasOne('App\models\Authentication\Departments','id','department_id');
  }

  public function hr_departments()
  {
    return $this->hasOne('App\models\Authentication\HRDepartments','id','department_id');
  }

  public function users()
  {
    return $this->hasOne('App\models\Authentication\Users','id','created_by');
  }

  public function documents()
  {
    return $this->hasOne('App\models\Settings\Statics\Documents','id','doc_type');
  }

  public function project_user()
  {
    return $this->hasOne('App\models\ProjectUsers','record_id','id');
  }

  // Shared Records
  public function share($share_to='',$share_to_code='')
  {
    $query = $this->hasMany('App\models\Share','record_id','id');
    if($share_to!='')
    {
       $query->where('share_to',$share_to)->where('share_to_code',$share_to_code);
    }

    return $query;
  }

  /**
   * searchData
   *
   * @param  string $connection
   * @param  string $table
   * @param  string $condition
   * @param  string $value
   * @return object
   */
  public static function searchData($condition,$field,$value='')
  {
     $query = Requests::query();
     if($field=='request_date')
     {
       $value = dateProvider($value,get_language());
     }
     $query->when($condition=='=',function($q) use($field,$value){
       return $q->where($field,$value);
     });
     $query->when($condition=='like', function($q) use($field,$value){
      return $q->where($field,'like','%'.$value.'%');
     });
     return $query->where(function($query){
        if(doIHaveRole('pmis_request','pmis_request_all_view')) {
          //Can view all records 
        }
        elseif(doIHaveRole('pmis_request','pmis_request_dep_view')) {
          // Can see record of own department 
          $query->where('department',departmentid());
        }
        else { 
          // Just seet own created record 
          $query->where('created_by',userid());
        }
      })->orderby('urn','desc')->paginate(10)->onEachSide(3);
  }

  /**
   * Search Shared request plan
   *
   * @param  mixed $condition
   * @param  mixed $field
   * @param  mixed $value
   * @return void
   */
  public static function searchShareRequestPlan($condition,$field,$value='',$department_id=0,$section="")
  {
    $query = Requests::join('shares','requests.id','=','shares.record_id')
                    ->leftJoin('projects as p','requests.id','=','p.request_id')
                    ->leftJoin('pmis_auth.departments as dept','dept.id','=','shares.share_from')
                    ->where('share_to',$department_id)->where('share_to_code',$section)
                    ->when($condition=='like',function ($query) use($field,$value)
                      {
                        return $query->where($field,'like','%'.$value.'%');
                      },
                      function ($query) use($field,$value)
                      {
                        return $query->where($field,$value);
                      }
                    )
                    ->select('requests.urn as rurn','requests.id as req_id','p.urn as purn','p.name','p.code','p.process','dept.name_'.get_language().' as share_from')
                    ->orderBy('requests.urn','desc')->groupBy('requests.id')->paginate(10)->onEachSide(3);
    return $query;    
  }

}
