<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\models\Authentication\Section_deps;

class Departments extends Model
{
  protected $connection = 'pmis_auth';
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['organization_id','name_en','name_dr','name_pa','code','icon','description'];
  
  function organization()
  {
    return $this->belongsTo('App\models\Authentication\Organization','organization_id');
  }

  function module_deps()
  {
    return $this->hasMany('App\models\Authentication\Module_deps','department_id','id');
  }

  function section_deps()
  {
    return $this->hasMany('App\models\Authentication\Section_deps','department_id','id');
  }

  // Get Sections 
  function sections()
  {
    return $this->belongsToMany('App\models\Authentication\Sections','section_deps','department_id','section_id')->whereNull('section_deps.deleted_at')->withTimestamps(); 
  }

  // Get Share Record 
  function sharedRecords()
  {
    return $this->hasMany('App\models\RequestShare','department_id','id'); 
  }

}

?>
