<?php

namespace App\models\Authentication;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $connection = 'pmis_auth'; 
    protected $table = 'user_role';

    function users()
    {
        return $this->belongsTo('App\models\Authentication\User','user_id','id');
    }
  
    function roles()
    {
        return $this->belongsTo('App\models\Authentication\Roles','role_id','id');
    }
}
