<?php

namespace App\models\Authentication;

use Illuminate\Database\Eloquent\Model;

class ModuleUsers extends Model
{
    //
    protected $connection = 'pmis_auth';
    protected $table = 'module_user';

    public function user(){
        return $this->belongsTo('App\models\Authentication\Users','user_id','id');
    }

    public function departments(){
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }
}
