<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section_deps extends Model
{
    function departments()
    {
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }
  
    function section()
    {
        return $this->belongsTo('App\models\Authentication\Sections','section_id','id');
    }
    

}

?>