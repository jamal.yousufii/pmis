<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Roles extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = ['code','section_id','name_en','name_dr','name_pa','description'];

  function section()
  {
    return $this->hasOne('App\models\Authentication\Sections','id','section_id');
  }

  // Get the users based on MANY TO MANY relation 
  function users()
  {
    return $this->belongsToMany('App\models\Auth\User','user_role','role_id','user_id'); 
  }

  public static function getAllSections($lang="")
  {
    return DB::table('sections')->select('id','name_'.$lang.' as name')->where('deleted_at',null)->get();
  }

}
?>
