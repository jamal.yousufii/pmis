<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_dr','name_pa','name_en','org_code'];
    
    function department()
    {
        return $this->hasOne('App\models\Authentication\Departments','organization_id','id');
    }
}
