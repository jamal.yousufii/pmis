<?php

namespace App\models\Authentication;

use Illuminate\Database\Eloquent\Model;

class user_section extends Model
{
    //
    protected $connection = 'pmis_auth';
    protected $table = 'user_section';

    public function user(){
        return $this->belongsTo('App\models\Authentication\Users','user_id','id');
    }
    public function section(){
        return $this->belongsTo('App\models\Authentication\Sections','section_id','id');
    }
}
