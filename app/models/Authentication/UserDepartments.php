<?php

namespace App\models\Authentication;

use Illuminate\Database\Eloquent\Model;

class UserDepartments extends Model
{
    //
    protected $connection = 'pmis_auth';
    protected $table = 'user_departments';

    public function user(){
        return $this->belongsTo('App\models\Authentication\Users','user_id','id');
    }

    public function departments(){
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }
}
