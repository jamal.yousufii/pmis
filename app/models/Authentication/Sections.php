<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Sections extends Model
{
  protected $connection = 'pmis_auth';
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name_dr','name_pa','name_en','module_id','department_id','code','url_route'];

  function modules()
  {
    return $this->hasOne('App\models\Authentication\Modules','id','module_id');
  }

  function section_deps()
  {
    return $this->hasMany('App\models\Authentication\Section_deps','section_id','id');
  }

  // Get the Department
  public function departments()
  {
    return $this->belongsToMany('App\models\Authentication\Departments','section_deps','section_id','department_id');
  }

  //User Sections Relation
  public function user(){
    return $this->belongsToMany('App\models\Authentication\Users','user_section','section_id','user_id');
  }

}

?>
