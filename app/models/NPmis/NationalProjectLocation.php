<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class NationalProjectLocation extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  
  /**
   * Get the request that owns the project.
   */
  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProjects','project_id','id');
  }

  public function province()
  {
    return $this->hasOne('App\models\Provinces','id','province_id');
  }

  public function district()
  {
    return $this->hasOne('App\models\Districts','id','district_id');
  }

  public function village()
  {
    return $this->hasOne('App\models\Villages','id','village_id');
  }


  
}


