<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalProjectFeedbacks extends Model
{
  protected $connection = 'pmis';

  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
  }
}
?>