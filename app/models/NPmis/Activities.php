<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activities extends Model
{
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public function nationalActivities()
    {
        return $this->hasMany('App\models\NPmis\NationalActivities','activitie_id');
    }
}
?>
