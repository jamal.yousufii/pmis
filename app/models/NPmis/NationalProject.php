<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalProject extends Model
{
  protected $connection = 'pmis';

  public function project_type()
  {
    return $this->hasOne('App\models\NPmis\NationalProjectTypes','id','project_type_id');
  }

  public function category()
  {
    return $this->hasOne('App\models\NPmis\NationalCategories','id','category_id');
  }

  // get project location 
  public function projectLocations()
  {
    return $this->hasMany('App\models\NPmis\NationalProjectLocation','national_project_id');
  }

  public function projectActivities()
  {
    return $this->hasMany('App\models\NPmis\NationalActivities','national_project_id');
  }

  public function projectProgress()
  {
    return $this->hasMany('App\models\NPmis\NationalProgress','national_project_id');
  }

  /**
   * searchData
   *
   * @param  string $connection
   * @param  string $table
   * @param  string $condition
   * @param  string $value
   * @return object
   */
  public static function searchData($condition,$field,$value='')
  {
     $query = NationalProject::query();
     $query->when($condition=='=',function($q) use($field,$value){
       return $q->where($field,$value);
     });
     $query->when($condition=='like', function($q) use($field,$value){
      return $q->where($field,'like','%'.$value.'%');
     });
     return $query->where(function($query){
        if(!doIHaveRoleInDep(encrypt(departmentid()),'npmis_projects','npro_all_view'))//If user can't view all records
        {
            $query->where('department_id',departmentid());
        }
      })->orderby('urn','desc')->paginate(10)->onEachSide(3);
  }  

  public function feedbacks()
  {
    return $this->hasMany('App\models\NPmis\NationalProjectFeedbacks','national_project_id');
  }

}
?>