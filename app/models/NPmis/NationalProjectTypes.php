<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NationalProjectTypes extends Model
{
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public function national_projects()
    {
        return $this->hasMany('App\models\NPmis\NationalProject','project_type_id');
    }
    
    public function national_categories()
    {
        return $this->belongsTo('App\models\NPmis\NationalCategories','national_categorie_id','id');
    }
}
?>
