<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalProgress extends Model
{
  protected $connection = 'pmis';

  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
  }

  public function main_problems()
  {
    return $this->hasOne('App\models\NPmis\NationalProblems','id','national_progress_id');
  }
}
?>