<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workers extends Model
{
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
}
?>
