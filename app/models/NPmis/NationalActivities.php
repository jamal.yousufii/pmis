<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalActivities extends Model
{
  protected $connection = 'pmis';

  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
  }

  public function activities()
  {
    return $this->belongsTo('App\models\NPmis\Activities','activitie_id','id');
  }
}
?>