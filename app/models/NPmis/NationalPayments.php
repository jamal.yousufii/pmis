<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalPayments extends Model
{
  protected $connection = 'pmis';

  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
  }

  public function payments()
  {
    return $this->belongsTo('App\models\NPmis\Payments','payment_id','id');
  }
}
?>