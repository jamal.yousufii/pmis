<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalProblems extends Model
{
    protected $connection = 'pmis';

    public function national_projects()
    {
        return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
    }

    public function national_progress()
    {
        return $this->belongsTo('App\models\NPmis\NationalProgress','national_progress_id','id');
    }

    public function problems()
    {
        return $this->belongsTo('App\models\NPmis\Problems','problem_id','id');
    }
}
?>