<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;

class NationalLabor extends Model
{
  protected $connection = 'pmis';

  public function national_projects()
  {
    return $this->belongsTo('App\models\NPmis\NationalProject','national_project_id','id');
  }

  public function national_progress()
  {
    return $this->belongsTo('App\models\NPmis\NationalProgress','national_progress_id','id');
  }

  public function labor()
  {
    return $this->hasOne('App\models\NPmis\Workers','id','type');
  }

}
?>