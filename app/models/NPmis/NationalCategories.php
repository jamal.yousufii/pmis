<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NationalCategories extends Model
{
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public function natonal_projects()
    {
        return $this->hasMany('App\models\NPmis\NationalProjects','category_id');
    }
    public function natonal_project_types()
    {
        return $this->hasMany('App\models\NPmis\NationalProjectType','national_categorie_id');
    }
}
?>