<?php
namespace App\models\NPmis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Problems extends Model
{
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public function natonal_problems()
    {
        return $this->hasMany('App\models\NPmis\NationalProblems','activitie_id');
    }
}
?>
