<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Water extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';

  /**
   * The attributes that aren't mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];


  //The users that belong to the Water.
  public function employee()
  {
    return $this->hasOne('App\models\Employee','id','employee_id');
  }

  public function changeStatusBy()
  {
    return $this->hasOne('App\models\Authentication\Users','id','changed_status_by');
  }

  /**
   * @Author: Jamal Yousufi
   * @Date: 2019-12-05 13:21:10
   * @Desc: Get Structure Project Location
   */
  public function waterProjectLocation()
  {
    return $this->hasMany('App\models\WaterProjectLocation','record_id','id');
  }

  public function project()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

  public function waterTeam()
  {
    return $this->hasMany('App\models\WaterTeam','water_id','id');
  }
}
?>
