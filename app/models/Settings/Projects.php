<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Projects extends Model
{
  protected $connection = 'pmis';
  protected $table = 'employee_projects';
  
  public function project()
  {
      return $this->belongsToMany('App\models\Plans', 'project_id');
  }
}

?>
