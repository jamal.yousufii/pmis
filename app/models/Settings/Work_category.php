<?php

namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Work_category extends Model
{
    use SoftDeletes;
    protected $table      = 'work_category';
    protected $connection = 'pmis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_name','category_code'];
}
