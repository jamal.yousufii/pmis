<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Portfolios extends Model
{
    use SoftDeletes;
    protected $connection = 'pmis';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_dr','name_pa','name_en'];

    public function PortfoliosSub()
    {
        return $this->hasMany('App\models\Settings\PortfoliosSub','portfolio_id','id');
    }

    public function department()
    {
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }

}
?>