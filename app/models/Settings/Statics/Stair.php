<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Stair extends Model
{
  protected $table = 'stair';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}
?>
