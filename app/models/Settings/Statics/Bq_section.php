<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bq_section extends Model
{
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
}
?>
