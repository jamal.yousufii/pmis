<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Financial_source extends Model
{
  protected $table = 'project_financial_source';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];

}
?>
