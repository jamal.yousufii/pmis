<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Wall_shape extends Model
{
  protected $table = 'wall_shape';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}
?>
