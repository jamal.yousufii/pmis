<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Employees extends Model
{
  protected $table = 'employees';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];

  function sections()
  {
      return $this->belongsTo('App\models\Authentication\Sections','section','code');
  }
}
?>
