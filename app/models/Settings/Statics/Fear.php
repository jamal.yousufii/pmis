<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Fear extends Model
{
  protected $table = 'fear';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}
?>
