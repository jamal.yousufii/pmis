<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Machinery extends Model
{
  protected $table = 'machinery';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
}
?>
