<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Documents extends Model
{
  protected $table = 'doc_types';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}
?>
