<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use DB;
class Statics extends Model
{
  public static function getAllStatics($lang="en")
  {
    return DB::table('statics')->select('code','name_'.$lang.' as name','tbl_name')->where('organization_id',organizationid())->get();
  }
}
?>
