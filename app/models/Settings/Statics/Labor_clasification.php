<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Labor_clasification extends Model
{
  protected $table = 'labor_clasification';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
}
?>
