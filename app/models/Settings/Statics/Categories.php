<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Categories extends Model
{
    protected $table = 'categories';
    protected $connection = 'pmis';
    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    function plans()
    {
        return $this->hasOne('App\models\Plans','category_id');
    }
}
?>
