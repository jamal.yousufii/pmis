<?php
namespace App\models\Settings\Statics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Projects extends Model
{
  protected $table = 'project_types';
  protected $connection = 'pmis';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];

  function categories()
  {
    return $this->hasOne('App\models\Settings\Statics\Categories','id','category_id');
  }

  public static function getAllCategories()
  {
    $lang = get_language();
    return DB::table('pmis.categories')->select('id','name_'.$lang.' as name')->where('deleted_at',null)->get();
  }
}
?>
