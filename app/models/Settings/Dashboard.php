<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Dashboard extends Model
{
  protected $table = 'dashboard';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}

?>
