<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PortfoliosSub extends Model
{
    use SoftDeletes;
    protected $connection = 'pmis';
    protected $table      = 'portfolios_sub';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_dr','name_pa','name_en'];

    public function Portfolios()
    {
        return $this->belongsTo('App\models\Settings\Portfolios','portfolio_id','id');
    }

    public function project()
    {
        return $this->belongsTo('App\models\Plans','project_id','id');
    }
}
?>