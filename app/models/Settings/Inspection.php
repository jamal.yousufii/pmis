<?php

namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Inspection extends Model
{
    //
    use SoftDeletes;
    protected $connection = 'pmis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_token'];
    /**
     * Get the sub_inspections for the inpection.
    */
    public function inspectionSub()
    {
        return $this->hasMany('App\models\Settings\InspectionSub','inspection_id','id');
    }

    public function billOfQuantity()
    {
        return $this->belongsToMany('App\models\Billofquatities','three_phase_inspections','inspection_id','bill_quant_id');
    }

}
