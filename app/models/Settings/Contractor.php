<?php

namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    //
    use SoftDeletes;
    protected $table = 'contractors';
    protected $connection = 'pmis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_name,primary_contact_name,primary_contact_phone,primary_contact_email,
                            alternative_contact_name,alternative_contact_phone,alternative_contact_email,
                            created_by,updated_by'];

    public function users()
    {
        return $this->hasOne('App\models\Authentication\Users','id','created_by');
    }
}
