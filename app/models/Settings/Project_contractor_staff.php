<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\Model;

class Project_contractor_staff extends Model
{
    protected $table = 'project_contractor_staff';
    protected $connection = 'pmis';

    public function project()
    {
        return $this->belongsTo('App\models\Plans','project_id','id');
    }

    public function contractor()
    {
        return $this->belongsTo('App\models\Settings\Contractor','contractor_id','id');
    }

    public function contractor_users()
    {
        return $this->hasOne('App\models\Authentication\Users','id','user_id');
    }

    public function users()
    {
        return $this->hasOne('App\models\Authentication\Users','id','created_by');
    }

    public function location()
    {
        return $this->belongsTo('App\models\Plan_locations','location_id','id');
    }
}
