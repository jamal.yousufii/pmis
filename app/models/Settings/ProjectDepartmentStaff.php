<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\Model;

class ProjectDepartmentStaff extends Model
{
    protected $table = 'project_department_staff';
    protected $connection = 'pmis';

    public function project()
    {
        return $this->belongsTo('App\models\Plans','project_id','id');
    }

    public function department()
    {
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }

    public function users()
    {
        return $this->hasOne('App\models\Authentication\Users','id','userid');
    }

    public function users1()
    {
        return $this->belongsTo('App\models\Authentication\Users','userid');
    }
    
    public function location()
    {
        return $this->belongsTo('App\models\Plan_locations','location_id','id');
    }
}
