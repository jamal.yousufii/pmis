<?php
namespace App\models\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Slider extends Model
{
  protected $table = 'slider';
  use SoftDeletes;
  protected $softDelete = true;
  protected $dates = ['deleted_at'];
}

?>
