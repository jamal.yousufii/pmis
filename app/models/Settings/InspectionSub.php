<?php

namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class InspectionSub extends Model
{
    //
    use SoftDeletes;
    protected $connection = 'pmis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_dr','name_pa','name_en'];

    public function inspection()
    {
        return $this->belongsTo('App\models\Settings\Inspection','inspection_id');
    }

    public function threePhaseInspection()
    {
        return $this->hasMany('App\models\ThreePhaseInspection','inspection_sub_id','id');
    }
}
