<?php

namespace App\models\Settings;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Feature_of_work extends Model
{
     //
     use SoftDeletes;
     protected $table      = 'feature_of_work';
     protected $connection = 'pmis';
 
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = ['feature_name','feature_description'];
}
