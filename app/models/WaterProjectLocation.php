<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class WaterProjectLocation extends Model
{
    protected $connection = 'pmis'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','record_id','project_id','project_location_id','section']; 

    /** 
     * @Author: Jamal YOusufi  
     * @Date: 2019-12-05 13:29:54 
     * @Desc: Get Architecure   
     */    
    public function water()
    {
        return $this->belongsTo('App\models\Water','id','record_id'); 
    }

    /** 
     * @Author: Jamal YOusufi  
     * @Date: 2019-12-05 13:47:07 
     * @Desc: Get Project Location   
     */    
    public function projectLocation()
    {
        return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
    }
}
