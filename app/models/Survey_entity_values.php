<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Survey_entity_values extends Model
{
    //Database Connection
    protected $connection = 'pmis';

    public function entity()
    {
        return $this->belongsTo('App\models\Survey_entities','entity_id','code');
    }
}
