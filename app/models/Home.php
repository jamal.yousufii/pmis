<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Home extends Model
{
  public static function getAllDepartmetns($paginate=true)
  {
    $lang = get_language();
    if($paginate)
    {
      return DB::table('departments')->select('id','name_'.$lang.' as name','created_at')->where('active',0)->where('deleted_at',null)->paginate(10);
    }
    else
    {
      return DB::table('departments')->select('id','name_'.$lang.' as name','created_at')->where('active',0)->where('deleted_at',null)->get();
    }
  }

  public static function updateFileRecord($table,$id,$data)
  {
    return DB::connection('pmis')->table($table)->whereId($id)->update($data);
  }

  public static function getFile($table='',$id=0)
  {
    return DB::connection('pmis')->table($table)->where('id',$id)->first();
  }

  public static function getAllCategories()
  {
    $lang = get_language();
    return DB::table('pmis.categories')->select('id','name_'.$lang.' as name')->where('deleted_at',null)->get();
  }

  public static function getAllProjectTypes()
  {
    $lang = get_language();
    return DB::table('pmis.project_types')->select('id','name_'.$lang.' as name','category_id')->where('deleted_at',null)->get();
  }

  public static function getAllProjectTypesByCategory($cat_id=0)
  {
    $lang = get_language();
    return DB::table('pmis.project_types')->select('id','name_'.$lang.' as name','category_id')->where('category_id',$cat_id)->where('deleted_at',null)->get();
  }

  public static function getAllEmployees()
  {
    return DB::table('pmis.employees')->where('deleted_at',null)->get();
  }

  public static function getAllStatics($table='')
  {
    $lang = get_language();
    return DB::connection('pmis')->table($table)->select('id','name_'.$lang.' as name')->where('active',0)->where('deleted_at',null)->get();
  }

  public static function getAllProvinces()
  {
    $lang = get_language();
    return DB::table('provinces')->select('id','name_'.$lang.' as name')->get();
  }

  public static function getAllDistrictsByProvince($id=0)
  {
    $lang = get_language();
    return DB::table('districts')->select('id','name_'.$lang.' as name','province_id')->where('province_id',$id)->get();
  }

  public static function getAllVillagesByDistrict($id=0)
  {
    $lang = get_language();
    return DB::table('villages')->select('id','name_'.$lang.' as name','district_id')->where('district_id',$id)->get();
  }

  public static function getFinancialSource($lang='dr')
  {
    return DB::table('pmis.project_financial_source')->select('id','name_'.$lang.' as name')->where('deleted_at',null)->get();
  }

  public static function getAllAttachments($id=0,$table='')
  {
    return DB::connection('pmis')->table($table)->select('*')->where('rec_id',$id)->orderBy('id','desc')->get();
  }

  public static function getAttachment($id=0,$table='')
  {
    return DB::connection('pmis')->table($table)->select('*')->where('id',$id)->first();
  }


  /** 
   * @Author: Jamal Yousufi  
   * @Date: 2019-12-17 13:51:51 
   * @Desc: Delete Record   
   */  
  public static function deleteRecord($table='',$condition=array())
  {
     return DB::connection('pmis')->table($table)->where($condition)->delete(); 
  }
  
    
  /**
   * getData from all tables 
   * @author Jamal Yousufi 
   * @return object 
   */
  public static function getData($connection,$table,$condition=array())
  {
    $query = DB::connection($connection)->table($table); 
    if($condition!='')
    {
      $query->where($condition); 
    }
    
    return $query->get(); 
  }
  
  /**
   * searchData
   *
   * @param  string $connection
   * @param  string $table
   * @param  string $condition
   * @param  string $value
   * @return object 
   */
  public static function searchData($connection,$table,$condition,$field,$value)
  {
     $query = DB::connection($connection)->table($table); 
     if($condition=='=')
     {
        $query->where($field,$condition);  
     }
     elseif($condition=='like')
     {
       $query->where($field,'like','%'.$value.'%');
     }

     return $query->paginate(10)->onEachSide(1); 
  }
}
