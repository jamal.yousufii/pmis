<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BillofquantitiesEst extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';
  protected $table = 'estimations_bill_quantities_est';

  public function plans()
  {
    return $this->belongsTo('App\models\plans','project_id');
  }

  public function estimation()
  {
    return $this->belongsTo('App\models\Estimation','rec_id','id');
  }

  public function unit()
  {
    return $this->belongsTo('App\models\Settings\Statics\Units','unit_id','id');
  }

  public function bq_section()
  {
    return $this->belongsTo('App\models\Settings\Statics\Bq_section','bq_section_id','id');
  }

  public function inspection()
  {
     return $this->belongsToMany('App\models\Settings\Inspection','three_phase_inspections','bill_quant_id','inspection_id');
  }

  public function hazard_analysis()
  {
     return $this->hasMany('App\models\Settings\HazardAnalysisSub','bill_quant_id','id');
  }

  public function locations()
  {
     return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
  }

  public function dailyActivity()
  {
    return $this->hasMany('App\models\Daily_activities','bq_id','id');
  }
  
  public function bq_base()
  {
    return $this->hasOne('App\models\BillOfQuantitiesBase','id','id');
  }

  /**
   * Get the Bq that owns the payment request activity.
   */
  public function paymentRequestActivity()
  {
      return $this->belongsTo('App\models\PaymentRequestActivity','bq_id','id');
  }
}
