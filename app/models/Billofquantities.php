<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use DB; 
use Carbon\Carbon;
use DateTime;
use setISODate;
use Config;

class Billofquantities extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';
  protected $table = 'estimations_bill_quantities';

  public function plans()
  {
    return $this->belongsTo('App\models\plans','project_id');
  }

  public function estimation()
  {
    return $this->belongsTo('App\models\Estimation','rec_id','id');
  }

  public function unit()
  {
    return $this->belongsTo('App\models\Settings\Statics\Units','unit_id','id');
  }

  public function bq_section()
  {
    return $this->belongsTo('App\models\Settings\Statics\Bq_section','bq_section_id','id');
  }

  public function inspection()
  {
     return $this->belongsToMany('App\models\Settings\Inspection','three_phase_inspections','bill_quant_id','inspection_id');
  }

  public function hazard_analysis()
  {
     return $this->hasMany('App\models\Settings\HazardAnalysisSub','bill_quant_id','id');
  }

  public function locations()
  {
     return $this->belongsTo('App\models\Plan_locations','project_location_id','id');
  }

  public function dailyActivity()
  {
    return $this->hasMany('App\models\Daily_activities','bq_id','id');
  }

  public static function activities_calculations($project_id,$project_location_id)
  {
    $query = DB::connection('pmis')
            ->table('daily_reports as dr')
            ->select('bq.*','s.name_dr','s.name_en','s.name_pa',DB::raw('SUM(dac.amount) as activity_total'))
            ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
            ->leftjoin('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
            ->leftjoin('static_data as s','s.code','=','bq.unit_id')
            ->where('dr.project_id',$project_id)
            ->where('dr.project_location_id',$project_location_id)
            ->where('dr.completed',1)
            ->where('dr.status','!=',-1)
            ->groupBy('dac.bq_id')
            ->get(); 
    
    return $query; 
  }

  // BQ Base
  public function bq_base()
  {
    return $this->hasOne('App\models\BillOfQuantitiesBase','id','id');
  }

  /**
   * Get the Bq that owns the payment request activity.
   */
  public function paymentRequestActivity()
  {
      return $this->belongsTo('App\models\PaymentRequestActivity','bq_id','id');
  }

  /** 
   * @Date: 2020-08-12 10:48:07 
   * @Desc:  Calculate total price of activity done and approve 
   */
  public static function culculate_total_act_done_price($bill_quantities)
  {
    $total_done_price = 0; 
    if($bill_quantities)
    {
      foreach($bill_quantities as $item)
      {
        $total_done_price += $item->activity_total * $item->price; 
      }
    }

    return $total_done_price; 
  }

  public static function getBudgetDone($location_id){
    return DB::connection('pmis')
          ->table('daily_reports as dr')
          ->select('bq.*',DB::raw('(SUM(dac.amount) * bq.price) as total_done'))
          ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
          ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
          ->whereIn('bq.project_location_id',$location_id)
          ->where('dr.completed',1)
          ->where('dr.status',1)
          ->groupBy('dac.bq_id')
          ->get()->sum('total_done');
  }

  public static function getBudgetDone_new($location_id,$start_date,$end_date){
    return DB::connection('pmis')
          ->table('daily_reports as dr')
          ->select('bq.*',DB::raw('(SUM(dac.amount) * bq.price) as total_done'))
          ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
          ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
          ->whereIn('bq.project_location_id',$location_id)
          ->where('dr.completed',1)
          ->where('dr.status',1)
          ->whereBetween('dr.report_date',[$start_date,$end_date])
          ->groupBy('dac.bq_id')
          ->get()->sum('total_done');
  }

  public static function getBudgetWorking($location_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities')
              ->select(DB::raw("SUM(total_price) as total_price"),DB::raw("DATE_FORMAT(end_date, '%m') as month"))
              ->whereIn('project_location_id',$location_id)
              ->where('start_date','>=',$start_fiscal_date)
              ->where('end_date','<=',$end_fiscal_date)
              ->groupBy(DB::raw("DATE_FORMAT(end_date, '%Y-%m')"))
              ->get();
  }

  public static function getBudgetWorkingStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date)
  {
    return DB::connection('pmis')
              ->table('estimations_bill_quantities')
              ->select(DB::raw("SUM(total_price) as total_price"),DB::raw("DATE_FORMAT(end_date, '%Y-%m') as month"),DB::raw("DATE_FORMAT(end_date, '%Y') as year"))
              ->whereIn('project_location_id',$location_id)
              ->where('start_date','>=',$start_fiscal_date)
              ->where('end_date','<=',$end_fiscal_date)
              ->groupBy(DB::raw("DATE_FORMAT(end_date, '%Y-%m')"))
              ->get();
  }


  public static function getBudgetActual($location_id=array(),$start_fiscal_date,$end_fiscal_date)
  {
    $data = DB::connection('pmis')
              ->table('daily_reports as dr')
              ->select(DB::raw('(SUM(dac.amount) * bq.price) as total_price'),DB::raw("DATE_FORMAT(dr.report_date, '%m') as month"),DB::raw("DATE_FORMAT(dr.report_date, '%Y') as year"),DB::raw("DATE_FORMAT(dr.report_date, '%Y-%m-%d') as date"))
              ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
              ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
              ->whereIn('bq.project_location_id',$location_id)
              ->where('dr.completed',1)
              ->where('dr.status',1)
              ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
              ->groupBy('dac.bq_id')
              ->get();
    return $data;           
  }

  public static function getBudgetDone1($project_id=array(),$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
          ->table('daily_reports as dr')
          ->select('bq.id','bq.operation_type',DB::raw('(SUM(dac.amount) * bq.price) as new_total'))
          ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
          ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
          ->whereIn('dr.project_id',$project_id)
          ->where('dr.completed',1)
          ->where('dr.status',1)
          ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
          ->groupBy(DB::raw("DATE_FORMAT(dr.report_date, '%Y-%m')"),'bq.id')
          ->get();
  }

  public static function getBudgetShare($location_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select(DB::raw("SUM(bq.total_price) as total_price"),'pro.name','p.contract_code')
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('procurements as p','pro.id','=','p.project_id')
              ->whereIn('bq.project_location_id',$location_id)
              ->where('bq.start_date','>=',$start_fiscal_date)
              ->where('bq.end_date','<=',$end_fiscal_date)
              ->groupBy('bq.project_id')
              ->get();
  }

  public static function getBudgetExpenditure($location_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select('pro.id','pro.name','p.contract_code',DB::raw('(SUM(dac.amount) * bq.price) as total_done'))
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('procurements as p','pro.id','=','p.project_id')
              ->join('daily_activities as dac','bq.id','=','dac.bq_id')
              ->join('daily_reports as dr','dr.id','=','dac.daily_report_id')
              ->whereIn('bq.project_location_id',$location_id)
              ->where('dr.completed',1)
              ->where('dr.status',1)
              ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
              ->groupBy('pro.id')
              ->orderBy('p.contract_price')
              ->get();
  }
  
  public static function getProjectBudget($project_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities')
              ->where('project_id',$project_id)
              ->where('start_date','>=',$start_fiscal_date)
              ->where('end_date','<=',$end_fiscal_date)
              ->sum('total_price');
  }

  public static function getInvoicedBudget($location_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select(DB::raw('(pra.request_amount * bq.price) as total_invoice'))
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('daily_reports as dr','pro.id','=','dr.project_id')
              ->join('payment_request_activities as pra','bq.id','=','pra.bq_id')
              ->join('payment_requests as preq','pro.id','=','preq.project_id')
              ->whereIn('bq.project_location_id',$location_id)
              ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
              ->whereIn('preq.status',[1,2,3,4,5])
              ->where('dr.status',1)
              ->groupBy('pra.id')
              ->get()->sum('total_invoice');
  }

  public static function getPaidBudget($location_id,$start_fiscal_date,$end_fiscal_date){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select(DB::raw('(pra.request_amount * bq.price) as total_invoice'))
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('daily_reports as dr','pro.id','=','dr.project_id')
              ->join('payment_request_activities as pra','bq.id','=','pra.bq_id')
              ->join('payment_requests as preq','pro.id','=','preq.project_id')
              ->whereIn('bq.project_location_id',$location_id)
              ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
              ->where('preq.status',6)
              ->where('dr.status',1)
              ->groupBy('pra.id')
              ->get()->sum('total_invoice');
  }

  public static function getTotalPaidBudget($location_id){
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select(DB::raw('(pra.request_amount * bq.price) as total_invoice'))
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('daily_reports as dr','pro.id','=','dr.project_id')
              ->join('payment_request_activities as pra','bq.id','=','pra.bq_id')
              ->join('payment_requests as preq','pro.id','=','preq.project_id')
              ->where('bq.project_location_id',$location_id)
              ->where('preq.status',6)
              ->where('dr.status',1)
              ->groupBy('pra.id')
              ->get()->sum('total_invoice');
  }

  public static function actualCostDone($project_id){
    return DB::connection('pmis')
          ->table('daily_reports as dr')
          ->select('bq.id as bq_id',DB::raw('(SUM(dac.amount) * bq.price) as total_price'))
          ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
          ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
          ->where('bq.project_id',$project_id)
          ->where('dr.completed',1)
          ->where('dr.status',1)
          ->groupBy('dac.bq_id')
          ->get()->sum('total_price');
  }

  public static function getBudgetExpenditureOneProject($project_id,$start_fiscal_date,$end_fiscal_date,$column)
  {
    return DB::connection('pmis')
              ->table('estimations_bill_quantities as bq')
              ->select(DB::raw('(SUM(dac.amount) * bq.price) as total_done'))
              ->join('projects as pro','pro.id','=','bq.project_id')
              ->join('project_locations as prol','pro.id','=','prol.project_id')
              ->join('procurements as p','pro.id','=','p.project_id')
              ->join('daily_activities as dac','bq.id','=','dac.bq_id')
              ->join('daily_reports as dr','dr.id','=','dac.daily_report_id')
              ->whereIn($column,$project_id)
              ->where('dr.completed',1)
              ->where('dr.status',1)
              ->whereBetween('dr.report_date',[$start_fiscal_date,$end_fiscal_date])
              ->groupBy('pro.id')
              ->value('total_done');
  }

  // Get weekly base budget 
  public static function getWeeklyReport($location_id,$table){
    return DB::connection('pmis')
            ->table($table)
            ->select('id','project_location_id','duration','start_date','end_date','total_price',DB::raw('total_price/duration as val_per_day'))
            ->whereIn('project_location_id',$location_id)
            ->get();
  }

  // Get weekly actual budget 
  public static function getWeeklyReportActual($location_id,$date,$months,$total_baseline)
  {
    $end_date = addMonths($date,$months);
  
    $data = DB::connection('pmis')
          ->table('daily_reports as dr')
          ->select(
             DB::raw('SUM(dac.amount * bq.price) as actual_price')
            ,DB::raw("EXTRACT(WEEK FROM dr.report_date) as end_weeks")
            ,DB::raw("DATE_FORMAT(dr.report_date, '%Y') as year")
            ,DB::raw("CONCAT(DATE_FORMAT(dr.report_date, '%Y'),'-',EXTRACT(WEEK FROM dr.report_date)) as year_week")
          )
          ->join('daily_activities as dac','dr.id','=','dac.daily_report_id')
          ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
          ->whereIn('dr.project_location_id',$location_id)
          ->where('dr.completed',1)
          ->where('dr.status',1)
          ->whereBetween('dr.report_date',[$date,$end_date])
          ->groupBy('dr.id')
          ->get();
    return $data; 
    
  }

  // Get working budget
  public static function getWorkingBudget($id,$start_fiscal_date,$end_fiscal_date,$column){
    return DB::connection('pmis')
                ->table('estimations_bill_quantities')
                ->whereIn($column,$id)
                ->where('start_date','>=',$start_fiscal_date)
                ->where('end_date','<=',$end_fiscal_date)
                ->sum('total_price');
  }

  // Get one project working budget planed 
  public static function getWorkingBudgetPlaned($location_id)
  {
    return Billofquantities::whereIn('project_location_id',$location_id)->get();
  }


}
