<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class ElectricityTeam extends Model
{
  protected $connection = 'pmis';
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['_token'];

  public function electricities()
  {
      return $this->belongsTo('App\models\Electricities','id','electricity_id'); 
  }

  public function employees()
  {
      return $this->belongsTo('App\models\Settings\Statics\Employees','employee_id','id'); 
  }

  
}
?>