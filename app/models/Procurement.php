<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Procurement extends Model
{
  use LogsActivity;
  protected $connection = 'pmis';
  /**
   * The users that belong to the survey.
   */
   public function plan()
   {
     return $this->belongsTo('App\models\Plans','project_id','id');
   }
  //Duplicate relation of Plans with different name for report in user activity
   public function project()
   {
     return $this->belongsTo('App\models\Plans','project_id','id');
   }
  public function contract()
  {
      return $this->hasOne('App\models\Contract');
  }

  public function users()
  {
    return $this->hasOne('App\models\Authentication\Users','id','created_by');
  }
}
