<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
class Survey_measurements extends Model
{
    //Database Connection
    protected $connection = 'pmis';

    public function survey()
    {
        return $this->belongsTo('App\models\Survey','record_id','id');
    }
    
    public function static_measurement_type()
    {
        return $this->hasOne('App\models\Static_data','id','type');
    }

    public function static_measurement_unit()
    {
        return $this->hasOne('App\models\Static_data','id','unit');
    }
}
