<?php
namespace App\models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PlanLocationStatus extends Model
{
  use SoftDeletes;
  protected $connection = 'pmis';
  protected $table = 'project_location_status';
  
  /**
   * Get the request that owns the project.
   */
  public function Projects()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }
  
  public function project_locations()
  {
    return $this->belongsTo('App\models\Plans','location_id','id');
  }
  
  // User
  public function users()
  {
      return $this->belongsTo('App\models\Authentication\Users','created_by'); 
  }

  // Get Section
  public function sections()
  {
      return $this->belongsTo('App\models\Authentication\Sections','section','code'); 
  }

}


