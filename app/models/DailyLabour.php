<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;

class DailyLabour extends Model
{
     protected $connection = 'pmis';
     protected $guarded = ['_token'];

     public function labor()
     {
          return $this->belongsTo('App\models\Settings\Statics\Labor_clasification','labor_clasification','id');
     }

     /**
     * Get the DailyReport that owns the labor.
     */
    public function dailyReport()
    {
        return $this->belongsTo('App\models\Daily_report');
    }
}
