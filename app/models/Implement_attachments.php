<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Implement_attachments extends Model
{
  protected $connection = 'pmis';

  public function implement()
  {
    return $this->belongsTo('App\models\Implement','record_id','id');
  }

  public function static_data()
  {
    return $this->belongsTo('App\models\Static_data','type','id');
  }

}
?>
