<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ChangeRequestAttachment extends Model
{
    protected $connection = 'pmis';
    protected $table = 'change_request_attachment';

    public function changerequestcostScope(){
        return $this->belongsTo('App\models\ChangeRequestCostScope','record_id');
    }

    public function changerequesttime(){
        return $this->belongsTo('App\models\ChangeRequestTime','record_id');
    }

    public function changerequeststartstop(){
        return $this->belongsTo('App\models\ChangeRequestStartStop','record_id');
    }
}
