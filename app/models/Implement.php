<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Implement extends Model
{
  protected $connection = 'pmis';
  use LogsActivity;
  //Used for implements itself
  public function plan()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }
  //Duplicate relation of Plan used for reports in users Activity
  public function project()
  {
    return $this->belongsTo('App\models\Plans','project_id','id');
  }

}
?>
