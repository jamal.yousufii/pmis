<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB; 

class PaymentRequestActivity extends Model
{
     protected $connection = 'pmis';

     /**
     * Get the payment request that owns the request activity.
     */
    public function paymentRequest()
    {
        return $this->belongsTo('App\models\PaymentRequest');
    }

    /**
    * Get the Bq that owns the payment request activity.
    */
    public function billofQuantity()
    {
        return $this->belongsTo('App\models\Billofquantities','bq_id','id');
    }

    /** 
     * @Date: 2020-08-12 11:32:49 
     * @Desc: Caculate total previouse invoce of specific project   
     */
    public static function previouse_inv_total_amount($requested_activities)
    {
        $previouce_invocie = 0; 
        if(count($requested_activities)>0)
        {
            foreach($requested_activities as $item)
            {
                $previouce_invocie += $item->request_amount * $item->price; 
            }
        }

        return $previouce_invocie; 
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2020-08-17 10:25:41 
     * @Desc: Get Previouse Activity based project_id & project_location_id  
     */
    public static function get_previouse_activity($project_id,$project_location_id,$cur_preq_id=0)
    {
        $pre_activities = DB::connection('pmis')
                        ->table('payment_requests as pr')
                        ->selectRaw('sum(request_amount) as request_amount,bq_id,bq.*')
                        ->join('payment_request_activities as pra','pr.id','=','pra.payment_request_id')
                        ->join('estimations_bill_quantities as bq','pra.bq_id','=','bq.id')
                        ->where('status','!=',-1)
                        ->where('pra.project_id',$project_id)
                        ->where('pra.project_location_id',$project_location_id)
                        ->when($cur_preq_id!=0,function ($query) use($cur_preq_id) //!include current invo as prev invo 
                            {
                                return $query->where('pr.id','!=',$cur_preq_id);
                            }
                           )
                        ->groupBy('pra.bq_id')
                        ->get(); 

        return $pre_activities;                   
    }

}
