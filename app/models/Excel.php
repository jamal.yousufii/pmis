<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Excel extends Model
{
    protected $connection = 'pmis';
    protected $table = 'estimations_bill_quantities';
    //protected $fillable = ['id','name_dr','name_pa','name_en','created_by','updated_by','deleted_at','created_at','updated_at'];
    protected $fillable = ['operation_type','unit_id','amount','price','total_price','percentage','remarks'];
}
