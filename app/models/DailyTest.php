<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class DailyTest extends Model
{
     protected $connection = 'pmis';
     protected $table = 'daily_test';
     
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['daily_report_id','bill_quant_id','test_number','test_type','test_location','is_completed','test_status','description','created_by'];

    public function bill_quantity()
    {
        return $this->belongsTo('App\models\Billofquantities','bill_quant_id','id');
    }

     /**
     * Get the post that owns the comment.
     */
    public function dailyReport()
    {
        return $this->belongsTo('App\models\Daily_report');
    }
    

}

