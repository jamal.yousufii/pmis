<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskNotify extends Notification
{
    use Queueable;
    public $task_id;
    protected $message;
    protected $created_by;
    protected $redirect_url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task_id, $message, $created_by, $redirect_url)
    {
        $this->task_id      = $task_id;
        $this->message      = $message;
        $this->created_by   = $created_by;
        $this->redirect_url = $redirect_url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'task_id'       => $this->task_id,
            'task'          => '1',
            'message'       => $this->message,
            'created_by'    => $this->created_by,
            'redirect_url'  => $this->redirect_url,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
