<?php
use App\models\auth\Module;
use App\models\Authentication\Modules;
use App\models\Authentication\Users;
use App\models\Authentication\Section_deps;
use App\models\Authentication\UserDepartments;
use App\models\Authentication\UserRole;
use App\models\Authentication\ModuleUsers;
use App\models\Authentication\UserSections;
use App\models\Authentication\Roles;
use App\models\Authentication\Departments;
use App\models\Provinces;
use App\models\Districts;
use App\models\Villages;
use App\models\Authentication\LanguageLog;
use App\models\Authentication\Sections;
use App\models\Plans;
use App\models\Static_data;
use App\models\Home;
use App\models\Billofquantities;
use App\models\BillOfQuantitiesBase;
use App\models\AdjustmentRequests;
use App\models\Requests;
use App\models\CalendarHoliday;
use App\models\CalendarWorkingDay;
use App\models\Daily_activities;
use App\models\Plan_locations;
use App\models\Daily_report;
use App\User;
use App\models\Settings\Contractor;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DB;
use DateTime;
use setISODate;

function get_user_modules()
{
  $userid = userid();
  // Get User Departments
  $departments = UserDepartments::select('dep.id','dep.code')
                                  ->join('departments as dep','department_id','=','dep.id')
                                  ->where('user_id',$userid);
  // Get User Departments id and code
  $departments_id = $departments->pluck('id')->toArray();
  $departments_code = $departments->get();
  $dep_array = array();
  foreach($departments_code as $deps)
  {
    $dep_array[$deps->id] = $deps->code;
  }
  Session::put('user_deps', $dep_array);
  // Get user modules
  $user_modules = Modules::join('module_user as mu','mu.module_id','=','modules.id')
                          ->where('mu.user_id',$userid)
                          ->whereIn('mu.department_id',$departments_id);
  $modules = $user_modules->groupBy('modules.id')->orderBy('orders','asc')->get();
  $modules_id = $user_modules->pluck('modules.id')->toArray();
  // Set users roles in array of session
  if($modules_id and Session::get('UserRoles')=="")
  {
    foreach($departments_id as $deps)
    {
      // Get user sections
      $sections = Sections::select('sections.id','sections.code','dep.code as dep_id')
                            ->join('user_section as usec','section_id','=','sections.id')
                            ->join('departments as dep','usec.department_id','=','dep.id')
                            ->whereIn('sections.module_id',$modules_id)
                            ->where('usec.department_id',$deps)
                            ->where('usec.user_id',$userid)
                            ->where('sections.deleted_at',null)->get();
      if($sections)
      {
        $section_array = array();
        foreach($sections as $sec)
        {
          // Get user roles
          $roles = UserRole::select('roles.id','roles.code','user_role.department_id')
                            ->join('roles as roles','user_role.role_id','=','roles.id')
                            ->where('roles.section_id',$sec->id)
                            ->where('user_role.department_id',$deps)
                            ->where('user_role.user_id',$userid)
                            ->where('roles.deleted_at',null)->get();
          if($roles)
          {
            $section_roles = array();
            foreach($roles as $rol)
            {
              $section_roles[] = $rol->code;
            }
            Session::put("UserRoles","1");
            $section_array[$sec->code] = $section_roles;
            Session::put($sec->dep_id, $section_array);
            Session::put($sec->code, $section_roles);
          }
        }
      }
    }
  }
  return $modules;
}

function get_module_sections($code='',$dep_id,$extra_section='0',$is_tab=0)
{
  $userid = userid();
  // Decrypt Department ID
  if($dep_id!='0')
  {
    $dep_id = decrypt($dep_id);
  }
  $mod_id = Module::where('code',$code)->firstOrFail();
  //ddd($mod_id);
  if($mod_id)
  {
    $mod_id = $mod_id->id;
  }
  else
  {
    $mod_id = 0;
  }
  return Module::getSectionsByModuleAndUser($mod_id,$userid,$dep_id,$extra_section,$is_tab);
}

function get_module_sections_summary($code='',$dep_id,$extra_section='0',$is_tab=0)
{
  $userid = userid();
  // Decrypt Department ID
  if($dep_id!='0')
  {
    $dep_id = decrypt($dep_id);
  }
  $mod_id = Module::where('code',$code)->firstOrFail();
  if($mod_id){
    $mod_id = $mod_id->id;
  }else{
    $mod_id = 0;
  }

  return Module::getSectionsByModule($mod_id,$userid,$dep_id,$extra_section,$is_tab);
}

function get_departments($code='')
{
  $lang = get_language();
  // Get Module ID
  $module_id =  Modules::where('code',$code)->first()->id;
  // Get Departments By Modules
  return Departments::select('departments.id','name_'.$lang.' as name','icon','code')
                      ->leftJoin('module_deps as md','md.department_id','=','departments.id')
                      ->where('md.module_id',$module_id)
                      ->where('departments.deleted_at',null)
                      ->where('md.deleted_at',null)
                      ->orderBy('departments.orders')->get();
}

function getDepartmentnameByCode($code='')
{
  $lang = get_language();
  // Get Departments By Modules
  return Departments::select('departments.id','name_'.$lang.' as name','icon')
                      ->WhereIn('code',$code)
                      ->where('deleted_at',null)
                      ->orderBy('id')->get();
}

function get_sections($did='',$tabe_code=0)
{
  // Get Current Module From Session
  $module_code = session('current_mod');
  // Get Module ID
  $mid = Modules::where('code',$module_code)->first()->id;
  $sections = Departments::with(['sections' => function($query) use($mid,$tabe_code) {
    $query->where('module_id',$mid);
    $query->where('tab',$tabe_code);
    $query->orderBy('orders');
    $query->whereNull('tab_code');

  }])->where('id',$did)->first();
  return $sections;
}

function get_contractors($did='')
{
  // Get Current Module From Session
  $module_code = session('current_mod');
  // Get All Contractors
  $contractors = Contractor::all();
  return $contractors;
}

//Get Current Language
function get_language()
{
  $def_lang = LanguageLog::where('userid',userid())->get();
  if(COUNT($def_lang)>0)
  {
    foreach($def_lang as $item){
      $lang = $item->language;
    }
    session()->put('locale',$lang);
  }
  else
  {
    $locale = App::getLocale();
    session()->put('locale',$locale);
   }
   return Session()->get('locale');
}

//Get Current Language Name
function get_language_name($lang="dr")
{
  $selected_lang = trans('home.english');
  if($lang=="dr"){
    $selected_lang = trans('home.dari');
  }else if($lang=="pa"){
    $selected_lang = trans('home.pashto');
  }
  return $selected_lang;
}

function getModuleIDByCode($code=0)
{
  if($code){
    return Modules::select('id')->where('code',$code)->first()->id;
  }else {
    return "";
  }
}

function check_my_section($departments=0,$code="")
{
  // Get User Departments
  $userid   = userid();
  $is_admin = is_admin();
  $access = Module::check_my_section($code,$userid,$departments);
  if($access OR $is_admin=='1')
  {
    return true;
  }
  else
  {
    return false;
  }
}

function check_my_auth_section($code="")
{
  // Get User Departments
  $userid   = userid();
  $is_admin = is_admin();
  $access = Module::check_my_auth_section($code,$userid);
  if($access OR $is_admin=='1')
  {
    return true;
  }
  else
  {
    return false;
  }
}

function UserDepartments()
{
  return UserDepartments::select('department_id')->where('user_id',userid())->pluck('department_id')->toArray();
}

function check_my_contractor($contractor="")
{
  $userid   = userid();
  $is_admin = is_admin();
  $access = Module::check_my_contractor($contractor,$userid);
  if($access OR $is_admin=='1')
  {
    return true;
  }
  else
  {
    return false;
  }
}

function check_my_departments($id=0)
{
  $userid     = userid();
  $is_admin   = is_admin();
  $access = UserDepartments::where('user_id',$userid)->where('department_id',$id)->first();
  // Check if I have access to this department
  if($access OR $is_admin=='1')
  {
    return true;
  }
  else
  {
    return false;
  }
}

function check_my_main_department($id=0)
{
  $userid   = userid();
  $is_admin = is_admin();
  $access = Users::whereId($userid)->where('department_id',$id)->first();
  // Check if I have access to this department
  if($access OR $is_admin=='1')
  {
    return true;
  }
  else
  {
    return false;
  }
}

function doIHaveRole($section="",$code="")
{
  $roles = Session::get($section);
  //Check if user is admin or not
  if(is_admin()=='1')
  {
    return true;
  }
  else if($roles!="" and in_array($code,$roles))
  {
    return true;
  }
  else
  {
    return false;
  }
}

function doIHaveRoleInDep($department,$section="",$code="")
{
  // Get user departments from session
  $deps = Session::get('user_deps');
  // Get department code by id from user department session
  if(isset($deps[decrypt($department)]))
  {
    $department = $deps[decrypt($department)];
  }
  else
  {
    $department = "";
  }
  // Get user roles by user department
  $roles = Session::get($department);
  //Check if user is admin or not
  if(is_admin()=='1')
  {
    return true;
  }
  else if($roles!="" and in_array($code,$roles[$section]))
  {
    return true;
  }
  else
  {
    return false;
  }
}

function dateProvider($date="",$lang="dr")
{
  if($date != "" and $lang != "en")
  {
    $date_exp = explode("-",$date);
    $ver_date = Verta::getGregorian($date_exp[0],$date_exp[1],$date_exp[2]);
    $imp_date = implode("-",$ver_date);
    return $imp_date;
  }
  else
  {
    return $date;
  }
}

function dateCheck($date="",$lang="dr",$status=false)
{
  if($date != "" and $lang != "en" and $date != "0000-00-00")
  {
    if($status)
      return substr(Verta::instance($date),0,-3);
    else
      return substr(Verta::instance($date),0,-9);
  }
  if($date != "" and $lang != "en" and $date == "0000-00-00")
  {
    return "";
  }
  else
  {
    if($status)
      return substr($date,0,-3);
    else
      return $date;
  }
}

function yearCheck($year="",$lang="dr")
{
  if($year!=""){
    $years = array();
    if($lang != "en"){
      $item = Verta::create($year);
      return $item->year;
    }else {
      return $year;
    }
  }
}

function yearProvider($lang="dr")
{
  $years = array();
  if($lang != "en"){
    $gre_year = date('Y');
    $year = verta()->year;
    $year = $year+1;
    $gre_year = $gre_year-30;
    for($i=$year-30; $i<=$year; $i++)
    {
      $years[$gre_year+1] = $i;
      $gre_year++;
    }
    krsort($years);
  }else {
    $year = date('Y', strtotime('+1 year'));
    for($i=$year-30; $i<=$year; $i++)
    {
      $years[$i] = $i;
    }
    krsort($years);
  }
  return $years;
}

function limit_text($string)
{
  $string = strip_tags($string);
  if (strlen($string) > 220) {
      $stringCut = substr($string, 0, 320);
      $endPoint = strrpos($stringCut, ' ');
      $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
      $string .= ' ... ';
  }
  echo $string;
}

function getProvinceNameByID($id='',$lang="dr")
{

   $province = Provinces::select('name_'.$lang.' as name')->where('id',$id)->first();
   if($province)
    return $province->name;
   else
    return '';

}

function getProvinceByZone($zone='',$lang="dr")
{
   if($zone)
        return Provinces::select('name_'.$lang.' as name')->where('zone',$zone)->get();
   else
        return '';
}

function getDistrictNameByID($id='',$lang="dr")
{
  if($id!=''){
    return Districts::select('name_'.$lang.' as name')->where('id',$id)->first()->name;
  }
}

function getVillageNameByID($id='',$lang="dr")
{
  if($id!=''){
    return Villages::select('name_'.$lang.' as name')->where('id',$id)->first()->name;
  }
}

function dateDifference($date, $differenceFormat='%a',$brackets=true)
{
  if($date!=""){
    $created_date = date_create($date);
    $current_date = date_create(date('Y-m-d'));
    if($created_date>$current_date){
      $interval = date_diff($created_date, $current_date);
      $diff_date = $interval->format('%a');
      $diff_date = $diff_date * -1;
    }else if($created_date<$current_date){
      $interval = date_diff($created_date, $current_date);
      $diff_date = $interval->format('%a');
    }else {
      $interval = date_diff($created_date, $current_date);
      $diff_date = $interval->format('%a');
    }
    $verta = verta();
    if($brackets){
      return "( ".$verta->subDays($diff_date)->formatDifference()." )";
    }else {
      return $verta->subDays($diff_date)->formatDifference();
    }
  }
  else {
    return "";
  }
}

function getDepartmentNameByID($id=0,$lang="dr")
{
  $department = Departments::select('name_'.$lang.' as name')->where('id',$id)->first();
  if($department)
   return $department->name;
  else
   return '';
}

function getStaticNameByID($table="",$id='',$lang="dr")
{
  if($table!='' & $id!=''){
    return DB::table('pmis.'.$table)->select('name_'.$lang.' as name')->where('id',$id)->first()->name;
  }
}

function getTabs($tab_code="",$lang="dr")
{
  $tabs = UserSections::select('sec.*','sec.name_'.$lang.' as name')
              ->join('sections as sec','sec.id','=','user_section.section_id')
              ->join('users as user','user.id','=','user_section.user_id')
              ->where('tab_code',$tab_code)
              ->groupBy('sec.id')
              ->orderBy('sec.orders')
              ->get();
  if($tabs){
    return $tabs;
  }else {
    return false;
  }
}

function GetAllRecords($table="",$code="",$depID="",$role)
{
  // Sections with all view roles
  $section_with_all_views = array("pmis_plan","pmis_survey","pmis_design","pmis_estimation","pmis_implement","pmis_procurement");
  if($code=="pmis_request")
  {
    return Requests::where(function($query){
      if(doIHaveRoleInDep(session('current_department'),'pmis_request','pmis_request_all_view')) {
        // Can see all records
      }elseif(doIHaveRoleInDep(session('current_department'),'pmis_request','pmis_request_dep_view')) {
        // Can see records of own department
        $query->where('department',departmentid());
      }else {
        // Just seet own created record
        $query->where('created_by',userid());
      }
    })->get()->count();
  }
  elseif($code == "pmis_plan")
  {
    return Requests::whereHas('share', function($query) use ($depID, $code){
      $query->where('share_to',$depID);
      $query->where('share_to_code',$code);
    })->where(function($query) use ($depID, $code){
      if(!doIHaveRoleInDep(session('current_department'),'pmis_plan','pla_all_view'))//If user can't view department's all records
      {
        $query->whereHas('project_user', function($query) use ($depID, $code){
          //Can view assigned records
          $query->where('user_id',userid());
          $query->where('department_id',$depID);
          $query->where('section',$code);
        });
      }
    })->get()->count();
  }
  elseif($code == "pmis_project_dashboard")
  {
    return Plans::whereHas('share', function($query) use ($depID, $code){
        $query->where('share_to_code','pmis_progress');
    })->get()->count();
  }
  elseif($code == "pmis_completed")
  {
    return Plans::whereHas('project_location', function($query){
      $query->where('status','1');
    })->where('department',$depID)->get()->count();
  }
  elseif($code == "pmis_stopped")
  {
    return Plans::whereHas('project_location', function($query){
      $query->where('status','2');
    })->where('department',$depID)->get()->count();
  }
  else
  {
    if($code == "daily_report_monitoring")
    {
      $code = "pmis_progress";
    }
    return Plans::whereHas('share', function($query) use ($depID, $code){
      $query->where('share_to',$depID);
      $query->where('share_to_code',$code);
    })->whereHas('project_location', function($query){
      $query->where('status','0');
    })->where(function($query) use ($depID, $code, $role, $section_with_all_views){
      if(in_array($code,$section_with_all_views) and !doIHaveRoleInDep(session('current_department'),$code,$role))//If user can't view department's all records
      {
        $query->whereHas('project_user', function($query) use ($depID, $code){
          //Can view assigned records
          $query->where('user_id',userid());
          $query->where('department_id',$depID);
          $query->where('section',$code);
        });
      }
    })->get()->count();
  }
}

function GetAllProjects($id="")
{
  $userid = userid();
  $is_admin = is_admin();
  if($is_admin==1)
  {
    return Plans::select('projects.id')
            ->join('pmis.project_contractor as pc','projects.id','=','pc.project_id')
            ->where('pc.contractor_id',$id)
            ->where('pc.deleted_at',null)
            ->where('projects.deleted_at',null)
            ->get()->count();
  }else{
    return Plans::select('projects.id')
            ->join('pmis.project_contractor as pc','projects.id','=','pc.project_id')
            ->join('pmis.project_contractor_staff as pcs','projects.id','=','pcs.project_id')
            ->where('pc.contractor_id',$id)
            ->where('pcs.user_id',$userid)
            ->where('pc.deleted_at',null)
            ->where('projects.deleted_at',null)
            ->groupBy('projects.id')
            ->get()->count();
  }
}

//Generate URN for tables
function GenerateURN($table, $field)
{
  DB::connection('pmis')->select('CALL GenerateURN("'.$table.'", "'.$field.'",@urn)');
  $data = DB::connection('pmis')->select('SELECT @urn AS urn');
  foreach ($data as $value) {
    $URN = $value->urn;
  }
  return $URN;
}

// Get all Provinces
function getAllProvinces()
{
  return Home::getAllProvinces();
}

// Get all Districts
function getAllDistrictsByProvince($province_id)
{
  return Home::getAllDistrictsByProvince($province_id);
}

// Get all Villages
function getAllVillagesByDistrict($district_id)
{
  return Home::getAllVillagesByDistrict($district_id);
}

// Arrange all selected data from table
function arrangeSelectedData($data=array(),$column)
{
  $result_array = array();
  if($data)
  {
    foreach($data as $item)
    {
      $result_array[] = $item->$column;
    }
  }
  return $result_array;
}


/**
 * @Author: Jamal Yousufi
 * @Date: 2019-11-21 13:50:41
 * @Desc: Upload documents based on record id
 */

if(!function_exists('uploadAttachments'))
{
  function uploadAttachments($parent_id=0,$rec_id=0,$section,$file,$file_name="",$i=1,$table,$custome_data=array(),$update=false,$file_id=0,$type="")
  {
    if($table!='0')
    {
      // Getting all of the post data
      $errors = "";
      if(!empty($file))
      {
        // Validating each file.
        // $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(
          [
              'file'      => $file,
              'extension' => Str::lower($file->getClientOriginalExtension()),
          ],
          [
              'file'      => 'required|max:500000',
              'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
          ]
        );
        if($validator->passes())
        {
          // Path is pmis/public/attachments/pmis...
          $destinationPath = 'public/attachments/pmis/'.$section;
          $fileOrgName     = $file->getClientOriginalName();
          $fileExtention   = $file->getClientOriginalExtension();
          $fileOrgSize     = $file->getSize();
          if($file_name!="")
          {
            $name = $file_name;
            $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
          }else{
            $name = explode('.', $fileOrgName)[0];
            $file_name = $name."ـ".$rec_id.".".$fileExtention;//$fileOrgName;
          }
          $upload_success = $file->move($destinationPath, $file_name);
          if($upload_success)
          {
            $destinationPathForDB = 'attachments/pmis/'.$section;
            //Insert Record
            $data = [
               'parent_id'  => $parent_id,
               'record_id'  => $rec_id,
               'section'    => $section,
               'filename'   => $name,
               'path'       => $destinationPathForDB."/".$file_name,
               'extension'  => $fileExtention,
               'size'       => $fileOrgSize,
               'created_at' => Carbon::now(),
               'created_by' => userid(),
            ];
            $data = array_merge($data,$custome_data);
            // Do Operation
            DB::connection('pmis')->table($table)->insert($data);
            return true;
          }
          else
          {
            $errors .= json('error', 400);
          }
        }
        else
        {
          // Return false.
          return false;
        }
      }
      else
      {
        //Insert Record
        $data = [
          'updated_at' => Carbon::now(),
          'updated_by' => userid(),
        ];
        $data = array_merge($data,$custome_data);
        // Do Operation
        DB::connection('pmis')->table($table)->whereId($file_id)->update($data);
        return true;
      }
    }
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2019-11-21 13:50:41
 * @Desc: Upload documents based on record id
 */

if(!function_exists('uploadUpdatedAttachments'))
{
  function uploadUpdatedAttachments($parent_id=0,$rec_id=0,$section,$file,$file_name,$i=1,$table,$custome_data=array(),$file_id=0)
  {
    if($table!='0')
    {
      // Getting all of the post data
      $errors = "";
      if(!empty($file))
      {
        // Validating each file.
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(
          [
              'file'      => $file,
              'extension' => Str::lower($file->getClientOriginalExtension()),
          ],
          [
              'file'      => 'required|max:500000',
              'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
          ]
        );
        if($validator->passes())
        {
          // Path is pmis/public/attachments/pmis...
          $destinationPath = 'public/attachments/pmis/'.$section;
          $fileOrgName     = $file->getClientOriginalName();
          $fileExtention   = $file->getClientOriginalExtension();
          $fileOrgSize     = $file->getSize();
          if($file_name!="")
          {
            $name = $file_name;
            $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
          }else{
            $name = explode('.', $fileOrgName)[0];
            $file_name = $fileOrgName;
          }
          $upload_success = $file->move($destinationPath, $file_name);
          if($upload_success)
          {
            $destinationPathForDB = 'attachments/pmis/'.$section;
            //Insert Record
            $data = [
               'path'       => $destinationPathForDB."/".$file_name,
               'extension'  => $fileExtention,
               'size'       => $fileOrgSize,
               'updated_at' => Carbon::now(),
               'updated_by' => userid(),
            ];
            $data = array_merge($data,$custome_data);
            // Do Operation
            DB::connection('pmis')->table($table)->whereId($file_id)->update($data);
            return true;
          }
          else
          {
            $errors .= json('error', 400);
          }
        }
        else
        {
          // Return false.
          return false;
        }
      }
    }
  }
}

/**
  * @Author: Jamal Yousufi
  * @Date: 2019-12-02 09:38:40
  * @Desc: Get Design Attachment
  */
function getAttachments($table,$parent_id=0,$record_id=0,$section='')
{
  $query = DB::connection('pmis')->table($table)->select('*');
  if($parent_id!=0)
    $query->where('parent_id',$parent_id);
  if($record_id!=0)
    $query->where('record_id',$record_id);
  if($section!='')
    $query->where('section',$section);
    return $query->orderBy('id','asc')->get();
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2019-12-08 15:51:31
 * @Desc: return the status
 */
function architectureStatus($status,$type='text')
{
  if($type=='text')
  {
    switch ($status)
    {

      case 1:
        return trans('designs.req_reject');
      break;

      case 2:
        return trans('designs.req_approve');
      break;

      default:
        return trans('designs.req_app_rej');
      break;
    }
  }
  else
  {
    $option  = '<option value="1">'.trans('designs.req_approve').'</option>';
    switch ($status)
    {
      case 1:
        echo '<option value="2">'.trans('designs.req_reject').'</option>';
      break;

      case 2: //Its already reject approve it
        echo '<option value="1">'.trans('designs.req_approve').'</option>';
      break;

      default:
        echo $option.='<option value="2">'.trans('designs.req_reject').'</option>';
      break;
    }
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2019-12-11 09:13:09
 * @Desc: return province id from relationshp
 */
function multiSelectArchitectureLocation($result,$locationRelation)
{
  $loc_id = array();
  foreach($result->{$locationRelation} as $rs)
  {
    $loc_id[] = $rs->projectLocation->id;
  }

  return $loc_id;
}

/**
* @Author: Jamal Yousufi
* @Date: 2019-12-15 13:32:37
* @Desc: Make array for where In
*/
function forWhereIn($result)
{
  return $result = explode(',',$result);
}

/**
 * Success Message
 */
function successMessage($message)
{
  return  '<div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-check-square"></i></div>
                <div class="m-alert__text">'.$message.'</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
              </div>
            ';
}

/**
 * @Date: 2019-12-17 14:48:34
 * @Desc:  Error Message
 */
function errorMessage($message)
{
  return  '<div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                <div class="m-alert__icon"><i class="la la-warning"></i></div>
                <div class="m-alert__text">'.$message.'</div>
                <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
              </div>
            ';
}


function inserOrUpdatetAttachment($table='',$data=array(),$request)
{
  if($request->is_edit)
  {
    return $result = DB::connection('pmis')->table($table)->where('id',decrypt($request->id))->update($data);
  }
  else
  {
    return DB::connection('pmis')->table($table)->insert($data);
  }
}

function deleteFile($table,$request,$delete_file=false)
{
  if($delete_file)
  {

      $file= public_path()."/".Home::getFile($table,decrypt($request->id))->path;;
      if(File::delete($file))
      return true;
      else
      return false;
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2019-12-19 10:11:41
 * @Desc: Get Record
 */
function getRecords($con='pmis',$table,$cond=array(),$order_column='0',$order_type='desc')
{
  $query = DB::connection($con)->table($table);
  if(count($cond)>0)
    $query->where($cond);
  if($order_column!='0')
    $query->orderBy($order_column,$order_type);

  return $query->get();

}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-03-11 11:42:07
 * @Desc: Get a column from table
 */
function getColumn($con='pmis',$table,$cond=array(),$column)
{
   $query = DB::connection($con)->table($table);
    if(count($cond)>0)
      $query->where($cond);

    if($query->count()>0)
      return $query->first()->{$column};
    else
      return '';
}

// Get date from table
function getData($connection='',$table,$cond=array())
{
  $result = DB::connection($connection)->table($table)->where($cond)->get();
  if(count($result)>0)
  {
    return $result;
  }
  else
  {
    return false;
  }
}

function ddd($result)
{
  echo "<pre>"; print_r($result); exit;
}

function calculate_percentage($total_price=0,$item_price=0)
{
   return ($item_price / 100) * $total_price;
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-02-23 11:00:28
 * @Desc: Static Option
 */
function staticOption($type,$code=0)
{
  // Get Language
  $lang = get_language();
  $result = Static_data::select('name_'.$lang.' as name','code')->where('type',$type)->get();
  $option = '';
  if($result->count()>0)
  {
    foreach($result as $item)
    {
      if($code==$item->code)
      {
        $option .= '<option selected="selected" value="'.$item->code.'">'.$item->name.'</option>';
      }
      else
      {
        $option .= "<option value='".$item->code."'>".$item->name."</option>";
      }
    }

    echo $option;
  }
  else
  {
    return '';
  }

}

function userid()
{
  return Auth::user()->id;
}

function is_admin()
{
  return Auth::user()->is_admin;
}


function organizationid()
{
  return Auth::user()->organization_id;
}

function departmentid()
{
  return Auth::user()->department_id;
}

function getSectionName($code)
{
  return Sections::where('code',$code)->first()->name_dr;
}

function getSectionsByTabCode($tabCode='')
{
  // Get Current Module From Session
  $module_code = session('current_mod');
  // Get Module ID
  $mid =  Modules::where('code',$module_code)->first()->id;
  $sections = Sections::where('module_id',$mid)->where('tab_code',$tabCode)->orderBy('orders')->get();
  return $sections;
}

function getPercentage($total=0,$price=0,$unit=false,$round=3)
{
  if($total!=0)
  {
    $percentage = ($price * 100) / $total;
    $percentage = round($percentage, $round);
    if($unit){
      return $percentage."%";
    }else{
      return $percentage;
    }
  }
  else
  {
    return 0;
  }
}

function getSectionByRoute($url_route='',$lang)
{
  return Sections::select('id','name_'.$lang.' as name')->where('url_route',$url_route)->first();
}

function getSectionByCode($code='',$lang)
{
    return Sections::select('name_'.$lang.' as name')->where('code',$code)->first()->name;
}

function getContractorNameById($id=0,$value=false)
{
  if($id!=0){
    if($value){
      return Contractor::select('company_name')->where('id',$id)->first()->company_name;
    }else{
      return Contractor::select('company_name')->where('id',$id)->first();
    }
  }else{
    return "";
  }
}

/**
 * Get parent menues
 */
function getMenu($module_code='0',$department_id=0,$extra_section='0',$is_tab=0,$parent_code='0')
{
  //Get the Moudle based on the code
  $module = Module::where('code',$module_code)->firstOrFail();
  //Decrypt department id
  $department_id = ($department_id ? decrypt($department_id) : 0);
  //Get the sections based on conditions
  $menus = Sections::select('sections.*')
              ->join('user_section as us','us.section_id','=','sections.id')
              ->when($department_id!=0,function ($query) use($department_id) // Department id is 0 for system and setting
                {
                    return $query->join('section_deps as sdep','sdep.section_id','=','sections.id')
                            ->where('sdep.department_id',$department_id)
                            ->where('sdep.deleted_at',null);
                }
              )
              ->where('module_id',$module->id)
              ->whereIn('sections.tab',[0,4]) // 0 => not tabe, 4 => parent menu
              ->where('us.user_id',userid())
              ->where('sections.deleted_at',null)
              ->where(function($q) use($extra_section,$is_tab) // On specfice session bring extra section ex: project => dialy report
              {
                if($extra_section!='0' && $extra_section!='')
                {
                  $q->where('sections.tab_code',$extra_section);
                  $q->orWhereNull('sections.tab_code');
                }
                else
                {
                  $q->whereNull('sections.tab_code');
                }
              })
              ->orderBy('sections.orders')
              ->groupBy('sections.id')
              ->get();
  renderMenu($menus);
}

/**
 * Get parent menues
 */
function getSections($module_code='0',$department_id=0,$extra_section='0',$is_tab=0)
{
  //Get the Moudle based on the code
  $module = Module::where('code',$module_code)->firstOrFail();
  //Decrypt department id
  $department_id = ($department_id ? decrypt($department_id) : 0);
  //Get the sections based on conditions
  $sections = Sections::select('sections.*')
              ->join('user_section as us','us.section_id','=','sections.id')
              ->when($department_id!=0,function ($query) use($department_id) // Department id is 0 for system and setting
                {
                    return $query->join('section_deps as sdep','sdep.section_id','=','sections.id')
                            ->where('sdep.department_id',$department_id)
                            ->where('sdep.deleted_at',null);
                }
              )
              ->where('module_id',$module->id)
              ->whereIn('sections.tab',[0,4]) // 0 => not tabe, 4 => parent menu
              ->where('us.user_id',userid())
              ->where('sections.deleted_at',null)
              ->where(function($q) use($extra_section,$is_tab) // On specfice session bring extra section ex: project => dialy report
              {
                if($extra_section!='0' && $extra_section!='')
                {
                  $q->where('sections.tab_code',$extra_section);
                  $q->orWhereNull('sections.tab_code');
                }
                else
                {
                  $q->whereNull('sections.tab_code');
                }
              })
              ->orderBy('sections.orders')
              ->groupBy('sections.id')
              ->get();
  return $sections;
}

/**
 * Get child menues
 */
function getChildMenu($module_code='0',$department_id=0,$parent_code='0')
{
  //Get the Moudle based on the code
  $module = Module::where('code',$module_code)->firstOrFail();
  //Decrypt department id
  $department_id = ($department_id ? decrypt($department_id) : 0);
  //Get the sections based on conditions
  $menus = Sections::select('sections.*')
              ->join('user_section as us','us.section_id','=','sections.id')
              ->join('section_deps as sdep','sdep.section_id','=','sections.id')
              ->where('module_id',$module->id)
              ->where('sdep.department_id',$department_id)
              ->where('us.user_id',userid())
              ->where('sections.tab_code',$parent_code) //parent code
              ->where('sections.deleted_at',null)
              ->where('sdep.deleted_at',null)
              ->orderBy('sections.orders')
              ->get();

  return $menus;

}

function renderMenu($menus)
{
  $res = '';
  $lang = get_language();
  $i = 0;
  foreach ($menus as $menu)
  {
    if ($menu->tab!=4) // It has no children
    {
      $res .='<li class="m-menu__item '.(session('current_section')==$menu->code ? 'm-menu__item--active' : '' ) .'" aria-haspopup="true" >
        <a href="'.route($menu->url_route,(($menu->code=='pmis_project') || ($menu->code=='pmis_req_adjustments')) ? ['con_id'=>session('current_contractor'),'dep_id'=>session('current_department')] : session('current_department')).'" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon '.$menu->icon.'"></i><span class="m-menu__link-text">'.$menu->{'name_'.$lang}.'</span></a>
      </li>';
    }
    elseif($menu->tab==4) // It has children
    {
      //Get the child menus based on the parent
      $childMenus = getChildMenu(session('current_mod'),session('current_department'),$menu->code);
      $res .='<li class="m-menu__item m-menu__item--submenu '.( in_array(Route::currentRouteName(), childMenueRouteArray($childMenus,$menu->code)) ? 'm-menu__item--open'  : ''  ) .' " aria-haspopup="true" m-menu-submenu-toggle="hover">
          <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon '.$menu->icon.'"></i><span class="m-menu__link-text">'.$menu->{'name_'.$lang}.'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
          <div class="m-menu__submenu ">
              <span class="m-menu__arrow"></span>
              <ul class="m-menu__subnav">';
              if($childMenus)
              {
                foreach ($childMenus as $child)
                {
                  if($child->tab!=4) // Child has no children
                  {
                    $res .='<li class="m-menu__item '.(session('current_section')==$child->code ? 'm-menu__item--active' : '' ) .'" aria-haspopup="true" >
                    <a href="'.route($child->url_route).'" class="m-menu__link"><span class="m-menu__item-here"></span><i class="m-menu__link-icon '.$child->icon.'"></i><span class="m-menu__link-text">'.$child->{'name_'.$lang}.'</span></a>
                    </li>';
                  }
                  elseif($child->tab==4) // Child menu has children
                  {
                    $subchildMenus = getChildMenu(session('current_mod'),session('current_department'),$child->code);
                    $res .='<li class="m-menu__item m-menu__item--submenu  '.( in_array(Route::currentRouteName(), ['child_route']) ? 'm-menu__item--open'  : ''  ) .'" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">'.$child->{'name_dr'.$lang}.'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div claschildMenuss="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">';
                            if($subchild)
                            {
                              foreach ($subchildMenus as $subchild){
                                $res.='<li class="m-menu__item '.(Route::currentRouteName()==$subchild->url_route ? 'm-menu__item--active' : '' ) .'" aria-haspopup="true" >
                                    <a href="?page=index" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon '.$subchild->icon.'"></i><span class="m-menu__link-text">'.$subchild->{'name_'.$lang}.'</span></a>
                                </li>';
                              }
                            }
                            $res.='</ul>

                        </div>
                    </li>';
                  }
                }
              }
              $res.='</ul>
          </div>
      </li>';
    }
    $i++;
  }
  echo $res;
}

function childMenueRouteArray($childMenus,$parent_code)
{
    $extra_child_route = config('sub_menu_route.'.$parent_code);
    $child_routes_arr = $childMenus->pluck('url_route')->toArray();

    return array_merge($child_routes_arr,$extra_child_route);
}

function hasSurvey($records)
{
  if(count($records)>0)
  {
    if($records->first()->has_survey=='yes')
    {
      return 1;
    }
    else
    {
      return 2;
    }
  }
  else
  {
    return 0;
  }
}

function hasData($records)
{
  if(count($records)>0)
  {
    if($records->first()->has_data=='yes')
    {
      return 1;
    }
    else
    {
      return 2;
    }
  }
  else
  {
    return 0;
  }
}

//chek if bq is in the session array
function payment_request_bq_check($selected_bq)
{
  if($selected_bq!=NULL)
    $filtered_bq = array_map(function($selected_bq) {  return array_pop($selected_bq); }, $selected_bq);
  else
    $filtered_bq = array();


  return $filtered_bq;
}

function getSectionIDByCode($code="")
{
  if($code!=""){
    return Sections::select('id')->where('code',$code)->first()->id;
  }
  else{
    return "";
  }
}

function iteration($page=0){
  if($page>0){
    return (($page-1)*9)+$page;
  }else{
    return 0;
  }
}

// get available work days
function working_days($from, $to, $workingDays, $holidayDays)
{
  if($from!="")
  {
    $from = new DateTime($from);
    $to = new DateTime($to);
    $to->modify('+1 day');
    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period)
    {
      if (!in_array($period->format('N'), $workingDays)) continue;
      if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
      $days++;
    }
    return $days;
  }
  else
  {
    return 0;
  }
}

// Get all public holidays
function holidays($location_id)
{
  $holiday = CalendarHoliday::where('location_id',$location_id)->get();
  $holidayDays = array();
  if($holiday)
  {
      foreach($holiday as $item)
      {
          $period = new DatePeriod(
              new DateTime($item->from_date),
              new DateInterval('P1D'),
              new DateTime($item->to_date)
          );
          foreach ($period as $key => $value) {
              $holidayDays[] = $value->format('Y-m-d');
          }
      }
  }
  return $holidayDays;
}

// Get working days only
function workingDays($location_id)
{
  $workday = CalendarWorkingDay::where('location_id',$location_id)->first();
  $workingDays = array();
  if($workday)
  {
      if($workday->monday!=0){
          $workingDays[] = 1;
      }
      if($workday->tuesday!=0){
          $workingDays[] = 2;
      }
      if($workday->wednesday!=0){
          $workingDays[] = 3;
      }
      if($workday->thursday!=0){
          $workingDays[] = 4;
      }
      if($workday->friday!=0){
          $workingDays[] = 5;
      }
      if($workday->saturday!=0){
          $workingDays[] = 6;
      }
      if($workday->sunday!=0){
          $workingDays[] = 7;
      }
  }
  return $workingDays;
}

// Increase date by months
function addMonths($date,$months=1){
  if($date and $months!=0){
    $time = strtotime($date);
    return date("Y-m-d", strtotime("+".$months." month", $time));
  }else{
    return "";
  }
}

// Calculate working days to display working percentage based on working days
function calculate_working($from, $to, $workingDays, $holidayDays,$total_days)
{
  // Count working days
  if($from!="")
  {
    $from = new DateTime($from);
    $to = new DateTime($to);
    $to->modify('+1 day');
    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);
    $days = 0;
    $percentage = 0;
    foreach ($periods as $period)
    {
      if (!in_array($period->format('N'), $workingDays)) continue;
      if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
      $days++;
    }
    if($days>0 and $total_days>0){
      $percentage = ($days * 100) / $total_days;
      $percentage = round($percentage,4);
      if($percentage>100){
        $percentage = 100;
      }
    }
    return $percentage;
  }
  else
  {
    return 0;
  }
}

function getBQDone($bq_id=0,$amount=0)
{
  if($bq_id!=0 and $amount!=0)
  {
    $work = Daily_activities::where('bq_id',$bq_id)->sum('amount');
    $percentage = ($work * 100) / $amount;
    $percentage = round($percentage, 5);
    if($percentage>100)
    {
      $percentage = 100;
    }
    return $percentage;
  }
  else
  {
    return 0;
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-08-20 09:07:50
 * @Desc: project location based it passed
 */
function getProjectLocation($project_locations,$project_id=false)
{
  $lang = get_language();
  $option = '<option value="">'. trans('global.select') .'</option>';
  // Get Project Locations by Users
  if($project_id!=false) //pass project id and query location based on
   $project_locations = Plan_locations::where('project_id',$project_id)->orderBy('id','desc')->get();

  if($project_locations)
  {
    $district   = "";
    $village    = "";
    $latitude   = "";
    $longitude  = "";
    foreach($project_locations as $location)
    {
      if($location->district_id)
      {
        $district = " / ".$location->district->{'name_'.$lang};
      }
      if($location->village_id)
      {
        $village = " / ".$location->village->{'name_'.$lang};
      }
      if($location->latitude)
      {
        $latitude = " / ".$location->latitude;
      }
      if($location->longitude)
      {
        $longitude = " / ".$location->longitude;
      }
      $option .= "<option value=".$location->id.">". $location->province->{'name_'.$lang} . $district . $village . $latitude . $longitude."</option>";
    }
  }

  echo $option;
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2020-08-20 09:07:50
 * @Desc: get location of the project based on locaton
 */
function projectLocationDetails($project_location_id)
{
  $lang = get_language();
  // Get Project Locations by Users
   $project_locations = Plan_locations::find($project_location_id);

   $option     = '';
  if($project_locations)
  {
    $district   = "";
    $village    = "";
    $latitude   = "";
    $longitude  = "";

      if($project_locations->district_id)
      {
        $district = " / ".$project_locations->district->{'name_'.$lang};
      }
      if($project_locations->village_id)
      {
        $village = " / ".$project_locations->village->{'name_'.$lang};
      }
      if($project_locations->latitude)
      {
        $latitude = " / ".$project_locations->latitude;
      }
      if($project_locations->longitude)
      {
        $longitude = " / ".$project_locations->longitude;
      }
      $option .= "<span value=".$project_locations->id.">". $project_locations->province->{'name_'.$lang} . $district . $village . $latitude . $longitude."</span>";
  }
  echo $option;
}

// get available work days
function portfolioWorkDays($project_id,$from, $to)
{
  $final_working_days = 0;
  $total_working_days = 0;
  foreach($project_id as $item){
    $one_project_days = 0;
    $holidays    = holidays($item);
    $workingDays = workingDays($item);
    $one_project_days   = working_days($from, $to,$workingDays,$holidays);
    $total_working_days = $total_working_days+$one_project_days;
  }
  if($total_working_days>0 and count($project_id)>0){
    $final_working_days = $total_working_days/count($project_id);
  }
  return $final_working_days;
}

function getProportion($cfd=0,$cfd_val=0,$cfy=0,$round=3)
{
  if($cfd!=0)
  {
    $value = ($cfy * $cfd_val) / $cfd;
    $value = round($value, $round);
    return $value;
  }
  else
  {
    return 0;
  }
}

// get end date by duration of work days
function working_days_end_date($from, $workingDays, $holidayDays, $duration)
{
  //$duration = ceil($duration);
  if($from!="")
  {
    // Get previous duration
    $prev_duration = working_days($from, $from, $workingDays, $holidayDays);
    // Check if end date exist
    $date = $from;
    $new_date = "";
    $x=1;
    for($i=1; $i<=$duration; $i++) {
        if($x==1){
            $new_date = date('Y-m-d', strtotime($date));
        }else{
          if($new_date==""){
            $new_date = date('Y-m-d', strtotime($date. ' + 1 days'));
          }else{
            $new_date = date('Y-m-d', strtotime($new_date. ' + 1 days'));
          }
        }
        $converted_date = new DateTime($new_date);
        if (!in_array($converted_date->format('N'), $workingDays)){
          $x++;
          $i--; continue;
        }
        if (in_array($converted_date->format('Y-m-d'), $holidayDays)){
          $x++;
          $i--; continue;
        }
        $date = $new_date;
        $x++;
    }
    return dateCheck($date,get_language());
  }else{
    return "";
  }
}

// get end date by duration of work days
function endDate_workingDays($from, $workingDays, $holidayDays, $duration)
{
  if($from!="")
  {
    $date = $from;
    // Check if end date exist
    $custom_date = "";
    for($i=1; $i<=$duration; $i++) {
      $custom_date = date('Y-m-d', strtotime($date. ' + 1 days'));
      $converted_date = new DateTime($custom_date);
      if(in_array($converted_date->format('N'), $workingDays) && !in_array($converted_date->format('Y-m-d'), $holidayDays)){
        $date = $custom_date;
      }else{
        $i--; continue;
      }
    }
    return dateCheck($date,get_language());
  }else{
    return "";
  }
}

// Get one project budget in current fiscal year
function getProjectBudget($project_id,$start_fiscal_date,$end_fiscal_date)
{
  $data = Billofquantities::getProjectBudget($project_id,$start_fiscal_date,$end_fiscal_date);
  if($data){
    return $data;
  }else{
    return 0;
  }
}

// Get project actual performance in field
function actualCostDone($project_id)
{
  return Billofquantities::actualCostDone($project_id);
}

// Get one project budget expenditure
function getBudgetExpenditureOneProject($project_id,$start_fiscal_date,$end_fiscal_date,$column)
{
  $data = Billofquantities::getBudgetExpenditureOneProject($project_id,$start_fiscal_date,$end_fiscal_date,$column);
  if($data){
    return $data;
  }else{
    return 0;
  }
}

// Get one project planed budget
function getPlanedBudget($id,$start_fiscal_date,$end_fiscal_date,$column="")
{
  return BillOfQuantitiesBase::getPlanedBudget($id,$start_fiscal_date,$end_fiscal_date,$column);
}

// Get project first and last date from planed schedule
function getFirstLastDateOfProject($id,$condition,$column)
{
  return BillOfQuantitiesBase::getFirstLastDateOfProject($id,$condition,$column);
}

// get date by work days
function getDateByWrokginDays($workingDays, $weekDays, $holidayDays)
{
  if($workingDays!=0)
  {
    $start_date = date('Y-m-d');
    for($i=1; $i<=$workingDays; $i++) {
      $start_date = date('Y-m-d', strtotime($start_date. ' + 1 days'));
      $converted_date = new DateTime($start_date);
      if(!in_array($converted_date->format('N'), $weekDays)){
        $i--; continue;
      }
      if(in_array($converted_date->format('Y-m-d'), $holidayDays)){
        $i--; continue;
      }
    }
    return $start_date;
  }else{
    return "";
  }
}

// Get Expenditure Budget
function getBudgetDone($location_id)
{
  return BillOfQuantities::getBudgetDone($location_id);
}

// Get Expenditure Budget
function getBudgetDone_new($location_id,$start_date,$end_date)
{
  return BillOfQuantities::getBudgetDone_new($location_id,$start_date,$end_date);
}


// Get Base Budget
function getBudgetBase($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantitiesBase::getBudgetBase($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Sort Budget of current fiscal year
function sortBudgetDataCfy($data,$WeekDays,$holidays,$lastDateOfProject=0)
{
  //return $data;
  $x=1;
  $result = array();
  // Set default values
  for($i=0; $i<=11; $i++)
  {

    $x   = str_pad($x, 2, '0', STR_PAD_LEFT);
    $start_date = Config::get('static.fiscal_year_months.'.$x.'.start_date');
    $end_date   = Config::get('static.fiscal_year_months.'.$x.'.end_date');
    // Set the date tile the project is not completed
    if($lastDateOfProject !=0 && $start_date > $lastDateOfProject && $end_date > $lastDateOfProject)
    {
      break;
    }

    $month_price = 0;
    $month_wrkd  = 0;
    $item_id = array();
    $y = 0;
    foreach($data as $item)
    {
      if($item['start_date'] >= $start_date && $item['end_date'] <= $end_date)
      {
          //activity  currate is in the range of month
          $month_wrkd = working_days($item['start_date'],$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
          $month_price  +=  $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['id'] = $item['id'];
          $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['work_day'] = $month_wrkd;
          $item_id[$y]['val_per_day'] = $item['val_per_day'];

      }
      else
      {
          // start date and end date of activity is out of current month
          if($item['start_date'] < $start_date && $item['end_date'] > $end_date)
          {
              $month_wrkd = working_days($start_date,$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price  +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          elseif($item['start_date'] <= $start_date && $item['end_date'] >= $start_date &&  $item['end_date'] <= $end_date)
          {
              // $end_date   = $item['end_date'];
              // custome date range working days
              $month_wrkd      =  working_days($start_date,$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price    +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          elseif($item['start_date'] >= $start_date && $item['start_date'] <= $end_date && $item['end_date'] >= $end_date)
          {
              // custome date range working days
              $month_wrkd   =  working_days($item['start_date'],$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price  +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          else
          {
            $in_range        = 0;
          }
      }
      $y++;
    }

    if(!array_key_exists($x,$result))
    {
      $result[$x]['total_price'] = $month_price;
      $result[$x]['month']       = $x;
      $result[$x]['workday']       = $month_wrkd;
    }
    $x++;
  }

  return $result;

}

// Sort Budget of from start of project to the current fiscal year
function sortBudgetStartProToCfy($data,$project_start_date,$end_fiscal_date,$WeekDays,$holidays,$count_pre_amount=true,$lastDateOfProject=0)
{

  $end_prv_year = Config::get('static.fiscal_year_months.01.start_date');
  $pre_amount  = 0;
  $check_range = array();
  $y=0;
  if($count_pre_amount)
  {
    foreach($data as $item)
    {
      // In rage of previouse year
      if($item['end_date'] < $end_prv_year)
      {
        $range_wrkd = working_days($item['start_date'],$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
        $pre_amount += $item['val_per_day'] * $range_wrkd;

      }
      // act end date is out of previouse year but in range of current fiscal year
      elseif($item['start_date'] < $end_prv_year && $item['end_date'] > $end_prv_year)
      {
        $range_wrkd = working_days($item['start_date'],$end_prv_year,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
        $pre_amount += $item['val_per_day'] * $range_wrkd;

      }
      $y++;
    }
  }
  $x=1;
  $result = array();
  // Set default values
  for($i=0; $i<=11; $i++)
  {
    $x = str_pad($x, 2, '0', STR_PAD_LEFT);
    $start_date = Config::get('static.fiscal_year_months.'.$x.'.start_date');
    $end_date   = Config::get('static.fiscal_year_months.'.$x.'.end_date');
     // Set the date tile the project is not completed
    if($lastDateOfProject !=0 && $start_date > $lastDateOfProject && $end_date > $lastDateOfProject)
    {
      break;
    }
    $month_price = 0;
    $item_id = array();
    $y = 0;
    $month_wrkd = 0;
    foreach($data as $item)
    {

      if($item['start_date'] >= $start_date && $item['end_date'] <= $end_date)
      {
          //activity  currate is in the range of month
          $month_wrkd = working_days($item['start_date'],$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
          $month_price  +=  $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['id'] = $item['id'];
          $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['work_day'] = $month_wrkd;
      }
      else
      {
          // start date and end date of activity is out of current month
          if($item['start_date'] < $start_date && $item['end_date'] > $end_date)
          {
              $month_wrkd = working_days($start_date,$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price  +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
          }
          // Start date out of range & End date is in range
          elseif($item['start_date'] <= $start_date && $item['end_date'] >= $start_date &&  $item['end_date'] <= $end_date)
          {
              // custome date range working days
              $month_wrkd      =  working_days($start_date,$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price    +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
          }
          // End date out of range & Start date is in range
          elseif($item['start_date'] >= $start_date && $item['start_date'] <= $end_date && $item['end_date'] >= $end_date)
          {
              // custome date range working days
              $month_wrkd   =  working_days($item['start_date'],$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price    +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
          }
          else
          {
            $in_range        = 0;
          }
      }
      $y++;
    }
    if(!array_key_exists($x,$result))
    {
      $result[$x]['total_price'] = ($x=='01' ? $month_price + $pre_amount : $month_price);
      $result[$x]['month']       = $x;
      $result[$x]['start_date']  = $start_date;
      $result[$x]['end_date']       = $end_date;
      $result[$x]['workday']       = $month_wrkd;
    }
    $x++;
  }
  return $result;
}

// Get Base Budget from start to end of project
function getBudgetBaseStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantitiesBase::getBudgetBaseStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get Base Budget from start of the project to the current Fiscal Year
function getBudgetBaseStartProjectToEndCfy($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantitiesBase::getBudgetBaseStartProjectToEndCfy($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get Working Budget
function getBudgetWorking($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getBudgetWorking($location_id,$start_fiscal_date,$end_fiscal_date);
}
// Get Working Budget
function getBudgetWorkingStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getBudgetWorkingStartToEnd($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get Actual Budget
function getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get project Share budget
function getBudgetShare($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getBudgetShare($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get project expenditure budget
function getBudgetExpenditure($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getBudgetExpenditure($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get project invoiced budget
function getInvoicedBudget($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getInvoicedBudget($location_id,$start_fiscal_date,$end_fiscal_date);
}

// Get project paid budget
function getPaidBudget($location_id,$start_fiscal_date,$end_fiscal_date)
{
  return BillOfQuantities::getPaidBudget($location_id,$start_fiscal_date,$end_fiscal_date);
}
// Get project total paid budget
function getTotalPaidBudget($location_id)
{
  return BillOfQuantities::getTotalPaidBudget($location_id);
}

// Get weekly base budget
function getWeeklyReport($location_id,$table)
{
  return BillOfQuantities::getWeeklyReport($location_id,$table);
}

// Get weekly base budget
function getWeeklyReportActual($location_id,$date,$month,$total_baseline)
{
  return BillOfQuantities::getWeeklyReportActual($location_id,$date,$month,$total_baseline);
}

// Get one project working budget
function getWorkingBudget($id,$start_fiscal_date,$end_fiscal_date,$column="")
{
  return BillOfQuantities::getWorkingBudget($id,$start_fiscal_date,$end_fiscal_date,$column);
}

function setCfyDateRange($data,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays)
{
  $data_array = array();
  $act_fy_s_date   = '';
  $act_fy_e_date   = '';
  $act_fy_month    = '';
  $total_price     = 0;
  $custome_wrkd    = 0;
  $in_range        = 0;
  $act_val_per_day = 0;
  $i = 0;
  if($data)
  {
      foreach($data as $item)
      {
          if($item->start_date >= $start_fiscal_date && $item->end_date <= $end_fiscal_date)
          {
              //activity d currate is in the range of physcalent fiscal year
              $total_price     = $item->total_price;
              $act_val_per_day = $item->total_price / $item->duration;
              $act_fy_s_date   =  $item->start_date;
              $act_fy_e_date   =  $item->end_date;
              $act_fy_month    =   date("m", strtotime($item->end_date));
              $in_range        = 'fiscal_year_range';
          }
          else
          {
              // start date or end date of activity is out of current fiscal year
              if($item->start_date < $start_fiscal_date && $item->end_date > $end_fiscal_date)
              {
                  $cfy_wrkd        = working_days($start_fiscal_date,$end_fiscal_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
                  $act_fy_s_date   = $start_fiscal_date;
                  $act_fy_e_date   = $end_fiscal_date;
                  $act_fy_month    = date("m", strtotime($end_fiscal_date));
                  $act_val_per_day = $item->total_price / $item->duration;
                  $total_price     = $act_val_per_day * $cfy_wrkd;
                  $in_range        = 'start_end_out_range';
              }
              elseif($item->start_date <= $start_fiscal_date && $item->end_date >= $start_fiscal_date &&  $item->end_date <= $end_fiscal_date)
              {
                  $act_fy_s_date = $start_fiscal_date;
                  $act_fy_e_date   = $item->end_date;
                  $act_fy_month    = date("m", strtotime($item->end_date));
                  // custome date range working days
                  $custome_wrkd    =  working_days($start_fiscal_date,$item->end_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
                  $act_val_per_day = $item->total_price / $item->duration;
                  $total_price     = $act_val_per_day * $custome_wrkd;
                  $in_range        = 'start_out_range';

              }
              elseif($item->start_date >= $start_fiscal_date && $item->start_date <= $end_fiscal_date && $item->end_date >= $end_fiscal_date)
              {

                  $act_fy_s_date  = $item->start_date;
                  $act_fy_e_date  = $end_fiscal_date;
                  $act_fy_month   = date("m", strtotime($end_fiscal_date));
                  // custome date range working days
                  $custome_wrkd =  working_days($item->start_date,$end_fiscal_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
                  $act_val_per_day = $item->total_price / $item->duration;
                  $total_price = $act_val_per_day * $custome_wrkd;
                  $in_range        = 'end_out_range';
              }
              else
              {
                $act_fy_s_date   = $item->start_date;
                $act_fy_e_date   = $item->end_date;
                $act_fy_month    = 0;
                $custome_wrkd    =  0;
                $act_val_per_day = 0;
                $total_price     = 0;
                $in_range        = 0;
              }

          }

          $data_array[$i]['start_date'] = $act_fy_s_date;
          $data_array[$i]['end_date'] =  $act_fy_e_date;
          $data_array[$i]['month']    =  $act_fy_month;
          $data_array[$i]['total_price'] = $item->total_price;
          $data_array[$i]['calculate_price'] = $total_price;
          $data_array[$i]['duration']    = $item->duration;
          $data_array[$i]['act_date_range_wrk']    = $custome_wrkd;
          $data_array[$i]['val_per_day']    = $act_val_per_day;
          $data_array[$i]['id']    = $item->id;
          $data_array[$i]['in_range']    = $in_range;
          $data_array[$i]['project_location_id'] = $item->project_location_id;
          $i++;
      }
  }
  if(count($data_array) > 0)
  {
    return $data_array;
  }
  else
  {
      return array();
  }

}

// Sort Budget of from start of project to the current fiscal year
function sortBudgetActual($data,$start_fiscal_date,$end_fiscal_date,$count_pre_amount=true,$lastDateOfProject=0)
{
  $end_prv_year = Config::get('static.fiscal_year_months.01.start_date');
  $pre_amount  = 0;
  $y=0;
  if($count_pre_amount)
  {
    foreach($data as $item)
    {
      // In rage of previouse year
      if($item->date < $end_prv_year)
      {
        $pre_amount += $item->total_price;
      }
      $y++;
    }
  }
  $m = substr(dateCheck(date('Y-m-d'),'dr'),5,2);
  $m += 2;
  $x=1;
  $result = array();
  // Set default values
  for($i=0; $i<=$m; $i++)
  {
    $x   = str_pad($x, 2, '0', STR_PAD_LEFT);
    $start_date = Config::get('static.fiscal_year_months.'.$x.'.start_date');
    $end_date   = Config::get('static.fiscal_year_months.'.$x.'.end_date');
    // Set the date tile the project is not completed
    if($lastDateOfProject !=0 && $start_date > $lastDateOfProject && $end_date > $lastDateOfProject)
    {
      break;
    }

    $month_price = 0;
    $y = 0;
    foreach($data as $item)
    {

      if($item->date >= $start_date && $item->date <= $end_date)
      {
          //activity  currate is in the range of month
          $month_price  +=  $item->total_price;
      }
      $y++;
     }
    if(!array_key_exists($x,$result))
    {
      $result[$x]['total_price'] = ($x=='01' ? $month_price + $pre_amount : $month_price);
      $result[$x]['month']       = $x;
      $result[$x]['start_date']  = $start_date;
      $result[$x]['end_date']    = $end_date;
    }
    $x++;
  }
  return $result;
}

function getStartAndEndDateOfWeek($week, $year) {
  $dto = new DateTime();
  $dto->setISODate($year, $week);
  $result['week_start'] = $dto->format('Y-m-d');
  $dto->modify('+6 days');
  $result['week_end'] = $dto->format('Y-m-d');
  return $result;
}

// Sort weekly report from
function sortWeeklyReport($data,$date,$month,$WeekDays,$holidays)
{
  $x=1;
  $result = array();

  $start_date = strtotime($date);
  $end_date   = strtotime(addMonths($date,$month));
  $weeks = array();
  $i=0;
  while ($start_date < $end_date)
  {
    $weeks[$i]['week'] = date('W', $start_date);
    $weeks[$i]['year'] = date('Y', $start_date);
    $start_date += strtotime('+1 week', 0);
    $week_start_end  = getStartAndEndDateOfWeek($weeks[$i]['week'],$weeks[$i]['year']);
    $week_start_date = $week_start_end['week_start'];
    $week_end_date   = $week_start_end['week_end'];
    $weeks[$i]['week_start_date'] = $week_start_date;
    $weeks[$i]['week_end_date']   = $week_end_date;
    $week_price = 0;
    $in_range  = 0;
    foreach($data as $item)
    {
      // start date and end date of activity is in range of current week
      if($item->start_date >= $week_start_date && $item->end_date <= $week_end_date)
      {
          //activity  currate is in the range of week
          $week_price +=  $item->total_price;
          $in_range   = 'start_end_inrange';
      }
      else
      {
          // start date and end date of activity is out of current week
          if($item->start_date < $week_start_date && $item->end_date > $week_end_date)
          {
              $week_wrkd = working_days($week_start_date,$week_end_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
              $week_price +=  $item->val_per_day * $week_wrkd;
              $in_range   = 'start_end_out_range';
          }
          // start date is out of range and  end date of activity is out of current week
          elseif($item->start_date <= $week_start_date && $item->end_date >= $week_start_date &&  $item->end_date <= $week_end_date)
          {
              // custome date range working days
              $week_wrkd  =  working_days($week_start_date,$item->end_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
              $week_price +=  $item->val_per_day * $week_wrkd;
              $in_range   = 'start_out_range';
          }
          elseif($item->start_date >= $week_start_date && $item->start_date <= $week_end_date && $item->end_date >= $week_end_date)
          {
              // custome date range working days
              $week_wrkd   =  working_days($item->start_date,$week_end_date,$WeekDays[$item->project_location_id],$holidays[$item->project_location_id]);
              $week_price    +=  $item->val_per_day * $week_wrkd;
              $in_range = 'end_out_range';
          }
          else
          {
              $week_price += 0;
          }
      }
    }
    $weeks[$i]['total_price']   = $week_price;
    $i++;
  }
  return $weeks;
}

// Sort weekly report from
function sortWeeklyReportActual($data,$date,$month)
{
  // return $data;
  $x=1;
  $result = array();

  $start_date = strtotime($date);
  $end_date   = strtotime(addMonths($date,$month));
  $weeks = array();
  $i=0;
  while ($start_date < $end_date)
  {
    $weeks[$i]['week'] = date('W', $start_date);
    $weeks[$i]['year'] = date('Y', $start_date);
    $weeks[$i]['year_week'] = date('Y', $start_date).'-'.date('W', $start_date);
    $start_date += strtotime('+1 week', 0);
    $week_start_end  = getStartAndEndDateOfWeek($weeks[$i]['week'],$weeks[$i]['year']);
    $week_start_date = $week_start_end['week_start'];
    $week_end_date   = $week_start_end['week_end'];
    $weeks[$i]['week_strat_date'] = $week_start_date;
    $weeks[$i]['week_end_date']   = $week_end_date;
    $week_price = 0;
    foreach($data as $item)
    {
      if($item->year_week == $weeks[$i]['year_week'])
      {
          //activity  currate is in the range of week
          $week_price  +=  $item->actual_price;
      }

    }
    $weeks[$i]['total_price']   = $week_price;
    $i++;
  }

  return $weeks;


}

// Get all public holidays
function holidaysAll($location_id)
{
  $holiday = CalendarHoliday::whereIn('location_id',$location_id)->get();
  $holidayDays = array();
  if($location_id)
  {
    foreach($location_id as $item)
    {
      $holidayDays[$item] = array();
    }
  }

  return $holidayDays;
  if($holiday)
  {
      foreach($holiday as $item)
      {
          $period = new DatePeriod(
              new DateTime($item->from_date),
              new DateInterval('P1D'),
              new DateTime($item->to_date)
          );
          foreach ($period as $key => $value) {
            $holidayDays[$item->location_id][] = $value->format('Y-m-d');
          }
      }
  }

  return $holidayDays;
}

// Get working days only
function weekDaysAll($location_id)
{
  $workingDays = array();
  if($location_id)
  {
    foreach($location_id as $item)
    {
      $workingDays[$item] = array();
    }
  }

  for($i=0; $i<count($location_id); $i++)
  {
    $workday = CalendarWorkingDay::where('location_id',$location_id[$i])->first();
    if($workday)
    {
      if($workday->monday!=0){
          $workingDays[$workday->location_id][] = 1;
      }
      if($workday->tuesday!=0){
          $workingDays[$workday->location_id][] = 2;
      }
      if($workday->wednesday!=0){
          $workingDays[$workday->location_id][] = 3;
      }
      if($workday->thursday!=0){
          $workingDays[$workday->location_id][] = 4;
      }
      if($workday->friday!=0){
          $workingDays[$workday->location_id][] = 5;
      }
      if($workday->saturday!=0){
          $workingDays[$workday->location_id][] = 6;
      }
      if($workday->sunday!=0){
          $workingDays[$workday->location_id][] = 7;
      }
    }
  }
  return $workingDays;
}

// Get one project working budget planed
function getWorkingBudgetPlaned($location_id)
{
  return BillOfQuantities::getWorkingBudgetPlaned($location_id);
}

/**
 * @Date: 2020-09-30 10:46:39
 * @Desc: Show Recieve and reject button of payments based on the rule and status of the payments
 */
function canRejectRecievePayments($payment_status=0)
{
  if($payment_status!=-1 && $payment_status!=-6)
  {
    if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_approve_payment') || doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_approve_ensijam') && in_array($payment_status,[0,1]) || doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_approve_procurment') && in_array($payment_status,[2,3]) || doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_approve_mali') && in_array($payment_status,[3,4]))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

function differenceBetweenDate($sdate, $edate, $differenceFormat='%a',$forma=false)
{
  $sdate = date('Y-m-d');
  if($sdate!=""){
    $start_date = date_create($sdate);
    $end_date = date_create($edate);
    if($start_date>$end_date){
      $interval = date_diff($start_date, $end_date);
      $diff_date = $interval->format('%a');
      $diff_date = $diff_date * -1;
    }else if($start_date<$end_date){
      $interval = date_diff($start_date, $end_date);
      $diff_date = $interval->format('%a');
    }else {
      $interval = date_diff($start_date, $end_date);
      $diff_date = $interval->format('%a');
    }
    $verta = verta();
    if($forma){
      return $verta->addDays($diff_date)->formatDifference().' ('.$diff_date.' '. __("calendar.day").')';
    }else{
      return $verta->addDays($diff_date)->formatDifference();
    }
  }
  else {
    return "";
  }
}

function getDepartmentCodeByID($id=0)
{
    $department = Departments::select('code')->where('id',$id)->first();
    if($department)
        return $department->code;
    else
        return '';
}

// Get Expenditure Budget
function getTotalPrice($location_id)
{
    return Billofquantities::whereIn('project_location_id',$location_id)->get()->sum('total_price');
}

// Get total budgets by all locations
function getTotalPriceByLocations($location_id)
{
    $price = Billofquantities::select(DB::raw('IFNULL(SUM(total_price),0) as total_price'), 'project_location_id')->whereIn('project_location_id',$location_id)->groupBy('project_location_id')->get()->toArray();
    $price_by_location = array();
    if($price){
        foreach($price as $item){
            $price_by_location[$item['project_location_id']] = $item['total_price'];
        }
    }
    return $price_by_location;
}

function getOneLocationBudgetDone($location_id){
    $done = Daily_report::select(DB::raw('(SUM(dac.amount) * bq.price) as total_done'), 'daily_reports.project_location_id')
        ->where('daily_reports.completed',1)
        ->where('daily_reports.status',1)
        ->join('daily_activities as dac','daily_reports.id','=','dac.daily_report_id')
        ->join('estimations_bill_quantities as bq','dac.bq_id','=','bq.id')
        ->whereIn('bq.project_location_id',$location_id)
        ->groupBy('dac.bq_id', 'daily_reports.project_location_id')
        ->get()->toArray();
    $done_by_location = array();
    if($done){
        foreach($done as $item){
            if(isset($done_by_location[$item['project_location_id']])){
                $done_by_location[$item['project_location_id']] = $done_by_location[$item['project_location_id']]+$item['total_done'];
            }else{
                $done_by_location[$item['project_location_id']] = $item['total_done'];
            }
        }
    }
    return $done_by_location;
}


function getModulesByDepartment($department,$lang)
{
    return Modules::select('modules.id','modules.name_'.$lang.' as name','deps.id as dep_id')
                ->join('module_deps as md','md.module_id','=','modules.id')
                ->join('departments as deps','md.department_id','=','deps.id')
                ->where('md.deleted_at',null)
                ->groupBy('modules.id')
                ->where('deps.id',$department)->get();
}

function getUserModulesByDepartment($id,$department_id)
{
  return ModuleUsers::where('user_id',$id)->where('department_id',$department_id)->pluck('module_id')->toArray();
}

function getSectionsByModules($department,$modules,$lang)
{
    $section_data = Sections::select('deps.id as dep_id','deps.name_'.$lang.' as dep_name','sections.module_id','mod.name_'.$lang.' as module_name','sections.id','sections.name_'.$lang.' as name')
                    ->join('section_deps as sd','sd.section_id','=','sections.id')
                    ->join('departments as deps','sd.department_id','=','deps.id')
                    ->join('modules as mod','sections.module_id','=','mod.id')
                    ->where('deps.id',$department)
                    ->whereIn('mod.id',$modules)
                    ->orderBy('mod.id','asc')
                    ->orderBy('sections.orders','asc')->get();
    $sections = array();
    if($section_data)
    {
        foreach($section_data as $item)
        {
        if($item->module_id!="0"){
            $sections[$item->module_id.'-'.$item->module_name][] = $item->id."-".$item->name;
        }
        }
    }
    return $sections;
}

function getUserSectionsByModules($id,$department_id)
{
    return UserSections::where('user_id',$id)->where('department_id',$department_id)->pluck('section_id')->toArray();
}

function getRolesBySections($sections_id,$department_id)
{
    $lang = get_language();
    $data = Roles::select('roles.id','roles.name_'.$lang.' as name','roles.code','s.name_'.$lang.' as section')
                  ->join('sections as s','roles.section_id','=','s.id')
                  ->join('section_deps as sd','sd.section_id','=','s.id')
                  ->whereIn('sd.section_id',$sections_id)
                  ->where('sd.department_id',$department_id)
                  ->where('roles.deleted_at',null)
                  ->get();
    $roles = array();
    if($data)
    {
      foreach($data as $item)
      {
        $roles[$item->section][] = $item->id."-".$item->name;
      }
    }
    return $roles;
}

function getUserRolesBySections($id,$department_id)
{
  return UserRole::where('user_id',$id)->where('department_id',$department_id)->pluck('role_id')->toArray();
}

// get available work days
function getDays($from, $to)
{
  if($from!="")
  {
    $from = new DateTime($from);
    $to = new DateTime($to);
    $to->modify('+1 day');
    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period)
    {
      $days++;
    }
    return $days;
  }
  else
  {
    return 0;
  }
}

/**
 * @Author: Jamal Yousufi
 * @Date: 2019-11-21 13:50:41
 * @Desc: Upload documents based on record id
 */

if(!function_exists('uploadAttachmentsMultiRecords'))
{
  function uploadAttachmentsMultiRecords($section,$file)
  {
      $data = array();
      if(!empty($file))
      {
        // Validating each file.
        $validator = Validator::make(
          [
              'file'      => $file,
              'extension' => Str::lower($file->getClientOriginalExtension()),
          ],
          [
              'file'      => 'required|max:500000',
              'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
          ]
        );
        if($validator->passes())
        {
          // Path is pmis/public/attachments/pmis...
          $destinationPath = 'public/attachments/pmis/'.$section;
          $fileOrgName     = $file->getClientOriginalName();
          $fileExtention   = $file->getClientOriginalExtension();
          $fileOrgSize     = $file->getSize();
          $name = explode('.', $fileOrgName)[0];
          $file_name = $name.".".$fileExtention;
          $upload_success = $file->move($destinationPath, $file_name);
          if($upload_success)
          {
            $data = array(
              'path'          => 'attachments/pmis/'.$section."/".$file_name,
              'fileOrgName'   => $fileOrgName,
              'fileExtention' => $fileExtention,
              'fileOrgSize'   => $fileOrgSize,
              'name'          => $name,
              'section'       => $section,
            );
          }
        }
      }
      return $data;
  }
}

// Sort Budget of current fiscal year
function sortBudgetDataFromStartToNow($data,$start_date,$end_date,$WeekDays,$holidays,$lastDateOfProject=0)
{
  //return $data;
  $x=1;
  $result = array();
  // Set default values
  for($i=0; $i<=11; $i++)
  {

    $x = str_pad($x, 2, '0', STR_PAD_LEFT);
    // Set the date tile the project is not completed
    if($lastDateOfProject !=0 && $start_date > $lastDateOfProject && $end_date > $lastDateOfProject)
    {
      break;
    }

    $month_price = 0;
    $month_wrkd  = 0;
    $item_id = array();
    $y = 0;
    foreach($data as $item)
    {
      if($item['start_date'] >= $start_date && $item['end_date'] <= $end_date)
      {
          //activity  currate is in the range of month
          $month_wrkd = working_days($item['start_date'],$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
          $month_price  +=  $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['id'] = $item['id'];
          $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
          $item_id[$y]['work_day'] = $month_wrkd;
          $item_id[$y]['val_per_day'] = $item['val_per_day'];

      }
      else
      {
          // start date and end date of activity is out of current month
          if($item['start_date'] < $start_date && $item['end_date'] > $end_date)
          {
              $month_wrkd = working_days($start_date,$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price  +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          elseif($item['start_date'] <= $start_date && $item['end_date'] >= $start_date &&  $item['end_date'] <= $end_date)
          {
              // $end_date   = $item['end_date'];
              // custome date range working days
              $month_wrkd      =  working_days($start_date,$item['end_date'],$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price    +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          elseif($item['start_date'] >= $start_date && $item['start_date'] <= $end_date && $item['end_date'] >= $end_date)
          {
              // custome date range working days
              $month_wrkd   =  working_days($item['start_date'],$end_date,$WeekDays[$item['project_location_id']],$holidays[$item['project_location_id']]);
              $month_price  +=  $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['id'] = $item['id'];
              $item_id[$y]['price'] = $item['val_per_day'] * $month_wrkd;
              $item_id[$y]['work_day'] = $month_wrkd;
              $item_id[$y]['val_per_day'] = $item['val_per_day'];
          }
          else
          {
            $in_range        = 0;
          }
      }
      $y++;
    }

    if(!array_key_exists($x,$result))
    {
      $result[$x]['total_price'] = $month_price;
      $result[$x]['month']       = $x;
      $result[$x]['workday']       = $month_wrkd;
    }
    $x++;
  }

  return $result;

}

?>
