<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',//comment added
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function modules()
    {
        return $this->belongsToMany('App\models\auth\Module');
    }

    // Get the roles based on the MANY OT MANY relation 
    function roles()
    {
      return $this->belongsToMany('App\models\Authentication\Roles','user_role','user_id','role_id'); 
    }

    public function department()
    {
      return $this->hasOne('App\models\Authentication\Departments','id','department_id');
    }

    public static function getUserRoleCode($code="")
    {
      $data = array();
      $record = DB::table('users as u')->select('u.id')->join('user_role as ur','ur.user_id','=','u.id')->join('roles as rol','ur.role_id','=','rol.id')->where('rol.code',$code)->where('rol.deleted_at',null)->groupBy('u.id')->get();
      foreach($record as $item){
        $data[] = $item->id;
      }
      return $data;
    }

    public static function getUserRoleCode_new($code="",$users=array())
    {
      $data = array();
      $record = DB::table('users as u')->select('u.id')->join('user_role as ur','ur.user_id','=','u.id')->join('roles as rol','ur.role_id','=','rol.id')->where('rol.code',$code)->whereIn('u.id',$users)->where('rol.deleted_at',null)->groupBy('u.id')->get();
      foreach($record as $item){
        $data[] = $item->id;
      }
      return $data;
    }
}
