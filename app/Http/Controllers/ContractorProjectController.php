<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\session;
use App\models\Plans;
use Redirect;

class ContractorProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($con_id=0,$dep_id=0, Request $request)
    {
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_project'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_project"]);
        // Forget a single key...
        $request->session()->forget('extra_section');
        // Get language
        $data['lang'] = get_language();
        // Decrypt Contractor's ID
        $conId = decrypt($con_id);
        $data['con_id'] = $con_id;
        $userid = userid();
        $is_admin = is_admin();
        // Get all data
        $data['records'] = Plans::whereHas('project_location', function($query){
                                    $query->where('status','0');
                                })->whereHas('project_contractor', function($query) use ($conId){
                                    $query->where('contractor_id',$conId);
                                })->whereHas('project_contractor_staff', function($query) use ($userid,$is_admin){
                                    if($is_admin!=1)
                                    {
                                        $query->where('user_id',$userid);// Check for admin if not then set user id
                                    }
                                })->orderBy('id','desc')->paginate(10)->onEachSide(1);
        if(Input::get('ajax') == 1)
        {
            return view('pmis/contractor_project/list_ajax',$data);
        }
        else
        {
            return view('pmis/contractor_project/list',$data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($enc_id,$con_id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {}

    public function filter_contractor_project(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Get contractor id
        $contractor_id = decrypt($request->contractor_id);
        // Search Shared Plan 
        $records  = Plans::searchContractorProjects($request->condition,$request->field,$request->value,$contractor_id);
        // Load View to display records
        return view('pmis/contractor_project/list_filter',compact('records','lang','request')); 
    }
}
