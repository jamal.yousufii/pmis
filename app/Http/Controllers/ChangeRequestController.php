<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use App\Notifications\NewProject;
use App\models\Plans;
use App\models\Home;
use App\models\Billofquantities;
use App\models\Plan_locations;
use App\models\Requests;
use App\models\ChangeRequestCostScope;
use App\models\ChangeRequestStartStop;
use App\models\ChangeRequestTime;
use App\models\ChangeRequestAttachment;
use Illuminate\Support\Facades\Input;
use SebastianBergmann\Environment\Console;
use App\User;
use DB;
use Redirect;

class ChangeRequestController extends Controller
{

    /** Request for scop/cost */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id=0,Request $request)
    {
        // if project id not passed get form session
        if($dep_id==0)
        {
           $dep_id = session('current_department');
        }
        // Get department id
        $depID = decrypt($dep_id);
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_change_request'))
        {
            return view('access_denied');
        }
        $lang = get_language();
        // Set project id && location to the seasion while filtering 
        if(!empty($request->project_id))
            session(['project_id' => $request->project_id]); session(['project_location_id' => $request->project_location_id]); 
        //project id  
        $project_id = session('project_id');
        // Get plan record
        $plan = Plans::find($project_id);
        $contractor_id = 0; 
        if($depID==6)//Contractor request 
        {
            // Contractor id 
            $contractor_id = decrypt(session('current_contractor')); 
            // Project for contractor 
            $projects = Plans::whereHas('project_contractor', function($query) use($project_id){
                $query->where('project_id',$project_id); 
            })->orderBy('id','desc')->get(); 
            // Get Project Locations by Users
            $project_locations = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $contractor_id){
                $query->where('user_id',userid());
                $query->where('project_id',$project_id);
                $query->where('contractor_id',$contractor_id);
            })->orderBy('id','desc')->get();
        }
        else
        {
            //Get Project which is shared with this department  
            $projects = Plans::whereHas('share', function($query) use ($depID){
                $query->where('share_to',$depID);
                $query->where('share_to_code','pmis_progress');
            })->orderBy('id','desc')->get();

            // Get Project Locations based on Users access
            $project_locations = Plan_locations::whereHas('department_staff', function($query) use ($project_id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$project_id);
                $query->where('department_id',$depID);
            })->orderBy('id','desc')->get();
        }
        session(['current_section' => "pmis_change_request","current_department" => $dep_id]);
        if(!session::has('project_id')) //Only open the filter page for the search 
            return view('pmis/progress/change_request/fiter_project',compact('projects','project_locations','contractor_id','dep_id')); 
        else    
            return view('pmis.progress.change_request.index',compact('lang','dep_id','plan','project_id','project_locations','plan','contractor_id'));   
    }

    /**
     * List Bill of Quantities
     *
     * @return \Illuminate\Http\Response
     */
    public function list_cost_scope(Request $request)
    {
        // Get Language
        $lang = get_language();
        $project_id = $request->project_id; 
        $project_location_id = $request->project_location_id; 
        // Contractor id 
        $contractor_id = session('current_contractor'); 
        // Check if department ID is exist or not
        if(session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = session('current_department');
        }
        // Get plan record
        $plan = Plans::find($project_id);
        if($plan)
        {
            // Get summary data
            $summary = Requests::whereId($plan->request_id)->first();
        }
        // Get Bill of Quantities
        $billQuantity = Billofquantities::where('project_id',$project_id)->where('project_location_id',$project_location_id)->get();
        // Get units
        $units = Home::getAllStatics('units');
        //Get Bill of Quantities data
        session([
                    'project_location_id' => $project_location_id,
                    'cost_scope_tabe' => 'bq'
                ]);
        //Get Request Changes
        $request_changes  = ChangeRequestCostScope::Where('project_id',$project_id)->where('location_id',$project_location_id)->orderBy('id','desc')->paginate(5)->onEachSide(1);
        if(Input::get('ajax') == 1)
        {
            //Static record iteration with pagination
            $counter = $request->counter;
            // Load view to show result
            return view('pmis.progress.change_request.cost_scope.cost_scope_list_ajax', compact('request_changes','billQuantity','units','plan','summary','request','lang','counter','dep_id','contractor_id'));
        }
        else
        {
            // Load view to show result
            return view('pmis.progress.change_request.cost_scope.cost_scope_list', compact('request_changes','billQuantity','units','plan','summary','request','lang','dep_id','contractor_id'));
        }
    }

    /**
     * Store cost and scope changes
     *
     * @return \Illuminate\Http\Response
     */
    public function store_cost_scope(Request $request)
    {
        $lang = get_language();
        $validates = $request->validate([
            'operation_type' => 'required',
            'unit_id'        => 'required',
            'amount'         => 'required',
            'price'          => 'required',
            'total_price'    => 'required',
            'remarks'        => 'required',
        ]);
        // Insert Record
        $record = new ChangeRequestCostScope;
        if($request->newbill)
        {
            $record->new_old_bill = 1;
            $record->operation_type   = $request->operation_type;
        }
        else
        {
            $record->bill_quantity_id = $request->operation_type;
        }
        $record->project_id       = decrypt($request->project_id);
        $record->location_id      = $request->location_id;
        $record->unit_id          = $request->unit_id;
        $record->amount           = $request->amount;
        $record->price            = $request->price;
        $record->total_price      = $request->total_price;
        $record->remarks          = $request->remarks;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        if($record->save())
        {
            // Upload Attachments
            if($request->hasFile('file'))
            {
                $files = $request->file('file');
                $i=0;
                foreach ($files as $file)
                {
                    $custome_data = array(
                        'type' => $request->attachment_type,
                    );
                    $file_name  = '';
                    // Call Upload Function to Upload all Posted Files
                    $fileUpload = uploadAttachments('0',$record->id,'change_request/cost_scope',$file,$file_name,$i,'change_request_attachment',$custome_data);
                    if(!$fileUpload)
                    {
                        Session()->flash('att_failed', __("global.attach_failed"));
                    }
                    $i++;
                }
            }
            // // Start Notification and get users which has Approval rule
            // $emp = User::getUserRoleCode('change_req_approval');
            // $users = User::whereIn('id',$emp)->get();
            // $redirect_url = route('list_cost_scope',['enc_id'=>$request->project_id,'dep_id'=>$request->dep_id,'project_location_id'=>$request->location_id]);
            // foreach($users as $user) {
            //     $user->notify(new NewProject($record->id, trans('global.adjustment_request_shared',['pro' => $request->project_name]), userid(), $redirect_url));
            // }
            // // End Notification

            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }
        return response()->json([
            'route'               => route('list_cost_scope'),
            'method'              => 'get',
            'project_id'          => decrypt($request->project_id), 
            'project_location_id' => $request->location_id, 
            'modal_id'            => 'costScopeModal'
        ]); 
    }

    /**
     * Edit cost and scope changes
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_cost_scope(Request $request)
    {
        $validates = $request->validate([
            'operation_type' => 'required',
            //'unit_id'        => 'required',
            'amount'         => 'required',
            'price'          => 'required',
            'total_price'    => 'required',
            'remarks'        => 'required',
        ]);
        $id = $request->record_id;
        // Insert Record
        $record = ChangeRequestCostScope::find($id);
        if($record->new_old_bill == 1){
            $record->operation_type   = $request->operation_type;
        }
        else{
            $record->bill_quantity_id = $request->operation_type;
        }
        $record->unit_id          = $request->unit_id;
        $record->amount           = $request->amount;
        $record->price            = $request->price;
        $record->total_price      = $request->total_price;
        $record->remarks          = $request->remarks;
        $record->updated_at       = date('Y-m-d H:i:s');
        $record->updated_by       = userid();
        $updated = $record->save();
        if($updated)
        {
            // Upload Attachments
            if($request->hasFile('file'))
            {
                $files = $request->file('file');
                $i=0;
                foreach ($files as $file)
                {
                    $custome_data = array(
                        'type' => $request->attachment_type,
                    );
                    $file_name  = '';
                    // Call Upload Function to Upload all Posted Files
                    $fileUpload = uploadAttachments('0',$record->id,'change_request/cost_scope',$file,$file_name,$i,'change_request_attachment',$custome_data);
                    if(!$fileUpload)
                    {
                        Session()->flash('att_failed', __("global.attach_failed"));
                    }
                    $i++;
                }
            }
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

         return response()->json([
            'route'               => route('list_cost_scope'),
            'method'              => 'get',
            'project_id'          => $record->project_id, 
            'project_location_id' => $record->location_id, 
            'modal_id'            => 'cost_scope_edit_modal'
        ]); 
    }

    /**
     * Get Content
     *
     * @return \Illuminate\Http\Response
     */
    public function getCostScopeContent(Request $request)
    {
        $pro_id   =  $request->pro_id;
        $loc_id   =  $request->loc_id;
        $viewFile =  $request->viewFile;
        $lang     = get_language();
        $units    = Home::getAllStatics('units');
        // Get Bill of Quantities
        $records = Billofquantities::where('project_id',$pro_id)->where('project_location_id',$loc_id)->get();
        return view('pmis.progress.change_request.cost_scope.'.$viewFile,compact('records','units','lang'));
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2020-08-25 11:18:04 
     * @Desc: Change request edit load contet 
     */
    public function changeRequestEditLoad(Request $request)
    {
        $scop_change_request = ChangeRequestCostScope::find($request->id);
        $project_id          = decrypt($request->project_id);
        $project_location_id = decrypt($request->project_location_id);
        $units               = Home::getAllStatics('units');
        $bill_quantities     = ''; 
        if($scop_change_request->new_old_bill==0) //Change is for OLD BQ
        {
            $bill_quantities     = Billofquantities::with('unit')->where('project_id',$project_id)->where('project_location_id',$project_location_id)->get(); 
        }

        return view('pmis.progress.change_request.cost_scope.cost_scope_edit_content',compact(
                                                                                                'scop_change_request',
                                                                                                'bill_quantities',
                                                                                                'units',
                                                                                                'request'
                                                                                              )
                                                                                         ); 
    }

    /**
     * Get Bill of Quantities
     *
     * @return \Illuminate\Http\Response
     */
    public function getBillOfQuantities(){
        $id              = Input::get('id');
        $data['lang']    = get_language();
        $data['units']   = Home::getAllStatics('units');
        $data['records'] = Billofquantities::where('id',$id)->first();
        return view('pmis.progress.change_request.cost_scope.new_billof_quantities',$data);
    }

    /** Request for Time */

    /**
     * List time changes
     *
     * @return \Illuminate\Http\Response
     */
    public function list_time(Request $request)
    {
        // Get Language
        $lang = get_language();
        //Get Bill of Quantities data
        $project_id = $request->project_id;
        $project_location_id = $request->project_location_id; 
        // Contractor id 
        $contractor_id = session('current_contractor'); 
        // Check if department ID is exist or not
        if(session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = session('current_department');
        }
        // Get plan record
        $plan = Plans::find($project_id);
        if($plan)
        {
            // Get summary data
            $summary = Requests::whereId($plan->request_id)->first();
        }
        // Get units
        $units = Home::getAllStatics('units');
        //Get Request Changes
        $request_changes  = ChangeRequestTime::Where('project_id',$project_id)->where('location_id',$project_location_id)->orderBy('id','desc')->paginate(5)->onEachSide(1);
        // bill qunatities of the project 
        $bill_quantities = Billofquantities::where('project_id',$project_id)->where('project_location_id',$project_location_id)->get();
        // Get Project Locations
        $project_locations = Plan_locations::find($project_location_id);
        session(['cost_scope_tabe' => 'time']); //Active request changes in work  
        if(Input::get('ajax') == 1)
        {
            //Static record iteration with pagination
            $counter = $request->counter;
            // Load view to show result
            return view('pmis.progress.change_request.time.time_list_ajax', compact('request_changes','bill_quantities','units','plan','summary','request','lang','counter','dep_id','contractor_id','project_locations'));
        }
        else
        {
            // Load view to show result
            return view('pmis.progress.change_request.time.time_list', compact('request_changes','bill_quantities','units','plan','summary','request','lang','dep_id','contractor_id','project_locations'));
        }
    }

    /**
     * Store Change time request
     *
     * @return \Illuminate\Http\Response
     */
    public function store_time(Request $request)
    {
        $validates = $request->validate([
            //'bq_id'       => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
            'remarks'       => 'required',
        ]);
        // Language
        $lang = get_language();
        // Get uploaded file
        $file = $request->file('file');
        // Call Upload Function to Upload all Posted Files
        $fileUpload = uploadAttachmentsMultiRecords('change_request/time', $file);
        // Get locations
        if($request->locations=='-'){
            $locations = Plan_locations::where('project_id', $request->project_id)->get();
        }else{
            $locations = Plan_locations::whereId($request->locations)->get();
        }
        // Check if location is more than one then set is multiple column = 1
        $is_multiple = 0;
        if(count($locations)>1){
            $is_multiple = 1;
        }
        foreach($locations as $item){
            // Insert Record
            $record = new ChangeRequestTime;
            $record->project_id     = $request->project_id;
            $record->location_id    = $item->id;
            $record->start_date     = dateProvider($request->start_date,$lang);
            $record->end_date       = dateProvider($request->end_date,$lang);
            $record->remarks        = $request->remarks;
            $record->created_at     = date('Y-m-d H:i:s');
            $record->created_by     = userid();
            if($record->save())
            {
                // Check if attachment uploaded to server
                if(!empty($fileUpload)){
                    // Insert record`s attachment
                    $attachment = new ChangeRequestAttachment;
                    $attachment->parent_id      = '0';
                    $attachment->record_id      = $record->id;
                    $attachment->type           = $request->attachment_type;
                    $attachment->section        = $fileUpload['section'];
                    $attachment->filename       = $fileUpload['name'];
                    $attachment->path           = $fileUpload['path'];
                    $attachment->extension      = $fileUpload['fileExtention'];
                    $attachment->size           = $fileUpload['fileOrgSize'];
                    $attachment->is_multiple    = $is_multiple;
                    $attachment->created_at     = date('Y-m-d H:i:s');
                    $attachment->created_by     = userid();
                    $attachment->save();
                }
                Session()->flash('success', __("global.success_msg"));
            }
        }
        // Return response to redirect function of js
        return response()->json([
            'route'               => route('list_time'),
            'method'              => 'get',
            'project_id'          => $request->project_id, 
            'project_location_id' => $request->location_id, 
            'modal_id'            => 'timeModal'
        ]);
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2020-08-26 10:33:09 
     * @Desc:  
     */
    public function editTimeLoad(Request $request)
    {
        $lang = get_language();
        $item = ChangeRequestTime::find($request->id);
        // bill qunatities of the project 
        $bill_quantities = Billofquantities::where('project_id',$item->project_id)->where('project_location_id',$item->location_id)->get(); 
        return view('pmis.progress.change_request.time.time_edit_content',compact('item','request','lang','bill_quantities')); 
    }

    /**
     * Edit time changes
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_time(Request $request)
    {
        $lang = get_language();
        $validates = $request->validate([
            //'bq_id'       => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
            'remarks'     => 'required',
        ]);
        $id = $request->record_id;
        // Insert Record
        $record = ChangeRequestTime::find($id);
        //$record->bill_quantity_id = $request->bq_id;
        $record->start_date       = dateProvider($request->start_date,$lang);
        $record->end_date         = dateProvider($request->end_date,$lang);
        $record->remarks          = $request->remarks;
        $record->updated_at       = date('Y-m-d H:i:s');
        $record->updated_by       = userid();
        $updated = $record->save();
        if($updated)
        {
            // Upload Attachments
            if($request->hasFile('file'))
            {
                $files = $request->file('file');
                $i=0;
                foreach ($files as $file)
                {
                    $custome_data = array(
                        'type' => $request->attachment_type_edit,
                    );
                    $file_name  = '';
                    // Call Upload Function to Upload all Posted Files
                    $fileUpload = uploadAttachments($record->bq_id,$record->id,'change_request/time',$file,$file_name,$i,'change_request_attachment',$custome_data);
                    if(!$fileUpload)
                    {
                        Session()->flash('att_failed', __("global.attach_failed"));
                    }
                    $i++;
                }
            }
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

        return response()->json([
            'route'               => route('list_time'),
            'method'              => 'get',
            'project_id'          => $record->project_id, 
            'project_location_id' => $record->location_id, 
            'modal_id'            => 'timeEditModal'
        ]); 
    }

    /**
     * Get Date of Quantities
     *
     * @return \Illuminate\Http\Response
     */
    public function getDateOfQuantities()
    {
        $lang = get_language();
        $id = Input::get('id');
        $end_date = Billofquantities::where('id',$id)->first()->end_date;
        if($end_date){
            return dateCheck($end_date,$lang);
        }
        else{
            return "";
        }
    }

    /** Request for Start/Stop Project */

    /**
     * List time changes
     *
     * @return \Illuminate\Http\Response
     */
    public function list_start_stop(Request $request)
    {
        // Get Language
        $lang = get_language();
        //Get Bill of Quantities data
        $project_id = $request->project_id;
        // Get project location id 
        $project_location_id = $request->project_location_id; 
        // Contractor id 
        $contractor_id = session('current_contractor'); 

        // Check if department ID is exist or not
        if(session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = session('current_department');
        }
        // Get plan record
        $plan = Plans::find($project_id);
        if($plan)
        {
            // Get summary data
            $summary = Requests::whereId($plan->request_id)->first();
        }
        // Get Project Locations
        $project_locations = Plan_locations::find($project_location_id);
        //Get Request Changes
        $request_changes = ChangeRequestStartStop::Where('project_id',$project_id)->where('location_id',$project_location_id)->orderBy('id','desc')->paginate(5)->onEachSide(1);
        session(['cost_scope_tabe' => 'start_stop']); //Active request changes in work  
        if(Input::get('ajax') == 1)
        {
            //Static record iteration with pagination
            $counter = $request->counter;
            // Load view to show result
            return view('pmis.progress.change_request.start_stop.start_stop_list_ajax', compact('request_changes','plan','summary','request','lang','counter','dep_id','contractor_id','project_locations'));
        }
        else
        {
            // Load view to show result
            return view('pmis.progress.change_request.start_stop.start_stop_list', compact('request_changes','plan','summary','request','lang','dep_id','contractor_id','project_locations'));
        }
    }

    /**
     * Store Change time request
     *
     * @return \Illuminate\Http\Response
     */
    public function store_start_stop(Request $request)
    {
        $validates = $request->validate([
            'start_date' => 'required',
            'end_date'   => 'required',
            'remarks'    => 'required',
        ]);
        // Language
        $lang = get_language();
        // Get uploaded file
        $file = $request->file('file');
        // Call Upload Function to Upload all Posted Files
        $fileUpload = uploadAttachmentsMultiRecords('change_request/start_stop', $file);
        // Get locations
        if($request->locations=='-'){
            $locations = Plan_locations::where('project_id', $request->project_id)->get();
        }else{
            $locations = Plan_locations::whereId($request->locations)->get();
        }
        // Check if location is more than one then set is multiple column = 1
        $is_multiple = 0;
        if(count($locations)>1){
            $is_multiple = 1;
        }
        foreach($locations as $item){
            // Insert Record
            $record = new ChangeRequestStartStop;
            if($request->startstop)
            {
                $record->start_date = dateProvider($request->start_date,$lang);
                $record->end_date   = dateProvider($request->end_date,$lang);
                $record->start_stop = '1';
            }
            else{
                $record->start_date = dateProvider($request->start_date,$lang);
                $record->start_stop = '0';
            }
            $record->project_id     = $request->project_id;
            $record->location_id    = $item->id;
            $record->remarks        = $request->remarks;
            $record->created_at     = date('Y-m-d H:i:s');
            $record->created_by     = userid();
            if($record->save())
            {
                // Check if attachment uploaded to server
                if(!empty($fileUpload)){
                    // Insert record`s attachment
                    $attachment = new ChangeRequestAttachment;
                    $attachment->parent_id      = '0';
                    $attachment->record_id      = $record->id;
                    $attachment->type           = $request->attachment_type;
                    $attachment->section        = $fileUpload['section'];
                    $attachment->filename       = $fileUpload['name'];
                    $attachment->path           = $fileUpload['path'];
                    $attachment->extension      = $fileUpload['fileExtention'];
                    $attachment->size           = $fileUpload['fileOrgSize'];
                    $attachment->is_multiple    = $is_multiple;
                    $attachment->created_at     = date('Y-m-d H:i:s');
                    $attachment->created_by     = userid();
                    $attachment->save();
                }
                Session()->flash('success', __("global.success_msg"));
            }
        }
        // Return response to redirect function of js
        return response()->json([
            'route'               => route('list_start_stop'),
            'method'              => 'get',
            'project_id'          => $request->project_id, 
            'project_location_id' => $request->location_id, 
            'modal_id'            => 'startStopModal'
        ]);
    }
    
    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2020-08-27 14:18:23 
     * @Desc: Load contenet modal of edit start & stop   
     */
    public function startStopEditLoad(Request $request)
    {
        $start_stop_change = ChangeRequestStartStop::find($request->id);
        return view('pmis.progress.change_request.start_stop.start_stop_edit_content',compact('start_stop_change','request')); 
    }

    /**
     * Edit cost and scope changes
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_start_stop(Request $request)
    {
        $validates = $request->validate([
            'start_date' => 'required',
            'end_date'   => 'required',
            'remarks'    => 'required',
        ]);
        $id = $request->record_id;
        // Insert Record
        $record = ChangeRequestStartStop::find($id);

        if($record->start_stop==0){
            $record->start_date = $request->start_date;
        }else{
            $record->start_date = $request->start_date;
            $record->end_date   = $request->end_date;
        }
        $record->remarks        = $request->remarks;
        $record->updated_at     = date('Y-m-d H:i:s');
        $record->updated_by     = userid();
        $updated = $record->save();
        if($updated)
        {
            // Upload Attachments
            if($request->hasFile('file'))
            {
                $files = $request->file('file');
                $i=0;
                foreach ($files as $file)
                {
                    $custome_data = array(
                        'type' => $request->attachment_type,
                    );
                    $file_name  = '';
                    // Call Upload Function to Upload all Posted Files
                    $fileUpload = uploadAttachments('0',$record->id,'change_request/start_stop',$file,$file_name,$i,'change_request_attachment',$custome_data);
                    if(!$fileUpload)
                    {
                        Session()->flash('att_failed', __("global.attach_failed"));
                    }
                    $i++;
                }
            }
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

         return response()->json([
            'route'               => route('list_start_stop'),
            'method'              => 'get',
            'project_id'          => $record->project_id, 
            'project_location_id' => $record->location_id, 
            'modal_id'            => 'start_stop_edit_modal'
        ]); 
    }

    /**
     * Get Content
     *
     * @return \Illuminate\Http\Response
     */
    public function get_start_stop_content(Request $request)
    {
        $viewFile =  $request->viewFile;
        $lang     = get_language();
        return view('pmis.progress.change_request.start_stop.'.$viewFile,compact('lang'));
    }

    /**
     * Function to approve change requests of all sections
     *
     * @return \Illuminate\Http\Response
     */
    public function change_request_approval(Request $request)
    { 
         
        $table          = $request->table;
        $id             = $request->record_id;
        $project_id     = $request->project_id;
        $location_id    = $request->location_id;
        $dep_id         = decrypt(session('current_department'));
        $project_name   = $request->project_name;
        $owner          = $request->created_by;
        $route          = $request->route;
        if($table)
        {
            // Get project_
            if($request->operation){
                $operation = '1';
            }else{
                $operation = '2';
            }
            // Insert Record
            $data = array(
                'operation'             => $operation,
                'operation_description' => $request->operation_description,
                'operation'             => $operation,
                'operation_at'          => date('Y-m-d H:i:s'),
                'operation_by'          => userid(),
            );
            $record = DB::connection('pmis')->table($table)->whereId($id)->update($data);
            if($record)
            {
                if($request->operation)
                {
                    $msg = trans('global.adjustment_request_approved',['pro' => $project_name]);
                }
                else
                {
                    $msg = trans('global.adjustment_request_rejected',['pro' => $project_name]);
                }
                // Start Notification
                $users = User::whereId($owner)->get();
                $redirect_url = route($route,['enc_id'=>$project_id,'dep_id'=>$dep_id,'project_location_id'=>$location_id]);
                foreach($users as $user) {
                    $user->notify(new NewProject($id, $msg, userid(), $redirect_url));
                }
                // End Notification
                Session()->flash('success', __("global.success_process_msg"));
            }
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            }
        }

          return response()->json([
            'route'               => route($route),
            'method'              => 'get',
            'project_id'          => $request->project_id, 
            'project_location_id' => $request->location_id, 
            'dep_id'              => $request->dep_id,
            'modal_id'            => $request->modal_id
        ]); 

    }

    /**
     * Load the cost scop list for edit
     *
     * @return \Illuminate\Http\Response
     */
    public function change_request_approval_load(Request $request)
    {
        if($request->type=='cost_scope')
        {
          $change_request = ChangeRequestCostScope::find($request->id);
          return view('pmis.progress.change_request.cost_scope.cost_scope_approval',compact('change_request')); 
        }
        elseif($request->type=='start_stop') 
        {
          $change_request = ChangeRequestStartStop::find($request->id);
          return view('pmis.progress.change_request.start_stop.start_stop_approval',compact('change_request')); 
        }
        else
        {
          $change_request = ChangeRequestTime::find($request->id);  
          return view('pmis.progress.change_request.time.time_approval',compact('change_request')); 
        }

    } 
}
