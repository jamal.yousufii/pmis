<?php
namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\DepartmentsRequest; 
use App\models\Authentication\Departments;
use App\models\Authentication\Module_deps;
use App\models\Authentication\Organization;
use App\models\Authentication\Modules;

class DepartmentsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(check_my_auth_section('auth_dep'))
    {
      session(['current_section' => "auth_dep"]);
      $data['records'] = Departments::paginate(10);
      if(Input::get('ajax') == 1)
      {
        return view('authentication/departments/list_ajax',$data);
      }
      else
      {
        return view('authentication/departments/list',$data);
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Get all organizations and modules
    $data['organizations'] = Organization::select('id','name_'.$lang.' as name')->get();
    $data['modules'] = Modules::select('id','name_'.$lang.' as name')->get();
    // Load view
    return view('authentication.departments.add',$data); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(DepartmentsRequest $request)
  {
    // Store data in database
    $id = Departments::create($request->except('module_id'))->id; 
    if($id>0)
    {
      $modules = $request->module_id;
      if($modules)
      {
        foreach($modules as $mod)
        {
            $data = array(
              'department_id' =>  $id,
              'module_id'     =>  $mod,
              'created_at'    =>  date('Y-m-d H:i:s'),
            );
        }
        // Insert department's modules
        Module_deps::insert($data);
      }
      Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    // Get language
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Record
    $data['record'] = Departments::find($id);
    // Load View
    return view('authentication.departments.view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    // Get language
    $lang = get_language();
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Record
    $data['record'] = Departments::find($id);
    // Get all organizations and modules
    $data['organizations'] = Organization::select('id','name_'.$lang.' as name')->get();
    $data['modules'] = Modules::select('id','name_'.$lang.' as name')->get();
    // Get department's modules
    $selected_mod = Module_deps::where('department_id',$id)->get();
    //Default value for selected modules
    $current_mod = array();
    if($selected_mod)
    {
      foreach($selected_mod as $item)
      {
        $current_mod[] = $item->module_id;
      }
    }
    $data['selected_mod'] = $current_mod;
    // Load View
    return view('authentication.departments.edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(DepartmentsRequest $request,$enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Update data in database
    $result = Departments::whereId($id)->update($request->except('module_id'));
    if($result)
    {
      $modules = $request->module_id;
      if($modules)
      {
        // Delete old records
        Module_deps::where('department_id',$id)->delete();
        // Insert posted data to array
        foreach($modules as $mod)
        {
          $data = array(
            'department_id' => $id,
            'module_id'     => $mod,
            'created_at'    => date('Y-m-d H:i:s'),
          );
          // Insert department's modules
          Module_deps::insert($data);
        }        
      }
      Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
      // Dycript the id
      $id = decrypt($enc_id);
      // Delete Data from table
      $result = Departments::whereId($id)->delete();
      if($result)
      {
        Module_deps::where('department_id',$id)->delete();
        Session()->flash('success', __("authentication.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', __("authentication.failed_msg"));

      }
  }

  function filterDepartment()
  {
      if(Input::get('item'))
      {
          $item = Input::get('item');
          // Get data by keywords
          $data['records'] = Departments::whereRaw("(name_en like '%".$item."%' OR name_dr like '%".$item."%' OR name_pa like '%".$item."%')")->paginate(10);
      }
      else
      {
          // Get all data
          $data['records'] = Departments::paginate(10);
      }    
      // Load View to display records
      return view('authentication/organizations/list_filter',$data); 
  }
}
?>
