<?php
namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ModulesRequest; 
use App\models\Authentication\Modules;

class ModulesController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(check_my_auth_section('auth_app'))
    {
      session(['current_section' => "auth_app"]);
      $data['records'] = Modules::paginate(10);
      if(Input::get('ajax') == 1)
      {
        return view('authentication/modules/list_ajax',$data);
      }
      else
      {
        //load view to show searchpa result
        return view('authentication/modules/list',$data);
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('authentication.modules.add'); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(ModulesRequest $request)
  {
    // Store data in database
    $result = Modules::create($request->all()); 
    if($result)
    {
        Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
        Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
      // Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      // Get Record
      $data['record'] = Modules::find($id);
      // Load View
      return view('authentication.modules.view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
      // Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      // Get Record
      $data['record'] = Modules::find($id);
      // Load View
      return view('authentication.modules.edit',$data);
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(ModulesRequest $request,$enc_id)
  {
      // Dycript the id
      $id = decrypt($enc_id);
      // Update data in database
      $result = Modules::whereId($id)->update($request->except('_token'));
      if($result)
      {
          Session()->flash('success', __("authentication.success_edit_msg"));
      }
      else
      {
          Session()->flash('fail', __("authentication.failed_msg"));
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Delete Data from table
    $result = Modules::whereId($id)->delete();
    if($result)
    {
        Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
        Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  function filterModule()
  {
      if(Input::get('item'))
      {
          $item = Input::get('item');
          // Get data by keywords
          $data['records'] = Modules::whereRaw("(name_en like '%".$item."%' OR name_dr like '%".$item."%' OR name_pa like '%".$item."%' OR code like '%".$item."%')")->paginate(10);
      }
      else
      {
          // Get all data
          $data['records'] = Modules::paginate(10);
      }    
      // Load View to display records
      return view('authentication/modules/list_filter',$data); 
  }
}
?>
