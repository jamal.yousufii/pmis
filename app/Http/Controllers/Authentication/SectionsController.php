<?php
namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\SectionsRequest; 
use App\models\Authentication\Sections;
use App\models\Authentication\Section_deps;
use App\models\Authentication\SectionSharings;
use App\models\Authentication\Modules;
use App\models\Authentication\Departments;

class SectionsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  { 
    if(check_my_auth_section('auth_section'))
    {
      session(['current_section' => "auth_section"]);
      $data['records'] = Sections::paginate(10);
      $lang = get_language();
      $data['lang'] = $lang;
      if(Input::get('ajax') == 1)
      {
        return view('authentication/sections/list_ajax',$data);
      }
      else
      {
        //load view to show searchpa result
        return view('authentication/sections/list',$data);
      }
      
    }
    else
    {
      return view('access_denied');
    }
  }
  
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Get all Departments
    $allDeps = Departments::select('departments.id','departments.name_'.$lang.' as name','org.name_'.$lang.' as org_name')->leftJoin('organizations as org','organization_id','=','org.id')->get();
    $departments = array();
    if($allDeps)
    {
      foreach($allDeps as $item)
      {
        $departments[$item->org_name][] = $item->id."-".$item->name;
      }
    }
    $data['departments'] = $departments;
    // Get all Modules
    $data['modules'] = Modules::select('id','name_'.$lang.' as name')->get();
    // Load view
    return view('authentication.sections.add',$data); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(SectionsRequest $request)
  {
    // Store data in database
    $id = sections::create($request->except('department_id'))->id; 
    if($id)
    {
     //INSERT Section's Departments
     $departments = $request->department_id;
     $deps = array();
     if($departments)
     {
       foreach($departments as $item)
       {
         $data = array(
           'section_id'    =>  $id,
           'department_id' =>  $item,
           'created_at'    =>  date('Y-m-d H:i:s'),
         );
         array_push($deps,$data);
       }
       if(COUNT($deps)>0)
       {
         // Insert Data 
         Section_deps::insert($deps);
       }
     }
     Session()->flash('success', __("authentication.success_msg"));
   }
   else
   {
     Session()->flash('fail', __("authentication.failed_msg"));
   }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    // Get language
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Record
    $data['record'] = Sections::find($id);
    // Get All Sections
    $data['sections'] = Sections::where('tab',0)->whereHas('modules', function($query) {
      $query->where('code','pmis');
    })->orderBy('orders','asc')->get();
    // Get All Allowed Sections for Sharing
    $allowed_sections = SectionSharings::where('section_id',$id)->get();
    $data['allowed_sections'] = $allowed_sections;
    $selected = array();
    foreach($allowed_sections as $item)
    {
      $selected[] = $item->sections;
    }
    $data['selected'] = $selected;
    // Load View
    return view('authentication.sections.view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    // Get language
    $lang = get_language();
    // Decrypt the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get all Departments
    $allDeps = Departments::select('departments.id','departments.name_'.$lang.' as name','org.name_'.$lang.' as org_name')->leftJoin('organizations as org','organization_id','=','org.id')->get();
    $departments = array();
    if($allDeps)
    {
      foreach($allDeps as $item)
      {
        $departments[$item->org_name][] = $item->id."-".$item->name;
      }
    }
    $data['departments'] = $departments;
    // Get all selected departments
    $selected_deps = Section_deps::where('section_id',$id)->get();
    $section_deps = array();
    if($selected_deps)
    {
      foreach($selected_deps as $item)
      {
        $section_deps[] = $item->department_id;
      }
    }    
    $data['section_deps'] = $section_deps;
    // Get all Modules
    $data['modules'] = Modules::select('id','name_'.$lang.' as name')->get();
    // Get record
    $data['record'] = Sections::find($id);
    // Load View
    return view('authentication.sections.edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(SectionsRequest $request,$enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Update data in database
    $result = Sections::whereId($id)->update($request->except('department_id','_token'));
    if($result)
    {
      // Delete old records
      Section_deps::where('section_id',$id)->delete();
      //INSERT Section's Departments
      $departments = $request->department_id;
      $deps = array();
      if($departments)
      {
        foreach($departments as $item)
        {
          $data = array(
            'section_id'    =>  $id,
            'department_id' =>  $item,
            'created_at'    =>  date('Y-m-d H:i:s'),
          );
          array_push($deps,$data);
        }
        if(COUNT($deps)>0)
        {
          // Insert Data 
          Section_deps::insert($deps);
        }
      }

      Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
    $id = decrypt($enc_id);
    //Update record
    $record = Sections::find($id);
    $data = $record->delete();
    if($data!=0)
    {
      Section_deps::where('section_id',$id)->delete();
      Session()->flash('success', __("authentication.success_delete_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }
  
  public function filterSection()
  {
    $lang = get_language();
    $data['lang'] = $lang;
    if(Input::get('item'))
    {         
        $item = Input::get('item');
        // Get data by keywords
        $data['records'] = Sections::whereRaw("(name_en like '%".$item."%' OR name_dr like '%".$item."%' OR name_pa like '%".$item."%')")->paginate(10);
    }
    else
    {
        // Get all data
        $data['records'] = Sections::paginate(10);
    }    
    // Load View to display records
    return view('authentication/sections/list_filter',$data); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function sharing(Request $request)
  {
    // Get Section ID
    $id = decrypt($request->id);
    // Get Section Code
    $code = $request->code;
    // Get Allowed Sections
    $sections = $request->section_id;
    $operation = false;
    $data = array();
    if($sections)
    {
      foreach($sections as $item)
      {
        $new_data = array(
          'section_id'    =>  $id,
          'code'          =>  $code,
          'sections'      =>  $item,
          'created_at'    =>  date('Y-m-d H:i:s'),
        );
        array_push($data,$new_data);
      }
      if(COUNT($data)>0)
      {
        // Delete Old Records
        SectionSharings::where('section_id',$id)->delete();
        // Insert Records 
        SectionSharings::insert($data);
        $operation = true;
      }
    }
    if($operation)
    {
      Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function distroySharing($enc_id)
  {
    $id = decrypt($enc_id);
    //Update record
    $record = SectionSharings::find($id);
    $data = $record->delete();
    if($data!=0)
    {
      Session()->flash('success', __("authentication.success_delete_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }
}
?>
