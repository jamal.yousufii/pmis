<?php
namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\RolesRequest; 
use App\models\Authentication\Roles;
use App\models\Authentication\Sections;

class RolesController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(check_my_auth_section('auth_role'))
    {
      session(['current_section' => "auth_role"]);
      $data['records'] = Roles::orderBy('id', 'ASC')->paginate(10)->onEachSide(1);
      $lang = get_language();
      $data['lang'] = $lang;
      if(Input::get('ajax') == 1)
      {
        return view('authentication/roles/list_ajax',$data);
      }
      else
      {
        //load view to show searchpa result
        return view('authentication/roles/list',$data);
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Get all Sections
    $data['sections'] = Sections::select('id','name_'.$lang.' as name')->get();
    // Load view
    return view('authentication.roles.add',$data); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(RolesRequest $request)
  {
    // Store data in database
    $result = Roles::create($request->all()); 
    if($result)
    {
      Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
      $lang = get_language();      
      // Dycript the id
      $id = decrypt($enc_id);
      $data['lang']   = $lang;
      $data['enc_id'] = $enc_id;
      // Get Record
      $data['record'] = Roles::find($id);
      // Load View
      return view('authentication.roles.view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
      // Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      // Get Record
      $data['record'] = Roles::find($id);
      $lang = get_language();
      $data['lang'] = $lang;
      $data['sections'] = Sections::select('id','name_'.$lang.' as name')->get();
      // Load View
      return view('authentication.roles.edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RolesRequest $request,$enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Update data in database
    $result = Roles::whereId($id)->update($request->except('_token'));
    if($result)
    {
        Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
        Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Delete Data from table
    $result = Roles::whereId($id)->delete();
    if($result)
    {
        Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
        Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  function filterRole()
  {
    $lang = get_language();
    $data['lang'] = $lang;
    if(Input::get('item'))
    {
        $item = Input::get('item');
        // Get data by keywords
        $data['records'] = Roles::whereRaw("(roles.code like '%".$item."%' OR roles.name_dr like '%".$item."%' OR roles.name_en like '%".$item."%' OR roles.name_pa like '%".$item."%')")->paginate(10);
    }
    else
    {
        // Get all data
        $data['records'] = Roles::paginate(10);
    }    
    // Load View to display records
    return view('authentication/roles/list_filter',$data); 
  }
}
?>
