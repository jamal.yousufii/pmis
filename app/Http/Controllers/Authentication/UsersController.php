<?php
namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\UsersRequest;
use App\models\Authentication\Organization;
use App\models\Authentication\Departments;
use App\models\Authentication\UserDepartments;
use App\models\Authentication\UserSections;
use App\models\Authentication\ModuleUsers;
use App\models\Authentication\UserRole;
use App\models\Authentication\Users;
use App\models\Authentication\Sections;
use App\models\Authentication\Modules;
use Response;
use Validator;

class UsersController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(check_my_auth_section('auth_user'))
    {
      session(['current_section' => "auth_user"]);
      $records = Users::where("contractor_staff",'0')->where('organization_id',organizationid())->paginate(10);
      $lang = get_language();
      if(Input::get('ajax') == 1)
      {
        return view('authentication.users.list_ajax',compact('records','lang'));
      }
      else
      {
        //load view to show searchpa result
        return view('authentication.users.list',compact('records','lang'));
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Get all organizations
    $organizations = Organization::select('id','name_'.$lang.' as name')->whereId(organizationid())->get();
    return view('authentication.users.add',compact('organizations','lang'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(UsersRequest $request)
  {
    $password = Hash::make($request->password);
    $record = new Users;
    $record->organization_id = $request->organization_id;
    $record->department_id  = $request->department_id;
    $record->name           = $request->name;
    $record->father         = $request->father;
    $record->email          = $request->email;
    $record->phone_number   = $request->phone_number;
    $record->position       = $request->position;
    $record->password       = $password;
    $record->is_admin       = $request->is_admin;
    $record->is_dashboard   = $request->is_dashboard;
    $record->profile_pic    = "default.png";
    $record->created_by     = userid();
    $user = $record->save();
    if($user>0)
    {
      // Get Departments
      $departments = $request->departments;
      if(!empty($departments))
      {
        foreach($departments as $dep)
        {
          //INSERT USER DEPARTMENT
          $data = array(
            'user_id'       =>  $record->id,
            'department_id' =>  $dep,
            'created_at'    =>  date('Y-m-d H:i:s'),
          );
          UserDepartments::insert($data);
          //INSERT USER modules
          $modules = 'modules_'.$dep;
          $modules = $request->$modules;
          $data = array();
          if($modules)
          {
            foreach($modules as $app)
            {
              $app_data = array(
                'user_id'       =>  $record->id,
                'module_id'     =>  $app,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$app_data);
            }
            if(COUNT($data)>0)
            {
              ModuleUsers::insert($data);
            }
          }
          //INSERT USER SECTIONS
          $sections = 'sections_'.$dep;
          $sections = $request->$sections;
          $data = array();
          if($sections)
          {
            foreach($sections as $sec)
            {
              $sec_data = array(
                'user_id'       =>  $record->id,
                'section_id'    =>  $sec,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$sec_data);
            }
            if(COUNT($data)>0)
            {
              UserSections::insert($data);
            }
          }
          //INSERT USER ROLES
          $roles = 'roles_'.$dep;
          $roles = $request->$roles;
          $data = array();
          if($roles)
          {
            foreach($roles as $rol)
            {
              $rol_data = array(
                'user_id'       =>  $record->id,
                'role_id'       =>  $rol,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$rol_data);
            }
            if(COUNT($data)>0)
            {
              UserRole::insert($data);
            }
          }
        }
      }
      Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    // Get language
    $lang = get_language();
    // Dycript the id
    $id = decrypt($enc_id);
    // Get Record
    $record = Users::find($id);
    //Get User Access Details
    $modules  = Users::getUserModules($id,$lang);
    $sections = Users::getUserSections($id,$lang);
    $roles    = Users::getUserRoles($id,$lang);
    // Get user departments
    $departments = Departments::select('departments.id','departments.name_'.$lang.' as name')
                                        ->join('user_departments as ud','ud.department_id','=','departments.id')
                                        ->where('ud.user_id',$id)
                                        ->get();
    // Load View to display data
    return view('authentication.users.view',compact('enc_id','lang','record','modules','sections','roles','departments'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    // Decrypt the id
    $id = decrypt($enc_id);
    // Get user data
    $record = Users::find($id);
    // Get all organizations
    $organizations = Organization::select('id','name_'.$lang.' as name')->whereId($record->organization_id)->get();
    // Get all Departments
    $departments = Departments::select('departments.id','departments.name_'.$lang.' as name')->where('organization_id',$record->organization_id)->where('code','!=','dep_06')->get();
    // Get selected departments
    $selectedDeps = UserDepartments::where('user_id',$id)->pluck('department_id')->toArray();
    // Load View to display data
    return view('authentication.users.edit',compact('enc_id','lang','record','organizations','departments','selectedDeps'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request,$enc_id)
  {
    // Validate the request...
    $validates = $request->validate([
      'department_id'     => 'required',
      'name'              => 'required',
      'father'            => 'required',
      'email'             => 'required',
      'position'          => 'required',
    ]);
    $id = decrypt($enc_id);
    $record = Users::find($id);
    $record->organization_id = $request->organization_id;
    $record->department_id   = $request->department_id;
    $record->name            = $request->name;
    $record->father          = $request->father;
    $record->email           = $request->email;
    $record->phone_number    = $request->phone_number;
    $record->position        = $request->position;
    $record->is_dashboard   = $request->is_dashboard;
    if($request->password)
    {
      $record->password     = Hash::make($request->password);
    }
    $record->is_admin       = $request->is_admin;
    $record->updated_by     = userid();
    $updated = $record->save();
    if($updated>0)
    {
      // Get Departments
      $departments = $request->departments;
      if(!empty($departments))
      {
        // Delete user department old records
        UserDepartments::where('user_id',$id)->delete();
        foreach($departments as $dep)
        {
          //INSERT USER DEPARTMENT
          $data = array(
            'user_id'       =>  $record->id,
            'department_id' =>  $dep,
            'created_at'    =>  date('Y-m-d H:i:s'),
          );
          UserDepartments::insert($data);
          //INSERT USER MODULES
          $modules = 'modules_'.$dep;
          $modules = $request->$modules;
          $data = array();
          if($modules)
          {
            foreach($modules as $app)
            {
              $app_data = array(
                'user_id'       =>  $record->id,
                'module_id'     =>  $app,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$app_data);
            }
            if(COUNT($data)>0)
            {
              // Delete user module old records
              ModuleUsers::where('user_id',$id)->where('department_id',$dep)->delete();
              // Insert user module new records
              ModuleUsers::insert($data);
            }
          }
          //INSERT USER SECTIONS
          $sections = 'sections_'.$dep;
          $sections = $request->$sections;
          $data = array();
          if($sections)
          {
            foreach($sections as $sec)
            {
              $sec_data = array(
                'user_id'       =>  $record->id,
                'section_id'    =>  $sec,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$sec_data);
            }
            if(COUNT($data)>0)
            {
              // Delete user section old records
              UserSections::where('user_id',$id)->where('department_id',$dep)->delete();
              // Insert user section new records
              UserSections::insert($data);
            }
          }
          //INSERT USER ROLES
          $roles = 'roles_'.$dep;
          $roles = $request->$roles;
          $data = array();
          if($roles)
          {
            foreach($roles as $rol)
            {
              $rol_data = array(
                'user_id'       =>  $record->id,
                'role_id'       =>  $rol,
                'department_id' =>  $dep,
                'created_at'    =>  date('Y-m-d H:i:s'),
              );
              array_push($data,$rol_data);
            }
            if(COUNT($data)>0)
            {
              // Delete user role old records
              UserRole::where('user_id',$id)->where('department_id',$dep)->delete();
              // Insert user role new records
              UserRole::insert($data);
            }
          }
        }
      }
      Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
    // Dycript the id
    $id = decrypt($enc_id);
    // Delete Data from table
    $result = Users::whereId($id)->delete();
    if($result)
    {
      Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Display a listing of the resource based on condition.
   *
   * @return \Illuminate\Http\Response
   */
  function filterUser()
  {
      $lang = get_language();
      if(Input::get('item'))
      {
          $item = Input::get('item');
          // Get data by keywords
          $records = Users::select('users.id', 'users.name', 'users.position', 'users.email', 'dep.name_'.$lang.' as dep_name')
                      ->join('departments as dep','users.department_id','=','dep.id')
                      ->where("contractor_staff",'0')
                      ->where('users.organization_id',organizationid())
                      ->whereRaw("(users.name like '%".$item."%' OR users.father like '%".$item."%' OR dep.name_".$lang." like '%".$item."%')")
                      ->paginate(10);
      }
      else
      {
          // Get all data
          $records = Users::select('users.id', 'users.name', 'users.position', 'users.email', 'dep.name_'.$lang.' as dep_name')
                      ->join('departments as dep','users.department_id','=','dep.id')
                      ->where("contractor_staff",'0')
                      ->where('users.organization_id',organizationid())
                      ->paginate(10);
      }
      // Load View to display records
      return view('authentication.users.list_filter',compact('records','lang'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function editPassword(Request $request)
  {
    //Validate the request...
    $validates = $request->validate([
        'password'          => 'required',
        'confirm_password'  => 'required',
    ]);
    $userid = Auth::user()->id;
    $record = Users::find($userid);
    $password = Hash::make($request->password);
    $record->password = $password;
    $updated = $record->save();
    if($updated>0)
    {
      return __("home.passord_success_edit_msg");
    }
    else
    {
      return __("home.passord_failed_edit_msg");
    }
  }

  /**
   * Show the form for editing the specified profile picture.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function uploadPic()
  {
    //Getting all of the post data
    $file = Input::file('profile_pic');
    $id = Input::post('userid');
    $errors = "";
    if(Input::hasFile('profile_pic'))
    {
      //validating each file.
      $rules = array('file' => 'required');
      $validator = Validator::make(
        [
          'file'      => $file,
          'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
          'file'      => 'required|max:800000',
          'extension' => 'required|in:jpg,jpeg,png'
        ]
      );
      if($validator->passes())
      {
        //Path is root/uploads
        $destinationPath = 'public/attachments/users';
        $filename = $file->getClientOriginalName();
        $temp = explode(".", $filename);
        $extension = end($temp);
        $filename = "user_".$id.'.'.$extension;
        $upload_success = $file->move($destinationPath, $filename);
        if($upload_success)
        {
          $data = array('profile_pic' => $filename);
          Users::where('id',$id)->update($data);
          return '<div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
                      <div class="m-alert__icon"><i class="la la-check-square"></i></div>
                      <div class="m-alert__text">'. __("authentication.pic_success_msg"). '</div>
                      <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                  </div>';
        }
        else
        {
          return '<div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                    <div class="m-alert__icon"><i class="la la-warning"></i></div>
                    <div class="m-alert__text">'. __("authentication.pic_failed_msg"). '</div>
                    <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                  </div>';
        }
      }
      else
      {
        //Redirect back with errors.
        return Redirect::back()->withErrors($validator);
      }
    }
    else
    {
      return __("authentication.pic_failed_msg");
    }
  }

  /**
   * @Desc: Get Modules based on department
   */
  public function getModulesByDepartments(Request $request)
  {
    $lang = get_language();
    $user_id       = $request->user_id;
    $department_id = $request->departments_id;
    // Get modules
    $modules = Modules::select('modules.id','modules.name_'.$lang.' as name')
                        ->join('module_deps as md','md.module_id','=','modules.id')
                        ->join('departments as deps','md.department_id','=','deps.id')
                        ->where('md.deleted_at',null)
                        ->groupBy('modules.id')
                        ->where('deps.id',$department_id)->get();
    // load view to display data
    return view('authentication.users.modules',compact('department_id','lang','user_id','modules'));
  }

  /**
   * @Desc:  Get Sections based on module & department
   */
  public function getModuleSections($dep_id,Request $request)
  {
    // Get language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->dep_id);
    // Get user id
    $user_id = $request->user_id;
    // Get modules
    $modules_id = $request->modules_id;
    $modules = explode(',',$modules_id);
    // Get sections
    $section_data = Sections::select('deps.id as dep_id','deps.name_'.$lang.' as dep_name','sections.module_id','mod.name_'.$lang.' as module_name','sections.id','sections.name_'.$lang.' as name')
                  ->join('section_deps as sd','sd.section_id','=','sections.id')
                  ->join('departments as deps','sd.department_id','=','deps.id')
                  ->join('modules as mod','sections.module_id','=','mod.id')
                  ->where('deps.id',$department_id)
                  ->whereIn('mod.id',$modules)
                  ->orderBy('mod.id','asc')
                  ->orderBy('sections.orders','asc')->get();
    $sections = array();
    if($section_data)
    {
      foreach($section_data as $item)
      {
        if($item->module_id!="0"){
          $sections[$item->module_id.'-'.$item->module_name][] = $item->id."-".$item->name;
        }
      }
    }
    // Get selected sections
    $selectedSec = getUserSectionsByModules($user_id,$department_id);
    // load view to display data
    return view('authentication.users.sections',compact('sections','selectedSec','department_id','lang','modules_id','user_id'));
  }

  /**
   * @Desc:  Get Roles based on module & department
   */
  public function getSectionRoles($dep_id,Request $request)
  {
    // Get language
    $lang = get_language();
    // Get user id
    $user_id = $request->user_id;
    // Get department
    $department_id = decrypt($request->dep_id);
    // Get sections
    $sections_id = explode(',',$request->sections);
    // Get sections
    $roles = Users::getRolesBySections($sections_id,$lang);
    // Get selected roles
    $selectedRol = getUserRolesBySections($user_id,$department_id);
    // load view to display data
    return view('authentication.users.roles',compact('roles','selectedRol','department_id'));
  }

  public function getDepartmentByOrganization(Request $request)
  {
    $lang = get_language();
    $org_id = $request->id;
    $target = $request->target;
    // Get all departments except contractors
    $departments = Departments::select('departments.id','departments.name_'.$lang.' as name')->where('organization_id',$org_id)->where('code','!=','dep_06')->get();
    // load view to display data
    if($target=='user_dep'){
      return view('authentication.users.departments',compact('departments'));
    }elseif($target=='all_dep'){
      return view('authentication.users.departments_all',compact('departments'));
    }
  }
}
?>
