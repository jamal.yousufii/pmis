<?php
namespace App\Http\Controllers\Authentication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\OrganizationRequest; 
use App\Http\Controllers\Controller;
use App\models\Authentication\Organization;

class OrganizationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(check_my_auth_section('auth_org'))
        {
            session(['current_section' => "auth_org"]);
            $data['records'] = Organization::paginate(10);
            $lang = get_language();
            $data['lang'] = $lang;
            if($request->ajax == 1)
            {
                return view('authentication/organizations/list_ajax',$data);
            }
            else
            {
                //load view to show searchpa result
                return view('authentication/organizations/list',$data);
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authentication.organizations.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizationRequest $request)
    {
        // Store data in database
        $result = Organization::create($request->all()); 
        if($result)
        {
            Session()->flash('success', __("authentication.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("authentication.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get Record
        $data['record'] = Organization::find($id);
        // Load View
        return view('authentication.organizations.view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get Record
        $data['record'] = Organization::find($id);
        // Load View
        return view('authentication.organizations.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationRequest $request,$enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Update data in database
        $result = Organization::whereId($id)->update($request->except('_token'));
        if($result)
        {
            Session()->flash('success', __("authentication.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("authentication.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $result = Organization::whereId($id)->delete();
        if($result)
        {
            Session()->flash('success', __("authentication.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("authentication.failed_msg"));
        }
    }

    function filterOrganization()
    {
        if(Input::get('item'))
        {
            $item = Input::get('item');
            // Get data by keywords
            $data['records'] = Organization::whereRaw("(name_en like '%".$item."%' OR name_dr like '%".$item."%' OR name_pa like '%".$item."%')")->paginate(10);
        }
        else
        {
            // Get all data
            $data['records'] = Organization::paginate(10);
        }    
        // Load View to display records
        return view('authentication/organizations/list_filter',$data); 
    }
}
