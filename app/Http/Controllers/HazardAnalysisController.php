<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Billofquantities;
use App\models\HazardAnalysis;
use App\models\HazardAnalysisSub;
use App\models\HazardInspection;
use App\models\Plans;
use App\models\Requests;
use App\models\Plan_locations;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Redirect;

class HazardAnalysisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0,$dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_hazard_analysis'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => 'pmis_progress']);
        // Get Language
        $lang = get_language();
        $data['lang'] = $lang;
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_hazard_analysis']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        // Load view to show result
        return view('pmis/progress/hazard_analysis/index',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($enc_id=0,$dep_id=0,$loc_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_hazard_analysis'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => 'pmis_progress']);
        // Get Language
        $lang = get_language();
        $data['lang'] = $lang;
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Project Location id
        $data['project_location_id'] = $loc_id;
        session(['project_location_id' => $loc_id]);
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_hazard_analysis']);
        // Get Bill of Quantities
        $data['records'] = Billofquantities::where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->orderBy('bq_section_id','asc')->paginate(20)->onEachSide(1);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        if(Input::get('ajax') == 1)
        {
            return view('pmis/progress/hazard_analysis/list_ajax',$data);
        }
        else
        {
            return view('pmis/progress/hazard_analysis/list',$data);
        }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'name'  => 'required',
        ]);
        //Insert data of Sub Hazard Analysis to Table HazardAnalysisSub
        $hazardAnalysis = array();
        for($i=0;$i<count($request->name);$i++)
        {
            $hazardAnalysis[] = array(
                'name'          => $request->name[$i],
                'project_id'    => $request->project_id,
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => userid(),
            );
        }
        if(HazardAnalysis::insert($hazardAnalysis))
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for creating a new sub resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store_sub(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'hazard_analysis_dropdown'  => 'required',
            'name_sub'                  => 'required',
            'location_id'               => 'required',
        ]);
        // Store Data
        $record = new HazardAnalysisSub;
        $record->description          = $request->name_sub;
        $record->hazard_analysis_id   = $request->hazard_analysis_dropdown;
        $record->bill_quant_id        = $request->bill_quant_id;
        $record->project_id           = $request->project_id;
        $record->project_location_id   = $request->location_id;
        $record->created_at           = date('Y-m-d H:i:s');
        $record->created_by           = userid();
        // Store data in database
        if($record->save())
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Store a newly created sub resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_hazard_inspection(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'equipment'         => 'required',
            'training'          => 'required',
            'pro_location_id'   => 'required',
        ]);
        // Store Data
        $record = new HazardInspection;
        $record->equipment              = $request->equipment;
        $record->training               = $request->training;
        $record->bill_quant_id          = $request->bill_quant_id;
        $record->project_id             = $request->project_id;
        $record->project_location_id    = $request->pro_location_id;
        $record->created_at             = date('Y-m-d H:i:s');
        $record->created_by             = userid();
        // Store data in database
        if($record->save())
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pro_id,$bq_id,$loc_id)
    {
        $proId = decrypt($pro_id);
        $bqId  = decrypt($bq_id);
        $locId = decrypt($loc_id);
        $data['pro_id'] = $pro_id;
        $data['bq_id']  = $bq_id;
        $data['locId']  = $locId;
        $data['hazard_inspection'] = HazardInspection::where('bill_quant_id',$bqId)->get();
        // Get plan record
        $data['plan'] = Plans::find($proId);
        // Get Language
        $data['lang'] = get_language();
        $data['hazardAnalysis'] = HazardAnalysis::get();
        $data['result'] = HazardAnalysis::has('hazardAnalysisSub')->with(['hazardAnalysisSub' => function($q) use ($bqId){
            $q->where('bill_quant_id',$bqId);
        }])->get();
        // Get Location
        $data['locations'] = Plan_locations::where('project_id',$proId)->get();
        // Load View
        return view('pmis/progress/hazard_analysis/hazard_analysis',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display more resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function more_hazard_analysis()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Load view
        return view('pmis/progress/hazard_analysis/more_hazard_analysis',compact('number','lang')); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_Hazard_Analysis_Data()
    {
        // Get Data
        $result = HazardAnalysis::with('hazardAnalysisSub')->get()->toArray();
        $final_array = array();
        $data = array();
        $data_child   = array();
        $parent_array = array();
        $child_array  = array();
        if($result)
        {
            foreach($result as $item)
            {
                $final_array[] = array('id'=>$item['id'],'parent' => '#', 'text' => $item['name']);
                foreach($item['hazard_analysis_sub'] as $sub)
                {
                    $final_array[] = array('id'=>$sub['id'],'parent' => $sub['hazard_analysis_id'], 'text' => $sub['description']);
                }
            }
        }         
        return json_encode($final_array);     
    }
}