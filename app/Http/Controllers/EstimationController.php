<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use App\Notifications\NewProject;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\StoreBillRequest;
use App\Imports\ExcelImport;
use App\Exports\ExcelExport;
use App\models\Requests;
use App\models\Plans;
use App\models\Estimation;
use App\models\Estimation_attachments;
use App\models\BillofquantitiesEst;
use App\models\Home;
use App\models\Plan_locations;
use App\models\Authentication\SectionSharings;
use App\models\Settings\Statics\Employees;
use App\models\EstimationTeam;
use App\models\Authentication\Users;
use App\models\ProjectUsers;
use App\User;
use Config;
use Validator;
use Redirect;
use Excel;
use Importer;
use PHPExcel_Style_Fill;

class EstimationController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department ID is exist or not
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_estimation'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_estimation"]);
    // Get Language
    $data['lang'] = get_language();
    // Get all data
    $data['records']  = Plans::whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_estimation');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_estimation','est_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view assigned records  
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_estimation');
                            });
                          }
                        })->whereHas('project_location', function($query){
                          $query->where('status','0');
                        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_estimation');
    })->where('department_id',$depID)->get();
    //Get URL Segments
    $data['segments'] = request()->segments();
    // load view to show result
    if(Input::get('ajax') == 1)
    {
      return view('pmis/estimations/list_ajax',$data);
    }
    else
    {
      return view('pmis/estimations/list',$data);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function estimation_notification($enc_id)
  {
    $id = decrypt($enc_id);
    $lang = get_language();
    $data['lang'] = $lang;
    if(Session::get('current_mod')!='pmis')
    {
      $name = 'name_'.$lang;
      session(['current_mod' => 'pmis']);
      $current_mod_name = Module::get_module_id('pmis')->$name;
      session(['current_mod_name' => $current_mod_name]);
    }
    session(['current_section' => "pmis_estimation"]);
    // Get data
    $data['records'] = Plans::where('id',$id)->get();
    // Load view to show result
    return view('pmis/estimations/list_notification',$data);
  }

  public function show($enc_id)
  {
    $data['lang'] = get_language();
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get plan record
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_estimation');
    }])->first();
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        $data['project_location'] = Plan_locations::where('project_id',$id)->get();
    }
    // Get Record
    $record = Estimation::where('project_id',$id)->first();
    $data['record'] = $record;
    if($record)
    {
      // Get Bill of Quantities
      $data['bill_quantities'] = BillofquantitiesEst::where('project_id',$id)->paginate(10)->onEachSide(1);
      // Get Bill of Quantities Total Price
      $total_price = BillofquantitiesEst::where('project_id',$id)->sum('estimated_total_price');
      // Get Total Percentage
      $total_percentage = getPercentage($record->cost,$total_price);
      // Get Remaining Percentage
      $data['remaining_percent'] = 100-$total_percentage;
      // Get Attachments
      $data['attachments'] = getAttachments('estimation_attachments',$id,$record->id,'estimation');
      //Pass Attachments table
      $data['table'] = 'estimation_attachments';
      // Get Allowed Sections for Sharing
      $data['sections'] = SectionSharings::where('code','pmis_estimation')->get();
      // Get previous department
      $previous_dep = $data['plan']->share()->where('share_to_code','pmis_survey')->first();
      if($previous_dep)
      {
        $data['previous_dep'] = $previous_dep->share_from;
      }
      else
      {
        $data['previous_dep'] = $previous_dep = $data['plan']->share()->where('share_to_code','pmis_design')->first()->share_from;
      }
      // Get units
      $data['units'] = Home::getAllStatics('units');
      $data['bq_sections'] = Home::getAllStatics('bq_sections');
      $data['section'] = 'estimation';
    }
    $data['project_location_id'] = 0;
    // Load view to show result
    return view('pmis/estimations/view',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Plan ID
    $enc_plan = $request->plan_id;
    // Get Plan Summary
    $plan = Plans::whereId(decrypt($enc_plan))->first();
    $summary = $plan;
    $employees = Employees::where('section','pmis_estimation')->get(); // Get all employees
    // Load view
    return view('pmis/estimations/add',compact('lang','enc_plan','plan','summary','employees'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
        //'cost'        => 'required',
        'start_date'  => 'required',
        'end_date'    => 'required',
        'employee_id' => 'required',
    ]);
    // Get Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('estimations','urn');
    // Get Plan ID
    $project_id = decrypt(Input::get('enc_plan'));
    // New Survey
    $record = new Estimation;
    $record->urn            = $urn;
    $record->project_id     = $project_id;
    $record->start_date     = dateProvider($request->start_date,$lang);
    $record->end_date       = dateProvider($request->end_date,$lang);
    $record->cost           = $request->cost;
    $record->employee_id    = $request->employee_id;
    $record->description    = $request->description;
    $record->created_at     = date('Y-m-d H:i:s');
    $record->created_by     = userid();
    $record->department     = departmentid();
    $record->process        = '0';
    if($record->save())
    {
      // Estimation Team Members
      if($request->employees)
      {
        $team=array();
        for($i=0;$i<count($request->employees);$i++)
        {
          $team[] = array(
            'estimation_id'   => $record->id,
            'employee_id'     => $request->employees[$i]
          );
        }
        if($team)
        {
          EstimationTeam::insert($team);
        }
      }
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request,$enc_id)
  {
    // Get Language
    $lang = get_language();
    // Record ID
    $id = decrypt($enc_id);
    // Plan ID
    $enc_plan = $request->plan_id;
    // Get Plan Summary
    $plan = Plans::whereId(decrypt($enc_plan))->first();
    if($plan)
    {
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
    }
    // Get Record
    $record = Estimation::whereId($id)->first();
    $employees = Employees::where('section','pmis_estimation')->get(); // Get all employees
    // Get team members
    $team = EstimationTeam::where('estimation_id',$record->id)->get();
    $team_members = array();
    if($team){
        foreach($team as $item){
            $team_members[] = $item->employee_id;
        }
    }
    // Load view to show result
    return view('pmis/estimations/edit',compact('lang','id','plan','record','summary','enc_id','employees','team_members','enc_plan'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request,$enc_id)
  {
    // Language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      //'cost'        => 'required',
      'start_date'  => 'required',
      'end_date'    => 'required',
      'employee_id' => 'required',
    ]);
    // Decrypt Record ID
    $id = decrypt($enc_id);
    $record = Estimation::find($id);
    $record->start_date     = dateProvider($request->start_date,$lang);
    $record->end_date       = dateProvider($request->end_date,$lang);
    $record->cost           = $request->cost;
    $record->employee_id    = $request->employee_id;
    $record->description    = $request->description;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    $updated = $record->save();
    if($updated)
    {
      // Estimation Team Members
      if($request->employees)
      {
        $team=array();
        for($i=0;$i<count($request->employees);$i++)
        {
          $team[] = array(
            'estimation_id' => $id,
            'employee_id'     => $request->employees[$i]
          );
        }
        if($team)
        {
          // Delete old records
          EstimationTeam::where('estimation_id',$id)->delete();
          // Add new records
          EstimationTeam::insert($team);
        }
      }
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Store a newly attachments in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function uploadDocuments($rec_id=0,$file_name="",$i=1)
  {
    // Getting all of the post data
    $file = Input::file('file_'.$i);
    $errors = "";
    if(Input::hasFile('file_'.$i))
    {
      // Validating each file.
      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
      $validator = Validator::make(
        [
            'file'      => $file,
            'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
            'file'      => 'required|max:500000',
            'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
        ]
      );
      if($validator->passes())
      {
        // Path is pmis/public/attachments/pmis...
        $destinationPath = 'attachments/pmis/estimations';
        $fileOrgName     = $file->getClientOriginalName();
        $fileExtention   = $file->getClientOriginalExtension();
        $fileOrgSize     = $file->getSize();
        if($file_name!=""){
          $name = $file_name;
          $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
        }else{
          $name = explode('.', $fileOrgName)[0];
          $file_name = $fileOrgName;
        }
        $upload_success = $file->move($destinationPath, $file_name);
        if($upload_success)
        {
          //Insert Record
          $attachment = new Estimation_attachments;
          $attachment->rec_id     = $rec_id;
          $attachment->filename   = $name;
          $attachment->path       = $destinationPath."/".$file_name;
          $attachment->extension  = $fileExtention;
          $attachment->size       = $fileOrgSize;
          $attachment->created_at = date('Y-m-d H:i:s');
          $attachment->created_by = userid();
          $attachment->save();
          return true;
        }
        else
        {
          $errors .= json('error', 400);
        }
      }
      else
      {
        // Return false.
        return false;
      }
    }
  }

  /**
   * Display a listing of the resource by keyword.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter_estimation(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    // Search Shared Plan
    $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$department_id,$section,$request->role);
    // Load View to display records
    return view('pmis/estimations/list_filter',compact('records','lang','request'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store_bill(Request $request)
  {
    //Validate the request...
    $validates = $request->validate([
      'operation_type'      => 'required',
      'unit_id'             => 'required',
      'amount'              => 'required',
      'price'               => 'required',
      'percentage'          => 'required',
      'bq_section_id'       => 'required',
      'project_location_id' => 'required',
    ]);
    $lang = get_language();
    $pla_id = decrypt(Input::get('pla_id'));//Plan ID
    $rec_id = decrypt(Input::get('rec_id'));//Record ID
    //New survey
    $record = new BillofquantitiesEst;
    $record->rec_id               = $rec_id;
    $record->project_id           = $pla_id;
    $record->operation_type       = $request->operation_type;
    $record->unit_id              = $request->unit_id;
    $record->amount               = $request->amount;
    $record->estimated_price      = $request->price;
    $record->estimated_total_price= $request->total_price;
    $record->remarks              = $request->remarks;
    $record->project_location_id  = $request->project_location_id;
    $record->bq_section_id        = $request->bq_section_id;
    $record->created_at           = date('Y-m-d H:i:s');
    $record->created_by           = userid();
    $record = $record->save();
    if($record)
    {
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update_bill(Request $request)
  {
    //Validate the request...
    $validates = $request->validate([
      'operation_type'      => 'required',
      'unit_id'             => 'required',
      'amount'              => 'required',
      'price'               => 'required',
      'percentage'          => 'required',
      'bq_section_id'       => 'required',
      'project_location_id' => 'required',
    ]);
    $lang = get_language();
    //ddd($request->all());
    $pla_id = decrypt(Input::get('pla_id'));//Plan ID
    $data['enc_id'] = encrypt($pla_id);
    $rec_id = decrypt(Input::get('rec_id'));//Record ID
    // Get Record
    $data['record'] = Estimation::where('project_id',$pla_id)->first();
    //New survey
    $record = BillofquantitiesEst::find($rec_id);
    $record->operation_type       = $request->operation_type;
    $record->unit_id              = $request->unit_id;
    $record->amount               = $request->amount;
    $record->estimated_price      = $request->price;
    $record->estimated_total_price= $request->total_price;
    $record->remarks              = $request->remarks;
    $record->project_location_id  = $request->project_location_id;
    $record->bq_section_id        = $request->bq_section_id;
    $record->updated_at           = date('Y-m-d H:i:s');
    $record->updated_by           = userid();
    $record = $record->save();
    if($record)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  // Bill of Quantity list based on locations
  public function locationBillQuantityList($enc_id,$loc_id,Request $request)
  {
    $project_location_id = $loc_id;
    $data['lang'] = get_language();
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get plan record
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_estimation');
    }])->first();
    if($data['plan'])
    {
      // Get summary data
      $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
      $data['project_location'] = Plan_locations::where('project_id',$id)->get();
    }
    // Get Record
    $record = Estimation::where('project_id',$id)->first();
    $data['record'] = $record;
    if($record)
    {
      // Get Bill of Quantities Total Price
      $total_price = BillofquantitiesEst::where('project_id',$id)->sum('estimated_total_price');
      // Get Total Percentage
      $total_percentage = getPercentage($record->cost,$total_price);
      // Get Remaining Percentage
      $data['remaining_percent'] = 100-$total_percentage;
      // Get Attachments
      $data['attachments'] = getAttachments('estimation_attachments',$id,$record->id,'estimation');
      //Pass Attachments table
      $data['table'] = 'estimation_attachments';
      // Get Allowed Sections for Sharing
      $data['sections'] = SectionSharings::where('code','pmis_estimation')->get();
      // Get previous department
      $previous_dep = $data['plan']->share()->where('share_to_code','pmis_survey')->first();
      if($previous_dep)
      {
        $data['previous_dep'] = $previous_dep->share_from;
      }
      else
      {
        $data['previous_dep'] = $previous_dep = $data['plan']->share()->where('share_to_code','pmis_design')->first()->share_from;
      }
      // Get units
      $data['units'] = Home::getAllStatics('units');
      $data['bq_sections'] = Home::getAllStatics('bq_sections');
      $data['section'] = 'estimation';
      $data['project_location_id'] = $project_location_id;
      // Get Bill of Quantities
      $data['bill_quantities'] = BillofquantitiesEst::where('project_id',$id)->where('project_location_id',$project_location_id)->orderBy('id','asc')->orderBy('bq_section_id','asc')->paginate(10)->onEachSide(3);

      // Get Total Price
      $data['total_price'] = BillofquantitiesEst::where('project_id',$id)->where('project_location_id',$project_location_id)->sum('estimated_total_price');
    }
    if(Input::get('ajax') == 1)
    {
        //Static record iteration with pagination
        $data['counter'] = $request->counter;
        // Load view to show result
        return view('pmis/estimations/show_ajax',$data);
    }
    else
    {
        // Load view to show result
        return view('pmis/estimations/show',$data);
    }
  }

  // Import BQ By Excel
  public function importExcel(Request $request)
  {
    $request->validate([
        'fileToUpload'        => 'required|file|max:2048|mimes:xls,xlsx',
        'project_location_id' => 'required',
        'bq_section_id'       => 'required',
    ]);
    $lang = get_language();
     $dataTime = date('Ymd_His');
     $file = request()->file('fileToUpload');
     $filename = $dataTime.'-'. $file->getClientOriginalName();
     $savePath = 'public/upload/';
     $file->move($savePath,$filename);

     $excel = Importer::make('Excel');
     $excel->load($savePath.$filename);
     $collection = $excel->getCollection();
     $data = $collection->slice(7);
     if(!empty($data)){
          try{
            $records = array();
            $x=0;
            foreach($data as $item)
            {
              if($item['0']!=''){
                $value = Config::get('static.units_result.'.$item[1].'.id');
                if(!isset($item['5']))
                {
                  $item['5'] = "";
                }
                $data_array = array(
                  'rec_id'                => $request->rec_id,
                  'project_id'            => decrypt($request->project_id),
                  'project_location_id'   => $request->project_location_id,
                  'operation_type'        => $item['0'],
                  'unit_id'               => $value,
                  'amount'                => $item['2'],
                  'estimated_price'       => $item['3'],
                  'estimated_total_price' => $item['4'],
                  'remarks'               => $item['5'],
                  'bq_section_id'         => $request->bq_section_id,
                  'created_by'            => userid(),
                  'created_at'            => date('Y-m-d H:i:s'),
                );
                $x++;
                array_push($records,$data_array);
              }

            }

            BillofquantitiesEst::insert($records);
            Session()->flash('success', __("global.success_msg"));
          }
          catch(\Exception $e){
            return Session()->flash('fail', __("global.failed_msg"));
          }
      }
     else{
      return Session()->flash('fail', __("global.failed_msg"));
     }
  }

  // Print Estimation Reports
  public function printExport($enc_id,$loc_id)
  {
    $id= decrypt($enc_id);
    $project_location_id= decrypt($loc_id);
    // Get Record
    $record = Estimation::where('project_id',$id)->first();
    $project_name = Plans::whereId($id)->first()->name;
    // Get BQ Data
    $bill_quantity = BillofquantitiesEst::where('project_id',$id)->where('project_location_id',$project_location_id)->get();
    Excel::load('public/print/BillOfQuantity.xlsx', function($file) use($bill_quantity,$record){
      $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($bill_quantity,$record){
        $row   = 8;
        $index = 0;
        $bq_section = 0;
        foreach($bill_quantity as $item)
        {
          $lang = get_language();
          if($bq_section != $item['bq_section_id']){
            $bq_section = $item['bq_section_id'];
            // Apply style
            $sheet->getStyle('A'.$row.':G'.$row)->applyFromArray(
              array(
                'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'EEEEEE')
                ),
                'font' => array(
                  'name' =>  'B Nazanin',
                  'size' =>  13,
                  'bold' =>  true
                )
              )
            );
            $sheet->setHeight($row, 40);
            $sheet->mergeCells('A'.$row.':G'.$row);
            // Set value
            $sheet->setCellValue('A'.$row.'',$item['bq_section']['name_'.$lang]);
            $row++;
            $index = 1;
          }
          // Apply style
          $sheet->getStyle('A'.$row.':G'.$row)->applyFromArray(
            array(
              'font' => array(
                'name' =>  'B Nazanin',
                'size' =>  11,
                'bold' =>  false
              )
            )
          );
          $sheet->setHeight($row, 50);
          $sheet->setCellValue('A'.$row.'',$index);
          $sheet->setCellValue('B'.$row.'',$item['operation_type']);
          $sheet->setCellValue('C'.$row.'',$item->unit->{'name_'.$lang});
          $sheet->setCellValue('D'.$row.'',$item['amount']);
          $sheet->setCellValue('E'.$row.'',$item['estimated_price']);
          $sheet->setCellValue('F'.$row.'',$item['estimated_total_price']);
          $sheet->setCellValue('G'.$row.'',$item['remarks']);
          $row++;
          $index++;
        }
      });
    })->setFilename(__("estimation.bill_quantitie")." ".$project_name)->export('xlsx');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      // Get project name
      $project_name = Plans::find($request->record_id)->name;
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('estimation', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.project_assigned',['pro'=>$project_name]), userid(), $redirect_url));
              }     
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_estimation')->get();
    // Load view to show result
    return view('pmis/estimations/list_assigned_users',compact('records'));
  }
}
