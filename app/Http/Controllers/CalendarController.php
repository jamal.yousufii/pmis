<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\models\Plans;
use App\models\CalendarWorkingDay;
use App\models\CalendarHoliday;
use App\models\Billofquantities;
use App\models\Plan_locations;
use Redirect;

class CalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0,$dep_id=0)
    {
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_calendar'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get plan record
        $plan = Plans::find($id);
        $projects = Plans::whereHas('CalendarHoliday')->get();
        // Check for BQ Schedule
        $schedule = Billofquantities::where('project_id',$id)->first();
        // Load tabs
        $tabs = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_calendar']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $project_location = Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $project_location = Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $excluded_locations = Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        // Load view to show result
        return view('pmis/progress/calendar/index',compact('plan','tabs','enc_id','depID','lang','projects','schedule','project_location','excluded_locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['created_by']    = userid();
        $request['department_id'] = departmentid();
        // New Record
        if($request->saturday || $request->sunday || $request->monday || $request->tuesday || $request->wednesday || $request->thursday || $request->friday)
        {
            $record = CalendarWorkingDay::create($request->all());
            if($record)
            {
                Session()->flash('success', __("global.success_msg"));
            }
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            }  
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_holidays(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'from_date'     => 'required',
            'to_date'       => 'required',
            'description'   => 'required',
        ]);

        $lang = get_language();
        $request['created_by']    = userid();
        $request['department_id'] = departmentid();
        if($request->from_date){
            $request['from_date'] = dateProvider($request->from_date,$lang);
        }
        if($request->to_date){
            $request['to_date'] = dateProvider($request->to_date,$lang);
        }
        // New Record
        $record = CalendarHoliday::create($request->all());
        if($record)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import_holidays(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'imp_project_id'  => 'required',
            'imp_location_id' => 'required',
        ]);
        $imp_pro_id = $request->imp_project_id;
        $imp_loc_id = $request->imp_location_id;
        $pro_id = $request->project_id;
        $loc_id = $request->location_id;
        // Get project holidays
        $holidays = CalendarHoliday::where('project_id',$imp_pro_id)->where('location_id',$imp_loc_id)->get();
        $data = array();
        foreach($holidays as $item)
        {
            $item_data = array(
                'project_id'    => $pro_id,
                'location_id'   => $loc_id,
                'from_date'     =>  $item->from_date,
                'to_date'       =>  $item->to_date,
                'description'   =>  $item->description,
                'created_by'    =>  userid(),
                'created_at'    =>  date('Y-m-d H:i:s'),
                'department_id' =>  departmentid(),
            );
            array_push($data,$item_data);       
        }
        if(COUNT($data)>0)
        {
            // Import data
            CalendarHoliday::insert($data);
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $Calendar
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0,$dep_id=0,$loc_id=0)
    {
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_calendar'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        // Project Location id
        $project_location_id = $loc_id;
        session(['project_location_id' => $loc_id]);
        // Project id
        $id = decrypt($enc_id);
        // Get plan record
        $plan = Plans::find($id);
        $working_days = CalendarWorkingDay::where('project_id',$id)->where('location_id',$project_location_id)->first();
        $holidays = CalendarHoliday::where('project_id',$id)->where('location_id',$project_location_id)->get();
        // Get holidays from other projects
        $projects = Plans::whereHas('CalendarHoliday')->where('department',$depID)->get();
        // Check for BQ Schedule
        $schedule = Billofquantities::where('project_id',$id)->where('project_location_id',$project_location_id)->first();
        // Load tabs
        $tabs = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_calendar']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $project_location = Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $project_location = Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $excluded_locations = Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        // Load view to show result
        return view('pmis/progress/calendar/calendar',compact('plan','project_location_id','working_days','holidays','tabs','enc_id','depID','lang','projects','schedule','project_location','excluded_locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $Calendar
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $Calendar
     * @return \Illuminate\Http\Response
     */
    public function update($enc_id=0,Request $request)
    {
        if($request->saturday OR $request->sunday OR $request->monday OR $request->tuesday OR $request->wednesday OR $request->thursday OR $request->friday)
        {
            // weather id 
            $id = decrypt($enc_id);
            $request['updated_by'] = userid();
            // Update Record
            if(!$request->saturday){
                $request['saturday'] = '0';   
            }
            if(!$request->sunday){
                $request['sunday'] = '0';   
            }
            if(!$request->monday){
                $request['monday'] = '0';   
            }
            if(!$request->tuesday){
                $request['tuesday'] = '0';   
            }
            if(!$request->wednesday){
                $request['wednesday'] = '0';   
            }
            if(!$request->thursday){
                $request['thursday'] = '0';   
            }
            if(!$request->friday){
                $request['friday'] = '0';   
            }
            $record = CalendarWorkingDay::find($id); 
            $record->update($request->except('id'));
            if($record)
            {
                Session()->flash('success', __("global.success_edit_msg"));
            }
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            }
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $Calendar
     * @return \Illuminate\Http\Response
     */
    public function update_holidays(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $Calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$enc_id=0)
    {
        // Decrypt ID
        $id = decrypt($enc_id);
        // Update record
        $record = CalendarHoliday::find($id);
        $id = $record->delete();
        if($id!=0)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }
    
    /**
     * return the project location .
     *
     * @return HTML
     */
    public function get_locations(Request $request)
    {
        $project_id = $request->id;
        $lang = get_language();
        // Get Project Locations by Users
        $locations = Plan_locations::where('project_id',$project_id)->whereHas('CalendarHoliday')->orderBy('id','desc')->get();
         $option = '<option value="">'. trans('global.select') .'</option>';
         if($locations)
         {
            $district   = "";
            $village    = "";
            $latitude   = "";
            $longitude  = "";
            foreach($locations as $location){
              if($location->district_id){
                $district = " / ".$location->district->{'name_'.$lang};
              }
              if($location->village_id){
                $village = " / ".$location->village->{'name_'.$lang};
              }
              if($location->latitude){
                $latitude = " / ".$location->latitude;
              }
              if($location->longitude){
                $longitude = " / ".$location->longitude;
              }
              $option .= "<option value=".$location->id.">". $location->province->{'name_'.$lang} . $district . $village . $latitude . $longitude."</option>";
            }
         }

         return $option;
    }
}
