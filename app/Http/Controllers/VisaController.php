<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\models\Requests;
use App\models\Architecture;
use App\models\Structure;
use App\models\Electricity;
use App\models\Water;
use App\models\Mechanic;
use App\models\Plans;
use App\models\Authentication\SectionSharings;
use DB;

class VisaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//project id
        $data['enc_id'] = $enc_id;
        $data['with_parent_id'] = $enc_id; 
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_section' => "pmis_design"]);
        session(['current_tab' => 'tab_visa']);
        // Get Allowed Sections for Sharing
        $data['sections'] = SectionSharings::where('code','pmis_design')->get();
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        $data['shared'] = $data['plan']->share()->where('share_from_code','pmis_design')->first();
        // Get all section`s records 
        $architecture = Architecture::select(DB::raw('*, "designs.architecture" as section, "architecture.approve" as route_approve, "architecture.view" as route_view, "tab_architecture" as tab_section, "arch_visa" as tab_role'))->where('project_id',$id)->where('completed',1)->whereIn('version',['first','latest'])->orderBy('id','desc');
        $structure = Structure::select(DB::raw('*, "designs.structure" as section, "structure.approve" as route_approve, "structure.view" as route_view, "tab_structure" as tab_section, "str_visa" as tab_role'))->where('project_id',$id)->where('completed',1)->whereIn('version',['first','latest'])->orderBy('id','desc');
        $electricity = Electricity::select(DB::raw('*, "designs.electricity" as section, "electricity.approve" as route_approve, "electricity.view" as route_view, "tab_electricity" as tab_section, "ele_visa" as tab_role'))->where('project_id',$id)->where('completed',1)->whereIn('version',['first','latest'])->orderBy('id','desc');
        $water = Water::select(DB::raw('*, "designs.water" as section, "water.approve" as route_approve, "water.view" as route_view, "tab_water" as tab_section, "wat_visa" as tab_role'))->where('project_id',$id)->where('completed',1)->whereIn('version',['first','latest'])->orderBy('id','desc');
        $data['all_design'] = Mechanic::select(DB::raw('*, "designs.mechanic" as section, "mechanic.approve" as route_approve, "mechanic.view" as route_view, "tab_mechanic" as tab_section, "mec_visa" as tab_role'))->where('project_id',$id)->where('completed',1)
                    ->whereIn('version',['first','latest'])
                    ->orderBy('id','desc')
                    ->unionAll($architecture)
                    ->unionAll($structure)
                    ->unionAll($electricity)
                    ->unionAll($water)
                    ->orderBy('created_at','desc')
                    ->get();
        // Check if desing has data then allow sharing
        $data['architecture'] = Architecture::where('project_id',$id)->where('status',1)->whereIn('version',['first','latest'])->first();
        // Load view to show result
        return view('pmis/designs/visa/visa',$data);         

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
