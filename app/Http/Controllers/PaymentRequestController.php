<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Requests;
use App\models\Estimation;
use App\models\Billofquantities;
use App\models\PaymentRequest;
use App\models\PaymentRequestActivity;
use App\models\Procurement;
use Redirect;
use DB; 

class PaymentRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id=0,Request $request)
    {
     
        // Get language 
        $lang = get_language();

        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = session('current_department');
        }

        // Get department id
        $depId = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depId),'pmis_payments'))
        {
            return view('access_denied');
        }
        // Set project id && location to the seasion while filtering 
        if(!empty($request->project_id))
            session(['project_id' => $request->project_id]); session(['project_location_id' => $request->project_location_id]); 
        // If project id not passed get form session
        if($dep_id==0)
            $dep_id = session('current_department');
        $projects = '';     
        // Project id  
        $project_id = session('project_id');
        if($depId==6) //Contractor request 
        {
            $projects = Plans::whereHas('project_contractor', function($query) use($project_id){
                $query->where('project_id',$project_id); 
            })->orderBy('id','desc')->get(); 
        }
        else
        {
            // Get Project which is shared with this department  
            $projects = Plans::whereHas('share', function($query) use ($depId){
                                $query->where('share_to',$depId);
                                $query->where('share_to_code','pmis_progress');
                            })->orderBy('id','desc')->get();
        }
        if(!session::has('project_id')) //Only open the filter page for the search 
        {
            session(['current_section' => "pmis_payments","current_department" => $dep_id]);
            return view('pmis/payment/request/fiter_project',compact('projects')); 
        }   
        else 
        {      
            // Contractor id
            $contractor_id = session('current_contractor');
            // Get plan record
            $plan = Plans::find($project_id);
            if($plan)
            {
                // Get summary data
                $summary = Requests::whereId($plan->request_id)->first();
            }
            // Get locations
            $project_location = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $contractor_id){
                $query->where('user_id',userid());
                $query->where('project_id',$project_id);
            })->orderBy('id','desc')->get();
            $payments = PaymentRequest::with('user')
                                        ->whereHas('paymentRequestAcitvity', function($query) use($project_id){
                                            $query->where('project_id',$project_id);
                                        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
            // Set current section in session
            session(['current_section' => "pmis_payments"]);
            session(['project_id' => $project_id]);
            $request->session()->flash('extra_section','for_project');
            $segments = request()->segments();
            if($request->ajax == 1)
            {
                // Static record iteration with pagination
                $counter = $request->counter;
                // Load view to show result
                return view('pmis/payment/request/payment_request_list_ajax', compact('lang','project_id','contractor_id','plan','summary','project_location','dep_id','segments','payments','projects'));
            }
            else
            {
                // Load view to show result
                return view('pmis/payment/request/index',compact('lang','project_id','contractor_id','plan','summary','project_location','dep_id','segments','payments','projects'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lang = get_language();
        // Project id 
        $project_id = session('project_id');
        // Contractor id
        $contractor_id = session('current_contractor');
        // Department id
        $dep_id = session('current_department');
        // Get plan record
        $plan = Plans::find($project_id);
        // Get locations
        $project_location = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $contractor_id){
            $query->where('user_id',userid());
            $query->where('project_id',$project_id);
            $query->where('contractor_id',decrypt($contractor_id));
        })->orderBy('id','desc')->get();
        // Set current section in session
        session(['current_section' => "pmis_payment_request"]);
        session(['project_id' => $project_id]);
        $request->session()->flash('extra_section','for_project');
        // Load view to show result
        return view('pmis/payment/request/create',compact('lang','project_id','contractor_id','plan','project_location','dep_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activities = session('payment_request.activity');
        $activity_array = [];
        // Remove first layer and sort the index of array 
        $activity_array =  array_merge($activity_array,array_map(function($activities) {  return array_pop($activities); }, $activities));
        try // Start of the transaction 
        {
            // Generate URN
            $urn = GenerateURN('payment_requests','urn');
            DB::beginTransaction();
            $payment_request = new PaymentRequest;
            $payment_request->urn                     = $urn;
            $payment_request->request_number          = $request->request_number;
            $payment_request->request_date            = dateProvider($request->request_date);
            $payment_request->description             = $request->description;
            $payment_request->contractor_id           = decrypt(session('current_contractor'));
            $payment_request->total_request_amount    = session('payment_request.money_total');
            $payment_request->total_request_activity  = session('payment_request.counter');
            $payment_request->created_by              = userid(); 
            $payment_request->save(); 
            // Add payment request id to
            for($i=0; $i<count($activity_array); $i++)
            {
                $activity_array[$i]['payment_request_id']   = $payment_request->id;
            }
            // Store Payment Request Activity
            PaymentRequestActivity::insert($activity_array);
            //ddd($activity_array);
            // Set the success session 
            Session()->flash('success', __("global.success_msg"));
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            Session()->flash('fail', __("global.failed_msg"));
            DB::rollBack(); 
        }
        
        return Redirect::route("payment_request.index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentRequest $paymentRequest)
    {
        if(doIHaveRoleInDep(session('current_department'),'pmis_payments','pmis_view_payment'))
        {
            $lang = get_language();
            //Projct "Plan id" 
            $project_info = $paymentRequest->paymentRequestAcitvity->first();
            // Get plan record
            $plan = Plans::find($project_info->project_id);

            $project_id = $project_info->project_id;  
            $project_location_id = $project_info->project_location_id; 
            // Price of the project from procurement 
            $procurement = Procurement::where('project_id',$project_id)->first(); 
            // Get Estimation 
            $estimation = Estimation::where('project_id',$project_id)->first(); 
            // Bill quantity total price 
            $project_bq = Billofquantities::select(DB::raw('SUM(total_price) as bq_total_price'))->where('project_id',$project_id)->get();
            //Bill of quantity and activity progress 
            $bill_quantities  = Billofquantities::activities_calculations($project_id,$project_location_id);
            // Get all activities done price of project and location 
            $all_done_act_price = Billofquantities::culculate_total_act_done_price($bill_quantities); 
            // previouse payment request of this project 
            $requested_activities = PaymentRequestActivity::get_previouse_activity($project_id,$project_location_id);
            //Previouse invocie total amount activity done 
            $previouse_inv_total_money = PaymentRequestActivity::previouse_inv_total_amount($requested_activities);
            // Current invoice requested 
            $payment_request = PaymentRequest::calculate_invocie_payment($paymentRequest->id); 
            
            session(['current_section' => "pmis_payments"]);
            session(['project_id' => $project_id]);
           
            
            return view('pmis.payment.request.show',compact('payment_request','lang','plan','procurement','estimation','project_bq','bill_quantities','all_done_act_price','requested_activities','previouse_inv_total_money','paymentRequest')); 
        }
        else
        {
            return view('access_denied');

        }
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Load Bill quantity  
     */
    public function paymentBquantity(Request $request)
    {
        // Destroy previus sessions
        session()->forget('payment_request.counter');
        session()->forget('payment_request.money_total');
        session()->forget('payment_request.activity');
        $lang = get_language();
        $project_id = session('project_id'); 
        $project_location_id = session('project_location_id'); 
        // Price of the project from procurement 
        $procurement = Procurement::where('project_id',$project_id)->first(); 
        // Get Estimation 
        $estimation = Estimation::where('project_id',$project_id)->first(); 
        // Bill quantity total price 
        $project_bq = Billofquantities::select(DB::raw('SUM(total_price) as bq_total_price'))->where('project_id',$project_id)->get();
        //Bill of quantity and activity progress 
        $bill_quantities  = Billofquantities::activities_calculations($project_id,$project_location_id);
        // Get all activities done price of project and location 
        $all_done_act_price = Billofquantities::culculate_total_act_done_price($bill_quantities); 
        // previouse payment request of this project 
        $requested_activities = PaymentRequestActivity::get_previouse_activity($project_id,$project_location_id); 
        //Previouse invocie total amount activity done 
        $previouse_inv_total_money = PaymentRequestActivity::previouse_inv_total_amount($requested_activities);
        // Selected bq  
        $selected_bq = payment_request_bq_check(session::get('payment_request.activity'));
    
        if($request->ajax == 1)
        {
         // Load view to show result
          return view('pmis/payment/request/show_ajax',$data);
        }
        else
        {
          // Load view to show result
          return view('pmis/payment/request/bilquantity',compact('lang','estimation','bill_quantities','project_bq','selected_bq','procurement','project_id','project_location_id','requested_activities','all_done_act_price','previouse_inv_total_money'));
        }
    }

    public function addActivityToRequest(Request $request)
    {
        $total_request_amount = 0; 
        $data = [
                'project_id'            => $request->project_id, 
                'project_location_id'   => $request->project_location_id, 
                'bq_id'                 => $request->bq_id, 
                'request_amount'        => $request->request_amount, 
        ];
        $request_amount_money = $request->request_amount * $request->item_price; 
        $counter = (session::has('payment_request.counter')==true ? session('payment_request.counter') : 0);
        $total_request_amount = (session::has('payment_request.money_total')==true ? session('payment_request.money_total') : $request_amount_money);
        if($request->is_check==1) //checkbox is check
        {  
            $counter = ++$counter; 
            $total_request_amount = ($counter==1? $request_amount_money  : $total_request_amount + $request_amount_money);
            session::push('payment_request.activity.'.$request->bq_id,$data);
        }
        else // discheck
        {  
            $counter = ($counter==0 ? 0 : --$counter);
            $total_request_amount = ($counter==0 ? 0 : $total_request_amount - $request_amount_money);
            session::forget('payment_request.activity.'.$request->bq_id);  
        }

        // Calculation for current request percentage 
        $current_inovice = $request->all_done_act_price - $request->prev_invo;
        session(['payment_request.counter' =>  $counter]);
        session(['payment_request.money_total' =>  $total_request_amount]);

        echo view('pmis.payment.request.payment_request_summary',compact('counter','total_request_amount','request','current_inovice')); 
    }

    /**
     * @Date: 2020-08-15 14:19:27 
     * @Desc:  Recieve the invoice 
     */
    public function recieveInvoice(Request $request)
    {
        $payment_request = paymentRequest::find($request->payment_request_id);
        if($request->status=='approve')
        {
          $payment_request->status = $payment_request->status + 1; 
        }
        else
        {
          $payment_request->status = $payment_request->status - 1; 
        }
        
        if($payment_request->save())
        {
             Session()->flash('success', __("global.success_process_msg"));
        }
        else
        {
             Session()->flash('fail', __("global.failed_msg"));
        }
          
        return view('pmis.payment.request.payment_recieve',compact('payment_request')); 
    }
}
