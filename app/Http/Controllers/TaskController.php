<?php
namespace App\Http\Controllers;
use App\models\Task;
use Illuminate\Http\Request;
use App\Notifications\NewProject;
use App\models\Authentication\Departments;
use App\models\Static_data;
use Illuminate\Support\Facades\Input;
use App\models\Authentication\Users;
use App\Notifications\TaskNotify;
use App\User;
use Redirect;

class TaskController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_tasks'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_tasks"]);
        $lang = get_language();
        // Get my tasks
        $tasks = Task::where('created_by',userid())->get();
        // Get all departments
        $departments = Departments::get();
        // Get all statics
        $priority = Static_data::where('type',10)->get();
        //Get URL Segments
        $segments = request()->segments();
        // Load view to display data
        return view('pmis/tasks/task',compact('tasks','departments','dep_id','lang','priority','segments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = $request->validate([
            'title'      => 'required',
            'start_date' => 'required',
            'duration'   => 'required',
            'priority'   => 'required',
            'departments'=> 'required',
            'user_id'    => 'required',
            'description'=> 'required',
        ]);
        // Insert Record
        $record = new Task;
        $record->title      = $request->title;
        $record->start_date = dateProvider($request->start_date);
        $record->duration   = $request->duration;
        $record->priority   = $request->priority;
        $record->dep_id     = $request->departments;
        $record->user       = $request->user_id;
        $record->description= $request->description;
        $record->status     = 1;
        $record->created_at = date('Y-m-d H:i:s');
        $record->created_by = userid();
        if($record->save())
        {
            // Start Notification;
            $redirect_url = route('assignedTasks',$record->id);
            $users = User::where('id',$request->user_id)->get();
            if($users)
            {
                foreach($users as $user) {
                    $user->notify(new NewProject($record->id,'global.new_task', userid(), $redirect_url));
                }
            }
            // End Notification
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }


    public function updateTasks(Request $request){

        $validates = $request->validate([
            'title'      => 'required',
            'start_date' => 'required',
            'duration'   => 'required',
            'priority'   => 'required',
            'departments'=> 'required',
            'user_id'    => 'required',
            'description'=> 'required',
        ]);
        $id = $request->record_id;
        // Update Record
        $record = Task::find($id);
        $record->title      = $request->title;
        $record->start_date = dateProvider($request->start_date);
        $record->duration   = $request->duration;
        $record->priority   = $request->priority;
        $record->dep_id     = $request->departments;
        $record->user       = $request->user_id;
        $record->description= $request->description;
        $record->status     = 1;
        $record->created_at = date('Y-m-d H:i:s');
        $record->created_by = userid();
        if($record->save())
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    //Get Users based On Departments
    public function bringDepartmentUsers()
    {
        $department_id = Input::get('dep_id');
        //Get users of Department
        $users  = Users::where('department_id',$department_id)->get();
        //Return response View
        return view('pmis.tasks.usersDepsDropdown',compact('users','department_id'));
    }

    //Update Tasks Status
    public function updateTaskStatus(){
        $id          = Input::get('id');
        $status      = Input::get('status');
        $description = Input::get('description');
        if($status==3)
        {
            $update = Task::where('id',$id)->update(['status'=> $status,'complete_comment'=> $description]);
        }
        else
        {
            $update = Task::where('id',$id)->update(['status'=> $status,'progress_comment'=> $description]);
        }

        if($update){
            // Start Notification;
            if($status==3)
            {
                $created_by   = Task::find($id)->value('created_by');
                $redirect_url = route('assignedTasks',$id);
                $users = User::where('id',$created_by)->get();
                if($users)
                {
                    foreach($users as $user) {
                        $user->notify(new NewProject($id,'global.new_task_completed', userid(), $redirect_url));
                    }
                }
            }
            // End Notification
            return "Success";
        }
        else{
            return "Failed";
        }

    }

    //Bring Edit Tasks
    public function bringEditView(){
        $id = Input::get('id');
        $lang = get_language();
        $task = Task::where('id',$id)->first();
        $departments = Departments::get();
        $users  = Users::where('department_id',$task->dep_id)->get();
        // Get all statics
        $priority    = Static_data::where('type',10)->get();
        return view('pmis.tasks.edit',compact('task','departments','users','lang','priority'));
    }

    public function calendarTasks()
    {
        session(['current_section' => "pmis_tasks"]);
        $lang = get_language();
        // Get my tasks
        $tasks = Task::where(function($query){
            if(is_admin()) {
              //Can view all records 
            }
            else{ 
              $query->where('user',userid());
            }
          })->get();
        // Kill session of sections
        session()->forget('current_section');
        //Get URL Segments
        $segments = request()->segments();
        return view('pmis/tasks/calendar_tasks',compact('tasks','lang','segments'));
    }

    public function assignedTasks($id=0)
    {
        session(['current_section' => "pmis_tasks"]);
        $lang = get_language();
        // Get my tasks
        $tasks = Task::where('id',$id)->get();
        // Kill session of sections
        session()->forget('current_section');
        //Get URL Segments
        $segments = request()->segments();
        return view('pmis/tasks/assigned_tasks',compact('tasks','lang','segments'));
    }
}
