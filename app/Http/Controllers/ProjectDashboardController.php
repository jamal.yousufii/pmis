<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\models\Plans;
use App\models\Billofquantities;
use App\models\BillOfQuantitiesBase;
use App\models\Plan_locations;
use App\models\Settings\Portfolios;
use Config;
use Redirect;

class ProjectDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_project_dashboard'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_project_dashboard"]);
        // Get language
        $lang = get_language();
        $data['lang'] = $lang;
        // Get department codes
        $dep_code = getColumn('pmis_auth','departments',['id' => $depID],'code');
        $data['records'] = Plans::whereHas('share', function($query) use ($depID,$dep_code){
            if($dep_code!='dep_01')
                $query->where('share_to',$depID);
            $query->where('share_to_code','pmis_progress');
        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
        //Get URL Segments
        $data['segments'] = request()->segments();
        // Load view to show result
        if(Input::get('ajax') == 1)
        {
            return view('pmis/project_dashboard/list_ajax',$data);
        }
        else
        {
            return view('pmis/project_dashboard/list',$data);
        }
    }

    /**
     * Function to display the dashboard based on all locations.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0,$dep_id=0)
    {
        session(['current_section' => "pmis_project_dashboard"]);
        $lang = get_language();
        // Project id
        $project_id = decrypt($enc_id);
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = $dep_id;
        }
        // Get Department ID
        $depID = decrypt($dep_id);
        $dep_id = $depID;
        // Get plan record
        $plan = Plans::find($project_id);
        // Get project locations
        $location_id = Plan_locations::where('project_id',$project_id)->pluck('id')->toArray();
        $project_location = Plan_locations::where('project_id',$project_id)->orderBy('id','desc')->get();
        // ddd($location_id);
        // Get excluded locations
        $exc_locations = Plan_locations::where('project_id',$project_id)->doesntHave('bill_quantities_base')->get();
        $excluded_locations = array();
        if($exc_locations){
            foreach($exc_locations as $item){
                $excluded_locations[] = $item->id;
            }
        }
        if($excluded_locations)
        {
            // Exclude locations
            $location_id = array_diff($location_id,$excluded_locations); 
            // Reset array index to zero
            $location_id = array_values($location_id);  
        }
        $portfolios = Portfolios::whereHas('PortfoliosSub', function($query) use ($project_id){
            $query->where('project_id',$project_id);
        })->get();
        // Get Start and End date of current fiscal year
        $start_fiscal_date = Config::get('static.fiscal_year_months.01.start_date');
        $end_fiscal_date   = Config::get('static.fiscal_year_months.12.end_date');
        // Get project location week days
        $WeekDays = weekDaysAll($location_id);
        // Get project location  holidays
        $holidays = holidaysAll($location_id);
        // cfy working days 
        //$cfy_wrkd = working_days($start_fiscal_date,$end_fiscal_date,$WeekDays,$holidays);
        $cfy_wrkd = 0; // Not Used
        // Get first date of planed schedule
        $getFirstDateOfProject = getFirstLastDateOfProject($location_id,'min(start_date)','project_location_id');
        // Get last date of planed schedule
        $getLastDateOfProject = getFirstLastDateOfProject($location_id,'max(end_date)','project_location_id');
        
        // Get project budget from base
        $project_base_budget = BillOfQuantitiesBase::whereIn('project_location_id',$location_id)->get(); 
        // Get project budget from working
        $project_working_budget = Billofquantities::whereIn('project_location_id',$location_id)->get();   
        // Get total price of Bill of Quantities
        $BQ_total_price = $project_working_budget->sum('total_price');
        
        // Calcualte baseline budget from start of project to end 
        $budget_base = setCfyDateRange($project_base_budget,$getFirstDateOfProject,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_base_curve = sortBudgetStartProToCfy($budget_base,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,false,$getLastDateOfProject);
        $budget_base = sortBudgetStartProToCfy($budget_base,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,true,$getLastDateOfProject);
        // Calcualte working budget from start of project to end
        $budget_working = setCfyDateRange($project_working_budget,$getFirstDateOfProject,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays); 
        $budget_working_curve = sortBudgetStartProToCfy($budget_working,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,false,$getLastDateOfProject);

        $budget_working = sortBudgetStartProToCfy($budget_working,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,true,$getLastDateOfProject);
        // Calcualte actual budget from start of project to end
        $budget_actual = getBudgetActual($location_id,$getFirstDateOfProject,$end_fiscal_date);
        $budget_actual_curve = sortBudgetActual($budget_actual,$start_fiscal_date,$end_fiscal_date,false,$getLastDateOfProject);
        $budget_actual = sortBudgetActual($budget_actual,$start_fiscal_date,$end_fiscal_date,true,$getLastDateOfProject); 
        
        // Calcualte baseline budget from current fiscal year start to end
        $budget_base_cfy = setCfyDateRange($project_base_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_base_cfy = sortBudgetDataCfy($budget_base_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        // Get baseline budget
        $total_baseline = round((count($budget_base_cfy) > 0 ? array_sum(array_column($budget_base_cfy,'total_price')) : 0 ),5);
        // Calcualte working budget from current fiscal year start to end
        $budget_working_cfy = setCfyDateRange($project_working_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays); 
        $budget_working_cfy = sortBudgetDataCfy($budget_working_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        // Get total price from working budget of current fiscal year start to end
        $total_working_budget_cfy = (count($budget_working_cfy) > 0 ? array_sum(array_column($budget_working_cfy,'total_price')) : 0 ); 
        // Get working budget
        $total_working = round($total_working_budget_cfy,5);
        // Calcualte actual budget from current fiscal year start to end
        $budget_actual_cfy = getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
        $budget_actual_cfy = sortBudgetActual($budget_actual_cfy,$start_fiscal_date,$end_fiscal_date,false,$getLastDateOfProject);
        // Get Expenditure Budget
        $total_expenditure = (count($budget_actual_cfy) > 0 ? array_sum(array_column($budget_actual_cfy,'total_price')) : 0 );
        // Get weekly base budget 
        $weekly_report_base = getWeeklyReport($location_id,'estimations_bill_quantities_base');
        $weekly_report_base = sortWeeklyReport($weekly_report_base,date('Y-m-01'),1,$WeekDays,$holidays);
        // Get weekly working budget
        $weekly_report_working = getWeeklyReport($location_id,'estimations_bill_quantities');
        $weekly_report_working = sortWeeklyReport($weekly_report_working,date('Y-m-01'),1,$WeekDays,$holidays);
        // Get weekly actual budget 
        $weekly_report_actual  = getWeeklyReportActual($location_id,date('Y-m-01'),1,$total_baseline);
        $weekly_report_actual = sortWeeklyReportActual($weekly_report_actual,date('Y-m-01'),1);
        
        /** Start: SPI Calculation */
        // Get project actual performance in field
        $spi = 0;
        if($total_baseline && $total_baseline!=0){
            $spi = round($total_expenditure/$total_baseline,4); 
        }
        /** End: SPI Calculation */
        
        /** Start: Estimated plan completion date */
        // Get remaining working days of project
        $project_remain_days = 0;
        if($location_id){
            $project_remain_days = portfolioworkDays(array($location_id[0]),date('Y-m-d'),$getLastDateOfProject);
        }            
        $estimated_completion_date = '';
        $working_days = 0;
        if($project_remain_days!=0 && $spi!=0){
            $working_days = ceil($project_remain_days/$spi);
        }
        if($working_days!=0)
        {
            // Get estimated date
            $estimated_completion_date = getDateByWrokginDays($working_days,$WeekDays[$location_id[0]],$holidays[$location_id[0]]);
        }
        /** End: Estimated plan completion date */

        /** Start: Estimated plan budget */
        // Get total price from working budget of current fiscal year start till now
        $planed_budget = setCfyDateRange($project_working_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $planed_budget = sortBudgetDataCfy($planed_budget,$WeekDays,$holidays);
        $planed_budget = (count($planed_budget) > 0 ? array_sum(array_column($planed_budget,'total_price')) : 0 );
        // Get actual budget
        $actual_budget = getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
        $actual_budget = sortBudgetActual($actual_budget,$start_fiscal_date,date('Y-m-d'),false);
        $actual_budget = (count($actual_budget) > 0 ? array_sum(array_column($actual_budget,'total_price')) : 0 );
        $estimated_budget = 0;
        if($planed_budget!=0){
            $est_spi = $actual_budget/$planed_budget;
            $estimated_budget = round($total_working * $est_spi,5);
        }
        /** End: Estimated plan budget */

        // Get baseline budget from start till now
        $total_baseline_SOP = setCfyDateRange($project_base_budget,$getFirstDateOfProject,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $total_baseline_SOP = sortBudgetStartProToCfy($total_baseline_SOP,$getFirstDateOfProject,date('Y-m-d'),$WeekDays,$holidays,true);
        //ddd($total_baseline_SOP);
        $total_baseline_SOP = (count($total_baseline_SOP) > 0 ? array_sum(array_column($total_baseline_SOP,'total_price')) : 0 );
        // Get working budget from start till now
        $total_working_SOP = setCfyDateRange($project_working_budget,$getFirstDateOfProject,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $total_working_SOP = sortBudgetStartProToCfy($total_working_SOP,$getFirstDateOfProject,date('Y-m-d'),$WeekDays,$holidays,true);
        $total_working_SOP = (count($total_working_SOP) > 0 ? array_sum(array_column($total_working_SOP,'total_price')) : 0 );
        // Get project budget expenditure from start till now
        $budget_expenditure_SOP = getBudgetActual($location_id,$getFirstDateOfProject,date('Y-m-d'));
        $budget_expenditure_SOP = sortBudgetActual($budget_expenditure_SOP,$getFirstDateOfProject,date('Y-m-d'),true); 
        $budget_expenditure_SOP = (count($budget_expenditure_SOP) > 0 ? array_sum(array_column($budget_expenditure_SOP,'total_price')) : 0 );
        // Get project invoiced budget from start till now
        $budget_invoiced_SOP = getInvoicedBudget($location_id,$getFirstDateOfProject,date('Y-m-d'));
        // Get project paid budget from start till now
        $budget_paid_SOP = getPaidBudget($location_id,$getFirstDateOfProject,date('Y-m-d'));            
        // Get baseline budget from start of current fiscal year till now
        $total_baseline_CFY = setCfyDateRange($project_base_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $total_baseline_CFY = sortBudgetStartProToCfy($total_baseline_CFY,$start_fiscal_date,date('Y-m-d'),$WeekDays,$holidays,false);
        $total_baseline_CFY = (count($total_baseline_CFY) > 0 ? array_sum(array_column($total_baseline_CFY,'total_price')) : 0 );
        // Get working budget from start of current fiscal year till now
        $total_working_CFY = setCfyDateRange($project_working_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $total_working_CFY = sortBudgetStartProToCfy($total_working_CFY,$start_fiscal_date,date('Y-m-d'),$WeekDays,$holidays,false);
        $total_working_CFY = (count($total_working_CFY) > 0 ? array_sum(array_column($total_working_CFY,'total_price')) : 0 );

        // Get project budget expenditure from start of current fiscal year till now
        $budget_expenditure_CFY = getBudgetActual($location_id,$start_fiscal_date,date('Y-m-d'));
        $budget_expenditure_CFY = sortBudgetActual($budget_expenditure_CFY,$start_fiscal_date,date('Y-m-d'),false); 
        $budget_expenditure_CFY = (count($budget_expenditure_CFY) > 0 ? array_sum(array_column($budget_expenditure_CFY,'total_price')) : 0 );
        // Get project invoiced budget from start of current fiscal year till now
        $budget_invoiced_CFY = getInvoicedBudget($location_id,$start_fiscal_date,date('Y-m-d'));
        // Get project paid budget from start of current fiscal year till now
        $budget_paid_CFY = getPaidBudget($location_id,$start_fiscal_date,date('Y-m-d'));
        // Load view to show data
        return view('pmis/project_dashboard/index',compact('location_id','plan','enc_id','dep_id','project_location','excluded_locations','lang','exc_locations','spi','estimated_completion_date','total_baseline','total_working','total_expenditure','getLastDateOfProject','estimated_budget','portfolios','BQ_total_price','budget_base','budget_working','budget_actual','budget_base_curve','budget_working_curve','budget_actual_curve','budget_base_cfy','budget_working_cfy','budget_actual_cfy','weekly_report_base','weekly_report_working','weekly_report_actual','total_baseline_SOP','total_working_SOP','budget_expenditure_SOP','budget_invoiced_SOP','budget_paid_SOP','total_baseline_CFY','total_working_CFY','budget_expenditure_CFY','budget_invoiced_CFY','budget_paid_CFY','total_working_budget_cfy'));
    }

    /**
     * Function to display the dashboard based on project and its location.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_dashboard(Request $request)
    {
        // Get langauge
        $lang = get_language();
        // Project id
        $project_id = decrypt($request->project_id);
        // Location id
        $location_id = array($request->location_id);
        // Get plan record
        $plan = Plans::find($project_id);
        // Project location
        $portfolios = Portfolios::whereHas('PortfoliosSub', function($query) use ($project_id){
            $query->where('project_id',$project_id);
        })->get();
        // Get Start and End date of current fiscal year
        $start_fiscal_date = Config::get('static.fiscal_year_months.01.start_date');
        $end_fiscal_date   = Config::get('static.fiscal_year_months.12.end_date');
        $WeekDays = weekDaysAll($location_id);
        // Get project location  holidays
        $holidays = holidaysAll($location_id);
        // cfy working days 
        //$cfy_wrkd = working_days($start_fiscal_date,$end_fiscal_date,$WeekDays,$holidays);
        $cfy_wrkd = 0; // Not Used
        // Get first date of planed schedule
        $getFirstDateOfProject = getFirstLastDateOfProject($location_id,'min(start_date)','project_location_id');
        // Get last date of planed schedule
        $getLastDateOfProject = getFirstLastDateOfProject($location_id,'max(end_date)','project_location_id');
         // Get project budget from base
        $project_base_budget = BillOfQuantitiesBase::whereIn('project_location_id',$location_id)->get(); 
        // Get project budget from working
        $project_working_budget = Billofquantities::whereIn('project_location_id',$location_id)->get();   
        // Get total price of Bill of Quantities
        $BQ_total_price = $project_working_budget->sum('total_price');
        // Calcualte baseline budget from start of project to end of current fiscal year 
        $budget_base = setCfyDateRange($project_base_budget,$getFirstDateOfProject,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_base_curve = sortBudgetStartProToCfy($budget_base,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,false,$getLastDateOfProject);
        $budget_base = sortBudgetStartProToCfy($budget_base,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,true,$getLastDateOfProject);
        // Calcualte working budget from start of project to end of current fiscal year
        $budget_working = setCfyDateRange($project_working_budget,$getFirstDateOfProject,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays); 
        $budget_working_curve = sortBudgetStartProToCfy($budget_working,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,false,$getLastDateOfProject);
        $budget_working = sortBudgetStartProToCfy($budget_working,$getFirstDateOfProject,$end_fiscal_date,$WeekDays,$holidays,true,$getLastDateOfProject);
        // Calcualte actual budget from start of project to end of current fiscal year
        $budget_actual = getBudgetActual($location_id,$getFirstDateOfProject,$end_fiscal_date);
        $budget_actual_curve = sortBudgetActual($budget_actual,$start_fiscal_date,$end_fiscal_date,false,$getLastDateOfProject);
        $budget_actual = sortBudgetActual($budget_actual,$start_fiscal_date,$end_fiscal_date,true,$getLastDateOfProject); 
        // Calcualte baseline budget from current fiscal year start to end
        $budget_base_cfy = setCfyDateRange($project_base_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_base_cfy = sortBudgetDataCfy($budget_base_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        $total_baseline = round((count($budget_base_cfy) > 0 ? array_sum(array_column($budget_base_cfy,'total_price')) : 0 ),5);
        // Calcualte working budget from current fiscal year start to end
        $budget_working_cfy = setCfyDateRange($project_working_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays); 
        $budget_working_cfy = sortBudgetDataCfy($budget_working_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        $total_working_budget_cfy = (count($budget_working_cfy) > 0 ? array_sum(array_column($budget_working_cfy,'total_price')) : 0 ); 
        $total_working = round($total_working_budget_cfy,5);
        
        // Calcualte actual budget from current fiscal year start to end
        $budget_actual_cfy = getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
        $budget_actual_cfy = sortBudgetActual($budget_actual_cfy,$start_fiscal_date,$end_fiscal_date,false,$getLastDateOfProject);
        // Get Expenditure Budget
        $total_expenditure = (count($budget_actual_cfy) > 0 ? array_sum(array_column($budget_actual_cfy,'total_price')) : 0 );
        // Get weekly base budget 
        $weekly_report_base = getWeeklyReport($location_id,'estimations_bill_quantities_base');
        $weekly_report_base = sortWeeklyReport($weekly_report_base,date('Y-m-01'),1,$WeekDays,$holidays);
        // Get weekly working budget
        $weekly_report_working = getWeeklyReport($location_id,'estimations_bill_quantities');
        $weekly_report_working = sortWeeklyReport($weekly_report_working,date('Y-m-01'),1,$WeekDays,$holidays);
        // Get weekly actual budget 
        $weekly_report_actual  = getWeeklyReportActual($location_id,date('Y-m-01'),1,'');
        $weekly_report_actual = sortWeeklyReportActual($weekly_report_actual,date('Y-m-01'),1);
        
        /** Start: SPI Calculation */
        // Get project actual performance in field
        $spi = 0;
        if($total_working && $total_working!=0){
            $spi = round($total_expenditure/$total_working,2); 
        }
        /** End: SPI Calculation */

        /** Start: Estimated plan completion date */
        // Get remaining working days of project
        $project_remain_days = 0;
        if($location_id){
            $project_remain_days = portfolioworkDays(array($location_id[0]),date('Y-m-d'),$getLastDateOfProject);
        }
        $estimated_completion_date = '';
        $working_days = 0;
        if($project_remain_days!=0 && $spi!=0){
            $working_days = ceil($project_remain_days/$spi);
        }
        if($working_days!=0)
        {
            // Get estimated date
            $estimated_completion_date = getDateByWrokginDays($working_days,$WeekDays[$location_id[0]],$holidays[$location_id[0]]);
        }
        /** End: Estimated plan completion date */

        /** Start: Estimated plan budget */
        // Get total price from working budget of current fiscal year start till now
        $planed_budget = setCfyDateRange($project_working_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $planed_budget = sortBudgetDataCfy($planed_budget,$WeekDays,$holidays);
        $planed_budget = (count($planed_budget) > 0 ? array_sum(array_column($planed_budget,'total_price')) : 0 );
        // Get actual budget
        $actual_budget = getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
        $actual_budget = sortBudgetActual($actual_budget,$start_fiscal_date,date('Y-m-d'),false);
        $actual_budget = (count($actual_budget) > 0 ? array_sum(array_column($actual_budget,'total_price')) : 0 );
        $estimated_budget = 0;
        if($planed_budget!=0){
            $est_spi = $actual_budget/$planed_budget;
            $estimated_budget = round($total_working * $est_spi,5);
        }
        /** End: Estimated plan budget */

        // Get baseline budget from start till now
        $total_baseline_SOP = setCfyDateRange($project_base_budget,$getFirstDateOfProject,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $total_baseline_SOP = sortBudgetStartProToCfy($total_baseline_SOP,$getFirstDateOfProject,date('Y-m-d'),$WeekDays,$holidays,true);
        $total_baseline_SOP = (count($total_baseline_SOP) > 0 ? array_sum(array_column($total_baseline_SOP,'total_price')) : 0 );
        // Get working budget from start till now
        $total_working_SOP = setCfyDateRange($project_working_budget,$getFirstDateOfProject,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $total_working_SOP = sortBudgetStartProToCfy($total_working_SOP,$getFirstDateOfProject,date('Y-m-d'),$WeekDays,$holidays,true);
        $total_working_SOP = (count($total_working_SOP) > 0 ? array_sum(array_column($total_working_SOP,'total_price')) : 0 );
        // Get project budget expenditure from start till now
        $budget_expenditure_SOP = getBudgetActual($location_id,$getFirstDateOfProject,date('Y-m-d'));
        $budget_expenditure_SOP = sortBudgetActual($budget_expenditure_SOP,$getFirstDateOfProject,date('Y-m-d'),true); 
        $budget_expenditure_SOP = (count($budget_expenditure_SOP) > 0 ? array_sum(array_column($budget_expenditure_SOP,'total_price')) : 0 );
        // Get project invoiced budget from start till now
        $budget_invoiced_SOP = getInvoicedBudget($location_id,$getFirstDateOfProject,date('Y-m-d'));
        // Get project paid budget from start till now
        $budget_paid_SOP = getPaidBudget($location_id,$getFirstDateOfProject,date('Y-m-d'));            
        // Get baseline budget from start of current fiscal year till now
        $total_baseline_CFY = setCfyDateRange($project_base_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $total_baseline_CFY = sortBudgetStartProToCfy($total_baseline_CFY,$start_fiscal_date,date('Y-m-d'),$WeekDays,$holidays,false);
        $total_baseline_CFY = (count($total_baseline_CFY) > 0 ? array_sum(array_column($total_baseline_CFY,'total_price')) : 0 );
        // Get working budget from start of current fiscal year till now
        $total_working_CFY = setCfyDateRange($project_working_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays); 
        $total_working_CFY = sortBudgetStartProToCfy($total_working_CFY,$start_fiscal_date,date('Y-m-d'),$WeekDays,$holidays,false);
        $total_working_CFY = (count($total_working_CFY) > 0 ? array_sum(array_column($total_working_CFY,'total_price')) : 0 );

        // Get project budget expenditure from start of current fiscal year till now
        $budget_expenditure_CFY = getBudgetActual($location_id,$start_fiscal_date,date('Y-m-d'));
        $budget_expenditure_CFY = sortBudgetActual($budget_expenditure_CFY,$start_fiscal_date,date('Y-m-d'),false); 
        $budget_expenditure_CFY = (count($budget_expenditure_CFY) > 0 ? array_sum(array_column($budget_expenditure_CFY,'total_price')) : 0 );
        // Get project invoiced budget from start of current fiscal year till now
        $budget_invoiced_CFY = getInvoicedBudget($location_id,$start_fiscal_date,date('Y-m-d'));
        // Get project paid budget from start of current fiscal year till now
        $budget_paid_CFY = getPaidBudget($location_id,$start_fiscal_date,date('Y-m-d'));
        // Load view to show data
        return view('pmis/project_dashboard/show',compact('location_id','plan','lang','spi','estimated_completion_date','total_baseline','total_working','total_expenditure','getLastDateOfProject','estimated_budget','portfolios','BQ_total_price','budget_base','budget_working','budget_actual','budget_base_curve','budget_working_curve','budget_actual_curve','budget_base_cfy','budget_working_cfy','budget_actual_cfy','weekly_report_base','weekly_report_working','weekly_report_actual','total_baseline_SOP','total_working_SOP','budget_expenditure_SOP','budget_invoiced_SOP','budget_paid_SOP','total_baseline_CFY','total_working_CFY','budget_expenditure_CFY','budget_invoiced_CFY','budget_paid_CFY','total_working_budget_cfy'));
    }

    /**
     * Function to display the list of records based on filter.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter_project_progress(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Get department
        $dep_id = decrypt($request->department_id);
        // Get section code
        $section = $request->section;
        //Get department code with dep_id
        $dep_code = getColumn('pmis_auth','departments',['id' => $dep_id],'code');
        // Search Shared Plan
        $records  = Plans::searchProjectDashboard($request->condition,$request->field,$request->value,$dep_id,$section,$dep_code);
        // Load View to display records
        return view('pmis/project_dashboard/list_filter',compact('records','lang','request','dep_id'));
    }

    /**
     * Function to display weekly report based on selected date and month.
     *
     * @return \Illuminate\Http\Response
     */
    public function weekly_report(Request $request)
    {
        $lang = get_language();
        // Locations Id
        $location_id = $request->location_id;
        $loc_id = array();
        if($location_id!="")
        {   
            $loc_id = explode(',',$location_id);
        }
        // Get project location week days
        $WeekDays = weekDaysAll($loc_id);
        // Get project location  holidays
        $holidays = holidaysAll($loc_id);
        
        // Baseline Budget
        $total_baseline = $request->total_baseline;
        // Date
        $date = dateProvider($request->date,$lang);
        // Months
        $months = $request->months;
        // Get weekly base budget 
        $weekly_report_base = getWeeklyReport($loc_id,'estimations_bill_quantities_base');
        $weekly_report_base = sortWeeklyReport($weekly_report_base,$date,$months,$WeekDays,$holidays);
        // Get weekly working budget
        $weekly_report_working = getWeeklyReport($loc_id,'estimations_bill_quantities');
        $weekly_report_working = sortWeeklyReport($weekly_report_working,$date,$months,$WeekDays,$holidays);
        // Get weekly actual budget 
        $weekly_report_actual  = getWeeklyReportActual($loc_id,$date,$months,$total_baseline);
        $weekly_report_actual = sortWeeklyReportActual($weekly_report_actual,$date,$months);

        
        // Load view to show data
        return view('pmis/project_dashboard/weekly_content',compact('lang','weekly_report_base','weekly_report_working','weekly_report_actual'));
    }
}
