<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Notifications\NewProject;
use App\models\Plans;
use App\models\Daily_report_bq;
use Redirect;
use DB;

class ProgressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($dep_id)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_progress'))
        {
        return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        $data['lang'] = $lang;
        // Get records
        $data['records'] = Plans::whereHas('project_location', function($query){
            $query->where('status','0');
        })->whereHas('share', function($query) use ($depID){
            $query->where('share_to',$depID);
            $query->where('share_to_code','pmis_progress');
        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
        //Get URL Segments
        $data['segments'] = request()->segments();
        // Load view to show result
        if(Input::get('ajax') == 1)
        {
            return view('pmis/progress/list_ajax',$data);
        }
        else
        {
            return view('pmis/progress/list',$data);
        }
    }

    public function filter_progress(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Get department
        $dep_id = decrypt($request->department_id);
        // Get section code
        $section = $request->section;
        // Search Shared Plan
        $records  = Plans::searchProgressProjects($request->condition,$request->field,$request->value,$dep_id,$section,'0');
        // Load View to display records
        return view('pmis/progress/list_filter',compact('records','lang','request','dep_id'));
    }

    /**
     * Bill of quantities chart.
     *
     * @return \Illuminate\Http\Response
     */
    public function BQChart($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//project id
        $data['enc_id'] = $enc_id;
        $userid = userid();
        //Get project
        $records = Plans::find($id);
        $data['records'] = $records;
        // Get project summary
        $data['summary'] = $records;
        //Get all performed BQ
        $data['reportBQ'] = Daily_report_bq::select(DB::raw('sum(amount) as amounts, operation_type'))->where('project_id',$id)->where('type',0)->groupBy('operation_type')->get();
        //Default value for remained BQ
        $data['remained'] = array();
        // Load view to show result
        return view('pmis/progress/BQChart/chart',$data);
    }

    /**
     * Change project priority.
     *
     * @return \Illuminate\Http\Response
     */
    public function priority(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'priority' => 'required',
        ]);
        $record_id = $request->record_id;
        // Change project priority
        $record = Plans::find($record_id);
        $record->priority = $request->priority;
        $record = $record->save();
        if($record)
        {
            Session()->flash('success',  __("global.success_process_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Store project priority form.
     *
     * @return \Illuminate\Http\Response
     */
    public function priority_add($id)
    {
        // Get project id
        $record_id = decrypt($id);
        // Get project
        $record = Plans::find($record_id);
        // Load view to show result
        return view('pmis/progress/priority_add', compact('record_id', 'record'));
    }

}
?>
