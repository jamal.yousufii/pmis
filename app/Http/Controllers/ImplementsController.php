<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Notifications\NewProject;
use App\models\Requests;
use App\models\Plans;
use App\models\Implement;
use App\models\Implement_attachments;
use App\models\Static_data;
use App\models\Authentication\SectionSharings;
use App\models\Authentication\Users;
use App\models\ProjectUsers;
use App\User;
use Validator;
use Redirect;

class ImplementsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department id is not exist
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_implement'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_implement"]);
    // Get Language
    $data['lang'] = get_language();
    // Get all data
    $data['records']  = Plans::whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_implement');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_implement','imp_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view assigned records  
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_implement');
                            });
                          }
                        })->whereHas('project_location', function($query){
                          $query->where('status','0');
                        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_implement');
    })->where('department_id',$depID)->get();
      //Get URL Segments
    $data['segments'] = request()->segments();
    // load view to show result
    if(Input::get('ajax') == 1)
    {
      return view('pmis/implements/list_ajax',$data);
    }
    else
    {
      return view('pmis/implements/list',$data);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    $data['lang'] = get_language();
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get plan record
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_implement');
    }])->first();
    if($data['plan'])
    {
      // Get summary data
      $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Record
    $data['record'] = Implement::where('project_id',$id)->first();
    // Get Attachments
    $data['attachments'] = Implement_attachments::where('parent_id',$id)->get();
    // Get Allowed Sections for Sharing
    $data['sections'] = SectionSharings::where('code','pmis_implement')->get();
    // Load view to show result
    return view('pmis/implements/view',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    // Get Language
    $data['lang'] = get_language();
    // Plan ID
    $enc_plan = $request->plan_id;
    $dec_plan = decrypt($enc_plan);
    $data['enc_plan'] = $enc_plan;
    // Get Plan Summary
    $data['plan'] = Plans::whereId($dec_plan)->first();
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Attachment type from static data table
    $data['attachment_type'] = Static_data::where('type','6')->get();
    // Load view
    return view('pmis/implements/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'attachment_type' => 'required',
      'attachment_name' => 'required',
    ]);
    // Get Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('implements','urn');
    // Get Plan ID
    $plan_id = decrypt(Input::get('enc_plan'));
    // New Survey
    $record = new Implement;
    $record->urn            = $urn;
    $record->project_id     = $plan_id;
    $record->description    = $request->description;
    $record->created_at     = date('Y-m-d H:i:s');
    $record->created_by     = userid();
    $record->department     = departmentid();
    $record->process        = '0';
    if($record->save())
    {
      // // Upload Attachments
      if($request->hasFile('file'))
      {
        $files = $request->file('file');
        $i=0;
        foreach ($files as $file)
        {
          $custome_data = [
            'type' => $request->attachment_type[$i],
          ];
          $file_name  = $request->attachment_name[$i];
          // Call Upload Function to Upload all Posted Files
          $fileUpload = uploadAttachments($plan_id,$record->id,'implement_attachments',$file,$file_name,$i,'implement_attachments',$custome_data);
          if(!$fileUpload)
          {
            Session()->flash('att_failed', __("global.attach_failed"));
          }
          $i++;
        }
      }
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request,$enc_id)
  {
    // Get Language
    $lang = get_language();
    $data['lang'] = $lang;
    // Record ID
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Plan ID
    $enc_plan = $request->plan_id;
    $dec_plan = decrypt($enc_plan);
    $data['enc_plan'] = $enc_plan;
    // Get Plan Summary
    $data['plan'] = Plans::whereId($dec_plan)->first();
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Record
    $data['record'] = Implement::whereId($id)->first();
    // Get Attachments
    $data['attachments'] = Implement_attachments::where('record_id',$id)->get();
    // Get Attachment type from static data table
    $data['attachment_type'] = Static_data::where('type','6')->get();
    // Load view to show result
    return view('pmis/implements/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    // Dycript Plan ID
    $plan_id = decrypt($request->plan_id);
    // Dycript Record ID
    $id = decrypt($request->enc_id);
    // New Survey
    $record = Implement::find($id);
    $record->description = $request->description;
    $record->updated_at  = date('Y-m-d H:i:s');
    $record->updated_by  = userid();
    $updated = $record->save();
    if($updated)
    {
      // Upload New Attachments
      if($request->hasFile('file'))
      {
        $files = $request->file('file');
        $i=0;
        foreach ($files as $file)
        {
          $custome_data = array(
            'type' => $request->attachment_type[$i],
          );
          $file_name  = $request->attachment_name[$i];
          // Call Upload Function to Upload all Posted Files
          $fileUpload = uploadAttachments($plan_id,$record->id,'implement_attachments',$file,$file_name,$i,'implement_attachments',$custome_data);
          if(!$fileUpload)
          {
            Session()->flash('att_failed', __("global.attach_failed"));
          }
          $i++;
        }
      }
      // Update Old Records
      $file_id = $request->file_id;
      $completedID = 0;
      foreach ($file_id as $items)
      {
        if($request->hasFile('file_'.$items) and $completedID != $items)
        {
          $files = $request->file('file_'.$items);
          $i=0;
          foreach ($files as $file)
          {
            $custome_data = array(
              'type'     => $request->{'attachment_type_'.$items},
              'filename' => $request->{'attachment_name_'.$items},
            );
            $file_name  = $request->{'attachment_name_'.$items};
            // Call Upload Function to Upload all Posted Files
            $fileUpload = uploadUpdatedAttachments($plan_id,$id,'implement_attachments',$file,$file_name,$i,'implement_attachments',$custome_data,$items);
            if(!$fileUpload)
            {
              Session()->flash('att_failed', __("global.attach_failed"));
            }
            $i++;
          }
        }
        else
        {
          // Update Record
          $data = [
            'type'        => $request->{'attachment_type_'.$items},
            'filename'    => $request->{'attachment_name_'.$items},
            'updated_at'  => date('Y-m-d H:i:s'),
            'updated_by'  => userid(),
          ];
          // Do Operation
          Implement_attachments::whereId($items)->update($data);
        }
        $completedID = $items;
      }
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Store a newly attachments in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  function uploadDocuments($rec_id=0,$file_name="",$i=1)
  {
    // Getting all of the post data
    $file = Input::file('file_'.$i);
    $errors = "";
    if(Input::hasFile('file_'.$i))
    {
      // Validating each file.
      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
      $validator = Validator::make(
        [
            'file'      => $file,
            'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
            'file'      => 'required|max:500000',
            'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
        ]
      );
      if($validator->passes())
      {
        // Path is pmis/public/attachments/pmis...
        $destinationPath = 'attachments/pmis/estimations';
        $fileOrgName     = $file->getClientOriginalName();
        $fileExtention   = $file->getClientOriginalExtension();
        $fileOrgSize     = $file->getSize();
        if($file_name!=""){
          $name = $file_name;
          $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
        }else{
          $name = explode('.', $fileOrgName)[0];
          $file_name = $fileOrgName;
        }
        $upload_success = $file->move($destinationPath, $file_name);
        if($upload_success)
        {
          //Insert Record
          $attachment = new Estimation_attachments;
          $attachment->rec_id     = $rec_id;
          $attachment->filename   = $name;
          $attachment->path       = $destinationPath."/".$file_name;
          $attachment->extension  = $fileExtention;
          $attachment->size       = $fileOrgSize;
          $attachment->created_at = date('Y-m-d H:i:s');
          $attachment->created_by = userid();
          $attachment->save();
          return true;
        }
        else
        {
          $errors .= json('error', 400);
        }
      }
      else
      {
        // Return false.
        return false;
      }
    }
  }

  /**
   * Display entities.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function more_attachment()
  {
    // Get Language
    $data['lang'] = get_language();
    // Get Counter Number
    $data['number'] = Input::post('number');
    // Get Static Data
    $data['attachments'] = Static_data::where('type','6')->get();
    // Load view
    return view('pmis/implements/more_attachment',$data);
  }

  /**
   * Approve/Reject Request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function approve_implement(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'status' => 'required',
      'date'   => 'required',
    ]);
    // Get encrypted id
    $id = decrypt($request->id);
    // Get Request
    $data = Implement::find($id);
    // To be approved
    // Update table
    $data->operation             = $request->status;
    $data->operation_date        = dateProvider($request->date,$lang);
    $data->operation_number      = $request->number;
    $data->operation_description = $request->description;
    $data->operation_by          = userid();
    $data->operation_at          = date('Y-m-d H:i:s');
    $data->save();

     //Get Projects Name with project Id
     $project_name = Plans::whereId($data->project_id)->first()->name;
    if($request->status==1)
    {
      // Start Notification
      $users = User::whereId($data->created_by)->get();
      $redirect_url = route('implements.show', encrypt($data ->project_id));
      foreach($users as $user) {
        $user->notify(new NewProject($id,trans('global.imnplements_approved',['pro' => $project_name]), userid(), $redirect_url));
      }
      // End Notification
      // Set msg
      Session()->flash('success', __("global.status_success_msg"));
    }
    else // To be Rejected
    {
      // Start Notification
      $users = User::whereId($data->created_by)->get();
      $redirect_url = route('implements.show', encrypt($data ->project_id));
      foreach($users as $user) {
        $user->notify(new NewProject($id,trans('global.imnplements_rejected',['pro' => $project_name]), userid(), $redirect_url));
      }
      // End Notification
      // Set msg
      Session()->flash('fail', __("global.status_success_msg"));
    }
  }

  /**
   * Display the notification resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function notification($enc_id,$dep_id)
  {
    $data['lang'] = get_language();
    //Set current department
    session(['current_department' => $dep_id]);
    //Set current section
    session(['current_section' => "pmis_implement"]);

    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Plan Summary
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('sections','pmis_implement');
    }])->first();
    // Get Record
    $data['record'] = Implement::where('project_id',$id)->first();
    // Get Attachments
    $data['attachments'] = Implement_attachments::where('parent_id',$id)->get();
    // Get Allowed Sections for Sharing
    $data['sections'] = SectionSharings::where('code','pmis_implement')->get();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/implements/view_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/implements/view',$data);
    }
  }

  /**
   * Display a listing of the resource by keyword.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter_implement(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    // Search Shared Plan
    $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$department_id,$section,$request->role);
    // Load View to display records
    return view('pmis/implements/list_filter',compact('records','lang','request'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      // Get project name
      $project_name = Plans::find($request->record_id)->name;
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('implement', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.project_assigned',['pro'=>$project_name]), userid(), $redirect_url));
              }     
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_implement')->get();
    // Load view to show result
    return view('pmis/implements/list_assigned_users',compact('records'));
  }

}
?>
