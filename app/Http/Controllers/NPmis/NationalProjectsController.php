<?php
namespace App\Http\Controllers\NPmis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\models\NPmis\NationalProject;
use App\models\NPmis\NationalProjectLocation;
use App\models\NPmis\NationalCategories;
use App\models\NPmis\NationalProjectTypes;
use App\models\Home;

class NationalProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Set current section in session
        session(['current_section' => 'npmis_projects']);
        // Get language
        $lang = get_language();
        // Get user department
        $depID = departmentid();
        // Get records
        $records =  NationalProject::with(['category','project_type'])
                    ->where(function($query) use ($depID){
                        if(!doIHaveRoleInDep(encrypt($depID),'npmis_projects','npro_all_view'))//If user can't view all records
                        {
                            $query->where('department_id',departmentid());
                        }
                    })
                    ->orderBy('id','desc')
                    ->paginate(10)
                    ->onEachSide(3);
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('npmis.national_projects.list_ajax',compact('records','lang'));
        }
        else
        {
            // Load view to show result
            return view('npmis.national_projects.list',compact('records','lang'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = get_language();
        // Get needed statics
        $categories = NationalCategories::all();
        // Load view to show result
        return view('npmis.national_projects.general.add',compact('categories','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'name'              => 'required',
            'category_id'       => 'required',
            'project_type_id'   => 'required',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'contractor'        => 'required',
            'contract_price'    => 'required',
            'contract_code'     => 'required',
        ]);
        // Language
        $lang = get_language();
        // Generate URN
        $urn = GenerateURN('national_projects','urn');
        // New project
        $record = new NationalProject;
        $record->urn                = $urn;
        $record->name               = $request->name;
        $record->category_id        = $request->category_id;
        $record->project_type_id    = $request->project_type_id;
        $record->start_date         = dateProvider($request->start_date,$lang);
        $record->end_date           = dateProvider($request->end_date,$lang);
        $record->donor              = $request->donor;
        $record->contractor         = $request->contractor;
        $record->contract_price     = $request->contract_price;
        $record->contract_code      = $request->contract_code;
        $record->responsible_name   = $request->responsible_name;
        $record->responsible_number = $request->responsible_number;
        $record->responsible_email  = $request->responsible_email;
        $record->goal               = $request->goals;
        $record->description        = $request->description;
        $record->created_at         = date('Y-m-d H:i:s');
        $record->created_by         = userid();
        $record->department_id      = departmentid();
        $record = $record->save();
        if($record)
        {
            Session()->flash('success',  __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Set current tab in session
        session(['current_tab' => 'npmis_info']);
        // Set current project id in session
        session(['national_project_id' => $id]);
        // Get tabs
        $tabs = getTabs('npmis_projects',$lang);
        // Get record
        $record = NationalProject::find($dec_id);
        // Get provinces
        $provinces = getAllProvinces();
        // Get locations
        $locations = NationalProjectLocation::where('national_project_id',$dec_id)->get();
        // Load view to show result
        return view('npmis.national_projects.general.view',compact('tabs','record','lang','id','provinces','locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Get record
        $record = NationalProject::find($dec_id);
        // Get needed statics
        $categories = NationalCategories::all();
        // Get project types by category
        //$project_types = Home::getAllProjectTypesByCategory($record->category_id);
        $project_types = NationalProjectTypes::where('national_categorie_id',$record->category_id)->get();
        // Load view to show result
        return view('npmis.national_projects.general.edit',compact('record','project_types','categories','id','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the request...
        $validates = $request->validate([
            'name'              => 'required',
            'category_id'       => 'required',
            'project_type_id'   => 'required',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'contractor'        => 'required',
            'contract_price'    => 'required',
            'contract_code'     => 'required',
        ]);
        // Language
        $lang = get_language();
        // Decrypt the id
        $dec_id = decrypt($id);
        // Update record
        $record = NationalProject::find($dec_id);
        $record->name               = $request->name;
        $record->category_id        = $request->category_id;
        $record->project_type_id    = $request->project_type_id;
        $record->start_date         = dateProvider($request->start_date,$lang);
        $record->end_date           = dateProvider($request->end_date,$lang);
        $record->donor              = $request->donor;
        $record->contractor         = $request->contractor;
        $record->contract_price     = $request->contract_price;
        $record->contract_code      = $request->contract_code;
        $record->responsible_name   = $request->responsible_name;
        $record->responsible_number = $request->responsible_number;
        $record->responsible_email  = $request->responsible_email;
        $record->goal               = $request->goals;
        $record->description        = $request->description;
        $record->updated_at         = date('Y-m-d H:i:s');
        $record->updated_by         = userid();
        $record = $record->save();
        if($record)
        {
            Session()->flash('success',  __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProjectType(Request $request)
    {
        // Get language
        $lang = get_language();
        $id = $request->id;
        // Get project type by project category
        $types = NationalProjectTypes::where('national_categorie_id',$id)->get();
        // load view to display data
        return view('npmis.national_projects.general.project_type',compact('types','lang'));
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createLocation()
    {
        // Get language
        $lang = get_language();
        // Get provinces
        $provinces = getAllProvinces(); 
        // Load view to show result
        return view('npmis.national_projects.general.location_add',compact('provinces','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLocation(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'province' => 'required',
            'district' => 'required',
            'village' => 'required'
        ]);
        // Decrypt Project ID
        $id = decrypt($request->id);
        // Store Project Details
        $record = new NationalProjectLocation;
        $record->national_project_id  = $id;
        $record->province_id    = $request->province;
        $record->district_id    = $request->district;
        $record->village_id     = $request->village;
        $record->created_at     = date('Y-m-d H:i:s');
        $record->created_by     = userid();
        $project = $record->save();
        if($project)
        {
            Session()->flash('success',  __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editLocation($id)
    {
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Get locations
        $record = NationalProjectLocation::find($dec_id);
        // Get provinces
        $provinces = getAllProvinces(); 
        // Get Districts
        $districts = getAllDistrictsByProvince($record->province_id); 
        // Get Villages
        $villages = getAllVillagesByDistrict($record->district_id); 
        // Load view to show result
        return view('npmis.national_projects.general.location_edit',compact('record','provinces','districts','villages','id','lang'));
    }

    /**
     * Store a newly update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateLocation(Request $request,$id)
    {
        // Validate the request...
        $validates = $request->validate([
            'province' => 'required',
            'district' => 'required',
            'village' => 'required'
        ]);
        // Decrypt Project ID
        $dec_id = decrypt($id);
        // Store Project Details
        $record = NationalProjectLocation::find($dec_id);
        $record->province_id    = $request->province;
        $record->district_id    = $request->district;
        $record->village_id     = $request->village;
        $record->updated_at     = date('Y-m-d H:i:s');
        $record->updated_by     = userid();
        $project = $record->save();
        if($project)
        {
            Session()->flash('success',  __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Filter list the resource .
     *
     * @return \Illuminate\Http\Response
     */  
    public function filterNationalProjects(Request $request)
    {
        $lang = get_language();
        $records = NationalProject::searchData($request->condition,$request->field,$request->value); 
        // Load view to show result
        return view('npmis.national_projects.list_filter',['records' => $records,'lang' => $lang,'request' => $request]);
    }
}
