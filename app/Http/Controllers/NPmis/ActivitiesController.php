<?php
namespace App\Http\Controllers\NPmis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\models\NPmis\NationalProject;
use App\models\NPmis\NationalActivities;
use App\models\NPmis\NationalProgress;
use App\models\NPmis\Activities;
use App\models\Settings\Statics\Categories;
use App\models\Home;

class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get Language
        $lang = get_language();
        // Get needed statics
        $activities = Activities::all();
        // Load view to show result
        return view('npmis.national_projects.activities.add',compact('activities','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'activitie_id'      => 'required',
            'activity_desc'     => 'required',
            'progress'          => 'required',
            'status'            => 'required',
        ]);
        // Language
        $lang = get_language();
        $record = array();
        $i = 0;
        if($request->activity_desc)
        {
            for($i=0; $i<count($request->activity_desc); $i++)
            {
                $data = array(
                    'national_project_id'    => decrypt($request->national_project_id),
                    'activitie_id'           => $request->activitie_id,
                    'activity_desc'          => $request->activity_desc[$i],
                    'status'                 => $request->status[$i],
                    'start_date'             => dateProvider($request->start_date[$i],$lang),
                    'end_date'               => dateProvider($request->end_date[$i],$lang),
                    'progress'               => $request->progress[$i],
                    'created_at'             => date('Y-m-d H:i:s'),
                    'created_by'             => userid(),
                    'department_id'          => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                // Insert record to database
                NationalActivities::insert($record);
            }
            Session()->flash('success',  __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Set current project id in session
        session(['national_project_id' => $id]);
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Set current tab in session
        session(['current_tab' => 'npmis_activities']);
        // Get tabs
        $tabs = getTabs('npmis_projects',$lang);
        // Get project record
        $project = NationalProject::find($dec_id);
        // Get progress record planned
        $progress_planned = NationalProgress::where('national_project_id',$dec_id)->sum('progress_planned');
        // Get progress record actual
        $progress_done = NationalProgress::where('national_project_id',$dec_id)->sum('progress_done');
        // Get record
        $records =  Activities::with(['nationalActivities' => function($query) use ($dec_id){
                        $query->where('national_project_id',$dec_id);
                    }])->get();
        // Load view to show result
        return view('npmis.national_projects.activities.view',compact('project','progress_planned','progress_done','records','lang','id','tabs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Get needed statics
        $activities = Activities::all();
        // Get record
        $record = NationalActivities::find($dec_id);
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get activity progress
        $progress = NationalActivities::where('national_project_id',$pro_id)->where('id','<>',$dec_id)->where('activitie_id',$record->activitie_id)->get()->sum('progress');
        // Load view to show result
        return view('npmis.national_projects.activities.edit',compact('record','activities','progress','id','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the request...
        $validates = $request->validate([
            'activitie_id'      => 'required',
            'activity_desc'     => 'required',
            'progress'          => 'required',
            'status'            => 'required',
        ]);
        // Language
        $lang = get_language();
        // Decrypt the id
        $dec_id = decrypt($id);
        // Update record
        $record = NationalActivities::find($dec_id);
        $record->activitie_id       = $request->activitie_id;
        $record->activity_desc      = $request->activity_desc;
        $record->status             = $request->status;
        $record->start_date         = dateProvider($request->start_date,$lang);
        $record->end_date           = dateProvider($request->end_date,$lang);
        $record->progress           = $request->progress;
        $record->updated_at         = date('Y-m-d H:i:s');
        $record->updated_by         = userid();
        $record = $record->save();
        if($record)
        {
            Session()->flash('success',  __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display more activities.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function moreActivities()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Load view
        return view('npmis.national_projects.activities.add_more',compact('number','lang'));
    }

    /**
     * Display more activities.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProgress(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Get category
        $category = $request->category;
        // Get project id
        $pro_id = decrypt($request->pro_id);
        // Get activity progress
        $progress = NationalActivities::where('national_project_id',$pro_id)->where('activitie_id',$category)->get()->sum('progress');
        // Load view
        return view('npmis.national_projects.activities.progress',compact('progress','lang'));
    }
}
