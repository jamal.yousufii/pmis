<?php
namespace App\Http\Controllers\NPmis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\models\NPmis\NationalProject;
use App\models\NPmis\NationalProgress;
use App\models\NPmis\NationalProblems;
use App\models\NPmis\NationalLabor;
use App\models\NPmis\NationalEquipment;
use App\models\NPmis\NationalPayments;
use App\models\NPmis\Problems;
use App\models\NPmis\Workers;
use App\models\NPmis\Equipments;
use App\models\NPmis\Payments;

class ConstructionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Set current project id in session
        session(['national_project_id' => $id]);
        // Decrypt the id
        $dec_id = decrypt($id);
        // Get language
        $lang = get_language();
        // Set current tab in session
        session(['current_tab' => 'npmis_constructions']);
        // Get tabs
        $tabs = getTabs('npmis_projects',$lang);
        // Get project record
        $project = NationalProject::find($dec_id);
        // Get record
        $data = NationalProgress::where('national_project_id',$dec_id);
        $records = $data->orderBy('id','desc')->get();
        $progress_planned = $data->sum('progress_planned');
        $progress_done    = $data->sum('progress_done');
        // Destroy sessions
        session()->forget('report_id');
        // Load view to show result
        return view('npmis.national_projects.constructions.list',compact('project','records','progress_planned','progress_done','tabs','lang','id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        // Decrypt the id
        $dec_id = decrypt($id);
        // Set current report id in session
        session(['report_id' => $dec_id]);
        // Get language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_progress']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Get record
        $record = NationalProgress::find($dec_id);
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get project record
        $project = NationalProject::find($pro_id);
        // Get progress record
        $progress = NationalProgress::where('national_project_id',$pro_id)->where('id','<>',$dec_id)->get();
        $progress_planned = $progress->sum('progress_planned');
        $progress_done    = $progress->sum('progress_done');
        // Load view to show result
        return view('npmis.national_projects.constructions.view',compact('record','progress_planned','progress_done','id','lang','tabs','project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_progress()
    {
        // Set current report id in session
        $report_id = session('report_id');
        // Get progress report
        $record = NationalProgress::whereId($report_id)->first();
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get progress record
        $progress = NationalProgress::where('national_project_id',$pro_id)->where('id','<>',$report_id)->get();
        $progress_planned = $progress->sum('progress_planned');
        $progress_done    = $progress->sum('progress_done');
        // Get project record
        $project = NationalProject::find($pro_id);
        // Get Language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_progress']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Load view to show result
        return view('npmis.national_projects.constructions.add_progress',compact('record','progress_planned','project','progress_done','lang','tabs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_progress(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'report_date'           => 'required',
            'work_progress_planned' => 'required',
            'work_progress_done'    => 'required',
        ]);
        // Language
        $lang = get_language();
        // Generate URN
        $urn = GenerateURN('national_progress','urn');
        // New project
        $record = new NationalProgress;
        $record->urn                    = $urn;
        $record->national_project_id    = decrypt($request->national_project_id);
        $record->report_date            = dateProvider($request->report_date,$lang);
        $record->progress_planned       = $request->work_progress_planned;
        $record->progress_done          = $request->work_progress_done;
        $record->description            = $request->description;
        $record->created_at             = date('Y-m-d H:i:s');
        $record->created_by             = userid();
        $record->department_id          = departmentid();
        $data = $record->save();
        if($data)
        {   
            Session()->flash('success',  __("global.success_msg"));
            // Record id
            $id = $record->id;
            // Set current report id in session
            session(['report_id' => $id]);
            // Get progress report
            $record = NationalProgress::whereId($id)->first();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get progress record
            $progress = NationalProgress::where('national_project_id',$pro_id)->where('id','<>',$id);
            $progress_planned = $progress->sum('progress_planned');
            $progress_done    = $progress->sum('progress_done');
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_progress',compact('record','progress_planned','progress_done','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_problems()
    {
        // Set current report id in session
        $report_id = session('report_id');
        // Get progress report
        $progress = NationalProgress::whereId($report_id)->first();
        // Get progress report
        $record = NationalProblems::where('national_progress_id',$report_id)->get();
        // Get Language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_problems']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Get all statics
        $problems = Problems::all();
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get project record
        $project = NationalProject::find($pro_id);
        // Load view to show result
        return view('npmis.national_projects.constructions.add_problems',compact('record','progress','problems','lang','tabs','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_problems(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'problem_id'    => 'required',
            'status'        => 'required',
        ]);
         
        $record = array();
        $i = 0;
        if($request->problem_id)
        {
            for($i=0; $i<count($request->problem_id); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'problem_id'                  => $request->problem_id[$i],
                    'status'           => $request->status[$i],
                    'description'          => $request->description[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                // Insert record to database 
                NationalProblems::insert($record);
            }
            Session()->flash('success',  __("global.success_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $problems = Problems::all();
            // Get problem report
            $record = NationalProblems::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_problems',compact('record','problems','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_labor()
    {
        // Set current report id in session
        $report_id = session('report_id');
        // Get progress report
        $progress = NationalProgress::whereId($report_id)->first();
        // Get progress report
        $record = NationalLabor::where('national_progress_id',$report_id)->get();
        // Get Language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_labor']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Get all statics
        $labor = Workers::all();
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get project record
        $project = NationalProject::find($pro_id);
        // Load view to show result
        return view('npmis.national_projects.constructions.add_labor',compact('progress','record','labor','lang','tabs','project'));
    }

    public function store_labor(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'type'          => 'required',
            'labor_exist'   => 'required',
            'labor_needed'  => 'required',
        ]);
        $record = array();
        $i = 0;
        if($request->type)
        {
            for($i=0; $i<count($request->type); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'type'                  => $request->type[$i],
                    'labor_exist'           => $request->labor_exist[$i],
                    'labor_needed'          => $request->labor_needed[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                // Insert record to database 
                NationalLabor::insert($record);
            }
            Session()->flash('success',  __("global.success_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $labor = Workers::all();
            // Get progress report
            $record = NationalLabor::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_labor',compact('record','labor','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_equipment()
    {
        // Set current report id in session
        $report_id = session('report_id');
        // Get progress report
        $progress = NationalProgress::whereId($report_id)->first();
        // Get progress report
        $record = NationalEquipment::where('national_progress_id',$report_id)->get();
        // Get Language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_equipment']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Get all statics
        $equipment = Equipments::all();
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get project record
        $project = NationalProject::find($pro_id);
        // Load view to show result
        return view('npmis.national_projects.constructions.add_equipment',compact('progress','record','equipment','lang','tabs','project'));
    }

    public function store_equipment(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'type'              => 'required',
            'equipment_exist'   => 'required',
            'equipment_needed'  => 'required',
        ]);
        $record = array();
        $i = 0;
        if($request->type)
        {
            for($i=0; $i<count($request->type); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'type'                  => $request->type[$i],
                    'equipment_exist'       => $request->equipment_exist[$i],
                    'equipment_needed'      => $request->equipment_needed[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                // Insert record to database 
                NationalEquipment::insert($record);
            }
            Session()->flash('success',  __("global.success_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $equipment = Equipments::all();
            // Get progress report
            $record = NationalEquipment::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_equipment',compact('record','equipment','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_payment()
    {
        // Set current report id in session
        $report_id = session('report_id');
        // Get progress report
        $progress = NationalProgress::whereId($report_id)->first();
        // Get payment record
        $record = NationalPayments::where('national_progress_id',$report_id)->get();
        // Get Language
        $lang = get_language();
        // Set current tab in session
        session(['current_sub_tab' => 'npmis_payments']);
        // Get tabs
        $tabs = getTabs('npmis_constructions',$lang);
        // Get all statics
        $payments = Payments::all();
        // Get project id
        $pro_id = decrypt(session('national_project_id'));
        // Get project record
        $project = NationalProject::find($pro_id);
        // Load view to show result
        return view('npmis.national_projects.constructions.add_payment',compact('progress','record','payments','lang','tabs','project'));
    }

    public function store_payment(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'payment_id'    => 'required',
            'amount'        => 'required',
        ]);
        $record = array();
        $i = 0;
        if($request->payment_id)
        {
            for($i=0; $i<count($request->payment_id); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'payment_id'            => $request->payment_id[$i],
                    'amount'                => $request->amount[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                // Insert record to database 
                NationalPayments::insert($record);
            }
            Session()->flash('success',  __("global.success_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $payments = Payments::all();
            // Get progress report
            $record = NationalPayments::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_payment',compact('record','payments','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_progress(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'report_date'           => 'required',
            'work_progress_planned' => 'required',
            'work_progress_done'    => 'required',
        ]);
        // Language
        $lang = get_language();
        // Record id
        $id = $request->id;
        // Update record
        $record = NationalProgress::find($id);
        $record->report_date        = dateProvider($request->report_date,$lang);
        $record->progress_planned   = $request->work_progress_planned;
        $record->progress_done      = $request->work_progress_done;
        $record->description        = $request->description;
        $record->updated_at         = date('Y-m-d H:i:s');
        $record->updated_by         = userid();
        $record->save();
        if($record)
        {   
            Session()->flash('success',  __("global.success_edit_msg"));
            // Get progress report
            $record = NationalProgress::whereId($id)->first();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get progress record
            $progress = NationalProgress::where('national_project_id',$pro_id)->where('id','<>',$id);
            $progress_planned = $progress->sum('progress_planned');
            $progress_done    = $progress->sum('progress_done');
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_progress',compact('record','progress_planned','progress_done','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_problems(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'problem_id'    => 'required',
            'status'        => 'required',
        ]);
        $i = 0;
        if($request->problem_id)
        {
            $record = array();
            for($i=0; $i<count($request->problem_id); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'problem_id'            => $request->problem_id[$i],
                    'status'                => $request->status[$i],
                    'description'           => $request->description[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                NationalProblems::where('national_progress_id',$request->national_progress_id)->delete();
                // Insert record to database 
                NationalProblems::insert($record);
            }
            Session()->flash('success',  __("global.success_edit_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $problems = Problems::all();
            // Get problem report
            $record = NationalProblems::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_problems',compact('record','problems','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_labor(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'type'          => 'required',
            'labor_exist'   => 'required',
            'labor_needed'  => 'required',
        ]);
        $i = 0;
        if($request->type)
        {
            $record = array();
            for($i=0; $i<count($request->type); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'type'                  => $request->type[$i],
                    'labor_exist'           => $request->labor_exist[$i],
                    'labor_needed'          => $request->labor_needed[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                NationalLabor::where('national_progress_id',$request->national_progress_id)->delete();
                // Insert record to database 
                NationalLabor::insert($record);
            }
            Session()->flash('success',  __("global.success_edit_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $labor = Workers::all();
            // Get progress report
            $record = NationalLabor::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_labor',compact('record','labor','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_equipment(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'type'              => 'required',
            'equipment_exist'   => 'required',
            'equipment_needed'  => 'required',
        ]);
        $i = 0;
        if($request->type)
        {
            $record = array();
            for($i=0; $i<count($request->type); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'type'                  => $request->type[$i],
                    'equipment_exist'       => $request->equipment_exist[$i],
                    'equipment_needed'      => $request->equipment_needed[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                NationalEquipment::where('national_progress_id',$request->national_progress_id)->delete();
                // Insert record to database 
                NationalEquipment::insert($record);
            }
            Session()->flash('success',  __("global.success_edit_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $equipment = Equipments::all();
            // Get progress report
            $record = NationalEquipment::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_equipment',compact('record','equipment','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_payment(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'payment_id'    => 'required',
            'amount'        => 'required',
        ]);
        $i = 0;
        if($request->payment_id)
        {
            $record = array();
            for($i=0; $i<count($request->payment_id); $i++)
            {
                $data = array(
                    'national_project_id'   => decrypt($request->national_project_id),
                    'national_progress_id'  => $request->national_progress_id,
                    'payment_id'            => $request->payment_id[$i],
                    'amount'                => $request->amount[$i],
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => userid(),
                    'department_id'         => departmentid(),
                );
                array_push($record,$data);
            }
            if(COUNT($record)>0)
            {
                NationalPayments::where('national_progress_id',$request->national_progress_id)->delete();
                // Insert record to database 
                NationalPayments::insert($record);
            }
            Session()->flash('success',  __("global.success_edit_msg"));
            // Language
            $lang = get_language();
            // Get all statics
            $payments = Payments::all();
            // Get progress report
            $record = NationalPayments::where('national_progress_id',$request->national_progress_id)->get();
            // Get project id
            $pro_id = decrypt(session('national_project_id'));
            // Get project record
            $project = NationalProject::find($pro_id);
            // load view to display data
            return view('npmis.national_projects.constructions.view_payment',compact('record','payments','lang','project'));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display more labor type.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function moreProblem()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Get all statics
        $problems = Problems::all();
        // Load view
        return view('npmis.national_projects.constructions.more_problem',compact('problems','number','lang'));
    }

    /**
     * Display more labor type.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function moreLabor()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Get all statics
        $labor = Workers::all();
        // Load view
        return view('npmis.national_projects.constructions.more_labor',compact('labor','number','lang'));
    }

    /**
     * Display more equipment type.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function moreEquipment()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Get all statics
        $equipment = Equipments::all();
        // Load view
        return view('npmis.national_projects.constructions.more_equipment',compact('equipment','number','lang'));
    }

    /**
     * Display more payments type.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function morePayment()
    {
        // Get Language
        $lang = get_language();
        // Get Counter Number
        $number = Input::post('number');
        // Get all statics
        $payments = Payments::all();
        // Load view
        return view('npmis.national_projects.constructions.more_payment',compact('payments','number','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function project_complete(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'comp_status_des'    => 'required',
        ]);
        $project_id = decrypt($request->national_project_id);
        // New project
        $record = NationalProject::find($project_id);
        $record->status             = $request->status;
        $record->status_des         = $request->comp_status_des;
        $record->status_at          = date('Y-m-d H:i:s');
        $record->status_by          = userid();
        $record = $record->save();
        if($record)
        {   
            Session()->flash('success',  __("global.status_success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function project_stop(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'stop_status_desc'    => 'required',
        ]);
        $project_id = decrypt($request->national_project_id);
        // New project
        $record = NationalProject::find($project_id);
        $record->status             = $request->status;
        $record->status_des         = $request->stop_status_desc;
        $record->status_at          = date('Y-m-d H:i:s');
        $record->status_by          = userid();
        $record = $record->save();
        if($record)
        {   
            Session()->flash('success',  __("global.status_success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

}
?>