<?php
namespace App\Http\Controllers\NPmis\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use App\Http\Controllers\Controller;
use App\models\NPmis\Activities;
use App\models\NPmis\NationalProjectTypes;
use App\models\NPmis\NationalProject;
use App\models\NPmis\NationalProgress;
use App\models\NPmis\NationalPayments;
use App\models\NPmis\NationalLabor;
use App\models\NPmis\NationalEquipment;
use App\models\NPmis\NationalProblems;
use App\models\NPmis\NationalProjectFeedbacks;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * Get the projects type
     * @return \Illuminate\Http\Response
     */
    public function getProjectType(Request $request)
    {
        //return $request->header('host');
        if($request->selected_badge_id==2)
        {
            $category_id = 1;// Selected large project id
            // Set current section in session
            session(['category_id' => $category_id]);
        }
        elseif($request->selected_badge_id==4)
        {
            $category_id = 2;// Selected special project id
            // Set current section in session
            session(['category_id' => $category_id]);
        }
        else
        {
            $category_id = session('category_id');
        }

        $prject_type = NationalProjectTypes::whereHas('national_projects.projectLocations', function($query) use($request) {
            if(isset($request->province_id))
            {
                $query->where('province_id',$request->province_id);
            }
        })->with(['national_projects'])->where('national_categorie_id',$category_id)->orderBy('id','asc')->get();

        return $prject_type;
    }

    /**
     * Display a listing of the resource.
     * Get the projects type
     * @return \Illuminate\Http\Response
     */
    public function nationalProjectList(Request $request)
    {
        if($request->selected_badge_id==2)
        {
            $category_id = 1;// Selected large project id
            // Set current section in session
            session(['category_id' => $category_id]);
        }
        elseif($request->selected_badge_id==4)
        {
            $category_id = 2;// Selected special project id
            // Set current section in session
            session(['category_id' => $category_id]);
        }
        else
        {
            $category_id = Session('category_id');
        }
        $first_type = NationalProjectTypes::where('national_categorie_id',$category_id)->first();
        if($first_type)
        {
          $national_projects = NationalProject::whereHas('projectLocations', function($query) use($request) {
            if(isset($request->province_id))
            {
                $query->where('province_id',$request->province_id);
            }
           })->with('projectActivities')->with('projectProgress')->with('feedbacks')->where('project_type_id',$first_type->id)->orderBy('id','asc')->get();
          return $national_projects;
        }
        else
          return 'no-data';
    }

    /**
     * Display a listing of the resource.
     * Get the projects type
     * @return \Illuminate\Http\Response
     */
    public function nationalProjectListByType(Request $request)
    {
        $national_projects = NationalProject::whereHas('projectLocations', function($query) use($request) {
            if(isset($request->province_id))
            {
                $query->where('province_id',$request->province_id);
            }
           })->with('projectActivities')->with('projectProgress')->with('feedbacks')->where('project_type_id',$request->project_type_id)->orderBy('id','asc')->get();
        if($national_projects)
          return $national_projects;
        else
          return 'no-data';
    }

    /**
     * @Date: 2021-03-13 10:58:36
     * @Desc: Get project Detials
     */
    public function getNationalProjectDetails(Request $request)
    {
       $national_projects = NationalProject::with(['project_type','category'])->where('id',$request->project_id)->get();
       return $national_projects;
    }

    /**
     * @Date: 2021-03-13 10:58:36
     * @Desc: Get project Activities
     */
    public function getProjectActivities(Request $request)
    {
        $project_id =   $request->project_id;
        $activities =   Activities::with(['nationalActivities' => function($query) use ($project_id){
                                        $query->where('national_project_id',$project_id);
                                    }])->whereHas('nationalActivities', function($query) use ($project_id){
                                        $query->where('national_project_id',$project_id);
                                    })->get();
        if($activities)
        {
            return $activities;
        }
        return 'no-data';
    }

    /**
     * @Date: 2021-03-13 10:58:36
     * @Desc: Get project Activities
     */
    public function getProjectGeneral(Request $request)
    {
        $project_id  = $request->project_id;
        $general  = NationalProject::where('id',$project_id)->get();
        if( $general )
        {
            return $general;
        }
        return 'no-data';
    }

    /**
     * @Date: 2021-03-13 10:58:36
     * @Desc: Get project Activities
     */
    public function getProjectImplementation(Request $request)
    {
        $project_id  = $request->project_id;
        $implementation  = Activities::whereHas('nationalActivities', function($query) use ($project_id){
                        $query->where('national_project_id',$project_id);
                    })->orderBy('id','ASC')->get();
        if( $implementation )
        {
            return $implementation;
        }
        return 'no-data';
    }

    /**
     * @Date: 2021-03-13 10:58:36
     * @Desc: Get project Activities
     */
    public function getProjectDataGraph(Request $request)
    {
        $data = array();
        $project_id  = $request->project_id;
        $national_projects = NationalProject::with(['project_type','category'])->where('id',$project_id)->first();
        //return $national_projects;
        if( $national_projects )
        {
            //Get Projects Details
            $data['details'] = $national_projects;
            // Get project actual progress
            $actual_progress    = NationalProgress::where('national_project_id',$project_id)->get()->sum('progress_done');
            $remained_progress  = 100 - $actual_progress;
            $total_progress     = 100;
            $data['series']     = [(int)$total_progress, (int)$actual_progress, (int)$remained_progress];

            // Get Physical Progress data graph
            $project_progress   = NationalProgress::where('national_project_id',$project_id)->groupBy('report_date')->get();
            $data['progress_date']    = $project_progress->pluck('report_date')->toArray();
            $data['progress_planned'] = $project_progress->pluck('progress_planned')->toArray();
            $data['progress_done']    = $project_progress->pluck('progress_done')->toArray();
            // Get Progress Based on Time
            $working_days       = getDays($national_projects->start_date, $national_projects->end_date);
            $working_days_now   = getDays($national_projects->start_date, date('Y-m-d'));
            $remained_days  = 0;
            if($working_days > $working_days_now){
                $remained_days  = $working_days - $working_days_now;
            }
            $data['timeChartSeries'] = [(int)$working_days, (int)$working_days_now, (int)$remained_days];
            // Get Progress Percentage Based on Time
            $total_days          = 100;
            $total_days_now      = getPercentage($working_days,$working_days_now);
            $total_days_remained = getPercentage($working_days,$remained_days);
            $data['percentageTimeChartSeries'] = [(int)$total_days, (int)$total_days_now, (int)$total_days_remained];
            // Get Workers
            $workers     = NationalLabor::select(DB::raw('SUM(labor_exist) as exist'),DB::raw('SUM(labor_needed) as needed'))->where('national_project_id',$project_id)->first();
            $worders_needed     = $workers->needed;
            $worders_exist      = $workers->exist;
            $worders_not_exist  = 0;
            if($worders_needed > $worders_exist){
                $worders_not_exist  = $worders_needed - $worders_exist;
            }
            $data['humanResourceschartSeries'] = [(int)$worders_needed, (int)$worders_exist, (int)$worders_not_exist];
            // Get Equipments
            $equipments     = NationalEquipment::select(DB::raw('SUM(equipment_exist) as exist'),DB::raw('SUM(equipment_needed) as needed'))->where('national_project_id',$project_id)->first();
            $equipments_needed     = $equipments->needed;
            $equipments_exist      = $equipments->exist;
            $equipments_not_exist  = 0;
            if($equipments_needed > $equipments_exist){
                $equipments_not_exist  = $equipments_needed - $equipments_exist;
            }
            $data['equipmentschartSeries'] = [(int)$equipments_needed, (int)$equipments_exist, (int)$equipments_not_exist];
            // Get Last Report Id
            $progress_id = NationalProgress::where('national_project_id',$project_id)->orderBy('id', 'desc')->first()->id;
            // Get Problems
            $data['challenges'] = NationalProblems::with('problems')->where('national_progress_id',$progress_id)->get();
            //Get Payments data graph
            $project_payments   = DB::connection('pmis')->table('payments as payment')
                                 ->select(DB::raw('SUM(npayment.amount) as total'),'payment.name_dr')
                                 ->join('national_payments as npayment','payment.id','=','npayment.payment_id')
                                 ->groupBy('npayment.payment_id')
                                 ->where('npayment.national_project_id',$project_id)
                                 ->get();
            $data['project_payment_total'] = $project_payments->pluck('total')->toArray();
            $project_payment_category = $project_payments->pluck('name_dr')->toArray();
            if($project_payments->count() > 0)
            {
                foreach ($project_payment_category as $category)
                {
                    $data['project_payment_category'][] = [$category];
                }
            }
            return $data;
        }
        return 'no-data';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @Date: 2021-03-17 11:35:45
     * @Desc: Get projects dashboard data
     */
    public function getProjectsDashboardData(Request $request)
    {
        // orders statistics
        $statistics = NationalProject::
                        whereHas('projectLocations', function($query) use ($request) {
                                if((isset($request->province['id'])))
                                {
                                  $query->where('province_id',$request->province['id']);
                                }
                            }
                        )
                        ->where('category_id',$request->national_category_id)
                            ->select(array(
                                    DB::raw('COUNT(*) as `total`'),
                                    DB::raw(
                                        "SUM(CASE
                                                    WHEN status = '0'
                                                    THEN 1 ELSE 0 END) AS 'under_construction'"
                                    ),
                                    DB::raw(
                                        "SUM(CASE
                                                    WHEN status = '1'
                                                    THEN 1 ELSE 0 END) AS 'completed'"
                                    ),
                                    DB::raw(
                                        "SUM(CASE
                                                    WHEN status = '2'
                                                    THEN 1 ELSE 0 END) AS 'stopped'"
                                    ),
                                ))->first();

        return [
            'count' => $statistics['total'],
            'series' => [(int)$statistics['stopped'], (int)$statistics['completed'], (int)$statistics['under_construction']],
        ];
    }

    public  function getNationalProjectsBySearch(Request $request)
    {
        $project_type_id = $request->project_type_id;
        $project_name = $request->project_name;
        $status = $request->status;
        if($project_name!="" || $status!="" )
        {
            $national_projects = NationalProject::whereHas('projectLocations', function($query) use($request) {
                                        if(isset($request->province_id))
                                        {
                                            $query->where('province_id',$request->province_id);
                                        }
                                    })->with('projectActivities')->with('projectProgress')->with('feedbacks')
                                    ->where('project_type_id',$project_type_id)
                                    ->where('name', 'like', '%'.$project_name.'%')
                                    ->where(function($query) use ($project_name){
                                        if($project_name!="")
                                        {
                                            $query->where('name', 'like', '%'.$project_name.'%');
                                        }
                                    })->where(function($query) use ($status){
                                        if($status!="")
                                        {
                                            $query->where('status',$status);
                                        }
                                    })->orderBy('id','asc')->get();
        }
        else
        {
            $national_projects = NationalProject::whereHas('projectLocations', function($query) use($request) {
                                        if(isset($request->province_id))
                                        {
                                            $query->where('province_id',$request->province_id);
                                        }
                                    })->with('projectActivities')->with('projectProgress')
                                    ->where('project_type_id',$project_type_id)
                                    ->orderBy('id','asc')->get();
        }
        if($national_projects)
            return $national_projects;
        else
            return 'no-data';
    }

    /**
     * Store feedback in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_project_feedback(Request $request)
    {
        if($request->project_id!=0 and $request->feedback!=""){
            // Insert feedback record to database
            $record = new NationalProjectFeedbacks;
            $record->national_project_id    = $request->project_id;
            $record->feedback               = $request->feedback;
            $record->created_at             = date('Y-m-d H:i:s');
            $record = $record->save();
            if($record){
                return 'success';
            }else{
                return 'failed';
            }
        }else{
            return 'failed';
        }
    }

    /**
     * show feedback.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show_project_feedback(Request $request)
    {
        if($request->project_id!=0){
            // get feedback record from database
            $record = NationalProjectFeedbacks::where('national_project_id',$request->project_id)->orderBy('id','desc')->get();
            //ddd($record);
            if($record){
                return $record;
            }else{
                return 'no-data';
            }
        }else{
            return 'no-data';
        }
    }
}
?>
