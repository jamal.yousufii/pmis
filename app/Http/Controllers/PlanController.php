<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Notifications\NewProject;
use App\Http\Requests\PlanRequest;
use App\models\Plans;
use App\models\Requests;
use App\models\Settings\Statics\Categories;
use App\models\Home;
use App\models\Plan_locations;
use App\models\Authentication\SectionSharings;
use App\models\Authentication\Users;
use App\models\Static_data;
use App\models\ProjectUsers;
use Redirect;
use App\User;

class PlanController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department id is not exist
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_plan'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_plan"]);
    // Get Language
    $lang = get_language();
    $data['lang'] = $lang;
    // Get all data
    $data['records'] = Requests::whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_plan');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_plan','pla_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view assigned records
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_plan');
                            });
                          }
                        })->orderBy('id','desc')->paginate(10)->onEachSide(3);
    //Get URL Segments
    $data['segments'] = request()->segments();
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_plan');
    })->where('department_id',$depID)->get();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/plans/list_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/plans/list',$data);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    $data['lang'] = get_language();
    // Request id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Request data
    $data['summary'] = Requests::whereId($id)->first();
    // Get Plan data
    $record = Plans::where('request_id',$id)->first();
    //ddd($record);
    $data['record'] = $record;
    // Default value for shared records
    $data['shared'] = array();
    $data['location'] = array();
    if($record)
    {
      // Get Plan Location
      $data['location'] = Plan_locations::where('project_id',$record->id)->orderBy('id','desc')->paginate(10)->onEachSide(3);
      // Check if record is shared
      $data['shared'] = $record->share()->where('share_from_code','pmis_plan')->first();
      $table['sec_code'] = "pmis_plan";
      // Get Allowed Sections for Sharing
      $data['sections'] = SectionSharings::where('code','pmis_plan')->get();
      // Get Provinces
      $data['provinces'] = getAllProvinces();
      // Get Attachments
      $data['attachments'] = getAttachments('project_attachments',0,$record->id,'plan');
      // Attachment Table
      $data['section'] = 'plan';
      $data['table'] = 'project_attachments';
    }
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/plans/view_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/plans/view',$data);
    }
  }

  public function show_ajax()
  {
    // Get Language
    $data['lang'] = get_language();
    // Project id
    $pro_id = Input::get('pro_id');
    $data['pro_id'] = $pro_id;
    // Get Plan Details
    $data['location'] = Plan_locations::where('project_id',$pro_id)->orderBy('id','desc')->paginate(10)->onEachSide(3);
    // Get Provinces
    $data['provinces'] = getAllProvinces();
    // Load view to show result
    return view('pmis/plans/view_ajax',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $lang = get_language();
    $enc_req = $request->req_id;//request id
    // Get Request Summary
    $request = Requests::whereId(decrypt($enc_req))->first();
    // Get all statics
    $status      = Static_data::where('type',1)->get();
    $year        = yearProvider($lang);
    $categories  = Categories::all();
    $summary = $request;
    // Load view
    return view('pmis/plans/add',compact('lang','enc_req','request','status','year','categories','summary'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(PlanRequest $request)
  {
    // Language
    $lang = get_language();
    $department = decrypt(session('current_department'));
    // Generate URN
    $urn = GenerateURN('projects','urn');
    // New project
    $record = new Plans;
    $record->urn                 = $urn;
    $record->request_id          = decrypt($request->enc_req);
    $record->status              = $request->status;
    $record->name                = $request->name;
    $record->code                = $request->code;
    $record->year                = $request->year;
    $record->project_start_date  = dateProvider($request->project_start_date,$lang);
    $record->project_end_date    = dateProvider($request->project_end_date,$lang);
    $record->category_id         = $request->category;
    $record->project_type_id     = $request->type;
    $record->goal                = $request->goals;
    $record->description         = $request->description;
    $record->created_at          = date('Y-m-d H:i:s');
    $record->created_by          = userid();
    $record->department          = departmentid();
    $record->process             = '0';
    $project = $record->save();
    if($project)
    {
      Session()->flash('success',  __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Record
    $data['record'] = Plans::whereId($id)->first();
    // Get Request Summary
    $data['request'] = Requests::whereId($data['record']->request_id)->first();
    $data['summary'] = $data['request'];
    // Get all statics
    $data['status']        = Static_data::where('type',1)->get();
    $data['year']          = yearProvider($lang);
    $data['categories']    = Categories::all();
    $data['project_types'] = Home::getAllProjectTypesByCategory($data['record']->category_id);
    // Get Attachments
    $data['attachments'] = getAttachments('project_attachments',0,$id,'plan');
    //Pass Attachments table
    $data['table'] = 'project_attachments';
    $data['section'] = 'plan';
    // Load view to show result
    return view('pmis/plans/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(PlanRequest $request,$enc_id)
  {
    // Language
    $lang = get_language();
    // Dycript the id
    $id = decrypt($enc_id);
    $record = Plans::find($id);
    $record->status              = $request->status;
    $record->name                = $request->name;
    $record->code                = $request->code;
    $record->category_id         = $request->category;
    $record->project_type_id     = $request->type;
    $record->year                = $request->year;
    $record->project_start_date  = dateProvider($request->project_start_date,$lang);
    $record->project_end_date    = dateProvider($request->project_end_date,$lang);
    $record->goal                = $request->goals;
    $record->description         = $request->description;
    $record->updated_at          = date('Y-m-d H:i:s');
    $record->updated_by          = userid();
    $updated = $record->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display a listing of the resource by keyword.
   *
   * @return \Illuminate\Http\Response
   */
  public function filterPlan(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    $records = Requests::searchShareRequestPlan($request->condition,$request->field,$request->value,$department_id,$section);
    // Load View to display records
    return view('pmis/plans/list_filter',compact('records','lang','request'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store_location(Request $request)
  {
    // Language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'province' => 'required',
      'district' => 'required',
      'village' => 'required'
    ]);
    // Decrypt Project ID
    $id = decrypt($request->enc_id);

    // Store Project Details
    $record = new Plan_locations;
    $record->project_id     = $id;
    $record->province_id    = $request->province;
    $record->district_id    = $request->district;
    $record->village_id     = $request->village;
    $record->latitude       = $request->latitude;
    $record->longitude      = $request->longitude;
    $record->created_at     = date('Y-m-d H:i:s');
    $record->created_by     = userid();
    $project = $record->save();
    if($project)
    {
      Session()->flash('success',  __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update_location(Request $request)
  {
    // Language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'province' => 'required'
    ]);
    // Decrypt Project ID
    $id = decrypt($request->enc_id);
    // Store Project Details
    $record = Plan_locations::find($id);
    $record->province_id    = $request->province;
    $record->district_id    = $request->district;
    $record->village_id     = $request->village;
    $record->latitude       = $request->latitude;
    $record->longitude       = $request->longitude;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    if($record->save())
    {
      Session()->flash('success_detail', __("global.success_edit_msg"));
      // Get Language
      $data['lang'] = get_language();
      // Get Plan Details
      $data['location'] = Plan_locations::whereId($id)->get();
      // Get Provinces
      $data['provinces'] = getAllProvinces();
      // Load view to show result
      return view('pmis/plans/location_ajax',$data);
    }
    else
    {

      Session()->flash('fail_detail', __("global.failed_msg"));
      // Get Language
      $data['lang'] = get_language();
      // Get Plan Details
      $data['location'] = Plan_locations::whereId($id)->get();
      // Get Provinces
      $data['provinces'] = getAllProvinces();
      // Load view to show result
      return view('pmis/plans/location_ajax',$data);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('plan', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.request_assigned',['urn'=>$request->record_urn]), userid(), $redirect_url));
              }
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_plan')->get();
    // Load view to show result
    return view('pmis/plans/list_assigned_users',compact('records'));
  }
}
?>
