<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\models\Provinces;
use App\models\Plan_locations;
use App\models\Plans;
use Redirect;

class GraphController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id)
    {
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_graph'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_graph"]);
        $lang = get_language();
        $segments = array(
            '0' =>'graph.index',
            '1' =>$dep_id
        );
        // Get all static tables
        $graph = get_module_sections(session('current_mod'),$dep_id,'pmis_graph','2');
        // Load view to display data
        return view('pmis/graphs/graph',compact('graph','lang','dep_id','segments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function location_year()
    {
        session(['current_section' => "pmis_graph"]);
        // Get Language
        $lang = get_language();
        $province = Provinces::get();
        $department = array(2,3);
        // Load View
        return view('pmis/graphs/location_year',compact('province','lang','department'));
    }

    public function location_year_graph(Request $request)
    {
        // Set current session
        session(['current_section' => "pmis_graph"]);
        // Get Language
        $lang = get_language();
        // Get posted value by user
        $year     = explode(',',$request->year);
        $province = explode(',',$request->province);
        $location = array();
        // Get projects with conditions
        $projects = Plans::when(!in_array('0',$year),function ($query) use($year)
                                {
                                    return $query->whereIn('year',$year);
                                }
                            )->whereHas('project_location', function($query) use ($province){
                                if(!in_array('0',$province)){
                                    $query->whereIn('province_id',$province);
                                }
                            })->get();
        // foreach all selected projects from db
        foreach($projects as $value){
            // Get project location based on project
            $locations = Plan_locations::where('project_id',$value->id)
                                        ->when(!in_array('0',$province),function ($query) use($province){
                                            return $query->whereIn('province_id',$province);
                                        })->groupBy('province_id')->get();
            if($locations){
                foreach($locations as $item){
                    $location[] = $item->province_id;
                }
            }
        }
        // Count from array based on location
        $location = array_count_values($location);
        // Load view to display data
        return view('pmis/graphs/location_year_view_graph',compact('lang','location'));
    }
}
