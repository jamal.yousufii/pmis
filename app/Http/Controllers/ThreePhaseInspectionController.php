<?php

namespace App\Http\Controllers;
use App\models\Billofquantities;
use App\models\Static_data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\session;
use App\models\ThreePhaseInspection;
use App\models\Settings\Inspection;
use App\models\Settings\InspectionSub;
use App\models\Plans;
use App\models\Requests;
use App\models\Plan_locations;
use Redirect;

class ThreePhaseInspectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0,$dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_progress'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        // Get Language
        $lang = get_language();
        $data['lang'] = $lang;
        //Get Inspection data
        $data['result'] = Inspection::orderBy('id', 'DESC')->get(); 
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        // Set current section in session
        session(['current_tab' => 'pmis_phase_inspection']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        // Load view to show result    
        return view('pmis/progress/three_phase_inspection/index',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($enc_id=0,$dep_id=0,$loc_id=0)
    {
        if(check_my_section('pmis_phase_Inspection'))
        {
            // Get Language
            $lang = get_language();
            $data['lang'] = $lang;
            //Get Inspection data
            session(['current_section' => "pmis_progress"]);
            $data['result'] = Inspection::orderBy('id', 'DESC')->get(); 
            // Project id
            $id = decrypt($enc_id);
            $data['enc_id'] = $enc_id;
            // Project Location id
            $data['project_location_id'] = $loc_id;
            session(['project_location_id' => $loc_id]);
            // Check if department ID is exist or not
            if($dep_id=='0' AND session('current_department')=='0')
            {
                return Redirect::route("home");
            }
            else
            {
                $dep_id = $dep_id;  
            }
            // Get Department ID
            $depID = decrypt($dep_id);
            $data['dep_id'] = $depID; 
            // Get plan record
            $data['plan'] = Plans::find($id);
            if($data['plan'])
            {
                // Get summary data
                $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
            }
            // Load tabs
            $data['tabs'] = getTabs('pmis_progress',$lang);
            session(['current_tab' => 'pmis_phase_inspection']);            
            // Get Bill of Quantities
            $data['records'] = Billofquantities::with('inspection')->where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->orderBy('bq_section_id','asc')->paginate(20)->onEachSide(1);
            // Project locations
            if(is_admin())// Get all locations for admin users
            {
                $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                    $query->where('project_id',$id);
                    $query->where('department',$depID);
                })->where('status','0')->orderBy('id','desc')->get();
            }
            else// Get project locations based on users access
            {
                $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                    $query->where('userid',userid());
                    $query->where('project_id',$id);
                    $query->where('department_id',$depID);
                })->where('status','0')->orderBy('id','desc')->get();
            }
            $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','<>','0')->orderBy('id','desc')->get();
            // Load view to show result
            if(Input::get('ajax') == 1)
            {
                return view('pmis/progress/three_phase_inspection/list_ajax',$data);
            }
            else
            {
                return view('pmis/progress/three_phase_inspection/list',$data);
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pro_id,$bq_id,$loc_id)
    {
        $proId = decrypt($pro_id);
        $bqId  = decrypt($bq_id);
        $locId = decrypt($loc_id);
        $data['pro_id'] = $pro_id;
        $data['bq_id']  = $bq_id; 
        $data['locId']  = $locId;
        $data['plan'] = Plans::find($proId);
        $data['result'] = Inspection::orderBy('id', 'DESC')->get(); 
        $data['status']  = Static_data::where('type',7)->get();
        // Get Language
        $data['lang'] = get_language();
        $data['inspection'] = Inspection::with(['InspectionSub.threePhaseInspection' => function($q) use($bqId){
            $q->where('bill_quant_id',$bqId);
        }])->get();
        // Get Location
        $data['locations'] = Plan_locations::where('project_id',$proId)->get();
        // Load View
        return view('pmis/progress/three_phase_inspection/inspection',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'main_insepction' => 'required',
            'sub_inspection'  => 'required',
            'status'          => 'required',
            'description'     => 'required',
        ]);
        // Store Data
        $record = new ThreePhaseInspection;
        $record->project_id           = decrypt($request->project_id);
        $record->project_location_id  = $request->location_id;
        $record->inspection_id        = $request->main_insepction;
        $record->inspection_sub_id    = $request->sub_inspection;
        $record->status               = $request->status;
        $record->bill_quant_id        = $request->bill_quant_id;
        $record->description          = $request->description;
        $record->created_by           = userid();
        if($record->save())
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubInepection()
    {
        $id = Input::get('id');
        // Get Language
        $lang = get_language();
        $result = InspectionSub::where('inspection_id', $id)->get(); 
        return view('pmis/progress/three_phase_inspection/sub_Inspection_Dropdown',compact('result','lang'));
    }
    
    /**
     * Display the sub inspection details.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getInspectionDetails(Request $request)
    {
        $details = ThreePhaseInspection::where('inspection_sub_id',$request->id)->where('bill_quant_id',$request->bq_id)->get();
        $lang = get_language();
        return view('pmis/progress/three_phase_inspection/details',compact('details','lang')); 
    }

}
