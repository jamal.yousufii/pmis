<?php
namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\models\Settings\Portfolios;
use App\models\Settings\PortfoliosSub;
use App\models\Plans;
use Redirect;

class PortfoliosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(check_my_auth_section('setting_portfolio'))
        {
            $lang  = get_language();
            session(['current_section' => "setting_portfolio"]);
            // Get All Work Portfolios   
            $records = Portfolios::orderBy('id','desc')->paginate(10);
            if(Input::get('ajax') == 1)
            {
                return view('settings.portfolios.list_ajax',compact('lang','records'));
            }
            else
            {
                return view('settings.portfolios.list',compact('lang','records'));
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = get_language(); 
        // Get Departments By Module code
        $departments = getDepartmentnameByCode(array('dep_02','dep_03','dep_08'));
        // Load View to display data
        return view('settings.portfolios.add',compact('departments','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'name_dr'       => 'required',
            'name_pa'       => 'required',
            'name_en'       => 'required',
            'department_id' => 'required',
            'project_id'    => 'required',
        ]);
        $record = new Portfolios;
        $record->name_dr        = $request->name_dr;
        $record->name_pa        = $request->name_pa;
        $record->name_en        = $request->name_en;
        $record->department_id  = $request->department_id;
        $record->created_by     = userid();
        if($record->save())
        {
            //INSERT Section's Departments
            $projects = $request->project_id;
            $pro = array();
            if($projects)
            {
                foreach($projects as $item)
                {
                    $data = array(
                        'portfolio_id'  =>  $record->id,
                        'project_id'    =>  $item,
                        'created_at'    =>  date('Y-m-d H:i:s'),
                        'created_by'    => userid(),
                    );
                    array_push($pro,$data);
                }
                if(COUNT($pro)>0)
                {
                    // Insert Data 
                    PortfoliosSub::insert($pro);
                }
            }

            Session()->flash('success', __("global.success_msg"));
        }    
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Get language
        $lang = get_language();
        // Dycript the id
        $id = decrypt($enc_id);
        // Get Record
        $record = Portfolios::with('PortfoliosSub')->find($id);
        // Load View to display data
        return view('settings.portfolios.view',compact('lang','enc_id','record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        $lang = get_language();
        // Get all Work Category
        $id = decrypt($enc_id);
        // Get Record
        $record = Portfolios::find($id);
        // Get Departments By Module code
        $departments = getDepartmentnameByCode(array('dep_02','dep_03'));
        // Get projects of this department
        $project_id = $record->department_id;
        $projects = Plans::whereHas('share', function($query) use ($project_id){
            $query->where('share_to',$project_id);
            $query->where('share_to_code','pmis_progress');
        })->get();
        return view('settings.portfolios.edit',compact('lang','enc_id','record','departments','projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {
        $validates = $request->validate([
            'name_dr'       => 'required',
            'name_pa'       => 'required',
            'name_en'       => 'required',
            'department_id' => 'required',
            'project_id'    => 'required',
        ]);
        // Dycript the id
        $id = decrypt($enc_id);
        // Update data in database
        $record = Portfolios::find($id);
        $record->name_dr    = $request->name_dr;
        $record->name_pa    = $request->name_pa;
        $record->name_en    = $request->name_en;
        $record = $record->save();
        if($record)
        {
            //INSERT Section's Departments
            $projects = $request->project_id;
            $pro = array();
            if($projects)
            {
                foreach($projects as $item)
                {
                    $data = array(
                        'portfolio_id'  => $id,
                        'project_id'    => $item,
                        'updated_at'    => date('Y-m-d H:i:s'),
                        'updated_by'    => userid(),
                    );
                    array_push($pro,$data);
                }
                if(COUNT($pro)>0)
                {
                    //Delete Befor Indert New Applications
                    PortfoliosSub::where('portfolio_id',$id)->delete();
                    // Insert Data 
                    PortfoliosSub::insert($pro);
                }
            }
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $record = Portfolios::whereId($id)->delete();
        if($record)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_delete_msg"));
        }
    }

    public function filterPortfolio()
    {
        $lang = get_language();
        if(Input::get('item'))
        {
            $item = Input::get('item');
            // Get data by keywords
            $records = Portfolios::whereRaw("(name_dr like '%".$item."%' OR name_pa like '%".$item."%' OR name_en like '%".$item."%')")->paginate(10);
        }
        else
        {
            // Get all data
            $records = Portfolios::paginate(10);
        }    
        // Load View to display records
        return view('settings.portfolios.list_filter',compact('records','lang')); 
    }

    public function getPorjectsByDepartment()
    {
        $id = Input::post('id');
        if($id)
        {
            // Get projects of this department
            $projects = Plans::whereHas('share', function($query) use ($id){
              $query->where('share_to',$id);
              $query->where('share_to_code','pmis_progress');
            })->get();
            return view('settings.portfolios.projects',compact('projects'));
        }
        else 
        {
            return;
        }
    }
}
