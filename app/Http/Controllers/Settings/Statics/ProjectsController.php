<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Projects;
use Response;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addProject()
    {
      $data['categories'] = Projects::getAllCategories();
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      return view('settings/statics/projects/add',$data);
    }

    public function storeProject(Request $request)
    {
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
          'category'  => 'required',
          'section'   => 'required',
      ]);
      $record = new Projects;
      $record->name_en      = $request->name_en;
      $record->name_dr      = $request->name_dr;
      $record->name_pa      = $request->name_pa;
      $record->category_id  = $request->category;
      $record->section      = $request->section;
      $insert = $record->save();
      if($insert>0)
      {
        Session()->flash('success', __("statics.success_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }

    public function viewProject()
    {
      $enc_id = Input::post('id');
      //Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Projects::find($id);
      $lang = get_language();
      $data['lang'] = $lang;
      return view('settings/statics/projects/view',$data);
    }

    public function editProject()
    {
      $enc_id = Input::post('id');
      $data['enc_id'] = $enc_id;
      $id = decrypt($enc_id);
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Projects::find($id);
      $data['categories'] = Projects::getAllCategories();
      return view('settings/statics/projects/edit',$data);
    }

    public function doEditProject(Request $request)
    {
      $id = decrypt(Input::get('enc_id'));
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
          'category'  => 'required',
          'section'   => 'required',
      ]);
      //Update record
      $record = Projects::find($id);
      $record->name_en      = $request->name_en;
      $record->name_dr      = $request->name_dr;
      $record->name_pa      = $request->name_pa;
      $record->category_id  = $request->category;
      $record->section      = $request->section;
      $update = $record->save();
      if($update>0)
      {
        Session()->flash('success', __("statics.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }

    public function deleteProject()
    {
      $enc_id = Input::post('id');
      $id = decrypt($enc_id);
      //Update record
      $record = Projects::find($id);
      $status = $record->delete();
      if($status!=0)
      {
        Session()->flash('success', __("statics.success_delete_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }
}
?>
