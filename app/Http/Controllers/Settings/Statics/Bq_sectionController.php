<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Bq_section;
use Response;

class Bq_sectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posted_value = Input::post('posted_value');
        $data['posted_value'] = $posted_value;
        return view('settings/statics/bq_section/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate the request...
        $validates = $request->validate([
            'name_en'   => 'required',
            'name_dr'   => 'required',
            'name_pa'   => 'required',
        ]);
        $documents = new Bq_section;
        $documents->name_en  = $request->name_en;
        $documents->name_dr  = $request->name_dr;
        $documents->name_pa  = $request->name_pa;
        $insert = $documents->save();
        if($insert>0)
        {
            Session()->flash('success', __("statics.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("statics.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        $enc_id = Input::post('id');
        //Dycript the id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        $posted_value = Input::post('posted_value');
        $data['posted_value'] = $posted_value;
        $data['record'] = Bq_section::find($id);
        return view('settings/statics/bq_section/view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        $enc_id = Input::post('id');
        $data['enc_id'] = $enc_id;
        $id = decrypt($enc_id);
        $posted_value = Input::post('posted_value');
        $data['posted_value'] = $posted_value;
        $data['record'] = Bq_section::find($id);
        return view('settings/statics/bq_section/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$enc_id)
    {
        $id = decrypt($enc_id);
        //Validate the request...
        $validates = $request->validate([
            'name_en'     => 'required',
            'name_dr'     => 'required',
            'name_pa'     => 'required',
        ]);
        //Update record
        $record = Bq_section::find($id);
        $record->name_en  = $request->name_en;
        $record->name_dr  = $request->name_dr;
        $record->name_pa  = $request->name_pa;
        $update = $record->save();
        if($update>0)
        {
            Session()->flash('success', __("statics.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', '__("statics.failed_msg")');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        $enc_id = Input::post('id');
        $id = decrypt($enc_id);
        //Update record
        $record = Bq_section::find($id);
        $status = $record->delete();
        if($status!=0)
        {
            Session()->flash('success', __("statics.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', '__("statics.failed_msg")');
        }
    }

}
?>
