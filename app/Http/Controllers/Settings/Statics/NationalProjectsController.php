<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\NPmis\NationalCategories;
use App\models\NPmis\NationalProjectTypes;
use Response;

class NationalProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addNationalProject()
    {
      $lang = get_language();
      $data['categories'] = NationalCategories::select('id','name_'.$lang.' as name')->get();
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      return view('settings/statics/national_projects/add',$data);
    }

    public function storeNationalProject(Request $request)
    {
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
          'category'  => 'required',
      ]);
      $record = new NationalProjectTypes;
      $record->name_en      = $request->name_en;
      $record->name_dr      = $request->name_dr;
      $record->name_pa      = $request->name_pa;
      $record->icon         = 'fas fa-layer-group'; // default icon
      $record->national_categorie_id  = $request->category;
      $insert = $record->save();
      if($insert>0)
      {
        Session()->flash('success', __("statics.success_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }

    public function viewNationalProject()
    {
      $enc_id = Input::post('id');
      //Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = NationalProjectTypes::find($id);
      $lang = get_language();
      $data['lang'] = $lang;
      return view('settings/statics/national_projects/view',$data);
    }

    public function editNationalProject()
    {
      $lang = get_language();
      $enc_id = Input::post('id');
      $data['enc_id'] = $enc_id;
      $id = decrypt($enc_id);
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = NationalProjectTypes::find($id);
      $data['categories'] = NationalCategories::select('id','name_'.$lang.' as name')->get();
      return view('settings/statics/national_projects/edit',$data);
    }

    public function doEditNationalProject(Request $request)
    {
      $id = decrypt(Input::get('enc_id'));
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
          'category'  => 'required',
      ]);
      //Update record
      $record = NationalProjectTypes::find($id);
      $record->name_en      = $request->name_en;
      $record->name_dr      = $request->name_dr;
      $record->name_pa      = $request->name_pa;
      $record->national_categorie_id  = $request->category;
      $update = $record->save();
      if($update>0)
      {
        Session()->flash('success', __("statics.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }

    public function deleteNationalProject()
    {
      $enc_id = Input::post('id');
      $id = decrypt($enc_id);
      //Update record
      $record = NationalProjectTypes::find($id);
      $status = $record->delete();
      if($status!=0)
      {
        Session()->flash('success', __("statics.success_delete_msg"));
      }
      else
      {
        Session()->flash('fail', '__("statics.failed_msg")');
      }
    }
}
?>
