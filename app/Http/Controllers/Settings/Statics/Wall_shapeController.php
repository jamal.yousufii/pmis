<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Wall_shape;
use Response;

class Wall_shapeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addWall_shape()
    {
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      return view('settings/statics/wall_shape/add',$data);
    }

    public function storeWall_shape(Request $request)
    {
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
      ]);
      $record = new Wall_shape;
      $record->name_en  = $request->name_en;
      $record->name_dr  = $request->name_dr;
      $record->name_pa  = $request->name_pa;
      $insert = $record->save();
      if($insert>0)
      {
        Session()->flash('success', __("statics.success_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function viewWall_shape()
    {
      $enc_id = Input::post('id');
      //Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Wall_shape::find($id);
      return view('settings/statics/wall_shape/view',$data);
    }

    public function editWall_shape()
    {
      $enc_id = Input::post('id');
      $data['enc_id'] = $enc_id;
      $id = decrypt($enc_id);
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Wall_shape::find($id);
      return view('settings/statics/wall_shape/edit',$data);
    }

    public function doEditWall_shape(Request $request)
    {
      $id = decrypt(Input::get('enc_id'));
      //Validate the request...
      $validates = $request->validate([
          'name_en'     => 'required',
          'name_dr'     => 'required',
          'name_pa'     => 'required',
      ]);
      //Update record
      $record = Wall_shape::find($id);
      $record->name_en  = $request->name_en;
      $record->name_dr  = $request->name_dr;
      $record->name_pa  = $request->name_pa;
      $update = $record->save();
      if($update>0)
      {
        Session()->flash('success', __("statics.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function deleteWall_shape()
    {
      $enc_id = Input::post('id');
      $id = decrypt($enc_id);
      //Update record
      $record = Wall_shape::find($id);
      $status = $record->delete();
      if($status!=0)
      {
        Session()->flash('success', __("statics.success_delete_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }
}
?>
