<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Foundation;
use Response;

class FoundationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addFoundation()
    {
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      return view('settings/statics/foundation/add',$data);
    }

    public function storeFoundation(Request $request)
    {
      //Validate the request...
      $validates = $request->validate([
          'name_en'   => 'required',
          'name_dr'   => 'required',
          'name_pa'   => 'required',
      ]);
      $record = new Foundation;
      $record->name_en  = $request->name_en;
      $record->name_dr  = $request->name_dr;
      $record->name_pa  = $request->name_pa;
      $insert = $record->save();
      if($insert>0)
      {
        Session()->flash('success', __("statics.success_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function viewFoundation()
    {
      $enc_id = Input::post('id');
      //Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Foundation::find($id);
      return view('settings/statics/foundation/view',$data);
    }

    public function editFoundation()
    {
      $enc_id = Input::post('id');
      $data['enc_id'] = $enc_id;
      $id = decrypt($enc_id);
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Foundation::find($id);
      return view('settings/statics/foundation/edit',$data);
    }

    public function doEditFoundation(Request $request)
    {
      $id = decrypt(Input::get('enc_id'));
      //Validate the request...
      $validates = $request->validate([
          'name_en'     => 'required',
          'name_dr'     => 'required',
          'name_pa'     => 'required',
      ]);
      //Update record
      $record = Foundation::find($id);
      $record->name_en  = $request->name_en;
      $record->name_dr  = $request->name_dr;
      $record->name_pa  = $request->name_pa;
      $update = $record->save();
      if($update>0)
      {
        Session()->flash('success', __("statics.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function deleteFoundation()
    {
      $enc_id = Input::post('id');
      $id = decrypt($enc_id);
      //Update record
      $record = Foundation::find($id);
      $status = $record->delete();
      if($status!=0)
      {
        Session()->flash('success', __("statics.success_delete_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }
}
?>
