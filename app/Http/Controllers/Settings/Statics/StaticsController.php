<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Statics;
use App\models\Settings\Statics\Categories;
use App\models\Settings\Statics\Documents;
use App\models\Settings\Statics\Projects;
use App\models\Settings\Statics\Units;
use App\models\Settings\Statics\Financial_source;
use App\models\Settings\Statics\Employees;
use App\models\Settings\Statics\Roof;
use App\models\Settings\Statics\Fear;
use App\models\Settings\Statics\Foundation;
use App\models\Settings\Statics\Stair;
use App\models\Settings\Statics\Wall;
use App\models\Settings\Statics\Wall_shape;
use App\models\Settings\Statics\Column;
use App\models\Settings\Statics\Column_shape;
use App\models\Settings\Statics\Brick;
use App\models\Settings\Statics\Machinery;
use App\models\Settings\Statics\Labor_clasification;
use App\models\Settings\Statics\Bq_section;
use App\models\NPmis\Activities;
use App\models\NPmis\Workers;
use App\models\NPmis\Equipments;
use App\models\NPmis\Payments;
use App\models\NPmis\Problems;
use App\models\NPmis\NationalCategories;
use App\models\NPmis\NationalProjectTypes;
use Response;

class StaticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(check_my_auth_section('setting_statics'))
      {
        $lang = get_language();
        session(['current_section' => "setting_statics"]);
        //Get all static tables
        $data['statics'] = Statics::getAllStatics($lang);
        return view('settings/statics/statics',$data);
      }
      else
      {
        return view('access_denied');
      }
    }

    public function viewStatics()
    {
      $lang = get_language();
      $value = Input::get('code');
      if($value)
      {
        $ex_value = explode('-',$value);
        $code  = $ex_value[0];
        $table = $ex_value[1];
        switch($code)
        {
          case '1':
          {
            $record = Categories::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/categories/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/categories/list',$data);
            }
            break;
          }
          case '2':
          {
            $record = Documents::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/documents/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/documents/list',$data);
            }
            break;
          }
          case '3':
          {
            $record = Projects::with('categories')->orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            $data['lang']  = $lang;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/projects/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/projects/list',$data);
            }
            break;
          }
          case '4':
          {
            $record = Units::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/units/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/units/list',$data);
            }
            break;
          }
          case '5':
          {
            $record = Financial_source::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/financial_source/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/financial_source/list',$data);
            }
            break;
          }
          case '6':
          {
            $record = Employees::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/employees/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/employees/list',$data);
            }
            break;
          }
          case '7':
          {
            $record = Roof::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/roof/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/roof/list',$data);
            }
            break;
          }
          case '8':
          {
            $record = Fear::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/fear/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/fear/list',$data);
            }
            break;
          }
          case '9':
          {
            $record = Foundation::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/foundation/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/foundation/list',$data);
            }
            break;
          }
          case '10':
          {
            $record = Stair::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/stair/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/stair/list',$data);
            }
            break;
          }
          case '11':
          {
            $record = Wall::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/wall/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/wall/list',$data);
            }
            break;
          }
          case '12':
          {
            $record = Wall_shape::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/wall_shape/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/wall_shape/list',$data);
            }
            break;
          }
          case '13':
          {
            $record = Column::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/column/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/column/list',$data);
            }
            break;
          }
          case '14':
          {
            $record = Column_shape::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/column_shape/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/column_shape/list',$data);
            }
            break;
          }
          case '15':
          {
            $record = Brick::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/brick/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/brick/list',$data);
            }
            break;
          }
          case '16':
          {
            $record = Labor_clasification::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/labor_clasification/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/labor_clasification/list',$data);
            }
            break;
          }
          case '17':
          {
            $record = Machinery::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/machinery/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/machinery/list',$data);
            }
            break;
          }
          case '18':
          {
            $record = Bq_section::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/bq_section/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/bq_section/list',$data);
            }
            break;
          }
          case '19':
          {
            $record = Activities::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/activities/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/activities/list',$data);
            }
            break;
          }
          case '20':
          {
            $record = Workers::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/workers/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/workers/list',$data);
            }
            break;
          }
          case '21':
          {
            $record = Equipments::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/equipments/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/equipments/list',$data);
            }
            break;
          }
          case '22':
          {
            $record = Payments::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/payments/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/payments/list',$data);
            }
            break;
          }
          case '23':
          {
            $record = Problems::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/problems/list_ajax',$data);
            }
            else
            {
              return view('settings/statics/problems/list',$data);
            }
            break;
          }
          case '24':
          {
            $record = NationalCategories::orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/national_categories/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/national_categories/list',$data);
            }
            break;
          }
          case '25':
          {
            $record = NationalProjectTypes::with('national_categories')->orderBy('id','desc')->paginate(10);
            $data['record'] = $record;
            $data['posted_value']  = $value;
            $data['lang']  = $lang;
            if(Input::get('ajax') == 1)
            {
              return view('settings/statics/national_projects/main_list_ajax',$data);
            }
            else
            {
              return view('settings/statics/national_projects/list',$data);
            }
            break;
          }
        }
      }
    }
}
?>
