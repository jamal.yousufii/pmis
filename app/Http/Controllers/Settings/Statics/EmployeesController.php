<?php
namespace App\Http\Controllers\Settings\Statics;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Statics\Employees;
use App\models\Authentication\Sections;
use Response;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addEmployee()
    {
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['sections'] = Sections::all();
      $data['lang'] = get_language();
      return view('settings/statics/employees/add',$data);
    }

    public function storeEmployee(Request $request)
    {
      //Validate the request...
      $validates = $request->validate([
          'first_name' => 'required',
          'last_name'  => 'required',
          'job'        => 'required',
          'section'    => 'required',
      ]);
      $record = new Employees;
      $record->first_name  = $request->first_name;
      $record->last_name   = $request->last_name;
      $record->job         = $request->job;
      $record->section     = $request->section;
      $insert = $record->save();
      if($insert>0)
      {
        Session()->flash('success', __("statics.success_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function viewEmployee()
    {
      $enc_id = Input::post('id');
      //Dycript the id
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Employees::find($id);
      $data['lang'] = get_language();
      return view('settings/statics/employees/view',$data);
    }

    public function editEmployee()
    {
      $enc_id = Input::post('id');
      $data['enc_id'] = $enc_id;
      $id = decrypt($enc_id);
      $posted_value = Input::post('posted_value');
      $data['posted_value'] = $posted_value;
      $data['record'] = Employees::find($id);
      $data['sections'] = Sections::all();
      $data['lang'] = get_language();
      return view('settings/statics/employees/edit',$data);
    }

    public function doEditEmployee(Request $request)
    {
      $id = decrypt(Input::get('enc_id'));
      //Validate the request...
      $validates = $request->validate([
          'first_name' => 'required',
          'last_name'  => 'required',
          'job'        => 'required',
          'section'    => 'required',
      ]);
      //Update record
      $record = Employees::find($id);
      $record->first_name  = $request->first_name;
      $record->last_name   = $request->last_name;
      $record->job         = $request->job;
      $record->section     = $request->section;
      $update = $record->save();
      if($update>0)
      {
        Session()->flash('success', __("statics.success_edit_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }

    public function deleteEmployee()
    {
      $enc_id = Input::post('id');
      $id = decrypt($enc_id);
      //Update record
      $record = Employees::find($id);
      $status = $record->delete();
      if($status!=0)
      {
        Session()->flash('success', __("statics.success_delete_msg"));
      }
      else
      {
        Session()->flash('fail', __("statics.failed_msg"));
      }
    }
}
?>
