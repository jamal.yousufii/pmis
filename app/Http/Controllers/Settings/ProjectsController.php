<?php
namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use App\Notifications\NewProject;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Settings\Project_contractor_staff;
use App\models\Settings\ProjectDepartmentStaff;
use App\models\Authentication\Users;
use App\models\Settings\Contractor;
use App\models\Settings\Project_contractor;

class ProjectsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(check_my_auth_section('setting_projects'))
    {
      $lang = get_language();
      $data['lang'] = $lang;
      session(['current_section' => 'setting_projects']);
      // Get Departments By Module code
      $data['departments'] = getDepartmentnameByCode(array('dep_02','dep_03','dep_08'));
      // Load view to show result
      return view('settings/projects/departments',$data);
    }
    else
    {
      return view('access_denied');
    }
  }
  
  // Contractor Staff
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function bringProjects($dep_id)
  {
    if(check_my_auth_section('setting_projects'))
    {
      $lang = get_language();
      $data['lang'] = $lang;
      session(['current_section' => 'setting_projects']);
      session(['current_department' => $dep_id]);
      // Get Department ID
      $depID = decrypt($dep_id);
      $data['dep_id'] = $depID;
      // Get Department's Name
      $data['department_name'] = getDepartmentNameByID($depID,$lang);
      // Get projects of this department
      $data['records'] = Plans::whereHas('project_location', function($query){
          $query->where('status','0');
        })->whereHas('share', function($query) use ($depID){
          $query->where('share_to',$depID);
          $query->where('share_to_code','pmis_progress');
        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
      // load view to show result
      if(Input::get('ajax') == 1)
      {
        return view('settings/projects/contractor/list_ajax',$data);
      }
      else
      {
        return view('settings/projects/contractor/list',$data);
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list($enc_id)
  {
    $data['lang'] = get_language();
    // Project id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get project data
    $data['plan'] = Plans::find($id);
    // Get data
    $data['records'] = Project_contractor::where('project_id',$id)->get();
    // Load view to show result
    return view('settings/projects/contractor/contractors',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    $data['lang'] = $lang;
    $enc_id = Input::get('project_id');
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get project data
    $data['plan'] = Plans::find($id);
    // Get contractor's data
    $data['contractors'] = Contractor::orderBy('id','DESC')->get();
    // Load view to show result
    return view('settings/projects/contractor/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'contractor_id'   => 'required',
      'project_id'      => 'required',
    ]);
    // Get Language
    $lang = get_language();
    // New Record
    $record = new Project_contractor;
    $record->contractor_id  = $request->contractor_id;
    $record->project_id     = $request->project_id;
    $record->created_at     = date('Y-m-d H:i:s');
    $record->created_by     = Auth::user()->id;
    // Store data in database
    if($record->save())
    {
        Session()->flash('success', __("global.success_msg"));
    }
    else
    {
        Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get data
    $record = Project_contractor::find($id);
    $data['record'] = $record;
    // Get contractor's staff
    $users = Users::select('pcs.id as staff_id','users.*','loc.latitude','loc.longitude','pro.name_'.$lang.' as pro_name','dis.name_'.$lang.' as dis_name','vil.name_'.$lang.' as vil_name')
              ->where('pcs.project_id',$record->project_id)
              ->where('users.contractor_id',$record->contractor_id)
              ->join('pmis.project_contractor_staff as pcs','users.id','=','pcs.user_id')  
              ->join('pmis.project_locations as loc','loc.id','=','pcs.location_id')  
              ->join('pmis_auth.provinces as pro','pro.id','=','loc.province_id')  
              ->leftJoin('pmis_auth.districts as dis','dis.id','=','loc.district_id')  
              ->leftJoin('pmis_auth.villages  as vil','vil.id','=','loc.village_id')  
              ->get();  
    $data['users'] = $users;
    // Load view to show result
    return view('settings/projects/contractor/view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $data['lang'] = get_language();
    // Project id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get data
    $data['record'] = Project_contractor::where('project_id',$id)->first();
    // Get contractor's data
    $data['contractors'] = Contractor::orderBy('id','DESC')->get();
    // Load view to show result
    return view('settings/projects/contractor/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    // Validate the request...
    $validates = $request->validate([
      'contractor_id'   => 'required',
      'project_id'      => 'required',
    ]);
    // Get Language
    $lang = get_language();
    $id = decrypt($id);
    // Update Record
    $record = Project_contractor::find($id);
    $record->contractor_id  = $request->contractor_id;
    $record->project_id     = $request->project_id;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = Auth::user()->id;
    // Store data in database
    if($record->save())
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    // Decrypt ID
    $id = decrypt($id);
    // Update record
    $record = Project_contractor_staff::find($id);
    $id = $record->delete();
    if($id!=0)
    {
      Session()->flash('success', __("global.success_delete_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  public function filter_projects()
  {
    $lang = get_language();
    $data['lang'] = $lang;
    $keyWord = Input::get('keyWord');
    $section = Input::get('section');
    $dep_id = Input::get('department_id');
    $data['keyWord'] = $keyWord;
    $data['department_id'] = $dep_id;
    if($keyWord!='')
    {
      // Get data by keyword
      $data['records'] = Plans::where('name', 'like','%'.$keyWord.'%')->whereHas('share', function($query) use ($dep_id){
        $query->where('share_to',$dep_id);
        $query->where('share_to_code','pmis_progress');
      })->orderBy('id','desc')->paginate(10)->onEachSide(1);
    }
    else
    {
      $data['records'] = Plans::whereHas('share', function($query) use ($dep_id){
        $query->where('share_to',$dep_id);
        $query->where('share_to_code','pmis_progress');
      })->orderBy('id','desc')->paginate(10)->onEachSide(1);

    }
    // Load view to show result
    if($section=='contractor')
    {
      return view('settings/projects/contractor/list_filter',$data);
    }
    elseif($section=='staff')
    {
      return view('settings/projects/staff/list_filter',$data);
    }
    else{
      return view('settings/projects/list_filter',$data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create_staff($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Plan ID
    $plan_id = decrypt($enc_id);
    $data['plan_id'] = $enc_id;
    // contractor_id
    $con_id = Input::get('contractor_id');
    $contractor_id = decrypt($con_id);
    $data['contractor_id'] = $con_id;
    // Get Plan
    $data['plan'] = Plans::find($plan_id);
    // Get Contractor
    $data['contractor'] = Contractor::find($contractor_id);
    // Get Plan Details
    $data['location'] = Plan_locations::where('project_id',$plan_id)->get();
    // Get contractor's staff
    $data['users'] = Users::where('contractor_id',$contractor_id)->get();
    // Load view to show result
    return view('settings/projects/contractor/add_staff',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store_staff(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'contractor_id'   => 'required',
      'project_id'      => 'required',
      'location_id'     => 'required',
    ]);
    // Get Language
    $lang = get_language();
    // New Record
    $users_id = $request->user_id;
    if($users_id)
    {
      $data = array();
      foreach($users_id as $uid)
      {
        $user_data = array(
          'contractor_id'   => $request->contractor_id,
          'project_id'      => $request->project_id,
          'location_id'     => $request->location_id,
          'user_id'         => $uid,
          'created_at'      => date('Y-m-d H:i:s'),
          'created_by'      => Auth::user()->id,
        );
        array_push($data,$user_data);
      }
      $record = Project_contractor_staff::insert($data);
      if($record)
      {
          Session()->flash('success', __("global.success_msg"));
      }
      else
      {
          Session()->flash('fail', __("global.failed_msg"));
      }
    }
    else
    {
        Session()->flash('fail', __("global.failed_msg"));
    }
  }

  // Department Staff
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function bringProjectsStaff($dep_id)
  {
    if(check_my_auth_section('setting_projects'))
    {
      $lang = get_language();
      $data['lang'] = $lang;
      session(['current_section' => 'setting_projects']);
      session(['current_department' => $dep_id]);
      // Get Department ID
      $depID = decrypt($dep_id);
      $data['dep_id'] = $depID;
      // Get Department's Name
      $data['department_name'] = getDepartmentNameByID($depID,$lang);
      // Get projects of this department
      $data['records'] = Plans::whereHas('project_location', function($query){
        $query->where('status','0');
      })->whereHas('share', function($query) use ($depID){
        $query->where('share_to',$depID);
        $query->where('share_to_code','pmis_progress');
      })->orderBy('id','desc')->paginate(10)->onEachSide(1);
      // load view to show result
      if(Input::get('ajax') == 1)
      {
        return view('settings/projects/staff/list_ajax',$data);
      }
      else
      {
        return view('settings/projects/staff/list',$data);
      }
    }
    else
    {
      return view('access_denied');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_staff($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get data
    $data['records'] = ProjectDepartmentStaff::where('project_id',$id)->get();
    // Get project data
    $data['plan'] = Plans::find($id);
    // Load view to show result
    return view('settings/projects/staff/view',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function staff_add($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Project ID
    $id = decrypt($enc_id);
    // Get Plan
    $data['plan'] = Plans::find($id);
    // Get Plan Details
    $data['location'] = Plan_locations::where('project_id',$id)->get();
    // Department ID
    $dep_id = session('current_department');
    $department_id = decrypt($dep_id);
    $data['department_id'] = $department_id;
    // Get Department's Name
    $data['department_name'] = getDepartmentNameByID($department_id,$lang);
    // Get contractor's staff
    $data['users'] = Users::where('department_id',$department_id)->get();
    // Load view to show result
    return view('settings/projects/staff/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function staff_store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'department_id'   => 'required',
      'project_id'      => 'required',
      'location_id'     => 'required',
    ]);
    // Get Language
    $lang = get_language();
    // New Record
    $users_id = $request->user_id;
    if($users_id)
    {
      $data = array();
      foreach($users_id as $uid)
      {
        $user_data = array(
          'department_id'   => $request->department_id,
          'project_id'      => $request->project_id,
          'location_id'     => $request->location_id,
          'userid'          => $uid,
          'created_at'      => date('Y-m-d H:i:s'),
          'created_by'      => Auth::user()->id,
        );
        array_push($data,$user_data);
      }
      $record = ProjectDepartmentStaff::insert($data);
      if($record)
      {
          Session()->flash('success', __("global.success_msg"));
      }
      else
      {
          Session()->flash('fail', __("global.failed_msg"));
      }
    }
    else
    {
        Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function staff_destroy($id)
  {
    // Decrypt ID
    $id = decrypt($id);
    // Update record
    $record = ProjectDepartmentStaff::find($id);
    $id = $record->delete();
    if($id!=0)
    {
      Session()->flash('success', __("global.success_delete_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

}
