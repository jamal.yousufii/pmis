<?php
namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\models\Settings\Contractor;
use Response;

class ContractorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(check_my_auth_section('setting_contractor'))
        {
            session(['current_section' => "setting_contractor"]);
            $data['records'] = Contractor::orderBy('id', 'DESC')->paginate(10)->onEachSide(1);
            if(Input::get('ajax') == 1)
            {
                return view('settings.contractors.list_ajax',$data);
            }
            else
            {
                return view('settings.contractors.list',$data);
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settings.contractors.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'company_name'          => 'required',
            'primary_contact_name'  => 'required',
            'primary_contact_phone' => 'required',
            'primary_contact_email' => 'required',
        ]);
        // Get Language
        $lang = get_language();
        // Generate URN
        $urn = GenerateURN('contractors','urn');
        // New Survey
        $record = new Contractor;
        $record->urn                        = $urn;
        $record->company_name               = $request->company_name;
        $record->primary_contact_name       = $request->primary_contact_name;
        $record->primary_contact_phone      = $request->primary_contact_phone;
        $record->primary_contact_email      = $request->primary_contact_email;
        $record->alternative_contact_name   = $request->alternative_contact_name;
        $record->alternative_contact_phone  = $request->alternative_contact_phone;
        $record->alternative_contact_email  = $request->alternative_contact_email;
        $record->created_at = date('Y-m-d H:i:s');
        $record->created_by = Auth::user()->id;
        // Store data in database
        if($record->save())
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Get language
        $lang = get_language();
        $data['lang'] = $lang;
        // Dycript the id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get Record
        $data['record'] = Contractor::find($id);
        //ddd($data);
        // Load View
        return view('settings.contractors.view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        //
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get Record
        $data['record'] = Contractor::find($id);
        // Load View
        return view('settings.contractors.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {
        $validates = $request->validate([
            'company_name'          => 'required',
            'primary_contact_name'  => 'required',
            'primary_contact_phone' => 'required',
            'primary_contact_email' => 'required',
        ]);
        // Dycript the id
        $id = decrypt($enc_id);
        // Update data in database
        $result = Contractor::whereId($id)->update($request->except('_token'));
        if($result)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $result = Contractor::whereId($id)->delete();
        if($result)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_delete_msg"));
        }
    }

    public function filterContractors()
    {
        $item = Input::get('item');
        $data['keyWord'] = $item;
        if($item!="")
        {
            // Get data by keywords
            $data['records'] = Contractor::whereRaw("(company_name like '%".$item."%')")->paginate(10)->onEachSide(1);
        }
        else
        {
            // Get all data
            $data['records'] = Contractor::paginate(10)->onEachSide(1);
        }    
        // Load View to display records
        return view('settings.contractors.list_filter',$data); 
    }
}
