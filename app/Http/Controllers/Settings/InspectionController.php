<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\models\Settings\Inspection;
use App\models\Settings\InspectionSub;

class InspectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(check_my_auth_section('setting_inspection'))
        {
            $lang  = get_language();
            session(['current_section' => "setting_inspection"]);
            // Get All Work Inspection   
            $inspection = Inspection::orderBy('id', 'DESC')->get();
            if(Input::get('ajax') == 1)
            {
                return view('settings.inspection.list_ajax',compact('lang','inspection'));
            }
            else
            {
                return view('settings.inspection.list',compact('lang','inspection'));
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['lang'] = get_language(); 
        return view('settings.inspection.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // Validate the request...
        $validates = $request->validate([
            'name_dr' => 'required',
            'name_pa' => 'required',
            'name_en' => 'required',
        ]);
        $userid = Auth::user()->id;
        $record = new Inspection;
        $record->name_dr        = $request->name_dr;
        $record->name_pa        = $request->name_pa;
        $record->name_en        = $request->name_en;
        $record->created_by     = $userid;
        $inspection_sub = array();
        if($record->save()){
            if($request->name_sub_dr){
                for($i=0;$i<count($request->name_sub_dr);$i++)
                {
                    $inspection_sub[] = array(
                        'inspection_id'   => $record->id,
                        'name_dr'         => $request->name_sub_dr[$i],
                        'name_pa'         => $request->name_sub_pa[$i],
                        'name_en'         => $request->name_sub_en[$i],
                        'created_by'      => $userid,
                    );
                }
                InspectionSub::insert($inspection_sub);
            }

            Session()->flash('success', __("global.success_msg"));
        }
            
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Get language
        $lang = get_language();
        // Dycript the id
        $id = decrypt($enc_id);
        // Get Record
        $inspection    = Inspection::with('inspectionSub')->find($id);
        return view('settings.inspection.view',compact('lang','enc_id','inspection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        $lang = get_language();
        // Get all Work Category
        $id = decrypt($enc_id);
        $inspection    = Inspection::with('inspectionSub')->find($id);
        return view('settings.inspection.edit',compact('lang','enc_id','inspection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {
        $validates = $request->validate([
            'name_dr'          => 'required',
            'name_pa'          => 'required',
            'name_en'          => 'required',
        ]);
        // Dycript the id
        $id = decrypt($enc_id);
        $userid = Auth::user()->id; 
        // Update data in database
        $inspection_data = [
            'name_dr' => $request->name_dr,
            'name_pa' => $request->name_pa,
            'name_en' => $request->name_en,
        ];
        $result = Inspection::whereId($id)->update($inspection_data); 
        if($result)
        {
            // else data exist update it 
            for($i=0;$i<count($request->name_sub_dr);$i++)
            {
                // check if data exist update it 
                if(isset($request->inspectionSub_id[$i]))
                {
                    $inspection_sub = array(
                        'inspection_id'   => $id,
                        'name_dr'         => $request->name_sub_dr[$i],
                        'name_pa'         => $request->name_sub_pa[$i],
                        'name_en'         => $request->name_sub_en[$i],
                        'created_by'      => $userid,
                    );     
                    // Update record
                    InspectionSub::whereId($request->inspectionSub_id[$i])->update($inspection_sub);
                }
                else 
                {
                    $inspection_sub = array(
                        'inspection_id'   => $id,
                        'name_dr'         => $request->name_sub_dr[$i],
                        'name_pa'         => $request->name_sub_pa[$i],
                        'name_en'         => $request->name_sub_en[$i],
                        'created_by'      => $userid,
                    );     
                    InspectionSub::insert($inspection_sub);
                }
            }
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $result = Inspection::whereId($id)->delete();
        if($result)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_delete_msg"));
        }
    }

    public function filterInspection()
    {
        $lang = get_language();
        $data['lang'] = $lang;
        if(Input::get('item'))
        {
            $item = Input::get('item');
            // Get data by keywords
            $data['records'] = Inspection::whereRaw("(name_dr like '%".$item."%' OR name_pa like '%".$item."%' OR name_en like '%".$item."%')")->paginate(10);
        }
        else
        {
            // Get all data
            $data['records'] = Inspection::paginate(10);
        }    
        // Load View to display records
        return view('settings.inspection.list_filter',$data); 
    }

    public function more_inspection_sub(){

        // Get Language
        $data['lang'] = get_language();
        // Get Counter Number
        $data['number'] = Input::post('number');

        return view('settings.inspection.more_inspection_sub',$data);
    }
}
