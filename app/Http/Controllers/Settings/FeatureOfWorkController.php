<?php
namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Settings\Feature_of_work;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class FeatureOfWorkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(check_my_auth_section('setting_work_feature'))
        {
            $data['lang']   = get_language();
            session(['current_section' => "setting_work_feature"]);
            // Get All Work Categories   
            $data['records'] = Feature_of_work::orderBy('id', 'DESC')->paginate(10)->onEachSide(1);
            
            if(Input::get('ajax') == 1)
            {
                return view('settings.feature_of_work.list_ajax',$data);
            }
            else
            {
                return view('settings.feature_of_work.list',$data);
            }
        }
        else
        {
            return view('access_denied');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['lang'] = get_language(); 
        return view('settings.feature_of_work.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'feature_name'          => 'required',
            'feature_description'   => 'required',
        ]);
        $userid = Auth::user()->id;
        $record = new Feature_of_work;
        $record->name        = $request->feature_name;
        $record->description = $request->feature_description;
        $record->created_by  = $userid;
        $id = $record->save();
        if($id>0)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        $lang = get_language();
        // Get all Work Category
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        $data['record'] = Feature_of_work::find($id);
        return view('settings.feature_of_work.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {
        $validates = $request->validate([
            'name'          => 'required',
            'description'   => 'required',
        ]);
        // Dycript the id
        $id = decrypt($enc_id);
        // Update data in database
        $result = Feature_of_work::whereId($id)->update($request->except('_token'));
        if($result)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $result = Feature_of_work::whereId($id)->delete();
        if($result)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_delete_msg"));
        }
    }

    public function filterFeatureOfWork()
    {
        $lang = get_language();
        $data['lang'] = $lang;
        if(Input::get('item'))
        {
            $item = Input::get('item');
            // Get data by keywords
            $data['records'] = Feature_of_work::whereRaw("(name like '%".$item."%' OR description like '%".$item."%')")->paginate(10);
        }
        else
        {
            // Get all data
            $data['records'] = Feature_of_work::paginate(10);
        }    
        // Load View to display records
        return view('settings.feature_of_work.list_filter',$data); 
    }
}
