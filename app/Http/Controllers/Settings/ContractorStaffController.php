<?php
namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\models\Authentication\Users;
use App\models\Authentication\Departments;
use App\models\Settings\Contractor;
use App\Http\Requests\UsersRequest; 
use App\models\Authentication\UserDepartments;
use App\models\Authentication\UserSections;
use App\models\Authentication\ModuleUsers;
use App\models\Authentication\UserRole;
use App\models\Authentication\Sections;
use App\models\Authentication\Modules;

use DB;

class contractorStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(check_my_auth_section('setting_staff'))
        {
            session(['current_section' => "setting_staff"]);
            $lang = get_language();
            // Get All Staffs From Users   
            $records = Users::where("contractor_staff",'1')->paginate(10);
            if(Input::get('ajax')==1)
            {
                //load view to show searchpa result
                return view('settings.contractor_staff.list_ajax',compact('records','lang'));
            }
            else
            {
                //load view to show searchpa result
                return view('settings.contractor_staff.list',compact('records','lang'));
            }
        }
        else
        {
            return view('access_denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = get_language();
        // Get all Contractors
        $contractor = Contractor::orderBy('id','DESC')->get();
        // Get all Departments
        $departments = Departments::select('id','name_'.$lang.' as name')->where('code','dep_06')->get();
        // Load view to display data
        return view('settings.contractor_staff.add',compact('contractor','departments','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'department_id'     => 'required',
            'contractor_id'     => 'required',
            'name'              => 'required',
            'email'             => 'required',
            'password'          => 'required',
            'confirm_password'  => 'required',
        ]);
        $password = Hash::make($request->password);
        $userid = Auth::user()->id;
        $record = new Users;
        $record->department_id      = $request->department_id;
        $record->name               = $request->name;
        $record->father             = $request->father;
        $record->email              = $request->email;
        $record->position           = $request->position;
        $record->password           = $password;
        $record->contractor_id      = $request->contractor_id;
        $record->contractor_staff   = '1';
        $record->profile_pic        = "default.png";
        $record->phone_number       = $request->phone_no;
        $record->created_by         = $userid;
        $id = $record->save();
        if($id>0)
        {

            //INSERT USER DEPARTMENT
            $data = array(
                'user_id'       =>  $record->id,
                'department_id' =>  $request->department_id,
                'created_at'    =>  date('Y-m-d H:i:s'),
            );
            UserDepartments::insert($data);
            //INSERT USER modules
            $modules = $request->modules;
            $app_data = array(
                'user_id'       =>  $record->id,
                'module_id'     =>  $modules,
                'department_id' =>  $request->department_id,
                'created_at'    =>  date('Y-m-d H:i:s'),
            );
            ModuleUsers::insert($app_data);

            //INSERT USER SECTIONS
            $sections = $request->sections;
            $data = array();
            if($sections)
            {
                foreach($sections as $sec)
                {
                    $sec_data = array(
                        'user_id'       =>  $record->id,
                        'section_id'    =>  $sec,
                        'department_id' =>  $request->department_id,
                        'created_at'    =>  date('Y-m-d H:i:s'),
                    );
                    array_push($data,$sec_data);
                }
                if(COUNT($data)>0)
                {
                    UserSections::insert($data);
                }
            }
            //INSERT USER ROLES
            $roles = $request->roles;
            $data = array();
            if($roles)
            {
                foreach($roles as $rol)
                {
                    $rol_data = array(
                        'user_id'       =>  $record->id,
                        'role_id'       =>  $rol,
                        'department_id' =>  $request->department_id,
                        'created_at'    =>  date('Y-m-d H:i:s'),
                    );
                    array_push($data,$rol_data);
                }
                if(COUNT($data)>0)
                {
                    UserRole::insert($data);
                }
            }
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id)
    {
        $lang = get_language();
        // Get all Contractors
        $contractor = Contractor::select('id','company_name')->get();
        //Decrypt the id
        $id = decrypt($enc_id);
        $record = Users::find($id);
        $dep_id = $record->department_id;
        // Get modules
        $modules = Users::getModulesByDepartment($dep_id);
        // Get selected modules
        $selectedMods = getUserModulesByDepartment($id,$dep_id);
        //Sections
        $sections = Users::getContractorSectionsByModule($selectedMods);
        // Get selected sections
        $selectedSec = getUserSectionsByModules($id,$dep_id);
        //Roles
        $roles = Users::getRolesBySections($selectedSec,$lang);
        // Get selected roles
        $selectedRol = getUserRolesBySections($id,$dep_id);
        // Get all Departments
        $departments = Departments::select('id','name_'.$lang.' as name')->where('code','dep_06')->get();
        // Load view to display data
        return view('settings.contractor_staff.edit',compact('contractor','enc_id','record','modules','selectedMods','sections','selectedSec','roles','selectedRol','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $enc_id)
    {
        // Validate the request...
        $validates = $request->validate([
            'department_id'     => 'required',
            'contractor_id'     => 'required',
            'name'              => 'required',
            'email'             => 'required',
        ]);
        
        $id = decrypt($enc_id);
        $record = Users::find($id);
        $record->department_id  = $request->department_id;
        $record->contractor_id  = $request->contractor_id;
        $record->name           = $request->name;
        $record->father         = $request->father;
        $record->email          = $request->email;
        $record->position       = $request->position;
        $record->phone_number   = $request->phone_no;
        // Encrypt User's Password
        if($request->password)
        {
            $record->password = Hash::make($request->password);
        }
        $record->updated_by     = userid();
        $updated = $record->save();
        if($updated>0)
        {
            $modules = $request->modules;
            $data = array();
            if($modules)
            {
                // Delete user module old records
                ModuleUsers::where('user_id',$id)->where('department_id',$request->department_id)->delete();
                $app_data = array(
                    'user_id'       =>  $record->id,
                    'module_id'     =>  $modules,
                    'department_id' =>  $request->department_id,
                    'created_at'    =>  date('Y-m-d H:i:s'),
                );
                // Insert user module new records
                ModuleUsers::insert($app_data);

                //INSERT USER SECTIONS
                $sections = $request->sections;
                $data = array();
                if($sections)
                {
                    foreach($sections as $sec)
                    {
                        $sec_data = array(
                            'user_id'       =>  $record->id,
                            'section_id'    =>  $sec,
                            'department_id' =>  $request->department_id,
                            'created_at'    =>  date('Y-m-d H:i:s'),
                        );
                        array_push($data,$sec_data);
                    }
                    if(COUNT($data)>0)
                    {
                        // Delete user section old records
                        UserSections::where('user_id',$id)->where('department_id',$request->department_id)->delete();
                        // Insert user section new records
                        UserSections::insert($data);
                    }
                }
                //INSERT USER ROLES
                $roles = $request->roles;
                $data = array();
                if($roles)
                {
                    foreach($roles as $rol)
                    {
                        $rol_data = array(
                            'user_id'       =>  $record->id,
                            'role_id'       =>  $rol,
                            'department_id' =>  $request->department_id,
                            'created_at'    =>  date('Y-m-d H:i:s'),
                        );
                        array_push($data,$rol_data);
                    }
                    if(COUNT($data)>0)
                    {
                        // Delete user role old records
                        UserRole::where('user_id',$id)->where('department_id',$request->department_id)->delete();
                        // Insert user role new records
                        UserRole::insert($data);
                    }
                }
            }
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Get language
        $lang = get_language();
        // Dycript the id
        $id = decrypt($enc_id);
        // Get Record
        $record = Users::find($id);
        $department_id = $record->department_id;

        // Get modules
        $modules = Users::getModulesByDepartment($department_id);
        // Get selected modules
        $selectedMods = getUserModulesByDepartment($id,$department_id);
        //Sections
        $sections = Users::getContractorSectionsByModule($selectedMods);
        // Get selected sections
        $selectedSec = getUserSectionsByModules($id,$department_id);
        //Roles
        $roles = Users::getRolesBySections($selectedSec,$lang);
        // Get selected roles
        $selectedRol = getUserRolesBySections($id,$department_id);
        // Load view to display data
        return view('settings.contractor_staff.view',compact('record','modules','selectedMods','sections','selectedSec','roles','selectedRol','enc_id','lang'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($enc_id)
    {
        // Dycript the id
        $id = decrypt($enc_id);
        // Delete Data from table
        $result = Users::whereId($id)->delete();
        if($result)
        {
            Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_delete_msg"));
        }
    }

    function filter_contractor_staff()
    {
        $lang = get_language();
        if(Input::get('item'))
        {
            $item = Input::get('item');
            // Get data by keywords
            $records = Users::where('contractor_staff','1')->whereRaw("(users.name like '%".$item."%' OR users.father like '%".$item."%')")->paginate(10);
        }
        else
        {
            // Get all data
            $records = Users::where("contractor_staff",'1')->paginate(10);
        }    
        // Load View to display records
        return view('settings.contractor_staff.list_filter',compact('records','lang')); 
    }

    public function modulesByDepartment()
    {
        $id = Input::post('id');
        if($id)
        {
            // Get modules
            $modules = Users::getModulesByDepartment($id);
            return view('settings/contractor_staff/modules',compact('modules'));
        }
        else 
        {
            return "";
        }
    }

    public function sectionsByModule()
    {
        $lang = get_language();
        $id = Input::post('modules');
        if($id)
        {
            // Get sections
            $sections = Users::getContractorSectionsByModule(array($id));
            return view('settings/contractor_staff/sections',compact('sections'));
        }
        else 
        {
            return "";
        }
    }

    public function rolesBySections()
    {
        $lang = get_language();
        $id = Input::post('sections');
        if($id)
        {
            // Get roles
            $roles = Users::getRolesBySections($id,$lang);
            return view('settings/contractor_staff/roles',compact('roles'));
        }
        else 
        {
            return "";
        }
    }
}
?>