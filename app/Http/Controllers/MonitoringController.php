<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\Requests;
use App\models\Plans;
use App\models\Daily_report;
use App\models\DailyReportStatusLog;
use App\models\Plan_locations;
use App\User;
use App\models\Authentication\Users;
use App\models\Authentication\Departments;
use Redirect;

class MonitoringController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depId = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depId),'daily_report_monitoring'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => 'daily_report_monitoring']);
        // Get language
        $data['lang'] = get_language();
        // Get all data
        $data['records'] = Plans::whereHas('share', function($query) use ($depId){
                                    $query->where('share_to',$depId);
                                    $query->where('share_to_code','pmis_progress');
                            })->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })->orderBy('id','desc')->paginate(10)->onEachSide(1);
        $data['segments'] = array(
            '0' => 'daily_report_monitoring/index/',
            '1' => $dep_id
        );
        if(Input::get('ajax') == 1)
        {
            return view('pmis/monitoring/list_ajax',$data);
        }
        else
        {
            return view('pmis/monitoring/list',$data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Language
        $lang = get_language();
        // Project id
        $project_id = $request->project_id;
        // Report id
        $report_id  = $request->report_id;
        // Insert Record
        $record = Daily_report::find($report_id);
        $record->status              = $request->status;
        $record->status_by           = userid();
        $record->status_date         = dateProvider($request->status_date,$lang);
        $record->description         = $request->description;
        $record->save();
        if($record->id>0)
        {
            // Insert Log Record
            $log = new DailyReportStatusLog;
            $log->report_id     = $report_id;
            $log->status        = $request->status;
            $log->status_by     = userid();
            $log->status_date   = dateProvider($request->status_date,$lang);
            $log->description   = $request->description;
            $log->created_at    = date('Y-m-d H:i:s');
            $log->save();

            // Start Notification
            $rec_owner = $record->created_by;
            $owner = Users::find($rec_owner);
            // Get Contractor Department
            $dep_id = Departments::where('code','dep_06')->first()->id;
            $users = User::whereId($rec_owner)->get();
            $redirect_url = route('daily_report/notifications',['id'=>encrypt($report_id),'con_id'=>encrypt($owner->contractor_id),'dep_id'=>encrypt($dep_id)]);
            foreach($users as $user) {
                if($request->status==1)
                {
                    $user->notify(new NewProject($report_id, trans('global.report_approved'), userid(), $redirect_url));
                }
                else
                {
                    $user->notify(new NewProject($report_id, trans('global.report_rejected'), userid(), $redirect_url));
                }
            }
            // End Notification

            Session()->flash('success', __("global.status_success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list($id)
    {
        // Get Language
        $lang = get_language();
        // Get Project ID
        $enc_project_id = $id;
        $project_id = decrypt($enc_project_id);
            // Get plan record
        $plan = Plans::find($project_id);
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
        session(['current_section' => 'daily_report_monitoring']);
        // Department id
        $dep_id = session('current_department');            
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $locations = Plan_locations::whereHas('projects', function($query) use ($project_id, $dep_id){
                $query->where('id',$project_id);
                $query->where('department',decrypt($dep_id));
            })->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $locations = Plan_locations::whereHas('department_staff', function($query) use ($project_id, $dep_id){
                $query->where('userid',userid());
                $query->where('project_id',$project_id);
                $query->where('department_id',decrypt($dep_id));
            })->orderBy('id','desc')->get();
        }
        // Load view to show result
        return view('pmis/monitoring/index',compact('lang','plan','summary','enc_project_id','dep_id','locations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$dep_id,$loc_id)
    {
        // Get Language
        $lang = get_language();
        // Get Department ID
        $enc_project_id = $id;
        $project_id = decrypt($enc_project_id);
            // Get plan record
        $plan = Plans::find($project_id);
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
        session(['current_section' => 'daily_report_monitoring']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $locations = Plan_locations::whereHas('projects', function($query) use ($project_id, $dep_id){
                $query->where('id',$project_id);
                $query->where('department',decrypt($dep_id));
            })->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            // Get Project Locations by Users
            $locations = Plan_locations::whereHas('department_staff', function($query) use ($project_id, $dep_id){
                $query->where('userid',userid());
                $query->where('project_id',$project_id);
                $query->where('department_id',decrypt($dep_id));
            })->orderBy('id','desc')->get();
        }
        $daily_report = array();
        $records = Daily_report::where('project_id',$project_id)->where('project_location_id',$loc_id)->where('completed','1')->orderBy('id','DESC')->paginate(10);
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('pmis/monitoring/show_ajax',compact('lang','plan','summary','enc_project_id','dep_id','locations','records','daily_report','loc_id'));
        }
        else
        {
            // Load view to show result
            return view('pmis/monitoring/show',compact('lang','plan','summary','enc_project_id','dep_id','locations','records','daily_report','loc_id'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request,$id)
    {
        // Project id
        $project_id = $request->project_id;
        // Project id
        $dec_id = decrypt($id);
        // Set Session
        session(['report_id' => $dec_id]);
        // Get Language
        $lang = get_language();
        // Get daily report
        $daily_report = Daily_report::find($dec_id);
        // Get daily report status log
        $daily_report_log = DailyReportStatusLog::where('report_id',$dec_id)->orderBy('id','DESC')->get();
        // Load the view to display data
        return view('pmis/monitoring/location',compact('project_id','lang','daily_report','daily_report_log'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_content(Request $request)
    {
        // Project id
        $project_id = $request->project_id;
        // Section code
        $code = $request->code;
        // Report id
        $dec_id = session('report_id');
        // Get Language
        $lang = get_language();
        // Get daily report
        $daily_report = Daily_report::find($dec_id);
        // Load the view to display data
        if($code=='daily_location'){
            $daily_report_log = DailyReportStatusLog::where('report_id',$dec_id)->orderBy('id','DESC')->get();
            return view('pmis/monitoring/location',compact('project_id','lang','daily_report','daily_report_log'));
        }elseif($code=='weather'){
            return view('pmis/monitoring/weather',compact('project_id','lang','daily_report'));
        }elseif($code=='qc_narratives'){
            return view('pmis/monitoring/qc_narratives',compact('project_id','lang','daily_report'));
        }elseif($code=='daily_qc_test'){
            return view('pmis/monitoring/qc_test',compact('project_id','lang','daily_report'));
        }elseif($code=='daily_activities'){
            return view('pmis/monitoring/activities',compact('project_id','lang','daily_report'));
        }elseif($code=='labor_list'){
            return view('pmis/monitoring/labor',compact('project_id','lang','daily_report'));
        }elseif($code=='daily_equipment'){
            return view('pmis/monitoring/equipment',compact('project_id','lang','daily_report'));
        }elseif($code=='daily_accidents'){
            return view('pmis/monitoring/accidents',compact('project_id','lang','daily_report'));
        }
    }

    public function filter_monitoring(Request $request)
    {    
        // Get Language
        $lang = get_language();
        // Get department
        $dep_id = decrypt($request->department_id);
        // Get section code
        $section = $request->section;
        // Search Shared Plan 
        $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$dep_id,$section);
        // Load View to display records
        return view('pmis/monitoring/list_filter',compact('records','lang','request','dep_id'));
    }
}
?>