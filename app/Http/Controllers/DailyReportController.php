<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use App\models\Plans;
use App\models\Billofquantities;
use App\models\Daily_activities;
use App\models\DailyLabour;
use App\models\DailyEquipments;
use App\models\Daily_report;
use App\models\DailyReportStatusLog;
use App\models\Requests;
use App\models\DailyMainProblem;
use App\Http\Requests\dailyWeatherRequest;
use App\Http\Requests\qcNarrativeRequest;
use App\Http\Requests\qctestRequest;
use App\Http\Requests\LaborRequest;
use App\Http\Requests\EquipmentRequest;
use App\Http\Requests\AccidentsRequest;
use App\Http\Requests\ActivityRequest;
use App\models\Plan_locations;
use App\models\Static_data;
use App\models\Settings\Statics\Labor_clasification;
use App\models\Settings\Statics\Machinery;
use App\models\DailyWeather;
use App\models\DailyTest;
use App\models\DailyAccidents;
use App\models\Procurement;
use App\models\DailyPhotos;

class DailyReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id=0)
    {
        // Get Language
        $lang = get_language();
        // Project id
        $project_id = decrypt($project_id);
        // Get plan record
        $plan = Plans::find($project_id);
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
        session(['extra_section' => 'for_project']);
        session(['current_section' => 'pmis_daily_report']);
        session(['project_id' => $project_id]);
        // Department id
        $dep_id = session('current_department');
        // Contractor id
        $con_id = session('current_contractor');
        $current_tab = 'weather';
        // Get Project Locations by Users
        $locations = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $con_id){
            $query->where('user_id',userid());
            $query->where('project_id',$project_id);
            $query->where('contractor_id',decrypt($con_id));
        })->where('status','0')->orderBy('id','desc')->get();
        $records = Daily_report::where('project_id',$project_id)->orderBy('id','DESC')->paginate(10);
        // Destroy sessions
        session()->forget('report_id');
        session()->forget('report_date');
        session()->forget('project_location_id');
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('pmis/contractor_project/daily_report/index_ajax',compact('lang','plan','summary','project_id','dep_id','con_id','locations','records'));
        }
        else
        {
            // Load view to show result
            return view('pmis/contractor_project/daily_report/index',compact('lang','plan','summary','project_id','dep_id','con_id','locations','records'));
        }
    }

    public function list_notification($id,$con_id,$dep_id)
    {
        // Report id
        $report_id = decrypt($id);
        // Get records
        $records = Daily_report::whereId($report_id)->first();
        // Project id
        $project_id = $records->project_id;
        // Get Language
        $lang = get_language();
        // Get plan record
        $plan = Plans::find($project_id);
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
        session(['extra_section' => 'for_project']);
        session(['current_section' => 'pmis_daily_report']);
        // Department id
        $dep_id = decrypt($dep_id);
        // Contractor id
        $con_id = decrypt($con_id);
        $current_tab = 'weather';
        // Get Project Locations by Users
        $locations = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $con_id){
            $query->where('user_id',userid());
            $query->where('project_id',$project_id);
            $query->where('contractor_id',$con_id);
        })->where('status','0')->orderBy('id','desc')->get();
        // Destroy sessions
        session()->forget('report_id');
        session()->forget('report_date');
        session()->forget('project_location_id');
        // Load view to show result
        return view('pmis/contractor_project/daily_report/index_notification',compact('lang','plan','summary','project_id','dep_id','con_id','locations','records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get Language
        $lang = get_language();
        $project_id = session('project_id');
        // Get plan record
        $plan = Plans::find($project_id);
        // Get summary data
        $summary = Requests::whereId($plan->request_id)->first();
        return view('pmis/contractor_project/daily_report/create',compact('plan','summary','lang','project_id')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function location(Request $request)
    {
        // Project id
        $project_id = $request->project_id;
        // Report id 
        $report_id  = ($request->report_id!=''? $request->report_id: session('report_id')); 
        
        // Department id
        $dep_id = session('current_department');
        // Contractor id
        $con_id = session('current_contractor');
        // Get Language
        $lang = get_language();
        // Get daily report
        $daily_report = Daily_report::find($report_id);
        // Get daily report status log
        $daily_report_log = DailyReportStatusLog::where('report_id',$report_id)->orderBy('id','DESC')->get();
        if($daily_report)
        {
            $project_location_id = $daily_report->project_location_id; 
            // Set Session
            session(['report_id' => $daily_report->id]);
            session(['report_date' => $daily_report->report_date]);
            session(['project_location_id' => $daily_report->project_location_id]);
        }
        else
        {
            $project_location_id = 0; 
        }
        // Get Project Locations by Users
        $locations = Plan_locations::whereHas('contractor_staff', function($query) use ($project_id, $con_id){
            $query->where('user_id',userid());
            $query->where('project_id',$project_id);
            $query->where('contractor_id',decrypt($con_id));
        })->orderBy('id','desc')->get();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Load the view
        return view('pmis/contractor_project/daily_report/location',compact('locations','lang','project_id','daily_report','project_location_id','daily_report_log','approval'));
    }

    public function locationStore(Request $request)
    {
        // Language
        $lang = get_language();
        // Generate URN
        $urn = GenerateURN('daily_reports','urn');
        // Insert Record
        $record = new Daily_report;
        $record->urn                 = $urn;
        $record->project_id          = $request->project_id;
        $record->project_location_id = $request->location_id;
        $record->report_date         = dateProvider($request->report_date,$lang);
        $record->created_at          = date('Y-m-d H:i:s');
        $record->created_by          = userid();
        $record->save();
        if($record->id>0)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Set Session
        session(['report_id' => $record->id]);
        session(['report_date' => $record->report_date]);
        session(['project_location_id' => $record->project_location_id]);
        // Get daily report
        $daily_report = $record;
        //load view to display result
        return view('pmis/contractor_project/daily_report/location_view',compact('lang','daily_report'));
    }

    /** 
     * @Date: 2020-02-29 14:53:10 
     * @Desc: Updat the location 
     */
    public function locationUpdate(Request $request)
    {
        // Language
        $lang = get_language();
        // daily record 
        $daily_report = Daily_report::find($request->report_id);
        $daily_report->project_location_id = $request->location_id;
        $daily_report->report_date = dateProvider($request->report_date,$lang);
        $daily_report->completed = '0'; 
        $daily_report->save();  

         // Set Session
        session(['report_id' => $daily_report->id]);
        session(['report_date' => $daily_report->report_date]);
        session(['project_location_id' => $daily_report->project_location_id]);

        if($daily_report)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        //load view to display result
        return view('pmis/contractor_project/daily_report/location_view',compact('lang','daily_report'));
    }

    /** 
     * @Date: 2020-02-16 13:21:48 
     * @Desc: create form or edit form 
     */
    public function weather(Request $request)
    {
         // Get Language
        $lang = get_language();
        // Project id
        $project_id = session('project_id');
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Report location id
        $project_location_id = session('project_location_id');
        // Get project locations
        $location = Plan_locations::with('province')->find($project_location_id);   
        // Get daily report
        $daily_report = Daily_report::with('weather')->find($report_id); 
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the weather 
        $weather = DailyWeather::where('daily_report_id',$report_id)->first();
        // Load view 
        return view('pmis/contractor_project/daily_report/weather',compact('lang','project_location_id','report_date','daily_report','location','approval','weather')); 
    }

    /** 
     * @Date: 2020-02-23 12:15:32 
     * @Desc: Store Weather 
     */
    public function storeWeather(dailyWeatherRequest $request)
    {
        // Report id
        $report_id = session('report_id');
        $request['daily_report_id'] = $report_id;
        // Get Language
        $lang = get_language();
        // Store the weather 
        $weather = DailyWeather::create($request->except(['report_date','project_location_id']));
        // Get daily report
        if($weather)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        return view('pmis/contractor_project/daily_report/weather_view',compact('weather','lang')); 
    }

    /** 
     * @Date: 2020-03-01 09:48:42 
     * @Desc: Update Daily Weather  
     */
    public function updateWeather(Request $request)
    {
        // Get Language
        $lang = get_language();
        // weather id 
        $id = decrypt($request->id);
        // Update the weather 
        $weather = DailyWeather::find($id); 
        $weather->update($request->except('id')); 
        if($weather)
        {
            // Update daily report table 
            Daily_report::whereId($weather->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        return view('pmis/contractor_project/daily_report/weather_view',compact('weather','lang')); 
    }

    /** 
     * @Date: 2020-02-16 13:27:21 
     * @Desc: QC Narrative Create or Update 
     */
    public function qcNarrative(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Project id 
        $project_id = session('project_id');
         // Daily report id 
        $report_id   = session('report_id');
        // report date 
        $report_date = session('report_date');
        //project location id 
        $project_location_id = session('project_location_id');
        // Get project locations
        $location = Plan_locations::with('province')->find($project_location_id);   
        // Get daily report
        $daily_report = Daily_report::with('narrative')->find($report_id);
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the main problems 
        $narrative = DailyMainProblem::where('daily_report_id',$report_id)->first();
        // Load View
        return view('pmis/contractor_project/daily_report/qc_narrative',compact('lang','location','report_date','project_location_id','daily_report','approval','narrative')); 
    }

    /** 
     * @Date: 2020-02-26 11:14:27 
     * @Desc:  
     */
    public function qcNarrativeStore(qcNarrativeRequest $request)
    {
        $request['created_by'] = userid(); 
        // Report id
        $report_id = session('report_id');
        $request['daily_report_id'] = $report_id;
        $request['is_worked_stoped'] = ($request->is_worked_stoped=='on'? 1 : 0);
        $qc_narrative = DailyMainProblem::create($request->except(['report_date','project_location_id']));
        // Store data in database
        if($qc_narrative)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Load View
        return view('pmis/contractor_project/daily_report/qc_narrative_view',compact('qc_narrative')); 
    }

    /** 
     * @Date: 2020-03-01 10:56:59 
     * @Desc: Update QC Narrative   
     */
    public function qcNarrativeUpdate(qcNarrativeRequest $request)
    {
        $id = decrypt($request->id); 
        $request['is_worked_stoped'] = ($request->is_worked_stoped=='on'? '1' : '0');
        $qc_narrative = DailyMainProblem::find($id);
        $qc_narrative->update($request->except('id'));
        // Store data in database
        if($qc_narrative)
        {
            // Update daily report table 
            Daily_report::whereId($qc_narrative->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Load View
        return view('pmis/contractor_project/daily_report/qc_narrative_view',compact('qc_narrative'));         
    }

    /** 
     * @Date: 2020-02-17 14:37:55 
     * @Desc: QC Test   
     */
    public function qcTest(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Project id
        $project_id = session('project_id');
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Project Location id
        $project_location_id = session('project_location_id');
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get Record
        $bill_quantity = Billofquantities::where('project_id',$project_id)->where('project_location_id',$project_location_id)->where('started',1)->get();
        // Get daily report
        $daily_report = Daily_report::with('test')->find($report_id);
        // Get all statics
        $status = Static_data::where('type',9)->get();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the tests
        $test = DailyTest::where('daily_report_id',$report_id)->get();
        // Load the view
        return view('pmis/contractor_project/daily_report/qc_test',compact('lang','bill_quantity','location','report_date','project_location_id','daily_report','approval','test')); 
    }

    /**
     * @Date: 2020-02-20 09:34:50 
     * @Desc: List Labor  
     */
    public function qcTestStore(qctestRequest $request)
    {
        // Get Language
        $lang = get_language();
        $request['created_by'] = userid(); 
        // Report id
        $report_id = session('report_id');
        $request['daily_report_id'] = $report_id;
        $request['is_completed'] = ($request->is_completed=='on'? 1 : 0);
        $qc_test = DailyTest::create($request->except(['report_date','project_location_id']));
        // Store data in database
        if($qc_test)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Get daily report
        $daily_report = Daily_report::with('test')->where('id',$report_id)->first();
        // Get the tests
        $test = DailyTest::where('daily_report_id',$report_id)->get();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Load View
        return view('pmis/contractor_project/daily_report/qc_test_list',compact('daily_report','lang','test','approval')); 
    }

    /** 
     * @Date: 2020-03-02 11:51:16 
     * @Desc: Edit QC Test   
     */
    public function qcTestEdit(Request $request)
    {
         // Get Language
        $lang = get_language();
        // test id 
        $id = $request->id; 
        // Daily report 
        $daily_test = DailyTest::with('dailyReport')->find($id); 
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($daily_test->dailyReport->project_location_id);
        // Get Record
        $bill_quantity = Billofquantities::where('project_id',$daily_test->dailyReport->project_id)->where('project_location_id',$daily_test->dailyReport->project_location_id)->where('started',1)->get();
        // Get all statics
        $status = Static_data::where('type',9)->get();
        // Load the view
        return view('pmis/contractor_project/daily_report/qc_test_edit',compact('lang','bill_quantity','location','daily_test')); 

    }

    /** 
     * @Date: 2020-03-02 11:51:16 
     * @Desc: Edit QC Test   
     */
    public function qcTestUpdate(qctestRequest $request)
    {
         // Get Language
        $lang = get_language();
        // test id 
        $id = decrypt($request->id); 
        $request['is_completed'] = ($request->is_completed=='on' ? 'completed' : 'waiting');  
        // Find the daily test 
        $daily_test = DailyTest::find($id); 
        // Update the daily report 
        $daily_test->update($request->except('id')); 
        // Store data in database
        if($daily_test)
        {
            // Update daily report table 
            Daily_report::whereId($daily_test->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Get Daily report and test 
        $daily_report = Daily_report::with('test')->find($daily_test->daily_report_id); 
        // Get the tests
        $test = DailyTest::where('daily_report_id',$daily_test->daily_report_id)->get();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Load View
        return view('pmis/contractor_project/daily_report/qc_test_list',compact('daily_report','lang','test','approval')); 

    }

    /** 
     * @Date: 2020-03-03 11:20:17 
     * @Desc: Delte Daily test   
     */
    public function qcTestDelete(Request $request)
    {
        $id = decrypt($request->id); 
        $qctest_delete = DailyTest::where('id',$id)->delete(); 
        if($qctest_delete)
        {
          Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

        return view('layouts.components.alert'); 
    }
 
    /** 
     * @Date: 2020-02-16 13:21:48 
     * @Desc: create form or edit form 
     */
    public function activities(Request $request)
    {
        // Project id
        $id = $request->project_id;
        // Report id
        $report_id = session('report_id');
        // Project Location id
        $project_location_id = session('project_location_id');
        // Report date
        $report_date = session('report_date');
        // Contractor id
        $con_id = session('current_contractor');
        // Get daily report
        $daily_report  = Daily_report::with('activities')->where('id',$report_id)->first();
        $procurement  = Procurement::where('project_id',$id)->first();
        // Get Bill of Quantity Record
        $BQRecord = Billofquantities::where('project_id',$id)->where('project_location_id',$project_location_id)->where('started',1)->get();
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get Language
        $lang = get_language();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the activities 
        $activities = Daily_activities::where('daily_report_id',$report_id)->get();
        // Load the view
        return view('pmis/contractor_project/daily_report/activities',compact('report_id','project_location_id','report_date','daily_report','BQRecord','location','lang','approval','activities','procurement'));
    }

    public function activitiesStore(ActivityRequest $request)
    {
        // Project id
        $project_id = session('project_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Report id
        $report_id = session('report_id');
        // Language
        $lang = get_language();
        $activity = Daily_activities::where('daily_report_id',$request->daily_report_id)->where('bq_id',$request->bq_id)->first();
        if(!$activity)
        {
            // Insert Record
            $request['created_by'] = userid(); 
            $activity = Daily_activities::create($request->all()); 
        }
        else
        {
            // Updated Record
            $activity = Daily_activities::find($activity->id);
            $request['amount'] = $request->amount+$activity->amount;
            $activity = $activity->update($request->all()); 
        }
        if($activity)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        $procurement  = Procurement::where('project_id',$project_id)->first();
        // Get daily report
        $daily_report  = Daily_report::with('activities')->where('id',$report_id)->first();
        //load view to display result
        return view('pmis/contractor_project/daily_report/activities_view',compact('daily_report','lang','procurement'));
    }

    /** 
     * @Date: 2020-03-10 13:28:25 
     * @Desc: Update Activities   
     */
    public function activitiesEdit(Request $request)
    {
        // Get Language
        $lang = get_language();
        //Activity ID 
        $id = $request->id; 
        // Project id 
        $project_id = session('project_id');
         // Project Location id
        $project_location_id = session('project_location_id');
        // Get activity with daily report 
        $activity = Daily_activities::with(['dailyReport','unit'])->find($id);
        // Get Bill of Quantity Record
        $bq = Billofquantities::where('project_id',$project_id)->where('project_location_id',$project_location_id)->where('started',1)->get();
        // activities amount 
        $activity_amount = Daily_activities::where('bq_id',$activity->bq_id)->sum('amount');
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($activity->dailyReport->project_location_id);
        // Load view to display data
        return view('pmis/contractor_project/daily_report/activities_edit',compact('bq','activity','location','lang','activity_amount')); 
    }

    /** 
     * @Author: Jamal Yousufi   
     * @Date: 2020-03-11 14:51:39 
     * @Desc: Update Daily Activity   
     */
    public function activitiesUpdate(ActivityRequest $request)
    {
        // Get Language
        $lang = get_language();
        // Activity id 
        $id = decrypt($request->id);
        // Find the activity 
        $activity  = Daily_activities::find($id);
        // Update the activity 
        $activity_update = $activity->update($request->except('id'));
        // Get daily report
        $daily_report  = Daily_report::with('activities')->where('id',$activity->daily_report_id)->first();
        if($activity_update)
        {
            // Update daily report table 
            Daily_report::whereId($activity->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Project id
        $project_id = session('project_id');
        $procurement  = Procurement::where('project_id',$project_id)->first();
        //load view to display result
        return view('pmis/contractor_project/daily_report/activities_view',compact('lang','daily_report','procurement'));
    }

    /** 
     * @Date: 2020-03-12 13:35:06 
     * @Desc:  
     */
    public function activitiesDelete(Request $request)
    {
        $id = decrypt($request->id); 
        $qctest_delete = Daily_activities::where('id',$id)->delete(); 
        if($qctest_delete)
        {
          Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

        return view('layouts.components.alert');         
    }

    public function bringUnit()
    {
        $lang= get_language();
        $id = Input::post('id');
        $element_name = Input::post('el_name');
        $element = $element_name;
        //Get Unit by Bill of Quantity ID
        $BQRecord = Billofquantities::whereId($id)->first();
        // Load View  
        return view('pmis/contractor_project/daily_report/bringUnit',compact('BQRecord','element','lang'));
    }

    public function bringAmount()
    {
        $lang = get_language();
        $id = Input::post('id');
        $element_name = Input::post('el_name');
        $element = $element_name;
        //Get Unit by Bill of Quantity ID
        $BQRecord = Billofquantities::whereId($id)->first();
        // Get Activity Record
        $activity = Daily_activities::where('bq_id',$id)->sum('amount');
        // Load View  
        return view('pmis/contractor_project/daily_report/bringAmount',compact('activity','BQRecord','element','lang'));
    }

    /**
     * @Date: 2020-02-20 09:34:50 
     * @Desc: List Labor  
     */
    public function labor(Request $request)
    {
        // Project id
        $id = $request->project_id;
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Project Location id
        $project_location_id = session('project_location_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Get daily report
        $daily_report = Daily_report::with('labour')->where('id',$report_id)->first();
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get all statics
        $labor = Labor_clasification::all();
        // Get Language
        $lang = get_language();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the laborer 
        $laborer = DailyLabour::where('daily_report_id',$report_id)->get();
        // Load the view
        return view('pmis/contractor_project/daily_report/labor',compact('report_id','report_date','project_location_id','daily_report','location','labor','lang','approval','laborer')); 
    }

    public function laborStore(LaborRequest $request)
    {
        // Project id
        $project_id = session('project_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Language
        $lang = get_language();
        // Insert Record
        $record = new DailyLabour;
        $record->daily_report_id     = $request->report_id;
        $record->labor_clasification = $request->labor_clasification;
        $record->labor_number        = $request->labor_number;
        $record->work_hours          = $request->work_hours;
        $record->created_at          = date('Y-m-d H:i:s');
        $record->created_by          = userid();
        $record->save();
        if($record->id>0)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Report_id id
        $report_id = session('report_id');
        // Get daily report
        $daily_report = Daily_report::with('labour')->where('id',$report_id)->first();
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the laborer 
        $laborer = DailyLabour::where('daily_report_id',$report_id)->get();
        //load view to display result
        return view('pmis/contractor_project/daily_report/labor_view',compact('daily_report','lang','laborer','approval'));
    }   

    /** 
     * @Date: 2020-03-03 13:58:26 
     * @Desc: Edit Labor   
     */
    public function laborEdit(Request $request)
    {
        // Language
        $lang = get_language();
        //Get Labor 
        $labor = DailyLabour::with('dailyReport')->find($request->id);
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($labor->dailyReport->project_location_id);
        // Get all statics
        $labor_classification = Labor_clasification::all();
        // Load View
        return view('pmis/contractor_project/daily_report/labor_edit',compact('labor','location','labor_classification','lang')); 
    }

    /** 
     * @Date: 2020-03-03 14:21:25 
     * @Desc: Labor Update 
     */
    public function laborUpdate(LaborRequest $request)
    {    
        // Language
        $lang = get_language();
        $id = decrypt($request->id);
        // Get the labor 
        $labor = DailyLabour::find($id);
        // Update the labor 
        $labor->update($request->except('id'));  
        // Get the the daily report and labors 
        $daily_report = Daily_report::with('labour')->where('id',$labor->daily_report_id)->first();
        if($labor)
        {
            // Update daily report table 
            Daily_report::whereId($labor->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the laborer 
        $laborer = DailyLabour::where('daily_report_id',$labor->daily_report_id)->get();

        return view('pmis/contractor_project/daily_report/labor_view',compact('laborer','daily_report','lang','approval')); 
    }

    /** 
     * @Date: 2020-03-03 14:56:10 
     * @Desc:  Delete Labor 
     */
    public function laborDelete(Request $request)
    {
        $labor = DailyLabour::where('id',decrypt($request->id))->delete(); 
        if($labor)
        {
          Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }

        return view('layouts.components.alert'); 
    }

    /** 
     * @Date: 2020-02-20 13:40:24 
     * @Desc: Equipments  
     */
    public function equipment(Request $request)
    {
        // Project id
        $id = $request->project_id;
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Project Location id
        $project_location_id = session('project_location_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Get daily report
        $daily_report = Daily_report::with('equipments')->where('id',$report_id)->first();
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get all statics
        $equipments = Machinery::all();
        // Get Language
        $lang = get_language();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the equipment 
        $equipment = DailyEquipments::where('daily_report_id',$report_id)->get();
        // Load view to display result
        return view('pmis/contractor_project/daily_report/equipment',compact('report_id','report_date','project_location_id','daily_report','location','equipments','lang','approval','equipment')); 
    }

    public function equipmentStore(EquipmentRequest $request)
    {
        // Project id
        $project_id = session('project_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Language
        $lang = get_language();
        // Insert Record
        $record = new DailyEquipments;
        $record->daily_report_id    = $request->report_id;
        $record->equipment_type     = $request->equipment_type;
        $record->equipment_no       = $request->equipment_no;
        $record->work_hourse         = $request->work_hourse;
        $record->created_at         = date('Y-m-d H:i:s');
        $record->created_by         = userid();
        $record->save();
        if($record->id>0)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Report_id id
        $report_id = session('report_id');
        // Get daily report
        $daily_report = Daily_report::with('equipments')->where('id',$report_id)->first();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the equipments 
        $equipments = DailyEquipments::where('daily_report_id',$report_id)->get();
        return view('pmis/contractor_project/daily_report/equipment_view',compact('equipments','daily_report','lang','approval')); 
    }

    /** 
     * @Date: 2020-03-03 15:06:45 
     * @Desc: Edit Equipment   
     */
    public function equipmentEdit(Request $request)
    {
        // Language
        $lang = get_language();
        // Equipments 
        $equipment = DailyEquipments::with('dailyReport')->find($request->id);
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($equipment->dailyReport->project_location_id);
        // Get all statics
        $equipments_category = Machinery::all();
        // Load View
        return view('pmis/contractor_project/daily_report/equipment_edit',compact('lang','equipment','location','equipments_category')); 
    }

    /** 
     * @Date: 2020-03-03 16:11:26 
     * @Desc:  Update Equipment  
     */
    public function equipmentUpdate(EquipmentRequest $request)
    {
         // Language
        $lang = get_language();
        // Get the equipment 
        $equipment = DailyEquipments::find(decrypt($request->id)); 
        // Update the equipment 
        $equipment->update($request->except('id'));  
        // Get the the daily report and equipments 
        //$daily_report = Daily_report::with('equipments')->where('report_date',$equipment->DailyReport->report_date)->where('project_id',$equipment->DailyReport->project_location_id)->first(); 
        $daily_report = Daily_report::with('equipments')->where('id',$equipment->daily_report_id)->first();
        if($equipment)
        {
            // Update daily report table 
            Daily_report::whereId($equipment->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the equipments 
        $equipments = DailyEquipments::where('daily_report_id',$equipment->daily_report_id)->get();
        return view('pmis/contractor_project/daily_report/equipment_view',compact('equipments','daily_report','lang','approval')); 
    }

    /** 
     * @Date: 2020-03-03 16:28:52 
     * @Desc: Delete Daily equipment   
     */
    public function equipmentDelete(Request $request)
    {
         $equipment = DailyEquipments::where('id',decrypt($request->id))->delete(); 
        if($equipment)
        {
          Session()->flash('success', __("global.success_delete_msg"));
        }
        else
        {
          Session()->flash('fail', __("global.failed_msg"));
        }
        return view('layouts.components.alert'); 
    }

    /** 
     * @Date: 2020-02-16 13:21:48 
     * @Desc: create form or edit form 
     */
    public function accidents(Request $request)
    {
        // Project id
        $id = $request->project_id;
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Contractor id
        $con_id = session('current_contractor');
        // Get daily report
        $daily_report  = Daily_report::with('accidents')->where('id',$report_id)->first();
        // Project Location id
        $project_location_id = session('project_location_id');
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get Language
        $lang = get_language();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the accidents 
        $accidents = DailyAccidents::where('daily_report_id',$report_id)->first();
        // Load the view
        return view('pmis/contractor_project/daily_report/accidents',compact('report_id','report_date','daily_report','project_location_id','location','lang','approval','accidents'));
    }

    public function accidentsStore(AccidentsRequest $request)
    {
        // Project id
        $project_id = session('project_id');
        // Contractor id
        $con_id = session('current_contractor');
        // Report id
        $report_id = $request->daily_report_id;    
        // Language
        $lang = get_language();
        // Store data in database
        $request['daily_report_id'] = $report_id;
        $request['accident_date'] = dateProvider($request->accident_date,$lang);
        $record = DailyAccidents::create($request->all());
        if($record)
        {
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Get daily report
        $daily_report = Daily_report::with('accidents')->where('id',$report_id)->first();
        //load view to display result
        return view('pmis/contractor_project/daily_report/accidents_view',compact('daily_report','lang'));
    }

    /** 
     * @Date: 2020-03-01 09:48:42 
     * @Desc: Update Daily Accidents  
     */
    public function accidentsUpdate(Request $request)
    {
        // Get Language
        $lang = get_language();
        // weather id 
        $id = decrypt($request->id);
        $request->accident_date = dateProvider($request->accident_date,$lang);
        // Update the weather 
        $record = DailyAccidents::find($id); 
        $record->update($request->except('id'));
        if($record)
        {
            // Update daily report table 
            Daily_report::whereId($record->daily_report_id)->update(array('completed'=>'0'));
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
        // Get daily report
        $daily_report = Daily_report::with('accidents')->where('id',$record->daily_report_id)->first();
        // Get Language
        $lang = $lang;
        //load view to display result
        return view('pmis/contractor_project/daily_report/accidents_view',compact('daily_report','lang'));
    }

    /**
     * Filter list the resource .
     *
     * @return \Illuminate\Http\Response
     */  
    public function filterDailyReports()
    {
        //Get Project ID
        $project_id = decrypt(Input::get('project_id'));
        // Get Language
        $lang = get_language();
        // Get status
        $status = Input::get('rep_status');
        if($status!=""){
            // Get data by approval type
            $records = Daily_report::where('project_id',$project_id)->where('status',$status)->orderBy('id','DESC')->paginate(10)->onEachSide(1);
        }else {
            // Get all data
            $records = Daily_report::where('project_id',$project_id)->orderBy('id','DESC')->paginate(10)->onEachSide(1);;
        }
        // Load view to show result
        return view('pmis/contractor_project/daily_report/index_filter',compact('lang','project_id','status','records')); 
    }

    /** 
     * @Date: 2020-02-16 13:21:48 
     * @Desc: create form or edit form 
     */
    public function photos(Request $request)
    {
        // Project id
        $project_id = $request->project_id;
        // Report id
        $report_id = session('report_id');
        // Report date
        $report_date = session('report_date');
        // Contractor id
        $con_id = session('current_contractor');
        // Get daily report
        $daily_report  = Daily_report::with('photos')->where('id',$report_id)->first();
        // Project Location id
        $project_location_id = session('project_location_id');
        // Get Project Locations by Users
        $location = Plan_locations::with('province')->find($project_location_id);
        // Get Language
        $lang = get_language();
        // Check if record not exists then set default value
        if($daily_report){
            $approval = $daily_report->status;
        }else{
            $approval = 0;
        }
        // Get the photos 
        $photos = getAttachments('daily_photos',$project_id,$report_id,'daily_photos');
        // Load the view
        return view('pmis/contractor_project/daily_report/photos',compact('report_id','report_date','daily_report','project_location_id','location','lang','approval','photos'));
    }

    public function bringMorePhotos(Request $request)
    {
      $number = $request->number;
      // Load the view
      return view('pmis/contractor_project/daily_report/morePhotos',compact('number'));
    }

    public function photosStore(Request $request)
    {
        // Upload Attachments
        if($request->hasFile('file'))
        {
            // Project id
            $project_id = session('project_id');
            // Report id
            $report_id = $request->report_id;
            $files = $request->file('file');
            $i=0;
            foreach ($files as $file)
            {
                // Call Upload Function to Upload all Posted Files
                $fileUpload = uploadAttachments($project_id,$report_id,'daily_photos',$file,'',$i,'daily_photos');
                if(!$fileUpload){
                    Session()->flash('att_failed', __("global.attach_failed"));
                }else{
                    Session()->flash('success', __("global.success_msg"));
                }
                $i++;
            }
        }
    }

    /** 
     * @Date: 2020-03-03 13:58:26 
     * @Desc: Edit Labor   
     */
    public function viewPhotos($enc_id=0)
    {
        $id = decrypt($enc_id);
        // Get record 
        $record = DailyPhotos::find($id);
        // Load view to display data
        return view('pmis/contractor_project/daily_report/viewPhotos',compact('record','enc_id'));
    }
}
