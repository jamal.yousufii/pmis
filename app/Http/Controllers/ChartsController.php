<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\models\Requests;
use App\models\Plans;
use App\models\Daily_report;
use App\models\Plan_locations;
use App\models\Billofquantities;
use Redirect;

class ChartsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0,$dep_id=0)
    {
        session(['current_section' => "pmis_progress"]);
        // Get Language
        $lang = get_language();
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = $dep_id;  
        }
        // Get Department ID
        $depID = decrypt($dep_id);
        // Project ID
        $project_id = decrypt($enc_id);
        // Get plan record
        $plan = Plans::find($project_id);
        if($plan)
        {
            // Get summary data
            $summary = Requests::whereId($plan->request_id)->first();
        } 
        // Load tabs
        $tabs = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_bq_chart']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $project_location= Plan_locations::whereHas('Projects', function($query) use ($project_id, $depID){
                $query->where('project_id',$project_id);
                $query->where('department',$depID);
            })->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $project_location= Plan_locations::whereHas('department_staff', function($query) use ($project_id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$project_id);
                $query->where('department_id',$depID);
            })->orderBy('id','desc')->get();
        }
        // Load view to show result
        return view('pmis/progress/BQChart/index',compact('summary','plan','lang','enc_id','dep_id','tabs','project_location'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function BQChart($enc_id=0,$dep_id=0,$loc_id=0)
    {
        session(['current_section' => "pmis_progress"]);
        // Get Language
        $lang = get_language();
        // Check if department ID is exist or not
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        else
        {
            $dep_id = $dep_id;  
        }
        // Get Department ID
        $depID = decrypt($dep_id);
        // Project ID
        $project_id = decrypt($enc_id);
        // Project Location id
        $project_location_id = $loc_id;
        session(['project_location_id' => $loc_id]);
        
        // Get Activities
        $reports = Daily_report::has('activities')->where('project_id',$project_id)->where('project_location_id',$loc_id)->get();

        // Get Bill of Quantities
        $records = Billofquantities::where('project_id',$project_id)->where('project_location_id',$project_location_id)->get();        
        // Get plan record
        $plan = Plans::find($project_id);
        if($plan)
        {
            // Get summary data
            $summary = Requests::whereId($plan->request_id)->first();
        } 
        // Load tabs
        $tabs = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'pmis_bq_chart']);
        // Default value
        $remained = array();
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $project_location= Plan_locations::whereHas('Projects', function($query) use ($project_id, $depID){
                $query->where('project_id',$project_id);
                $query->where('department',$depID);
            })->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $project_location= Plan_locations::whereHas('department_staff', function($query) use ($project_id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$project_id);
                $query->where('department_id',$depID);
            })->orderBy('id','desc')->get();
        }
        // Load View
        return view('pmis/progress/BQChart/chart',compact('summary','plan','records','lang','enc_id','dep_id','tabs','remained','project_location','project_location_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //BQ Chart
    public function billQuantityChart($enc_id=0)
    {
        // Get Language
        $lang = get_language();
        // Project ID
        $id = decrypt($enc_id);
        //Get project
        $record = Plans::find($id);
        // Load View
        return view('pmis/progress/BQChart/chart',compact('record','lang','enc_id'));
    }

}
?>
