<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectProgressSearchRequest;
use App\models\Share;
use App\models\Requests;
use App\models\Plans;
use App\models\Estimation;
use App\models\Daily_report;
use App\models\Daily_activities;
use App\models\Plan_locations;
use App\models\Billofquantities;
use App\models\Authentication\Departments;
use App\models\Authentication\Section_deps;
use App\models\Authentication\Users;
use App\models\Settings\Contractor;
use App\models\Settings\Statics\Categories;
use App\models\Settings\Statics\Projects;
use App\Imports\ExcelImport;
use App\Exports\ExcelExport;
use Illuminate\Support\Facades\Input;
use Spatie\Activitylog\Models\Activity;
use Excel;
use Importer;
use DB;
use PDF;
use Redirect;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id,Request $request)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_reports'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_reports"]);
        $segments = array(
            '0' =>'graph.index',
            '1' =>$dep_id
        );
        // Get language
        $lang = get_language();
        // Forget a single key...
        $request->session()->forget('extra_section');
        $segments = array(
            '0' => 'report',
            '1' => session('current_department')
        );
        // Get all static tables
        $report_option = get_module_sections(session('current_mod'),session('current_department'),'report','3');
        // Load view to display data
        return view('pmis/reports/report',compact('report_option','lang','segments','dep_id'));
    }

    /**
     * Show the form for searching the report .
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data['code'] = $request->code;
        $data['lang'] = get_language();
        switch($data['code'])
        {
            case 'project_status_report' :
            {
                $data['lang'] = get_language();
                $department = decrypt(session('current_department'));
                if($department==1)
                {
                    $data['departments'] = Departments::whereNotIn('code',array('dep_01','dep_06'))->where('organization_id', organizationid())->get();
                }
                else
                {
                    $data['departments'] = Departments::whereId($department)->where('organization_id', organizationid())->get();
                }
                return view('pmis/reports/project_status_report_search',$data);
                break;
            }
            case 'project_progressive_report' :
            {
                $department = decrypt(session('current_department'));
                if($department==1)
                {
                    $data['departments'] = Departments::whereIn('code',array('dep_02','dep_03'))->where('organization_id', organizationid())->get();
                }
                else
                {
                    $data['departments'] = Departments::whereId($department)->where('organization_id', organizationid())->get();
                }
                $data['categories'] = Categories::all();
                // Load view to display data
                return view('pmis/reports/project_progressive_search',$data);
                break;
            }
            case 'daily_report_print':
            {
                $department = decrypt(session('current_department'));
                if($department==1)
                {
                    $data['departments'] = Departments::whereIn('code',array('dep_02','dep_03'))->where('organization_id', organizationid())->get();
                }
                else
                {
                    $data['departments'] = Departments::whereId($department)->where('organization_id', organizationid())->get();
                }
                return view('pmis/reports/daily_report_search',$data);
                break;
            }
        }
    }

    /** Project status reports numeric */

    /**
     * return the search result of the project status report.
     *
     * @return
     * @param $request
     */
    public function project_status_report(Request $request)
    {
        // Department id
        $department_id = $request->department;
        // Get language
        $lang = get_language();
        // Set default value
        $records = array();
        $code = getDepartmentCodeByID($department_id);
        switch($code)
        {
            case 'dep_02' :
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                //ddd($records);
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
            case 'dep_03' :
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
            case 'dep_04' :
            {
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Merge all queries by using Union
                $data = $survey_data->union($design_data)->union($estimation_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
            case 'dep_05' :
            {
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $procurement_data->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
            case 'dep_07' :
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                //ddd($records);
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
            case 'dep_09' :
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','departments.id as dep_id','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
                //ddd($records);
                // Load View
                return view('pmis/reports/project_status_report_show',compact('lang','department_id','records'));
                break;
            }
        }
    }

    /**
     * return the list of the projects.
     *
     * @return
     * @param $request
     */
    public function project_status_report_list($id,$share_to,$status,$share_code,$table,Request $request)
    {
        // Get language
        $lang = get_language();
        // Get department code
        $code = getDepartmentCodeByID($id);
        if($share_code!='pmis_plan'){
            // Get record
            $records = Plans::select('projects.name','projects.urn','project_types.name_'.$lang.' as type')
                            ->leftJoin($table,$table.'.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where(function($query) use ($id,$code){
                                if($code!='dep_04' and $code!='dep_05'){
                                    $query->where('projects.department',$id);
                                }
                            })
                            ->where('shares.share_to_code',$share_code)
                            ->where('shares.share_to',$share_to)
                            ->where(function($query) use ($status,$table){
                                if($status!=2){
                                    $query->where($table.'.process',$status);
                                }else{
                                    $query->whereNull($table.'.id');
                                }
                            })
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.id')
                            ->orderBy('projects.id','desc')
                            ->paginate(15)->onEachSide(1);
        }else{
            // Get record
            $records = Requests::select('requests.urn','hr_departments.name_'.$lang.' as department','requests.request_date','requests.doc_number')
                            ->leftJoin($table,$table.'.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.hr_departments','requests.department_id','=','hr_departments.id')
                            ->where('shares.share_to',$share_to)
                            ->where('shares.share_to_code',$share_code)
                            ->where(function($query) use ($status,$table){
                                if($status!=2){
                                    $query->where($table.'.process',$status);
                                }else{
                                    $query->whereNull($table.'.id');
                                }
                            })
                            ->whereNull('shares.deleted_at')
                            ->orderBy('requests.id','desc')
                            ->paginate(15)->onEachSide(1);
        }
        if(Input::get('ajax') == 1)
        {
            //Static record iteration with pagination
            $counter = $request->counter;
            // Load view to show result
            return view('pmis/reports/project_list_ajax',compact('lang','records','share_to','counter'));
        }
        else
        {
            // Load view to show result
            return view('pmis/reports/project_list',compact('lang','records','share_to'));
        }
    }

    /**
     * return the list of the projects in excel file.
     *
     * @return
     * @param $request
     */
    public function project_status_report_excel($department_id)
    {
        // Get language
        $lang = get_language();
        // Department id
        $department_id = decrypt($department_id);
        $code = getDepartmentCodeByID($department_id);
        $records = array();
        if($department_id)
        {
            if($code == 'dep_02')
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_03')
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_04')
            {
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Merge all queries by using Union
                $data = $survey_data->union($design_data)->union($estimation_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_05')
            {
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $procurement_data->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            Excel::load('public/print/project_status_report_'.$lang.'.xlsx', function($file) use($records){
                $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($records){
                    $row   = 7;
                    $index = 0;
                    foreach($records as $key=>$val)
                    {
                        $key = explode('-',$key);
                        $total = 0;
                        if(isset($val[2])) { $total = $total+$val[2]; }
                        if(isset($val[0])) { $total = $total+$val[0]; }
                        if(isset($val[1])) { $total = $total+$val[1]; }
                        // Apply style to sheet
                        $sheet->setStyle(array(
                            'font' => array(
                                'name'      =>  'B Nazanin',
                                'size'      =>  13,
                                'bold'      =>  false
                            )
                        ));
                        $sheet->setHeight($row, 30);
                        $sheet->setCellValue('A'.$row, ++$index);
                        $sheet->setCellValue('B'.$row, $key[1]);
                        $sheet->setCellValue('C'.$row, $key[0]);
                        if(isset($val[2])){
                            $val[2] = $val[2];
                        }else{
                            $val[2] = '0';
                        }
                        $sheet->setCellValue('D'.$row, $val[2]);
                        if(isset($val[0])){
                            $val[0] = $val[0];
                        }else{
                            $val[0] = '0';
                        }
                        $sheet->setCellValue('E'.$row, $val[0]);
                        if(isset($val[1])){
                            $val[1] = $val[1];
                        }else{
                            $val[1] = '1';
                        }
                        $sheet->setCellValue('F'.$row, $val[1]);
                        $sheet->setCellValue('G'.$row, $total);
                        $row++;
                    }
                    $row--;
                    $sheet->cell('A7:G'.$row,function($cell){
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                });
            })->export('xlsx');
        }
    }

    /**
     * return the list of the projects in pdf file.
     *
     * @return
     * @param $request
     */
    public function project_status_report_pdf($department_id)
    {
        // Get language
        $lang = get_language();
        // Department id
        $department_id = decrypt($department_id);
        $code = getDepartmentCodeByID($department_id);
        $department_name = getDepartmentNameByID($department_id,$lang);
        $records = array();
        if($department_id)
        {
            if($code == 'dep_02')
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_03')
            {
                // Get record from projects
                $plan_data = Requests::select(DB::raw('count(requests.id) as total'),DB::raw('IFNULL(projects.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('projects','projects.request_id','=','requests.id')
                            ->join('shares','shares.record_id','=','requests.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_plan')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('projects.process','shares.share_to');
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Get record from implements
                $checklist_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('implements','implements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_estimation')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_implement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('implements.process','shares.share_to');
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('projects.department',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $plan_data->union($survey_data)->union($design_data)->union($estimation_data)->union($checklist_data)->union($procurement_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_04')
            {
                // Get record from surveys
                $survey_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_plan')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_survey')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('surveys.process','shares.share_to');
                // Get record from architectures
                $design_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_survey')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_design')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('architectures.process','shares.share_to');
                // Get record from estimations
                $estimation_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_design')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_estimation')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('estimations.process','shares.share_to');
                // Merge all queries by using Union
                $data = $survey_data->union($design_data)->union($estimation_data)->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            elseif($code == 'dep_05')
            {
                // Get record from procurements
                $procurement_data = Plans::select(DB::raw('count(DISTINCT(projects.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'),'departments.name_'.$lang.' as department','sections.code as section_code','sections.name_'.$lang.' as section','sections.orders as order','sections.tables')
                            ->leftJoin('procurements','procurements.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('pmis_auth.departments','shares.share_to','=','departments.id')
                            ->join('pmis_auth.sections','shares.share_to_code','=','sections.code')
                            ->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })
                            ->where('shares.share_from_code','pmis_implement')
                            ->where('shares.share_to',$department_id)
                            ->where('shares.share_to_code','pmis_procurement')
                            ->whereNull('shares.deleted_at')
                            ->groupBy('procurements.process','shares.share_to');
                // Merge all queries by using Union
                $data = $procurement_data->orderBy('order')->get();
                if($data)
                {
                    foreach($data as $item)
                    {
                        $records[$item['department'].'-'.$item['section'].'-'.$item['section_code'].'-'.$item['tables'].'-'.$item['dep_id']][$item['status']] = $item['total'];
                    }
                }
            }
            // pass view file
            $pdf = PDF::loadView('pmis/reports/project_status_report_pdf',compact('lang','records','department_name'));
            // download pdf
            return $pdf->setPaper('a4')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
        }
    }

    /** Project status reports detailed */

    /**
     * return the search result of the project status report.
     *
     * @return
     * @param $request
     */
    public function project_status_report_detailed(Request $request)
    {
        // Department id
        $department_id = $request->dep_id;
        // Section code
        $code = $request->section_code;
        // Get language
        $lang = get_language();
        switch($code)
        {
            case 'pmis_survey' :
            {
                // Get record from surveys
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'surveys.start_date' ,'surveys.end_date' ,DB::raw('IFNULL(surveys.process,2) as status'), 'surveys.description', 'surveys.has_survey', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->leftJoin('employees','surveys.team_head','=','employees.id')
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->groupBy('project_locations.id')
                            ->orderBy('projects.id','desc')->get();
                // ddd($records);
                // Load view to show data
                return view('pmis/reports/project_status_report_show_detailed',compact('lang','department_id','code','records'));
                break;
            }
            case 'pmis_design' :
            {
                // Get record from design
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'architectures.start_date' ,'architectures.end_date' ,DB::raw('IFNULL(architectures.process,2) as status'), 'architectures.description', 'architectures.has_data', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->leftJoin('employees','architectures.employee_id','=','employees.id')
                            ->whereIn('version',['first','latest'])
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->groupBy('project_locations.id')
                            ->orderBy('projects.id','desc')->get();
                // ddd($records);
                // Load view to show data
                return view('pmis/reports/project_status_report_show_detailed',compact('lang','department_id','code','records'));
                break;
            }
            case 'pmis_estimation' :
            {
                // Get record from estimation
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'estimations.start_date' ,'estimations.end_date' ,DB::raw('IFNULL(estimations.process,2) as status'), 'estimations.cost', 'estimations.description', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->leftJoin('employees','estimations.employee_id','=','employees.id')
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->groupBy('project_locations.id')
                            ->orderBy('projects.id','desc')->get();
                // Load view to show data
                return view('pmis/reports/project_status_report_show_detailed',compact('lang','department_id','code','records'));
                break;
            }
            case 'pmis_procurement' :
            {
                // Get record from procurement
                $records = Plans::with('procurement')
                            ->whereHas('share', function($query) use ($department_id, $code){
                                $query->where('share_to', $department_id);
                                $query->where('share_to_code', $code);
                            })->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })->orderBy('id','desc')->get();
                // ddd($records[0]->procurement->first());
                // Load view to show data
                return view('pmis/reports/project_status_report_show_detailed',compact('lang','department_id','code','records'));
                break;
            }
        }
    }

    /**
     * return the list of the projects in pdf file.
     *
     * @return
     * @param $request
     */
    public function project_status_report_detailed_pdf($department_id, $code)
    {
        // Get language
        $lang = get_language();
        $department_id = decrypt($department_id);
        $code          = decrypt($code);
        $department_name = getDepartmentNameByID($department_id,$lang);
        switch($code)
        {
            case 'pmis_survey' :
            {
                // Get record from surveys
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'surveys.start_date' ,'surveys.end_date' ,DB::raw('IFNULL(surveys.process,2) as status'), 'surveys.description', 'surveys.has_survey', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('surveys','surveys.project_id','=','projects.id')
                            ->leftJoin('employees','surveys.team_head','=','employees.id')
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->orderBy('projects.id','desc')->get();
                // Pass view file
                $pdf = PDF::loadView('pmis/reports/project_status_report_detailed_pdf',compact('lang','records','department_id','code','department_name'));
                // Download pdf
                return $pdf->setPaper('a3')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
                break;
            }
            case 'pmis_design' :
            {
                // Get record from design
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'architectures.start_date' ,'architectures.end_date' ,DB::raw('IFNULL(architectures.process,2) as status'), 'architectures.description', 'architectures.has_data', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('architectures','architectures.project_id','=','projects.id')
                            ->leftJoin('employees','architectures.employee_id','=','employees.id')
                            ->whereIn('version',['first','latest'])
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->orderBy('projects.id','desc')->get();
                // Pass view file
                $pdf = PDF::loadView('pmis/reports/project_status_report_detailed_pdf',compact('lang','records','department_id','code','department_name'));
                // Download pdf
                return $pdf->setPaper('a3')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
                break;
            }
            case 'pmis_estimation' :
            {
                // Get record from estimation
                $records = Plan_locations::select('project_locations.*', 'projects.urn', 'projects.name', 'projects.code', 'categories.name_'.$lang.' as category', 'project_types.name_dr as type' ,'estimations.start_date' ,'estimations.end_date' ,DB::raw('IFNULL(estimations.process,2) as status'), 'estimations.cost', 'estimations.description', 'employees.first_name')
                            ->join('projects','project_locations.project_id','=','projects.id')
                            ->join('shares','shares.record_id','=','projects.id')
                            ->join('categories','projects.category_id','=','categories.id')
                            ->join('project_types','projects.project_type_id','=','project_types.id')
                            ->leftJoin('estimations','estimations.project_id','=','projects.id')
                            ->leftJoin('employees','estimations.employee_id','=','employees.id')
                            ->where('shares.share_to', $department_id)
                            ->where('shares.share_to_code', $code)
                            ->whereNull('shares.deleted_at')
                            ->where('project_locations.status','0')
                            ->orderBy('projects.id','desc')->get();
                // Pass view file
                $pdf = PDF::loadView('pmis/reports/project_status_report_detailed_pdf',compact('lang','records','department_id','code','department_name'));
                // Download pdf
                return $pdf->setPaper('a3')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
                break;
            }
            case 'pmis_procurement' :
            {
                // Get record from procurement
                $records = Plans::with('procurement')
                            ->whereHas('share', function($query) use ($department_id, $code){
                                $query->where('share_to', $department_id);
                                $query->where('share_to_code', $code);
                            })->whereHas('project_location', function($query){
                                $query->where('status','0');
                            })->orderBy('id','desc')->get();
                // Pass view file
                $pdf = PDF::loadView('pmis/reports/project_status_report_detailed_pdf',compact('lang','records','department_id','code','department_name'));
                // Download pdf
                return $pdf->setPaper('a3')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
                break;
            }
        }
    }


    /** Project daily reports */

    /**
     * return the search result of the daily reports.
     *
     * @return
     * @param $request
     */

    public function daily_report(Request $request)
    {
        // Language
        $lang = get_language();
        $from_date = dateProvider($request->from_report_date,$lang);
        $to_date   = dateProvider($request->to_report_date,$lang);
        $department_id = $request->department;
        $project_id = $request->project_id;
        $project_location_id = $request->project_location_id;
        // Get daily report
        $daily_report = Daily_report::with(['weather','activities','labour','equipments','test','narrative'])
                         ->whereBetween('report_date',[$from_date,$to_date])
                         ->where('project_id',$project_id)
                         ->where('status','1')
                         ->where('project_location_id',$project_location_id)
                         ->get();
        // Get daily activities done
        $get_activities = Daily_report::select('daily_activities.bq_id', DB::raw('SUM(daily_activities.amount) as amount'))
                                    ->join('daily_activities','daily_activities.daily_report_id','=','daily_reports.id')
                                    ->whereBetween('daily_reports.report_date',[$from_date,$to_date])
                                    ->where('daily_reports.project_id',$project_id)
                                    ->where('status','1')
                                    ->where('daily_reports.project_location_id',$project_location_id)
                                    ->groupBy('daily_activities.bq_id')
                                    ->get();
        // Sort data in array
        $activities = array();
        if($get_activities){
            foreach($get_activities as $item){
                $activities[$item->bq_id] = $item->amount;
            }
        }
        // Get project details
        $project = Plans::with('project_contractor')->with('procurement')->find($project_id);
        // Get project budget from BQ
        $project_budget = Billofquantities::where('project_location_id',$project_location_id)->get()->sum('total_price');
        // Calcualte budget done
        $budget_done = getBudgetDone(array($project_location_id));
        $work_progress = 0;
        if($project_budget > 0)
        {
            $work_progress = getPercentage($project_budget,$budget_done);
        }
        // Get project location
        $location = Plan_locations::with('province')->find($project_location_id);
        // Load view to show data
        return view('pmis/reports/daily_report_show',compact('daily_report','lang','project','location','from_date','to_date','project_id','department_id','project_id','project_location_id','work_progress','activities'))->with($request->all());
    }

    /**
     * return the list of daily report in pdf file.
     *
     * @return
     * @param $request
     */
    public function daily_report_pdf(Request $request)
    {
        // Get language
        $lang = get_language();
        $from_date = $request->from_report_date;
        $to_date   = $request->to_report_date;
        $department_id = $request->department;
        $project_id = $request->project_id;
        $project_location_id = $request->project_location_id;
        // Get daily report
        $daily_report = Daily_report::with(['weather','activities','labour','equipments','test','narrative'])
                         ->whereBetween('report_date',[$from_date,$to_date])
                         ->where('project_id',$project_id)
                         ->where('status','1')
                         ->where('project_location_id',$project_location_id)
                         ->get();
        // Get daily activities done
        $get_activities = Daily_report::select('daily_activities.bq_id', DB::raw('SUM(daily_activities.amount) as amount'))
                                    ->join('daily_activities','daily_activities.daily_report_id','=','daily_reports.id')
                                    ->whereBetween('daily_reports.report_date',[$from_date,$to_date])
                                    ->where('daily_reports.project_id',$project_id)
                                    ->where('status','1')
                                    ->where('daily_reports.project_location_id',$project_location_id)
                                    ->groupBy('daily_activities.bq_id')
                                    ->get();
        // Sort data in array
        $activities = array();
        if($get_activities){
            foreach($get_activities as $item){
                $activities[$item->bq_id] = $item->amount;
            }
        }
        // Get project details
        $project = Plans::with('project_contractor')->find($project_id);
        // Get project budget from BQ
        $project_budget = Billofquantities::where('project_location_id',$project_location_id)->get()->sum('total_price');
        // Calcualte budget done
        $budget_done = getBudgetDone(array($project_location_id));
        $work_progress = 0;
        if($project_budget > 0)
        {
            $work_progress = getPercentage($project_budget,$budget_done);
        }
        // Get project location
        $location = Plan_locations::with('province')->find($project_location_id);
        if($location)
        {
            // Pass view file
            $pdf = PDF::loadView('pmis/reports/daily_report_pdf',compact('daily_report','lang','project','location','from_date','to_date','project_id','department_id','project_id','project_location_id','work_progress','activities'));
            // Download pdf
            return $pdf->setPaper('a4')->setOrientation('landscape')->download(__("global.project_status_report").'.pdf');
        }
    }

    /** Project progressive reports */

    /**
     * Show project progressicv report in each directorates
     */
    public function project_progressive_report(Request $request)
    {
        // Get language
        $lang = get_language();
        // Department id
        $department_id = $request->department_id;
        // From date
        $from_date = $request->from_date;
        $fdate = dateProvider($from_date,$lang);
        // To date
        $to_date = $request->to_date;
        $tdate = dateProvider($to_date,$lang);
        // Category
        $category_id = $request->category_id;
        // Project type
        $project_type_id = $request->project_type_id;
        // Projects
        $projects_id = $request->projects_id;
        // Project status
        $status = $request->status;
        // Get language
        $lang = get_language();
        // Get department name
        $department_name = getDepartmentNameByID($department_id);
        // Get projects
        $records =  Plan_locations::with(['daily_reports'=>function($query) use ($fdate,$tdate){
                                        $query->whereBetween('report_date',[$fdate,$tdate]);
                                        $query->where('status','1');
                                    }])->with(['change_request_time'=>function($query){
                                        $query->where('operation','1');
                                        $query->orderBy('id', 'desc')->first();
                                    }])->whereHas('projects', function($query) use ($department_id, $category_id, $project_type_id, $projects_id){
                                        $query->whereHas('share', function($query) use ($department_id){
                                            $query->where('share_to',$department_id);
                                            $query->where('share_to_code','pmis_progress');
                                        })->where(function($query) use ($category_id, $project_type_id, $projects_id){
                                            if($category_id!="")
                                            {
                                                $query->where('category_id',$category_id);
                                            }
                                            if($project_type_id!="")
                                            {
                                                $query->where('project_type_id',$project_type_id);
                                            }
                                            if(!in_array('-',$projects_id))
                                            {
                                                $query->whereIn('id',$projects_id);
                                            }
                                        });
                                    })->where(function($query) use ($status){
                                        if($status!='')
                                        {
                                            $query->where('status',$status);
                                        }
                                    })->orderBy('project_id','desc')->get();
        $projects_id = implode(',',$projects_id);
        // Load view to show result
        return view('pmis/reports/project_progressive_show',compact('lang','department_id','records','status','category_id','project_type_id','projects_id','department_name','from_date','to_date'));
    }

    /**
     * Generate PDF
     * Show project progressicv report in each directorates
     */
    public function project_progressive_report_pdf(Request $request)
    {
        // Get language
        $lang = get_language();
        // Department id
        $department_id = $request->department_id;
        // From date
        $from_date = $request->from_date;
        $fdate = dateProvider($from_date,$lang);
        // To date
        $to_date = $request->to_date;
        $tdate = dateProvider($to_date,$lang);
        // Category
        $category_id = $request->category_id;
        // Project type
        $project_type_id = $request->project_type_id;
        // Projects
        $projects_id = explode(',',$request->projects_id);
        // Project status
        $status = $request->status;
        // Get language
        $lang = get_language();
        // Get department name
        $department_name = getDepartmentNameByID($department_id);
        // Get projects
        $records = Plan_locations::with(['daily_reports'=>function($query) use ($fdate,$tdate){
                                        $query->whereBetween('report_date',[$fdate,$tdate]);
                                        $query->where('status','1');
                                    }])->with(['change_request_time'=>function($query){
                                        $query->where('operation','1');
                                        $query->orderBy('id', 'desc')->first();
                                    }])->whereHas('projects', function($query) use ($department_id, $category_id, $project_type_id, $projects_id){
                                        $query->whereHas('share', function($query) use ($department_id){
                                            $query->where('share_to',$department_id);
                                            $query->where('share_to_code','pmis_progress');
                                        })->where(function($query) use ($category_id, $project_type_id, $projects_id){
                                            if($category_id!="")
                                            {
                                                $query->where('category_id',$category_id);
                                            }
                                            if($project_type_id!="")
                                            {
                                                $query->where('project_type_id',$project_type_id);
                                            }
                                            if(!in_array('-',$projects_id))
                                            {
                                                $query->whereIn('id',$projects_id);
                                            }
                                        });
                                    })->where(function($query) use ($status){
                                        if($status!='')
                                        {
                                            $query->where('status',$status);
                                        }
                                    })->orderBy('project_id','desc')->get();
            // pass view file
            $pdf = PDF::loadView('pmis/reports/project_progressive_pdf',compact('lang','records','department_name'));
            // download pdf
            return $pdf->setPaper('a3')->setOrientation('landscape')->download(__("report.project_progressive_report").'.pdf');
    }

    /**
     * return the project location .
     *
     * @return HTML
     */
    public function getPorjectLocation(Request $request)
    {
        $project_id = $request->id;
        $lang = get_language();
        // Get Project Locations by Users
        $locations = Plan_locations::where('project_id',$project_id)->orderBy('id','desc')->get();
         $option = '<option value="">'. trans('global.select') .'</option>';
         if($locations)
         {
            $district   = "";
            $village    = "";
            $latitude   = "";
            $longitude  = "";
            foreach($locations as $location){
              if($location->district_id){
                $district = " / ".$location->district->{'name_'.$lang};
              }
              if($location->village_id){
                $village = " / ".$location->village->{'name_'.$lang};
              }
              if($location->latitude){
                $latitude = " / ".$location->latitude;
              }
              if($location->longitude){
                $longitude = " / ".$location->longitude;
              }
              $option .= "<option value=".$location->id.">". $location->province->{'name_'.$lang} . $district . $village . $latitude . $longitude."</option>";
            }
         }

         return $option;
    }

    /**
     * Show the form for searching the report .
     *
     * @return \Illuminate\Http\Response
     */
    public function projectProgressSearch(ProjectProgressSearchRequest $request)
    {
      $lang = get_language();
      $bill_quantities  = Billofquantities::activities_calculations($request->project_id,$request->project_location_id);
      $project_contract = Estimation::where('project_id',$request->project_id)->first();
      return view('pmis/reports/project_progress_result',compact('bill_quantities','lang','project_contract'));
    }

    /**
     * Bring projects based on departments
     */
    public function bringProjectsByDepartments(Request $request)
    {
        $department_id  = $request->id;
        $data['lang'] = get_language();
        // Get Records
        $data['projects']  = Plans::whereHas('project_contractor')->where('department',$department_id)->get();
        //Return response View
        return view('pmis/reports/daily_report_projects',$data);
    }

    /**
     * Bring categories by directorate
     */
    public function bringCategoriesByDirectorate(Request $request)
    {
        $id = $request->id;
        $lang = get_language();
        // Get Records
        $project_categories = Categories::whereHas('plans', function($query) use ($id){
                                        $query->where('department',$id);
                                    })->get();
        //Return response View
        return view('pmis.reports.project_categories_dropdown',compact('project_categories','lang'));
    }

    /**
     * Bring projects type by category
     */
    public function bringProjectTypesByCategory(Request $request)
    {
        $lang = get_language();
        // Get Records
        $project_types = Projects::where('category_id',$request->id)->get();
        //Return response View
        return view('pmis.reports.project_types_dropdown',compact('project_types','lang'));
    }

    /**
     * Bring projects by type
     */
    public function bringProjectsByType(Request $request)
    {
        $lang = get_language();
        // Get Records
        $projects = Plans::where('project_type_id',$request->id)->get();
        //Return response View
        return view('pmis.reports.projects_dropdown',compact('projects','lang'));
    }

    /**
     * Bring sections by department
     */
    public function bringSectionsByDepartment(Request $request)
    {
        $lang = get_language();
        // Display only thies sections if exists
        $sections = array('pmis_survey', 'pmis_design', 'pmis_estimation', 'pmis_procurement');
        // Get sections
        $record = Section_deps::select('sections.code', 'sections.name_'.$lang.' as section')
                                ->join('sections','section_deps.section_id','=','sections.id')
                                ->whereIn('sections.code', $sections)
                                ->where('section_deps.department_id',$request->id)
                                ->get();
        // Return response View
        return view('pmis.reports.sections_dropdown',compact('record'));
    }
}
