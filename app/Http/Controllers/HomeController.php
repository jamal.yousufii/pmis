<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\DocumentRequest;
use App\models\auth\Module;
use App\models\Requests;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Daily_report;
use App\Notifications\NewProject;
use App\models\Home;
use App\User;
use App\models\Settings\Dashboard;
use App\models\Settings\Contractor;
use Illuminate\Support\Facades\Input;
use App\models\Survey_documents;
use App\models\Authentication\Users;
use App\models\Authentication\Province;
use App\models\Authentication\District;
use App\models\Task;
use App\models\PlanLocationStatus;
use App\models\Billofquantities;
use App\models\PlanLocationStatusAttachment;
use App\models\NPmis\NationalProject;
use App\models\Provinces;
use File;
use Response;
use Validator;
use Config;
use DB;
use Carbon\Carbon;
use PhpParser\Node\Stmt\Foreach_;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mod='pmis')
    {
      if(organizationid()==2 and $mod=='pmis')
      {
        $mod = 'npmis';
      }
      $lang = get_language();
      $data['lang'] = $lang;
      $name = 'name_'.$lang;
      session(['current_mod' => $mod]);
      if($mod!='pmis')
      {
        session(['current_section' => "0"]);
      }
      else
      {
        session(['current_section' => ""]);
      }
      $current_mod_name = __('home.module');
      if($mod!='')
      {
        $current_mod_name = Module::get_module_id($mod)->$name;
      }
      session(['current_mod_name' => $current_mod_name]);
      if(Auth::user()->is_dashboard=='1' and $mod=='pmis') // Dashboard page
      {
        // Get all zone
        $zone = Province::select('zone as zcode','zname_'.$lang.' as zname')->groupBy('zone')->get();
        // Get all provinces
        $provinces = Province::all();
        // Get total of under construction projects
        $project_progress = Plans::select('pl.id','projects.priority')
                            ->whereHas('share', function($query){
                                $query->where('share_to_code','pmis_progress');
                            })
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->where('pl.status','0')->orderBy('projects.priority','asc')->get();
        // Set default value
        $location_id = array(
            '1' => array(),
            '2' => array(),
            '3' => array(),
        );
        // Arrange project location ids
        if($project_progress){
            foreach($project_progress as $item){
                if(isset($location_id[$item->priority]))
                    array_push($location_id[$item->priority], $item->id);
            }
        }
        // Get physical progress based on priority
        $physical_progress_high   = getBudgetDone($location_id[1]);
        $physical_progress_medium = getBudgetDone($location_id[2]);
        $physical_progress_low    = getBudgetDone($location_id[3]);
        $all_locations = array_merge($location_id[1],$location_id[2],$location_id[3]);
        // locations without base schedule
        $excluded_locations = Plan_locations::whereIn('id',$all_locations)->with('Projects')->doesntHave('bill_quantities_base')->get();
        $excluded_loc_id   = array();
        if($excluded_locations){
            foreach($excluded_locations as $item){
                $excluded_loc_id[] = $item->id;
            }
        }
        if($excluded_loc_id)
        {
            $all_locations = array_diff($all_locations,$excluded_loc_id);
        }
        // Get total of under construction projects
        $total_projects = Plans::select('projects.priority', 'pl.id', DB::raw('SUM(bq.total_price) as price'))
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->join('estimations_bill_quantities as bq','bq.project_location_id','=','pl.id')
                            ->whereIn('pl.id', $all_locations)->groupBy('pl.id')->get();
        // Set default value
        $projects = array(
            '1' => array('total' => 0, 'price' => 0),
            '2' => array('total' => 0, 'price' => 0),
            '3' => array('total' => 0, 'price' => 0)
        );
        // Arrange data
        if($total_projects){
            foreach($total_projects as $item){
                $projects[$item->priority]['total'] += 1;
                $projects[$item->priority]['price'] += $item->price;
            }
        }
        // Get total projects based on zones
        $zone_projects = Plan_locations::select('pr.zone', DB::raw('count(project_locations.id) as total'))
                                        ->whereIn('project_locations.id', $all_locations)
                                        ->join('pmis_auth.provinces as pr','project_locations.province_id','=','pr.id')
                                        ->groupBy('pr.zone')->get();
        // Set default value
        $zones = array('01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0);
        // Arrange data
        if($zone_projects){
            foreach($zone_projects as $item){
                $zones[$item->zone] = $item->total;
            }
        }
        // Get projects with budgets
        $project_budget = Plan_locations::select('project_locations.project_id','project_locations.id', DB::raw('SUM(bq.total_price) as total_price'))
                                        ->join('estimations_bill_quantities as bq','bq.project_location_id','=','project_locations.id')
                                        ->whereIn('bq.project_location_id',$all_locations)
                                        ->groupBy('project_locations.id')
                                        ->orderBy('project_locations.project_id', 'asc')
                                        ->get()->toArray();
        // Get location activity done
        $getLocationsBudgetDone = getOneLocationBudgetDone($all_locations);
        // Set default values
        $performance = array();
        foreach($all_locations as $item){
            // Get first date of planned schedule
            $getStartDate = getFirstLastDateOfProject(array($item),'min(start_date)','project_id');
            // Get todays date
            $getEndDate = date('Y-m-d');
            // Get project actual performance in field
            $spi = 0;
            // Get project location week days
            $WeekDays = weekDaysAll(array($item));
            // Get project location holidays
            $holidays = holidaysAll(array($item));
            // Get planned budget
            $plannedpBudget = getWorkingBudgetPlaned(array($item));
            // Calcualte working budget from start of project till now
            $totalWorking = setCfyDateRange($plannedpBudget,$getStartDate,$getEndDate,0,$WeekDays,$holidays);
            $totalWorking = sortBudgetDataCfy($totalWorking,$WeekDays,$holidays);
            // Get planed budget from start of project till now
            $budgetPlaned = (count($totalWorking) > 0 ? array_sum(array_column($totalWorking,'total_price')) : 0 );
            if(isset($getLocationsBudgetDone[$item]) and $budgetPlaned>0)
            {
                $spi = round($getLocationsBudgetDone[$item]/$budgetPlaned,4);
            }
            $performance[] = array('location_id'=>$item,'spi'=>$spi);
        }
        $project_spi = array(
            'good'   => 0,
            'normal' => 0,
            'risk'   => 0
        );
        if($performance){
            foreach($performance as $item){
                if($item['spi']<=0.55)
                    $project_spi['risk'] +=1;
                elseif($item['spi']<=0.8)
                    $project_spi['normal'] +=1;
                elseif($item['spi']>0.8)
                    $project_spi['good'] +=1;
            }
        }
        // Load view to display data
        return view('dashboard/dashboard', compact('zone','provinces','projects','lang','excluded_locations','all_locations','physical_progress_high','physical_progress_medium','physical_progress_low','zones','project_spi'));
      }
      else // System page
      {
        // Get All Departments By Module
        $data['departments'] = get_departments($mod);
        // Get Main Page's Slider and Content
        $data['content'] = Dashboard::get()->first();
        //Get Task Notification
        $data['tasks'] = Task::where(function($query){
          if(is_admin()) {
            //Can view all records
          }
          else{
            $query->where('user',userid());
          }
        })->where('status','<>',3)->get()->count();
        // Get Zone and Provinces
        $data['provinces'] = Province::all();
        $data['zone'] = Province::select('zone as zcode','zname_'.$lang.' as zname')->groupBy('zone')->get();
        // Get project based on zone
        $data['projectByZone'] = Plan_locations::select('prov.zname_'.$lang.' as zone','prov.zone as zcode',DB::raw('COUNT(province_id) as total_projects'))
                ->join('projects as proj','proj.id','=','project_locations.project_id')
                ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                ->groupBy('prov.zone')->get();
        // Get project budget from working
        $working_budget = Billofquantities::select(DB::raw('IFNULL(SUM(total_price),0) as total_price'),'prov.zone as zcode')
              ->join('project_locations as loc','estimations_bill_quantities.project_location_id','=','loc.id')
              ->join('pmis_auth.provinces as prov','prov.id','=','loc.province_id')
              ->groupBy('prov.zone')->get();

        $project_budget = array();
        if($working_budget){
          foreach($working_budget as $item){

            $project_budget[$item->zcode] = round($item->total_price);
          }
        }
        $data['project_budget'] = $project_budget;
        // Total Projects
        $data['total_projects'] = Plans::select(DB::raw('COUNT(project_locations.id) as total_projects'))
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->first()->total_projects;

        // Get record from projects
        $plan_data = Requests::select(DB::raw('IFNULL(count(project_locations.id),0) as total'),DB::raw('IFNULL(projects.process,2) as status'))
              ->leftJoin('projects','projects.request_id','=','requests.id')
              ->join('shares','shares.record_id','=','requests.id')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('shares.share_to_code','pmis_plan')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('projects.process');
        // Get projects from plan
        $plan = $plan_data;
        // Get record from surveys
        $survey_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'))
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->leftJoin('surveys','surveys.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->where('shares.share_from_code','pmis_plan')
              ->where('shares.share_to_code','pmis_survey')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('surveys.process');
        // Get projects from survey
        $survey = $survey_data;
        // Get record from architectures
        $design_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'))
              ->leftJoin('architectures','architectures.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('shares.share_from_code','pmis_survey')
              ->where('shares.share_to_code','pmis_design')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('architectures.process');
        // Get projects from design
        $design = $design_data;
        // Get record from estimations
        $estimation_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'))
              ->leftJoin('estimations','estimations.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('shares.share_from_code','pmis_design')
              ->where('shares.share_to_code','pmis_estimation')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('estimations.process');
        // Get projects from estimation
        $estimation = $estimation_data;
        // Get record from implements
        $checklist_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'))
              ->leftJoin('implements','implements.project_id','=','projects.id')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->where('shares.share_from_code','pmis_estimation')
              ->where('shares.share_to_code','pmis_implement')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('implements.process');
        // Get projects from checklist
        $checklist = $checklist_data;
        // Under technical documentation
        $tech_doc = $plan_data->unionAll($survey_data)->unionAll($design_data)->unionAll($estimation_data)->unionAll($checklist_data)->get();
        $under_tech_doc = 0;
        if($tech_doc){
          foreach($tech_doc as $item){
            if($item->status!='1'){
              $under_tech_doc = $under_tech_doc + $item->total;
            }
          }
        }
        $data['under_tech_doc'] = $under_tech_doc;
        // Get record from procurements
        $procurement_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'))
              ->leftJoin('procurements','procurements.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('shares.share_from_code','pmis_implement')
              ->where('shares.share_to_code','pmis_procurement')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')
              ->groupBy('procurements.process')->get();
        // Under technical documentation
        $total_procurements = 0;
        if($procurement_data){
          foreach($procurement_data as $item){
            if($item->status!='1'){
              $total_procurements += $item->total;
            }
          }
        }
        $data['total_procurements'] = $total_procurements;
        // Get under construction, completed and stopped projects
        $construction_data = Plans::select(DB::raw('count(project_locations.id) as total'),'project_locations.status as status')
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->join('shares','shares.record_id','=','projects.id')
              ->where('shares.share_from_code','pmis_procurement')
              ->where('shares.share_to_code','pmis_progress')
              ->where('project_locations.status','0')
              ->whereNull('shares.deleted_at')->first()->total;
        $completed_data = Plans::select(DB::raw('count(project_locations.id) as total'))
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('project_locations.status','1')->first()->total;
        $stopped_data = Plans::select(DB::raw('count(project_locations.id) as total'))
              ->join('project_locations','project_locations.project_id','=','projects.id')
              ->where('project_locations.status','2')->first()->total;
        // Under technical documentation
        $construction_projects = 0;
        $completed_projects = 0;
        $stopped_projects = 0;
        if($construction_data){
          $construction_projects = $construction_data;
        }
        if($completed_data){
          $completed_projects = $completed_data;
        }
        if($stopped_data){
          $stopped_projects = $stopped_data;
        }
        $data['construction_projects'] = $construction_projects;
        $data['completed_projects']    = $completed_projects;
        $data['stopped_projects']      = $stopped_projects;
        // Get projects from plan
        $plans = $plan->get();
        $plan_projects = 0;
        if($plan){
          foreach($plan as $item){
            if($item->status!='1'){
              $plan_projects += $item->total;
            }
          }
        }
        $data['plan_projects'] = $plan_projects;
        // Get projects from survey
        $surveys = $survey->get();
        $survey_projects = 0;
        if($surveys){
          foreach($surveys as $item){
            if($item->status!='1'){
              $survey_projects += $item->total;
            }
          }
        }
        $data['survey_projects'] = $survey_projects;
        // Get projects from design
        $designs = $design->get();
        $design_projects = 0;
        if($designs){
          foreach($designs as $item){
            if($item->status!='1'){
              $design_projects += $item->total;
            }
          }
        }
        $data['design_projects'] = $design_projects;
        // Get projects from estimation
        $estimations = $estimation->get();
        $estimation_projects = 0;
        if($estimations){
          foreach($estimations as $item){
            if($item->status!='1'){
              $estimation_projects += $item->total;
            }
          }
        }
        $data['estimation_projects'] = $estimation_projects;
        // Get projects from checklist
        $checklists = $checklist->get();
        $checklist_projects = 0;
        if($checklists){
          foreach($checklists as $item){
            if($item->status!='1'){
              $checklist_projects += $item->total;
            }
          }
        }
        $data['checklist_projects'] = $checklist_projects;
        // Load view to display data
        return view('home',$data);
      }
    }

    /**
     * Get all sections by department.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringSections($enc_id)
    {
      session(['current_section' => "0"]);
      session(['current_department' => $enc_id]);
      // Get language
      $lang = get_language();
      $data['lang'] = $lang;
      // Decrypt ID
      $id = decrypt($enc_id);
      $data['enc_depId'] = $enc_id;
      session()->forget('project_id');
      // Get Department's Name
      $data['department_name'] = getDepartmentNameByID($id,$lang);
      // Get All Department's Sections
      $data['sections'] = get_sections($id);
      //ddd($data['sections']);
      $data['segments'] = request()->segments();
      return view('entities',$data);
    }

    /**
     * Get all contractors.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringContractors($enc_id)
    {
      session(['current_section' => ""]);
      session(['current_department' => $enc_id]);
      // Get language
      $lang = get_language();
      // Decrypt ID
      $id = decrypt($enc_id);
      $data['enc_depId'] = $enc_id;
      // Get Department's Name
      $data['department_name'] = getDepartmentNameByID($id,$lang);
      // Get All Department's Contractors
      $data['contractors'] = get_contractors($id);
      $data['segments'] = request()->segments();
      return view('contractors',$data);
    }

    /**
     * Get all sections by contractors.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringContractorSections($dep_id=0,$con_id=0)
    {
      session(['current_section' => "0"]);
      session(['current_department' => $dep_id]);
      session(['current_contractor' => $con_id]);
      // Get language
      $lang = get_language();
      $data['lang'] = $lang;
      // Decrypt Department ID
      $depId = decrypt($dep_id);
      $data['enc_depId'] = $dep_id;
      // Decrypt Contractor ID
      $conId = decrypt($con_id);
      $data['enc_conId'] = $con_id;
      // Get Contractor Name
      $data['contractor'] = Contractor::find($conId);
      // Get All Department's Sections
      $data['sections'] = get_sections($depId);
      $data['segments'] = request()->segments();
      // Load view to display data
      return view('contractor_section',$data);
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-12-17 14:37:38
     * @Desc: Destory Attachment
     */
    public function destoryAttachment(Request $request)
    {
      $id    = decrypt($request->record_id);
      $table = decrypt($request->table);
      $document = Home::getFile($table,$id)->path;
      $file= public_path()."/".$document;

      if(File::delete($file))
			{
        $result = Home::deleteRecord($table,array('id' => $id));
        if($result)
          return successMessage(trans('global.success_delete_msg'));
        else
          return errorMessage(trans('global.failed_delete_msg'));
      }
      else
      {
        return errorMessage(trans('global.failed_delete_msg'));
      }
    }

    /**
     * Download documents.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadDoc($id=0,$table='',$folder='')
  	{
      $id = decrypt($id);
  		//get file name from database
  		$fileName = Home::getFileName($table,$id)->file_name;
      //public path for file
      $file= public_path(). "public/attachments/pmis/".$folder."/".$fileName;
      //download file
      if(file_exists($file))
      {
        return Response::download($file, $fileName);
      }
      else
      {
        Session()->flash('fail',  __("global.file_not_found"));
        return back();
      }
  	}

    /**
     * Bring project types.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringProjectTypes()
    {
      $id = Input::get('id');
      $data['types'] = Home::getAllProjectTypesByCategory($id);
      return view('pmis/plans/typesDropdown',$data);
    }

    /**
     * Bring districts.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringDistricts()
    {
      $id = Input::get('id');
      $data['counter'] = Input::get('counter');
      $data['types'] = Home::getAllDistrictsByProvince($id);
      return view('pmis/plans/districtsDropdown',$data);
    }

    /**
     * Bring villages.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringVillages()
    {
      $id = Input::get('id');
      $data['counter'] = Input::get('counter');
      $data['types'] = Home::getAllVillagesByDistrict($id);
      return view('pmis/plans/villageDropdown',$data);
    }

    /**
     * Change status of notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function markAsRead()
    {
      $id = Input::get('id');
      Auth::user()->unreadNotifications->where('id', $id)->markAsRead();
    }

    /**
     * List of attachments.
     *
     * @return \Illuminate\Http\Response
     */
    public function attachments_list($enc_id=0,$table='')
    {
      $data['enc_id'] = $enc_id;
      $data['table']  = $table;
      //Get Attachments
      $data['attachments'] = Home::getAllAttachments($id,$table);
      // Load view to show result
      return view('pmis/attachments/modal_ajax',$data);
    }

    /**
     * Store attachments.
     *
     * @return \Illuminate\Http\Response
     */
    public function store_attachments(Request $request)
    {
      // Validate the request...
      $validates = $request->validate([
        'file_name'  => 'required',
        'file'       => 'required',
      ]);
      // Get Language
      $lang = get_language();

      // Getting post data
      $file = $request->file;// Get File
      $parent_id = decrypt($request->parent_id);
      $table     = decrypt($request->table);
      $record_id = decrypt($request->record_id); // Record id
      $section   = decrypt($request->section);
      $file_name = $request->file_name;
      $errors = "";
      if($request->hasFile('file'))
      {
        // Validating each file.
        $rules = array('file' => 'required'); //'required|mimes:jpg,jpeg,png,doc,docx,pdf,xls,ppt,pptx,xlsx'
        $validator = Validator::make(
          [
              'file'      => $file,
              'extension' => Str::lower($file->getClientOriginalExtension()),
          ],
          [
              'file'      => 'required|max:5000000',
              'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx,skp,dwg,sab,rft,bim,dwl,dxf,wtg,gis,mdb,sca,scada,ep.anet'
          ]
        );
        if($validator->passes())
        {
          // Path is pmis/public/attachments/pmis...
          $destinationPath = 'public/attachments/pmis/'.$section;
          $fileOrgName     = $file->getClientOriginalName();
          $fileExtention   = $file->getClientOriginalExtension();
          $fileOrgSize     = $file->getSize();
          if($file_name!="")
          {
            $name = $file_name;
            $file_name = $file_name."ـ".$record_id.".".$fileExtention;
          }
          else
          {
            $name = explode('.', $fileOrgName)[0];
            $file_name = $fileOrgName;
          }
          deleteFile($table,$request,($request->is_edit==true?true:false));
          $upload_success = $file->move($destinationPath, $file_name);
          if($upload_success)
          {
              $destinationPathForDB = 'attachments/pmis/'.$section;
              $data = [
                  'filename'    => $name,
                  'path'        => $destinationPathForDB."/".$file_name,
                  'extension'   => $fileExtention,
                  'size'        => $fileOrgSize,
                  'created_at'  => date('Y-m-d H:i:s'),
                  'created_by'  => Auth::user()->id,
                  'parent_id'   => $parent_id,
                  'record_id'   => $record_id,
                  'section'     => $section,
              ];

            $record = inserOrUpdatetAttachment($table,$data,$request);
            if($record)
            {
              //Get Attachments
              $request['attachments'] = getAttachments($table,$parent_id,$record_id,$section);
              // Load view to show result
              return view('pmis/attachments/modal_load',$request->all());
            }
            else
            {
              Session()->flash('fail', __("global.failed_att_msg"));
              return '
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                  <div class="m-alert__icon"><i class="la la-warning"></i></div>
                  <div class="m-alert__text">'.__("global.success_att_msg").'</div>
                  <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                </div>
              ';
            }
          }
          else
          {
            Session()->flash('fail', __("global.failed_att_msg"));
            return '
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                  <div class="m-alert__icon"><i class="la la-warning"></i></div>
                  <div class="m-alert__text">'.__("global.success_att_msg").'</div>
                  <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                </div>
              ';
          }
        }
        else
        {
          // Return false.
          Session()->flash('fail', __("global.failed_att_msg"));
          return '
                <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                  <div class="m-alert__icon"><i class="la la-warning"></i></div>
                  <div class="m-alert__text">'.__("global.success_att_msg").'</div>
                  <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                </div>
              ';
        }
      }
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-12-19 13:30:28
     * @Desc: Store Document
     */
    public function storeDocument(DocumentRequest $request)
    {
      // Get Language
      $lang = get_language();
      // Getting post data
      $file = $request->file;// Get File
      $table     = decrypt($request->table);
      $record_id = decrypt($request->record_id); // Record id
      $section   = decrypt($request->section);
      $file_name = $request->file_name;
      $errors = "";
      // Path is pmis/public/attachments/pmis...
      $destinationPath = 'public/attachments/pmis/'.$section;
      $fileOrgName     = $file->getClientOriginalName();
      $fileExtention   = $file->getClientOriginalExtension();
      $fileOrgSize     = $file->getSize();
      // Delete the file if it's update
      deleteFile($table,$request,($request->is_edit==true?true:false));
      if($file_name!="")
      {
        $name = $file_name;
        $file_name = $file_name."ـ".$record_id."_".Carbon::now().".".$fileExtention;
      }
      else
      {
        $name = explode('.', $fileOrgName)[0];
        $file_name = $fileOrgName."ـ".$record_id."_".Carbon::now();
      }

      $upload_success = $file->move($destinationPath, $file_name);
      if($upload_success)
      {
          $destinationPathForDB = 'attachments/pmis/'.$section;
          $data = [
            'record_id'     => $record_id,
            'document_type' => $request->document_type,
            'filename'      => $name,
            'path'          => $destinationPathForDB."/".$file_name,
            'extension'     => $fileExtention,
            'size'          => $fileOrgSize,
            'created_at'    => date('Y-m-d H:i:s'),
          ];

        $record = inserOrUpdatetAttachment($table,$data,$request);
        if($record)
        {
          //Get Attachments
          $request['documents'] = Survey_documents::where('record_id',$record_id)->with('documents_type')->get();
          // Load view to show result
          return view('pmis/attachments/load_modal_document',$request->all());
        }
        else
        {
          Session()->flash('fail', __("global.failed_att_msg"));
          return '
            <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
              <div class="m-alert__icon"><i class="la la-warning"></i></div>
              <div class="m-alert__text">'.__("global.success_att_msg").'</div>
              <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
          ';
        }
      }
      else
      {
        Session()->flash('fail', __("global.failed_att_msg"));
        return '
            <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
              <div class="m-alert__icon"><i class="la la-warning"></i></div>
              <div class="m-alert__text">'.__("global.success_att_msg").'</div>
              <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
            </div>
          ';
      }
    }

    /**
     * Bring more attachments.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringMoreAttachments()
    {
      $data['number'] = Input::get('number');
      // Return view
      return view('pmis/attachments/moreAttachments',$data);
    }

    /**
     * Download attachments.
     *
     * @return \Illuminate\Http\Response
     */
    public function DownloadAttachments($id=0,$table='')
  	{
      $id = decrypt($id);
  		//get data from database
  		$data = Home::getAttachment($id,$table);
      //get file name from database
      $fileName = $data->filename.".".$data->extension;
      //public path for file
      $file = 'public/'.$data->path;
      //download file
      if(file_exists($file))
      {
        return Response::download($file, $fileName);
      }
      else
      {
        Session()->flash('fail',  __("global.file_not_found"));
        return back();
      }
    }

    /**
     * Create Attachment
     */
    function createAttachment(Request $request)
    {
      $request['record_id'] = (isset($request->record_id)?$request->record_id:encrypt(0));
      return view('pmis.attachments.create',$request->all());
    }

    /**
     * Create Attachment
     */
    function viewAttachment($id=0,$table='')
    {
      $data['id'] = $id;
      $data['table']  = $table;
      //Get Attachments
      $data['attachments'] = Home::getAttachment($id,$table);
      //ddd($data['attachments']);
      // Load view to show result
      return view('pmis/attachments/modal_view',$data);
    }

    /**
     * Edit Attachments
     */
    public function editAttachment(Request $request)
    {
      return view('pmis.attachments.edit',$request->all());
    }

    /**
     * Create Attachment
     */
    function createDocument(Request $request)
    {
      $request['record_id'] = (isset($request->record_id)?$request->record_id:encrypt(0));
      $request['document_types'] = getRecords('pmis','static_data',['type' => 4]);
      session(['document_table' => 'survey_documents']);

      return view('pmis.attachments.create_document',$request->all());
    }

    /**
     * Edit Documents
     */
    public function editDocument(Request $request)
    {
      $request['record_id'] = (isset($request->record_id)?$request->record_id:encrypt(0));
      $request['document_types'] = getRecords('pmis','static_data',['type' => 4]);
      $request['table'] = encrypt('survey_documents');
      return view('pmis.attachments.edit_document',$request->all());
    }

    /**
     * Change records status.
     *
     * @return \Illuminate\Http\Response
     */
    public function completeRecords()
    {
      $id         = decrypt(Input::get('rec_id'));
      $project_id = decrypt(Input::get('project_id'));
      $table      = Input::get('table');
      // Run Query
      $query = DB::connection('pmis')->table($table)->whereId($id)->update(['completed' => '1']);
      if($query)
      {
        // Send notification for request section
        if($table=="requests")
        {
          // Start Notification and get List of users which has rules of Approval in Requests
          $emp = User::getUserRoleCode('req_approval');
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('requests.show',encrypt($id));
          if($users)
          {
            foreach($users as $user) {
              $user->notify(new NewProject($id, 'global.request_complete', Auth::user()->id, $redirect_url));
            }
          }
        }

        // Send notification for architecture section
        if($table=="architectures")
        {
          // Start Notification and get List of users which has rules of arch_visa in visa
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('vis_approval');//Get users who have role of Visa Approval
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('visa.show', encrypt($project_id));
          if($users)
          {
            foreach($users as $user) {
              $user->notify(new NewProject($project_id,trans('global.architecture_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }
        }

        // Send notification for structure section
        if($table=="structures")
        {
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('vis_approval');//Get users who have role of str_visa
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('visa.show', encrypt($project_id));
          if($users)
          {
            foreach($users as $user) {
              $user->notify(new NewProject($project_id, trans('global.structure_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }
        }

        // Send notification for electricity section
        if($table=='electricities')
        {
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('vis_approval');//Get users who have role of ele_visa
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('visa.show', encrypt($project_id));
          if($users)
          {
            foreach($users as $user) {
              $user->notify(new NewProject($project_id, trans('global.electricity_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }
        }

        // Send notification for water section
        if($table=='waters')
        {
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('vis_approval');//Get users who have role of wat_visa
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('visa.show', encrypt($project_id));
          if($users){
            foreach($users as $user) {
              $user->notify(new NewProject($project_id, trans('global.water_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }

        }

        // Send notification for electricity section
        if($table=='mechanics')
        {
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('vis_approval');//Get users who have role of mec_visa
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('visa.show', encrypt($project_id));
          if($users){
            foreach($users as $user) {
              $user->notify(new NewProject($project_id, trans('global.mechanic_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }
        }

        // Send notification for implement section
        if($table=='implements')
        {
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('imp_add');//Get users who have role of Visa Approval
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('implements.show', encrypt($project_id));
          foreach($users as $user) {
             $user->notify(new NewProject($project_id, trans('global.implements_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
          }
        }

        // Send notification for implement section
        if($table=='procurements')
        {
          //Pending .................
          // $emp   = User::getUserRoleCode('imp_add');//Get users who have role of Visa Approval
          // $users = User::whereIn('id',$emp)->get();
          // $redirect_url = route('implements.show', encrypt($project_id));
          // foreach($users as $user) {
          //    $user->notify(new NewProject($project_id, trans('global.implements_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
          // }
        }

        // Send notification for electricity section
        if($table=='daily_reports')
        {

          // Run Query and remove status from table
          $query = DB::connection('pmis')->table($table)->whereId($id)->update(['status' => '0','status_by' => null, 'status_date' => null, 'description' => null]);

          // Start Notification and get List of users which has rules of arch_visa in visa
          $project_name = Plans::whereId($project_id)->first()->name;
          $emp   = User::getUserRoleCode('daily_monitoring_approval');//Get users who have role of report Approval
          $users = User::whereIn('id',$emp)->get();
          $redirect_url = route('daily_report_monitoring/list', encrypt($project_id));
          if($users)
          {
            foreach($users as $user) {
              $user->notify(new NewProject($project_id,trans('global.daily_report_added',['pro'=>$project_name]), Auth::user()->id, $redirect_url));
            }
          }
        }
        // End Notification
        Session()->flash('success', __("global.completed_success_msg"));
      }
      else
      {
        Session()->flash('fail', __("global.failed_msg"));
      }
    }

    /**
     * Bring users.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringUsers()
    {
      $id = Input::get('id');
      $data['users'] = Users::where('contractor_id',$id)->get();
      $data['disabled'] = '';
      return view('settings/projects/users',$data);
    }

    /**
     * Bring selected users.
     *
     * @return \Illuminate\Http\Response
     */
    public function bringSelectedUsers()
    {
      return view('settings/projects/selected_users',$data);
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-12-17 14:37:38
     * @Desc: Destory Attachment
     */
    public function deleteUpdatedFile($id=0,$table='')
    {
      $id    = decrypt($id);
      $table = $table;
      $document = Home::getFile($table,$id)->path;
      $file= public_path()."/".$document;
      if(File::delete($file))
			{
        $data = array
        (
          'path'      => null,
          'extension' => null,
          'size'      => null,
        );
        $query = DB::connection('pmis')->table($table)->whereId($id)->update($data);
        if($query)
          return "true";
        else
          return "false";
      }
      else
      {
        return "false";
      }
    }

    /**
     * Generate search options.
     *
     * @return \Illuminate\Http\Response
     */
    public function SearchOption(Request $request)
    {
        switch ($request->type)
        {
          case 'department_id': //List Deparment
            $departments = Home::getData('pmis_auth','hr_departments');
            $option = '<select class="form-control m-input m-input--air select-2" name="value" id="search_value" style="width:100%;">';
            $option.='<option value="">'.trans('global.select').'</option>';
            if(count($departments)>0)
            {
              foreach($departments as $item)
              {
                 $option.='<option value='.$item->id.'>'.$item->name_dr.'</option>';
              }
            }
            $option.='</select>';
            return $option;
          break;

          case 'process': // Request status 0 => pending, 1=> approve & 2 => rejected
            $option = '<select class="form-control m-input m-input--air select-2" name="value" id="search_value" style="width:100%;">';
            $option.='<option value="">'.trans('global.select').'</option>
                      <option value="0">'.trans('requests.pending').'</option>
                      <option value="1">'.trans('requests.approved').'</option>
                      <option value="2">'.trans('requests.rejected').'</option>';
            $option.='</select>';
            return $option;
          break;

          case 'p.process': // Request status 0 => pending, 1=> approve & 2 => rejected
          case 'projects.process':
          case 's.process':
            $option = '<select class="form-control m-input m-input--air select-2" name="value" id="search_value" style="width:100%;">';
            $option .='<option value="">'.trans('global.select').'</option>
                      <option value="0">'.trans('global.under_process').'</option>
                      <option value="1">'.trans('global.completed').'</option>
                      <option value="2">'.trans('global.pending').'</option>';
            $option .='</select>';
            return  $option;
          break;
          default:
            # code...
          break;
       }
    }

    /**
     * @Author: Muhammad Ehsan Mujadidi
     * @Date: 2020-08-05
     * @Desc: Destory Attachment
     */
    public function destroyAttachments($id=0,$table='')
    {
      $id    = decrypt($id);
      $table = $table;
      $document = Home::getFile($table,$id);
      $file= public_path()."/".$document->path;
      // check if attachment belongs to multi records
      if($table=='change_request_attachment' and $document->is_multiple==1){
        // Delete record
        DB::connection('pmis')->table($table)->whereId($id)->delete();
        return "true";
      }
      // Delete file from server
      if(File::delete($file))
			{
        // Delete record
        $query = DB::connection('pmis')->table($table)->whereId($id)->delete();
        if($query)
          return "true";
        else
          return "false";
      }
      else
      {
        return "false";
      }
    }

    /**
     * Show filtered projects based on zone.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardFilter(Request $request)
    {
      $lang = get_language();
      $data['lang'] = $lang;
      // Check if filter condition
      $condition = $request->condition;
      // Check if all zones selected
      if($condition=='zone')
      {
        if(in_array('-',$request->zone)){
          $condition = 'all';
        }
      }
      switch ($condition)
      {
        case 'zone': // Get projects by zone
            $zone = $request->zone;
            // Get Zone and Provinces
            $data['locations'] = Province::whereIn('zone',$zone)->get();
            // Get project based on zone
            $data['projectByZone'] = Plan_locations::select('prov.name_'.$lang.' as province_name','prov.id as pro_id','prov.zone as zcode',DB::raw('IFNULL(COUNT(province_id),0) as total_projects'))
                ->join('projects as proj','proj.id','=','project_locations.project_id')
                ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                ->whereIn('prov.zone',$zone)->groupBy('project_locations.province_id')->orderBy('prov.zone', 'asc')->orderBy('prov.id','asc')->get();
            // Get project budget from working
            $working_budget = Billofquantities::select(DB::raw('IFNULL(SUM(total_price),0) as total_price'),'prov.id as pro_id')
                ->join('project_locations as loc','estimations_bill_quantities.project_location_id','=','loc.id')
                ->join('pmis_auth.provinces as prov','prov.id','=','loc.province_id')
                ->whereIn('prov.zone',$zone)
                ->groupBy('loc.province_id')->get();
            $project_budget = array();
            if($working_budget){
              foreach($working_budget as $item){
                $project_budget[$item->pro_id] = round($item->total_price);
              }
            }
            $data['project_budget'] = $project_budget;
            // Get record from projects
            $plan_data = Requests::select(DB::raw('IFNULL(count(project_locations.id),0) as total'),DB::raw('IFNULL(projects.process,2) as status'))
                  ->leftJoin('projects','projects.request_id','=','requests.id')
                  ->join('shares','shares.record_id','=','requests.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_to_code','pmis_plan')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('projects.process');
            // Get record from surveys
            $survey_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'))
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->leftJoin('surveys','surveys.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_plan')
                  ->where('shares.share_to_code','pmis_survey')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('surveys.process');
            // Get record from architectures
            $design_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'))
                  ->leftJoin('architectures','architectures.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_survey')
                  ->where('shares.share_to_code','pmis_design')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('architectures.process');
            // Get record from estimations
            $estimation_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'))
                  ->leftJoin('estimations','estimations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_design')
                  ->where('shares.share_to_code','pmis_estimation')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('estimations.process');
            // Get record from implements
            $checklist_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'))
                  ->leftJoin('implements','implements.project_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_estimation')
                  ->where('shares.share_to_code','pmis_implement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('implements.process');
            // Get record from procurements
            $procurement_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'))
                  ->leftJoin('procurements','procurements.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_implement')
                  ->where('shares.share_to_code','pmis_procurement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('procurements.process')->get();
            // Under technical documentation
            $total_procurements = 0;
            if($procurement_data){
              foreach($procurement_data as $item){
                if($item->status!='1'){
                  $total_procurements += $item->total;
                }
              }
            }
            $data['total_procurements'] = $total_procurements;
            // Get under construction, completed and stopped projects
            $construction_data = Plans::select(DB::raw('count(project_locations.id) as total'),'project_locations.status as status')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->whereIn('prov.zone',$zone)
                  ->where('shares.share_from_code','pmis_procurement')
                  ->where('shares.share_to_code','pmis_progress')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('project_locations.status')->get();
            // Under technical documentation
            $construction_projects = 0;
            $completed_projects = 0;
            $stopped_projects = 0;
            if($construction_data){
              foreach($construction_data as $item){
                if($item->status==0){
                  $construction_projects += $item->total;
                }elseif($item->status==1){
                  $completed_projects += $item->total;
                }elseif($item->status==2){
                  $stopped_projects += $item->total;
                }
              }
            }
            $data['construction_projects'] = $construction_projects;
            $data['completed_projects']    = $completed_projects;
            $data['stopped_projects']      = $stopped_projects;
            // Get projects from plan
            $plans = $plan_data->get();
            $plan_projects = 0;
            if($plans){
              foreach($plans as $item){
                if($item->status!='1'){
                  $plan_projects += $item->total;
                }
              }
            }
            $data['plan_projects'] = $plan_projects;
            // Get projects from survey
            $surveys = $survey_data->get();
            $survey_projects = 0;
            if($surveys){
              foreach($surveys as $item){
                if($item->status!='1'){
                  $survey_projects += $item->total;
                }
              }
            }
            $data['survey_projects'] = $survey_projects;
            // Get projects from design
            $designs = $design_data->get();
            $design_projects = 0;
            if($designs){
              foreach($designs as $item){
                if($item->status!='1'){
                  $design_projects += $item->total;
                }
              }
            }
            $data['design_projects'] = $design_projects;
            // Get projects from estimation
            $estimations = $estimation_data->get();
            $estimation_projects = 0;
            if($estimations){
              foreach($estimations as $item){
                if($item->status!='1'){
                  $estimation_projects += $item->total;
                }
              }
            }
            $data['estimation_projects'] = $estimation_projects;
            // Get projects from checklist
            $checklists = $checklist_data->get();
            $checklist_projects = 0;
            if($checklists){
              foreach($checklists as $item){
                if($item->status!='1'){
                  $checklist_projects += $item->total;
                }
              }
            }
            $data['checklist_projects'] = $checklist_projects;
            // Load view to show data
            return view('dashboard_filter_zone',$data);
        break;
        case 'province': // Get projects by province
            // Get Zone and Provinces
            $province = $request->province;
            $data['locations'] = District::where('province_id',$province)->get();
            // Get project based on zone
            $data['projectByZone'] = Plan_locations::select('dist.name_'.$lang.' as district_name','prov.zone as zcode','project_locations.district_id as district_id',DB::raw('IFNULL(COUNT(project_locations.district_id),0) as total_projects'))
                      ->join('projects as proj','proj.id','=','project_locations.project_id')
                      ->leftJoin('procurements as proc','proj.id','=','proc.project_id')
                      ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                      ->leftJoin('pmis_auth.districts as dist','dist.id','=','project_locations.district_id')
                      ->where('project_locations.province_id',$province)
                      ->groupBy('project_locations.district_id')->orderBy('dist.id','asc')->get();

            // Get project budget from working
            $working_budget = Billofquantities::select(DB::raw('IFNULL(SUM(total_price),0) as total_price'),'loc.district_id as district_id')
                ->join('project_locations as loc','estimations_bill_quantities.project_location_id','=','loc.id')
                ->join('pmis_auth.provinces as prov','prov.id','=','loc.province_id')
                ->leftJoin('pmis_auth.districts as dist','dist.id','=','loc.district_id')
                ->where('loc.province_id',$province)
                ->groupBy('loc.district_id')->get();
            $project_budget = array();
            if($working_budget){
              foreach($working_budget as $item){
                $project_budget[$item->district_id] = round($item->total_price);
              }
            }
            $data['project_budget'] = $project_budget;

            // Get record from projects
            $plan_data = Requests::select(DB::raw('IFNULL(count(project_locations.id),0) as total'),DB::raw('IFNULL(projects.process,2) as status'))
                  ->leftJoin('projects','projects.request_id','=','requests.id')
                  ->join('shares','shares.record_id','=','requests.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_to_code','pmis_plan')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('projects.process');
            // Get record from surveys
            $survey_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'))
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->leftJoin('surveys','surveys.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_plan')
                  ->where('shares.share_to_code','pmis_survey')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('surveys.process');
            // Get record from architectures
            $design_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'))
                  ->leftJoin('architectures','architectures.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_survey')
                  ->where('shares.share_to_code','pmis_design')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('architectures.process');
            // Get record from estimations
            $estimation_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'))
                  ->leftJoin('estimations','estimations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_design')
                  ->where('shares.share_to_code','pmis_estimation')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('estimations.process');
            // Get record from implements
            $checklist_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'))
                  ->leftJoin('implements','implements.project_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_estimation')
                  ->where('shares.share_to_code','pmis_implement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('implements.process');
            // Get record from procurements
            $procurement_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'))
                  ->leftJoin('procurements','procurements.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','prov.id','=','dist.province_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_implement')
                  ->where('shares.share_to_code','pmis_procurement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('procurements.process')->get();
            // Under technical documentation
            $total_procurements = 0;
            if($procurement_data){
              foreach($procurement_data as $item){
                if($item->status!='1'){
                  $total_procurements += $item->total;
                }
              }
            }
            $data['total_procurements'] = $total_procurements;
            // Get under construction, completed and stopped projects
            $construction_data = Plans::select(DB::raw('count(project_locations.id) as total'),'project_locations.status as status')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                  ->join('pmis_auth.districts as dist','dist.id','=','project_locations.district_id')
                  ->where('dist.province_id',$province)
                  ->where('shares.share_from_code','pmis_procurement')
                  ->where('shares.share_to_code','pmis_progress')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('project_locations.status')->get();
            // Under technical documentation
            $construction_projects = 0;
            $completed_projects = 0;
            $stopped_projects = 0;
            if($construction_data){
              foreach($construction_data as $item){
                if($item->status==0){
                  $construction_projects += $item->total;
                }elseif($item->status==1){
                  $completed_projects += $item->total;
                }elseif($item->status==2){
                  $stopped_projects += $item->total;
                }
              }
            }
            $data['construction_projects'] = $construction_projects;
            $data['completed_projects']    = $completed_projects;
            $data['stopped_projects']      = $stopped_projects;
            // Get projects from plan
            $plans = $plan_data->get();
            $plan_projects = 0;
            if($plans){
              foreach($plans as $item){
                if($item->status!='1'){
                  $plan_projects += $item->total;
                }
              }
            }
            $data['plan_projects'] = $plan_projects;
            // Get projects from survey
            $surveys = $survey_data->get();
            $survey_projects = 0;
            if($surveys){
              foreach($surveys as $item){
                if($item->status!='1'){
                  $survey_projects += $item->total;
                }
              }
            }
            $data['survey_projects'] = $survey_projects;
            // Get projects from design
            $designs = $design_data->get();
            $design_projects = 0;
            if($designs){
              foreach($designs as $item){
                if($item->status!='1'){
                  $design_projects += $item->total;
                }
              }
            }
            $data['design_projects'] = $design_projects;
            // Get projects from estimation
            $estimations = $estimation_data->get();
            $estimation_projects = 0;
            if($estimations){
              foreach($estimations as $item){
                if($item->status!='1'){
                  $estimation_projects += $item->total;
                }
              }
            }
            $data['estimation_projects'] = $estimation_projects;
            // Get projects from checklist
            $checklists = $checklist_data->get();
            $checklist_projects = 0;
            if($checklists){
              foreach($checklists as $item){
                if($item->status!='1'){
                  $checklist_projects += $item->total;
                }
              }
            }
            $data['checklist_projects'] = $checklist_projects;
            // Load view to show data
            return view('dashboard_filter_province',$data);
            // Load view to show data
            return view('dashboard_filter',$data);
        break;
        case 'all': // Get all projects
            // Get Zone and Provinces
            $data['locations'] = Province::all();
            // Get project based on zone
            $data['projectByZone'] = Plan_locations::select('prov.zname_'.$lang.' as zone','prov.zone as zcode',DB::raw('COUNT(province_id) as total_projects'))
                ->join('projects as proj','proj.id','=','project_locations.project_id')
                ->join('pmis_auth.provinces as prov','prov.id','=','project_locations.province_id')
                ->groupBy('prov.zone')->get();
            // Get project budget from working
            $working_budget = Billofquantities::select(DB::raw('IFNULL(SUM(total_price),0) as total_price'),'prov.zone as zcode')
                ->join('project_locations as loc','estimations_bill_quantities.project_location_id','=','loc.id')
                ->join('pmis_auth.provinces as prov','prov.id','=','loc.province_id')
                ->groupBy('prov.zone')->get();
            $project_budget = array();
            if($working_budget){
              foreach($working_budget as $item){
                $project_budget[$item->zcode] = round($item->total_price);
              }
            }
            $data['project_budget'] = $project_budget;

            // Get record from projects
            $plan_data = Requests::select(DB::raw('IFNULL(count(project_locations.id),0) as total'),DB::raw('IFNULL(projects.process,2) as status'))
                  ->leftJoin('projects','projects.request_id','=','requests.id')
                  ->join('shares','shares.record_id','=','requests.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->where('shares.share_to_code','pmis_plan')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('projects.process');
            // Get record from surveys
            $survey_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(surveys.process,2) as status'))
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->leftJoin('surveys','surveys.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_plan')
                  ->where('shares.share_to_code','pmis_survey')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('surveys.process');
            // Get record from architectures
            $design_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(architectures.process,2) as status'))
                  ->leftJoin('architectures','architectures.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_survey')
                  ->where('shares.share_to_code','pmis_design')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('architectures.process');
            // Get record from estimations
            $estimation_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(estimations.process,2) as status'))
                  ->leftJoin('estimations','estimations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_design')
                  ->where('shares.share_to_code','pmis_estimation')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('estimations.process');
            // Get record from implements
            $checklist_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(implements.process,2) as status'))
                  ->leftJoin('implements','implements.project_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_estimation')
                  ->where('shares.share_to_code','pmis_implement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('implements.process');
            // Get record from procurements
            $procurement_data = Plans::select(DB::raw('count(DISTINCT(project_locations.id)) as total'),DB::raw('IFNULL(procurements.process,2) as status'))
                  ->leftJoin('procurements','procurements.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_implement')
                  ->where('shares.share_to_code','pmis_procurement')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('procurements.process')->get();
            // Under technical documentation
            $total_procurements = 0;
            if($procurement_data){
              foreach($procurement_data as $item){
                if($item->status!='1'){
                  $total_procurements += $item->total;
                }
              }
            }
            $data['total_procurements'] = $total_procurements;
            // Get under construction, completed and stopped projects
            $construction_data = Plans::select(DB::raw('count(project_locations.id) as total'),'project_locations.status as status')
                  ->join('project_locations','project_locations.project_id','=','projects.id')
                  ->join('shares','shares.record_id','=','projects.id')
                  ->where('shares.share_from_code','pmis_procurement')
                  ->where('shares.share_to_code','pmis_progress')
                  ->whereNull('shares.deleted_at')
                  ->groupBy('project_locations.status')->get();
            // Under technical documentation
            $construction_projects = 0;
            $completed_projects = 0;
            $stopped_projects = 0;
            if($construction_data){
              foreach($construction_data as $item){
                if($item->status==0){
                  $construction_projects += $item->total;
                }elseif($item->status==1){
                  $completed_projects += $item->total;
                }elseif($item->status==2){
                  $stopped_projects += $item->total;
                }
              }
            }
            $data['construction_projects'] = $construction_projects;
            $data['completed_projects']    = $completed_projects;
            $data['stopped_projects']      = $stopped_projects;
            // Get projects from plan
            $plans = $plan_data->get();
            $plan_projects = 0;
            if($plans){
              foreach($plans as $item){
                if($item->status!='1'){
                  $plan_projects += $item->total;
                }
              }
            }
            $data['plan_projects'] = $plan_projects;
            // Get projects from survey
            $surveys = $survey_data->get();
            $survey_projects = 0;
            if($surveys){
              foreach($surveys as $item){
                if($item->status!='1'){
                  $survey_projects += $item->total;
                }
              }
            }
            $data['survey_projects'] = $survey_projects;
            // Get projects from design
            $designs = $design_data->get();
            $design_projects = 0;
            if($designs){
              foreach($designs as $item){
                if($item->status!='1'){
                  $design_projects += $item->total;
                }
              }
            }
            $data['design_projects'] = $design_projects;
            // Get projects from estimation
            $estimations = $estimation_data->get();
            $estimation_projects = 0;
            if($estimations){
              foreach($estimations as $item){
                if($item->status!='1'){
                  $estimation_projects += $item->total;
                }
              }
            }
            $data['estimation_projects'] = $estimation_projects;
            // Get projects from checklist
            $checklists = $checklist_data->get();
            $checklist_projects = 0;
            if($checklists){
              foreach($checklists as $item){
                if($item->status!='1'){
                  $checklist_projects += $item->total;
                }
              }
            }
            $data['checklist_projects'] = $checklist_projects;
            // Load view to show data
            return view('dashboard_filter_all',$data);
        break;
      }
    }

    /**
     * Change project status by location.
     *
     * @return \Illuminate\Http\Response
     */
    public function projectStatus(Request $request)
    {
      // Validate the request...
      $validates = $request->validate([
        'description' => 'required',
        'file'        => 'required',
      ]);
      // Project Id
      $project_id = decrypt($request->project_id);
      // Location Id
      $location_id = decrypt($request->location_id);
      // Update record
      $record = Plan_locations::find($location_id);
      $record->status = $request->status;
      $record = $record->save();
      if($record)
      {
        // Insert into log
        $status = new PlanLocationStatus;
        $status->project_id   = $project_id;
        $status->location_id  = $location_id;
        $status->status       = $request->status;
        $status->description  = $request->description;
        $status->section      = $request->section;
        $status->created_at   = date('Y-m-d H:i:s');
        $status->created_by   = userid();
        $status = $status->save();
        // Upload Attachments
        if($request->hasFile('file'))
        {
          $file = $request->file('file');
          if(!empty($file))
          {
            // Validating the file.
            $validator = Validator::make
            (
              [
                  'file'      => $file,
                  'extension' => Str::lower($file->getClientOriginalExtension()),
              ],
              [
                  'file'      => 'required|max:500000',
                  'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
              ]
            );
            if($validator->passes())
            {
              // Path is pmis/public/attachments/pmis/complete_stop/...
              $destinationPath = 'public/attachments/pmis/complete_stop';
              $fileOrgName     = $file->getClientOriginalName();
              $fileExtention   = $file->getClientOriginalExtension();
              $fileOrgSize     = $file->getSize();
              $name = explode('.', $fileOrgName)[0];
              $file_name = $fileOrgName;

              $upload_success = $file->move($destinationPath, $file_name);
              if($upload_success)
              {
                $destinationPathForDB = 'attachments/pmis/complete_stop';
                // Insert into attachment
                $attachment = new PlanLocationStatusAttachment;
                $attachment->project_id           = $project_id;
                $attachment->project_location_id  = $location_id;
                $attachment->section              = $request->status;
                $attachment->filename             = $name;
                $attachment->path                 = $destinationPathForDB."/".$file_name;
                $attachment->extension            = $fileExtention;
                $attachment->size                 = $fileOrgSize;
                $attachment->created_at           = date('Y-m-d H:i:s');
                $attachment->created_by           = userid();
                $attachment                       = $attachment->save();
              }
            }
          }
          else
          {
            // Set message on session
            Session()->flash('att_failed', __("global.attach_failed"));
          }
        }
        // Set success session
        Session()->flash('success',  __("global.success_process_msg"));
      }
      else
      {
        Session()->flash('fail', __("global.failed_msg"));
      }
    }

    /**
     * Change project status for all locations.
     *
     * @return \Illuminate\Http\Response
     */
    public function projectStatusAll(Request $request)
    {
      // Validate the request...
      $validates = $request->validate([
        'description' => 'required',
        'file'        => 'required',
      ]);
      // Project Id
      $project_id = decrypt($request->project_id);
      if($project_id)
      {
        // Location Id
        $locations = Plan_locations::where('project_id',$project_id)->pluck('id')->toArray();
        foreach($locations as $location_id)
        {
          // Update record
          $record = Plan_locations::find($location_id);
          $record->status = $request->status;
          $record = $record->save();
          if($record)
          {
            // Insert into log
            $status = new PlanLocationStatus;
            $status->project_id   = $project_id;
            $status->location_id  = $location_id;
            $status->status       = $request->status;
            $status->description  = $request->description;
            $status->section      = $request->section;
            $status->created_at   = date('Y-m-d H:i:s');
            $status->created_by   = userid();
            $status = $status->save();
            // Upload Attachments
            if($request->hasFile('file'))
            {
              $file = $request->file('file');
              if(!empty($file))
              {
                // Validating the file.
                $validator = Validator::make
                (
                  [
                      'file'      => $file,
                      'extension' => Str::lower($file->getClientOriginalExtension()),
                  ],
                  [
                      'file'      => 'required|max:500000',
                      'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
                  ]
                );
                if($validator->passes())
                {
                  // Path is pmis/public/attachments/pmis/complete_stop/...
                  $destinationPath = 'public/attachments/pmis/complete_stop';
                  $fileOrgName     = $file->getClientOriginalName();
                  $fileExtention   = $file->getClientOriginalExtension();
                  $fileOrgSize     = $file->getSize();
                  $name = explode('.', $fileOrgName)[0];
                  $file_name = $fileOrgName;

                  $upload_success = $file->move($destinationPath, $file_name);
                  if($upload_success)
                  {
                    $destinationPathForDB = 'attachments/pmis/complete_stop';
                    // Insert into attachment
                    $attachment = new PlanLocationStatusAttachment;
                    $attachment->project_id           = $project_id;
                    $attachment->project_location_id  = $location_id;
                    $attachment->section              = $request->status;
                    $attachment->filename             = $name;
                    $attachment->path                 = $destinationPathForDB."/".$file_name;
                    $attachment->extension            = $fileExtention;
                    $attachment->size                 = $fileOrgSize;
                    $attachment->created_at           = date('Y-m-d H:i:s');
                    $attachment->created_by           = userid();
                    $attachment                       = $attachment->save();
                  }
                }
              }
              else
              {
                // Set message on session
                Session()->flash('att_failed', __("global.attach_failed"));
              }
            }
            // Set success session
            Session()->flash('success',  __("global.success_process_msg"));
          }
        }
      }
      else
      {
        Session()->flash('fail', __("global.failed_msg"));
      }
    }

    /**
     * Projects dashbaord.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        // Get language
        $lang = get_language();
        // Get all zone
        $zone = Province::select('zone as zcode','zname_'.$lang.' as zname')->groupBy('zone')->get();
        // Get all provinces
        $provinces = Province::all();
        // Get total of under construction projects
        $project_progress = Plans::select('pl.id','projects.priority')
                            ->whereHas('share', function($query){
                                $query->where('share_to_code','pmis_progress');
                            })
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->where('pl.status','0')->orderBy('projects.priority','asc')->get();
        // Set default value
        $location_id = array(
            '1' => array(),
            '2' => array(),
            '3' => array(),
        );
        // Arrange project location ids
        if($project_progress){
            foreach($project_progress as $item){
                if(isset($location_id[$item->priority]))
                    array_push($location_id[$item->priority], $item->id);
            }
        }
        // Get physical progress based on priority
        $physical_progress_high   = getBudgetDone($location_id[1]);
        $physical_progress_medium = getBudgetDone($location_id[2]);
        $physical_progress_low    = getBudgetDone($location_id[3]);
        $all_locations = array_merge($location_id[1],$location_id[2],$location_id[3]);
        // locations without base schedule
        $excluded_locations = Plan_locations::whereIn('id',$all_locations)->with('Projects')->doesntHave('bill_quantities_base')->get();
        $excluded_loc_id   = array();
        if($excluded_locations){
            foreach($excluded_locations as $item){
                $excluded_loc_id[] = $item->id;
            }
        }
        if($excluded_loc_id)
        {
            $all_locations = array_diff($all_locations,$excluded_loc_id);
        }
        // Get total of under construction projects
        $total_projects = Plans::select('projects.priority', 'pl.id', DB::raw('SUM(bq.total_price) as price'))
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->join('estimations_bill_quantities as bq','bq.project_location_id','=','pl.id')
                            ->whereIn('pl.id', $all_locations)->groupBy('pl.id')->get();
        // Set default value
        $projects = array(
            '1' => array('total' => 0, 'price' => 0),
            '2' => array('total' => 0, 'price' => 0),
            '3' => array('total' => 0, 'price' => 0)
        );
        // Arrange data
        if($total_projects){
            foreach($total_projects as $item){
                $projects[$item->priority]['total'] += 1;
                $projects[$item->priority]['price'] += $item->price;
            }
        }
        // Get total projects based on zones
        $zone_projects = Plan_locations::select('pr.zone', DB::raw('count(project_locations.id) as total'))
                                        ->whereIn('project_locations.id', $all_locations)
                                        ->join('pmis_auth.provinces as pr','project_locations.province_id','=','pr.id')
                                        ->groupBy('pr.zone')->get();
        // Set default value
        $zones = array('01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0);
        // Arrange data
        if($zone_projects){
            foreach($zone_projects as $item){
                $zones[$item->zone] = $item->total;
            }
        }
        // Get projects with budgets
        $project_budget = Plan_locations::select('project_locations.project_id','project_locations.id', DB::raw('SUM(bq.total_price) as total_price'))
                                        ->join('estimations_bill_quantities as bq','bq.project_location_id','=','project_locations.id')
                                        ->whereIn('bq.project_location_id',$all_locations)
                                        ->groupBy('project_locations.id')
                                        ->orderBy('project_locations.project_id', 'asc')
                                        ->get()->toArray();
        // Get location activity done
        $getLocationsBudgetDone = getOneLocationBudgetDone($all_locations);
        // Set default values
        $performance = array();
        foreach($all_locations as $item){
            // Get first date of planned schedule
            $getStartDate = getFirstLastDateOfProject(array($item),'min(start_date)','project_id');
            // Get todays date
            $getEndDate = date('Y-m-d');
            // Get project actual performance in field
            $spi = 0;
            // Get project location week days
            $WeekDays = weekDaysAll(array($item));
            // Get project location holidays
            $holidays = holidaysAll(array($item));
            // Get planned budget
            $plannedpBudget = getWorkingBudgetPlaned(array($item));
            // Calcualte working budget from start of project till now
            $totalWorking = setCfyDateRange($plannedpBudget,$getStartDate,$getEndDate,0,$WeekDays,$holidays);
            $totalWorking = sortBudgetDataCfy($totalWorking,$WeekDays,$holidays);
            // Get planed budget from start of project till now
            $budgetPlaned = (count($totalWorking) > 0 ? array_sum(array_column($totalWorking,'total_price')) : 0 );
            if(isset($getLocationsBudgetDone[$item]) and $budgetPlaned>0)
            {
                $spi = round($getLocationsBudgetDone[$item]/$budgetPlaned,4);
            }
            $performance[] = array('location_id'=>$item,'spi'=>$spi);
        }
        $project_spi = array(
            'good'   => 0,
            'normal' => 0,
            'risk'   => 0
        );
        if($performance){
            foreach($performance as $item){
                if($item['spi']<=0.55)
                    $project_spi['risk'] +=1;
                elseif($item['spi']<=0.8)
                    $project_spi['normal'] +=1;
                elseif($item['spi']>0.8)
                    $project_spi['good'] +=1;
            }
        }
        //ddd($project_spi);
        // Load view to display data
        return view('dashboard/dashboard', compact('zone','provinces','projects','lang','excluded_locations','all_locations','physical_progress_high','physical_progress_medium','physical_progress_low','zones','project_spi'));
    }

    /**
     * Projects dashbaord by filter.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardByFilter(Request $request)
    {
        // Get language
        $lang = get_language();
        // Get selected zones
        $zone = Province::select('zone as zcode','zname_'.$lang.' as zname')->whereIn('zone',$request->zone)->groupBy('zone')->get();
        // Get selected province
        if(in_array('-',$request->zone)){
            // Get zones by province
            $provinces = Province::all()->toArray();
        }elseif(in_array('-',$request->province)){
            // Get zones by province
            $provinces = Province::whereIn('zone', $zone)->get()->toArray();
        }else{
            // Get zones by province
            $provinces = Province::whereIn('id', $request->province)->get()->toArray();
        }
        // Get total of under construction projects
        $project_progress = Plans::select('pl.id','projects.priority')
                            ->whereHas('share', function($query){
                                $query->where('share_to_code','pmis_progress');
                            })
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->whereIn('pl.province_id',$provinces)->where('pl.status','0')
                            ->orderBy('projects.priority','asc')->get();
        // Set default value
        $location_id = array(
            '1' => array(),
            '2' => array(),
            '3' => array(),
        );
        // Arrange project location ids
        if($project_progress){
            foreach($project_progress as $item){
                if(isset($location_id[$item->priority]))
                    array_push($location_id[$item->priority], $item->id);
            }
        }

        // Get physical progress based on priority
        $physical_progress_high   = getBudgetDone($location_id[1]);
        $physical_progress_medium = getBudgetDone($location_id[2]);
        $physical_progress_low    = getBudgetDone($location_id[3]);
        $all_locations = array_merge($location_id[1],$location_id[2],$location_id[3]);
        // locations without base schedule
        $excluded_locations = Plan_locations::whereIn('id',$all_locations)->with('Projects')->doesntHave('bill_quantities_base')->get();
        $excluded_loc_id   = array();
        if($excluded_locations){
            foreach($excluded_locations as $item){
                $excluded_loc_id[] = $item->id;
            }
        }
        if($excluded_loc_id)
        {
            $all_locations = array_diff($all_locations,$excluded_loc_id);
        }
        // Get total of under construction projects
        $total_projects = Plans::select('projects.priority', 'pl.id', DB::raw('SUM(bq.total_price) as price'))
                            ->join('project_locations as pl','pl.project_id','=','projects.id')
                            ->join('estimations_bill_quantities as bq','bq.project_location_id','=','pl.id')
                            ->whereIn('pl.id', $all_locations)->groupBy('pl.id')->get();
        // Set default value
        $projects = array(
            '1' => array('total' => 0, 'price' => 0),
            '2' => array('total' => 0, 'price' => 0),
            '3' => array('total' => 0, 'price' => 0)
        );
        // Arrange data
        if($total_projects){
            foreach($total_projects as $item){
                $projects[$item->priority]['total'] += 1;
                $projects[$item->priority]['price'] += $item->price;
            }
        }
        // Get total projects based on zones
        $zone_projects = Plan_locations::select('pr.zone', DB::raw('count(project_locations.id) as total'))
                                        ->whereIn('project_locations.id', $all_locations)
                                        ->join('pmis_auth.provinces as pr','project_locations.province_id','=','pr.id')
                                        ->groupBy('pr.zone')->get();
        // Set default value
        $zones = array('01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0);
        // Arrange data
        if($zone_projects){
            foreach($zone_projects as $item){
                $zones[$item->zone] = $item->total;
            }
        }
        // Get projects with budgets
        $project_budget = Plan_locations::select('project_locations.project_id','project_locations.id', DB::raw('SUM(bq.total_price) as total_price'))
                                        ->join('estimations_bill_quantities as bq','bq.project_location_id','=','project_locations.id')
                                        ->whereIn('bq.project_location_id',$all_locations)
                                        ->groupBy('project_locations.id')
                                        ->orderBy('project_locations.project_id', 'asc')
                                        ->get()->toArray();
        // Get location activity done
        $getLocationsBudgetDone = getOneLocationBudgetDone($all_locations);
        // Set default values
        $performance = array();
        foreach($all_locations as $item){
            // Get first date of planned schedule
            $getStartDate = getFirstLastDateOfProject(array($item),'min(start_date)','project_id');
            // Get todays date
            $getEndDate = date('Y-m-d');
            // Get project actual performance in field
            $spi = 0;
            // Get project location week days
            $WeekDays = weekDaysAll(array($item));
            // Get project location holidays
            $holidays = holidaysAll(array($item));
            // Get planned budget
            $plannedpBudget = getWorkingBudgetPlaned(array($item));
            // Calcualte working budget from start of project till now
            $totalWorking = setCfyDateRange($plannedpBudget,$getStartDate,$getEndDate,0,$WeekDays,$holidays);
            $totalWorking = sortBudgetDataCfy($totalWorking,$WeekDays,$holidays);
            // Get planed budget from start of project till now
            $budgetPlaned = (count($totalWorking) > 0 ? array_sum(array_column($totalWorking,'total_price')) : 0 );
            if(isset($getLocationsBudgetDone[$item]) and $budgetPlaned>0)
            {
                $spi = round($getLocationsBudgetDone[$item]/$budgetPlaned,4);
            }
            $performance[] = array('location_id'=>$item,'spi'=>$spi);
        }
        $project_spi = array(
            'good'   => 0,
            'normal' => 0,
            'risk'   => 0
        );
        if($performance){
            foreach($performance as $item){
                if($item['spi']<=0.55)
                    $project_spi['risk'] +=1;
                elseif($item['spi']<=0.8)
                    $project_spi['normal'] +=1;
                elseif($item['spi']>0.8)
                    $project_spi['good'] +=1;
            }
        }
        // Load view to display data
        return view('dashboard/dashboard_filter', compact('zone','provinces','projects','lang','excluded_locations','all_locations','physical_progress_high','physical_progress_medium','physical_progress_low','zones','project_spi'));
    }

    /**
     * Province by zones
     *
     * @return \Illuminate\Http\Response
     */
    public function bringProvinces(Request $request)
    {
        $lang = get_language();
        $id = explode(',',$request->id);
        $provinces = Provinces::where(function($query) use ($id){
                                    if(!in_array('-', $id))
                                        $query->whereIn('zone',$id);
                                })->get();
        return view('dashboard.provinces_dropdown', compact('lang','provinces','id'));
    }

    /**
     * Projects by zones
     *
     * @return \Illuminate\Http\Response
     */
    public function projectsByZone(Request $request)
    {
        $lang = get_language();
        // Get selected zone
        $zone = $request->zone;
        // Get locations
        $all_locations = explode(',',$request->all_locations);
        // Get total projects based on zones
        $records = Plan_locations::select('project_locations.*')
                                    ->whereHas('projects', function($query){
                                        $query->orderBy('projects.id', 'asc');
                                    })
                                    ->whereIn('project_locations.id', $all_locations)
                                    ->join('pmis_auth.provinces as pr','project_locations.province_id','=','pr.id')
                                    ->where('pr.zone', $zone)
                                    ->orderBy('project_locations.id', 'asc')
                                    ->get();
        // Load view to display data
        return view('dashboard.project_by_zone', compact('lang','records'));
    }
}
?>
