<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ArchitectureRequest;
use App\Notifications\NewProject;
use App\models\Requests;
use App\models\Mechanic;
use App\models\Architecture;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\MechanicProjectLocation;
use App\models\Settings\Statics\Employees; 
use App\models\MechanicTeam; 
use App\User;
use Redirect;

class MechanicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($request->id);//project id
        $data['enc_id'] = $request->id; //Encrupted ID
        $data['locations'] = Plan_locations::where('project_id',$id)->orderBy('id','desc')->with(['province'])->get(); // Get Plan Location
        $loc_data = MechanicProjectLocation::where('project_id',$id)->get(); // Get Inserted Location
        $added_loc = array();
        if($loc_data)
        {
            foreach($loc_data as $item)
            {
                $added_loc[] = $item->project_location_id;
            }
        }
        $data['loc_data'] = $added_loc;
        $data['employees'] = Employees::where('section','tab_mechanic')->get(); // Get all employees
        $data['record'] = Plans::find($id); // Get project summary record
        return view('pmis/designs/mechanic/create',$data); // Load view to show result
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArchitectureRequest $request)
    {
        $lang = get_language();
        $request['created_by'] = userid(); 
        $request['department_id'] = departmentid(); 
        $request['start_date'] = dateProvider($request->start_date,$lang); 
        $request['end_date'] = dateProvider($request->end_date,$lang);
        $project_id  = decrypt($request->project_id);
        $request['project_id'] = $project_id;
        $updated = false; 
        if(!empty($request->version_id))
        {
          $mechanic_id = decrypt($request->version_id); 
          $request['version']    = 'latest';  
          $first_version_arch = Mechanic::find($mechanic_id); 
          $first_version_arch->version = 'updated'; 
          $first_version_arch->update(); 
          $request['version_id'] = $mechanic_id.','.$first_version_arch->version_id; 
          $updated = true; 
        }
        // Generate URN
        if($updated){
            $urn = $first_version_arch->urn;
        }else{
            $urn = GenerateURN('mechanics','urn');
        }
        $request['urn'] = $urn;
        $mechanic = Mechanic::create($request->except(['employees','file','file_name','project_location_id'])); 
        // Architecture Team Members
        if($request->employees)
        {
          $team=array();
          for($i=0;$i<count($request->employees);$i++)
          {
            $team[] = array(
              'mechanic_id'     => $mechanic->id,
              'employee_id'     => $request->employees[$i]
            );
          }
          if($team)
          {
            MechanicTeam::insert($team);
          }
        }
        $data = array(); 
        foreach($request->project_location_id as $id)
        {
                $data[] = [
                    'record_id'  => $mechanic->id,
                    'project_id' => $project_id,
                    'project_location_id' => $id,
                    'section' => 'mechanic'
                ];             
        } //Make record for bulk insert 
        $arch_proj_locatoin = MechanicProjectLocation::insert($data); //Bulk insertion 
        if($arch_proj_locatoin)
        {
            //Upload File
            if($request->file)
            {
                if($request->file)
                {
                    $i = 0;   
                    foreach($request->file as $file)
                    {  
                       $isUploaded = uploadAttachments($project_id,$mechanic->id,'mechanic',$file,$request->file_name[$i],$i,'design_attachments'); 
                       if(!$isUploaded)
                        {
                            Session()->flash('att_failed', __("global.attach_failed"));
                        }
                      $i++; 
                    }
                }
            }
            // Check if record is version then send notification to visa 
            if($updated==true){
                $emp = User::getUserRoleCode('vis_approval');//Get users who have role of Visa Approval
                $users = User::whereIn('id',$emp)->get();
                $redirect_url = route('mechanic.show', encrypt($request->project_id));
                $project_name = Plans::whereId($project_id)->first()->name;
                foreach($users as $user) {
                    $user->notify(new NewProject($id, trans('designs.mechanic_rej_update',['pro'=>$project_name]), userid(), $redirect_url));
                }
            }
            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
            return Redirect::route("designs")->withErrors($validates)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mechanic  $architectur
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//project id
        $data['enc_id'] = $enc_id;
        $data['with_parent_id'] = $enc_id; 
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_section' => "pmis_design"]);
        session(['current_tab' => 'tab_mechanic']);
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        $data['shared'] = $data['plan']->share()->where('share_from_code','pmis_design')->first();
        // Check if Architecture record is exist or not
        $data['architecture'] = Architecture::where('project_id',$id)->get();
        // Get record 
        $data['mechanic'] = Mechanic::where('project_id',$id)->whereIn('version',['first','latest'])->orderBy('id','desc')->get();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$id,0,'mechanic');
        // Attachment Table
        $data['table']  = 'design_attachments';
        $data['section'] = 'mechanic';
        // Get remained locations
        $data['plan_loc'] = Plan_locations::where('project_id',$id)->get()->count(); // Get Plan Location
        $data['inserted_loc'] = MechanicProjectLocation::where('project_id',$id)->get()->count(); // Get Inserted Location
        // Load view to show result
        return view('pmis/designs/mechanic/mechanic',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $project_id = decrypt($request->project_id);//project id
        $data['enc_id'] = $request->project_id; //Encrupted ID
        $data['locations'] = Plan_locations::where('project_id',$project_id)->orderBy('id','desc')->with(['province'])->get(); // Get Plan Location
        $loc_data = MechanicProjectLocation::where('project_id',$project_id)->where('record_id','!=',$request->id)->get(); // Get Inserted Location
        $added_loc = array();
        if($loc_data)
        {
            foreach($loc_data as $item)
            {
                $added_loc[] = $item->project_location_id;
            }
        }
        $data['loc_data'] = $added_loc;
        $data['record'] = Plans::find($project_id); // Get project summary record
        $data['mechanic'] = Mechanic::with('mechanicProjectLocation.projectLocation')->findOrFail($request->id);
        $data['employees']   = Employees::where('section','tab_mechanic')->get(); // Get all employees
        // Get team members
        $team = MechanicTeam::where('mechanic_id',$request->id)->get();
        $team_members = array();
        if($team){
            foreach($team as $item){
                $team_members[] = $item->employee_id;
            }    
        }
        $data['team_members'] = $team_members;
        // Get Current Selected Locations
        $data['location_selected'] = multiSelectArchitectureLocation($data['mechanic'],'mechanicProjectLocation');
        // Load view to show result
        return view('pmis/designs/mechanic/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $lang = get_language();
        $data = array(
            'start_date'  =>  dateProvider($request->start_date,$lang), 
            'end_date'    => dateProvider($request->end_date,$lang), 
            'employee_id' => $request->employee_id,
            'description' => $request->description, 
        ); 
        $record_id = decrypt($request->version_id);
        $project_id = decrypt($request->project_id); 
        $mechanic = Mechanic::findOrFail($record_id); 
        $mechanic->update($data); 
        // Mechanic Team Members
        if($request->employees)
        {
          $team=array();
          for($i=0;$i<count($request->employees);$i++)
          {
            $team[] = array(
              'mechanic_id'     => $mechanic->id,
              'employee_id'     => $request->employees[$i]
            );
          }
          if($team)
          {
            // Delete old records
            MechanicTeam::where('mechanic_id',$mechanic->id)->delete();
            // Add new records
            MechanicTeam::insert($team);
          }
        }
        $project_location = array();
        // Delete the record first 
        $mechanic->mechanicProjectLocation()->delete();  
        foreach($request->project_location_id as $id)
        {
                $project_location[] = [
                    'record_id'  => $record_id,
                    'project_id' => $project_id,
                    'project_location_id' => $id,
                    'section' => 'mechanic'
                ];             
        } //Make record for bulk insert 
        $arch_proj_locatoin = MechanicProjectLocation::insert($project_location); //Bulk insertion 
        if($arch_proj_locatoin)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
            return Redirect::route("designs")->withErrors($validates)->withInput();
        }
       
       
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-12-05 12:03:53 
     * @Desc:  
     */    
    public function approveMechanic(Request $request)
    {
        $data['lang'] = get_language();
        $data['enc_id'] = $request->project_id;
        // Get record
        $data['mechanic'] = Mechanic::findOrFail($request->id);
        $data['version_comments'] = Mechanic::whereIn('id',forWhereIn($data['mechanic']->version_id))->orderBy('id','desc')->get();       
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',decrypt($request->project_id),$request->id,'mechanic');
        $data['section'] = 'mechanic'; 
        $data['table'] = 'design_attachemtns'; 
        $data['with_parent_id']  = $request->project_id; 
        // Load view to show result
        return view('pmis/designs/mechanic/approve',$data);
    }

     /** 
      * @Author: Jamal Yuusufi  
      * @Date: 2019-12-07 15:22:43 
      * @Desc: Do Approve the architecture by the Visa    
      */     
    public function doApproveMechanic(Request $request)
    {
        // Validate the request...
        $project_id = decrypt($request->project_id);//project_id
        $project_name = Plans::whereId($project_id)->first()->name;
        $id = $request->id; 
        $record = Mechanic::findOrFail($id);
        // to be approved
        $request['changed_status_by'] = userid();
        $request['changed_date'] = date('Y-m-d H:i:s');                
        $record->update($request->except(['project_id','id'])); //change status 
        if($request->status == 1)
        {
            // Send notification to owner
            $emp = array();
            $emp[] = $record->created_by;//Get owner of record to send him or her notification
            $users = User::whereIn('id',$emp)->get();
            $redirect_url = route('mechanic.show', encrypt($project_id));
            foreach($users as $user) {
                $user->notify(new NewProject($project_id,trans('global.mechanic_approved',['pro'=>$project_name]), userid(), $redirect_url));
            }
            // End Notification
            Session()->flash('success', __("designs.approved_msg"));
        }
        else
        {
            // Start Notification
            $emp = array();
            $emp[] = $record->created_by;//Get owner of record to send him or her notification
            $users = User::whereIn('id',$emp)->get();
            $redirect_url = route('mechanic.show',encrypt($project_id));
            foreach($users as $user) {
                $user->notify(new NewProject($project_id,trans('global.mechanic_rejcted',['pro'=>$project_name]), userid(), $redirect_url));
            }
            // End Notification
            Session()->flash('success', __("designs.rejected_msg"));
        }
        $record->save();

    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-12-10 11:01:52 
     * @Desc: View Mechanic   
     */      
    public function view(Request $request)
    {
        $data['lang'] = get_language();
        $data['enc_id'] = $request->project_id;
        // Get record
        $data['mechanic'] = Mechanic::findOrFail($request->id);
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',decrypt($request->project_id),$request->id,'mechanic');
        $data['table'] = 'design_attachments'; 
        $data['with_parent_id']  = $request->project_id; 
        $data['record_id']       = $request->id;  
        $data['section']         = 'mechanic';
        // Get Versions
        $data['versions'] = Mechanic::whereIn('id',forWhereIn($data['mechanic']->version_id))->orderBy('id','asc')->get();
        // Load view to show result
        return view('pmis/designs/mechanic/view',$data);
    }

    /**
     * @Author: Jamal YOusufi  
     * @Date: 2019-12-25 15:16:12 
     * @Desc: Show Version of the Architecture  
     */      
    public function showVersion(Request $request)
    {
       $lang     = get_language(); 
       $versions = Mechanic::whereIn('id',forWhereIn($request->version_id))->orderBy('id','desc')->get();
       return view('pmis/designs/mechanic/version',compact(['versions','lang'])); 
    }
}
