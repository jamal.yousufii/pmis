<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Http\Request;
use App\models\Requests;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Survey;
use App\models\Survey_entity_values;
use App\models\Architecture;
use App\models\Structure;
use App\models\Electricity;
use App\models\Water;
use App\models\Mechanic;
use App\models\Estimation;
use App\models\BillofquantitiesEst;
use App\models\Implement;
use App\models\Implement_attachments;
use App\models\Procurement;

class SummaryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $req_id = Input::get('req_id');
        $code = Input::get('code');
        $firstCode = Input::get('firstCode');   
        if($code)
        {
            switch($code)
            {
                case 'pmis_request':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    // Get Section Data
                    if($firstCode!="pmis_request")
                    {
                        $plan = Plans::whereId($id)->first(); 
                        $id = $plan->request_id;
                    }
                    $record = Requests::whereId($id)->first();
                    $data['summary'] = $record;
                    return view('pmis/summary/view_request',$data);
                    break;
                }
                case 'pmis_plan':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    if($firstCode=="pmis_request")
                    {   
                        // Get Rquest Data
                        $request = Requests::whereId($req_id)->first();
                        // Get Project ID By Request
                        $id = $request->project->id;
                    }
                    $record = Plans::whereId($id)->first();
                    
                    $data['summary'] = $record;
                    if($record)
                    {   
                        // Get Plan Location
                        $data['location'] = Plan_locations::where('project_id',$record->id)->orderBy('id','desc')->get();
                    }
                    return view('pmis/summary/view_plan',$data);
                    break;
                }
                case 'pmis_survey':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    if($firstCode=="pmis_request")
                    {   
                        // Get Project ID By Request
                        $id = Plans::where('request_id',$req_id)->first()->id;
                    }
                    // Get Section Data
                    $record = Survey::where('project_id',$id)->get();
                    $data['summary'] = $record;
                    $data['project_id']  = $id;
                    return view('pmis/summary/view_survey',$data);
                    break;
                }
                case 'pmis_design':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    // Load tabs
                    $data['tabs'] = getTabs('pmis_design',$lang);
                    session(['current_tab' => 'tab_architecture']);
                    if($firstCode=="pmis_request")
                    {   
                        // Get Project ID By Request
                        $id = Plans::where('request_id',$req_id)->first()->id;
                    }
                    // Get Section Data
                    $record = Architecture::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
                    $data['summary'] = $record;
                    $data['project_id']  = $id;
                    return view('pmis/summary/view_design',$data);
                    break;
                }
                case 'pmis_estimation':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    // Load tabs
                    $data['tabs'] = getTabs('pmis_design',$lang);
                    session(['current_tab' => 'pmis_estimation']);
                    if($firstCode=="pmis_request")
                    {   
                        // Get Project ID By Request
                        $id = Plans::where('request_id',$req_id)->first()->id;
                    }
                    // Get Section Data
                    $record = Estimation::where('project_id',$id)->first();
                    // Get Bill of Quantities
                    $data['bill_quantities'] = BillofquantitiesEst::where('project_id',$id)->paginate(10)->onEachSide(1);
                    $data['summary'] = $record;
                    $data['project_id']  = $id;
                    return view('pmis/summary/view_estimation',$data);
                    break;
                }
                case 'pmis_implement':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    // Load tabs
                    $data['tabs'] = getTabs('pmis_design',$lang);
                    session(['current_tab' => 'pmis_implement']);
                    if($firstCode=="pmis_request")
                    {   
                        // Get Project ID By Request
                        $id = Plans::where('request_id',$req_id)->first()->id;
                    }
                    // Get Section Data
                    $record = Implement::where('project_id',$id)->first();
                    $data['summary'] = $record;
                    // Get Attachments
                    $data['attachments'] = Implement_attachments::where('parent_id',$id)->get();
                    $data['project_id']  = $id;
                    return view('pmis/summary/view_implement',$data);
                    break;
                }
                case 'pmis_procurement':
                {
                    $lang = get_language();
                    $data['lang']   = $lang;
                    if($firstCode=="pmis_request")
                    {   
                        // Get Project ID By Request
                        $id = Plans::where('request_id',$req_id)->first()->id;
                    }
                    // Get Section Data
                    $record = Procurement::where('project_id',$id)->first();
                    $data['summary'] = $record;
                    $data['project_id']  = $id;
                    return view('pmis/summary/view_procurement',$data);
                    break;
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_survey($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Survey id
        $data['rec_id']  = $rec_id;
        $rec_id = decrypt($rec_id);
        // Get Plan data
        $data['record'] = Survey::whereId($rec_id)->with('documents.documents_type')->first();
        $employees = "";
        if($data['record'])
        {
            // Get Plan Summary
            $data['plan'] = Plans::whereId($parent_id)->first();
            // Get Survey Team Members
            $getEmp = Survey::getTeamEmployeesName($rec_id);
            if($getEmp)
            {
                foreach($getEmp as $emp)
                {
                    $employees .= $emp->first_name.' '.$emp->last_name.", ";
                }
                $employees = substr($employees,0,-2);
            }
            //Get Aabedat entity values
            $data['aabedat'] = Survey_entity_values::where('survey_id',$rec_id)->orderBy('entity_id')->get();
        }
        session(['document_table' => "survey_documents"]);
        $data['employees'] = $employees;
        // Load view to show result
        return view('pmis/summary/view_survey_ajax',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_architecture($id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        // Parent id
        $data['project_id']  = $id;
        $id = decrypt($id);
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_tab' => 'tab_architecture']);
        // Get Section Data
        $record = Architecture::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        $data['summary'] = $record;
        return view('pmis/summary/view_architecture',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_architecture($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$data['lang']);
        session(['current_tab' => 'tab_architecture']);
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Record id
        $data['rec_id']  = $rec_id;
        $record_id = decrypt($rec_id);
        // Get Plan Summary
        $data['plan'] = Plans::whereId($parent_id)->first();
        // Get record
        $data['summary'] = Architecture::whereId($record_id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$parent_id,$record_id,'architecture');
        $data['attachment_table'] = "design_attachments";
        // Load view to show result
        return view('pmis/summary/view_architecture_ajax',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_structure($id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        // Parent id
        $data['project_id']  = $id;
        $id = decrypt($id);
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_tab' => 'tab_structure']);
        // Get Section Data
        $record = Structure::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        $data['summary'] = $record;
        return view('pmis/summary/view_structure',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_structure($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$data['lang']);
        session(['current_tab' => 'tab_structure']);
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Record id
        $data['rec_id']  = $rec_id;
        $record_id = decrypt($rec_id);
        // Get Plan Summary
        $data['plan'] = Plans::whereId($parent_id)->first();
        // Get record
        $data['summary'] = Structure::whereId($record_id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$parent_id,$record_id,'structure');
        $data['attachment_table'] = "design_attachments";
        // Load view to show result
        return view('pmis/summary/view_structure_ajax',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_electricity($id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        // Parent id
        $data['project_id']  = $id;
        $id = decrypt($id);
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_tab' => 'tab_electricity']);
        // Get Section Data
        $record = Electricity::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        $data['summary'] = $record;
        return view('pmis/summary/view_electricity',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_electricity($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$data['lang']);
        session(['current_tab' => 'tab_electricity']);
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Record id
        $data['rec_id']  = $rec_id;
        $record_id = decrypt($rec_id);
        // Get Plan Summary
        $data['plan'] = Plans::whereId($parent_id)->first();
        // Get record
        $data['summary'] = Electricity::whereId($record_id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$parent_id,$record_id,'electricity');
        $data['attachment_table'] = "design_attachments";
        // Load view to show result
        return view('pmis/summary/view_electricity_ajax',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_water($id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        // Parent id
        $data['project_id']  = $id;
        $id = decrypt($id);
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_tab' => 'tab_water']);
        // Get Section Data
        $record = Water::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        $data['summary'] = $record;
        return view('pmis/summary/view_water',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_water($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$data['lang']);
        session(['current_tab' => 'tab_water']);
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Record id
        $data['rec_id']  = $rec_id;
        $record_id = decrypt($rec_id);
        // Get Plan Summary
        $data['plan'] = Plans::whereId($parent_id)->first();
        // Get record
        $data['summary'] = Water::whereId($record_id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$parent_id,$record_id,'water');
        $data['attachment_table'] = "design_attachments";
        // Load view to show result
        return view('pmis/summary/view_water_ajax',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_mechanic($id)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        // Parent id
        $data['project_id']  = $id;
        $id = decrypt($id);
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_tab' => 'tab_mechanic']);
        // Get Section Data
        $record = Mechanic::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        $data['summary'] = $record;
        return view('pmis/summary/view_mechanic',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_mechanic($rec_id,$parent_id)
    {
        $data['lang'] = get_language();
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$data['lang']);
        session(['current_tab' => 'tab_mechanic']);
        // parent id
        $parent_id = decrypt($parent_id);
        $data['project_id']  = $parent_id;
        // Record id
        $data['rec_id']  = $rec_id;
        $record_id = decrypt($rec_id);
        // Get Plan Summary
        $data['plan'] = Plans::whereId($parent_id)->first();
        // Get record
        $data['summary'] = Mechanic::whereId($record_id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$parent_id,$record_id,'mechanic');
        $data['attachment_table'] = "design_attachments";
        // Load view to show result
        return view('pmis/summary/view_mechanic_ajax',$data);
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list_bill_of_quantities($enc_id)
    {
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Language
        $data['lang'] = get_language();
        // Get project
        $data['plan'] = Plans::whereId($id)->first();
        // Get locations
        $data['locations'] = Plan_locations::where('project_id',$id)->get();
        // Load view to show result
        return view('pmis/summary/list_bill_of_quantities',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view_bill_of_quantities($enc_id,$loc_id,Request $request)
    {
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        $data['loc_id'] = $loc_id;
        $data['lang'] = get_language();
        // Get Bill of Quantities Total Price
        $data['total_price'] = BillofquantitiesEst::where('project_id',$id)->where('project_location_id',$loc_id)->sum('estimated_total_price');
        // Get locations
        $data['locations'] = Plan_locations::where('project_id',$id)->get();
        // Get Bill of Quantities
        $data['bill_quantities'] = BillofquantitiesEst::where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->orderBy('project_location_id','asc')->orderBy('bq_section_id','asc')->paginate(10)->onEachSide(3);
        if(Input::get('ajax') == 1)
        {
            //Static record iteration with pagination
            $data['counter'] = $request->counter;
            // Load view to show result
            return view('pmis/summary/view_bill_of_quantities_ajax',$data);
        }
        else
        {
            // Load view to show result
            return view('pmis/summary/view_bill_of_quantities',$data);
        }
        
    }
}
