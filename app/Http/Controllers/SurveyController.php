<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use App\Notifications\NewProject;
use App\models\Requests;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Survey;
use App\models\Survey_teams;
use App\models\Home;
use App\models\Static_data;
use App\models\Survey_entities;
use App\models\Survey_entity_values;
use App\models\Survey_attachments;
use App\models\Survey_beneficiaries;
use App\models\Survey_documents;
use App\models\Survey_measurements;
use App\models\Authentication\SectionSharings;
use App\models\Settings\Statics\Employees;
use App\models\Authentication\Users;
use App\models\ProjectUsers;
use App\User;
use Validator;
use Redirect;
use PDF;

class SurveyController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department id is not exist
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_survey'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_survey"]);
    // Get Language
    $data['lang'] = get_language();
    // Get all data
    $data['records']  = Plans::with('survey')
                        ->whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_survey');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_survey','sur_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view only assigned records  
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_survey');
                            });
                          }
                        })->whereHas('project_location', function($query){
                          $query->where('status','0');
                        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_survey');
    })->where('department_id',$depID)->get();
    //Get URL Segments
    $data['segments'] = request()->segments();
    if(Input::get('ajax') == 1)
    {
      return view('pmis/surveys/list_ajax',$data);
    }
    else
    {
      // Load view to show searchpa result
      return view('pmis/surveys/list',$data);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list($enc_id)
  {
    //Set current section
    session(['current_section' => "pmis_survey"]);
    $data['lang'] = get_language();
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get plan record
    $data['plan'] = Plans::find($id);
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get locations and check if survey inserted for all locations    
    $data['locations'] = Plan_locations::where('project_id',$id)->get();
    // Get Record
    $records = Survey::where('project_id',$id);
    $data['allRecords'] = $records->get();
    $data['records'] = $records->paginate(10)->onEachSide(1);
    //echo "<pre>"; print_r($data['records']); exit;
    // Get Allowed Sections for Sharing
    $data['sections'] = SectionSharings::where('code','pmis_survey')->get();
    $data['shared']  = $data['plan']->share()->where('share_from_code','pmis_survey')->first(); 
    // Get Attachments
    $data['attachments'] = getAttachments('survey_attachments',$id,0,'survey');
    //Pass Attachments table
    $data['table'] = 'survey_attachments';
    // Section for attachment table 
    $data['section'] = 'survey'; 
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/surveys/survey_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/surveys/survey',$data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    // Get Language
    $data['lang'] = get_language();
    // Get Survey entities
    $data['survey_entities']  = Survey_entities::get();
    // Plan ID
    $enc_plan = $request->plan_id;
    $dec_plan = decrypt($enc_plan);
    $data['enc_plan'] = $enc_plan;
    // Get Plan Summary
    $data['plan'] = Plans::whereId($dec_plan)->first();
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    //Get All Statics
    $data['roof']        = Home::getAllStatics('roof');
    $data['fear']        = Home::getAllStatics('fear');
    $data['foundation']  = Home::getAllStatics('foundation');
    $data['stair']       = Home::getAllStatics('stair');
    $data['wall']        = Home::getAllStatics('wall');
    $data['wall_shap']   = Home::getAllStatics('wall_shape');
    $data['column']      = Home::getAllStatics('column');
    $data['column_shap'] = Home::getAllStatics('column_shape');
    $data['brick']       = Home::getAllStatics('brick');
    $data['employees']   = Employees::where('section','pmis_survey')->get();
    // Get inserted locations
    $locations = Survey::select('location_id')->where('project_id',$dec_plan)->get();
    $data['locations']   = array();
    if($locations)
    {
      foreach($locations as $item)
      {
        $data['locations'][] = $item->location_id; 
      }
    }
    // Get Project Locations
    $data['location']    = Plan_locations::where('project_id',$dec_plan)->get();
    // Get Static Data
    $data['measurement'] = Static_data::where('type','3')->get();
    $data['unit']        = Static_data::where('type','2')->get();
    $data['beneficiary'] = Static_data::where('type','5')->get();
    $data['documents']   = Static_data::where('type','4')->get();
    // Load view
    return view('pmis/surveys/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  { 
       // Get Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('surveys','urn');
    // Get Plan ID
    $project_id = decrypt(Input::get('enc_plan'));

    if($request->has_survey=='yes')
    {
      // Validate the request...
      $validates = $request->validate([
        'location'    => 'required',
        'start_date'  => 'required',
        'end_date'    => 'required',
        'survey_head' => 'required',
      ]);
      // New Survey
      $record = new Survey;
      $record->urn            = $urn;
      $record->project_id     = $project_id;
      $record->location_id    = $request->location;
      $record->start_date     = dateProvider($request->start_date,$lang);
      $record->end_date       = dateProvider($request->end_date,$lang);
      $record->team_head      = $request->survey_head;
      $record->well           = $request->water_well;
      $record->piping         = $request->plumbery;
      $record->pool           = $request->pool;
      $record->wastewater     = $request->sewage;
      $record->description    = $request->description;
      $record->created_at     = date('Y-m-d H:i:s');
      $record->created_by     = userid();
      $record->department     = departmentid();
      $record->process        = '0';
      if($record->save())
      { 
        // Survey Measurements
        if($request->measurement)
        {
          $measurement = array();
          for($i=0;$i<count($request->measurement);$i++)
          {
            $measurement[] = array(
              'record_id'   => $record->id,
              'type'        => $request->measurement[$i],
              'unit'        => $request->unit[$i],
              'quantity'    => $request->quantity[$i],
            );
          }
          Survey_measurements::insert($measurement);
        }
        
        // Survey Beneficiaries
        if($request->beneficiary)
        {
          $beneficiary = array();
          for($i=0;$i<count($request->beneficiary);$i++)
          {
            $beneficiary[] = array(
              'record_id'   => $record->id,
              'users_type'  => $request->beneficiary[$i],
              'total'       => $request->total[$i],
              'comments'    => $request->comments[$i],
            );
          }
          Survey_beneficiaries::insert($beneficiary); 
        }
        
        // survey Team Members
        if($request->emp_name)
        {
          $team=array();
          for($i=0;$i<count($request->emp_name);$i++)
          {
            $team[] = array(
              'survey_id'   => $record->id,
              'user_id'     => $request->emp_name[$i]
            );
          }
          Survey_teams::insert($team);
        }
        
        //Insert Aabedat entity values
        
        if(isset($request->entities ) && count($request->entities)>0)
        {
          if(count($request->entities)>0)
          {
            $entities = $request->entities;
            for($i=0;$i<count($entities);$i++)
            {
              $etities_value = array(
                'entity_id'             => $entities[$i],
                'survey_id'             => $record->id,
                'type'                  => $request->type[$i],
                'number'                => $request->number[$i], 
                'thickness'             => $request->thickness[$i], 
                'strenght'              => $request->strenght[$i],
                'destroy_component'     => $request->destroy_component[$i], 
                'height'                => $request->height[$i],
                'width'                 => $request->width[$i],
                'length'                => $request->length[$i],
                'quality'               => $request->quality[$i],
                'stair'                 => $request->stair[$i],
                'engine_specification'  => $request->engine_specification[$i],
                'shape'                 => $request->shape[$i],
                'perseverance'          => $request->perseverance[$i],
                'material'              => $request->material[$i],
                'moisture_status'       => $request->moisture_status[$i],
              );
              Survey_entity_values::insert($etities_value); 
            }
          }
        }
        Session()->flash('success', __("global.success_msg"));
      }
      else
      {
        Session()->flash('fail', __("global.failed_msg"));
      }
    } // end of if has survey 
    else
    {
      // New Survey
      $survey = new Survey;
      $survey->urn            = $urn;
      $survey->project_id     = $project_id;
      $survey->description    = $request->description; 
      $survey->has_survey     = $request->has_survey; 
      if($survey->save())
      {
        Session()->flash('success', __("global.success_msg")); 
      } 
      else
      {
        Session()->flash('fail', __("global.failed_msg"));
      }
    }
  }

  /**
   * Display more measurement.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function more_measurement()
  {
    // Get Language
    $data['lang'] = get_language();
    // Get Counter Number
    $data['number'] = Input::post('number');
    // Get Static Data
    $data['measurement'] = Static_data::where('type','3')->get();
    $data['unit'] = Static_data::where('type','2')->get();
    // Load view
    return view('pmis/surveys/more_measurement',$data);
  }

  /**
   * Display more beneficairy.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function more_beneficairy()
  {
    // Get Language
    $data['lang'] = get_language();
    // Get Counter Number
    $data['number'] = Input::post('number');
    // Get Static Data
    $data['beneficiary'] = Static_data::where('type','5')->get();
    // Load view
    return view('pmis/surveys/more_beneficairy',$data);
  }

  /**
   * Display more document.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function more_document()
  {
    // Get Language
    $data['lang'] = get_language();
    // Get Counter Number
    $data['number'] = Input::post('number');
    // Get Static Data
    $data['documents']   = Static_data::where('type','4')->get();
    // Load view
    return view('pmis/surveys/more_document',$data);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    $data['lang'] = get_language();
    // Survey id
    $id = decrypt($enc_id);
    // Get Plan data
    $data['record'] = Survey::whereId($id)->with('documents.documents_type')->first();
    $employees = "";
    if($data['record'])
    {
      // Get Plan Record
      $data['plan'] = Plans::whereId($data['record']->project_id)->first();
      // Get summary data
      $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
      // Get Survey Team Members
      $getEmp = Survey::getTeamEmployeesName($data['record']->id);
      if($getEmp)
      {
        foreach($getEmp as $emp)
        {
          $employees .= $emp->first_name.' '.$emp->last_name.", ";
        }
        $employees = substr($employees,0,-2);
      }
      //Get Aabedat entity values
      $data['aabedat'] = Survey_entity_values::where('survey_id',$id)->orderBy('entity_id')->get();
    }
    $data['enc_id'] = $enc_id; 
    // Get Attachments
    $data['attachments'] = getAttachments('survey_attachments',$data['record']->project_id,$id,'survey');
    // Get documnets 
    $data['table'] = 'survey_attachments'; 
    $data['section'] = 'survey';
    $data['employees'] = $employees;
    // Load view to show result
    return view('pmis/surveys/view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Survey entities
    $data['survey_entities']  = Survey_entities::get();
    // Get Survey Entity Values
    $data['aabedat'] = Survey_entity_values::where('survey_id',$id)->orderBy('entity_id')->get();
    // Get data
    $data['record'] = Survey::whereId($id)->first();
    if($data['record'])
    {
      // Get Plan ID
      $project_id = $data['record']->project_id;
      // Get Plan Summary
      $data['plan'] = Plans::whereId($project_id)->first();
      // Get summary data
      $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
      // Get inserted locations
      $locations = Survey::select('location_id')->where('project_id',$project_id)->get();
      $data['locations'] = arrangeSelectedData($locations,'location_id');
      // Get sub tables details
      $survey_teams = Survey_teams::where('survey_id',$id)->get();
      $data['survey_teams'] = arrangeSelectedData($survey_teams,'user_id');
      //echo "<pre>"; print_r($survey_teams); exit;
      // Get Project Locations
      $data['location'] = Plan_locations::where('project_id',$project_id)->get();
      // Get Static Data
      $data['measurement'] = Static_data::where('type','3')->get();
      $data['unit']        = Static_data::where('type','2')->get();
      $data['beneficiary'] = Static_data::where('type','5')->get();
      $data['documents']   = Static_data::where('type','4')->get();
      $data['employees']   = Employees::where('section','pmis_survey')->get();
      //Get All Statics
      $data['roof']        = Home::getAllStatics('roof');
      $data['fear']        = Home::getAllStatics('fear');
      $data['foundation']  = Home::getAllStatics('foundation');
      $data['stair']       = Home::getAllStatics('stair');
      $data['wall']        = Home::getAllStatics('wall');
      $data['wall_shap']   = Home::getAllStatics('wall_shape');
      $data['column']      = Home::getAllStatics('column');
      $data['column_shap'] = Home::getAllStatics('column_shape');
      $data['brick']       = Home::getAllStatics('brick');
    }
    // Load view to show result
    return view('pmis/surveys/edit',$data);
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update_data(Request $request)
  {
    // Language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'location'    => 'required',
      'start_date'  => 'required',
      'end_date'    => 'required',
      'survey_head' => 'required',
    ]);
    // Dycript the id
    $id = decrypt(Input::get('enc_id'));
    $record = Survey::find($id);
    $record->location_id    = $request->location;
    $record->start_date     = dateProvider($request->start_date,$lang);
    $record->end_date       = dateProvider($request->end_date,$lang);
    $record->team_head      = $request->survey_head;
    $record->description    = $request->description;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    $updated = $record->save();
    if($updated)
    {
      // survey Team Members
      if($request->emp_name)
      {
        Survey_teams::where('survey_id',$id)->delete();
        $team = array();
        for($i=0;$i<count($request->emp_name);$i++)
        {
          $team[] = array(
            'survey_id'   => $id,
            'user_id'     => $request->emp_name[$i]
          );
        }
        Survey_teams::insert($team);
      }
      
      // Survey Measurements
      if($request->measurement)
      {
        for($i=0;$i<count($request->measurement);$i++)
        {
          $data = array();
          if(isset($request->measurement_id[$i])) // Check if record exist then update
          {
            $data = array(
              'record_id'   => $id,
              'type'        => $request->measurement[$i],
              'unit'        => $request->unit[$i],
              'quantity'    => $request->quantity[$i],
            );
            // Update record
            Survey_measurements::whereId($request->measurement_id[$i])->update($data);
          }
          else // Check if record not exist then inser
          {
            $data = array(
              'record_id'   => $id,
              'type'        => $request->measurement[$i],
              'unit'        => $request->unit[$i],
              'quantity'    => $request->quantity[$i],
            );
            // Insert record
            Survey_measurements::insert($data);
          }
        }
      }
      
      // Survey Beneficiaries
      if($request->beneficiary)
      {
        for($i=0;$i<count($request->beneficiary);$i++)
        {
          $data = array();
          if(isset($request->beneficiary_id[$i])) // Check if record exist then update
          {
            $data = array(
              'record_id'  => $id,
              'users_type' => $request->beneficiary[$i],
              'total'      => $request->total[$i],
              'comments'   => $request->comments[$i],
            );
            // Update record
            Survey_beneficiaries::whereId($request->beneficiary_id[$i])->update($data);
          }
          else // Check if record not exist then inser
          {
            $data = array(
              'record_id'  => $id,
              'users_type' => $request->beneficiary[$i],
              'total'      => $request->total[$i],
              'comments'   => $request->comments[$i],
            );
            // Insert record
            Survey_beneficiaries::insert($data);
          } 
        }
      }
    
      // Survey Aabedats
      if(isset($request->entities) && count($request->entities)>0)
      {
          for($i=0;$i<count($request->entities);$i++)
          {
            $data = array();
            if(isset($request->sub_id[$i]) && $request->sub_id[$i]!='') // Check if record exist then update
            {
              $data = array(
                'type'                  => $request->type[$i],
                'number'                => $request->number[$i], 
                'thickness'             => $request->thickness[$i], 
                'strenght'              => $request->strenght[$i],
                'destroy_component'     => $request->destroy_component[$i], 
                'height'                => $request->height[$i],
                'width'                 => $request->width[$i],
                'length'                => $request->length[$i],
                'quality'               => $request->quality[$i],
                'stair'                 => $request->stair[$i],
                'engine_specification'  => $request->engine_specification[$i],
                'shape'                 => $request->shape[$i],
                'perseverance'          => $request->perseverance[$i],
                'material'              => $request->material[$i],
                'moisture_status'       => $request->moisture_status[$i],
              );
              // Update record
              Survey_entity_values::whereId($request->sub_id[$i])->update($data); 
            }
            else // Check if record not exist then inser
            {
              $data = array(
                'entity_id'             => $request->entities[$i],
                'survey_id'             => $record->id,
                'type'                  => $request->type[$i],
                'number'                => $request->number[$i], 
                'thickness'             => $request->thickness[$i], 
                'strenght'              => $request->strenght[$i],
                'destroy_component'     => $request->destroy_component[$i], 
                'height'                => $request->height[$i],
                'width'                 => $request->width[$i],
                'length'                => $request->length[$i],
                'quality'               => $request->quality[$i],
                'stair'                 => $request->stair[$i],
                'engine_specification'  => $request->engine_specification[$i],
                'shape'                 => $request->shape[$i],
                'perseverance'          => $request->perseverance[$i],
                'material'              => $request->material[$i],
                'moisture_status'       => $request->moisture_status[$i],
              );
              //echo "<pre>"; print_r($data);exit;
              // Update record
              Survey_entity_values::insert($data);    
            }
          }
      }

      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Store a newly attachments in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  function uploadDocuments($rec_id=0,$file_name="",$i=1)
  {
    // Getting all of the post data
    $file = Input::file('file_'.$i);
    $errors = "";
    if(Input::hasFile('file_'.$i))
    {
      // Validating each file.
      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
      $validator = Validator::make(
        [
            'file'      => $file,
            'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
            'file'      => 'required|max:500000',
            'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
        ]
      );
      if($validator->passes())
      {
        // Path is pmis/public/attachments/pmis...
        $destinationPath = 'attachments/pmis/surveys';
        $fileOrgName     = $file->getClientOriginalName();
        $fileExtention   = $file->getClientOriginalExtension();
        $fileOrgSize     = $file->getSize();
        if($file_name!=""){
          $name = $file_name;
          $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
        }else{
          $name = explode('.', $fileOrgName)[0];
          $file_name = $fileOrgName;
        }
        $upload_success = $file->move($destinationPath, $file_name);
        if($upload_success)
        {
          //Insert Record
          $attachment = new Survey_attachments;
          $attachment->rec_id     = $rec_id;
          $attachment->filename   = $name;
          $attachment->path       = $destinationPath."/".$file_name;
          $attachment->extension  = $fileExtention;
          $attachment->size       = $fileOrgSize;
          $attachment->created_at = date('Y-m-d H:i:s');
          $attachment->created_by = userid();
          $attachment->save();
          return true;
        }
        else
        {
            $errors .= json('error', 400);
        }
      }
      else
      {
        // Return false.
        return false;
      }
    }
  }

  /**
   * Store a newly attachments in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  function uploadSurveyFiles($request,$rec_id=0,$file_name="",$file,$i,$operation)
  {
    // Getting all of the post data
    $errors = "";
    // Validating each file.
    $validator = Validator::make(
      [
          'file'      => $file,
          'extension' => Str::lower($file->getClientOriginalExtension()),
      ],
      [
          'file'      => 'required|max:500000',
          'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
      ]
    );
    if($validator->passes())
    {
      // Path is pmis/public/attachments/pmis...
      $destinationPath = 'attachments/pmis/surveys';
      $fileOrgName     = $file->getClientOriginalName();
      $fileExtention   = $file->getClientOriginalExtension();
      $fileOrgSize     = $file->getSize();
      if($file_name!=""){
        $name = $file_name;
        $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
      }else{
        $name = explode('.', $fileOrgName)[0];
        $file_name = $fileOrgName;
      }
      $upload_success = $file->move($destinationPath, $file_name);
      if($upload_success)
      {
        if($operation=="insert")//Insert Record
        {
          $data[] = array(
            'record_id'     => $rec_id,
            'document_type' => $request->document_type[$i],
            'filename'      => $request->document_name[$i],
            'path'          => $destinationPath."/".$file_name,
            'extension'     => $fileExtention,
            'size'          => $fileOrgSize,  
            'created_at'    => date('Y-m-d H:i:s'),  
          );
          Survey_documents::insert($data);
        }
        else//Update Record
        {
          $data = array(
            'path'          => $destinationPath."/".$file_name,
            'extension'     => $fileExtention,
            'size'          => $fileOrgSize,  
            'updated_at'    => date('Y-m-d H:i:s'),  
          );
          Survey_documents::whereId($rec_id)->update($data); 
        }
        return true;
      }
      else
      {
          $errors .= json('error', 400);
      }
    }
    else
    {
      // Return false.
      return false;
    }
  }

  /**
   * Display the filtered resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function filter_survey(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    // Search Shared Plan 
    $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$department_id,$section,$request->role);
    // Load View to display records
    return view('pmis/surveys/list_filter',compact('records','lang','request'));
  }

  /**
   * Display more entities.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function SurveyEntities()
  {
    $lang = get_language();
    $code = Input::get('code');
    $counter = Input::get('counter');
    // Default Value For Aabedat
    $data['aabedat'] = ''; 
    $data['is_edit'] = ''; 
    if($code)
    {
      switch($code)
      {
        case 'sa':
        {
          $data['roof']    = Home::getAllStatics('roof');
          $data['div_id']  = $code; 
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/ceiling',$data);
          break;
        }
        case 'sb':
        {
          $data['fear']    = Home::getAllStatics('fear');
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/tras',$data);
          break; $data['values']  = '';
        }
        case 'sc':
        {
          $data['foundation'] = Home::getAllStatics('foundation');
          $data['div_id']     = $code;
          $data['counter']    = $counter; 
          return view('pmis/surveys/survey_aabedat/foundation',$data);
          break;
        }
        case 'sd':
        {
          $data['stair']   = Home::getAllStatics('stair');
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/numberOfFloor',$data);
          break;
        }
        case 'se':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/rooms',$data);
          break;
        }
        case 'sf':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/washrooms',$data);
          break;
        }
        case 'sg':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          $data['column']  = Home::getAllStatics('column');
          return view('pmis/surveys/survey_aabedat/halls',$data);
          break;
        }
        case 'sh':
        {
          $data['div_id']      = $code;
          $data['counter']     = $counter; 
          $data['column_shap'] = Home::getAllStatics('column_shape');
          return view('pmis/surveys/survey_aabedat/balconies',$data);
          break;
        }
        case 'si':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/kitchen',$data);
          break;
        }
        case 'sj':
        {
          $data['stair']   = Home::getAllStatics('stair');
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/stairs',$data);
          break;
        }
        case 'sk':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/lift',$data);
          break;
        }
        case 'sl':
        {
          $data['wall']      = Home::getAllStatics('wall');
          $data['wall_shap'] = Home::getAllStatics('wall_shape');
          $data['div_id']    = $code;
          $data['counter']   = $counter; 
          return view('pmis/surveys/survey_aabedat/walls',$data);
          break;
        }
        case 'sm':
        {
          $data['column']      = Home::getAllStatics('column');
          $data['column_shap'] = Home::getAllStatics('column_shape');
          $data['div_id']      = $code;
          $data['counter']     = $counter; 
          return view('pmis/surveys/survey_aabedat/pillars',$data);
          break;
        }
        case 'sn':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/stonyElements',$data);
          break;
        }
        case 'so':
        {
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/stonyCements',$data);
          break;
        }
        case 'sp':
        {
          $data['brick']   = Home::getAllStatics('brick');
          $data['div_id']  = $code;
          $data['counter'] = $counter; 
          return view('pmis/surveys/survey_aabedat/bricks',$data);
          break;
        }
      }
    }
  }

  /**
   * Display the notification resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function notification($enc_id,$dep_id)
  {
    //Set current department
    session(['current_department' => $dep_id]);
    //Set current section
    session(['current_section' => "pmis_survey"]);
    $data['lang'] = get_language();
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    echo $enc_id;exit;
    // Get Plan Summary
    $data['plan'] = Plans::whereId($id)->first();
    // Get locations and check if survey inserted for all locations    
    $data['locations'] = Plan_locations::where('project_id',$id)->get();
    // Get Record
    $data['records'] = Survey::where('project_id',$id)->paginate(10)->onEachSide(1);
    // Get Allowed Sections for Sharing
    $data['sections'] = SectionSharings::where('code','pmis_plan')->get();
    $data['shared']  = $data['plan']->share()->where('sections','pmis_survey')->first();
    // Get Attachments
    $data['attachments'] = getAttachments('survey_attachments',$id,0,'survey');
    //Pass Attachments table
    $data['table'] = 'survey_attachments';
    // Section for attachment table 
    $data['section'] = 'survey'; 
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/surveys/survey_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/surveys/survey',$data);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      $project_name = Plans::find($request->record_id)->name;
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('survey', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.project_assigned',['pro'=>$project_name]), userid(), $redirect_url));
              }     
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_survey')->get();
    // Load view to show result
    return view('pmis/surveys/list_assigned_users',compact('records'));
  }

  /**
   * Generate pdf of specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function survey_print($pro_id,$rec_id)
  {
    // Get language
    $lang = get_language();
    // Decrypt project and record id
    $pro_id= decrypt($pro_id);
    $rec_id= decrypt($rec_id);
    // Current department
    $department_name = getDepartmentNameByID(decrypt(session('current_department')),$lang);
    // Get record
    $record = Survey::whereId($rec_id)->with('documents.documents_type')->first();
    // Get survey team members
    $getEmp = Survey::getTeamEmployeesName($rec_id);
    $employees = "";
    if($getEmp)
    {
      foreach($getEmp as $emp)
      {
        $employees .= $emp->first_name.' '.$emp->last_name.", ";
      }
      $employees = substr($employees,0,-2);
    }
    // Load view file
    $file = PDF::loadView('pmis/surveys/survey_pdf',compact('lang','record','department_name','employees'));
    // Download pdf file
    return $file->setPaper('a4')->setOrientation('landscape')->download(__("report.project_progressive_report").'.pdf');
  }
}
?>
