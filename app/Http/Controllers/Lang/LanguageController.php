<?php
namespace App\Http\Controllers\Lang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use App\models\aut\Lang;
use App\models\Authentication\Modules;
use App\models\Authentication\LanguageLog;
use DB;
use App;

class LanguageController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  
  /**
   * @desc To change the current language.
   *
   * @request Ajax.
   */
  public function changeLanguage(Request $request)
  {
    Session::put('locale', $request->locale);
    $userid = Auth::id();
    //Check if user has a default language
    $def_lang = LanguageLog::where('userid',$userid)->get();
    if(count($def_lang)>0)
    {
      $data = array(
        'language'    => $request->locale
      );
      LanguageLog::where('userid',$userid)->update($data);
    }
    else
    {
      $data = array(
        'userid'      => $userid,
        'language'    => $request->locale
      );
      LanguageLog::insert($data);
    }
    $lang = $request->locale;
    $mod_name = Modules::select('name_'.$lang.' as name')->where('code',session('current_mod'))->get();
    foreach($mod_name as $item)
    {
      Session::put('current_mod_name', $item->name);
    }
    return Redirect()->back();
  }
}
