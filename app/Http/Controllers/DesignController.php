<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Facades\Input;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\auth\Module;
use App\models\Plans;
use App\models\Authentication\Users;
use App\models\ProjectUsers;
use App\User;
use Redirect;
use PDF;

class DesignController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index($dep_id)
  {
    // Check if department ID is exist or not
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_design'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_design"]);
    // Get Language
    $lang = get_language();
    $data['lang'] = $lang;
    // Get all data
    $data['records'] = Plans::whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_design');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_design','des_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view assigned records  
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_design');
                            });
                          }
                        })->whereHas('project_location', function($query){
                          $query->where('status','0');
                        })->groupBy('id')->orderBy('id','desc')->paginate(10);
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_design');
    })->where('department_id',$depID)->get();
    //Get URL Segments
    $data['segments'] = request()->segments();
    // load view to show result
    if(Input::get('ajax') == 1)
    {
      return view('pmis/designs/list_ajax',$data);
    }
    else
    {
      return view('pmis/designs/list',$data);
    }
  }

  public function design_notification($enc_id)
  {
    // ID
    $id = decrypt($enc_id);
    $lang = get_language();
    $data['lang'] = $lang;
    if(Session::get('current_mod')!='pmis')
    {
      $name = 'name_'.$lang;
      session(['current_mod' => 'pmis']);
      $current_mod_name = Module::get_module_id('pmis')->$name;
      session(['current_mod_name' => $current_mod_name]);
    }
    session(['current_section' => "pmis_design"]);
    // Get data
    $data['records'] = Plans::where('id',$id)->get();
    // Load view to show result
    return view('pmis/designs/list_notification',$data);
  }

  public function filter_design(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    // Search Shared Plan 
    $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$department_id,$section,$request->role);
    // Load View to display records
    return view('pmis/designs/list_filter',compact('records','lang','request'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      // Get project name
      $project_name = Plans::find($request->record_id)->name;
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('design', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.project_assigned',['pro'=>$project_name]), userid(), $redirect_url));
              }     
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_design')->get();
    // Load view to show result
    return view('pmis/designs/list_assigned_users',compact('records'));
  }

  /**
   * Generate pdf of specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function design_print($pro_id)
  {
    // Get language
    $lang = get_language();
    // Decrypt project id
    $pro_id= decrypt($pro_id);
    // Current department
    $department_name = getDepartmentNameByID(decrypt(session('current_department')),$lang);
    // Get record
    $record = Plans::with('architecture')->whereId($pro_id)->first();
    // Load view file
    $file = PDF::loadView('pmis/designs/design_pdf',compact('lang','record','department_name','pro_id'));
    // Download pdf file
    return $file->setPaper('a4')->setOrientation('landscape')->download(__("report.project_progressive_report").'_'.__("report.pmis_design").'.pdf');
  }
}
?>
