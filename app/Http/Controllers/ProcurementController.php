<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use App\Notifications\NewProject;
use Illuminate\Support\Facades\Input;
use App\Imports\ExcelImport;
use App\Exports\ExcelExport;
use App\models\Requests;
use App\models\Plans;
use App\models\Procurement;
use App\models\Billofquantities;
use App\models\Estimation;
use App\models\Authentication\SectionSharings;
use App\models\Home;
use App\models\Plan_locations;
use App\models\Authentication\Users;
use App\models\Settings\Statics\Bq_section;
use App\models\ProjectUsers;
use App\User;
use Redirect;
use Config;
use Excel;
use Importer;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Protection;

class ProcurementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /** Procurements */

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department id is not exist
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depID = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depID),'pmis_procurement'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => "pmis_procurement"]);
    // Get Language
    $data['lang'] = get_language();
    // Get all data
    $data['records']  = Plans::whereHas('share', function($query) use ($depID){
                          $query->where('share_to',$depID);
                          $query->where('share_to_code','pmis_procurement');
                        })->where(function($query) use ($depID){
                          if(!doIHaveRoleInDep(encrypt($depID),'pmis_procurement','pro_all_view'))//If user can't view department's all records
                          {
                            $query->whereHas('project_user', function($query) use ($depID){
                              //Can view assigned records  
                              $query->where('user_id',userid());
                              $query->where('department_id',$depID);
                              $query->where('section','pmis_procurement');
                            });
                          }
                        })->whereHas('project_location', function($query){
                          $query->where('status','0');
                        })->orderBy('id','desc')->paginate(10)->onEachSide(1);
    // Section Users
    $data['users'] = Users::whereHas('section', function($query) use ($depID){
      $query->where('code','pmis_procurement');
    })->where('department_id',$depID)->get();
    //Get URL Segments
    $data['segments'] = request()->segments();
    // load view to show result
    if(Input::get('ajax') == 1)
    {
      // Load view to display data
      return view('pmis/procurements/list_ajax',$data);
    }
    else
    {
      // Load view to display data
      return view('pmis/procurements/list',$data);
    }
  }

  public function filter_procurement(Request $request)
  {
    // Get Language
    $lang = get_language();
    // Get department
    $department_id = decrypt($request->department_id);
    // Get section code
    $section = $request->section;
    // Search Shared Plan 
    $records  = Plans::searchSharedPlan($request->condition,$request->field,$request->value,$department_id,$section,$request->section);
    // Load View to display records
    return view('pmis/procurements/list_filter',compact('records','lang','request'));
  }

  /** Executive Department */

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function executive_list($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Load tabs
    $data['tabs'] = getTabs('pmis_procurement',$lang);
    session(['current_tab' => 'tab_executive']);
    // Get Plan Record 
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_procurement');
    }])->first();  
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Record
    $data['record'] = Procurement::where('project_id',$id)->first();
    // Get Attachments
    $data['attachments'] = array();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/procurements/executive/list_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/procurements/executive/list',$data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function executive_create(Request $request)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Project ID
    $enc_id = Input::get('plan_id');
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Load view to show result
    return view('pmis/procurements/executive/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function executive_store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'executive'       => 'required',
      'executive_date'  => 'required',
    ]);
    $id = decrypt(Input::get('plan_id'));
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('procurements','urn');
    // Insert Record
    $record = new Procurement;
    $record->urn            = $urn;
    $record->project_id     = $id;
    $record->executive      = $request->executive;
    $record->executive_date = dateProvider($request->executive_date,$lang);
    $record->executive_desc = $request->executive_desc;
    $record->created_at     = date('Y-m-d H:i:s');
    $record->created_by     = userid();
    $insert = $record->save();
    if($insert>0)
    {
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Electricity  $architectur
   * @return \Illuminate\Http\Response
   */
  public function executive_show($enc_id=0)
  {
    $data['lang'] = get_language();
    // Record ID
    $id = decrypt($enc_id);
    // plan ID
    $plan_id = Input::get('plan_id');
    $plan_id = decrypt($plan_id);
    // Get data
    $data['record'] = Procurement::whereId($id)->first();
    // Load view to show result
    return view('pmis/procurements/executive/view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function executive_edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Record id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // plan ID
    $plan_id = Input::get('plan_id');
    $data['plan_id'] = $plan_id;
    // Get Record
    $data['record'] = Procurement::find($id);
    // Load view to show result
    return view('pmis/procurements/executive/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function executive_update(Request $request)
  {
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'executive'       => 'required',
      'executive_date'  => 'required',
    ]);
    $id = decrypt(Input::get('enc_id'));
    $record = Procurement::find($id);
    $record->executive      = $request->executive;
    $record->executive_date = dateProvider($request->executive_date,$lang);
    $record->executive_desc = $request->executive_desc;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    $updated = $record->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /** Planning Department */

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function planning_list($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Load tabs
    $data['tabs'] = getTabs('pmis_procurement',$lang);
    session(['current_tab' => 'tab_planning']);
    // Get Plan Record 
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_procurement');
    }])->first();  
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Record
    $data['record'] = Procurement::where('project_id',$id)->first();
    // Get Attachments
    $data['attachments'] = array();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/procurements/planning/list_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/procurements/planning/list',$data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function planning_create(Request $request)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Project ID
    $enc_id = Input::get('plan_id');
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Load view to show result
    return view('pmis/procurements/planning/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function planning_store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'planning'       => 'required',
      'tin'            => 'required',
    ]);
    $id = decrypt(Input::get('plan_id'));
    $lang = get_language();
    // Update Record
    $record = Procurement::where('project_id',$id)->first();
    $record->planning       = $request->planning;
    $record->tin            = $request->tin;
    $record->planning_desc  = $request->planning_desc;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    $record->con_current    = 'financial_letter';
    $record->con_previous   = 'financial_letter';
    $update = $record->save();
    if($update>0)
    {
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Electricity  $architectur
   * @return \Illuminate\Http\Response
   */
  public function planning_show($enc_id=0)
  {
    $data['lang'] = get_language();
    // Record ID
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // plan ID
    $enc_plan = Input::get('plan_id');
    $data['enc_plan'] = $enc_plan;
    $plan_id = decrypt($enc_plan);
    // Get data
    $data['record'] = Procurement::whereId($id)->first();
    // Load view to show result
    return view('pmis/procurements/planning/view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function planning_edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Record id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // plan ID
    $plan_id = Input::get('plan_id');
    $data['plan_id'] = $plan_id;
    // Get Record
    $data['record'] = Procurement::find($id);
    // Load view to show result
    return view('pmis/procurements/planning/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function planning_update(Request $request)
  {
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'planning'       => 'required',
      'tin'            => 'required',
    ]);
    $id = decrypt(Input::get('enc_id'));
    $record = Procurement::find($id);
    $record->planning       = $request->planning;
    $record->tin            = $request->tin;
    $record->planning_desc  = $request->planning_desc;
    $record->updated_at     = date('Y-m-d H:i:s');
    $record->updated_by     = userid();
    $updated = $record->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /** Construction Department */

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function construction_list($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Plan id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Load tabs
    $data['tabs'] = getTabs('pmis_procurement',$lang);
    session(['current_tab' => 'tab_construction']);
    // Get Plan Record 
    $data['plan'] = Plans::whereId($id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_procurement');
    }])->first();  
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get Record
    $data['record'] = Procurement::where('project_id',$id)->first();
    // Get Attachments
    $data['attachments'] = array();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/procurements/construction/list_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/procurements/construction/list',$data);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Electricity  $architectur
   * @return \Illuminate\Http\Response
   */
  public function construction_show($enc_id=0,$paln_id=0)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Record ID
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // plan ID
    $enc_plan = $paln_id;
    $data['enc_plan'] = $enc_plan;
    $plan_id = decrypt($enc_plan);
    // Load tabs
    $data['tabs'] = getTabs('pmis_procurement',$lang);
    session(['current_tab' => 'tab_construction']);
    // Get Plan Summary 
    $data['plan'] = Plans::whereId($plan_id)->with(['share' => function ($query){
      $query->where('share_from_code','pmis_procurement');
    }])->first();
    if($data['plan'])
    {
        // Get summary data
        $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
    }
    // Get data
    $data['record'] = Procurement::whereId($id)->first();
    // Get Allowed Sections for Sharing
    $data['sections'] = SectionSharings::where('code','pmis_procurement')->get();
    // Get shared data
    $data['shared']  = $data['plan']->share()->where('share_from_code','pmis_procurement')->first();
    // Get previous department
    $data['previous_dep'] = $data['plan']->share()->where('share_to_code','pmis_procurement')->first()->share_from;
    // Get all steps from config
    $data['steps'] = Config::get('static.'.$lang.'.construction_steps');
    // Get locations
    $data['project_location'] = Plan_locations::where('project_id',$plan_id)->get();
    // Get BQ sections for old data
    $data['bq_sections'] = Billofquantities::with('bq_section')->where('project_id',$plan_id)->groupBy('bq_section_id')->get();
    // Get BQ sections for new data
    $data['static_sections'] = Home::getAllStatics('bq_sections');
    // Get Attachments
    $data['attachments'] = getAttachments('procurement_attachments',$plan_id,$id,'procurement');
    // Attachment Table
    $data['section'] = 'procurement';
    $data['table'] = 'procurement_attachments';
    // Load view to show result
    return view('pmis/procurements/construction/view',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function construction_create(Request $request)
  { 
    $lang = get_language();
    $data['lang'] = $lang;
    // Project ID
    $enc_id = Input::get('plan_id');
    $id = decrypt($enc_id);
    //  echo $id;exit;
    $data['enc_id'] = $enc_id;
    // Get data
    $data['record'] = Procurement::whereId($id)->first();
    //ddd($data);
    // Check code
    $code = Input::get('con_current');
    if($code)
    {
      switch($code)
      {
        case 'financial_letter':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_financial',$data);
          break;
        }
        case 'bidding':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_bidding',$data);
          break;
        }
        case 'bidding_approval':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_bidding_approval',$data);
          break;
        }
        case 'announcement':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_announcement',$data);
          break;
        }
        case 'bids':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_bids',$data);
          break;
        }
        case 'estimate':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_estimate',$data);
          break;
        }
        case 'public_announce':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_public_announce',$data);
          break;
        }
        case 'send':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_send',$data);
          break;
        }
        case 'contract_arrangement':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_contract_arrangement',$data);
          break;
        }
        case 'contract_approval':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_contract_approval',$data);
          break;
        }
        case 'company':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_company',$data);
          break;
        }
        case 'controle':
        {
          // Load view to show result
          return view('pmis/procurements/construction/add_controle',$data);
          break;
        }
      }
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function construction_store(Request $request)
  {
    $id = decrypt(Input::get('plan_id'));
    $lang = get_language();
    // Update Record
    $record = false;
    if($request->columns)
    {
      $data = array();
      $vals = array();
      for($i=0; $i<count($request->columns); $i++)
      {
        if(strstr($request->columns[$i],'time',true) OR strstr($request->columns[$i],'date',true))// Check for data
          $vals = array($request->columns[$i] => dateProvider($request->value[$i],$lang));     
        else
          $vals = array($request->columns[$i] => $request->value[$i]);
        $data = array_merge($data, $vals);
      }
      $data = array_merge($data, $vals);
      if(count($data)>0)
      {
        Procurement::whereId($id)->update($data);
        $record = true;
      }
    }
    if($record)
    {
      $lang = get_language();
      $data['lang'] = $lang;
      // Project ID
      $enc_id = Input::get('plan_id');
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      // Get data
      $data['record'] = Procurement::whereId($id)->first();
      // Check code
      $view = Input::get('current_view');
      return view('pmis/procurements/construction/add_'.$view,$data);
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function construction_edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Record id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // plan ID
    $plan_id = Input::get('plan_id');
    $data['plan_id'] = $plan_id;
    // Get Record
    $data['record'] = Procurement::find($id);
    // Check code
    $code = Input::get('con_previous');
    if($code)
    {
      switch($code)
      {
        case 'financial_letter':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_financial',$data);
          break;
        }
        case 'bidding':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_bidding',$data);
          break;
        }
        case 'bidding_approval':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_bidding_approval',$data);
          break;
        }
        case 'announcement':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_announcement',$data);
          break;
        }
        case 'bids':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_bids',$data);
          break;
        }
        case 'estimate':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_estimate',$data);
          break;
        }
        case 'public_announce':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_public_announce',$data);
          break;
        }
        case 'send':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_send',$data);
          break;
        }
        case 'contract_arrangement':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_contract_arrangement',$data);
          break;
        }
        case 'contract_approval':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_contract_approval',$data);
          break;
        }
        case 'company':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_company',$data);
          break;
        }
        case 'controle':
        {
          // Load view to show result
          return view('pmis/procurements/construction/edit_controle',$data);
          break;
        }
      }
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function construction_update(Request $request)
  {
    $id = decrypt(Input::get('id'));
    $lang = get_language();
    // Update Record
    $record = false;
    if($request->columns)
    {
      $data = array();
      $vals = array();
      for($i=0; $i<count($request->columns); $i++)
      {
        if(strstr($request->columns[$i],'time',true) OR strstr($request->columns[$i],'date',true))// Check for data
          $vals = array($request->columns[$i] => dateProvider($request->value[$i],$lang));     
        else
          $vals = array($request->columns[$i] => $request->value[$i]);  
        $data = array_merge($data, $vals);
      }
      $data = array_merge($data, $vals);
      if(count($data)>0)
      {
        Procurement::whereId($id)->update($data);
        $record = true;
      }
    }
    if($record)
    {
      $lang = get_language();
      $data['lang'] = $lang;
      // Project ID
      $enc_id = Input::get('plan_id');
      $id = decrypt($enc_id);
      $data['enc_id'] = $enc_id;
      // Get data
      $data['record'] = Procurement::whereId($id)->first();
      // Check code
      $view = Input::get('current_view');
      return view('pmis/procurements/construction/add_'.$view,$data);
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  public function construction_create_test()
  {
    
    $lang = get_language();
    $data['lang'] = $lang;
    // Project ID
    $enc_id = Input::get('plan_id');
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get data
    $data['record'] = Procurement::where('project_id',$id)->first();
    //ddd($data);
    // Check code
    $code = Input::get('con_current');
    return view('pmis/procurements/construction/add_all_construction',$data);
   
  }

  /** Bill of Quantities */
  /**
   * Display the specified resource.
   *
   * @param  \App\Electricity  $architectur
   * @return \Illuminate\Http\Response
   */
  public function bq_list($enc_id=0,$pro_id=0,Request $request)
  {
    $proId = decrypt($pro_id);
    $recId = decrypt($enc_id);
    $lang = get_language();
    // Get Project Locations
    $project_locations = Plan_locations::where('project_id',$proId)->get();
    // Load view to show result
    return view('pmis/procurements/bill_quantities/index',compact('pro_id','enc_id','lang','project_locations'));
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Electricity  $architectur
   * @return \Illuminate\Http\Response
   */
  public function bq_show($pro_id=0,$loc_id=0,$enc_id=0,Request $request)
  {
    $locId = $loc_id;
    $proId = decrypt($pro_id);
    $recId = decrypt($enc_id);
    $lang = get_language();
    // Get Bill of Quantities
    $bill_quantities = Billofquantities::where('project_id',$proId)->where('project_location_id',$locId)->paginate(20)->onEachSide(3);
    // Get Bill of Quantities Total Price
    $total_price = Billofquantities::where('project_id',$proId)->where('project_location_id',$locId)->sum('total_price');
    // Get Project Locations
    $project_locations = Plan_locations::where('project_id',$proId)->get();
    // Get units
    $units = Home::getAllStatics('units');
    // Get procurement
    $procurement = Procurement::where('project_id',$proId)->first();
    if(Input::get('ajax') == 1)
    {
      //Static record iteration with pagination
      $counter = $request->counter;
      // Load view to show result
      return view('pmis/procurements/bill_quantities/view_ajax',compact('enc_id','pro_id','loc_id','lang','bill_quantities','counter','project_locations','total_price','procurement','units'));
    }
    else
    {
      // Load view to show result
      return view('pmis/procurements/bill_quantities/view',compact('enc_id','pro_id','loc_id','lang','bill_quantities','project_locations','total_price','procurement','units'));
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function bq_update(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'operation_type'      => 'required',
      'unit_id'             => 'required',
    ]);
    // Project id
    $project_id = decrypt($request->project_id);
    // Record id
    $record_id = decrypt($request->record_id);
    // Update bill of quantitie
    $record = Billofquantities::find($record_id);
    $record->operation_type       = $request->operation_type;
    $record->unit_id              = $request->unit_id;
    $record->remarks              = $request->remarks;
    $record->updated_at           = date('Y-m-d H:i:s');
    $record->updated_by           = userid();
    $record = $record->save();
    if($record)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function assign_users(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
      'user_id'   => 'required',
      'record_id' => 'required',
    ]);
    // Language
    $lang = get_language();
    $status = false;
    if($request->user_id)
    {
      // Get project name
      $project_name = Plans::find($request->record_id)->name;
      foreach($request->user_id as $item)
      {
        // New record
        $record = new ProjectUsers;
        $record->record_id        = $request->record_id;
        $record->user_id          = $item;
        $record->department_id    = decrypt($request->department_id);
        $record->section          = $request->section;
        $record->created_at       = date('Y-m-d H:i:s');
        $record->created_by       = userid();
        $data = $record->save();
        if($data)
        {
          // Send notification to user
          $emp = array();
          $users = User::whereId($item)->get();
          $redirect_url = route('procurement', $request->department_id);
          if($users)
          {
              foreach($users as $user) {
                  $user->notify(new NewProject(decrypt($request->department_id),trans('global.project_assigned',['pro'=>$project_name]), userid(), $redirect_url));
              }     
          }
          $status = true;
        }
      }
    }
    if($status)
    {
      Session()->flash('success',  __("global.status_success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function list_assigned_users($id)
  {
    // Get Request data
    $records = ProjectUsers::where('record_id',$id)->where('section','pmis_procurement')->get();
    // Load view to show result
    return view('pmis/procurements/list_assigned_users',compact('records'));
  }

  /**
   * Get BQ section by project locations.
   * This function is used to edit old records
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function bq_sections(Request $request)
  {
    // Get language
    $lang = get_language();
    // Project id
    $id = $request->id;
    // Get data
    $records = Billofquantities::with('bq_section')->where('project_location_id',$id)->groupBy('bq_section_id')->get();
    // Load view to display data
    return view('pmis/procurements/construction/excel_template_sections', compact('lang','id','records'));
   
  }

  /**
   * Download BQ template by section.
   * This function is used to edit old records
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function excel_template(Request $request)
  {
    // Project id
    $project_id = decrypt($request->project_id);
    // Location id
    $project_location_id = $request->project_location_id;
    // Section id
    $bq_section_id = $request->bq_section_id;
    // Get project name
    $project_name = Plans::whereId($project_id)->first()->name;
    // Get bill of quantity records
    $bill_quantity = Billofquantities::where('project_location_id',$project_location_id)->where('bq_section_id',$bq_section_id)->get();
    $section_name = Bq_section::whereId($bq_section_id)->first()->{'name_'.get_language()};
    // Load excel template file
    Excel::load('public/print/BillOfQuantityBySection.xlsx', function($file) use($bill_quantity){
      $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($bill_quantity){
        $row   = 8;
        $index = 0;
        $bq_section = 0;
        foreach($bill_quantity as $item)
        {
          $lang = get_language();
          if($bq_section != $item['bq_section_id']){
            $bq_section = $item['bq_section_id'];
            // Apply style
            $sheet->getStyle('A'.$row.':F'.$row)->applyFromArray(
              array(
                'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'EEEEEE')
                ),
                'font' => array(
                  'name' =>  'B Nazanin',
                  'size' =>  13,
                  'bold' =>  true
                )
              )
            );
            $sheet->setHeight($row, 35);
            $sheet->mergeCells('A'.$row.':F'.$row);
            // Set value
            $sheet->setCellValue('A'.$row,$item['bq_section']['name_'.$lang]);
            $row++;
            $index = 1;
          }
          // Apply style
          $sheet->getStyle('A'.$row.':F'.$row)->applyFromArray(
            array(
              'font' => array(
                'name' =>  'B Nazanin',
                'size' =>  11,
                'bold' =>  false
              )
            )
          );
          // Hide id column from user
          $sheet->getColumnDimension('B')->setVisible(false);
          // Lock columns
          //$sheet->getProtection()->setSheet(true);
          $sheet->getStyle('B'.$row.':B'.$row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
          $sheet->getStyle('C'.$row.':C'.$row)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
          // Set height for each row
          $sheet->setHeight($row, 50);
          $sheet->cell('A'.$index.':A'.$index,function($cell){
            $cell->setAlignment('center');
          });
          // Set values
          $sheet->setCellValue('A'.$row.'',$index);
          $sheet->setCellValue('B'.$row.'',$item['id']);
          $sheet->setCellValue('C'.$row.'',$item['operation_type']);
          $sheet->setCellValue('D'.$row.'',$item['amount']);
          $sheet->setCellValue('E'.$row.'','');
          $sheet->setCellValue('F'.$row.'','');
          $row++;
          $index++;
        }
      });
    })->setFilename('احجام کاری بخش '.$section_name.' پروژه '.$project_name)->export('xlsx');
  }

  /**
   * Store resource in storage using excel file.
   * This function is used to edit old records
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function excel_import(Request $request)
  {
    $request->validate([
      'fileToUpload'        => 'required|file|max:2048|mimes:xls,xlsx',
      'project_location_id' => 'required',
      'bq_sec_id'           => 'required',
    ]);
    // project id
    $project_id = decrypt($request->project_id);
    // location_id
    $location_id = $request->project_loc_id;
    // section_id
    $bq_section_id = $request->bq_sec_id;
    // Get procurement record
    $procurement = Procurement::whereId($request->rec_id)->first();
    $dataTime = date('Ymd_His');
    $file = request()->file('fileToUpload');
    $filename = $dataTime.'-'. $file->getClientOriginalName();
    $savePath = 'public/upload/';
    $file->move($savePath,$filename);
    $excel = Importer::make('Excel');
    $excel->load($savePath.$filename);
    $collection = $excel->getCollection();
    $data = $collection->slice(8);
    if(!empty($data))
    {
      try{
        $discount_price = 0;
        $item_price = 0;
        $total_price = 0;
        foreach($data as $item)
        {
          if($item['1']!='' and $item['2']!='' and $item['3']!=''){
            $discount_price = calculate_percentage($procurement->discount,$item['4']);
            $item_price = $item['4']-$discount_price;
            // Find record
            $record = Billofquantities::find($item['1']);
            $record->amount             = $item['3'];
            // Price befor discount
            $record->price_before       = $item['4'];
            $record->total_price_before = $item['5'];
            // Price after discount
            $record->price              = $item_price;
            $record->total_price        = $item_price * $item['3'];
            $record->updated_at         = date('Y-m-d H:i:s');
            $record->updated_by         = userid();
            $record->save();  
          }
        }
        // Set success message
        Session()->flash('success', __("global.success_msg"));
      }
      catch(\Exception $e){
        // Set failed message
        return Session()->flash('fail', __("global.failed_msg"));
      }
    }
    else{
      return Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store_bill_of_quantities(Request $request)
  {
    $request->validate([
        'fileToUpload'        => 'required|file|max:2048|mimes:xls,xlsx',
        'project_location_id' => 'required',
        'bq_sections'       => 'required',
    ]);
    // Get procurement record
    $record = Procurement::whereId($request->rec_id)->first();
    $lang = get_language();
    $dataTime = date('Ymd_His');
    $file = request()->file('fileToUpload');
    $filename = $dataTime.'-'. $file->getClientOriginalName();
    $savePath = 'public/upload/';
    $file->move($savePath,$filename);
    $excel = Importer::make('Excel');
    $excel->load($savePath.$filename);
    $collection = $excel->getCollection();
    $data = $collection->slice(7);
    if(!empty($data)){
          try{
            $records = array();
            $x=0;
            $discount_price = 0;
            $item_price = 0;
            $total_price = 0;
            foreach($data as $item)
            {
              if($item['0']!=''){
                $value = Config::get('static.units_result.'.$item[1].'.id');
                if(!isset($item['5']))
                {
                  $item['5'] = "";
                }
                $discount_price = calculate_percentage($record->discount,$item['3']);
                $item_price = $item['3']-$discount_price;
                $total_price = $item_price*$item['2'];
                $data_array = array(
                  'rec_id'                => $request->rec_id,
                  'project_id'            => decrypt($request->project_id),
                  'project_location_id'   => $request->project_location_id,
                  'operation_type'        => $item['0'],
                  'unit_id'               => $value,
                  'amount'                => $item['2'],
                  // Price befor discount
                  'price_before'          => $item['3'],
                  'total_price_before'    => $item['4'],
                  // Price after discount
                  'price'                 => $item_price,
                  'total_price'           => $total_price,
                  'remarks'               => $item['5'],
                  'bq_section_id'         => $request->bq_sections,
                  'created_by'            => userid(),
                  'created_at'            => date('Y-m-d H:i:s'),
                );
                $x++;
                array_push($records,$data_array);
              }
            }
            if(!empty($records))
            {
              Billofquantities::insert($records);
              Session()->flash('success', __("global.success_msg"));
            }
            else
            {
              Session()->flash('fail', __("global.failed_msg"));
            }
          }
          catch(\Exception $e){
            return Session()->flash('fail', __("global.failed_msg"));
          }
      }
     else{
      return Session()->flash('fail', __("global.failed_msg"));
     }
  }

  // Print Estimation Reports
  public function download($pro_id,$loc_id)
  {
    $id= decrypt($pro_id);
    $project_location_id= decrypt($loc_id);
    // Get Record
    $record = Estimation::where('project_id',$id)->first();
    $project_name = Plans::whereId($id)->first()->name;
    // Get BQ Data
    $bill_quantity = Billofquantities::where('project_id',$id)->where('project_location_id',$project_location_id)->get();
    Excel::load('public/print/BillOfQuantity.xlsx', function($file) use($bill_quantity,$record){
      $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($bill_quantity,$record){
        $row   = 8;
        $index = 0;
        $bq_section = 0;
        foreach($bill_quantity as $item)
        {
          $lang = get_language();
          if($bq_section != $item['bq_section_id']){
            $bq_section = $item['bq_section_id'];
            // Apply style
            $sheet->getStyle('A'.$row.':G'.$row)->applyFromArray(
              array(
                'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'EEEEEE')
                ),
                'font' => array(
                  'name' =>  'B Nazanin',
                  'size' =>  13,
                  'bold' =>  true
                )
              )
            );
            $sheet->setHeight($row, 40);
            $sheet->mergeCells('A'.$row.':G'.$row);
            // Set value
            $sheet->setCellValue('A'.$row.'',$item['bq_section']['name_'.$lang]);
            $row++;
            $index = 1;
          }
          // Apply style
          $sheet->getStyle('A'.$row.':G'.$row)->applyFromArray(
            array(
              'font' => array(
                'name' =>  'B Nazanin',
                'size' =>  11,
                'bold' =>  false
              )
            )
          );
          $sheet->setHeight($row, 50);
          $sheet->setCellValue('A'.$row.'',$index);
          $sheet->setCellValue('B'.$row.'',$item['operation_type']);
          if($item->unit){
            $unit = $item->unit->{'name_'.$lang};
          }
          $sheet->setCellValue('C'.$row.'',$unit);
          $sheet->setCellValue('D'.$row.'',$item['amount']);
          $sheet->setCellValue('E'.$row.'',$item['estimated_price']);
          $sheet->setCellValue('F'.$row.'',$item['estimated_total_price']);
          $sheet->setCellValue('G'.$row.'',$item['remarks']);
          $row++;
          $index++;
        }
      });
    })->setFilename(__("estimation.bill_quantitie")." ".$project_name)->export('xlsx');
  }
}
?>
