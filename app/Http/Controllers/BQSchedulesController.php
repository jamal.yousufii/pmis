<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\models\Plans;
use App\models\Billofquantities;
use App\models\BillOfQuantitiesLog;
use App\models\BillOfQuantitiesBase;
use App\models\Plan_locations;
use App\models\Requests;
use Redirect;

class BQSchedulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0,$dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $dep_id;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_progress'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        $data['lang'] = $lang;
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'tab_bq_schedule']);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        // Load view to show result
        return view('pmis/progress/schedule/index',$data);
    }

    public function list($enc_id=0,$dep_id=0,$loc_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_progress'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        $data['lang'] = $lang;
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Project Location id
        $data['project_location_id'] = $loc_id;
        session(['project_location_id' => $loc_id]);
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'tab_bq_schedule']);
        // Get Bill of Quantities
        $data['records']  = Billofquantities::where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->orderBy('bq_section_id','asc')->paginate(20)->onEachSide(3);
        $data['base']     = BillOfQuantitiesBase::where('project_id',$id)->where('project_location_id',$loc_id)->get();
        $data['versions'] = BillOfQuantitiesLog::where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->get();
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location'] = Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations'] = Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        $data['excluded_locations_array'] = $data['excluded_locations']->pluck('id')->toArray();
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('pmis/progress/schedule/list_actual_ajax',$data);
        }
        else
        {
            // Load view to show result
            return view('pmis/progress/schedule/list_actual',$data);
        }
    }

    public function base($enc_id=0,$dep_id=0,$loc_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        $data['dep_id'] = $depID;
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_progress'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_progress"]);
        $lang = get_language();
        $data['lang'] = $lang;
        // Project id
        $id = decrypt($enc_id);
        $data['enc_id'] = $enc_id;
        // Project Location id
        $data['project_location_id'] = $loc_id;
        session(['project_location_id' => $loc_id]);
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'tab_bq_schedule']);
        // Get Bill of Quantities
        $data['records'] = BillOfQuantitiesBase::where('project_id',$id)->where('project_location_id',$loc_id)->orderBy('id','asc')->orderBy('bq_section_id','asc')->paginate(20)->onEachSide(1);
        // Project locations
        if(is_admin())// Get all locations for admin users
        {
            $data['project_location']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
                $query->where('project_id',$id);
                $query->where('department',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        else// Get project locations based on users access
        {
            $data['project_location']= Plan_locations::whereHas('department_staff', function($query) use ($id, $depID){
                $query->where('userid',userid());
                $query->where('project_id',$id);
                $query->where('department_id',$depID);
            })->where('status','0')->orderBy('id','desc')->get();
        }
        $data['excluded_locations']= Plan_locations::whereHas('Projects', function($query) use ($id, $depID){
            $query->where('project_id',$id);
            $query->where('department',$depID);
        })->where('status','<>','0')->orderBy('id','desc')->get();
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('pmis/progress/schedule/list_base_ajax',$data);
        }
        else
        {
            // Load view to show result
            return view('pmis/progress/schedule/list_base',$data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->start_date!="" && $request->end_date!="" && $request->duration!="0" && $request->shift!="0" && $request->resources!="0")
        {
            // Validate the request...
            $validates = $request->validate([
                'start_date' => 'required',
                'end_date'   => 'required',
                'duration'   => 'required|min:1',
                'shift'      => 'required|min:1',
                'resources'  => 'required|min:1',
            ]);
            // Language
            $lang = get_language();
            // Update Record
            $record = Billofquantities::find($request->id);
            $record->start_date = dateProvider($request->start_date,$lang);
            $record->end_date   = dateProvider($request->end_date,$lang);
            $record->duration   = $request->duration;
            $record->shift      = $request->shift;
            $record->resources  = $request->resources;
            if($request->started){
                $record->started  = $request->started;
            }
            $record->created_at = date('Y-m-d H:i:s');
            $record->created_by = userid();
            $data = $record->save();
            if($data)
            {
                // Get Log
                $log_data = array();
                $log = $record->replicate();
                $log = $log->toArray();
                // Insert into log from orignal record
                $log_data = $log;
                $log_data['id'] = $record->id;
                $log_data['logged_at'] = date('Y-m-d H:i:s');
                $log_data['logged_by'] = userid();
                BillOfQuantitiesLog::insert($log_data);

                Session()->flash('success', __("global.success_msg"));
            }
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            }
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Water  $architectur
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//Record id
        $data['enc_id'] = $enc_id;
        // Load tabs
        $data['tabs'] = getTabs('pmis_progress',$lang);
        session(['current_tab' => 'tab_bq_schedule']);
        // Get Record
        $data['record'] = Billofquantities::find($id);
        // Get plan record
        $data['plan'] = Plans::find($data['record']->project_id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load view to show result
        return view('pmis/progress/schedule/edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$enc_id=0)
    {
        // Validate the request...
        $validates = $request->validate([
            'start_date' => 'required',
            'end_date'   => 'required',
            'duration'   => 'required',
            'shift'      => 'required',
            'resources'  => 'required',
        ]);
        // Language
        $lang = get_language();
        $id = decrypt($enc_id);//Record id
        // Update Record
        $record = Billofquantities::find($id);
        $record->start_date = dateProvider($request->start_date,$lang);
        $record->end_date   = dateProvider($request->end_date,$lang);
        $record->duration   = $request->duration;
        $record->shift      = $request->shift;
        $record->resources  = $request->resources;
        if($request->started){

            $record->started  = $request->started;
        }
        $record->updated_at = date('Y-m-d H:i:s');
        $record->updated_by = userid();
        $data = $record->save();
        if($data)
        {
            // Get Log
            $log_data = array();
            $log = $record->replicate();
            $log = $log->toArray();
            // Insert into log from orignal record
            $log_data = $log;
            $log_data['id'] = $id;
            $log_data['logged_at'] = date('Y-m-d H:i:s');
            $log_data['logged_by'] = userid();
            BillOfQuantitiesLog::insert($log_data);

            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * calculate the resource in schedule.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BQSchedules
     * @return \Illuminate\Http\Response
     */
    public function calculate_schedules(Request $request)
    {
        // Language
        $lang = get_language();
        $id             = $request->id;
        $project_id     = decrypt($request->project_id);
        $location_id    = $request->location_id;
        $duration       = $request->duration;
        $status         = $request->status;
        $shift          = $request->shift;
        $resources      = $request->resources;
        $def_duration   = $request->def_duration;
        $def_resources  = $request->def_resources;

        if($request->start_date){
            $from = dateProvider($request->start_date,$lang);
        }else{
            $from = "";
        }
        if($request->end_date){
            $to = dateProvider($request->end_date,$lang);
        }else{
            $to = "";
        }
        // Get all public holidays
        $holidayDays = holidays($location_id);
        // Get week working days only
        $workingDays = workingDays($location_id);
        // Get working days duration
        if($status=='end_date'){
            if($request->end_date<$request->start_date){
                return "0_0";
            }else{
                $working_days = working_days($from,$to,$workingDays,$holidayDays);
                $resource     = getProportion($working_days,1,$duration,1);
                return $resource."_1";
            }
        }elseif($status=='duration'){
            $result = working_days_end_date($from,$workingDays,$holidayDays,$duration);
            return $duration."_".$result;
        }elseif($status=='shift'){
            $duration = ceil($duration / $resources);
            if($shift==2){
                $duration = ceil($duration / 2);
            }
            return working_days_end_date($from,$workingDays,$holidayDays,$duration);
        }elseif($status=='resources'){

            $duration = ceil($duration / $resources);
            if($shift==2){
                $duration = ceil($duration / 2);
            }
            return working_days_end_date($from,$workingDays,$holidayDays,$duration);
        }
    }

    /**
     * calculate the resource in schedule.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BQSchedules
     * @return \Illuminate\Http\Response
     */
    public function make_base($enc_id=0,$loc_id=0)
    {
        $lang = get_language();
        // Project ID
        $project_id  = decrypt($enc_id);
        // Location ID
        $location_id = decrypt($loc_id);
        // Language
        if($project_id!=0 and $location_id!=0)
        {
            // Insert duplicate record into log table
            $record = Billofquantities::where('project_id',$project_id)->where('project_location_id',$location_id)->get();
            foreach($record as $rec){
                $log_data = array();
                $log = $rec->replicate();
                $log = $log->toArray();
                // Insert into log from orignal record
                $log_data = $log;
                $log_data['id'] = $rec->id;
                $log_data['logged_at'] = date('Y-m-d H:i:s');
                $log_data['logged_by'] = userid();
                BillOfQuantitiesBase::insert($log_data);
            }
            Session()->flash('success', __("global.status_success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }
}
?>
