<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\models\Plans;
use App\models\Settings\Portfolios;
use App\models\Settings\PortfoliosSub;
use App\models\Billofquantities;
use App\models\BillOfQuantitiesBase;
use App\models\Plan_locations;
use Config;
use Redirect;

class PortfoliosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depID = decrypt($dep_id);
        // Check user access in this section
        if(!check_my_section(array($depID),'pmis_portfolio'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_portfolio"]);
        // Set current department
        session(['current_department' => $dep_id]);

        $lang = get_language();
        // Get URL Segments
        $segments = request()->segments();
        // Get Project Category
        if($depID==1)
        {
            $categories = Portfolios::all();
        }
        else
        {
            $categories = Portfolios::where('department_id',$depID)->get();
        }
        // Load view to display data
        return view('pmis/portfolio/index',compact('categories','segments','lang','dep_id'));
    }

    /**
     * Function to display projects based on selected portfolio.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // Language
        $lang = get_language();
        // portfolio_id
        $portfolio_id = $request->portfolio_id;
        // Get all projects
        $project_id = PortfoliosSub::where('portfolio_id',$portfolio_id)->pluck('project_id')->toArray();
        $excluded_projects = array();
        $excluded_loc_id   = array();
        $location_id = Plan_locations::whereIn('project_id',$project_id)->pluck('id')->toArray();
        if($project_id)
        {
            $excluded_projects  = Plans::whereIn('id',$project_id)->doesntHave('bill_quantities_base')->pluck('id')->toArray();
            $excluded_locations = Plan_locations::whereIn('id',$location_id)->with('Projects')->doesntHave('bill_quantities_base')->get();
            if($excluded_locations){
                foreach($excluded_locations as $item){
                    $excluded_loc_id[]   = $item->id;
                }
            }
        }
        if($excluded_loc_id)
        {
            $location_id = array_diff($location_id,$excluded_loc_id);
        }
        if($excluded_projects)
        {
            $project_id = array_diff($project_id,$excluded_projects);
        }
        // Get Start and End date of current fiscal year
        $start_fiscal_date = Config::get('static.fiscal_year_months.01.start_date');
        $end_fiscal_date   = Config::get('static.fiscal_year_months.12.end_date');
        $location_id = array_values($location_id);
        $WeekDays = weekDaysAll($location_id);
        // Get project location  holidays
        $holidays = holidaysAll($location_id);
        // cfy working days
        //$cfy_wrkd = working_days($start_fiscal_date,$end_fiscal_date,$WeekDays,$holidays);
        $cfy_wrkd = 0; // Not Used

        // Get first date of planed schedule
        $getFirstDateOfProject = getFirstLastDateOfProject($location_id,'min(start_date)','project_location_id');
        // Get last date of planed schedule
        $getLastDateOfProject = getFirstLastDateOfProject($location_id,'max(end_date)','project_location_id');
        // Get project budget from base
        $project_base_budget = BillOfQuantitiesBase::whereIn('project_location_id',$location_id)->get();
        // Get project budget from working
        $project_working_budget = Billofquantities::whereIn('project_location_id',$location_id)->get();

        // Calcualte baseline budget from project start to end
        $baseline_budget = $project_base_budget->sum('total_price');
        // Calcualte working budget from project start to end
        $working_budget  = $project_working_budget->sum('total_price');
        // Calcualte actual budget from project start to end
        $actual_budget = getBudgetActual($location_id,$getFirstDateOfProject,$getLastDateOfProject)->sum('total_price');

        // Calcualte baseline budget from current fiscal year start to end
        $budget_base_cfy = setCfyDateRange($project_base_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_base_cfy = sortBudgetDataCfy($budget_base_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        $total_baseline = round((count($budget_base_cfy) > 0 ? array_sum(array_column($budget_base_cfy,'total_price')) : 0 ),5);
        // Calcualte working budget from current fiscal year start to end
        $budget_working_cfy = setCfyDateRange($project_working_budget,$start_fiscal_date,$end_fiscal_date,$cfy_wrkd,$WeekDays,$holidays);
        $budget_working_cfy = sortBudgetDataCfy($budget_working_cfy,$WeekDays,$holidays,$getLastDateOfProject);
        $total_working = (count($budget_working_cfy) > 0 ? array_sum(array_column($budget_working_cfy,'total_price')) : 0 );
        // Calcualte actual budget from current fiscal year start to end
        $budget_actual_array = getBudgetActual($location_id,$start_fiscal_date,$end_fiscal_date);
        $budget_actual = sortBudgetActual($budget_actual_array,$start_fiscal_date,$end_fiscal_date,false,$getLastDateOfProject);
        $total_expenditure = (count($budget_actual) > 0 ? array_sum(array_column($budget_actual,'total_price')) : 0 );


        // Get Budget baseline till now
        $budget_baseline_array = setCfyDateRange($project_base_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $budget_baseline_now = sortBudgetDataCfy($budget_baseline_array,$WeekDays,$holidays);
        $budget_baseline_now = round((count($budget_baseline_now) > 0 ? array_sum(array_column($budget_baseline_now,'total_price')) : 0 ),5);

        // Get total price from working budget of current fiscal year start till now
        $budget_working_tile_now = setCfyDateRange($project_working_budget,$start_fiscal_date,date('Y-m-d'),$cfy_wrkd,$WeekDays,$holidays);
        $total_budget_working_now = sortBudgetDataCfy($budget_working_tile_now,$WeekDays,$holidays);
        $total_budget_working_now = (count($total_budget_working_now) > 0 ? array_sum(array_column($total_budget_working_now,'total_price')) : 0 );

        // Calcualte actual budget from current fiscal year start to current date
        $budget_actual_now = getBudgetActual($location_id,$start_fiscal_date,date('Y-m-d'));
        $actual_budget_now = sortBudgetActual($budget_actual_now,$start_fiscal_date,date('Y-m-d'),false);
        $total_actual_budget_now = (count($actual_budget_now) > 0 ? array_sum(array_column($actual_budget_now,'total_price')) : 0 );
        /** Start: Estimated plan budget */
        $estimated_budget = 0;
        if($total_budget_working_now!=0){
            $est_spi = round($total_actual_budget_now/$total_budget_working_now,5);
            $estimated_budget = $total_working * $est_spi;
        }
        /** End: Estimated plan budget */

        // Get total price of Bill of Quantities
        $BQ_total_price = $total_working;
        $budget_share       = array();
        $budget_expenditure = array();

        // Get project invoiced budget till now
        $budget_invoiced_now = getInvoicedBudget($location_id,$start_fiscal_date,date('Y-m-d'));
        // Get project paid budget till now
        $budget_paid_now = getPaidBudget($location_id,$start_fiscal_date,date('Y-m-d'));

        // Get projects detaile
        $projects_detail  = Plans::whereHas('procurement', function($query){
            $query->orderBy('contract_price');
        })->whereHas('bill_quantities_base')->whereIn('id',$project_id)->get();
        // Load view to show result
        return view('pmis/portfolio/show',compact('portfolio_id','lang','project_id','baseline_budget','working_budget','actual_budget','total_baseline','total_working','excluded_projects','excluded_locations','total_expenditure','budget_base_cfy','budget_working_cfy','budget_actual','estimated_budget','BQ_total_price','budget_share','budget_expenditure','start_fiscal_date','end_fiscal_date','budget_invoiced_now','budget_paid_now','projects_detail','project_id','budget_baseline_now','total_actual_budget_now','total_budget_working_now'));
    }
}
