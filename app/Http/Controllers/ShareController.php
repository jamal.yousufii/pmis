<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Share; 
use Illuminate\Support\Facades\Input;
use App\models\Plans;
use App\Notifications\NewProject;
use App\models\Share as ShareModel; 
use App\models\Authentication\Sections; 
use App\User;
use DB;

class ShareController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }
    
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // 
  }

  /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function create()
  {
      //
  }

  /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Share $request)
  {
    $lang = get_language();
    $request['user_id']       = userid(); 
    $request['department_id'] = departmentid(); 
    $request['doc_date']      = dateProvider($request->doc_date,$lang); 
    $result = ShareModel::create($request->all()); 
    $table = $request->table;
    // Update process status in parent table
    DB::connection('pmis')->table($table)->where($request->column,$request->record_id)->update(['process' => 1]);
    //Project Id 
    $project_id = $request->record_id;
    //Get Projects Name with project Id
    $project = Plans::whereId($project_id)->first();
    $project_name = ($project ? $project->name: ''); 
    // Set msg
    if($result)
    {
      // Start Notification
      if($table=='requests')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'pla_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('plans.show',encrypt($request->record_id));
        foreach($users as $user) {
          $user->notify(new NewProject($request->record_id, 'global.request_shared', userid(), $redirect_url));
        }
      }
      if($table=='projects')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'sur_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('list_survey',encrypt($request->record_id));
        if($users)
        {
          foreach($users as $user) {
            $user->notify(new NewProject($request->record_id, trans('global.plan_shared',['pro' => $project_name]), userid(), $redirect_url));
          }
        }
      }
      if($table=='surveys')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'des_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('architecture.show',encrypt($request->record_id));
        if($users)
        {
          foreach($users as $user) {
            $user->notify(new NewProject($request->record_id,trans('global.survey_shared',['pro' => $project_name]), userid(), $redirect_url));
          }  
        }
      }
      if($table=='architectures')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'est_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('estimations.show',encrypt($request->record_id));
        foreach($users as $user) {
          $user->notify(new NewProject($request->record_id,trans('global.estimation_added',['pro' => $project_name]), userid(), $redirect_url));
        }
      }
      if($table=='estimations')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'imp_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('implements.show',encrypt($request->record_id));
        if($users)
        {
          foreach($users as $user) {
            $user->notify(new NewProject($request->record_id,trans('global.procurment_shared',['pro' => $project_name]), userid(), $redirect_url));
          }
        }
      }
      if($table=='implements')
      {
        $users = User::with(['roles' => function ($query) {
          $query->where('code', 'imp_add');
        }])->where('department_id',$request->share_to)->get();
        $redirect_url = route('executive/list',encrypt($request->record_id));
        if($users)
        {
          foreach($users as $user) {
            $user->notify(new NewProject($request->record_id, trans('global.procurement_added',['pro' => $project_name]), userid(), $redirect_url));
          }
        }
      }
      // End Notification
      Session()->flash('success', __("global.share_success"));
    }
    else{
      Session()->flash('fail', __("global.share_failed"));
    } 
  }


  /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function show($id)
  {
      //
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function edit($id)
  {
      //
  }

  /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update(Request $request, $id)
  {
      //
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy($id, Request $request)
  {
    $id = decrypt($id);
    $rec_id = decrypt($request->rec_id);
    $table  = $request->table;
    $column = $request->column;
    // ShareModel is Alias name for share 
    $result = ShareModel::whereId($id)->delete(); 
    if($result)
    {
      DB::connection('pmis')->table($table)->where($column,$rec_id)->update(['process' => 0]);
      Session()->flash('success', __("global.unshare_succ_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }


  public function bringDepartments()
  {
    // Get Language
    $lang = get_language();
    // Get section code
    $code = Input::get('id');
    // Get previous department
    $previous_dep = Input::get('prev_dep');
    // Get availble departments for sharing
    $section = Sections::with('departments')->where('code',$code)->first();
    // Load view to show result
    return view('pmis/share/bringDepartment',compact('section','lang','previous_dep'));
  }
}
