<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Notifications\NewProject;
use App\models\Requests;
use App\models\Plans;
use App\models\Architecture;
use App\models\Plan_locations;
use App\models\ArchitectureProjectLocation;
use App\models\Settings\Statics\Employees; 
use App\models\ArchitectureTeam;
use App\User;

class ArchitectureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($request->id);//project id
        $data['enc_id'] = $request->id; //Encrupted ID
        $data['locations'] = Plan_locations::where('project_id',$id)->orderBy('id','desc')->with(['province'])->get(); // Get Plan Location
        $loc_data = ArchitectureProjectLocation::where('project_id',$id)->get(); // Get Inserted Location
        $added_loc = array();
        if($loc_data)
        {
            foreach($loc_data as $item)
            {
                $added_loc[] = $item->project_location_id;
            }
        }
        $data['loc_data'] = $added_loc;
        $data['employees'] = Employees::where('section','tab_architecture')->get(); // Get all employees
        $data['record'] = Plans::find($id); // Get project summary record
        return view('pmis/designs/architecture/create',$data); // Load view to show result
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = get_language();
        $project_id  = decrypt($request->project_id);
        if($request->has_data=='no' and $request->has_data!="")// Check if Design has no data
        {
            // New architecture 
            $urn = GenerateURN('architectures','urn');
            $record = new Architecture;
            $record->urn            = $urn;
            $record->project_id     = $project_id;
            $record->description    = $request->description_noData; 
            $record->status         = '1'; 
            $record->has_data       = $request->has_data; 
            if($record->save())
            {
                Session()->flash('success', __("global.success_msg")); 
            } 
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            } 
        }
        else
        {
            // Validate the request...
            $validates = $request->validate([
                'employee_id'         => 'required',
                'start_date'          => 'required',
                'end_date'            => 'required',
                'project_location_id' => 'required',
            ]);
            $request['created_by']          = userid(); 
            $request['department_id']       = departmentid(); 
            $request['start_date']          = dateProvider($request->start_date,$lang); 
            $request['end_date']            = dateProvider($request->end_date,$lang);
            $request['project_id']          = $project_id;
            $updated = false; 
            if(!empty($request->version_id))
            {
                $architecture_id = decrypt($request->version_id); 
                $request['version']    = 'latest';  
                $first_version_arch = Architecture::find($architecture_id); 
                $first_version_arch->version = 'updated'; 
                $first_version_arch->update(); 
                $request['version_id'] = $architecture_id.','.$first_version_arch->version_id; 
                $updated = true; 
            }
            // Generate URN
            if($updated){
                $urn = $first_version_arch->urn;
            }else{
                $urn = GenerateURN('architectures','urn');
            }
            $request['urn'] = $urn; 
            $architecture = Architecture::create($request->except(['employees','file','file_name','project_location_id','description_noData'])); 
            // Architecture Team Members
            if($request->employees)
            {
            $team=array();
            for($i=0;$i<count($request->employees);$i++)
            {
                $team[] = array(
                'architecture_id' => $architecture->id,
                'employee_id'     => $request->employees[$i]
                );
            }
            if($team)
            {
                ArchitectureTeam::insert($team);
            }
            }
            $data = array(); 
            foreach($request->project_location_id as $id)
            {
                $data[] = [
                    'record_id'  => $architecture->id,
                    'project_id' => $project_id,
                    'project_location_id' => $id,
                    'section' => 'architecture'
                ];             
            } //Make record for bulk insert 
            $arch_proj_locatoin = ArchitectureProjectLocation::insert($data); //Bulk insertion 
            if($arch_proj_locatoin)
            {
                //Upload File
                if($request->file)
                {
                    if($request->file)
                    {
                        $i = 0;   
                        foreach($request->file as $file)
                        {  
                            $isUploaded = uploadAttachments($project_id,$architecture->id,'architecture',$file,$request->file_name[$i],$i,'design_attachments'); 
                            if(!$isUploaded)
                            {
                                Session()->flash('att_failed', __("global.attach_failed"));
                            }
                            $i++; 
                        }
                    }
                }
                // Check if record is version then send notification to visa 
                if($updated==true){
                    $emp = User::getUserRoleCode('vis_approval');//Get users who have role of Visa Approval
                    $users = User::whereIn('id',$emp)->get();
                    $redirect_url = route('architecture.show', encrypt($request->project_id));
                    $project_name = Plans::whereId($project_id)->first()->name;
                    foreach($users as $user) {
                        $user->notify(new NewProject($id, trans('designs.arch_rej_update',['pro'=>$project_name]), userid(), $redirect_url));
                    }
                }
                
                Session()->flash('success', __("global.success_msg"));
            }
            else
            {
                Session()->flash('fail', __("global.failed_msg"));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Architecture  $architectur
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//project id
        $data['enc_id'] = $enc_id;
        $data['with_parent_id'] = $enc_id; 
        // Load tabs
        $data['tabs'] = getTabs('pmis_design',$lang);
        session(['current_section' => "pmis_design"]);
        session(['current_tab' => 'tab_architecture']);
        // Get plan record
        $data['plan'] = Plans::find($id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Get record
        $data['architecture'] = Architecture::where('project_id',$id)->whereIn('version',['first','latest'])->orderBy('id','desc')->get();
        // Get record and check if has no data is true
        $data['architecture_data'] = Architecture::where('project_id',$id)->first();
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',$id,0,'architecture');
        // Attachment Table
        $data['table']   = 'design_attachments';
        $data['section'] = 'architecture';
        // Get remained locations
        $data['plan_loc']     = Plan_locations::where('project_id',$id)->get()->count(); // Get Plan Location
        $data['inserted_loc'] = ArchitectureProjectLocation::where('project_id',$id)->get()->count(); // Get Inserted Location
        // Load view to show result
        return view('pmis/designs/architecture/architecture',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $project_id = decrypt($request->project_id);//project id
        $data['enc_id'] = $request->project_id; //Encrupted ID
        $data['locations'] = Plan_locations::where('project_id',$project_id)->orderBy('id','desc')->with(['province'])->get(); // Get Plan Location
        $loc_data = ArchitectureProjectLocation::where('project_id',$project_id)->where('record_id','!=',$request->id)->get(); // Get Inserted Location
        $added_loc = array();
        if($loc_data)
        {
            foreach($loc_data as $item)
            {
                $added_loc[] = $item->project_location_id;
            }
        }
        $data['loc_data'] = $added_loc;
        $data['record'] = Plans::find($project_id); // Get project summary record
        $data['architecture'] = Architecture::with('architectureProjectLocation.projectLocation')->findOrFail($request->id);
        $data['employees']   = Employees::where('section','tab_architecture')->get(); // Get all employees
        // Get team members
        $team = ArchitectureTeam::where('architecture_id',$request->id)->get();
        $team_members = array();
        if($team){
            foreach($team as $item){
                $team_members[] = $item->employee_id;
            }    
        }
        $data['team_members'] = $team_members;
        // Get Current Selected Locations
        $data['location_selected'] = multiSelectArchitectureLocation($data['architecture'],'architectureProjectLocation');
        // Load view to show result
        return view('pmis/designs/architecture/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $lang = get_language();
        $data = array(
            'start_date'  => dateProvider($request->start_date,$lang), 
            'end_date'    => dateProvider($request->end_date,$lang), 
            'employee_id' => $request->employee_id,
            'description' => $request->description,
        ); 
        $record_id = decrypt($request->version_id);
        $project_id = decrypt($request->project_id); 
        $architecture = Architecture::findOrFail($record_id); 
        $architecture->update($data); 
        // Architecture Team Members
        if($request->employees)
        {
          $team=array();
          for($i=0;$i<count($request->employees);$i++)
          {
            $team[] = array(
              'architecture_id' => $architecture->id,
              'employee_id'     => $request->employees[$i]
            );
          }
          if($team)
          {
            // Delete old records
            ArchitectureTeam::where('architecture_id',$architecture->id)->delete();
            // Add new records
            ArchitectureTeam::insert($team);
          }
        }
        $project_location = array();
        // Delete the record first 
        $architecture->architectureProjectLocation()->delete();  
        foreach($request->project_location_id as $id)
        {
                $project_location[] = [
                    'record_id'  => $record_id,
                    'project_id' => $project_id,
                    'project_location_id' => $id,
                    'section' => 'architecture'
                ];             
        } //Make record for bulk insert 
        $arch_proj_locatoin = ArchitectureProjectLocation::insert($project_location); //Bulk insertion 
        if($arch_proj_locatoin)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-12-05 12:03:53 
     * @Desc:  
     */    
    public function approveArchitecture(Request $request)
    { 
        $data['lang'] = get_language();
        $data['enc_id'] = $request->project_id;
        // Get record
        $data['architecture'] = Architecture::findOrFail($request->id);
        $data['version_comments'] = Architecture::whereIn('id',forWhereIn($data['architecture']->version_id))->orderBy('id','desc')->get();       
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',decrypt($request->project_id),$request->id,'architecture');
        $data['table'] = 'design_attachemtns'; 
        $data['with_parent_id']  = $request->project_id; 
        $data['section'] = 'architecture'; 
        // Load view to show result
        return view('pmis/designs/architecture/approve',$data);
    }

    /** 
     * @Author: Jamal Yuusufi  
     * @Date: 2019-12-07 15:22:43 
     * @Desc: Do Approve the architecture by the Visa    
     */     
    public function doApproveArchitecture(Request $request)
    {     
        // Validate the request...
        $validates = $request->validate([
            'status'            => 'required',
            'changed_comment'   => 'required',
        ]);
        $project_id    = decrypt($request->project_id);//project_id
        $project_name = Plans::whereId($project_id)->first()->name;
        $id = $request->id; 
        $record = Architecture::findOrFail($id);
        // to be approved
        $request['changed_status_by'] = userid();
        $request['changed_date'] = date('Y-m-d H:i:s');                
        $record->update($request->except(['project_id','id'])); //change status 
        if($request->status == 1)
        {
            // Start Notification
            $employees = array();
            $str_emp = User::getUserRoleCode('str_add');//Get users who have access to add structure
            $ele_emp = User::getUserRoleCode('ele_add');//Get users who have access to add electricity
            $wat_emp = User::getUserRoleCode('wat_add');//Get users who have access to add water
            $mec_emp = User::getUserRoleCode('mec_add');//Get users who have access to add mechanic
            $employees = array_merge($str_emp,$ele_emp,$wat_emp,$mec_emp);
            $emp = array_unique($employees);
            $users = User::whereIn('id',$emp)->get();
            $redirect_url = route('architecture.show', encrypt($project_id));
            if($users)
            {
                foreach($users as $user) {
                    $user->notify(new NewProject($project_id, trans('global.architecture_added',['pro'=>$project_name]), userid(), $redirect_url));
                }
            }
            // Send notification to owner
            $emp = array();
            $emp[] = $record->created_by;//Get owner of record to send him or her notification
            $users = User::whereIn('id',$emp)->get();
            $redirect_url = route('architecture.show', encrypt($project_id));
            if($users)
            {
                foreach($users as $user) {
                    $user->notify(new NewProject($project_id,trans('global.architecture_approved',['pro'=>$project_name]), userid(), $redirect_url));
                }     
            }
            
            // End Notification
            Session()->flash('success', __("designs.approved_msg"));
        }
        else
        {
            // Start Notification
            $emp = array();
            $emp[] = $record->created_by;//Get owner of record to send him or her notification
            $users = User::whereIn('id',$emp)->get();
            $redirect_url = route('architecture.show',encrypt($project_id));
            foreach($users as $user) {
                $user->notify(new NewProject($project_id,trans('global.architecture_rejcted',['pro'=>$project_name]), userid(), $redirect_url));
            }
            // End Notification
            Session()->flash('success', __("designs.rejected_msg"));
        }
        $record->save();

    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-12-10 11:01:52 
     * @Desc: View Architecture   
     */      
    public function view(Request $request)
    {
        $data['lang'] = get_language();
        $data['enc_id'] = $request->project_id;
        // Get record
        $data['architecture'] = Architecture::findOrFail($request->id);
        // Get Attachments
        $data['attachments'] = getAttachments('design_attachments',decrypt($request->project_id),$request->id,'architecture');
        $data['table'] = 'design_attachments'; 
        $data['with_parent_id']  = $request->project_id; 
        $data['record_id']       = $request->id;  
        $data['section']         = 'architecture';
        // Get Versions
        $data['versions'] = Architecture::whereIn('id',forWhereIn($data['architecture']->version_id))->orderBy('id','asc')->get();
        // Load view to show result
        return view('pmis/designs/architecture/view',$data);
    }

    /** 
     * @Author: Jamal YOusufi  
     * @Date: 2019-12-25 15:16:12 
     * @Desc: Show Version of the Architecture  
     */      
    public function showVersion(Request $request)
    {
       $lang     = get_language(); 
       $versions = Architecture::whereIn('id',forWhereIn($request->version_id))->orderBy('id','desc')->get();
       return view('pmis/designs/architecture/version',compact(['versions','lang'])); 
    }

}
