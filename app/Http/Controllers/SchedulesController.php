<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\session;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Estimation;
use App\models\Billofquantities;
use App\models\Requests;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($enc_id=0, Request $request)
    {
        if(check_my_section('pmis_schedule'))
        {
            // check for project id
            if($enc_id===0 && session('project_id')!='')
            {
                $enc_id = encrypt(session('project_id'));
            }
            $lang = get_language();
            $data['lang'] = $lang;
            // Plan id
            $id = decrypt($enc_id);
            $data['enc_id'] = $enc_id;
            // Contractor id
            $con_id = session('current_contractor');
            // Contractor id
            $data['con_id'] = $con_id;
            // Department id
            $data['dep_id'] = session('current_department');
            // Get plan record
            $data['plan'] = Plans::find($id);
            if($data['plan'])
            {
                // Get summary data
                $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
            }
            // Get locations
            $data['project_location'] = Plan_locations::whereHas('contractor_staff', function($query) use ($id, $con_id){
                $query->where('user_id',userid());
                $query->where('project_id',$id);
                $query->where('contractor_id',decrypt($con_id));
            })->orderBy('id','desc')->get();

            session(['current_section' => "pmis_schedule"]);
            session(['project_id' => $id]);
            $request->session()->flash('extra_section','for_project');
            // Load view to show result
            return view('pmis/contractor_project/schedules/index',$data);
        }
        else
        {
          return view('access_denied');
        }
    }

    //get Schedules list based on locations
    public function list($enc_id,$loc_id, Request $request)
    {    
        $project_location_id = $loc_id;
        if(check_my_section('pmis_schedule'))
        {
            // check for project id
            if($enc_id==0 and session('project_id')!='')
            {
                $enc_id = encrypt(session('project_id'));
            }
            $lang = get_language();
            $data['lang'] = $lang;
            // Plan id
            $id = decrypt($enc_id);
            $data['enc_id'] = $enc_id;
            // Contractor id
            $con_id = session('current_contractor');
            // Contractor id
            $data['con_id'] = $con_id;
            // Department id
            $data['dep_id'] = session('current_department');
            // Get plan record
            $data['plan'] = Plans::find($id);
            if($data['plan'])
            {
                // Get summary data
                $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
            }
            // Get locations
            // Get locations
            $data['project_location'] = Plan_locations::whereHas('contractor_staff', function($query) use ($id, $con_id){
                $query->where('user_id',userid());
                $query->where('project_id',$id);
                $query->where('contractor_id',decrypt($con_id));
            })->orderBy('id','desc')->get();

            session(['current_section' => "pmis_schedule"]);
            $request->session()->flash('extra_section','for_project');
            $data['project_location_id'] = $project_location_id;
            session(['project_location_id' => $project_location_id]);
            // Get Estimation Cost
            $data['cost'] = Estimation::where('project_id',$id)->first()->cost;
            // Get BQ Data
            $data['records'] = Billofquantities::where('project_id',$id)->where('project_location_id',$project_location_id)->orderBy('id','ASC')->paginate(20)->onEachSide(1);
            if(Input::get('ajax') == 1)
            {
                // Load view to show result
                return view('pmis/contractor_project/schedules/list_ajax',$data);
            }
            else
            {
                // Load view to show result
                return view('pmis/contractor_project/schedules/list',$data);
            }
        }
        else
        {
          return view('access_denied');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validates = $request->validate([
            'actual_start_date'    => 'required',
            'actual_end_date'      => 'required',
        ]);
        // Language
        $lang = get_language();
        $id = $request->selectedBQ;
        $data_array = array();
        if($id)
        {
            $id = explode(',',$id);
            foreach($id as $item)
            {
                // Update Record
                $record = Billofquantities::find($item);
                $record->actual_start_date = dateProvider($request->actual_start_date,$lang);
                $record->actual_end_date   = dateProvider($request->actual_end_date,$lang);
                $record->updated_by   = userid();
                $record->save();
            }

            Session()->flash('success', __("global.success_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Water  $architectur
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//Record id
        $data['enc_id'] = $enc_id;
        // Project Location ID
        $data['project_location_id'] = session('project_location_id');
        // Get Record
        $data['record'] = Billofquantities::with('locations')->find($id);
        // Get Estimation Cost
        $data['cost'] = Estimation::where('project_id',$data['record']->project_id)->first()->cost;
        // Get plan record
        $data['plan'] = Plans::find($data['record']->project_id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load view to show result
        return view('pmis/contractor_project/schedules/view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function edit($enc_id=0)
    {
        $lang = get_language();
        $data['lang'] = $lang;
        $id = decrypt($enc_id);//Record id
        $data['enc_id'] = $enc_id;
        // Get Record
        $data['record'] = Billofquantities::find($id);
        // Project Location ID
        $data['project_location_id'] = session('project_location_id');
        // Get plan record
        $data['plan'] = Plans::find($data['record']->project_id);
        if($data['plan'])
        {
            // Get summary data
            $data['summary'] = Requests::whereId($data['plan']->request_id)->first();
        }
        // Load view to show result
        return view('pmis/contractor_project/schedules/edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Architectur  $architectur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$enc_id=0)
    {
        // Validate the request...
        $validates = $request->validate([
            'actual_start_date'    => 'required',
            'actual_end_date'      => 'required',
        ]);
        // Language
        $lang = get_language();
        $id = decrypt($enc_id);//Record id
        // Update Record
        $record = Billofquantities::find($id);
        $record->actual_start_date = dateProvider($request->actual_start_date,$lang);
        $record->actual_end_date   = dateProvider($request->actual_end_date,$lang);
        $data = $record->save();
        if($data)
        {
            Session()->flash('success', __("global.success_edit_msg"));
        }
        else
        {
            Session()->flash('fail', __("global.failed_msg"));
        }
    }


}
