<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\auth\Module;
use App\models\Requests;
use App\models\Request_attachments;
use App\models\Settings\Statics\Documents;
use App\models\Authentication\SectionSharings; 
use App\models\Authentication\HRDepartments; 
use App\Http\Requests\RequestRequest;
use App\User;
use Validator;
use Redirect;
use PhpParser\Node\Stmt\Else_;

class RequestController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($dep_id)
  {
    // Check if department id is not exist
    if($dep_id=='0' AND session('current_department')=='0')
    {
      return Redirect::route("home");
    }
    // Get department id
    $depId = decrypt($dep_id);
    $data['dep_id'] = $dep_id;
    // Check user access in this section
    if(!check_my_section(array($depId),'pmis_request'))
    {
      return view('access_denied');
    }
    // Set current section in session
    session(['current_section' => 'pmis_request']);
    // Get Language
    $lang = get_language();
    $data['lang'] = $lang;
    // Get Data
    $data['records'] = Requests::orderBy('id','desc')->where(function($query) use ($depId){
      if(doIHaveRoleInDep(encrypt($depId),'pmis_request','pmis_request_all_view')) {
        //Can view all records 
      }
      elseif(doIHaveRoleInDep(encrypt($depId),'pmis_request','pmis_request_dep_view')) {
        // Can see record of own department 
        $query->where('department',$depId);
      }
      else{ 
        // Just seet own created record 
        $query->where('created_by',userid());
      }
    })->paginate(10)->onEachSide(1);
    //Get URL segments from 
    $data['segments'] = request()->segments();
    if(Input::get('ajax') == 1)
    {
      // Load view to show result
      return view('pmis/requests/list_ajax',$data);
    }
    else
    {
      // Load view to show result
      return view('pmis/requests/list',$data);
    }
  }

  /**
   * Display a listing of the resource by notification.
   *
   * @return \Illuminate\Http\Response
   */ 
  public function requests_notification($enc_id)
  {
    $id = decrypt($enc_id);
    $lang = get_language();
    $data['lang'] = $lang;
    // Open notification from other modules
    if(Session::get('current_mod')!='pmis')
    {
      $name = 'name_'.$lang;
      session(['current_mod' => 'pmis']);
      $current_mod_name = Module::get_module_id('pmis')->$name;
      session(['current_mod_name' => $current_mod_name]);
    }
    session(['current_section' => 'current_section']);
    $data['records'] = Requests::where('id',$id)->get();
    // Load view to show result
    return view('pmis/requests/list_notification',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    $data['lang']  = $lang;
    $data['deps']  = HRDepartments::select('id','name_'.$lang.' as name')->where('unactive',1)->get();
    $data['types'] = Documents::select('id','name_'.$lang.' as name')->get();
    $data['segments'] = request()->segments();
    // Load view to show result
    return view('pmis/requests/add',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
        'doc_type'      => 'required',
        'department_id' => 'required',
        'request_date'  => 'required',
    ]);
    // Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('requests','urn');
    // Insert Record
    $record = new Requests;
    $record->urn           = $urn;
    $record->request_date  = dateProvider($request->request_date,$lang);
    $record->doc_type      = $request->doc_type;
    $record->department_id = $request->department_id;
    $record->doc_number    = $request->request_number;
    $record->goals         = $request->goals;
    $record->description   = $request->description;
    $record->created_at    = date('Y-m-d H:i:s');
    $record->created_by    = userid();
    $record->department    = departmentid();
    $record->save();
    if($record->id>0)
    {
    
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    session(['current_section' => 'pmis_request']);
    // Get language
    $lang = get_language();
    // Dycript the id
    $id = decrypt($enc_id);
    //Get Record
    $record = Requests::find($id);
    $summary = $record;
    // Check if record is shared 
    $shared = $record->share()->where('share_from_code','pmis_request')->whereNull('deleted_at')->first();
     // Get Attachments
    $attachments = getAttachments('requests_attachments',0,$id,'request');
    $section = 'request';
    //Pass Attachments table
    $table = 'requests_attachments';
    // Get Allowed Sections for Sharing
    $sections = SectionSharings::where('code','pmis_request')->get();
    // Load view to show result
    return view('pmis/requests/view',compact('lang','enc_id','record','summary','shared','attachments','section','table','sections'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Departments
    $data['deps']  = HRDepartments::select('id','name_'.$lang.' as name')->get();
    // Get Document Type
    $data['types'] = Documents::select('id','name_'.$lang.' as name')->get();
    // Get Record
    $data['record'] = Requests::find($id);
    // Load view to show result
    return view('pmis/requests/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RequestRequest $request)
  {
    $lang = get_language();
    $id = decrypt(Input::get('enc_id'));
    $req = Requests::find($id);
    $req->request_date  = dateProvider($request->request_date,$lang);
    $req->department_id = $request->department_id;
    $req->doc_type      = $request->doc_type;
    $req->doc_number    = $request->request_number;
    $req->goals         = $request->goals;
    $req->description   = $request->description;
    $req->updated_by    = userid();
    $updated = $req->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($enc_id)
  {
    // Decrypt ID
    $id = decrypt($enc_id);
    // Update record
    $record = Requests::find($id);
    $id = $record->delete();
    if($id!=0)
    {
      Session()->flash('success', __("global.success_delete_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Filter list the resource .
   *
   * @return \Illuminate\Http\Response
   */  
  public function filterRequest(Request $request)
  {
    $lang = get_language();
    $records = Requests::searchData($request->condition,$request->field,$request->value); 
    // Load view to show result
    return view('pmis/requests/list_filter',['records' => $records,'lang' => $lang,'request' => $request]);
  }

  /**
   * Store a newly attachment in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  function uploadDocuments($rec_id=0,$file_name="",$i=1)
  {
    // Getting all of the post data
    $file = Input::file('file_'.$i);
    $errors = "";
    if(Input::hasFile('file_'.$i))
    {
      // Validating each file.
      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
      $validator = Validator::make(
        [
            'file'      => $file,
            'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
            'file'      => 'required|max:500000',
            'extension' => 'required|in:jpg,JPG,jpeg,JPEG,png,PNG,doc,docx,pdf,xls,ppt,pptx,xlsx'
        ]
      );
      if($validator->passes())
      {
        // Path is pmis/public/attachments/pmis...
        $destinationPath = 'attachments/pmis/requests';
        $fileOrgName     = $file->getClientOriginalName();
        $fileExtention   = $file->getClientOriginalExtension();
        $fileOrgSize     = $file->getSize();
        if($file_name!=""){
          $name = $file_name;
          $file_name = $file_name."ـ".$rec_id.".".$fileExtention;
        }else{
          $name = explode('.', $fileOrgName)[0];
          $file_name = $fileOrgName;
        }
        $upload_success = $file->move($destinationPath, $file_name);
        if($upload_success)
        {
          //Insert Record
          $attachment = new Request_attachments;
          $attachment->rec_id     = $rec_id;
          $attachment->filename   = $name;
          $attachment->path       = $destinationPath."/".$file_name;
          $attachment->extension  = $fileExtention;
          $attachment->size       = $fileOrgSize;
          $attachment->created_at = date('Y-m-d H:i:s');
          $attachment->created_by = userid();
          $attachment->save();
          return true;
        }
        else
        {
            $errors .= json('error', 400);
        }
      }
      else
      {
        // Return false.
        return false;
      }
    }
  }

  /**
   * Approve/Reject Request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function approve_request(Request $request)
  {
    // Get language
    $lang = get_language();
    // Validate the request...
    $validates = $request->validate([
      'status' => 'required',
      'date'   => 'required',
    ]);
    // Get encrypted id
    $id = decrypt($request->id);
    // Get Request
    $data = Requests::find($id);
    //Record owner
    $reco_onwer                  = $data->created_by;
    
    // To be approved
    // Update table
    $data->operation             = $request->status;
    $data->operation_date        = dateProvider($request->date,$lang);
    $data->operation_number      = $request->number;
    $data->operation_description = $request->description;
    $data->operation_by          = userid();
    $data->operation_at          = date('Y-m-d H:i:s');
    $data->save();
    if($request->status==1)
    {
      // Start Notification
      $users        = User::whereId($reco_onwer)->get();
      $redirect_url = route('requests.show',encrypt($id));
      foreach($users as $user) {
        $user->notify(new NewProject($id, 'global.request_approved', userid(), $redirect_url));
      }
      // End Notification
      // Set msg
      Session()->flash('success', __("requests.approved_msg"));
    }
    else // To be Rejected
    {
      // Start Notification
      $users = User::whereId($reco_onwer)->get();
      $redirect_url = route('requests.show', encrypt($id));
      foreach($users as $user) {
        $user->notify(new NewProject($id, 'global.request_rejcted', userid(), $redirect_url));
      }
      // End Notification
      // Set msg
      Session()->flash('success', __("requests.rejected_msg"));
    }
  }
}
?>