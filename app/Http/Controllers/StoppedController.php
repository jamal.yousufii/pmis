<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use App\models\Requests;
use App\models\Plans;
use App\models\Plan_locations;
use App\models\Survey;
use App\models\Survey_entity_values;
use App\models\Architecture;
use App\models\Structure;
use App\models\Electricity;
use App\models\Water;
use App\models\Mechanic;
use App\models\Estimation;
use App\models\BillofquantitiesEst;
use App\models\Implement;
use App\models\Implement_attachments;
use App\models\Procurement;
use App\models\Billofquantities;
use App\models\ChangeRequestTime;
use App\models\ChangeRequestStartStop;
use App\models\ChangeRequestCostScope;
use App\models\PlanLocationStatus;
use App\models\PlanLocationStatusAttachment;
use App\models\Settings\ProjectDepartmentStaff;
use Redirect;

class StoppedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dep_id)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depId = decrypt($dep_id);
        // Check user access in this section
        if(!check_my_section(array($depId),'pmis_stopped'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_stopped"]);
        // Get language
        $lang = get_language();
        // Get records
        $records = Plans::whereHas('project_location', function($query){
            $query->where('status','2');
        })->where('department',$depId)->orderBy('id','desc')->paginate(10)->onEachSide(1);
        //Get URL Segments
        $segments = request()->segments();
        if(Input::get('ajax') == 1)
        {
            // Load view to show result
            return view('pmis.stopped.list_ajax',compact('records','lang','segments','dep_id'));
        }
        else
        {
            // Load view to show result
            return view('pmis.stopped.list',compact('records','lang','segments','dep_id'));
        }
    }

    /**
     * Filter a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter_stopped(Request $request)
    {
        // Get Language
        $lang = get_language();
        // Get department
        $dep_id = decrypt($request->department_id);
        // Get section code
        $section = $request->section;
        // Search stopped projects
        $records  = Plans::searchCompleteStopProjects($request->condition,$request->field,$request->value,$dep_id,2);
        // Load View to display records
        return view('pmis.stopped.list_filter',compact('records','lang','request','dep_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function projects($enc_id=0,$dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Get department id
        $depId = decrypt($dep_id);
        // Check user access in this section
        if(!check_my_section(array($depId),'pmis_stopped'))
        {
            return view('access_denied');
        }
        // Set current section in session
        session(['current_section' => "pmis_stopped"]);
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Get plan record
        $record = Plans::find($id);
        // Project locations
        $project_location = Plan_locations::where('project_id',$id)->where('status','2')->orderBy('id','desc')->get();
        $excluded_locations = Plan_locations::whereHas('Projects', function($query) use ($id, $depId){
            $query->where('project_id',$id);
            $query->where('department',$depId);
        })->where('status','<>','2')->orderBy('id','desc')->get();
        // Load view to show result
        return view('pmis.stopped.index',compact('record','enc_id','depId','lang','project_location','excluded_locations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_project(Request $request)
    {
        // Get langauge
        $lang = get_language();
        // Project id
        $project_id = decrypt($request->project_id);
        // Location id
        $location_id = array($request->location_id);
        // Get project record
        $location = Plan_locations::whereIn('id',$location_id)->first();
        // Get project owner
        $owner = ProjectDepartmentStaff::whereIn('location_id',$location_id)->first();
        // Get project budget from working
        $project_budget = Billofquantities::whereIn('project_location_id',$location_id)->get();  
        // Get total price of Bill of Quantities
        $total_price = $project_budget->sum('total_price');
        // Calcualte actual budget done from current fiscal year start to end
        $budget_expenditure = getBudgetDone($location_id);
        $spi = 0;
        $remained_budget = 0;
        $extra_budget = 0;
        $scope_chnages = 0;
        if($total_price && $total_price!=0){
            // Get project actual performance in field [SPI Calculation]
            $spi = round($budget_expenditure/$total_price,2); 
            // Get remained work
            $remained_budget = $total_price - $budget_expenditure;
            if($total_price < $budget_expenditure)
            {
            // Get extra work
            $extra_budget = $budget_expenditure - $total_price;
            }
            // Get Scope Changes
            $scope_chnages = ChangeRequestCostScope::where('project_id',$project_id)->whereIn('location_id',$location_id)->where('operation',1)->get()->sum('total_price');
        }
        // Get first date of planed schedule
        $getFirstDateOfProject = getFirstLastDateOfProject($location_id,'min(start_date)','project_location_id');
        // Get last date of planed schedule
        $getLastDateOfProject = getFirstLastDateOfProject($location_id,'max(end_date)','project_location_id');
        // Get project working days
        $working_days = portfolioworkDays($location_id,$getFirstDateOfProject ,$getLastDateOfProject);
        // Get requested time extention
        $time_extention_date = ChangeRequestTime::where('project_id',$project_id)->whereIn('location_id',$location_id)->where('operation',1)->get();
        $time_extention = 0;
        if($time_extention_date){
            foreach($time_extention_date as $item){
            $time_extention = $time_extention + portfolioworkDays($location_id, $item->start_date ,$item->end_date);
            }
        }
        // Get requested time extention
        $stop_extention_date = ChangeRequestStartStop::where('project_id',$project_id)->whereIn('location_id',$location_id)->where('operation',1)->get();
        $stop_extention = 0;
        if($stop_extention_date){
            foreach($stop_extention_date as $item){
            $stop_extention = $stop_extention + portfolioworkDays($location_id, $item->start_date ,$item->end_date);
            }
        }
        // Project status details
        $project_status = PlanLocationStatus::where('project_id',$project_id)->whereIn('location_id',$location_id)->where('status',2)->orderBy('id','desc')->first();
        // Project status attachment
        $project_status_attachment = PlanLocationStatusAttachment::where('project_id',$project_id)->whereIn('project_location_id',$location_id)->first();
        // Load view to show data
        return view('pmis.stopped.show',compact('location','location_id','lang','spi','remained_budget','extra_budget','total_price','budget_expenditure','owner','scope_chnages','working_days','time_extention','stop_extention','project_status','project_status_attachment'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allProjects($enc_id=0,$dep_id=0)
    {
        // Check if department id is not exist
        if($dep_id=='0' AND session('current_department')=='0')
        {
            return Redirect::route("home");
        }
        // Department id
        $depId = decrypt($dep_id);
        // Set current section in session
        session(['current_section' => "pmis_stopped"]);
        session(['current_view' => "pmis_request"]);
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Get plan record
        $project = Plans::find($id);
        // Get summary data
        $record = Requests::whereId($project->request_id)->first();
        // Get all sections
        $sections = get_module_sections_summary(session('current_mod'),'0');
        // Array of required sections
        $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement'); 
        // Project status details
        $project_status = PlanLocationStatus::where('project_id',$id)->where('status',2)->first();
        // Project status attachment
        $project_status_attachment = PlanLocationStatusAttachment::where('project_id',$id)->first();
        // Load view to show result
        return view('pmis.stopped.show_all',compact('project','record','enc_id','depId','lang','sections','custom_sections','project_status','project_status_attachment'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Section code
        $code = Input::get('code');
        // Set current section tab in session
        session(['current_view' => $code]);
        // Get all sections
        $sections = get_module_sections_summary(session('current_mod'),'0');
        // Array of required sections
        $custom_sections = array('pmis_request','pmis_plan','pmis_survey','pmis_design','pmis_estimation','pmis_implement','pmis_procurement');
        // Project status details
        $project_status = PlanLocationStatus::where('project_id',$id)->where('status',2)->first();
        if($code)
        {
            switch($code)
            {
                case 'pmis_request':
                {
                    // Get project data
                    $project = Plans::whereId($id)->first();
                    // Get current section data
                    $record = Requests::whereId($project->request_id)->first();
                    // Load view to display data
                    return view('pmis.stopped.requests.view',compact('record','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
                case 'pmis_plan':
                {
                    // Get current section data
                    $record = Plans::whereId($id)->first();
                    // Get project location
                    $locations = Plan_locations::where('project_id',$id)->orderBy('id','desc')->get();
                    // Load view to display data
                    return view('pmis.stopped.plans.view',compact('record','locations','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
                case 'pmis_survey':
                {
                    // Get current section data
                    $records = Survey::where('project_id',$id)->get();
                    // Load view to display data
                    return view('pmis.stopped.surveys.view',compact('records','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
                case 'pmis_design':
                {
                    // Load design`s tabs
                    $tabs = getTabs('pmis_design',$lang);
                    // Set current design tab in session
                    session(['current_tab' => 'tab_architecture']);
                    // Get current section data
                    $records = Architecture::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
                    // Load view to display data
                    return view('pmis.stopped.designs.view',compact('records','enc_id','lang','sections','custom_sections','project_status','tabs'));
                    break;
                }
                case 'pmis_estimation':
                {
                    // Get current section data
                    $record = Estimation::where('project_id',$id)->first();
                    // Get Bill of Quantities
                    $bill_quantities = BillofquantitiesEst::where('project_id',$id)->paginate(10)->onEachSide(1);
                    // Load view to display data
                    return view('pmis.stopped.estimations.view',compact('record','bill_quantities','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
                case 'pmis_implement':
                {
                    // Get current section data
                    $record = Implement::where('project_id',$id)->first();
                    // Get Attachments
                    $attachments = Implement_attachments::where('parent_id',$id)->get();
                    // Load view to display data
                    return view('pmis.stopped.implements.view',compact('record','attachments','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
                case 'pmis_procurement':
                {
                    // Get current section data
                    $record = Procurement::where('project_id',$id)->first();
                    // Load view to display data
                    return view('pmis.stopped.procurements.view',compact('record','enc_id','lang','sections','custom_sections','project_status'));
                    break;
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_survey($rec_id,$pro_id)
    {
        // Get language
        $lang = get_language();
        // Get project id
        $project_id = decrypt($pro_id);
        // Get project data
        $project = Plans::whereId($project_id)->first();
        // Survey id
        $id = decrypt($rec_id);
        // Get project data
        $record = Survey::whereId($id)->with('documents.documents_type')->first();
        // Get survey data
        $record = Survey::whereId($id)->with('documents.documents_type')->first();
        $employees = '';
        $aabedat = '';
        if($record)
        {
            // Get survey team members
            $getEmp = Survey::getTeamEmployeesName($id);
            if($getEmp)
            {
                foreach($getEmp as $emp)
                {
                    $employees .= $emp->first_name.' '.$emp->last_name.", ";
                }
                $employees = substr($employees,0,-2);
            }
            //Get aabedat entity values
            $aabedat = Survey_entity_values::where('survey_id',$id)->orderBy('entity_id')->get();
        }
        // Load view to show result
        return view('pmis.stopped.surveys.view_ajax',compact('pro_id','rec_id','record','project','employees','aabedat','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_architecture($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Load design tabs
        $tabs = getTabs('pmis_design',$lang);
        // Set current design tab in sessio
        session(['current_tab' => 'tab_architecture']);
        // Get Section Data
        $records = Architecture::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        // Load view to show result
        return view('pmis.stopped.designs.architecture_list',compact('records','enc_id','tabs','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_architecture($rec_id,$pro_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $project_id = decrypt($pro_id);
        // Record id
        $record_id = decrypt($rec_id);
        // Get record
        $record = Architecture::whereId($record_id)->first();
        // Get Attachments
        $attachments = getAttachments('design_attachments',$project_id,$record_id,'architecture');
        $attachment_table = "design_attachments";
        // Load view to show result
        return view('pmis.stopped.designs.architecture_view',compact('record','attachments','attachment_table','pro_id','rec_id','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_structure($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Load design tabs
        $tabs = getTabs('pmis_design',$lang);
        // Set current design tab in sessio
        session(['current_tab' => 'tab_structure']);
        // Get Section Data
        $records = Structure::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        // Load view to show result
        return view('pmis.stopped.designs.structure_list',compact('records','enc_id','tabs','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_structure($rec_id,$pro_id)
    { 
        // Get language
        $lang = get_language();
        // Project id
        $project_id = decrypt($pro_id);
        // Record id
        $record_id = decrypt($rec_id);
        // Get record
        $record = Structure::whereId($record_id)->first();
        // Get Attachments
        $attachments = getAttachments('design_attachments',$project_id,$record_id,'structure');
        $attachment_table = "design_attachments";
        // Load view to show result
        return view('pmis.stopped.designs.structure_view',compact('record','attachments','attachment_table','pro_id','rec_id','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_electricity($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Load design tabs
        $tabs = getTabs('pmis_design',$lang);
        // Set current design tab in sessio
        session(['current_tab' => 'tab_electricity']);
        // Get Section Data
        $records = Electricity::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        // Load view to show result
        return view('pmis.stopped.designs.electricity_list',compact('records','enc_id','tabs','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_electricity($rec_id,$pro_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $project_id = decrypt($pro_id);
        // Record id
        $record_id = decrypt($rec_id);
        // Get record
        $record = Electricity::whereId($record_id)->first();
        // Get Attachments
        $attachments = getAttachments('design_attachments',$project_id,$record_id,'electricity');
        $attachment_table = "design_attachments";
        // Load view to show result
        return view('pmis.stopped.designs.electricity_view',compact('record','attachments','attachment_table','pro_id','rec_id','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_water($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Load design tabs
        $tabs = getTabs('pmis_design',$lang);
        // Set current design tab in sessio
        session(['current_tab' => 'tab_water']);
        // Get Section Data
        $records = Water::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        // Load view to show result
        return view('pmis.stopped.designs.water_list',compact('records','enc_id','tabs','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_water($rec_id,$pro_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $project_id = decrypt($pro_id);
        // Record id
        $record_id = decrypt($rec_id);
        // Get record
        $record = Water::whereId($record_id)->first();
        // Get Attachments
        $attachments = getAttachments('design_attachments',$project_id,$record_id,'water');
        $attachment_table = "design_attachments";
        // Load view to show result
        return view('pmis.stopped.designs.water_view',compact('record','attachments','attachment_table','pro_id','rec_id','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tab_mechanic($enc_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $id = decrypt($enc_id);
        // Load design tabs
        $tabs = getTabs('pmis_design',$lang);
        // Set current design tab in sessio
        session(['current_tab' => 'tab_mechanic']);
        // Get Section Data
        $records = Mechanic::where('project_id',$id)->where('status','1')->whereIn('version',['first','latest'])->get();
        // Load view to show result
        return view('pmis.stopped.designs.mechanic_list',compact('records','enc_id','tabs','lang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tab_mechanic($rec_id,$pro_id)
    {
        // Get language
        $lang = get_language();
        // Project id
        $project_id = decrypt($pro_id);
        // Record id
        $record_id = decrypt($rec_id);
        // Get record
        $record = Mechanic::whereId($record_id)->first();
        // Get Attachments
        $attachments = getAttachments('design_attachments',$project_id,$record_id,'mechanic');
        $attachment_table = "design_attachments";
        // Load view to show result
        return view('pmis.stopped.designs.mechanic_view',compact('record','attachments','attachment_table','pro_id','rec_id','lang'));
    }
}
?>
