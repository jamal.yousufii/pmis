<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class DepartmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_dr'           => 'required', 
            'name_pa'           => 'required', 
            'name_en'           => 'required', 
            'code'              => 'required', 
            'module_id'         => 'required', 
            'organization_id'   => 'required',
            'icon'              => 'required', 
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name_dr.required'              => 'نام دیپارتمنت به دری ضروری میباشد.',
            'name_pa.required'              => 'نام دیپارتمنت به پشتو ضروری میباشد.',
            'name_en.required'              => 'نام دیپارتمنت به انگلیسی ضروری میباشد.',
            'module_id.required'            => 'آپلیکیشن ضروری میباشد.',
            'organization_id.required'      => 'ارگان ضروری میباشد.',
        ];
    }
}
