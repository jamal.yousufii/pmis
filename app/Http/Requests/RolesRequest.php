<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'code'        => 'required',
            'name_en'     => 'required',
            'name_dr'     => 'required',
            'name_pa'     => 'required',
            'section_id'  => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name_dr.required'      => 'نام رول به دری ضروری میباشد*',
            'name_pa.required'      => 'نام رول به پشتو ضروری میباشد*',
            'name_en.required'      => 'نام رول به انگلیسی ضروری میباشد*',
            'section_id.required'   => 'انتخاب بخش ضروری میباشد.',
            'code.required'         => 'کود رول ضروری میباشد.',
        ];
    }
}
