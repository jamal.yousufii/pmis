<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'department_id'     => 'required',
            'name'              => 'required',
            'father'            => 'required',
            'email'             => 'required|email',
            'position'          => 'required',
            'password'          => 'required|min:6|confirmed',
            'confirm_password'  => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'             => 'نام و تخلص ضروری میباشد*',
            'father.required'           => 'نام پدر ضروری میباشد*',
            'email.required'            => 'ایمیل ضروری میباشد*',
            'position.required'         => 'وظیفه ضروری میباشد*',
            'department_id.required'    => 'انتخاب دیپارتمنت ضروری میباشد*',
            'password.required'         => 'رمز ضروری میباشد*',
            'confirm_password.required' => 'تایید رمز ضروری میباشد *',
        ];
    }
}
