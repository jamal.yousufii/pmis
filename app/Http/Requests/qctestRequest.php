<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class qctestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bill_quant_id' => 'required',
            'test_number'   => 'required',
            'test_status'   => 'required',
        ];
    }
}
