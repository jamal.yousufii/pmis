<?php
    if(date('m')=='12' and date('d')>'20'){
        $syear = date('Y');
        $eyear = date('Y',strtotime("+1 year"));
    }else{
        $syear = date('Y',strtotime("-1 year"));
        $eyear = date('Y');
    }

    $start_year = date('Y');
    $end_year   = date('Y',strtotime("+1 year"));

return [
    'dr' =>
    [
        'test_result' =>
        [
            'waiting' => [
                'value' => 'waiting',
                'name'  => 'daily_report.waiting',
            ],
            'failed' => [
                'value' => 'failed',
                'name'  => 'daily_report.failed',
            ],
            'passed' => [
                'value' => 'passed',
                'name'  => 'daily_report.passed',
            ],
        ],

        'construction_steps' =>
        [
            'financial_letter' => [
                'id'        => '1',
                'name'      => 'procurement.financial_letter',
                'code'      => 'financial_letter',
                'addview'   => 'add_financial_letter'
            ],
            'bidding' => [
                'id'        => '2',
                'name'      => 'procurement.bidding',
                'code'      => 'bidding',
                'addview'   => 'add_bidding'
            ],
            'bidding_approval' => [
                'id'        => '3',
                'name'      => 'procurement.bidding_approval',
                'code'      => 'bidding_approval',
                'addview'   => 'add_bidding_approval'
            ],
            'announcement' => [
                'id'        => '4',
                'name'      => 'procurement.announcement',
                'code'      => 'announcement',
                'addview'   => 'add_announcement'
            ],
            'bids' => [
                'id'        => '5',
                'name'      => 'procurement.bids',
                'code'      => 'bids',
                'addview'   => 'add_bids'
            ],
            'estimate' => [
                'id'        => '6',
                'name'      => 'procurement.estimate',
                'code'      => 'estimate',
                'addview'   => 'add_estimate'
            ],
            'public_announce' => [
                'id'        => '7',
                'name'      => 'procurement.public_announce',
                'code'      => 'public_announce',
                'addview'   => 'add_public_announce'
            ],
            'send' => [
                'id'        => '8',
                'name'      => 'procurement.send',
                'code'      => 'send',
                'addview'   => 'add_send'
            ],
            'contract_arrangement' => [
                'id'        => '9',
                'name'      => 'procurement.contract_arrangement',
                'code'      => 'contract_arrangement',
                'addview'   => 'add_contract_arrangement'
            ],
            'contract_approval' => [
                'id'        => '10',
                'name'      => 'procurement.contract_approval',
                'code'      => 'contract_approval',
                'addview'   => 'add_contract_approval'
            ],
            'send_letter' => [
                'id'        => '11',
                'name'      => 'procurement.send_letter',
                'code'      => 'company',
                'addview'   => 'add_company'
            ],
            'controle' => [
                'id'        => '12',
                'name'      => 'procurement.controle',
                'code'      => 'controle',
                'addview'   => 'add_controle'
            ]
        ],
        'adjustment_step' =>
        [
            'send_letter' => [
                'id'        => '1',
                'name'      => 'global.adjustments',
            ]
        ],
        'province' =>
        [
            '1'=> ['name' => 'کابل'     ,'color' =>  '#37a2da' ],
            '2'=> ['name' => 'کاپیسا'   ,'color' =>  '#61a0a7' ],
            '3'=> ['name' => 'پروان'    ,'color' =>  '#69d5b8' ],
            '4'=> ['name' => 'وردک'     ,'color' =>  '#d5d269' ],
            '5'=> ['name' => 'لوگر'     ,'color' =>  '#cb99c0' ],
            '6'=> ['name' => 'ننگرهار'  ,'color' =>  '#cf7f95' ],
            '7'=> ['name' => 'لغمان'    ,'color' =>  '#7a9ecd' ],
            '8'=> ['name' => 'پنجشیر'   ,'color' =>  '#1c477f' ],
            '9'=> ['name' => 'بغلان'     ,'color' =>  '#08264b' ],
            '10'=>['name' => 'بامیان'   ,'color' =>  '#646c6c' ],
            '11'=>['name' => 'غزنی'     ,'color' =>  '#6c36b6' ],
            '12'=>['name' => 'پکتیکا'   ,'color' =>  '#2f4454' ],
            '13'=>['name' => 'پکتیا'    ,'color' =>  '#c23531' ],
            '14'=>['name' => 'خوست'     ,'color' =>  '#fb9f7e' ],
            '15'=>['name' => 'کنرها'    ,'color' =>  '#fcdb5c' ],
            '16'=>['name' => 'نورستان'  ,'color' =>  '#e7bcf2' ],
            '17'=>['name' => 'بدخشان'   ,'color' =>  '#fb9f7e' ],
            '18'=>['name' => 'تخار'     ,'color' =>  '#36c5e9' ],
            '19'=>['name' => 'کندوز'    ,'color' =>  '#91c6ad' ],
            '20'=>['name' => 'سمنگان'   ,'color' =>  '#343a40' ],
            '21'=>['name' => 'بلخ'      ,'color' =>  '#2F4F4F' ],
            '22'=>['name' => 'سرپل'     ,'color' =>  '#AFEEEE' ],
            '23'=>['name' => 'غور'      ,'color' =>  '#4169E1' ],
            '24'=>['name' => 'دایکندی'  ,'color' =>  '#D8BFD8' ],
            '25'=>['name' => 'اورزگان'  ,'color' =>  '#FFB6C1' ],
            '26'=>['name' => 'زابل'     ,'color' =>  '#F4A460' ],
            '27'=>['name' => 'کندهار'   ,'color' =>  '#B0C4DE' ],
            '28'=>['name' => 'جوزجان'   ,'color' =>  '#9370DB' ],
            '29'=>['name' => 'فاریاب'   ,'color' =>  '#20B2AA' ],
            '30'=>['name' => 'هلمند'    ,'color' =>  '#90EE90' ],
            '31'=>['name' => 'بادغیس'   ,'color' =>  '#FFA500' ],
            '32'=>['name' => 'هرات'     ,'color' =>  '#F08080' ],
            '33'=>['name' => 'فراه'     ,'color' =>  '#B22222' ],
            '34'=>['name' => 'نیمروز'   ,'color' =>  '#CD5C5C' ]
        ],
        'month' =>
        [
            "حمل",
            "ثور",
            "جوزا",
            "سرطان",
            "اسد",
            "سنبله",
            "میزان",
            "عقرب",
            "قوس",
            "جدی",
            "دلو",
            "حوت"
        ],
        'days_of_the_week' =>
        [
            "يكشنبه",
            "دوشنبه",
            "سه شنبه",
            "چهارشنبه",
            "پنج شنبه",
            "جمعه",
            "شنبه"
        ],
        'portfolio_month' =>
        [
            "01" => "حمل",
            "02" => "ثور",
            "03" => "جوزا",
            "04" => "سرطان",
            "05" => "اسد",
            "06" => "سنبله",
            "07" => "میزان",
            "08" => "عقرب",
            "09" => "قوس",
            "10" => "جدی",
            "11" => "دلو",
            "12" => "حوت",
        ],
        'fiscal_year_month' =>
        [
            "01" => "جدی",
            "02" => "دلو",
            "03" => "حوت",
            "04" => "حمل",
            "05" => "ثور",
            "06" => "جوزا",
            "07" => "سرطان",
            "08" => "اسد",
            "09" => "سنبله",
            "10" => "میزان",
            "11" => "عقرب",
            "12" => "قوس",
        ],
    ],
    'pa' =>
    [
        'test_result' =>
        [
            'waiting' => [
                'value' => 'waiting',
                'name'  => 'daily_report.waiting',
            ],
            'failed' => [
                'value' => 'failed',
                'name'  => 'daily_report.failed',
            ],
            'passed' => [
                'value' => 'passed',
                'name'  => 'daily_report.passed',
            ],
        ],

        'construction_steps' =>
        [
            'financial_letter' => [
                'id'        => '1',
                'name'      => 'procurement.financial_letter',
                'code'      => 'financial_letter',
                'addview'   => 'add_financial_letter'
            ],
            'bidding' => [
                'id'        => '2',
                'name'      => 'procurement.bidding',
                'code'      => 'bidding',
                'addview'   => 'add_bidding'
            ],
            'bidding_approval' => [
                'id'        => '3',
                'name'      => 'procurement.bidding_approval',
                'code'      => 'bidding_approval',
                'addview'   => 'add_bidding_approval'
            ],
            'announcement' => [
                'id'        => '4',
                'name'      => 'procurement.announcement',
                'code'      => 'announcement',
                'addview'   => 'add_announcement'
            ],
            'bids' => [
                'id'        => '5',
                'name'      => 'procurement.bids',
                'code'      => 'bids',
                'addview'   => 'add_bids'
            ],
            'estimate' => [
                'id'        => '6',
                'name'      => 'procurement.estimate',
                'code'      => 'estimate',
                'addview'   => 'add_estimate'
            ],
            'public_announce' => [
                'id'        => '7',
                'name'      => 'procurement.public_announce',
                'code'      => 'public_announce',
                'addview'   => 'add_public_announce'
            ],
            'send' => [
                'id'        => '8',
                'name'      => 'procurement.send',
                'code'      => 'send',
                'addview'   => 'add_send'
            ],
            'contract_arrangement' => [
                'id'        => '9',
                'name'      => 'procurement.contract_arrangement',
                'code'      => 'contract_arrangement',
                'addview'   => 'add_contract_arrangement'
            ],
            'contract_approval' => [
                'id'        => '10',
                'name'      => 'procurement.contract_approval',
                'code'      => 'contract_approval',
                'addview'   => 'add_contract_approval'
            ],
            'send_letter' => [
                'id'        => '11',
                'name'      => 'procurement.send_letter',
                'code'      => 'company',
                'addview'   => 'add_company'
            ],
            'controle' => [
                'id'        => '12',
                'name'      => 'procurement.controle',
                'code'      => 'controle',
                'addview'   => 'add_controle'
            ]
        ],
        'adjustment_step' =>
        [
            'send_letter' => [
                'id'        => '1',
                'name'      => 'global.adjustments',
            ]
        ],
        'province' =>
        [
            '1'=> ['name' => 'کابل'     ,'color' =>  '#37a2da' ],
            '2'=> ['name' => 'کاپیسا'   ,'color' =>  '#61a0a7' ],
            '3'=> ['name' => 'پروان'    ,'color' =>  '#69d5b8' ],
            '4'=> ['name' => 'وردک'     ,'color' =>  '#d5d269' ],
            '5'=> ['name' => 'لوگر'     ,'color' =>  '#cb99c0' ],
            '6'=> ['name' => 'ننگرهار'  ,'color' =>  '#cf7f95' ],
            '7'=> ['name' => 'لغمان'    ,'color' =>  '#7a9ecd' ],
            '8'=> ['name' => 'پنجشیر'   ,'color' =>  '#1c477f' ],
            '9'=> ['name' => 'بغلان'     ,'color' =>  '#08264b' ],
            '10'=>['name' => 'بامیان'   ,'color' =>  '#646c6c' ],
            '11'=>['name' => 'غزنی'     ,'color' =>  '#6c36b6' ],
            '12'=>['name' => 'پکتیکا'   ,'color' =>  '#2f4454' ],
            '13'=>['name' => 'پکتیا'    ,'color' =>  '#c23531' ],
            '14'=>['name' => 'خوست'     ,'color' =>  '#fb9f7e' ],
            '15'=>['name' => 'کنرها'    ,'color' =>  '#fcdb5c' ],
            '16'=>['name' => 'نورستان'  ,'color' =>  '#e7bcf2' ],
            '17'=>['name' => 'بدخشان'   ,'color' =>  '#fb9f7e' ],
            '18'=>['name' => 'تخار'     ,'color' =>  '#36c5e9' ],
            '19'=>['name' => 'کندوز'    ,'color' =>  '#91c6ad' ],
            '20'=>['name' => 'سمنگان'   ,'color' =>  '#343a40' ],
            '21'=>['name' => 'بلخ'      ,'color' =>  '#2F4F4F' ],
            '22'=>['name' => 'سرپل'     ,'color' =>  '#AFEEEE' ],
            '23'=>['name' => 'غور'      ,'color' =>  '#4169E1' ],
            '24'=>['name' => 'دایکندی'  ,'color' =>  '#D8BFD8' ],
            '25'=>['name' => 'اورزگان'  ,'color' =>  '#FFB6C1' ],
            '26'=>['name' => 'زابل'     ,'color' =>  '#F4A460' ],
            '27'=>['name' => 'کندهار'   ,'color' =>  '#B0C4DE' ],
            '28'=>['name' => 'جوزجان'   ,'color' =>  '#9370DB' ],
            '29'=>['name' => 'فاریاب'   ,'color' =>  '#20B2AA' ],
            '30'=>['name' => 'هلمند'    ,'color' =>  '#90EE90' ],
            '31'=>['name' => 'بادغیس'   ,'color' =>  '#FFA500' ],
            '32'=>['name' => 'هرات'     ,'color' =>  '#F08080' ],
            '33'=>['name' => 'فراه'     ,'color' =>  '#B22222' ],
            '34'=>['name' => 'نیمروز'   ,'color' =>  '#CD5C5C' ]
        ],
        'month' =>
        [
            "حمل",
            "ثور",
            "جوزا",
            "سرطان",
            "اسد",
            "سنبله",
            "میزان",
            "عقرب",
            "قوس",
            "جدی",
            "دلو",
            "حوت"
        ],
        'fiscal_year_month' =>
        [
            "01" => "جدی",
            "02" => "دلو",
            "03" => "حوت",
            "04" => "حمل",
            "05" => "ثور",
            "06" => "جوزا",
            "07" => "سرطان",
            "08" => "اسد",
            "09" => "سنبله",
            "10" => "میزان",
            "11" => "عقرب",
            "12" => "قوس",
        ],
        'days_of_the_week' =>
        [
            "يكشنبه",
            "دوشنبه",
            "سه شنبه",
            "چهارشنبه",
            "پنج شنبه",
            "جمعه",
            "شنبه"
        ],
        'portfolio_month' =>
        [

            "01" => "جدی",
            "02" => "دلو",
            "03" => "حوت",
            "04" => "حمل",
            "05" => "ثور",
            "06" => "جوزا",
            "07" => "سرطان",
            "08" => "اسد",
            "09" => "سنبله",
            "10" => "میزان",
            "11" => "عقرب",
            "12" => "قوس",
        ],
    ],
    'en' =>
    [
        'test_result' =>
        [
            'waiting' => [
                'value' => 'waiting',
                'name'  => 'daily_report.waiting',
            ],
            'failed' => [
                'value' => 'failed',
                'name'  => 'daily_report.failed',
            ],
            'passed' => [
                'value' => 'passed',
                'name'  => 'daily_report.passed',
            ],
        ],

        'construction_steps' =>
        [
            'financial_letter' => [
                'id'        => '1',
                'name'      => 'procurement.financial_letter',
                'code'      => 'financial_letter',
                'addview'   => 'add_financial_letter'
            ],
            'bidding' => [
                'id'        => '2',
                'name'      => 'procurement.bidding',
                'code'      => 'bidding',
                'addview'   => 'add_bidding'
            ],
            'bidding_approval' => [
                'id'        => '3',
                'name'      => 'procurement.bidding_approval',
                'code'      => 'bidding_approval',
                'addview'   => 'add_bidding_approval'
            ],
            'announcement' => [
                'id'        => '4',
                'name'      => 'procurement.announcement',
                'code'      => 'announcement',
                'addview'   => 'add_announcement'
            ],
            'bids' => [
                'id'        => '5',
                'name'      => 'procurement.bids',
                'code'      => 'bids',
                'addview'   => 'add_bids'
            ],
            'estimate' => [
                'id'        => '6',
                'name'      => 'procurement.estimate',
                'code'      => 'estimate',
                'addview'   => 'add_estimate'
            ],
            'public_announce' => [
                'id'        => '7',
                'name'      => 'procurement.public_announce',
                'code'      => 'public_announce',
                'addview'   => 'add_public_announce'
            ],
            'send' => [
                'id'        => '8',
                'name'      => 'procurement.send',
                'code'      => 'send',
                'addview'   => 'add_send'
            ],
            'contract_arrangement' => [
                'id'        => '9',
                'name'      => 'procurement.contract_arrangement',
                'code'      => 'contract_arrangement',
                'addview'   => 'add_contract_arrangement'
            ],
            'contract_approval' => [
                'id'        => '10',
                'name'      => 'procurement.contract_approval',
                'code'      => 'contract_approval',
                'addview'   => 'add_contract_approval'
            ],
            'send_letter' => [
                'id'        => '11',
                'name'      => 'procurement.send_letter',
                'code'      => 'company',
                'addview'   => 'add_company'
            ],
            'controle' => [
                'id'        => '12',
                'name'      => 'procurement.controle',
                'code'      => 'controle',
                'addview'   => 'add_controle'
            ]
        ],
        'adjustment_step' =>
        [
            'send_letter' => [
                'id'        => '1',
                'name'      => 'global.adjustments',
            ]
        ],
        'province' =>
        [
            '1'=> ['name' =>  'KABUL'     ,'color' =>  '#37a2da' ],
            '2'=> ['name' =>  'KAPISA'    ,'color' =>  '#61a0a7' ],
            '3'=> ['name' =>  'PARWAN'    ,'color' =>  '#69d5b8' ],
            '4'=> ['name' =>  'WARDAK'    ,'color' =>  '#d5d269' ],
            '5'=> ['name' =>  'LOGAR'     ,'color' =>  '#cb99c0' ],
            '6'=> ['name' =>  'NANGARHAR' ,'color' =>  '#cf7f95' ],
            '7'=> ['name' =>  'LAGHMAN'   ,'color' =>  '#7a9ecd' ],
            '8'=> ['name' =>  'PANJSHER'  ,'color' =>  '#1c477f' ],
            '9'=> ['name' =>  'BAGHLAN'   ,'color' =>  '#08264b' ],
            '10'=>['name' => 'BAMYAN'    ,'color' =>   '#646c6c' ],
            '11'=>['name' => 'GHAZNI'    ,'color' =>   '#6c36b6' ],
            '12'=>['name' => 'PAKTIKA'   ,'color' =>   '#2f4454' ],
            '13'=>['name' => 'PAKTYA'    ,'color' =>   '#c23531' ],
            '14'=>['name' => 'KHOST'     ,'color' =>   '#fb9f7e' ],
            '15'=>['name' => 'KUNARHA'   ,'color' =>   '#fcdb5c' ],
            '16'=>['name' => 'NOORISTAN' ,'color' =>   '#e7bcf2' ],
            '17'=>['name' => 'BADAKHSHAN','color' =>   '#fb9f7e' ],
            '18'=>['name' => 'TAKHAR'    ,'color' =>   '#36c5e9' ],
            '19'=>['name' => 'KUNDUZ'    ,'color' =>   '#91c6ad' ],
            '20'=>['name' => 'SAMANGAN'  ,'color' =>   '#343a40' ],
            '21'=>['name' => 'BALKH'     ,'color' =>   '#2F4F4F' ],
            '22'=>['name' => 'SAR-E-PUL' ,'color' =>   '#AFEEEE' ],
            '23'=>['name' => 'GHOR'      ,'color' =>   '#4169E1' ],
            '24'=>['name' => 'DAYKUND'   ,'color' =>   '#D8BFD8' ],
            '25'=>['name' => 'UROZGAN'   ,'color' =>   '#FFB6C1' ],
            '26'=>['name' => 'ZABUL'     ,'color' =>   '#F4A460' ],
            '27'=>['name' => 'KANDAHAR'  ,'color' =>   '#B0C4DE' ],
            '28'=>['name' => 'JAWZJAN'   ,'color' =>   '#9370DB' ],
            '29'=>['name' => 'FARYAB'    ,'color' =>   '#20B2AA' ],
            '30'=>['name' => 'HELMAND'   ,'color' =>   '#90EE90' ],
            '31'=>['name' => 'BADGHIS'   ,'color' =>   '#FFA500' ],
            '32'=>['name' => 'HERAT'     ,'color' =>   '#F08080' ],
            '33'=>['name' => 'FARAH'     ,'color' =>   '#B22222' ],
            '34'=>['name' => 'NIMROZ'    ,'color' =>   '#CD5C5C' ]
        ],
        'month' =>
        [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ],
        'days_of_the_week' =>
        [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
        ],
        'portfolio_month' =>
            [
                "01" => "Jan",
                "02" => "Feb",
                "03" => "Mar",
                "04" => "Apr",
                "05" => "May",
                "06" => "Jun",
                "07" => "Jul",
                "08" => "Aug",
                "09" => "Sep",
                "10" => "Oct",
                "11" => "Nov",
                "12" => "Dec"
            ],
        'fiscal_year_month' =>
            [
                "12" => "Dec",
                "01" => "Jan",
                "02" => "Feb",
                "03" => "Mar",
                "04" => "Apr",
                "05" => "May",
                "06" => "Jun",
                "07" => "Jul",
                "08" => "Aug",
                "09" => "Sep",
                "10" => "Oct",
                "11" => "Nov",
                
            ],
    ],
    'location' =>//set default value for each province
    [
        "1" => '0',
        "2" => '0',
        "3" => '0',
        "4" => '0',
        "5" => '0',
        "6" => '0',
        "7" => '0',
        "8" => '0',
        "9" => '0',
        "10" => '0',
        "11" => '0',
        "12" => '0',
        "13" => '0',
        "14" => '0',
        "15" => '0',
        "16" => '0',
        "17" => '0',
        "18" => '0',
        "19" => '0',
        "20" => '0',
        "21" => '0',
        "22" => '0',
        "23" => '0',
        "24" => '0',
        "25" => '0',
        "26" => '0',
        "27" => '0',
        "28" => '0',
        "29" => '0',
        "30" => '0',
        "31" => '0',
        "32" => '0',
        "33" => '0',
        "34" => '0',
    ],
    'units_result' =>
    [
        
        'm2' => [
            'value' => 'm2',
            'id'    => '1'
        ],
        'm3' => [
            'value' => 'm3',
            'id'    => '2'
        ],
        'm' => [
            'value' => 'm',
            'id'    => '3'
        ],
        'MeterLength' => [
            'value' => 'MeterLength',
            'id'    => '4'
        ],
        'Kg' => [
            'value' => 'Kg',
            'id'    => '5'
        ],
        'Set' => [
            'value' => 'Set',
            'id'    => '6'
        ],
        'Piece' => [
            'value' => 'Piece',
            'id'    => '7'
        ],
        'Job' => [
            'value' => 'Job',
            'id'    => '8'
        ],
        'Box' => [
            'value' => 'Box',
            'id'    => '9'
        ],
        'Number' => [
            'value' => 'Number',
            'id'    => '10'
        ],
        'Pair' => [
            'value' => 'Pair',
            'id'    => '11'
        ],
        'Ton' => [
            'value' => 'Ton',
            'id'    => '12'
        ],
        'Each' => [
            'value' => 'Each',
            'id'    => '13'
        ],
        'Asla' => [
            'value' => 'Asla',
            'id'    => '14'
        ],
        'Qalib' => [
            'value' => 'Qalib',
            'id'    => '15'
        ],
        'Bab' => [
            'value' => 'Bab',
            'id'    => '16'
        ],
        'Paya' => [
            'value' => 'Paya',
            'id'    => '17'
        ],
        'Halqa' => [
            'value' => 'Halqa',
            'id'    => '18'
        ],
        'Khada' => [
            'value' => 'Khada',
            'id'    => '19'
        ],
        'Rm' => [
            'value' => 'Rm',
            'id'    => '20'
        ],
        'Lump sum' => [
            'value' => 'Lump sum',
            'id'    => '21'
        ],
        'Nos' => [
            'value' => 'Nos',
            'id'    => '22'
        ],
        'Mt' => [
            'value' => 'Mt',
            'id'    => '23'
        ],
        'Lot' => [
            'value' => 'Lot',
            'id'    => '24'
        ],
        'Hr' => [
            'value' => 'Hr',
            'id'    => '25'
        ],
        'Bag' => [
            'value' => 'Bag',
            'id'    => '26'
        ],
        'M.ton' => [
            'value' => 'M.ton',
            'id'    => '27'
        ],
        
    ],
    'province_latlon' =>//set latitude and longitude for each province
    [
        "1" => "'34.5184042259092','69.201296853017'",
        "2" => '0',
        "3" => '0',
        "4" => '0',
        "5" => '0',
        "6" => '0',
        "7" => '0',
        "8" => '0',
        "9" => '0',
        "10" => '0',
        "11" => '0',
        "12" => '0',
        "13" => '0',
        "14" => '0',
        "15" => '0',
        "16" => '0',
        "17" => '0',
        "18" => '0',
        "19" => '0',
        "20" => '0',
        "21" => '0',
        "22" => '0',
        "23" => '0',
        "24" => '0',
        "25" => '0',
        "26" => '0',
        "27" => '0',
        "28" => '0',
        "29" => '0',
        "30" => '0',
        "31" => '0',
        "32" => '0',
        "33" => '0',
        "34" => '0',
    ],
    'inv_rcv' => [
        'send' => [1,3],
        '2'    => '50',
        '3'    => '50',
        '4'    => '100', 
        '5'    => '100', 
        '6'    => '100' 
    ],
    // Set default value for months of current fiscal year
    'fiscal_year_months' => [
        '01' => [
            'start_date'  => $syear.'-12-21',
            'end_date'    => $eyear.'-01-19',
            'name'        => 'جدی',
        ],
        '02' => [
            'start_date'  => $eyear.'-01-20',
            'end_date'    => $eyear.'-02-19',
            'name'        => 'دلو',
        ],
        '03' => [
            'start_date'  => $eyear.'-02-20',
            'end_date'    => $eyear.'-03-19',
            'name'        => 'حوت',
        ],
        '04' => [
            'start_date'  => $eyear.'-03-20',
            'end_date'    => $eyear.'-04-19',
            'name'        => 'حمل',
        ],
        '05' => [
            'start_date'  => $eyear.'-04-20',
            'end_date'    => $eyear.'-05-20',
            'name'        => 'ثور',
        ],
        '06' => [
            'start_date'  => $eyear.'-05-21',
            'end_date'    => $eyear.'-06-20',
            'name'        => 'جوزا',
        ],
        '07' => [
            'start_date'  => $eyear.'-06-21',
            'end_date'    => $eyear.'-07-21',
            'name'        => 'سرطان',
        ],
        '08' => [
            'start_date'  => $eyear.'-07-22',
            'end_date'    => $eyear.'-08-21',
            'name'        => 'اسد',
        ],
        '09' => [
            'start_date'  => $eyear.'-08-22',
            'end_date'    => $eyear.'-09-21',
            'name'        => 'سنبله',
        ],
        '10' => [
            'start_date'  => $eyear.'-09-22',
            'end_date'    => $eyear.'-10-21',
            'name'        => 'میزان',
        ],
        '11' => [
            'start_date'  => $eyear.'-10-22',
            'end_date'    => $eyear.'-11-20',
            'name'        => 'عقرب',
        ],
        '12' => [
            'start_date'  => $eyear.'-11-21',
            'end_date'    => $eyear.'-12-20',
            'name'        => 'قوس',
        ],
    ],
    // Set default value for months of current year 
    'current_year_months' => [
       
       '01' => [
            'start_date'  => $start_year.'-03-20',
            'end_date'    => $start_year.'-04-19',
            'name'        => 'حمل',
        ],
        '02' => [
            'start_date'  => $start_year.'-04-20',
            'end_date'    => $start_year.'-05-20',
            'name'        => 'ثور',
        ],
        '03' => [
            'start_date'  => $start_year.'-05-21',
            'end_date'    => $start_year.'-06-20',
            'name'        => 'جوزا',
        ],
        '04' => [
            'start_date'  => $start_year.'-06-21',
            'end_date'    => $start_year.'-07-21',
            'name'        => 'سرطان',
        ],
        '05' => [
            'start_date'  => $start_year.'-07-22',
            'end_date'    => $start_year.'-08-21',
            'name'        => 'اسد',
        ],
        '06' => [
            'start_date'  => $start_year.'-08-22',
            'end_date'    => $start_year.'-09-21',
            'name'        => 'سنبله',
        ],
        '07' => [
            'start_date'  => $start_year.'-09-22',
            'end_date'    => $start_year.'-10-21',
            'name'        => 'میزان',
        ],
        '08' => [
            'start_date'  => $start_year.'-10-22',
            'end_date'    => $start_year.'-11-20',
            'name'        => 'عقرب',
        ],
        '09' => [
            'start_date'  => $start_year.'-11-21',
            'end_date'    => $start_year.'-12-20',
            'name'        => 'قوس',
        ],
         '10' => [
            'start_date'  => $start_year.'-12-21',
            'end_date'    => $end_year.'-01-19',
            'name'        => 'جدی',
        ],
        '11' => [
            'start_date'  => $end_year.'-01-20',
            'end_date'    => $end_year.'-02-19',
            'name'        => 'دلو',
        ],
        '12' => [
            'start_date'  => $end_year.'-02-20',
            'end_date'    => $end_year.'-03-19',
            'name'        => 'حوت',
        ],
    ],

    'zone' =>
    [
        '01'=> ['color' =>  '#4eb5e2' ],
        '02'=> ['color' =>  '#fd7e14' ],
        '03'=> ['color' =>  '#0442f1' ],
        '04'=> ['color' =>  '#2f4454' ],
        '05'=> ['color' =>  '#a6f1a7' ],
        '06'=> ['color' =>  '#0ddeb4' ],
        '07'=> ['color' =>  '#cd5c5a' ],
        '08'=> ['color' =>  '#bff1f1' ],
        '09'=> ['color' =>  '#f9bc59' ],
        '10'=> ['color' =>  '#111' ],
    ],
    'sectionColor' =>
    [
        'pmis_plan'         => '#00c5dc',
        'pmis_survey'       => '#5867dd',
        'pmis_design'       => '#ffc107',
        'pmis_estimation'   => '#34bfa3',
        'pmis_procurement'  => '#36a3f7',
        'pmis_request'      => '#9816f4',
        'pmis_implement'    => '#c26992',
        'pmis_progress'     => '#0f0c33',
    ],
];

?>
