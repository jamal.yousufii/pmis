<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sub menu route 
    |--------------------------------------------------------------------------
    |
    */

    'pmis_payments' => [
        'payment_request.index', 
        'payment_request.show',
        'payment_request.edit'
    ]

];

