<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/project/type','NPmis\Api\DashboardController@getProjectsDashboardData');
Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

// all the route inside it will be protect for not logged in users
Route::middleware(['auth'])->group(function () {
/*** Home ***/
Route::get('/home/{mod?}', 'HomeController@index')->name('home');
Route::get('/bringSections/{id}', 'HomeController@bringSections')->name('bringSections');
Route::get('/bringContractors/{id}', 'HomeController@bringContractors')->name('bringContractors');
Route::get('/bringContractorSections/{id}/{con_id}', 'HomeController@bringContractorSections')->name('bringContractorSections');
Route::post('/deleteFile', 'HomeController@deleteFile')->name('deleteFile');
Route::get('/DownloadDoc/{id},{table},{folder}', 'HomeController@DownloadDoc')->name('DownloadDoc');
Route::post('/bringTypes', 'HomeController@bringProjectTypes')->name('bringTypes');
Route::post('/bringDistricts', 'HomeController@bringDistricts')->name('bringDistricts');
Route::post('/bringVillages', 'HomeController@bringVillages')->name('bringVillages');
Route::post('/bringUsers', 'HomeController@bringUsers')->name('bringUsers');
Route::post('/completeRecords', 'HomeController@completeRecords')->name('completeRecords');
Route::get('/deleteUpdatedFile/{id}/{table}', 'HomeController@deleteUpdatedFile')->name('deleteUpdatedFile');
Route::get('/destroyAttachments/{id}/{table}', 'HomeController@destroyAttachments')->name('destroyAttachments');
Route::post('dashboardFilter', 'HomeController@dashboardFilter')->name('dashboardFilter');
Route::post('project_status/', 'HomeController@projectStatus')->name('project_status');
Route::post('projectStatusAll/', 'HomeController@projectStatusAll')->name('projectStatusAll');
Route::get('dashboard', 'HomeController@dashboard')->name('home.dashboard');
Route::post('dashboard/bringProvinces', 'HomeController@bringProvinces')->name('home.bringProvinces');
Route::post('dashboard/projectsByZone', 'HomeController@projectsByZone')->name('home.projectsByZone');
Route::post('dashboard/dashboardByFilter', 'HomeController@dashboardByFilter')->name('home.dashboardByFilter');


/*** Attachments ***/
Route::post('/bringMoreAttachments', 'HomeController@bringMoreAttachments')->name('bringMoreAttachments');
Route::get('/attachments_list/{id},{table}', 'HomeController@attachments_list')->name('attachments_list');
Route::post('/store_attachments', 'HomeController@store_attachments')->name('store_attachments');
Route::get('/DownloadAttachments/{id},{table}', 'HomeController@DownloadAttachments')->name('DownloadAttachments');
/*** Localization Routes ***/
Route::get('/language/{locale?}', 'Lang\LanguageController@changeLanguage')->name('language');

/*** Requests ***/
Route::resource('requests','RequestController');
Route::get('/request/{id?}', 'RequestController@index')->name('request');
Route::post('/filterRequest', 'RequestController@filterRequest')->name('filterRequest');
Route::post('/approve_request', 'RequestController@approve_request')->name('approve_request');
Route::get('/requests_notification/{id?}', 'RequestController@requests_notification')->name('requests_notification');

/*** Projects ***/
Route::resource('plans','PlanController');
Route::get('/plan/{id?}', 'PlanController@index')->name('plan');
Route::post('/filterPlan', 'PlanController@filterPlan')->name('filterPlan');
Route::get('/plan_notification/{id?}', 'PlanController@plan_notification')->name('plan_notification');
Route::post('/store_location', 'PlanController@store_location')->name('store_location');
Route::get('/show_ajax', 'PlanController@show_ajax')->name('show_ajax');
Route::post('/update_location', 'PlanController@update_location')->name('update_location');
Route::post('/plan/assign_users', 'PlanController@assign_users')->name('plans.assign_users');
Route::post('/plan/list_assigned_users/{id?}', 'PlanController@list_assigned_users')->name('plans.list_assigned_users');

/*** Surveys ***/
Route::resource('surveys','SurveyController');
Route::get('/survey/{id?}', 'SurveyController@index')->name('survey');
Route::get('/list_survey/{id?}', 'SurveyController@list')->name('list_survey');
Route::post('/survey_update', 'SurveyController@update_data')->name('survey_update');
Route::post('/more_measurement', 'SurveyController@more_measurement')->name('more_measurement');
Route::post('/more_beneficairy', 'SurveyController@more_beneficairy')->name('more_beneficairy');
Route::post('/more_document', 'SurveyController@more_document')->name('more_document');
Route::post('/filter_survey', 'SurveyController@filter_survey')->name('filter_survey');
Route::get('/SurveyEntities', 'SurveyController@SurveyEntities')->name('SurveyEntities');
Route::get('/storeSurveyEntities', 'SurveyController@storeSurveyEntities')->name('storeSurveyEntities');
Route::get('/survey_notification/{id?}', 'SurveyController@survey_notification')->name('survey_notification');
Route::post('/survey/assign_users', 'SurveyController@assign_users')->name('survey.assign_users');
Route::post('/survey/list_assigned_users/{id?}', 'SurveyController@list_assigned_users')->name('survey.list_assigned_users');
Route::get('/survey_print/{pro_id}/{rec_id}', 'SurveyController@survey_print')->name('survey_print');

/*** Designs ***/
Route::resource('designs','DesignController')->except(['index']);
Route::get('/design/{id?}', 'DesignController@index')->name('design');
Route::post('/filter_design', 'DesignController@filter_design')->name('filter_design');
Route::post('/design/assign_users', 'DesignController@assign_users')->name('design.assign_users');
Route::post('/design/list_assigned_users/{id?}', 'DesignController@list_assigned_users')->name('design.list_assigned_users');
Route::get('/design_notification/{id?}', 'DesignController@design_notification')->name('design_notification');
Route::get('/design_print/{id?}', 'DesignController@design_print')->name('design_print');

/*** Designs->Architecture ***/
Route::resource('architecture','ArchitectureController')->except(['create','edit','update']);
Route::post('/architecture/create','ArchitectureController@create')->name('architecture.create');
Route::post('/architecture/approve', 'ArchitectureController@approveArchitecture')->name('architecture.approve');
Route::post('/architecture/do/approve','ArchitectureController@doApproveArchitecture')->name('architecture.doapprove');
Route::post('/architecture/view','ArchitectureController@view')->name('architecture.view');
Route::post('/architecture/edit','ArchitectureController@edit')->name('architecture.edit');
Route::post('/architecture/update','ArchitectureController@update')->name('architecture.update');
Route::post('/architecture/version','ArchitectureController@showVersion')->name('architecure.version');

/*** Designs->Structure ***/
Route::resource('structure','StructureController')->except(['create','edit','update']);
Route::post('/structure/create','StructureController@create')->name('structure.create');
Route::post('/structure/approve', 'StructureController@approveStructure')->name('structure.approve');
Route::post('/structure/do/approve','StructureController@doApproveStructure')->name('structure.doapprove');
Route::post('/structure/view','StructureController@view')->name('structure.view');
Route::post('/structure/edit','StructureController@edit')->name('structure.edit');
Route::post('/structure/update','StructureController@update')->name('structure.update');
Route::post('/structure/version','StructureController@showVersion')->name('structure.version');

/*** Designs->Electricity ***/
Route::resource('electricity','ElectricityController')->except(['create','edit','update']);
Route::post('/electricity/create','ElectricityController@create')->name('electricity.create');
Route::post('/electricity/approve', 'ElectricityController@approveElectricity')->name('electricity.approve');
Route::post('/electricity/do/approve','ElectricityController@doApproveElectricity')->name('electricity.doapprove');
Route::post('/electricity/view','ElectricityController@view')->name('electricity.view');
Route::post('/electricity/edit','ElectricityController@edit')->name('electricity.edit');
Route::post('/electricity/update','ElectricityController@update')->name('electricity.update');
Route::post('/electricity/version','ElectricityController@showVersion')->name('electricity.version');

/*** Designs->Water ***/
Route::resource('water','WaterController')->except(['create','edit','update']);
Route::post('/water/create','WaterController@create')->name('water.create');
Route::post('/water/approve', 'WaterController@approveWater')->name('water.approve');
Route::post('/water/do/approve','WaterController@doApproveWater')->name('water.doapprove');
Route::post('/water/view','WaterController@view')->name('water.view');
Route::post('/water/edit','WaterController@edit')->name('water.edit');
Route::post('/water/update','WaterController@update')->name('water.update');
Route::post('/water/version','WaterController@showVersion')->name('water.version');

/*** Designs->Mechanic ***/
Route::resource('mechanic','MechanicController')->except(['create','edit','update']);
Route::post('/mechanic/create','MechanicController@create')->name('mechanic.create');
Route::post('/mechanic/approve', 'MechanicController@approveMechanic')->name('mechanic.approve');
Route::post('/mechanic/do/approve','MechanicController@doApproveMechanic')->name('mechanic.doapprove');
Route::post('/mechanic/view','MechanicController@view')->name('mechanic.view');
Route::post('/mechanic/edit','MechanicController@edit')->name('mechanic.edit');
Route::post('/mechanic/update','MechanicController@update')->name('mechanic.update');
Route::post('/mechanic/version','MechanicController@showVersion')->name('mechanic.version');

/*** Designs->Visa ***/
Route::resource('visa','VisaController');

/*** Estimations ***/
Route::resource('estimations','EstimationController')->except(['index']);
Route::get('/estimation/{id?}', 'EstimationController@index')->name('estimation');
Route::post('/filter_estimation/{id?}', 'EstimationController@filter_estimation')->name('filter_estimation');
Route::post('/store_bill', 'EstimationController@store_bill')->name('store_bill');
Route::post('/update_bill', 'EstimationController@update_bill')->name('update_bill');
Route::get('/estimation/billquantity/list/{id}/{loc_id}', 'EstimationController@locationBillQuantityList')->name('location_bill_quantity.list');
Route::post('importExcel', 'EstimationController@importExcel')->name('importExcel');
Route::get('printExport/{id}/{loc_id}', 'EstimationController@printExport')->name('printExport');
Route::post('/estimation/assign_users', 'EstimationController@assign_users')->name('estimation.assign_users');
Route::post('/estimation/list_assigned_users/{id?}', 'EstimationController@list_assigned_users')->name('estimation.list_assigned_users');

/*** Implementing Plan ***/
Route::resource('implements','ImplementsController')->except(['index','update']);
Route::get('/implement/{id?}', 'ImplementsController@index')->name('implement');
Route::post('/more_attachment', 'ImplementsController@more_attachment')->name('more_attachment');
Route::post('/implements/update', 'ImplementsController@update')->name('implements/update');
Route::post('/approve_implement', 'ImplementsController@approve_implement')->name('approve_implement');
Route::post('/filter_implement', 'ImplementsController@filter_implement')->name('filter_implement');
Route::post('/implement/assign_users', 'ImplementsController@assign_users')->name('implement.assign_users');
Route::post('/implement/list_assigned_users/{id?}', 'ImplementsController@list_assigned_users')->name('implement.list_assigned_users');

/*** Procurements ***/
Route::get('/procurement/{id?}', 'ProcurementController@index')->name('procurement');
Route::post('/filter_procurement', 'ProcurementController@filter_procurement')->name('filter_procurement');
Route::post('/procurement/assign_users', 'ProcurementController@assign_users')->name('procurement.assign_users');
Route::post('/procurement/list_assigned_users/{id?}', 'ProcurementController@list_assigned_users')->name('procurement.list_assigned_users');
Route::post('procurement/excel_import', 'ProcurementController@excel_import')->name('procurement.excel_import');
Route::post('procurement/excel_template', 'ProcurementController@excel_template')->name('procurement.excel_template');
Route::post('procurement/bq_sections', 'ProcurementController@bq_sections')->name('procurement.bq_sections');
Route::post('procurement/store_bill_of_quantities', 'ProcurementController@store_bill_of_quantities')->name('procurement.store_bill_of_quantities');
// Executive
Route::get('/executive/list/{id?}', 'ProcurementController@executive_list')->name('executive/list');
Route::get('/executive/create', 'ProcurementController@executive_create')->name('executive/create');
Route::post('/executive/store', 'ProcurementController@executive_store')->name('executive/store');
Route::get('/executive/show/{id?}', 'ProcurementController@executive_show')->name('executive/show');
Route::get('/executive/edit/{id?}', 'ProcurementController@executive_edit')->name('executive/edit');
Route::post('/executive/update', 'ProcurementController@executive_update')->name('executive/update');
// Planning
Route::get('/planning/list/{id?}', 'ProcurementController@planning_list')->name('planning/list');
Route::get('/planning/create', 'ProcurementController@planning_create')->name('planning/create');
Route::post('/planning/store', 'ProcurementController@planning_store')->name('planning/store');
Route::get('/planning/show/{id?}', 'ProcurementController@planning_show')->name('planning/show');
Route::get('/planning/edit/{id?}', 'ProcurementController@planning_edit')->name('planning/edit');
Route::post('/planning/update', 'ProcurementController@planning_update')->name('planning/update');
// Construction
Route::get('/construction/list/{id?}', 'ProcurementController@construction_list')->name('construction/list');
Route::get('/construction/create', 'ProcurementController@construction_create')->name('construction/create');
Route::get('/construction/create_test', 'ProcurementController@construction_create_test')->name('construction/create_test');
Route::post('/construction/store', 'ProcurementController@construction_store')->name('construction/store');
Route::get('/construction/show/{rec_id},{plan_id}', 'ProcurementController@construction_show')->name('construction/show');
Route::get('/construction/edit/{id?}', 'ProcurementController@construction_edit')->name('construction/edit');
Route::post('/construction/update', 'ProcurementController@construction_update')->name('construction/update');
Route::get('/construction/bq/list/{id}/{pro_id}', 'ProcurementController@bq_list')->name('construction.bq.list');
Route::get('/construction/bq/show/{pro_id}/{loc_id}/{id}', 'ProcurementController@bq_show')->name('construction.bq.show');
Route::post('/construction/bq/update', 'ProcurementController@bq_update')->name('construction.bq.update');
Route::get('/construction/bq/download/{pro_id}/{loc_id}', 'ProcurementController@download')->name('construction.bq.download');

/*** Completed Projects ***/
Route::get('/completed/{id?}', 'CompletedController@index')->name('completed');
Route::post('/filter_completed', 'CompletedController@filter_completed')->name('filter_completed');
Route::get('/completed/projects/{id}/{dep_id}','CompletedController@projects')->name('completed.projects');
Route::post('/completed/show_project', 'CompletedController@show_project')->name('completed.show_project');
Route::get('/completed/allProjects/{id}/{dep_id}','CompletedController@allProjects')->name('completed.allProjects');
Route::get('/completed/show/{id?}', 'CompletedController@show')->name('completed.show');
Route::get('/completed/show_survey/{rec_id},{pro_id}', 'CompletedController@show_survey')->name('completed.show_survey');
Route::get('/completed/tab_architecture/{id?}', 'CompletedController@tab_architecture')->name('completed.tab_architecture');
Route::get('/completed/show_tab_architecture/{rec_id},{pro_id}', 'CompletedController@show_tab_architecture')->name('completed.show_tab_architecture');
Route::get('/completed/tab_structure/{id?}', 'CompletedController@tab_structure')->name('completed.tab_structure');
Route::get('/completed/show_tab_structure/{rec_id},{pro_id}', 'CompletedController@show_tab_structure')->name('completed.show_tab_structure');
Route::get('/completed/tab_electricity/{id?}', 'CompletedController@tab_electricity')->name('completed.tab_electricity');
Route::get('/completed/show_tab_electricity/{rec_id},{pro_id}', 'CompletedController@show_tab_electricity')->name('completed.show_tab_electricity');
Route::get('/completed/tab_water/{id?}', 'CompletedController@tab_water')->name('completed.tab_water');
Route::get('/completed/show_tab_water/{rec_id},{pro_id}', 'CompletedController@show_tab_water')->name('completed.show_tab_water');
Route::get('/completed/tab_mechanic/{id?}', 'CompletedController@tab_mechanic')->name('completed.tab_mechanic');
Route::get('/completed/show_tab_mechanic/{rec_id},{pro_id}', 'CompletedController@show_tab_mechanic')->name('completed.show_tab_mechanic');

/*** Stopped Projects ***/
Route::get('/stopped/{id?}', 'StoppedController@index')->name('stopped');
Route::post('/filter_stopped', 'StoppedController@filter_stopped')->name('filter_stopped');
Route::get('/stopped/projects/{id}/{dep_id}','StoppedController@projects')->name('stopped.projects');
Route::post('/stopped/show_project', 'StoppedController@show_project')->name('stopped.show_project');
Route::get('/stopped/allProjects/{id}/{dep_id}','StoppedController@allProjects')->name('stopped.allProjects');
Route::get('/stopped/show/{id?}', 'StoppedController@show')->name('stopped.show');
Route::get('/stopped/show_survey/{rec_id},{pro_id}', 'StoppedController@show_survey')->name('stopped.show_survey');
Route::get('/stopped/tab_architecture/{id?}', 'StoppedController@tab_architecture')->name('stopped.tab_architecture');
Route::get('/stopped/show_tab_architecture/{rec_id},{pro_id}', 'StoppedController@show_tab_architecture')->name('stopped.show_tab_architecture');
Route::get('/stopped/tab_structure/{id?}', 'StoppedController@tab_structure')->name('stopped.tab_structure');
Route::get('/stopped/show_tab_structure/{rec_id},{pro_id}', 'StoppedController@show_tab_structure')->name('stopped.show_tab_structure');
Route::get('/stopped/tab_electricity/{id?}', 'StoppedController@tab_electricity')->name('stopped.tab_electricity');
Route::get('/stopped/show_tab_electricity/{rec_id},{pro_id}', 'StoppedController@show_tab_electricity')->name('stopped.show_tab_electricity');
Route::get('/stopped/tab_water/{id?}', 'StoppedController@tab_water')->name('stopped.tab_water');
Route::get('/stopped/show_tab_water/{rec_id},{pro_id}', 'StoppedController@show_tab_water')->name('stopped.show_tab_water');
Route::get('/stopped/tab_mechanic/{id?}', 'StoppedController@tab_mechanic')->name('stopped.tab_mechanic');
Route::get('/stopped/show_tab_mechanic/{rec_id},{pro_id}', 'StoppedController@show_tab_mechanic')->name('stopped.show_tab_mechanic');

/*** Notification ***/
Route::get('/markAsRead','HomeController@markAsRead')->name('markAsRead');
/*** Settings ***/
Route::get('/statics', 'Settings\Statics\StaticsController@index')->name('statics');
Route::get('/viewStatics', 'Settings\Statics\StaticsController@viewStatics')->name('viewStatics');
/*** Static table of Categories ***/
Route::post('/addCategory', 'Settings\Statics\CategoriesController@addCategory')->name('addCategory');
Route::post('/storeCategory', 'Settings\Statics\CategoriesController@storeCategory')->name('storeCategory');
Route::post('/viewCategory', 'Settings\Statics\CategoriesController@viewCategory')->name('viewCategory');
Route::post('/editCategory', 'Settings\Statics\CategoriesController@editCategory')->name('editCategory');
Route::post('/doEditCategory', 'Settings\Statics\CategoriesController@doEditCategory')->name('doEditCategory');
Route::post('/deleteCategory', 'Settings\Statics\CategoriesController@deleteCategory')->name('deleteCategory');
/*** Static table of doc_type ***/
Route::post('/addDocument', 'Settings\Statics\DocumentsController@addDocument')->name('addDocument');
Route::post('/storeDocument', 'Settings\Statics\DocumentsController@storeDocument')->name('storeDocument');
Route::post('/viewDocument', 'Settings\Statics\DocumentsController@viewDocument')->name('viewDocument');
Route::post('/editDocument', 'Settings\Statics\DocumentsController@editDocument')->name('editDocument');
Route::post('/doEditDocument', 'Settings\Statics\DocumentsController@doEditDocument')->name('doEditDocument');
Route::post('/deleteDocument', 'Settings\Statics\DocumentsController@deleteDocument')->name('deleteDocument');
/*** Static table of project_types ***/
Route::post('/addProject', 'Settings\Statics\ProjectsController@addProject')->name('addProject');
Route::post('/storeProject', 'Settings\Statics\ProjectsController@storeProject')->name('storeProject');
Route::post('/viewProject', 'Settings\Statics\ProjectsController@viewProject')->name('viewProject');
Route::post('/editProject', 'Settings\Statics\ProjectsController@editProject')->name('editProject');
Route::post('/doEditProject', 'Settings\Statics\ProjectsController@doEditProject')->name('doEditProject');
Route::post('/deleteProject', 'Settings\Statics\ProjectsController@deleteProject')->name('deleteProject');
/*** Static table of units ***/
Route::post('/addUnits', 'Settings\Statics\UnitsController@addUnits')->name('addUnits');
Route::post('/storeUnits', 'Settings\Statics\UnitsController@storeUnits')->name('storeUnits');
Route::post('/viewUnits', 'Settings\Statics\UnitsController@viewUnits')->name('viewUnits');
Route::post('/editUnits', 'Settings\Statics\UnitsController@editUnits')->name('editUnits');
Route::post('/doEditUnits', 'Settings\Statics\UnitsController@doEditUnits')->name('doEditUnits');
Route::post('/deleteUnits', 'Settings\Statics\UnitsController@deleteUnits')->name('deleteUnits');
/*** Static table of project_financial_source ***/
Route::post('/addFinancial_source', 'Settings\Statics\Financial_sourceController@addFinancial_source')->name('addFinancial_source');
Route::post('/storeFinancial_source', 'Settings\Statics\Financial_sourceController@storeFinancial_source')->name('storeFinancial_source');
Route::post('/viewFinancial_source', 'Settings\Statics\Financial_sourceController@viewFinancial_source')->name('viewFinancial_source');
Route::post('/editFinancial_source', 'Settings\Statics\Financial_sourceController@editFinancial_source')->name('editFinancial_source');
Route::post('/doEditFinancial_source', 'Settings\Statics\Financial_sourceController@doEditFinancial_source')->name('doEditFinancial_source');
Route::post('/deleteFinancial_source', 'Settings\Statics\Financial_sourceController@deleteFinancial_source')->name('deleteFinancial_source');
/*** Static table of employees ***/
Route::post('/addEmployee', 'Settings\Statics\EmployeesController@addEmployee')->name('addEmployee');
Route::post('/storeEmployee', 'Settings\Statics\EmployeesController@storeEmployee')->name('storeEmployee');
Route::post('/viewEmployee', 'Settings\Statics\EmployeesController@viewEmployee')->name('viewEmployee');
Route::post('/editEmployee', 'Settings\Statics\EmployeesController@editEmployee')->name('editEmployee');
Route::post('/doEditEmployee', 'Settings\Statics\EmployeesController@doEditEmployee')->name('doEditEmployee');
Route::post('/deleteEmployee', 'Settings\Statics\EmployeesController@deleteEmployee')->name('deleteEmployee');
/*** Static table of roof ***/
Route::post('/addRoof', 'Settings\Statics\RoofController@addRoof')->name('addRoof');
Route::post('/storeRoof', 'Settings\Statics\RoofController@storeRoof')->name('storeRoof');
Route::post('/viewRoof', 'Settings\Statics\RoofController@viewRoof')->name('viewRoof');
Route::post('/editRoof', 'Settings\Statics\RoofController@editRoof')->name('editRoof');
Route::post('/doEditRoof', 'Settings\Statics\RoofController@doEditRoof')->name('doEditRoof');
Route::post('/deleteRoof', 'Settings\Statics\RoofController@deleteRoof')->name('deleteRoof');
/*** Static table of fear ***/
Route::post('/addFear', 'Settings\Statics\FearController@addFear')->name('addFear');
Route::post('/storeFear', 'Settings\Statics\FearController@storeFear')->name('storeFear');
Route::post('/viewFear', 'Settings\Statics\FearController@viewFear')->name('viewFear');
Route::post('/editFear', 'Settings\Statics\FearController@editFear')->name('editFear');
Route::post('/doEditFear', 'Settings\Statics\FearController@doEditFear')->name('doEditFear');
Route::post('/deleteFear', 'Settings\Statics\FearController@deleteFear')->name('deleteFear');
/*** Static table of foundation ***/
Route::post('/addFoundation', 'Settings\Statics\FoundationController@addFoundation')->name('addFoundation');
Route::post('/storeFoundation', 'Settings\Statics\FoundationController@storeFoundation')->name('storeFoundation');
Route::post('/viewFoundation', 'Settings\Statics\FoundationController@viewFoundation')->name('viewFoundation');
Route::post('/editFoundation', 'Settings\Statics\FoundationController@editFoundation')->name('editFoundation');
Route::post('/doEditFoundation', 'Settings\Statics\FoundationController@doEditFoundation')->name('doEditFoundation');
Route::post('/deleteFoundation', 'Settings\Statics\FoundationController@deleteFoundation')->name('deleteFoundation');
/*** Static table of stair ***/
Route::post('/addStair', 'Settings\Statics\StairController@addStair')->name('addStair');
Route::post('/storeStair', 'Settings\Statics\StairController@storeStair')->name('storeStair');
Route::post('/viewStair', 'Settings\Statics\StairController@viewStair')->name('viewStair');
Route::post('/editStair', 'Settings\Statics\StairController@editStair')->name('editStair');
Route::post('/doEditStair', 'Settings\Statics\StairController@doEditStair')->name('doEditStair');
Route::post('/deleteStair', 'Settings\Statics\StairController@deleteStair')->name('deleteStair');
/*** Static table of wall ***/
Route::post('/addWall', 'Settings\Statics\WallController@addWall')->name('addWall');
Route::post('/storeWall', 'Settings\Statics\WallController@storeWall')->name('storeWall');
Route::post('/viewWall', 'Settings\Statics\WallController@viewWall')->name('viewWall');
Route::post('/editWall', 'Settings\Statics\WallController@editWall')->name('editWall');
Route::post('/doEditWall', 'Settings\Statics\WallController@doEditWall')->name('doEditWall');
Route::post('/deleteWall', 'Settings\Statics\WallController@deleteWall')->name('deleteWall');
/*** Static table of Wall_shape ***/
Route::post('/addWall_shape', 'Settings\Statics\Wall_shapeController@addWall_shape')->name('addWall_shape');
Route::post('/storeWall_shape', 'Settings\Statics\Wall_shapeController@storeWall_shape')->name('storeWall_shape');
Route::post('/viewWall_shape', 'Settings\Statics\Wall_shapeController@viewWall_shape')->name('viewWall_shape');
Route::post('/editWall_shape', 'Settings\Statics\Wall_shapeController@editWall_shape')->name('editWall_shape');
Route::post('/doEditWall_shape', 'Settings\Statics\Wall_shapeController@doEditWall_shape')->name('doEditWall_shape');
Route::post('/deleteWall_shape', 'Settings\Statics\Wall_shapeController@deleteWall_shape')->name('deleteWall_shape');
/*** Static table of column ***/
Route::post('/addColumn', 'Settings\Statics\ColumnController@addColumn')->name('addColumn');
Route::post('/storeColumn', 'Settings\Statics\ColumnController@storeColumn')->name('storeColumn');
Route::post('/viewColumn', 'Settings\Statics\ColumnController@viewColumn')->name('viewColumn');
Route::post('/editColumn', 'Settings\Statics\ColumnController@editColumn')->name('editColumn');
Route::post('/doEditColumn', 'Settings\Statics\ColumnController@doEditColumn')->name('doEditColumn');
Route::post('/deleteColumn', 'Settings\Statics\ColumnController@deleteColumn')->name('deleteColumn');
/*** Static table of column_shape ***/
Route::post('/addColumn_shape', 'Settings\Statics\Column_shapeController@addColumn_shape')->name('addColumn_shape');
Route::post('/storeColumn_shape', 'Settings\Statics\Column_shapeController@storeColumn_shape')->name('storeColumn_shape');
Route::post('/viewColumn_shape', 'Settings\Statics\Column_shapeController@viewColumn_shape')->name('viewColumn_shape');
Route::post('/editColumn_shape', 'Settings\Statics\Column_shapeController@editColumn_shape')->name('editColumn_shape');
Route::post('/doEditColumn_shape', 'Settings\Statics\Column_shapeController@doEditColumn_shape')->name('doEditColumn_shape');
Route::post('/deleteColumn_shape', 'Settings\Statics\Column_shapeController@deleteColumn_shape')->name('deleteColumn_shape');
/*** Static table of brick ***/
Route::post('/addBrick', 'Settings\Statics\BrickController@addBrick')->name('addBrick');
Route::post('/storeBrick', 'Settings\Statics\BrickController@storeBrick')->name('storeBrick');
Route::post('/viewBrick', 'Settings\Statics\BrickController@viewBrick')->name('viewBrick');
Route::post('/editBrick', 'Settings\Statics\BrickController@editBrick')->name('editBrick');
Route::post('/doEditBrick', 'Settings\Statics\BrickController@doEditBrick')->name('doEditBrick');
Route::post('/deleteBrick', 'Settings\Statics\BrickController@deleteBrick')->name('deleteBrick');
/*** Static table of activities ***/
Route::post('/addActivities', 'Settings\Statics\ActivitiesController@addActivities')->name('addActivities');
Route::post('/storeActivities', 'Settings\Statics\ActivitiesController@storeActivities')->name('storeActivities');
Route::post('/viewActivities', 'Settings\Statics\ActivitiesController@viewActivities')->name('viewActivities');
Route::post('/editActivities', 'Settings\Statics\ActivitiesController@editActivities')->name('editActivities');
Route::post('/doEditActivities', 'Settings\Statics\ActivitiesController@doEditActivities')->name('doEditActivities');
Route::post('/deleteActivities', 'Settings\Statics\ActivitiesController@deleteActivities')->name('deleteActivities');
/*** Static table of workers ***/
Route::post('/addWorkers', 'Settings\Statics\WorkersController@addWorkers')->name('addWorkers');
Route::post('/storeWorkers', 'Settings\Statics\WorkersController@storeWorkers')->name('storeWorkers');
Route::post('/viewWorkers', 'Settings\Statics\WorkersController@viewWorkers')->name('viewWorkers');
Route::post('/editWorkers', 'Settings\Statics\WorkersController@editWorkers')->name('editWorkers');
Route::post('/doEditWorkers', 'Settings\Statics\WorkersController@doEditWorkers')->name('doEditWorkers');
Route::post('/deleteWorkers', 'Settings\Statics\WorkersController@deleteWorkers')->name('deleteWorkers');
/*** Static table of Equipments ***/
Route::post('/addEquipments', 'Settings\Statics\EquipmentsController@addEquipments')->name('addEquipments');
Route::post('/storeEquipments', 'Settings\Statics\EquipmentsController@storeEquipments')->name('storeEquipments');
Route::post('/viewEquipments', 'Settings\Statics\EquipmentsController@viewEquipments')->name('viewEquipments');
Route::post('/editEquipments', 'Settings\Statics\EquipmentsController@editEquipments')->name('editEquipments');
Route::post('/doEditEquipments', 'Settings\Statics\EquipmentsController@doEditEquipments')->name('doEditEquipments');
Route::post('/deleteEquipments', 'Settings\Statics\EquipmentsController@deleteEquipments')->name('deleteEquipments');
/*** Static table of Payments ***/
Route::post('/addPayments', 'Settings\Statics\PaymentsController@addPayments')->name('addPayments');
Route::post('/storePayments', 'Settings\Statics\PaymentsController@storePayments')->name('storePayments');
Route::post('/viewPayments', 'Settings\Statics\PaymentsController@viewPayments')->name('viewPayments');
Route::post('/editPayments', 'Settings\Statics\PaymentsController@editPayments')->name('editPayments');
Route::post('/doEditPayments', 'Settings\Statics\PaymentsController@doEditPayments')->name('doEditPayments');
Route::post('/deletePayments', 'Settings\Statics\PaymentsController@deletePayments')->name('deletePayments');
/*** Static table of problems ***/
Route::post('/addProblems', 'Settings\Statics\ProblemsController@addProblems')->name('addProblems');
Route::post('/storeProblems', 'Settings\Statics\ProblemsController@storeProblems')->name('storeProblems');
Route::post('/viewProblems', 'Settings\Statics\ProblemsController@viewProblems')->name('viewProblems');
Route::post('/editProblems', 'Settings\Statics\ProblemsController@editProblems')->name('editProblems');
Route::post('/doEditProblems', 'Settings\Statics\ProblemsController@doEditProblems')->name('doEditProblems');
Route::post('/deleteProblems', 'Settings\Statics\ProblemsController@deleteProblems')->name('deleteProblems');

/*** Static table of national Categories ***/
Route::post('/addNationalCategory', 'Settings\Statics\NationalCategoriesController@addNationalCategory')->name('addNationalCategory');
Route::post('/storeNationalCategory', 'Settings\Statics\NationalCategoriesController@storeNationalCategory')->name('storeNationalCategory');
Route::post('/viewNationalCategory', 'Settings\Statics\NationalCategoriesController@viewNationalCategory')->name('viewNationalCategory');
Route::post('/editNationalCategory', 'Settings\Statics\NationalCategoriesController@editNationalCategory')->name('editNationalCategory');
Route::post('/doEditNationalCategory', 'Settings\Statics\NationalCategoriesController@doEditNationalCategory')->name('doEditNationalCategory');
Route::post('/deleteNationalCategory', 'Settings\Statics\NationalCategoriesController@deleteNationalCategory')->name('deleteNationalCategory');
/*** Static table of national project types ***/
Route::post('/addNationalProject', 'Settings\Statics\NationalProjectsController@addNationalProject')->name('addNationalProject');
Route::post('/storeNationalProject', 'Settings\Statics\NationalProjectsController@storeNationalProject')->name('storeNationalProject');
Route::post('/viewNationalProject', 'Settings\Statics\NationalProjectsController@viewNationalProject')->name('viewNationalProject');
Route::post('/editNationalProject', 'Settings\Statics\NationalProjectsController@editNationalProject')->name('editNationalProject');
Route::post('/doEditNationalProject', 'Settings\Statics\NationalProjectsController@doEditNationalProject')->name('doEditNationalProject');
Route::post('/deleteNationalProject', 'Settings\Statics\NationalProjectsController@deleteNationalProject')->name('deleteNationalProject');


/*** Labor Clasification ***/
Route::resource('clasification','Settings\Statics\ClasificationController');
/*** Machinery ***/
Route::resource('machinery','Settings\Statics\MachineryController');
/*** BQ Section ***/
Route::resource('bq_section','Settings\Statics\Bq_sectionController');
/*** Setting Portfolio ***/
Route::resource('setting_portfolios','Settings\PortfoliosController')->except(['index']);
Route::get('/setting_portfolio', 'Settings\PortfoliosController@index')->name('setting_portfolio');
Route::get('/filterPortfolio','Settings\PortfoliosController@filterPortfolio')->name('filterPortfolio');
Route::post('setting_portfolios/projects','Settings\PortfoliosController@getPorjectsByDepartment')->name('setting_portfolios.projects');

/***** Authentication Module Routes *****/

/*** Modules ***/
Route::resource('modules','Authentication\ModulesController');
Route::get('/modules', 'Authentication\ModulesController@index')->name('modules');
Route::get('/filterModule', 'Authentication\ModulesController@filterModule')->name('filterModule');

/*** Organizations ***/
Route::resource('organizations','Authentication\OrganizationsController');
Route::get('/organizations', 'Authentication\OrganizationsController@index')->name('organizations');
Route::get('/filterOrganization', 'Authentication\OrganizationsController@filterOrganization')->name('filterOrganization');

/*** Department ***/
Route::resource('departments','Authentication\DepartmentsController');
Route::get('/departments', 'Authentication\DepartmentsController@index')->name('departments');
Route::get('/filterDepartment', 'Authentication\DepartmentsController@filterDepartment')->name('filterDepartment');

/*** Sections ***/
Route::resource('sections','Authentication\SectionsController');
Route::get('/sections','Authentication\SectionsController@index')->name('sections');
Route::get('/filterSection', 'Authentication\SectionsController@filterSection')->name('filterSection');
Route::post('sections/sharing', 'Authentication\SectionsController@sharing')->name('sections.sharing');
Route::get('sections/distroySharing/{id?}', 'Authentication\SectionsController@distroySharing')->name('sections.distroySharing');

/*** Roles ***/
Route::resource('roles','Authentication\RolesController');
Route::get('roles','Authentication\RolesController@index')->name('roles');
Route::get('/filterRole','Authentication\RolesController@filterRole')->name('filterRole');

/*** Users ***/
Route::resource('users','Authentication\UsersController');
Route::get('/users', 'Authentication\UsersController@index')->name('users');
Route::get('/filterUser', 'Authentication\UsersController@filterUser')->name('filterUser');
Route::post('/uploadPic', 'Authentication\UsersController@uploadPic')->name('uploadPic');
Route::post('/editPassword', 'Authentication\UsersController@editPassword')->name('editPassword');
Route::post('getModulesByDepartments','Authentication\UsersController@getModulesByDepartments')->name('getModulesByDepartments');
Route::get('getModuleSections/{dep_id}','Authentication\UsersController@getModuleSections')->name('getModuleSections');
Route::get('getSectionRoles/{dep_id}','Authentication\UsersController@getSectionRoles')->name('getSectionRoles');
Route::post('getDepartmentByOrganization','Authentication\UsersController@getDepartmentByOrganization')->name('getDepartmentByOrganization');

/*** Searcch ***/
Route::get('/report/{dep_id?}', 'ReportController@index')->name('report');
Route::get('/viewReport', 'ReportController@viewReport')->name('viewReport');
Route::get('/report_request', 'ReportController@report_request')->name('report_request');
Route::get('/report_plan', 'ReportController@report_plan')->name('report_plan');
Route::post('/bringProjectTypesReport', 'ReportController@bringProjectTypesReport')->name('bringProjectTypesReport');
Route::get('/report_survey', 'ReportController@report_survey')->name('report_survey');
Route::get('/report_design', 'ReportController@report_design')->name('report_design');


/*** Request Share Route ***/
// Route::resource('/request_share','RequestShareController')->except(['update']);

/*** Share Route ***/
Route::resource('/share','ShareController')->except(['update']);
Route::post('bringDepartments', 'ShareController@bringDepartments')->name('bringDepartments');

/** Attachments */
Route::post('attachment/destroy','HomeController@destoryAttachment')->name('attachment.destroy');
Route::post('attachment/update','HomeController@editAttachment')->name('attachment.edit');
Route::post('attachment/create','HomeController@createAttachment')->name('attachment.create');
Route::get('attachment/view/{id},{table}', 'HomeController@viewAttachment')->name('attachment.view');

/** Documents */
Route::post('document/update','HomeController@editDocument')->name('document.edit');
Route::post('document/create','HomeController@createDocument')->name('document.create');
Route::post('document/store','HomeController@storeDocument')->name('document.store');


/*** Summary Route ***/
Route::resource('/summary','SummaryController');
Route::get('summary/show_survey/{rec_id},{parent_id}', 'SummaryController@show_survey')->name('summary.show_survey');
Route::get('tab_architecture/{id?}', 'SummaryController@tab_architecture')->name('tab_architecture');
Route::get('summary/show_tab_architecture/{rec_id},{parent_id}', 'SummaryController@show_tab_architecture')->name('summary.show_tab_architecture');
Route::get('tab_structure/{id?}', 'SummaryController@tab_structure')->name('tab_structure');
Route::get('summary/show_tab_structure/{rec_id},{parent_id}', 'SummaryController@show_tab_structure')->name('summary.show_tab_structure');
Route::get('tab_electricity/{id?}', 'SummaryController@tab_electricity')->name('tab_electricity');
Route::get('summary/show_tab_electricity/{rec_id},{parent_id}', 'SummaryController@show_tab_electricity')->name('summary.show_tab_electricity');
Route::get('tab_water/{id?}', 'SummaryController@tab_water')->name('tab_water');
Route::get('summary/show_tab_water/{rec_id},{parent_id}', 'SummaryController@show_tab_water')->name('summary.show_tab_water');
Route::get('tab_mechanic/{id?}', 'SummaryController@tab_mechanic')->name('tab_mechanic');
Route::get('summary/show_tab_mechanic/{rec_id},{parent_id}', 'SummaryController@show_tab_mechanic')->name('summary.show_tab_mechanic');
Route::get('summary/list_bill_of_quantities/{id?}', 'SummaryController@list_bill_of_quantities')->name('summary.list_bill_of_quantities');
Route::get('summary/view_bill_of_quantities/{id}/{loc_id}', 'SummaryController@view_bill_of_quantities')->name('summary.view_bill_of_quantities');

/** Contractors */
Route::resource('contractors','Settings\ContractorController');
Route::get('/contractors', 'Settings\ContractorController@index')->name('contractors');
Route::get('/filterContractors', 'Settings\ContractorController@filterContractors')->name('filterContractors');

/*** Project Setting Routes ***/
Route::resource('projects','Settings\ProjectsController');
Route::get('/bringProjects/{id}', 'Settings\ProjectsController@bringProjects')->name('bringProjects');
Route::get('/projects/list/{id}', 'Settings\ProjectsController@list')->name('projects.list');
Route::get('/filter_projects', 'Settings\ProjectsController@filter_projects')->name('filter_projects');
Route::get('/projects/create_staff/{id}', 'Settings\ProjectsController@create_staff')->name('projects.create_staff');
Route::post('/projects/store_staff', 'Settings\ProjectsController@store_staff')->name('projects.store_staff');

Route::get('/bringProjectsStaff/{id}', 'Settings\ProjectsController@bringProjectsStaff')->name('bringProjectsStaff');
Route::get('list_staff/{id?}', 'Settings\ProjectsController@list_staff')->name('list_staff');
Route::get('staff_destroy/{id?}', 'Settings\ProjectsController@staff_destroy')->name('staff_destroy');
Route::get('staff_add/{id?}', 'Settings\ProjectsController@staff_add')->name('staff_add');
Route::post('staff_store', 'Settings\ProjectsController@staff_store')->name('staff_store');

/** Contractors Staff */
Route::resource('contractor_staff', 'Settings\ContractorStaffController');
Route::get('/contractor_staff', 'Settings\ContractorStaffController@index')->name('contractor_staff');
Route::get('/filter_contractor_staff', 'Settings\ContractorStaffController@filter_contractor_staff')->name('filter_contractor_staff');
Route::post('/contractor_modulesByDepartment', 'Settings\ContractorStaffController@modulesByDepartment')->name('contractor_modulesByDepartment');
Route::post('/contractor_sectionsByModule', 'Settings\ContractorStaffController@sectionsByModule')->name('contractor_sectionsByModule');
Route::post('/contractor_rolesBySections', 'Settings\ContractorStaffController@rolesBySections')->name('contractor_rolesBySections');

/*** Routes of Contractor's Project ***/
Route::resource('contractor_projects','ContractorProjectController')->except(['index']);
Route::get('/contractor_project/{con_id}/{dep_id}','ContractorProjectController@index')->name('contractor_project');
Route::get('/contractor_projects/view/{id}/{dep_id}','ContractorProjectController@view')->name('contractor_projects.view');
Route::post('/filter_contractor_project','ContractorProjectController@filter_contractor_project')->name('filter_contractor_project');

/*** Routes of Work Categroy Controller ***/
Route::resource('work_category','Settings\WorkCategoryController');
Route::get('/work_category','Settings\WorkCategoryController@index')->name('work_category');
Route::get('/filterWorkCategory','Settings\WorkCategoryController@filterWorkCategory')->name('filterWorkCategory');

/*** Routes of Feature of Work  Controller ***/
Route::resource('feature_of_work','Settings\FeatureOfWorkController');
Route::get('/feature_of_work','Settings\FeatureOfWorkController@index')->name('feature_of_work');
Route::get('/filterFeatureOfWork','Settings\FeatureOfWorkController@filterFeatureOfWork')->name('filterFeatureOfWork');


/*** Routes of Inspection  Controller ***/
Route::resource('inspection','Settings\InspectionController');
Route::get('/inspection','Settings\InspectionController@index')->name('inspection');
Route::get('/filterInspection','Settings\InspectionController@filterInspection')->name('filterInspection');
Route::post('/more_inspection_sub','Settings\InspectionController@more_inspection_sub')->name('more_inspection_sub');
Route::get('/getBillOfQuantity/{id}/{dep_id}','Settings\InspectionController@getBillOfQuantity')->name('getBillOfQuantity');

/*** Routes of Inspection  Controller ***/
Route::resource('three_phase_Inspection','ThreePhaseInspectionController')->except(['index','show']);
Route::get('/three_phase_Inspection/show/{pro_id}/{bq_id}/{loc_id}','ThreePhaseInspectionController@show')->name('three_phase_Inspection/show');
Route::get('/three_phase_Inspection/{id}/{dep_id}','ThreePhaseInspectionController@index')->name('three_phase_Inspection');
Route::get('/three_phase_Inspection/list/{id}/{dep_id}/{loc_id}','ThreePhaseInspectionController@list')->name('three_phase_Inspection/list');
Route::get('/getTreeView','ThreePhaseInspectionController@getTreeView')->name('getTreeView');
Route::post('/getSubInepection','ThreePhaseInspectionController@getSubInepection')->name('getSubInepection');
Route::get('/getInspectionDetails','ThreePhaseInspectionController@getInspectionDetails')->name('getInspectionDetails');


/*** Progress ***/
Route::get('/progress/{id?}', 'ProgressController@index')->name('progress');
Route::post('/filter_progress', 'ProgressController@filter_progress')->name('filter_progress');
Route::get('/progress_notification/{id?}', 'ProgressController@progress_notification')->name('progress_notification');
Route::post('progress/priority', 'ProgressController@priority')->name('progress.priority');
Route::get('progress/priority/priority_add/{id?}', 'ProgressController@priority_add')->name('progress.priority_add');

/*** Routes of BQ Schedules ***/
Route::resource('BQSchedules','BQSchedulesController')->except(['index']);
Route::get('/BQSchedule/{id}/{dep_id}','BQSchedulesController@index')->name('BQSchedule');
Route::get('/BQSchedule/list/{id}/{dep_id}/{loc_id}','BQSchedulesController@list')->name('BQSchedule/list');
Route::post('/calculate_schedules','BQSchedulesController@calculate_schedules')->name('BQSchedules.calculate_schedules');
Route::get('BQSchedule/make_base/{pro_id}/{loc_id}','BQSchedulesController@make_base')->name('BQSchedule.make_base');
Route::get('/BQSchedule/base/{id}/{dep_id}/{loc_id}','BQSchedulesController@base')->name('BQSchedule.base');

/** Daily Reports route   */
Route::get('daily_report/{project_id}','DailyReportController@index')->name('daily_report.index');
Route::resource('daily_report','DailyReportController')->except(['index']);
Route::get('daily_report/notifications/{id}/{con_id}/{dep_id}', 'DailyReportController@list_notification')->name('daily_report/notifications');
Route::get('daily_report/filterDailyReports', 'DailyReportController@filterDailyReports')->name('filterDailyReports');

Route::post('daily_report/weather','DailyReportController@weather')->name('weather.index');
Route::post('daily_report/weather/store','DailyReportController@storeWeather')->name('daily_weather.store');
Route::post('daily_report/weather/update','DailyReportController@updateWeather')->name('daily_weather.update');

Route::post('daily_report/qcnarrative','DailyReportController@qcNarrative')->name('qcnarrative.index');
Route::post('daily_report/qcnarrative/store','DailyReportController@qcNarrativeStore')->name('qcnarrative.store');
Route::post('daily_report/qcnarrative/edit','DailyReportController@qcNarrativeEdit')->name('qcnarrative.edit');
Route::post('daily_report/qcnarrative/update','DailyReportController@qcNarrativeUpdate')->name('qcnarrative.update');

Route::post('daily_report/qctest','DailyReportController@qcTest')->name('qctest.index');
Route::post('daily_report/qctest/store', 'DailyReportController@qcTestStore')->name('qcTest.store');
Route::post('daily_report/qctest/edit', 'DailyReportController@qcTestEdit')->name('qcTest.edit');
Route::post('daily_report/qctest/update', 'DailyReportController@qcTestUpdate')->name('qcTest.update');
Route::post('daily_report/qctest/delete', 'DailyReportController@qcTestDelete')->name('qcTest.delete');

Route::post('daily_report/activities','DailyReportController@activities')->name('daily_activities.index');
Route::post('daily_report/activities/store', 'DailyReportController@activitiesStore')->name('daily_activities.store');
Route::post('daily_report/activities/edit', 'DailyReportController@activitiesEdit')->name('daily_activities.edit');
Route::post('daily_report/activities/update', 'DailyReportController@activitiesUpdate')->name('daily_activities.update');
Route::post('daily_report/activities/delete', 'DailyReportController@activitiesDelete')->name('daily_activities.delete');

Route::post('daily_report/location','DailyReportController@location')->name('location.index');
Route::post('daily_report/location/store', 'DailyReportController@locationStore')->name('location.store');
Route::post('daily_report/location/update','DailyReportController@locationUpdate')->name('location.update');

Route::post('daily_report/equipment','DailyReportController@equipment')->name('equipment.index');
Route::post('aily_report/equipment/store', 'DailyReportController@equipmentStore')->name('equipment.store');
Route::post('aily_report/equipment/edit', 'DailyReportController@equipmentEdit')->name('equipment.edit');
Route::post('aily_report/equipment/update', 'DailyReportController@equipmentUpdate')->name('equipment.update');
Route::post('aily_report/equipment/delete', 'DailyReportController@equipmentDelete')->name('equipment.delete');

Route::post('daily_report/labor','DailyReportController@labor')->name('labor.index');
Route::post('daily_report/labor/store', 'DailyReportController@laborStore')->name('labor.store');
Route::post('daily_report/labor/edit', 'DailyReportController@laborEdit')->name('labor.edit');
Route::post('daily_report/labor/update', 'DailyReportController@laborUpdate')->name('labor.update');
Route::post('daily_report/labor/delete', 'DailyReportController@laborDelete')->name('labor.delete');

Route::post('daily_report/deficiencey_item','DailyReportController@deficiencey_item')->name('deficiencey_item.index');
Route::post('/bringUnit', 'DailyReportController@bringUnit')->name('bringUnit');
Route::post('/bringAmount', 'DailyReportController@bringAmount')->name('bringAmount');

/*** Accidents ***/
Route::post('daily_report/accidents','DailyReportController@accidents')->name('accidents.index');
Route::post('daily_report/accidents/store','DailyReportController@accidentsStore')->name('accidents.store');
Route::post('daily_report/accidents/edit','DailyReportController@accidentsEdit')->name('accidents.edit');
Route::post('daily_report/accidents/update','DailyReportController@accidentsUpdate')->name('accidents.update');
/*** Photos ***/
Route::post('daily_report/photos','DailyReportController@photos')->name('photos.index');
Route::post('daily_report/photos/store','DailyReportController@photosStore')->name('photos.store');
Route::post('daily_report/photos/delete','DailyReportController@photosDelete')->name('photos.delete');
Route::post('daily_report/photos/bringMorePhotos', 'DailyReportController@bringMorePhotos')->name('bringMorePhotos');
Route::post('daily_report/photos/store','DailyReportController@photosStore')->name('photos.store');
Route::get('daily_report/photos/view/{id?}','DailyReportController@viewPhotos')->name('photos.view');


/*** Hazard Analysis ***/
Route::resource('hazard_analysis','HazardAnalysisController')->except(['index','show']);
Route::get('/hazard_analysis/{id}/{dep_id}','HazardAnalysisController@index')->name('hazard_analysis');
Route::get('/hazard_analysis/list/{id}/{dep_id}/{loc_id}','HazardAnalysisController@list')->name('hazard_analysis/list');
Route::get('/hazard_analysis/show/{pro_id}/{bq_id}/{loc_id}','HazardAnalysisController@show')->name('hazard_analysis/show');
Route::post('/hazard_analysis/store_sub','HazardAnalysisController@store_sub')->name('hazard_analysis/store_sub');
Route::post('/more_hazard_analysis','HazardAnalysisController@more_hazard_analysis')->name('more_hazard_analysis');
Route::post('/store_hazard_inspection','HazardAnalysisController@store_hazard_inspection')->name('store_hazard_inspection');
Route::post('/get_Hazard_Analysis_Data','HazardAnalysisController@get_Hazard_Analysis_Data')->name('get_Hazard_Analysis_Data');

/*** Charts ***/
Route::resource('charts','ChartsController')->except(['index']);
Route::get('charts/index/{id}/{dep_id}','ChartsController@index')->name('charts/index');
Route::get('charts/BQChart/{id}/{dep_id}/{loc_id}','ChartsController@BQChart')->name('charts/BQChart');

Route::get('/billQuantityChart/{id?}', 'ChartsController@billQuantityChart')->name('billQuantityChart');

/*** Reports route */

/*** Project status report ***/
Route::post('project_status_report','ReportController@project_status_report')->name('project_status_report');
Route::get('report/project_status_report_list/{id}/{share_to}/{status}/{share_code}/{table}','ReportController@project_status_report_list')->name('project_status_report_list');
Route::get('report/project_status_report_excel/{id}','ReportController@project_status_report_excel')->name('project_status_report_excel');
Route::get('report/project_status_report_pdf/{id}','ReportController@project_status_report_pdf')->name('project_status_report_pdf');
Route::get('report/project_status_report_detailed_pdf/{id}/{code}','ReportController@project_status_report_detailed_pdf')->name('project_status_report_detailed_pdf');

Route::post('project_status_report_detailed','ReportController@project_status_report_detailed')->name('project_status_report_detailed');

/*** Project progressive report ***/
Route::post('report/project_progressive_report','ReportController@project_progressive_report')->name('report.project_progressive_report');
Route::get('report/project_progressive_report_pdf/{id}','ReportController@project_progressive_report_pdf')->name('project_progressive_report_pdf');
Route::post('report/bringCategoriesByDirectorate','ReportController@bringCategoriesByDirectorate')->name('report.bringCategoriesByDirectorate');
Route::post('report/bringProjectTypesByCategory','ReportController@bringProjectTypesByCategory')->name('report.bringProjectTypesByCategory');
Route::post('report/bringProjectsByType','ReportController@bringProjectsByType')->name('report.bringProjectsByType');

/*** Project daily report ***/
Route::get('report/index/{dep_id}','ReportController@index')->name('report.index');
Route::post('report/show','ReportController@show')->name('report.show');
Route::post('report/daily_report','ReportController@daily_report')->name('report.daily_report');
Route::get('report/daily_report_pdf/{id}','ReportController@daily_report_pdf')->name('daily_report_pdf');

/*** Project status report ***/
Route::post('project/location/option','ReportController@getPorjectLocation')->name('project_location.option');
Route::post('project/status','ReportController@getProjectStatus')->name('getProjectStatus');
Route::post('project/section/activity','ReportController@dep_sections_activity')->name('dep_sections_activity');
Route::post('project/section/bringDepSections','ReportController@bringDepSections')->name('bringDepSections');
Route::post('project/section/bringSectionUsers','ReportController@bringSectionUsers')->name('bringSectionUsers');
Route::post('project/section/bringContractorSections','ReportController@bringContractorSections')->name('contractorSection');
Route::post('project/section/bringContractorSectionUsers','ReportController@bringContractorSectionUsers')->name('bringContractorSectionUsers');
Route::post('bringProjectsByDepartments','ReportController@bringProjectsByDepartments')->name('bringProjectsByDepartments');
Route::post('report/bringSectionsByDepartment','ReportController@bringSectionsByDepartment')->name('report.bringSectionsByDepartment');

/*** Daily Reports Monitoring ***/
Route::resource('daily_report_monitoring','MonitoringController')->except(['index','show']);
Route::get('daily_report_monitoring/index/{dep_id}','MonitoringController@index')->name('daily_report_monitoring/index/');
Route::get('daily_report_monitoring/list/{id?}', 'MonitoringController@list')->name('daily_report_monitoring/list');
Route::get('daily_report_monitoring/show/{id}/{dep_id}/{loc_id}','MonitoringController@show')->name('daily_report_monitoring/show');

Route::post('filter_monitoring', 'MonitoringController@filter_monitoring')->name('filter_monitoring');
Route::get('daily_report_monitoring/location/{id?}', 'MonitoringController@location')->name('daily_report_monitoring/location/');
Route::post('daily_report_monitoring/show_content', 'MonitoringController@show_content')->name('daily_report_monitoring/show_content');
Route::post('daily_report_monitoring/store', 'MonitoringController@store')->name('daily_report_monitoring/store');
/*** Graphs ***/
Route::resource('graph','GraphController')->except(['index']);
Route::get('graph/index/{dep_id}','GraphController@index')->name('graph.index');
Route::post('location_year','GraphController@location_year')->name('location_year');
Route::post('location_year_graph','GraphController@location_year_graph')->name('location_year_graph');


Route::post('report/project_progress','ReportController@projectProgressSearch')->name('project_progress.search');
/*** Bring search option route ***/
Route::post('search/option','HomeController@SearchOption')->name('search.option');

/*** Payments route **/
Route::post('payment/request/billquantity','PaymentRequestController@paymentBquantity')->name('payment_request.billquantity');
Route::post('payment/activity/add','PaymentRequestController@addActivityToRequest')->name('add_activity_to_payment');
Route::any('payment/request/{id?}','PaymentRequestController@index')->name('payment_request');
Route::resource('payment_request','PaymentRequestController');
// Invoice Route
Route::post('payment/recieve','PaymentRequestController@recieveInvoice')->name('payment_recieve');

/*** Calendar ***/
Route::resource('calendar','CalendarController')->except(['index','show']);
Route::get('/calendars/{id}/{dep_id}','CalendarController@index')->name('calendars');
Route::get('/calendars/show/{id}/{dep_id}/{loc_id}','CalendarController@show')->name('calendars.show');
Route::post('/calendar/store_holidays', 'CalendarController@store_holidays')->name('calendar.store_holidays');
Route::post('/calendar/import_holidays', 'CalendarController@import_holidays')->name('calendar.import_holidays');
Route::post('/calendar/update_holidays', 'CalendarController@update_holidays')->name('calendar.update_holidays');
Route::post('calendar/locations','CalendarController@get_locations')->name('calendar.locations');


/*** Change Request ***/
Route::resource('change_requests','ChangeRequestController')->except(['index']);
Route::get('/change_request/{dep_id}', 'ChangeRequestController@index')->name('change_request');
Route::post('change_request_approval', 'ChangeRequestController@change_request_approval')->name('change_request_approval');
Route::post('change_request_approval_load', 'ChangeRequestController@change_request_approval_load')->name('change_request_approval_load');
Route::post('change_request_edit_load', 'ChangeRequestController@changeRequestEditLoad')->name('change_request_edit_load');

Route::get('list_cost_scope', 'ChangeRequestController@list_cost_scope')->name('list_cost_scope');
Route::post('store_cost_scope', 'ChangeRequestController@store_cost_scope')->name('store_cost_scope');
Route::post('edit_cost_scope', 'ChangeRequestController@edit_cost_scope')->name('edit_cost_scope');

Route::get('list_time', 'ChangeRequestController@list_time')->name('list_time');
Route::post('store_time', 'ChangeRequestController@store_time')->name('store_time');
Route::post('edit_time_load', 'ChangeRequestController@editTimeLoad')->name('edit_time_load');
Route::post('edit_time', 'ChangeRequestController@edit_time')->name('edit_time');

Route::get('list_start_stop', 'ChangeRequestController@list_start_stop')->name('list_start_stop');
Route::post('store_start_stop', 'ChangeRequestController@store_start_stop')->name('store_start_stop');
Route::post('start_stop_edit_load', 'ChangeRequestController@startStopEditLoad')->name('start_stop_edit_load');
Route::post('edit_start_stop', 'ChangeRequestController@edit_start_stop')->name('edit_start_stop');

Route::get('getBillOfQuantities', 'ChangeRequestController@getBillOfQuantities')->name('getBillOfQuantities');
Route::get('getDateOfQuantities', 'ChangeRequestController@getDateOfQuantities')->name('getDateOfQuantities');
Route::get('getCostScopeContent', 'ChangeRequestController@getCostScopeContent')->name('getCostScopeContent');
Route::get('get_start_stop_content', 'ChangeRequestController@get_start_stop_content')->name('get_start_stop_content');

/*** Portfolio ***/
Route::resource('portfolios','PortfoliosController')->except(['index','show']);
Route::get('/portfolio/{dep_id}', 'PortfoliosController@index')->name('portfolio');
Route::post('/portfolios/show', 'PortfoliosController@show')->name('portfolios.show');
Route::post('/portfolios/showByDate', 'PortfoliosController@showByDate')->name('portfolios.showByDate');
/*** Task ***/
Route::resource('tasks', 'TaskController')->except(['index']);
Route::get('task/{dep_id}', 'TaskController@index')->name('task');
Route::get('calendarTasks', 'TaskController@calendarTasks')->name('calendarTasks');
Route::get('assignedTasks/{task_id}', 'TaskController@assignedTasks')->name('assignedTasks');
Route::post('task/bringDepartmentUsers', 'TaskController@bringDepartmentUsers')->name('bringDepartmentUsers');
Route::post('task/updateTaskStatus', 'TaskController@updateTaskStatus')->name('updateTaskStatus');
Route::post('task/updateTasks', 'TaskController@updateTasks')->name('updateTasks');
Route::post('task/bringEditView', 'TaskController@bringEditView')->name('bringEditView');


/*** Project Dashboard ***/
Route::resource('project_dashboard','ProjectDashboardController')->except(['index','show']);
Route::get('/project_dashboard/{dep_id}', 'ProjectDashboardController@index')->name('project_dashboard');
Route::get('/project_dashboard/show/{id}/{dep_id}', 'ProjectDashboardController@show')->name('project_dashboard.show');
Route::post('/project_dashboard/show_dashboard', 'ProjectDashboardController@show_dashboard')->name('project_dashboard.show_dashboard');
Route::post('/project_dashboard/filter_project_progress', 'ProjectDashboardController@filter_project_progress')->name('project_dashboard.filter_project_progress');
Route::post('/project_dashboard/weekly_report', 'ProjectDashboardController@weekly_report')->name('project_dashboard.weekly_report');


/*** National Projects [NPMIS] ***/

/*** Projects ***/
Route::resource('national_projects','NPmis\NationalProjectsController')->except(['index']);
Route::get('national_project', 'NPmis\NationalProjectsController@index')->name('national_project');
Route::post('getProjectType', 'NPmis\NationalProjectsController@getProjectType')->name('getProjectType');
Route::post('createLocation', 'NPmis\NationalProjectsController@createLocation')->name('national_projects.createLocation');
Route::post('storeLocation', 'NPmis\NationalProjectsController@storeLocation')->name('national_projects.storeLocation');
Route::get('editLocation/{id}', 'NPmis\NationalProjectsController@editLocation')->name('national_projects.editLocation');
Route::get('updateLocation/{id}', 'NPmis\NationalProjectsController@updateLocation')->name('national_projects.updateLocation');
Route::resource('activities','NPmis\ActivitiesController')->except(['index']);
Route::post('moreActivities', 'NPmis\ActivitiesController@moreActivities')->name('moreActivities');
Route::resource('constructions','NPmis\ConstructionsController')->except(['index']);
Route::post('constructions/progress/create', 'NPmis\ConstructionsController@create_progress')->name('constructions.create_progress');
Route::post('constructions/problems/create', 'NPmis\ConstructionsController@create_problems')->name('constructions.create_problems');
Route::post('constructions/labor/create', 'NPmis\ConstructionsController@create_labor')->name('constructions.create_labor');
Route::post('constructions/equipment/create', 'NPmis\ConstructionsController@create_equipment')->name('constructions.create_equipment');
Route::post('constructions/payment/create', 'NPmis\ConstructionsController@create_payment')->name('constructions.create_payment');
Route::post('constructions/progress/store', 'NPmis\ConstructionsController@store_progress')->name('constructions.store_progress');
Route::post('constructions/problems/store', 'NPmis\ConstructionsController@store_problems')->name('constructions.store_problems');
Route::post('constructions/labor/store', 'NPmis\ConstructionsController@store_labor')->name('constructions.store_labor');
Route::post('constructions/equipment/store', 'NPmis\ConstructionsController@store_equipment')->name('constructions.store_equipment');
Route::post('constructions/payment/store', 'NPmis\ConstructionsController@store_payment')->name('constructions.store_payment');
Route::get('constructions/progress/view/{id}', 'NPmis\ConstructionsController@view')->name('constructions.view');
Route::post('constructions/progress/update', 'NPmis\ConstructionsController@update_progress')->name('constructions.update_progress');
Route::post('constructions/problems/update', 'NPmis\ConstructionsController@update_problems')->name('constructions.update_problems');
Route::post('constructions/labor/update', 'NPmis\ConstructionsController@update_labor')->name('constructions.update_labor');
Route::post('constructions/equipment/update', 'NPmis\ConstructionsController@update_equipment')->name('constructions.update_equipment');
Route::post('constructions/payment/update', 'NPmis\ConstructionsController@update_payment')->name('constructions.update_payment');
Route::post('activities/progress', 'NPmis\ActivitiesController@getProgress')->name('getProgress');
Route::post('moreProblem', 'NPmis\ConstructionsController@moreProblem')->name('moreProblem');
Route::post('moreLabor', 'NPmis\ConstructionsController@moreLabor')->name('moreLabor');
Route::post('moreEquipment', 'NPmis\ConstructionsController@moreEquipment')->name('moreEquipment');
Route::post('morePayment', 'NPmis\ConstructionsController@morePayment')->name('morePayment');
Route::post('constructions/project/complete', 'NPmis\ConstructionsController@project_complete')->name('constructions.project_complete');
Route::post('constructions/project/stop', 'NPmis\ConstructionsController@project_stop')->name('constructions.project_stop');
Route::post('filterNationalProjects', 'NPmis\NationalProjectsController@filterNationalProjects')->name('filterNationalProjects');

}); // End of the auth middleware
