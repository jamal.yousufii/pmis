<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('project/type','NPmis\Api\DashboardController@getProjectType');
Route::get('project_report','NPmis\Api\DashboardController@getProjectReport');
Route::post('national_projects/list','NPmis\Api\DashboardController@nationalProjectList');
Route::post('national_projects/type/list','NPmis\Api\DashboardController@nationalProjectListByType');
// Get Nation project detials
Route::post('get_national_project_details','NPmis\Api\DashboardController@getNationalProjectDetails');
//Get Projects Activities
Route::post('get_project_activities','NPmis\Api\DashboardController@getProjectActivities');
Route::post('get_project_general','NPmis\Api\DashboardController@getProjectGeneral');
Route::post('get_project_implementation','NPmis\Api\DashboardController@getProjectImplementation');
// Dashboard route
Route::post('project/dashboard/data','NPmis\Api\DashboardController@getProjectsDashboardData');
//Get Projects Data Graph
Route::post('get_project_data_graph','NPmis\Api\DashboardController@getProjectDataGraph');
//Get Projects Data By Search
Route::post('get_national_projects_by_search','NPmis\Api\DashboardController@getNationalProjectsBySearch');
// Stor Feedback
Route::post('store_project_feedback','NPmis\Api\DashboardController@store_project_feedback');
Route::post('show_project_feedback','NPmis\Api\DashboardController@show_project_feedback');
